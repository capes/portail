Calendrier M2
============

B3 avec M. Guichard : 06/04 ; 27/04 ; 04/05


Semaine 19
----------

### Mardi 10 mai

Deux séances d'oraux :

| Quand       | Où        | Quoi  | Qui                     |
|-------------|-----------|-------|-------------------------|
| 8:30-10:00  | M3 211    | UE A1 | Ph. Marquet, P. Thibaud |
| 10:15-11:45 | M3 Turing | UE A1 | B. Papegay, P. Thibaud  |

Semaine 18
----------

### Mardi 3 mai ###

| Quand       | Où     | Quoi  | Qui                     |
|-------------|--------|-------|-------------------------|
| 8:30-10:00  | M3-226 | UE A1 | B. Papegay , P. Thibaud |
| 10:15-11:45 | M3 226 | UE A1 | B. Papegay , P. Thibaut |

### Mercredi 4 mai ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M3 211       | UE B3 | M. Guichard |

Semaine 17
-----------

### Mardi 26 avril ###

| Quand      | Où     | Quoi  | Qui                     |
|------------|--------|-------|-------------------------|
| 8:30-10:00 | m3-226 | UE A1 | Ph. Marquet, P. Thibaud |


### Mercredi 27 avril ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M3-211       | UE B3 | M. Guichard |
| 13:00-14:30 | M3-226       | UE A1 | B. Papegay  |


### Jeudi 28 avril ###

| Quand       | Où      | Quoi    | Qui        |
|-------------|---------|---------|------------|
| 13:00-15:30 | M5-A4   | UE A1/2 | B. Papegay |

- tp : [programmation dynamique](ue1/progdyn/Readme.md)

Semaine 15 et 16
----------------
Congé

Semaine 14
----------

### Lundi 4 avril ###

stage (étudiants)

### Mardi 5 avril ###

| Quand       | Où       | Quoi  | Qui                     |
|-------------|----------|-------|-------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | Ph. Marquet, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay              |

*Note*
- cours de l'après-midi : problèmes d'optimisation

### Mercredi 6 avril ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M1 202       | UE B3 | M. Guichard |
| 13:00-14:30 | M1-Hadamard  | UE A1 | B. Papegay  |


### Jeudi 7 avril ###

| Quand       | Où      | Quoi    | Qui        |
|-------------|---------|---------|------------|
| 13:00-15:30 | M5-A4   | UE A1/2 | B. Papegay |

- tp : [knn et les arbres de décision](ue1/knn_vs_classifier/tp_hp_knn.md)

Semaine 13
----------

écrits les 30 et 31 mars

Semaine 12
----------

### Lundi 21 mars ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 10:15-11:45 | M5 A13   | UE A2 | P. Thibaud |
| 13:00-14:30 | M1 Fatou | UE A1 | B. Papegay |

*notes*
- cours de l'après-midi : l'algorithme de Bellman

Semaine 11
----------

### Lundi 14 mars ###

stage (étudiants)

### Mardi 15 mars ###

| Quand       | Où       | Quoi  | Qui                     |
|-------------|----------|-------|-------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | P. Thibaud, Ph. Marquet |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay              |

*Note*
- cours de l'après-midi : logique et preuve de programme.

### Mercredi 16 mars ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |

*Note*
- vérifier avec les enseignants du matin si les cours sont bien maintenus
- cours de l'après-midi : les paradigmes de programmation

### Jeudi 17 mars ###

| Quand       | Où      | Quoi    | Qui        |
|-------------|---------|---------|------------|
| 10:15-11:45 | M1(202) | UEA1    | B. Papegay |
| 13:00-14:30 | M5-A4   | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4   | UE A1/2 | B. Papegay |

*Note*
- TP : [knn vs arbre de décision](ue1/knn_vs_classifier/tp_hp_knn.md)
  
### Vendredi 11 mars ###

stage (étudiants)


Semaine 10
----------

### Lundi 7 mars ###

stage (étudiants)

### Mardi 8 mars ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*Note*
- cours de l'après-midi : logique et preuve de programme.

### Mercredi 9 mars ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M1 202       | UE B3 | M. Guichard |
| 13:00-14:30 | M1-Hadamard  | UE A1 | B. Papegay  |

*Note*
- vérifier avec les enseignants du matin si les cours sont bien maintenus
- cours de l'après-midi : les paradigmes de programmation

### Jeudi 10 mars ###

| Quand       | Où    | Quoi    | Qui        |
|-------------|-------|---------|------------|
| 13:00-14:30 | M5-A4 | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4 | UE A1/2 | B. Papegay |

*Note*
- réaliser une carte des stations vlille avec javascript, leaflet et opendata. 
  
### Vendredi 11 mars ###

stage (étudiants)


Semaine 9
---------

### Mercredi 2 Mars  ###

DS A2 P1 Fresnel , 8h00-12h00

### Jeudi 3 Mars  ###

DS A1 (rattrapage S1), SUP Amphi Galilée, 8h00-12h00

Semaine 8
---------

### Lundi 21 février ###

stage 

### Mardi 22 Février ###

| Quand       | Où           | Quoi  | Qui                    |
|-------------|--------------|-------|------------------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226       | UE A1 | B. Papegay             |

*notes :*
- cours de l'après-midi : la classification

### Mercredi 23 février ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:001 | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M3 211       | UE B3 | M. Guichard |
| 13:00-14:30 | Amphi Turing | UE A1 | B. Papegay  |

*notes:*
- cours de l'après-midi : sécurisation.

### Jeudi 24 février ###

| Quand       | Où           | Quoi    | Qui        |
|-------------|--------------|---------|------------|
| 10:15-11:45 | Amphi Turing | UE A1   | B. Papegay |
| 13:00-14:30 | M5-A4        | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4        | UE A1/2 | B. Papegay |


Semaine 7
---------

Interruption pédagogique 

Semaine 6
---------

Interruption pédagogique 

*si vous le souhaitez* : 

### Mercredi 9 février ###

DS A1, au A5 , 8h00-13h00

Semaine 5
---------

### Lundi 31 janvier ###

stage (étudiants)

### Mardi 1 février ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*Note*
- cours de l'après-midi : le hachage.

### Mercredi 2 février ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M1 202       | UE B3 | M. Guichard |
| 13:00-14:30 | M1-Hadamard  | UE A1 | B. Papegay  |

*Note*
- cours de l'après-midi : La sécurisation

### Jeudi 3 février ###

| Quand       | Où    | Quoi    | Qui        |
|-------------|-------|---------|------------|
| 13:00-14:30 | M5-A4 | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4 | UE A1/2 | B. Papegay |

*Note*
- Jury S1-S3 le matin
- TP : [Icewalker](./ue1/icewalker/icewalker.md)
  
### Vendredi 4 février ###

stage (étudiants)

Semaine 4
----------

### Lundi 24 janvier ###

stage (étudiants)

### Mardi 25 janvier ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*Note*
- cours de l'après-midi : le hachage.

### Mercredi 26 janvier ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M1 202       | UE B3 | M. Guichard |
| 13:00-14:30 | M1-Hadamard  | UE A1 | B. Papegay  |

*Note*
- cours de l'après-midi : Dijkstra (pour Amélie)

### Jeudi 27 janvier ###

| Quand       | Où    | Quoi    | Qui        |
|-------------|-------|---------|------------|
| 13:00-14:30 | M5-A4 | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4 | UE A1/2 | B. Papegay |

*Notes*
- tp : [les filtres de Bloom](./ue1/bloom/bloom.md)
  
### Vendredi 28 janvier ###

stage (étudiants)

Semaine 3
----------

### Lundi 17 janvier ###

stage (étudiants)

### Mardi 18 janvier ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 8:30-10:00  | M1 Fatou  | UE A1 | B. Papegay |
| 13:00-14:30 | M3 Turing | UE A1 | B. Papegay |

*Note*
- cours de l'après-midi : le hachage.

### Mercredi 19 janvier ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M1 202       | UE B3 | M. Guichard |
| 13:00-14:30 | M1-Hadamard  | UE A1 | B. Papegay  |

*Note*
- cours de l'après-midi : Dijkstra (pour Amélie) et OSPF (pour Marc)

### Jeudi 20 janvier ###

| Quand       | Où    | Quoi    | Qui        |
|-------------|-------|---------|------------|
| 13:00-14:30 | M5-A4 | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4 | UE A1/2 | B. Papegay |

*Notes*
- tp : 
  + normalisation et ihm (création d'un formulaire d'interrogation).
  + utilisation du hachage. dans la résolution des jeux.
  
### Vendredi 21 janvier ###

stage (étudiants)

Semaine 2
----------

### Mardi 11 janvier ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M3 226 | UE A1 | P. Thibaud |
| 10:15-11:45 | M3 226 | UE A1 | B. Papegay |
| 13:00-14:30 | M3 226 | UE A1 | B. Papegay |

Notes
- cours du matin : le hachage. (en lien avec les BDD) ;
- cours de l'après-midi : les arbres (avec les m1).

### Mercredi 12 janvier ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard  |
| 14:45-16:15 | M5-A6        | UE A1 | B. Papegay   |

Notes :
- Les cours de M. Guichard sont sous réserve ;
- cours de l'après-midi : Dijkstra (pour Amélie) et OSPF (pour Marc)

### Jeudi 13 janvier ###

| Quand       | Où        | Quoi    | Qui        |
|-------------|-----------|---------|------------|
| 10:15-11:45 | M3-Turing | UE A1   | B. Papegay |
| 13:00-14:30 | M5-A4     | UE A1/2 | P. Thibaud |
| 14:45-16:15 | M5-A4     | UE A1/2 | P. Thibaud |

Notes :
- cours : les arbres (avec les m1, si vous n'avez pas inspe)
- tp : normalisation et ihm (création d'un formulaire d'interrogation).

Semaine 1
----------

### Mardi 4 janvier ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M1-LIE | UE A1 | P. Thibaud |
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 |        | UE A1 | B. Papegay |

- [Cours sur les bases de données](./ue1/donnees/donnees.md)

### Jeudi 6 janvier ###

| Quand       | Où        | Quoi    | Qui        |
|-------------|-----------|---------|------------|
| 10:15-11:45 | M3-Turing | UE A1   | B. Papegay |
| 13:00-14:30 | M5-A4     | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4     | UE A1/2 | B. Papegay |

- [TP SQL 1](./ue1/sql1/Readme.md)
- [TP SQL 2](./ue1/sql2/Readme.md)
- [TP Normalisation](./ue1/sql4/tp_normalisation.md)

Semaine 50
----------

### Mercredi 15 décembre ###

ds d'ue A1
8h - 13h, M1 Galois

| Quand       | Où    | Quoi  | Qui         |
|-------------|-------|-------|-------------|
| 14:00-15:30 | M5-A6 | UE B3 | M. Guichard |

### Jeudi 16 décembre ###

ds d'ue A2
8h30 - 12h30, M3 Turing

Semaine 49
----------

### Lundi 6 décembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 7 décembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M1-LIE | UE A2 | P. Thibaud |

### Mercredi 8 décembre ###

| Quand       | Où     | Quoi  | Qui          |
|-------------|--------|-------|--------------|
| 8:30-10:00  |        | *     |              |
| 10:15-11:45 | M1 302 | UE B3 | M. Guichard  |
| 13:00-14:30 | M5-A6  | UE C  | F. De Comité |
| 14:45-16:15 | M5-A6  | UE A1 | B. Papegay   |

### Jeudi 9 décembre ###

| Quand       | Où     | Quoi | Qui         |
|-------------|--------|------|-------------|
| 13:00-14:30 | M5-A15 | UE C | M. Pupin    |

Semaine 48
----------

### Lundi 29 novembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 30 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:00-11:30 | M1 LIE | UE A2 | P. Thibaud |


### Mercredi 1 décembre ###

| Quand       | Où     | Quoi  | Qui         |
|-------------|--------|-------|-------------|
| 8:30-10:00  |        | *     |             |
| 10:15-11:45 | M1 302 | UE B3 | M. Guichard |
| 13:00-14:30 |        | *     |             |
| 14:45-16:15 | M5-A6  | UE A1 | B. Papegay  |

`*` : ces plages sont laissées libres pour permettre une entrevue avec 
      votre tuteur de mémoire.

thème ue 1 : ihm sur le web.

### Jeudi 2 décembre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13:00-14:30 | M5-A15 | UE A1 | B. Papegay |
| 14:30-16:15 |        | *     |            |

`*` : ces plages sont laissées libres pour permettre une entrevue avec 
      votre tuteur de mémoire.

TP : ihm sur le web.

Semaine 47
----------

### Lundi 22 novembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 23 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:00-11:30 | M1 LIE | UE A2 | P. Thibaud |


### Mercredi 24 novembre ###

| Quand       | Où    | Quoi  | Qui        |
|-------------|-------|-------|------------|
| 8:30-10:00  |       | *     |            |
| 10:15-11:45 |       | *     |            |
| 13:00-14:30 |       | *     |            |
| 14:45-16:15 | M5-A6 | UE A1 | B. Papegay |

`*` : ces plages sont laissées libres pour permettre une entrevue avec 
      votre tuteur de mémoire.

thème ue 1 : ihm sur le web.

### Jeudi 25 novembre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13:00-14:30 | M5-A15 | UE A1 | B. Papegay |
| 14:30-16:15 |        | *     |            |

`*` : ces plages sont laissées libres pour permettre une entrevue avec 
      votre tuteur de mémoire.

TP : ihm sur le web.

Semaine 46
----------

### Lundi 15 novembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 16 novembre ###

Avec les M1, en fonction de vos disponibilités : 
- Entrainement à l'oral ;
- Cours Réseau

[Voir ici](./calendrier.md)

### Mercredi 17 novembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard  |
| 13:00-14:30 | M5-A6        | UE A1 | F. De Comité |
| 14:45-16:15 | M5-A6        | UE A1 | B. Papegay   |

### Jeudi 18 novembre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi    | Qui |
|-------------|--------|---------|-----|
| 13:00-14:30 | M5-A15 | UE A1/2 |     |

Semaine 45
----------

### Lundi 8 novembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 9 novembre ###

Avec les M1, en fonction de vos disponibilités : 
- Entrainement à l'oral ;
- Cours Réseau

[Voir ici](./calendrier.md)

### Mercredi 10 novembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard  |
| 13:00-14:30 | M5-A6        | UE A1 | F. De Comité |
| 14:45-16:15 | M5-A6        | UE A1 | B. Papegay   |

UE A1 : Conception d'un serveur utilisant
- flask
- javascript
- sqlite
(amenez votre portable)


Semaine 42
----------

### Lundi 18 octobre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 19 octobre ###

Avec les M1, en fonction de vos disponibilités : 
- Entrainement à l'oral ;
- Cours Réseau

[Voir ici](./calendrier.md)

### Mercredi 20 octobre ###

| Quand      | Où        | Quoi     | Qui        |
|------------|-----------|----------|------------|
| 8:00-13:00 | C15 - 006 | DS UE A2 | P. Thibaud |

Surveillance assurée par M. Volker

### Jeudi 21 octobre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi | Qui      |
|-------------|--------|------|----------|
| 13:00-14:30 | M5-A15 | UE C | M. Pupin |

[tp processing](./ue3/py5/Readme.md)

Semaine 41
----------

### Lundi 11 octobre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 12 octobre ###

Avec les M1, en fonction de vos disponibilités : 
- Entrainement à l'oral ;
- Cours Réseau

[Voir ici](./calendrier.md)

### Mercredi 13 octobre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard  |
| 13:00-14:30 | M5-A6        | UE A1 | F. De Comité |
| 14:45-16:15 | M5-A6        | UE A1 | B. Papegay   |

UE A1 : Interaction client serveur (de socket à flask)
(amenez votre portable)

### Jeudi 14 octobre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi | Qui        |
|-------------|--------|------|------------|
| 13h00-14:30 | M5-A16 | UE C | B. Papegay |

UE C : Présentation des sujets, des modalités.

Semaine 40
----------

### Lundi 4 octobre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 5 octobre ###

Avec les M1, en fonction de vos disponibilités : 
- Entrainement à l'oral ;
- Cours Réseau

[Voir ici](./calendrier.md)

### Mercredi 6 octobre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard  |
| 13:00-14:30 | M5-A6        | UE A1 | F. De Comité |
| 14:45-16:15 | M3-211       | UE A1 | B. Papegay   |

### Jeudi 7 octobre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi        | Qui        |
|-------------|--------|-------------|------------|
| 13h00-14:30 | M5-A16 | UE A1/2, tp | P. Thibaud |


Semaine 39
----------

### Lundi 27 septembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 28 septembre ###

Avec les M1, en fonction de vos disponibilités : 
- Entrainement à l'oral ;
- Cours Réseau

[Voir ici](./calendrier.md)

### Mercredi 29 septembre ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud  |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard |
| 13:00-14:30 | M5-A6        | UE C  | M. Pupin    |
| 14:45-16:15 | M3-A6        | UE C  | M. Pupin    |

### Jeudi 30 septembre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi        | Qui        |
|-------------|--------|-------------|------------|
| 13h00-14:30 | M5-A16 | UE A1/2, tp | P. Thibaud |

- Projet Labyrinthe

Semaine 38
----------

### Lundi 20 septembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mardi 21 septembre ###

Avec les M1, en fonction de vos disponibilités : 
- Entrainement à l'oral ;
- Cours Réseau

[Voir ici](./calendrier.md)

### Mercredi 22 septembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard  |
| 13:00-14:30 | M5-A6        | UE A1 | F. De Comité |
| 14:45-16:15 | M3-211       | UE A1 | B. Papegay   |

### Jeudi 23 septembre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi        | Qui        |
|-------------|--------|-------------|------------|
| 13h00-14:30 | M5-A16 | UE A1/2, tp | P. Thibaut |

[implanter les parcours de graphe](./ue1/parcours_graphes/tp_parcours.md)

[utilisation de networkx](./ue2/graphes/Readme.md)


Semaine 37
----------

### Lundi 13 septembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mercredi 15 septembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 10:15-11:45 | M1 302       | UE B3 | M. Guichard  |
| 13:00-14:30 | M5-A6        | UE A1 | F. De Comité |
| 14:45-16:15 | M5 A16       | UE A1 | B. Papegay   |

**La séance d'ue b3 n'a pas pu être assurée : B. Papegay n'a pas poussé sur le git quand il aurait dû**

### Jeudi 16 septembre ###

Matin : INSPE selon calendrier

| Quand       | Où     | Quoi        | Qui        |
|-------------|--------|-------------|------------|
| 13h00-14:30 | M5-A16 | UE A1/2, tp | B. Papegay |

[lien vers le tp](./ue1/parcours_graphes/tp_parcours.md)

Semaine 36
----------

### Lundi 6 septembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay |
| 10:15-12:45 | Amphi Turing | UE A1 | B. Papegay |

### Mercredi 8 septembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M1 Chatelet  | UE A1 | F. De Comité |
| 14:45-16:15 | M5 A16       | UE A1 | B. Papegay   |

### Jeudi 9 septembre ###

Matin : INSPE selon calendrier

| Quand  | Où                       | Quoi       | Qui      |
|--------|--------------------------|------------|----------|
| 14h00- | Amphithéâtre de l'IRCICA | Conférence | M. Pupin |

**Conférence :**
Le manque de femmes en informatique n’est pas une fatalité
Isabelle Collet, professeure à l'Université de Genève


pour y aller : https://ircica.univ-lille.fr/plan-dacces
Jeudi 9 septembre, **14h**

Semaine 35
----------

### Jeudi 2 septembre ###

  - pré-rentrée (Amphi Turing, Bâtiment M3) à 14h00
