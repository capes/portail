---
title: Mutabilité des données
date: juin 2019
author: Éric Wegrzynowski, Master MEEF NSI
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.1.7
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

<!-- #region -->
**Objectifs :**

* avoir un aperçu de ce que sont les variables en Python
* comprendre le partage de valeurs (aliasing)
* comprendre les effets de ce partage pour des valeurs mutables


**Moyen :**


* [Python tutor](http://pythontutor.com)

**Référence :**

* fortement inspiré du document ressource NSI [Types mutables et problèmes associés](https://cache.media.eduscol.education.fr/file/NSI/77/8/RA_Lycee_G_NSI_repd_types_mutables_1170778.pdf) trouvé sur Eduscol.
<!-- #endregion -->

```python
# Commande pour ajouter la commande magique %%tutor
from metakernel import register_ipython_magics
register_ipython_magics()
```

# Variables en Python

<!-- #region -->
On présente souvent la notion de variable aux programmeurs débutant avec l'image d'une boîte : le nom de la variable représente la boîte et la valeur de la variable est le contenu de la boîte.

Ainsi avec la séquence d'instructions suivantes

```python
a = 1
b = a
b = b + 1
```
on peut dire que la première instruction range dans une boîte nommée `a` le nombre 1, la deuxième range dans une boîte nommée `b` le contenu de la boîte `a`, et la troisième prend le contenu de la boîte `b`, lui ajoute 1, et range le résultat de cette opération dans la boîte `b`.

Cette image est certainement correcte pour des langages de programmation comme le C. Mais elle ne l'est pas pour Python.
<!-- #endregion -->

L'illustration avec Python Tutor qui suit le montre. 

(**Remarque :** l'intégration de Python Tutor au notebook n'offre pas toutes les fonctionnalités que permet Python Tutor. Pour exploiter les fonctionnalités manquantes, on peut suivre ce [lien](http://pythontutor.com/live.html#code=a%20%3D%201%0Ab%20%3D%20a%0Aprint%28'adresses%20des%20valeurs%20de%20a%20et%20b%20%3A%20',%20id%28a%29,%20id%28b%29%29%0Aa%20%3D%20True%0Aprint%28'adresses%20des%20valeurs%20de%20a%20et%20b%20%3A%20',%20id%28a%29,%20id%28b%29%29%0Al1%20%3D%20%5B1,%202,%203%5D%0Aprint%28'adresse%20de%20la%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l1%20%3A%20',id%28l1%29%29%0Al2%20%3D%20l1%0Aprint%28'adresse%20de%20la%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l2%20%3A%20',%20id%28l2%29%29%0Al3%20%3D%20%5B1,%202,%203%5D%0Aprint%28'adresse%20de%20la%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l3%20%3A%20',%20id%28l3%29%29%0At1%20%3D%20%281,%202,%203%29%0Aprint%28'adresse%20du%20tuple%20r%C3%A9f%C3%A9renc%C3%A9%20par%20t1%20%3A%20',%20id%28t1%29%29%0A&cumulative=false&curInstr=13&heapPrimitives=true&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) et choisir l'option  `render all objects on the heap (Python)` si elle n'est pas déjà choisie.)

```python
%%tutor

a = 1
b = a
print('adresses des valeurs de a et b : ', id(a), id(b))
a = True
print('adresses des valeurs de a et b : ', id(a), id(b))
l1 = [1, 2, 3]
print('adresse de la liste référencée par l1 : ',id(l1))
l2 = l1
print('adresse de la liste référencée par l2 : ', id(l2))
l3 = [1, 2, 3]
print('adresse de la liste référencée par l3 : ', id(l3))
t1 = (1, 2, 3)
print('adresse du tuple référencé par t1 : ', id(t1))
```

On retiendra de cette démo qu'en Python

* les variables sont des références (pointeurs) vers les valeurs
* certaines valeurs sont référencées par plusieurs variables
* la fonction prédéfinie `id` renvoie l'adresse en mémoire d'une valeur (attention pas de la variable).


# Phénomène de partage de valeurs (ou alias)


Les listes de Python sont mutables

```python
l1 = [3, 1, 4]
l2 = l1
l2.append(1)
l2[2] = 2
l1
```

```python
l2 is l1
```

Les $n$-uplets (ou tuples) de Python ne sont pas mutables.

```python
t1 = (3, 1, 4)
t2 = t1
t2 = t2 + (1,)
t1
```

```python
t2 is t1
```

```python
%%tutor
l1 = [3, 1, 4]
l2 = l1
l3 = l1
l2.append(1)
l3 = l3 + [1]
l2[2] = 2
t1 = (3, 1, 4)
t2 = t1
t2 = t2 + (1,)
```

## Exercices autour de la copie


Pour éviter ce piège, faire une copie de la liste.

```python
%%tutor
l1 = [3, 1, 4]
l2 = l1.copy()
l2.append(1)
l2[2] = 2
```

**Exercice**

Écrire une fonction copie qui renvoie une copie de la liste passée en argument.

```python
def copie(l):
    res = []
    for elt in l:
        res.append(elt)
    return res
```

```python
%%tutor
def copie(l):
    res = []
    for elt in l:
        res.append(elt)
    return res

l1 = list(range(4))
l2 = copie(l1)
```

**Exercice**

M. X utilise la fonction précédente pour copier une matrice :

```python
M = [[1, 2], 
     [3, 4]]
N = copie(M)
N
```

```python
N is M
```

puis il modifie le terme en haut à gauche de la matrice M :

```python
M[0][0] = 0
```

**Q1** Que se passe-t-il ?

```python
%%tutor
def copie(l):
    res = []
    for elt in l:
        res.append(elt)
    return res
M = [[1, 2], 
     [3, 4]]
N = copie(M)
M[0][0] = 0
```

```python
N[0] is M[0]
```

**Q2** Réaliser une fonction ``copie_matrice``.

```python
def copie_matrice(M):
    res = []
    for i in range(len(M)):
        # copie de la ligne i
        ligne = []
        for elt in M[i]:
            ligne.append(elt)
        res.append(ligne)
    return res
```

```python
N = copie_matrice(M)
N[0][0] = 12
print('M = ', M)
print('N = ', N)
```

```python
N[0] is M[0]
```

**Exercice**

Écrire une fonction ``copie_profonde`` qui renvoie une copie de la liste passée en argument, la copie étant elle-même effectuée récursivement sur les listes éventuellement contenues. Autrement dit, la fonction doit copie les listes, les listes de listes, les listes de listes de listes ...

Indications : fonction récursive. Utiliser ``isinstance(elt, list)``.

```python
def copie_profonde(l):
    res = []
    for elt in l:
        if isinstance(elt, list):
            res.append(copie_profonde(elt))
        else:
            res.append(elt)
    return res
```

```python
l1 = [1, [2, 3], [[4, 5], [6, 7]]]
l2 = copie_profonde(l1)
l2
```

```python
[L2 is L1 for L2, L1 in ((l2, l1), (l2[1], l1[1]), (l2[2], l1[2]), 
                         (l2[2][0], l1[2][0]), (l2[2][1], l1[2][1]))]
```

# Fonctions dont un paramètre est mutable

```python
def truc(t):
    t.append(0)
```

```python
t = [1]
s = [2]
truc(s)
```

```python
s, t
```

**=>** Importance de bien spécifier les fonctions ! Y a-t-il effet de bord ou non sur certains paramètres.

Exemple des fonctions de tris : fonction `sorted` et méthode `sort` des listes.


# Mise en garde contre `+=` et consorts


Voici deux fonctions qui ne diffèrent que par le raccourci `+=`.

```python
def f(l):
    for k in range(len(l)):
        l += [l[k]]
    return l

def g(l):
    for k in range(len(l)):
        l = l + [l[k]]
    return l
```

```python
f([1, 2, 3])
```

```python
g([1, 2, 3])
```

```python
l = [1, 2, 3]
l1 = f(l)
l2 = g(l)
l1, l2
```

# Intérêt de m'immutabilité


# Données mutables et ensembles/dictionnaires


Pourquoi, en Python, les ensembles ne peuvent pas contenir de données mutables, et donc en particulier ne pas contenir de listes, ni d'ensembles, ni de dictionnaires ?

Pourquoi les clés d'un dictionnaire ne peuvent pas être des données mutables ?


Une réponse fréquente est que les listes, les ensembles, les dictionnaires ne sont pas *hachables*.

En Python, les objets hachables sont ceux disposant de la méthode spéciale `__hash__`.

La question initiale, peut donc se ramener à la question : Pourquoi la classe `list` (ou `set` ou `dict`) ne possède-t-elle pas de méthode spéciale `__hash__` ?


**Exercice :** Trouver une réponse à cette question.


# Mutabilité des listes récursives


## Deux visions des listes récursives avec Python tutor

```python
%%tutor
class ListError(Exception):
    def __init__(self, msg):
        self.message = msg

class List():

    def __init__(self, *args):
        if len(args) == 0:
            self.__content = dict()
        elif len(args) == 2:
            if isinstance(args[1], List):
                self.__content = {'head' : args[0],
                                  'tail' : args[1]}
            else:
                raise ListError('bad type for second argument')
        else:
            raise ListError('bad number of arguments')

    def is_empty(self):
        return len(self.__content) == 0

    def head(self):
        try:
            return self.__content['head']
        except KeyError:
            raise ListError('head: empty list')

    def tail(self):
        try:
            return self.__content['tail']
        except KeyError:
            raise ListError('tail: empty list')
    def set_head(self, x):
        try:
            self.__content['head'] = x
        except KeyError:
            raise ListError('set_head: empty list')

    def set_tail(self, l):
        if isinstance(l, List):
            try:
                self.__content['tail'] = l
            except KeyError:
                raise ListError('set_tail: empty list')
        else:
            raise ListError('set_tail: argument is not a list')
        
               
l = List(1, List(2, List(3, List())))
l3 = List(3, List())
l2 = List(2, l3)
l1 = List(1, l2)
```

## Effet insoupçonné de la mutation ?

```python
%%tutor
class ListError(Exception):
    def __init__(self, msg):
        self.message = msg

class List():

    def __init__(self, *args):
        if len(args) == 0:
            self.__content = dict()
        elif len(args) == 2:
            if isinstance(args[1], List):
                self.__content = {'head' : args[0],
                                  'tail' : args[1]}
            else:
                raise ListError('bad type for second argument')
        else:
            raise ListError('bad number of arguments')

    def is_empty(self):
        return len(self.__content) == 0

    def head(self):
        try:
            return self.__content['head']
        except KeyError:
            raise ListError('head: empty list')

    def tail(self):
        try:
            return self.__content['tail']
        except KeyError:
            raise ListError('tail: empty list')
    def set_head(self, x):
        try:
            self.__content['head'] = x
        except KeyError:
            raise ListError('set_head: empty list')

    def set_tail(self, l):
        if isinstance(l, List):
            try:
                self.__content['tail'] = l
            except KeyError:
                raise ListError('set_tail: empty list')
        else:
            raise ListError('set_tail: argument is not a list')
        
l3 = List(3, List())
l2 = List(2, l3)
l1 = List(1, l2)
l3.set_tail(l1)
```

```python

```
