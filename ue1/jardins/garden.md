

# Introduction

La métropole européenne de Lille dispose de 159 jardins. Vous avez la responsabilité informatique de ces données.

On veut créer un ensemble de fonctions permettant de : 

-   accéder aux propriétés des jardins (Nom, Quartier, ouverture au publique, &#x2026;)
-   créer de nouveaux jardins (pour étendre le logiciel).
-   créer une liste de jardins à partir d'un fichier.
-   connaitre le quartier disposant du maximum de jardin.
-   trier la liste des jardins par superficie, et l'enregistrer dans un fichier.
-   connaitre le quartier le 'plus vert'
-   &#x2026;


# Préparation


## Les fichiers CSV

Télécharger le fichier [mel<sub>gardens.csv</sub>](./mel_gardens.csv), contenant les données des jardins et espaces verts de la MEL.

Ce fichier a été téléchargé sur le [site open data de la MEL](https://opendata.lillemetropole.fr/pages/home/).

Les fichiers csv sont des fichiers texte représentant une table. 

-   à chaque ligne du fichier correspond une ligne de la table, la première ligne contenant ici les intitulés des colonnes.
-   sur une ligne, les cellules de la table sont séparées par &#x2026; un séparateur ! Souvent il s'agit d'une virgule
    (ce qui justifie l'extension **C** omma **S** eparated **V** alues) ou d'un point-virgule mais il n'y a pas de norme.
    ici le séparateur est un caractère de tabulation (`'\t'` en python.)


## Méthodes utiles en Python

-   méthodes des chaînes de caractères utiles :
    -   `strip`
    -   `split`
    -   `join`
-   méthodes des listes utiles :
    -   `sort`
-   fonctions utiles :
    -   `min`, `max`
    -   `sorted`

*consulter la documentation de ces fonctions et méthodes*


## Gestion des jardins

1.  Comment vous proposez vous de gérer les jardins ? Précisez les avantages/inconvénients de votre solution.
2.  Écrire une fonction `gardens_from_csv` permettant de lire un fichier csv dont le nom est passé sous forme d'une chaîne de caractères et qui renvoie la liste des jardins contenus dans le fichier.
3.  Écrire une fonction de comparaison `garden_comp` prenant en paramètre deux jardins `g1` et `g2` et renvoyant :
    -   un nombre négatif si `g1` est plus petit que `g2` ;
    -   un nombre positif si `g1` est plus grand que `g2` ;
    -   0 si `g1` et `g2` ont la même superficie.!
4.  Déterminer le nom du plus grand jardin. En une ligne de code ?
5.  Trier la liste des jardins par superficie. En une ligne de code ?
6.  Déterminer le quartier le plus vert. En deux lignes de code ?
7.  Écrire une fonction prenant en paramètre : 
    
    -   le nom sous forme d'une chaîne de caractères d'un fichier `csv` contenant une table de jardins ;
    -   le nom `quart` sous forme d'une chaîne de caractères d'un quartier  ;
    -   le nom `dest` sous forme d'une chaîne de caractère d'un fichier de sortie
    
    et qui écrit dans le fichier `dest` le nom, le type et la superficie (et uniquement ces attributs)
    des jardins du quartier `quart`. Les enregistrements seront triés par ordre décroissant de superficie.

