 QUARTIERS = {'Fives', 'Lille-Moulins', 'Bois-Blancs', 'Wazemmes', 'Lomme', 'Vauban-Esquermes',
              'Lille-Sud', 'Lille-Centre', 'Vieux-Lille', 'Faubourg de Béthune',
              'Saint-Maurice Pellevoisin', 'Hellemmes'}
 G_TYPES = {'Espace vert public', "Jardin d'Habitants", 'Ferme pédagogique', 'Cimetière'}
 G_SUBTYPES = {'Place', 'Pelouse', 'Parc', 'Réserve naturelle volontaire', 'Jardin botanique',
               'Cimetière', 'Jardin thérapeutique', 'Ferme pédagogique', 'Réserve écologique',
               'Jardins familiaux', 'Jardin communautaire', 'Square', 'Jardin public'}
