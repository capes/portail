# Arithmétique

Recopiez le fichier `arithmetique.py` du dossier `cesar` dans ce dépôt

1. Dans le fichier `arithmetique.py`, définissez la fonction *récursive* `pgcd` dont la
   spécification est donnée ci-dessous.

   On rappelle que pour tout $a > 0$, $pgcd(a, 0) = |a|$ et pour tout couple $(a, b)$ avec
   $b ≠ 0$, $pgcd(a, b) = pgcd(b, r)$ avec $r$ le reste de la division euclidienne de $a$
   par $b$.

```{.py}
def pgcd(a : int, b : int) -> int:
    """Renvoie le plus grand diviseur commun de `a` et `b`.
    Précondition : (a, b) ≠ (0, 0)
    $$$ pgcd(2024, 0)
    2024
    $$$ pgcd(42, -2024)
    2
    """
```

2. Encore dans le fichier `arithmetique.py` définissez la fonction *récursive*
   `coefficients_bezout` dont la spécification est donnée ci-dessous.

   On rappelle que avec $d = pgcd(a, b)$ et $a = bq + r$, alors $a u + b v = d$ si et
   seulement si $b u' + r v' = d$ avec $u = v'$ et $v = u' - v'q$ ; et que de plus si $b =
   0$, les coefficients de Bezout sont $(1, 0)$ si $a>0$, $(-1, 0) si $a<0$ et le pgcd est
   $|a|$.

```{.py}
def coefficients_bezout(a : int, b : int) -> tuple[int, int, int]:
    """Renvoie le triplet (u, v, d) tels que $a u + b v = d = pgcd(a, b)$
    obtenu à l'aide de l'algorithme d'Euclide étendu.
    Précondition : (a, b) ≠ (0, 0)
    $$$ coefficients_bezout(2024, 42)
    (-5, 241, 2)
    $$$ coefficients_bezout(42, -2024)
    (241, 5, 2)
    """
```

3. Toujours le fichier `arithmetique.py`, définissez la fonction `inverse_mod`
   dont la spécification est donnée ci-dessous.

```
def inverse_mod(a : int, n : int) -> int:
    """Renvoie l'unique inverse modulaire de a modulo n compris entre
    1 et n-1 inclus.
    Précondition : n > 0 et a inversible modulo n
    $$$ inverse_mod(3, 7)
    5
    $$$ inverse_mod(3, 26)
    9
    $$$ inverse_mod(47337338776803609, 114875698154581919)
    64484911222477487
```

# Cryptographie

4. Définissez les fonctions `cle_valide`, `chiffre_lettre`, `chiffre_message` et
   `dechiffre_message` après avoir soigneusement lu les spécifications.

5. Pour chacun des alphabets proposés par le module `alphabet`, calculez
   l'ensemble des clés $(a, b)$ valides sur cet alphabet en utilisant
   une définition en compréhension.

6. Pour chaque triplet `msg, clé, alphabet` qui suit, procédez au chiffrement puis
   déchiffrement.
   - `msg='CHIFFREMENTAFFINE', clé=(17, 2), alphabet = ALPHABETS['CAPITAL_LATIN']`
   - `msg='La fille du proviseur aime a voir decharger les peniches de potasse.', clé=(61, 17), alphabet = ALPHABETS['PRINTABLE_ASCII']`

7. Pour quelle(s) clé(s) le message `msg='ουκελαβονπολιναλλαγαρελπιςεφηκακα'`,
   donne-t-il le chiffré `'νξθγδσονυινδμυσδδσλσεγδιμαγκτθσθσ'` ?
