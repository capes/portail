# le chiffrement de Vigenère

## Chiffrement symétrique de Vigenère à la main

[les carrés de Vigenere](tikz_carre_vigenere.pdf)

### Chiffrement

Pour chiffrer un message , on choisi une ****clé**** sous la forme d'un
mot constitué des 26 lettres de l'alphabet latin.

Par exemple `CRY`

On dispose ensuite :

-   le message à chiffrer (le ****clair****) sur la première ligne ;
-   la clé, éventuellement répétée, sur la deuxième ligne

Le ****chiffré**** prendra place sur la troisième ligne.

Pour chaque lettre *c* du clair, et chaque lettre *l* de la clé, on
repère dans la table de Vigenère la cellule située aux coordonnées
(*c*,*l*). La lettre du chiffré est le contenu de cette cellule.

Par exemple :

| S | O | L | U | T | I | O | N |
|---|---|---|---|---|---|---|---|
| C | R | Y | C | R | Y | C | R |
| U | F | J | W | K | G | Q | E |


****analyse**** :

-   les deux `O` sont chiffrés par des lettres différentes ;
-   Il s'agit d'une **substitution polyalphabétique** ;
-   Le décryptage par analyse de fréquence devient difficile.

### Déchiffrement

**Méthode** : La méthode de déchiffrement est plus délicate. Pour
chaque lettre *k* du chiffré et chaque lettre *l* de la clé qui lui
correspond :

-   je cherche dans la ligne *l*, la lettre *k*. Elle se situe à la
    colonne *c* ;
-   la lettre située dans la première ligne de la colonne *c* est la
    lettre du clair correspondante.

**Exemple** : déchiffrer le message RPRJFL avec la clé CRY

## en machine

**à faire** renommer et compléter le fichier `squel_vigenere.py`

# la cryptanalyse de Vigenère

La cryptanalyse du chiffrement de Vigenère est "facile" si on connait la
longueur de la clé *k*: Chaque message $M_k$ constitué des
*k* ième lettres de chaque bloc est en effet chiffré avec un chiffrement
de César, ce qui est vulnérable à une analyse de fréquence.

La sécurité réside essentiellement dans la longueur de la clé :

-   si cette dernière est petite, le chiffrement n'est pas sûr mais la
    clé est facile à transmettre.
-   si cette dernière est égale à la longueur du message, on obtient un
    chiffrement parfait (cela s'apparente au masque jetable), mais la
    clé devient difficile à transmettre.

Il y a toutefois un moyen de déterminer la longueur de la clé lorsque
cette dernière est petite par rapport à la longueur du message.

# Indice de coïncidence.

Une manière de déterminer la longueur de la clé est d'utiliser un nombre
calculé à partir de l'alphabet et du texte (cela suppose connaitre
l'alphabet utilisé).

Soit un message de longueur *n*, on considère l'expérience aléatoire
consistant à choisir deux lettres au hasard dans le texte.

Quelle est la probabilité d'obtenir deux fois la même lettre ?

Pour simplifier, supposons que l'alphabet contienne la lettre "A", et
déterminons la probabilité d'obtenir deux "A".

On suppose que les tirages de deux lettres dont équiprobables. Le nombre
de couple de deux lettres est alors $\frac{n(n-1)}{2}$ et le nombre de
couple contenant deux fois la lettre A est $\frac{n_A(n_A-1)}{2}$ où
*n*<sub>*A*</sub> est le nombre d'occurrences de A dans le message. Par
conséquent, la probabilité d'obtenir deux A est

$$ \frac{n_A(n_A-1)}{n(n-1)} $$

Et donc, la probabilité d'obtenir deux lettres identiques est la somme :

$$\mbox{indice}(m) = \sum\_{\ell \in \mbox{alphabet}}\frac{n\_\ell(n\ell - 1)}{n(n-1)} 
= \frac{1}{n(n-1)}\sum\_{\ell \in \mbox{alphabet}}n\_\ell(n\ell - 1)$$

Ce nombre ne dépend que du texte. De plus, si *m*′ est le transformé de
*m* par une substitution mono-alphabétique, on a

indice(*m*) = indice(*m*′)

1.  Rédigez dans votre fichier `arithmetique.py` une fonction
    `indice_coincidence` dont voici la spécification :


    ``` python
    def indice_coincidence(message: str, alphabet = ALPHABET['CAPITAL_LATIN']) -> float:
        """
        Calcule l'indice de coïncidence d'un message.

        exemples:
    
        >>> msg = "CECI EST UN TEXTE EN FRANCAIS L INDICE DEVRAIT ETRE ELEVE"
        >>> ind = indice_coincidence(msg, ALPHABETS['CAPITAL_LATIN_SPACE'])
        >>> abs(ind - 0.09) < 0.01
        True
        """
        pass
    ```

    Supposons maintenant qu'un texte soit parfaitement aléatoire, dans
    ce cas, l'indice de coïncidence devrait se rapprocher de
    $\frac{1}{26}\simeq 0,038$. Par contre pour un texte en français,
    on obtient des valeurs sensiblement plus élevées, de l'ordre de
    0,07.

# Cryptanalyse du chiffrement de Vigenère

## Trouver la longueur de la clé de chiffrement

Une méthode pour déterminer la longueur de la clé utilisée est de
calculer l'indice de coïncidence :

-   du message
-   d'une lettre sur deux,
-   d'une lettre sur trois,
-   d'une lettre sur quatre,
-   …
-   d'une lettre sur *k*

Lorsque l'indice est élevé (supérieur à 0,05) il y a des chances pour
que *k* soit un multiple de la longueur clé.

2.  Téléchargez le fichier
    [cryptanalyse_vigenere_squel.py](./cryptanalyse_vigenere_squel.py)
    et renommez le en `cryptanalyse_vigenere.py`. Puis, implantez dans
    ce fichier la fonction `une_lettre_sur_k(msg, k)` dont la
    spécification est la suivante :

    ``` python
    def une_lettre_sur_k(msg: str, k: int) -> str:
       """
       renvoie la chaîne des caractères d'indice multiple de k de `msg`.
       précondition: k >= 1
       exemples:

       >>> une_lettre_sur_k("", 3)
       ""
       >>> msg = "BONJOUR"
       >>> une_lettre_sur_k(msg, 1)
       'BONJOUR'
       >>> une_lettre_sur_k(msg, 2)
       'BNOR'
       >>> une_lettre_sur_k(msg, 3)
       'BJR'
       >>> une_lettre_sur_k(msg, 4)
       'BO'
       """
       ...
    ```

    Vous pourrez mettre à profit la syntaxe `s[a:b:k]` utilisable pour
    les chaînes de caractères.

3.  On souhaite cryptanalyser le texte contenu dans la variable
    `CHIFFRE` du fichier `cryptanalyse_vigenere.py`. Pour cela, nous
    allons utiliser le module pylab ou pyplot de matplotlib pour
    produire un graphique avec :

    -   en abscisses, un nombre *k* ;
    -   en ordonnées, le nombre
        `indice_coincidence(une_lettre_sur_k(CHIFFRE, k), alphabet)`.

    On doit obtenir un graphique tel que celui ci (obtenu pour des
    valeurs de *k* compris entre 1 et 20) :

    ![](./19123ERf.png)

4.  En déduire la longueur probable de la clé

## Cryptanalyse

5.  En utilisant des attaques par analyse de fréquence, déterminer :

    -   la clé ;

    -   le message clair.

        ``` ipython
        K = 5 # 5 est un exemple, il faut trouver la bonne valeur
        alphabet = ALPHABETS['PRINTABLE_ASCII']
        cle = tuple(cle_cesar(CHIFFRE[i::K], ' ', alphabet)[0] for i in range(K))
        ```


    On a utilisé la fonction `cle_cesar` ainsi spécifiée :

    ``` python
    def cle_cesar(msg, car_freq, alphabet=ALPHABETS['CAPITAL_LATIN']):
        """
        :param msg: (str) un message
        :param car_freq: (str) le caractère supposé le plus fréquent 
                         dans la langue du message
        :param alphabet: (Alphabet) l'alphabet utilisé
        :return: (tuple of int) un tuple contenant les clés possibles.
        """
        pass

    ```
