#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:auteur(s): 
:date: 
:objet: Chiffrement/déchiffrement de messages par la méthode affine


"""

from alphabet import Alphabet, ALPHABETS

from arithmetique import pgcd, inverse_mod


def cle_valide(cle : tuple[int, int], alphabet : Alphabet) -> bool:
    """Renvoie
      - True si cle est une clé valide, i.e. si a est premier avec la taille de l'alphabet
      - False sinon
    Précondition : aucune

    $$$ cle_valide((4, 3), ALPHABETS['CAPITAL_LATIN'])
    False
    $$$ cle_valide((5, 3), ALPHABETS['CAPITAL_LATIN'])
    True
    $$$ cle_valide((5, 3), ALPHABETS['DECIMAL_DIGITS'])
    False
    $$$ cle_valide((12, 9), ALPHABETS['LOWER_GREEK'])
    True
    """


def chiffre_lettre(lettre : str, cle : tuple[int, int], alphabet : Alphabet) -> str:
    """Renvoie la lettre `lettre` chiffrée avec la clé cle.
    Précondition : cle = (a,b) doit être tel que a inversible modulo la taille de alphabet
    et lettre dans alphabet

    $$$ tuple(chiffre_lettre(k, (3, 7), ALPHABETS['CAPITAL_LATIN']) for k in 'ABC')
    ('H', 'K', 'N')
    $$$ tuple(chiffre_lettre(k, (3, 7), ALPHABETS['DECIMAL_DIGITS']) for k in '012')
    ('7', '0', '3')
    $$$ tuple(chiffre_lettre(k, (12, 9), ALPHABETS['LOWER_GREEK']) for k in 'μιακ')
    ('ρ', 'ζ', 'κ', 'ς')
    """

def chiffre_message(msg : str, cle : tuple[int, int], alphabet : Alphabet) -> str:
    """Renvoie le message msg chiffré avec la clé cle.
    Précondition : cle = (a,b) doit être tel que a inversible modulo la taille de alphabet
         les caractères de msg doivent être dans alphabet

    $$$ chiffre_message('ABC', (3, 7), ALPHABETS['CAPITAL_LATIN'])
    'HKN'
    $$$ decimal = ALPHABETS['DECIMAL_DIGITS']
    $$$ chiffre_message('012', (3, 7), decimal)
    '703'
    $$$ greek = ALPHABETS['LOWER_GREEK']
    $$$ chiffre_message('μιακ', (12, 9), greek)
    'ρζκς'
    """



def dechiffre_message(msg : str, cle : tuple[int, int], alphabet : Alphabet) -> str:
    """Renvoie le message msg déchiffré avec la clé cle.
    Précondition : cle = (a,b) doit être tel que a inversible modulo la taille de alphabet
         les caractères de msg doivent être dans alphabet

    $$$ dechiffre_message('HKN', (3, 7), ALPHABETS['CAPITAL_LATIN'])
    'ABC'
    $$$ decimal = ALPHABETS['DECIMAL_DIGITS']
    $$$ dechiffre_message('703', (3, 7), decimal)
    '012'
    $$$ greek = ALPHABETS['LOWER_GREEK']
    $$$ dechiffre_message('ρζκς', (12, 9), greek)
    'μιακ'
    """

