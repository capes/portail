# Description

Enigma est une machine électromecanique portable servant au chiffrement
et au déchiffrement de messages textuels. Elle a été inventée par
l'Allemand Athur Scherbius, reprenant un brevet du Néerlandais Hugo
Koch, datant de 1919.

Enigma a été utilisée principalement par les allemands (Die
Chiffriermaschine Enigma) pendant la Seconde Guerre Mondiale.

L'idée derrière Enigma est de s'approcher d'un chiffrement parfait en
changeant la correspondance entre les lettres à chaque lettre chiffrée.
La machine Enigma est une machine *electro-mécanique* : Elle est munie
d'un clavier et d'un "écran" composé de lettres pouvant s'illuminer.
Lorsque l'on appuie sur une touche, un circuit électrique se ferme et
une lettre s'illumine, correspondant à la lettre chiffrée.

![](EnigmaMachineLabeled.jpg)

Le circuit électrique est constitué de plusieurs élements en chaîne :

-   un tableau de connexions ;
-   des rotors :
-   un réflecteur.

La vidéo ci-dessous explique le fonctionnement de la machine et effectue
le calcul de la taille de l'espace des clés :

<https://www.youtube.com/embed/G2_Q9FoD-oQ>

Une clé est constituée :

-   des 10 cablages possibles de lettres dans le tableau des connexions
    ;
-   du choix des trois rotors (parmi 5) et de leur agencement ;
-   de leur position de départ ;
-   des cablages du reflecteur.

La taille de cet espace est de 158 962 555 217 826 360 000 clés ce qui
est gigantesque. Les clés étaient consignées dans des livres :

![](./enigma-secret-sheet.png)

à chaque jour correspondait une clé.

# En python

Le module `py-enigma` est une bibliothèque permettant de simuler le
fonctionnement d'une machine Enigma.

## Installation

Pour installer cette bibliothèque :

-   soit utiliser `pip` en ligne de commande :

    ``` shell
    pip3 install py-enigma
    ```

    ou

    ``` shell
    pip install py-enigma
    ```

    selon la version de `pip` installée sur votre système ;

Pour utiliser la bibliothèque dans un script, on utilise ensuite la
commande :

``` {.py .noeval}
from enigma.machine import EnigmaMachine
```

## Initialisation d'une machine

Pour initialiser une machine virtuelle, on utilise la méthode
`from_key_sheet` prenant en paramètres :

-   Les rotors : paramètre `rotors` dont la valeur doit être une chaîne
    de caractères précisant les rotors choisis et leur placement de
    gauche à droite. Chacun des cinq rotor est désigné par un chiffre
    romain de I à V. Les rotors sont séparés par des espaces ;
-   le réflecteur : paramètre `reflector` peut prendre deux valeur :
    `'B'` ou `'C'`.
-   le tableau des connextion : paramètre `plugboard_settings` dont la
    valeur doit être une chaîne de caractères décrivant les appariements
    choisis, séparés par des espaces.

``` {.py .noeval}
machine = EnigmaMachine.from_key_sheet(rotors = "III IV II",
                                       reflector = "C",
                                       plugboard_settings = "CE DV IO NR ST QX AP")
```

Avant de commencer à chiffrer, il convient de régler la position des
trois rotors en utilisant la méthode `set_display` :

``` {.py .noeval}
machine.set_display('PIX')
```

À tout moment on peut lire la position des trois rotors :

``` {.python .noeval}
>>> machine.get_display()
'PIX'
```

## Chiffrer / Déchiffrer

### Chiffrer une lettre à la fois

Pour simuler l'appui d'une touche du clavier de la machine, on utilise
la méthode `key_press` qui prend en paramètre la touche sous forme d'une
chaîne de caractères et qui renvoie la lettre qui s'illumine :

``` {.py .noeval}
machine.key_press('A')
```

À noter que chaque appui d'une touche au clavier a un effet de bord : le
rotor le plus à droite tourne d'un cran,

``` {.python .noeval}
>>> machine.get_display()
'PIY'
```

ce qui a pour effet qu'un nouvel appui sur la même touche produit une
autre lettre correspondante

``` {.python .noeval}
>>> machine.key_press('A')
'U'
```

et une nouvelle rotation du rotor le plus à droite.

``` {.python .noeval}
>>> machine.get_display()
'PIZ'
```

Si on chiffre encore une lettre,

``` {.python .noeval}
>>> machine.key_press('A')
'C'
```

alors le rotor le plus à droite a encore tourné :

``` {.python .noeval}
>>> machine.get_display()
'PIA'
```

La conception d'une machine Enigma fait que le déchiffrement se fait
exactement de la même manière que le chiffrement. Pour déchiffrer, il
suffit donc de régler la machine comme pour le chiffrement, puis de
taper le message chiffé.

``` {.python .noeval}
>>> machine.set_display('PIX')
>>> for lettre in "IUC":
...    print(machine.key_press(lettre))
A
A
A
```

### Chiffrer un message

Voici un exemple de chiffrement d'un message :

``` {.python .noeval}
>>> machine.set_display('PIX')
>>> msg_clair = "CECI EST UN TEXTE PLUS LONG"
>>> msg_chiffre = machine.process_text(msg_clair)
>>> msg_chiffre
'UIAXRWHNFXLIAYBFBMGQVQAFRLO'
```

Pour déchiffrer le message, il suffit de remettre le réglage des rotors
et de faire appel à la même méthode.

``` {.python .noeval}
>>> machine.set_display('PIX')
>>> machine.process_text(msg_chiffre)
'CECIXESTXUNXTEXTEXPLUSXLONG'
```

**Remarque :** Les espaces du message clair initial on été changées en
`X`.

# Exercices

On a chiffré ainsi un message :

``` {.python .noeval}
>>> CRYPTOGRAMMES = []
>>> machine = EnigmaMachine.from_key_sheet(rotors = "IV II I",
...                                        reflector = "B",
...                                        plugboard_settings = "CV DO IR NT SX QP AE")
>>> machine.set_display("AEC")
>>> CRYPTOGRAMMES.append(machine.process_text(CLAIRS[0]))
>>> CRYPTOGRAMMES[0]
'SOSORAFEJOEWWHRMSXVNGUMFRVADXBRROPTTDBWPHJZNLWPTAFPRSNJYBLWGVDHVAMLSGWDLEZGHGJRKRCAVZFJNFVZP'
```

### Question 1

Le fichier [cryptogrammes_enigma.py](cryptogrammes_enigma.py) contient 3
cryptogrammes obtenus avec une machine enigma. Le premier est celui
`CRYPTOGRAMMES[0]`. Déchiffrez le.

# Cryptanalyse d'Enigma

On estime que la [cryptanalyse
d'Enigma](https://fr.wikipedia.org/wiki/Cryptanalyse_d%27Enigma), menée
par les mathématiciens polonais d'abord, puis anglais a permi d'écourter
la seconde guerre mondiale de deux ans.

Pour effectuer la cryptanalyse d'Enigma, nous allons réutiliser l'indice
de coïncidence abordé lors du tp précédent.

Examinons d'abord l'indice de coïncidence d'un texte aléatoire :

``` {.py .noeval}
import sys
sys.path.append('.')

from alphabet import ALPHABETS

def nb_occurrences(txt: str) -> dict[str, int]:
    '''
    renvoie le dictionnaire des occurrences du texte `txt`
    
    précondition: aucune
    Exemples:
    
    $$$ occ =  nb_occurrences('ALPHABET')
    $$$ [occ[k] for k in 'ABC']
    [2, 1, 0]
    '''
    res = dict()
    for c in txt:
        res[c] = res.get(c, 0) + 1
    return res

def indice_coincidence(txt: str, alphabet = ALPHABETS['CAPITAL_LATIN']) -> float:
    """
    Renvoie l'indice de coincidence du texte `txt`.
    
    précondition: len (txt) >= 2 et tous les caractères de txt sont dans ALPHABET
    """
    occ = nb_occurrences(txt)
    N = 0
    somme = 0
    for c in alphabet:
        n = occ.get(c, 0)
        somme += n * (n - 1)
        N += n
    return somme / (N * (N - 1)) 
```

``` {.python .noeval}
>>> from random import choice
>>> from alphabet import ALPHABETS
>>> from cryptogrammes_enigma import CRYPTOGRAMMES
>>> alphabet = ALPHABETS['CAPITAL_LATIN']
>>> texte_aleatoire = ''.join(choice(alphabet) for _ in range(200))
>>> indice_coincidence(texte_aleatoire)
0.03688442211055276
```

``` {.python .noeval}
>>> 1/26
0.038461538461538464
```

Qu'en est-il pour les cryptogrammes ?

``` {.python .noeval}
>>> for i in range(len(CRYPTOGRAMMES)):
...     print('Cryptogramme {:d} : {:f}'.format(i,
...         indice_coincidence(CRYPTOGRAMMES[i])))
Cryptogramme 0 : 0.037868
Cryptogramme 1 : 0.038359
Cryptogramme 2 : 0.038683
```

Ces cryptogrammes ont un indice proche de celui d'un texte aléatoire.

## Exercices

### Question 2

Comparez l'indice de coïncidence du premier cryptogramme

-   avec celui du texte obtenu en le déchiffrant avec les mêmes
    paramètres que ceux permettant de déchiffrer, mais sans les
    appariements ;
-   et avec celui du texte obtenu avec les appariements.

Que constate-t-on ?

### Question 3

Chiffrez les messages de votre choix avec les méthodes de chiffrement de
César, affine, Vigenère. Comparez les indices de coïncidences des
messages clairs et des chiffrés correspondant. Quelles remarques
pouvez-vous formuler ? Expliquez !

## Recherche des rotors

L'exercice qui précéde montre que si on déchiffre un message avec les
bons réglages mais sans les appariements, le texte obtenu (encore du
charabia incompréhensible) a un indice de coincidences bien supérieur.

Et c'est justement les appariements qui rendent la recherche exhaustive
de la clé inenvisageable. Si on les néglige, le nombre de clés est plus
restreint, car il est égal à :

avec ce nombre de clés, une recherche exhaustive commence à être
envisageable.

Nous allons encore limiter l'espace des clés en envisageant que seuls
les rotors I II et III sont utilisés et que le réflecteur est B. Le
nombre de clé est alors :

``` {.python .noeval}
>>> 1 * ( 3 * 2 * 1 ) * (26**3)
105456
```

### Question 4

Complétez la fonction `decrypte_machine_enigma_sans_fiches` pour qu'elle
renvoie le réglage parmi tous les réglages possibles (les 6 permutations
des rotors I II III et les $26^3$ triplets de départ) celui qui donne
l'indice de coïncidence le plus élevé.

### Question 5

Appliquez votre fonction au deuxième cryptogramme pour ensuite le
déchiffrer.

*remarques* :

-   ce message a été chiffré sans tableau de connexion. Dans ce cas, il
    faut utiliser `plugboard_settings = ''` en paramètre pour
    `from_key_sheet`
-   Le calcul prend plusieurs minutes.

## Recherche des appariements

*partie optionnelle* : complétez la fonction `trouve_appariements` pour
que, lorsqu'elle est utilisée avec les rotors et leur position déduits
de la fonction précédente, renvoie **un** appariement : celui qui
maximise l'indice de coincidence parmi tous les appariements (en
pratique on testera les appariements de deux lettres XY dans l'alphabet,
où X est avant Y dans l'alphabet (et différent).

Utilisez cette fonction pour que, de proche en proche, on détermine les
appariements utilisés pour obtenir le message `CRYPTOGRAMMES[2]`.
