```
title: cryptographie
```

# chiffrements élémentaires 

- [chiffrement de César](./cesar1.md)
- [cryptanalyse de César](./cesar2.md)
- [chiffrement affine](./affine.md)

# chiffrement polyalphabétique

- [vigenere](./vigenere.md)
- [enigma](./enigma-etu.md)
- [rsa](./rsa.md)
