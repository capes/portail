# Alphabets

La plupart des algorithmes de chiffrement que vous aurez à programmer dans ce cours
procèdent par chiffrement de lettres (ou groupes de lettres) d'un alphabet qu'on pourra
faire varier. Si la plupart du temps l'alphabet pourra être l'alphabet latin usuel, on
pourra aussi travailler avec des alphabets autres (par exemple les chiffres décimaux ou
hexadécimaux, les lettres grecques, ...). Pour cela vous utiliserez un module nommé
`alphabet`.

- Éditez le fichier `alphabet.py`. Il sera complété tout au long le semestre.

Pour utiliser ce module dans vos scripts Python, il suffit de taper la commande

    `from alphabet import *`
    
Ce module définit une classe (un type de données) appelée `Alphabet` et
une constante nommée `ALPHABETS`.

- Dans un shell Python, tapez les commandes :

```python
from alphabet import *
ALPHABETS.keys()
latin_capital = ALPHABETS['CAPITAL_LATIN']
type(latin_capital)
print(latin_capital)
len(latin_capital)
latin_capital[0]
latin_capital[5]
latin_capital.index('E')
```

Il existe d'autres alphabets prédéfinis dans le dictionnaire `ALPHABETS`. Reprenez la
session qui précède avec l'alphabet de votre choix.


# Cryptographie

- Éditez le fichier nommé `cesar.py`.
- Complétez l'en-tête.
- Réalisez les fonctions` chiffre_lettre`, `chiffre_message` et `dechiffre_message` après
  avoir soigneusement lu les spécifications.
- Pour chaque triplet `(msg, clé, alphabet)` qui suit, procédez au chiffrement puis
  déchiffrement.

```python
msg='OUI', clé=10, alphabet = CAPITAL_LATIN
msg='Le grec propose son astuce bien attique.', clé=47, alphabet = PRINTABLE ASCII
msg='ουκελαβονπολιναλλαγαρελπιςεφηκακα', clé=14, alphabet = LOWER GREEK
```
