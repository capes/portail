---
author: B. Papegay
title: Le chiffrement RSA
---

RSA est un système de chiffrement à clé publique

# Principe du chiffrement à clé publique

Comment deux personnes peuvent-elles utiliser un système de chiffrement
symétrique pour échanger des messages confidentiels si ces deux
personnes n'ont pas de canal de communication sûr pour échanger une clé
secrète commune ?

Une réponse possible à cette question est d'utiliser un système de
chiffrement à clé publique.

Un système de chiffrement à clé publique est un système dans lequel deux
clés sont utilisées :

-   une clé **publique** pour chiffrer un message
-   et une clé **privée** pour déchiffrer le message chiffré.

Dans le cadre d'utilisation d'un système de chiffrement à clé publique,
chaque protagoniste dispose d'une paire de clés publique/privée..

Par définition la clé publique est publique, elle est donc connue de
tous. Seule la clé privée est un secret connu de son seul propriétaire.

Le fait que deux clés entrent en jeu, l'une pour chiffrer et l'autre
pour déchiffrer, conduit à qualifier les systèmes de chiffrement à clé
publique de systèmes **asymétriques** par opposition aux systèmes
classiques que l'on qualifie de systèmes **symétriques**.

Supposons donc qu'Alice souhaite envoyer un message confidentiel à Bob
mais qu'elle n'a pas convenu avec Bob d'un système de chiffrement
symétrique et a fortiori ne partage pas de clé secrète avec lui.
Supposons que Bob possède une paire de clés publique/privée d'un système
de chiffrement asymétrique. Alice se procure la clé publique de Bob (en
la lui demandant ou en consultant un annuaire de clés), chiffre son
message avec cette clé et envoie à Bob le message chiffré. Lorsqu'il
reçoit le message chiffré d'Alice, Bob déchiffre ce message avec sa clé
privée et prend connaissance du message d'Alice. Comme il est le seul à
posséder la clé privée, il est le seul à pouvoir lire le message
d'Alice.

Le concept de chiffrement à clé publique a été introduit par Diffie et
Hellman au milieu des années 1970. De très nombreux systèmes
asymétriques ont été, et sont encore, proposés. L'un des plus anciens et
des plus largement utilisé de nos jours est le système RSA, élaboré en
1977 par Rivest, Shamir et Adleman. C'est ce système que nous présentons
dans la suite.

# Génération des clés RSA

Une paire de clés publique/privée RSA se construit à partir de nombres
premiers.

La **clé publique** est un couple de deux nombres entiers $(n,e)$,
n* étant le produit de deux nombres premiers distincts $p$ et $q$, et
$e$ étant un nombre quelconque premier avec le nombre
$\varphi(n) = (p−1)(q−1)$.

La **clé privée** est un nombre *d* inverse de $e$ modulo $\varphi(n)$.

Pour construire une paire de clés RSA il faut suivre la procédure
suivante :

1.  trouver deux nombres premiers $p$ et $q$ distincts ;
2.  calculer $n =p\times q$ ;
3.  calculer $\varphi(n) = (p−1)(q−1)$ ;
4.  trouver un nombre $e$ tel que $\mathtt{pgcd}(\varphi(n),e) = 1$ ;
5.  calculer $d ≡ e^{−1}\,(mod  \varphi(n))$ ;
6.  rendre publique le couple $(n,e)$ ;
7.  conserver précieusement $d$, $p$, $q$, $\varphi(n)$.

Le nombre $n$ est appelé **modulus**, le nombre $e$ est l'**exposant de
chiffrement** et le nombre $d$ l'**exposant de déchiffrement**.

## Un petit exemple

On commence par choisir deux nombres premiers distincts à partir
desquels on peut calculer le modulus n et le nombre $\varphi(n)$.

``` python
p, q =  5, 7
n = p * q
phi = (p - 1)*(q - 1)
print(f'n = {n}, phi(n) = {phi}')
```

Ensuite il nous faut trouver un exposant de chiffrement $e$ qui soit
premier avec $\varphi(n)= 24$. Pour cela l'algorithme d'Euclide est d'un
grand secours pour calculer le pgcd. Cherchons donc quels sont les
candidats potentiels inférieurs à 10.

``` python
from arithmetique import *
e_candidats = [e for e in range(1, n) if pgcd(e, phi) == 1]
e_candidats
```

Nous avons un certain nombre de candidats. Écartons le premier (on verra pourquoi
lorsqu'on abordera le chiffrement) et prenons un exposant au hasard.

``` python
from random import choice
e  = choice(e_candidats[1:])
e
```

Calculons maintenant l'exposant de déchiffrement $d =e^{−1} (mod  \varphi(n))$. C'est
l'algorithme d'Euclide étendu qui est cette fois d'un grand secours, puisque $d$ se
calcule à partir du coefficient de Bezout de $e$.

``` python
d = inverse_mod(e, phi)
d
```

Vérification :

``` python
(e * d) % phi
```

La clé publique est le couple $(n,e) $

``` python
cle_publique = (n, e)
```

La clé privée est le nombre $d$. Elle doit être tenue secrète par son propriétaire qui ne
doit la communiquer à personne.

Les nombres premiers $p$ et $q$ ainsi que le nombre $\varphi(n)$ ne servent (en principe)
plus à rien. Ils doivent être détruits ou au moins maintenus eux aussi secrets.

## Exercices

On considère une paire de clés RSA quelconque : clé publique $(n,e)$, clé privée $d$.

1.  Montrez que ni l'exposant de chiffrement $e$, ni l'exposant de déchiffrement $d$ ne
    peuvent être des nombres pairs.
2.  Montrez qu'en connaissant n et $\varphi(n)$ il est possible de trouver les facteurs
    premiers $p$ et $q$ de n.

# Chiffrement/déchiffrement RSA

## Chiffrement

Les messages qu'on chiffre avec une clé publique RSA $(n,e)$ sont des nombres
$m\in\mathbb{Z}/n\mathbb{Z}$.  autrement dit des nombres compris entre 0 et n − 1.

Le chiffré correspondant au message *m* se calcule comme étant

$c ≡m^e (mod  n)$.

C'est pourquoi le nombre $e$ est appelé exposant de chiffrement. (c'est aussi pourquoi
dans notre exemple de construction d'une paire de clés RSA, nous avons rejeté le candidat
1 pour $e$.)

**Exemple :** Chiffrons le message *m* = 10 avec la clé publique
$(n,e) = (527,7)$.

```math
\begin{align*}
   10^7 &\equiv (10^2)^3\times 10\pmod{527}\\
        &\equiv 100^3\times 10\pmod{527}\\
        &\equiv 100^2\times 100\times 10\pmod{527}\\
        &\equiv 100^2\times 473\pmod{527}\\
        &\equiv 514\times 473\pmod{527}\\
        &\equiv 175
\end{align*}
```

Le message chiffré est donc *c* = 175. (Notons qu'il a été obtenu en
faisant 3 multiplications modulaires.)

Vérifions ce résultat avec Python :

``` python
n = 527
e = 7
m = 10
c = expo_mod_rapide(m, e, n)
c
```

## Déchiffrement

Pour déchiffrer un message, on effectue la même opération que pour le chiffrement, mais en
utilisant la clé privée $d$ à la place de l'exposant de chiffrement :
$m ≡c^d\, (mod  n)$.

**Exemple :** Déchiffrons le message *c* = 175 obtenu avec la clé
publique $(n,e) = (7,527)$. Nous devons donc utiliser la clé privée
correspondante $d = 343$.

```math
\begin{align*}
   175^{343} &\equiv (175^2)^{171}\times 175\pmod{527}\\
        &\equiv 59^{171}\times 175\pmod{527}\\
        &\equiv (59^2)^{85}\times 59\times 175\pmod{527}\\
        &\equiv 319^{85}\times 312\pmod{527}\\
        &\equiv (319^2)^{42}\times 319\times 312\pmod{527}\\
        &\equiv 50^{42} \times 452 \pmod{527} \\
        &\equiv (50^2)^{21} \times 452\pmod{527}\\
        &\equiv 392^{21} \times 452\pmod{527}\\
        &\equiv (392^2)^{10} \times 392\times 452\pmod{527}\\
        &\equiv 307^{10} \times 112\pmod{527}\\
        &\equiv (307^2)^5 \times 112\pmod{527}\\
        &\equiv 443^5 \times 112\pmod{527}\\
        &\equiv (443^2)^2 \times 443\times 112\pmod{527}\\
        &\equiv 205^2 \times 78 \pmod{527}\\
        &\equiv 392 \times 78 \pmod{527}\\
        &\equiv 10
\end{align*}
```

On retrouve donc bien le message clair *m* = 10. (Notons qu'il a été
obtenu en effectuant 14 multiplications modulaires.)

Vérifions ce résultat en Python :

``` python
d = 343
expo_mod_rapide(c, d, n)
```

## Exercices

1.  Programmez une fonction `chiffre(m, kpub)` qui renvoie le message `m` chiffré avec la
    clé publique ̀`kpub` Votre fonction devra déclencher une exception `AssertionError:
    message incorrect pour la clé` si le message `m` n'est pas dans le domaine autorisé
    par la clé publique `kpub`.

2.  Programmez une fonction `dechiffre(c, kpriv)` qui renvoie le message `c` déchiffré
    avec la clé publique `kpriv`. Votre fonction devra déclencher la même exception que la
    précédente dans les mêmes circonstances.

# Sécurité RSA

Sur quoi repose la sécurité du système RSA ? Essentiellement deux problèmes mathématiques
selon ce qu'un adversaire cherche à faire.

1.  L'adversaire peut chercher à décrypter un message *c* qui a été chiffrée avec la clé
    publique $(n,e)$, son problème consiste alors à résoudre l'équation $c ≡x^e (mod  n)$,
    où $x$ est l'inconnue. Autrement dit, il s'agit pour lui d'extraire la racine $e$-ème
    de *c* modulo n.

    Malgré les recherches de nombreux mathématiciens, aucun algorithme efficace n'est
    connu pour effectuer ce calcul.

2.  L'adversaire peut aussi chercher à calculer la clé privée $d$ correspondant à une clé
    publique $(n,e)$. S'il y parvient, il est en mesure de décrypter tous les messages
    chiffrés avec cette clé publique.

Il est prouvé que ce second problème est équivalent à celui de la factorisation du
modulus. Il est en effet clair que si l'adversaire sait factoriser n et ainsi connaître
ses facteurs premiers $p$ et $q$ alors il est en mesure de calculer $\varphi(n)$ puis
$d$. Inversement, on peut montrer que si on connaît la clé privée $d$ correspondant à une
clé publique (n,$e$) alors on peut (par une méthode probabiliste) factoriser n.

Ainsi la sécurité du système RSA est très liée à la factorisation des entiers.

## Dernier record de factorisation

Voici le dernier record de factorisation d'un modulus RSA. Il a été établi par une équipe
internationale de cinq chercheurs (dont trois français) qui a annoncé la factorisation du
nombre RSA-250 en février 2020.

Ce nombre RSA-250 fait partie d'une liste de défis de factorisation lancés à la communauté
internationale. C'est un nombre de 250 chiffres décimaux (d'où son nom) ou encore de 829
bits en binaire.

Voici ce nombre :

``` python
RSA_250 = int("2140324650240744961264423072839333563008614715144755017797754920881418023447140136643\
3455190958046796109928518724709145876873962619215573630474547705208051190564931066876\
91590019759405693457452230589325976697471681738069364894699871578494975937497937")
```

et sa factorisation en deux nombres premiers :

``` python
p_250 = int("64135289477071580278790190170577389084825014742943447208116859\
632024532344630238623598752668347708737661925585694639798853367")
q_250 = int("33372027594978156556226010605355114227940760344767554666784520\
987023841729210037080257448673296881877565718986258036932062711")
```

``` python
RSA_250 - p_250 * q_250
```

Pour en savoir plus sur les records de factorisations de modulus RSA consultez la [page
Wikipedia](https://en.wikipedia.org/wiki/RSA_Factoring_Challenge)

Vérification de la primalité de `p_250` et `q_250` avec le test fourni par le module
`sympy`.

``` python
from sympy import ntheory
ntheory.primetest.isprime(p_250)
```

``` python
ntheory.primetest.isprime(q_250)
```

## Conclusion

La sécurité RSA n'est pas connue. Néanmoins RSA est très largement utilisé aujourd'hui
(cartes bancaires, Web sécurisé, …). Aujourd'hui (avril 2017), il est recommandé
d'utiliser des clés RSA dont le modulus a une taille d'au moins ****2048 bits**** (voir
par exemple le site de l' [Agence Nationale de la Sécurité des Systèmes d'Informations
(ANSSI)](http://www.ssi.gouv.fr)

# Utilisation pratique de RSA

## Générer des nombres premiers

Comme on vient de le voir, les clés RSA doivent être d'une taille d'au moins 2048 bits. Ce
qui signifie que le modulus doit être produit de deux nombres d'au moins 1024 bits. De
tels nombres sont très grands. De plus les nombres premiers intervenant dans la
construction d'une clé ne doivent figurer dans aucune table connue.

Il va donc falloir être en mesure de

-   trouver de très grands nombres premiers pêchés au hasard ;
-   tester la (pseudo) primalité de ces nombres.

On importe une fonction de tirage de nombre au hasard et un module
contenant un test probabiliste de non primalité dû à Miller-Rabin.

``` python
from random import randrange
from math import log
```

Partons à la «pêche» aux nombres premiers de taille 2048 bits.

Pour cela, fixons la taille de la clé à 2048 bits.

``` python
t = 2048
```

Prenons un nombre impair au hasard de taille $\frac{t}{2} = 1024$ et testons s'il est
premier selon le test de Miller-Rabin. S'il ne l'est pas on lui ajoute 2 et on refait le
test. Et on recommence tant qu'on n'a pas un nombre premier.

``` python
tsur2 = t // 2
p = 2 * randrange(2**(tsur2 - 2), 2**(tsur2 - 1)) + 1
while not(ntheory.primetest.isprime):
    p += 2
print('p trouvé = {:d}'.format(p))
print('Taille de p : {:d}'.format(int(log(p)/log(2)) + 1))
```

Voilà donc une première prise : un nombre premier de la taille voulue.  Il reste à
repartir à la pêche d'un autre nombre premier de même taille et nous pourrons produire une
paire de clés RSA.

### Exercice

Réalisez une fonction `genere_cles_RSA(t)` qui renvoie un quintuplet $(n,e,d,p,q)$
constitué d'une clé RSA publique $(n,e)$, d'une clé RSA privée $d$, et des deux facteurs
premiers $p$ et $q$ de n.

## Quels messages chiffre-t-on avec RSA ?

Les opérations de chiffrement/déchiffrement RSA sont des exponentiations modulaires de
très grands nombres (quelques milliers de bits). Ce sont donc des opérations assez
coûteuses en temps de calcul. On estime qu'il existe un facteur 1000 entre la vitesse de
chiffrement/déchiffrement de RSA et celle d'un système symétrique (comme l'AES). On
n'utilise donc pas RSA pour chiffrer de grosses données.

Alors que chiffre-t-on avec RSA ? Essentiellement des clés de systèmes
symétriques.

Par exemple, supposons qu'Alice et Bob correspondent de manière confidentielle en
utilisant un système de chiffrement symétrique (donc rapide). Alice et Bob ne se sont
jamais rencontrés pour convenir de la clé à utilser avec ce système. Alice possède une
paire de clés RSA.  Voici une solution à leur problème :

1.  Alice envoie sa clé publique à Bob.
2.  Bob crée une clé *k* pour le système symétrique. Cette clé est
    constituée de quelques dizaines/centaines de bits tirés au hasard.
3.  Il chiffre la clé *k* avec la clé publique d'Alice, et envoie à
    Alice le résultat de ce chiffrement.
4.  Ayant reçu de Bob cette clé chiffrée, Alice utilise sa clé privée
    pour la déchiffrer. Elle obtient donc la clé *k*.
5.  À partir de ce moment, Alice et Bob peuvent correspondre de manière
    confidentielle en chiffrant/déchiffrant leurs messages avec la clé
    *k* qu'ils sont les seuls à connaître.

### Exercice

Bob a envoyé un message chiffré à Alice. Ce message a été chiffré par le
procédé de Vigenère. Voici le message chiffré :

``` python
msg_chiffre = ">e%r'9;/SmId|v0z3.SmL[;1m#+3`SgZ!/c~8.ObiuU/?*8&TSZW;|#}/5S`i$"
```

Il a chiffré la clé qu'il a utilisée dans le chiffrement de Vigenère avec la clé publique
RSA d'Alice. Le nombre qu'il a chiffré est construit en concaténant les nombres de la clé
de Vigenère.

``` python
cle_chiffree = 310818198187295853099334495558803865917410428245341294034432
```

Sachant que la clé privée d'Alice est

``` python
n, d = (1283445238229434564027644676770179621690769829797459507738769, 
        419341942508916830485435028512616360017008170656144801977093)
```

déchiffrez le message envoyé par Bob.

# Challenge RSA de 1977

Défi publié dans la revue Scientific American en 1977, avec 100$ de récompense :

Chiffré :

9686 9613 7546 2206 1477 1409 2225 4355 8829 0575 9991 1245 7431 9874
6951 2093 0816 2982 2514 5708 3569 3147 6622 8839 8962 8013 3919 9055
1829 9451 5781 5154

Clé publique :

N = 114381625757888867669235779976146 612010218296721242362562561842935
706935245733897830597123563958705 058989075147599290026879543541

e = 9007

``` python
N = int("114381625757888867669235779976146\
612010218296721242362562561842935\
706935245733897830597123563958705\
058989075147599290026879543541")
```

``` python
log(N, 10)
```

n est un entier de 129 chiffres. Peu d'espoir de le factoriser avec
des méthodes élémentaires.

Résolu en 1994, en factorisant n par la méthode du [crible
quadratique](https://fr.wikipedia.org/wiki/Crible_quadratique) avec une
quantité de travail de 5000 MIPS-année

``` python
p = 3490529510847650949147849619903898133417764638493387843990820577
q = 32769132993266709549961988190834461413177642967992942539798288533
print(ntheory.isprime(p))
print(ntheory.isprime(q))
print(N - p*q)
```

## Exercice

Déchiffrez le message du challenge en sachant qu'il n'est composé que de
lettres latines majuscules non accentuées et d'espaces.
