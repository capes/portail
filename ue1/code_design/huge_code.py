from random import *

def hauteur_grille(v):
    pass
def largeur_grille(v):
    pass
def nb_cellules_voisins(a,b,c):
    pass


def thon_requin(grille,ges,gestation_init_thon,gestation_init_req,energie,energie_init):
    
    cpt1=0
    cpt2=0
    
    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):

            x=randint(max(0,i-1),min(largeur_grille(grille)-1,i+1))
            y=randint(max(0,j-1),min(hauteur_grille(grille)-1,j+1))
            
            # case vide
            if grille[i][j]==0:   
                x=randint(max(0,i-1),min(largeur_grille(grille)-1,i+1))
                y=randint(max(0,j-1),min(hauteur_grille(grille)-1,j+1))
            
            # case thon
            elif grille[i][j]==2:
                cpt2 = cpt2 + 1
                
                if (x,y)==(i,j) or grille[x][y]==1:
                    if ges[i][j]==0:
                        ges[i][j]=gestation_init_thon
                    else:
                        ges[i][j]-=1
                else:
                    if grille[x][y]==0:
                        grille[x][y]=2
                        
                        if ges[i][j]==0:
                            grille[i][j]=2
                            ges[i][j]=gestation_init_thon
                        else:
                            grille[i][j]=0
                            ges[i][j]-=1
            
            # case requin
            elif grille[i][j]==1:
                cpt1 = cpt1 + 1
                energie[i][j]-=1
                
                if energie[i][j]!=0:
                    
                    if nb_cellules_voisins(grille,i,j,2)==0:
                                    
                        if (x,y)==(i,j) or grille[x][y]==1:
                            if ges[i][j]==0:
                                ges[i][j]=gestation_init_req
                                
                            else:
                                ges[i][j]-=1
                        
                        elif (x,y)!=(i,j) and grille[x][y]==0:
                            grille[x][y]=1
                            
                            if ges[i][j]==0:
                                grille[i][j]=1
                                ges[x][y]=gestation_init_req
                                ges[i][j]=gestation_init_req
                                energie[x][y]=energie[i][j]
                                energie[i][j]=energie_init
                                
                            else:
                                grille[i][j]=0
                                ges[x][y]=ges[i][j]-1
                                ges[i][j]=0
                                energie[x][y]=energie[i][j]
                                energie[i][j]=0
                    
                    else:
                        if grille[x][y]!=2:
                            x=randint(max(0,i-1),min(largeur_grille(grille)-1,i+1))
                            y=randint(max(0,j-1),min(hauteur_grille(grille)-1,j+1))
                        
                        if (x,y)!=(i,j) and grille[x][y]==2:
                            grille[x][y]=1
                            
                            if ges[i][j]==0:
                                grille[i][j]=1
                                ges[x][y]=gestation_init_req
                                ges[i][j]=gestation_init_req
                                energie[x][y]=energie_init
                                energie[i][j]=energie_init
                                
                            else:
                                grille[i][j]=0
                                ges[x][y]=ges[i][j]-1
                                ges[i][j]=0
                                energie[x][y]=energie_init
                                energie[i][j]=0
                    
                else:
                    grille[i][j]=0
                    ges[i][j]=0
                    
    return grille,ges,energie,cpt2,cpt1   


def cree_grille(largeur, hauteur):
    '''
    '''
    ligne = [ 0 for _ in range(largeur) ]
    grille = [ligne for _ in range(hauteur) ]
    return grille