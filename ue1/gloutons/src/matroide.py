import abstractmethod

class matroide:
    def __init__(self, E):
        """
        constucteur
        :param E: (set) un ensemble fini
        """
        self._E = E

    @abstractmethod
    def weight(self, e):
        """
        :param e: (any) un élément de E
        """
        pass

    @abstractmethod
    def est_independant(self, F):
        """
        :param F: un sous-ensemble de E
        :return: (boolean) True ssi F est un sous-ensemble indépendant
        """
        pass

    def glouton_matroide():
        """
        :return: (list) une solution optimale pour le problème considéré.
        """
        res = set()
        T = sorted(self._E, key = self.weight, reverse = True)
        for x in T:
            if self.est_independant( res.union( {x} ) ):
                res.add(x)
        return res

