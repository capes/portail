Ordonnancement de tâches unitaires
-----------------------------------
**matériel fourni**

le fichier [matroide.py](./src/matroide.py)

Une tâche unitaire est une tâche nécessitant l'utilisation d'une unité
de temps d'un système mono-processeur.

Dans certains systèmes, l'ordonnanceur utilise des dates
d'échéances. À chaque tâche $`i`$, on associe une date d'échéance $`d_i`$.
Si la tâche $`i`$ n'est pas exécutée avant la date $`d_i`$ alors une
pénalité $`w_i`$ doit être payée.

On cherche à ordonnancer un ensemble de $`n`$ tâches unitaires. Un
ordonnancement est une permutation de $`\lbrace 1, 2, \ldots, n\rbrace `$. 

Soit `perm` un ordonnancement. On dit qu'une tâche est **en retard**
 dans cet ordonnancement si elle se termine après sa date
 limite. Sinon, elle est **en avance**.

Plus précisément, sur l'intervalle de temps $`[d, d+1]`$, on execute la
tâche $`t = perm[d]`$. Cette tâche est en retard si $`d_t <= d`$

Pour un ordonnancement $`o`$, on note $`w(o)`$ la somme des pénalités des
tâches en retard.  On souhaite trouver un ordonnancement qui minimise
$`w(o)`$.

**Programmation** On considère une classe `Ordonnancement` héritant de
 la classe `Matroide` :

```python
from matroide import Matroide

class Ordonnancement(Matroide):

    def __init__(self, dates, weight):
        assert len(dates) == len(weight), "pas bon"
        super.__init__( range(len(dates)) )
        self._dates = dates
        self._weight = weight

```

**à faire**

Écrire une méthode `est_en_retard(t, d)` prenant en paramètre une
tache `t` et une date `d` et qui renvoie `True` ssi la tache `t`
est en retard à la date `d`.

**à faire**

Rédiger une methode `cout(o)` prenant en paramètre :
un tuple `o` représentant un ordonnancement.
et qui renvoie le coût de l'ordonnancement passé en paramètre.

**Définition** 

Un ordonnancement est sous **forme en avance d'abord**, si
 toutes les tâches en avance précèdent les tâches en retard.

**à faire** 

Écrire une methode `est_avance_dabord(o)` qui renvoie `True` si
 l'ordonnancement `o` est un ordonnancement sous forme en avance
 d'abord, et `False` sinon.

**à faire**

Démontrer que pour un ordonnancement $`o`$, il existe un
ordonnancement $`o'`$ en avance d'abord tel que $`w(o) = w(o')`$

**Définition**  

Un ordonnancement $`o`$ est sous **forme canonique** si :

-   $`o`$ est en avance d'abord ;
-   les tâches en avance de $`o`$ sont triées par ordre croissant de
    date d'échéance.

**à faire** 

Démontrer que pour un ordonnancement $`o`$, il existe un
ordonnancement $`o'`$ sous forme canonique tel que $`w(o) = w(o')`$.  On
dit alors que $`o'`$ est une forme canonique de $`o`$.

**à faire**

Écrire une méthode `forme_canonique(o)` prenant en paramètre un
ordonnancement `o` et qui renvoie une forme canonique de `o`.

La recherche d'un ordonnancement optimal se ramène donc à la recherche
d'un ensemble $`F`$ de tâches en avance dans l'ordonnancement dont la
somme des couts est maximale.

**définition**

On dit qu'un ensemble $`F`$ de tâches est **indépendant** s'il existe un
ordonnancement de ces tâches tel qu'aucune de ces tâches soit en
retard. 

**à faire**

Écrire une méthode `est_independant(F)` de la classe `Ordonnancement` 
renvoyant `True` si $`F`$ est un ensemble de tâches indépendant, `False` sinon.

**Théorème** 

Si $`E`$ est un ensemble de tâches de durée unitaire et avec des dates
 d'échéance, et si $`\mathcal I`$ est l'ensemble des
 ensembles de tâches indépendants, alors $`(E, \mathcal I)`$ est un
 matroïde.

**à faire**

Démontrer le théorème

**à faire**

Utiliser l'algorithme glouton de la classe `Matroide` pour résoudre le
problème de l'ordonnancement.

