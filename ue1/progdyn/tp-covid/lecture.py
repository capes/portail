"""Lecture des lignes d'un fichier texte."""


def lecture_fichier(nom_fichier: str) -> list[str]:
    """Lecture d'un fichier texte."""
    with open(nom_fichier, "r") as filin:
        _ = filin.readline()
        lignes = [s.rstrip() for s in filin.readlines()]
    return lignes
