"""Quelques elements de gestion de tableau de tableau pour gérer la matrice."""


def affiche_matrice(m: list[list]):
    """Affiche la matrice m."""
    for i in range(0, len(m)):
        for j in range(0, len(m[i])):
            print(m[i][j], ' ', end='')
        print('\n')


def initialise_matrice(ligne: int,
                       colonne: int,
                       valeur) -> list[list]:
    """initialise la matrice."""
    matrice = [[valeur for _ in range(colonne)]
               for _ in range(ligne)]
    return matrice
