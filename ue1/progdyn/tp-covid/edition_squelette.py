"""Implantation de la distance d'édition."""


from lecture import lecture_fichier
from matrice import initialise_matrice, affiche_matrice


def valeur(E: list[list], i: int, j: int) -> int:
    """
    Renvoie la 'valeur' relative dans la matrice.

    >>> A=[[1,2,3],[2,3,4]]
    >>> valeur(A,1,1)
    3
    >>> valeur(A,-1,1)
    2
    """
    ...


def MdistanceEdition(s1: int, s2: int, matrice: list[list]):
    """Remplit la matrice d'édition."""
    ...


def distanceEdition(s1: str, s2: str, matrice: list[list]) -> int:
    """
    Renvoie la distance d'édition entre deux mots.

    >>> s4 = 'AGORRYTNE'
    >>> s3 = 'ALGORITHME'
    >>> m2 = initialise_matrice(len(s3), len(s4), 0)
    >>> distanceEdition(s3, s4, m2)
    5
    """
    return MdistanceEdition(s1, s2, matrice)[len(s1) - 1][len(s2) - 1]


def alignement(s1, s2, matrice):
    """


    """
    i = len(s1)
    j = len(s2)
    s1b = s1
    s2b = s2
    ...
    return [s1b, s2b, aligne]


def covid(nom_fichier_humain, nom_fichier_animal):
    animal = lecture_fichier( nom_fichier_animal )
    humain = lecture_fichier( nom_fichier_humain )
    a = len(animal)
    h = len(humain)
    ...


def test_covid():
    animal="covid19/bat-SL-CoVZC45.fa"
    humain="covid19/2019-nCoV_WH01.fa"
    covid(humain,animal)


def test_fonctions():
    s4 = 'AGORRYTNE'
    s3 = 'ALGORITHME'
    m2 = initialise_matrice(len(s3), len(s4), 0)

    d = distanceEdition(s3, s4, m2)
    print("La distance d'edition entre", s3, "et ", s4, "est de ", d)
    m3 = MdistanceEdition(s3, s4, m2)
    affiche_matrice(m3)
    aligne = alignement(s3, s4, m3)
    print("alignement de ", s3,"et ", s4, ": \n",
          aligne[0],"\n", aligne[2],"\n", aligne[1])
    sequence1, sequence2 = "aagtagccactag", "aagtaagct"
    m4 = initialise_matrice(len(sequence1), len(sequence2),0)
    d2 = distanceEdition(sequence1, sequence2, m4)
    print("La distance d'edition entre", sequence1, "et ", sequence2, "est de ", d)
    m5 = MdistanceEdition(sequence1, sequence2, m4)
    # affiche_matrice(m5)
    aligne = alignement(sequence1, sequence2, m5)
    print("alignement de ", sequence1, "et ", sequence2,
          ": \n", aligne[0], "\n", aligne[2], "\n", aligne[1], )


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
