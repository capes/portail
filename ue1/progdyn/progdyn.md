# Définition

La programmation dynamique est, en général, appliquée aux problèmes d'optimisation. Dans ce type de problème, la recherche exhaustive fonctionne toujours, mais impossible à mettre en oeuvre (exponentielle)


## exemples

-   rendu de monnaie ;
-   sac à dos
-   distance d'édition


# Exemple : ordonnancement sur une chaîne de montage


## présentation

Une usine automobile dispose de deux chaines de montage : 1 et 2 composées des mêmes ateliers, réalisant les mêmes tâches mais pas dans le même temps.

Une instance du problème est la donnée de quatre listes :

-   le tableau $`d`$ de temps de transition;
-   le tableau $`a`$ des temps d'assemblage.


## exemples

| $`i`$       | 1 | 2 | 3 | 4 | 5 | 6 |   |
|----------- |--- |--- |--- |--- |--- |--- |--- |
| $`d(i, 1)`$ | 2 | 2 | 3 | 1 | 3 | 4 | 3 |
| $`d(i, 2)`$ | 4 | 2 | 1 | 2 | 2 | 1 | 2 |
| $`a(i, 1)`$ | 7 | 9 | 3 | 4 | 8 | 4 |   |
| $`a(i, 2)`$ | 8 | 5 | 6 | 4 | 5 | 7 |   |


## problèmes

Problème : déterminer

-   une valeur : on cherche à minimiser la durée totale de fabrication de la voiture ;
-   un chemin : trouver un chemin qui réalise ce minimum.


## Approche exhaustive

Combien de chemins possibles (avec n tâches) ?

2<sup>6</sup> ici &#x2026; 2<sup>n</sup> en toute généralité

Impossible à utiliser dès que n est grand.


## Approche par programmation dynamique


### Caractériser la structure d'une solution optimale ;

à chaque étape, on note :

$`mst_{i, j}`$ le minimum de la somme des temps pour aller de l'étape initiale à l'atelier $`i`$ de la chaîne $`j`$ et effectuer la tâche.


### caractérisation 2

par exemple, $`mst_{i, 1}`$ est le temps minimum pour aller de l'étape initiale à l'atelier $`i`$ de la châine 1. Deux possibilités existent :

-   La voiture provient de l'atelier $`i-1`$ de la chaîne 1. Elle a parcouru un chemin optimal du point de départ à cet atelier, puis est restée sur la chaîne 1 ;
-   La voiture provient de l'atelier $`i-1`$ de la chaîne 2. Elle a parcouru un chemin optimal du point de départ à cet atelier, puis est passée de la chaîne 2 à la chaîne 1.


### caractérisation 3

Nous sommes conduit à déterminer deux sous-problèmes optimaux (deux sous-structures optimales)


### Définir récursivement la valeur d'une solution optimale (descendante) ;

Pour $`i \geqslant 2`$, $$`\left\lbrace\begin{array}{rcl} mst(i, 1) & = & {\rm min}( mst(i-1, 2) + d(i, 1), mst(i-1, 1))+a(i, 1) \\ mst(i, 2) & = & {\rm min}( mst(i-1, 1) + d(i, 2), mst(i-1, 2))+a(i, 2) \end{array}\right.`$$

Pour $`i = 1`$

$$`\left\lbrace\begin{array}{rcl} mst(1, 1) &=& d(1, 1) + a(1, 1)\\ mst(1, 2) &=& d(1, 2) + a(1, 2) \end{array}\right. `$$


### compléxité

-   Complexité O(2<sup>n</sup>) en raison des chevauchements des sous-problèmes.
-   Améliorations :
    -   Utiliser un tableau pour mémoriser les valeurs (top-bottom);
    -   Utiliser une approche ascendante, dérécursiver (bottom-top).


### Calculer la valeur d'une solution optimale de manière ascendente ;

-   Bottom-TOP
-   cela revient à dérécursiver l'algorithme.
-   remarque : les solutions optimale à l'étape $`n`$ ne dépendent que des solutions optimales à l'étape $`n-1`$


### Construire une solution optimale à partir des valeurs optimales calculées.

-   Soit partir de la solution optimale (tableau des temps optimum) et on "remonte" les solutions : à chaque étape, on détermine le choix effectué.
-   Soit directement dans l'algorithme : on enregistre directement les choix effectués.

```python
liste_temps = [ [ 2 , 2 , 3 , 1 , 3 , 4 , 3 ],
                [ 4 , 2 , 1 , 2 , 2 , 1 , 2 ],
                [ 7 , 9 , 3 , 4 , 8 , 4 ],
                [ 8 , 5 , 6 , 4 , 5 , 7 ] ]

def mst_rec(i, j, data, tab = None):
    """
    :param i: (int) le numéro de l'atelier
    :param j: (int) la chaine de montage
    :param data: (list) les temps
    :return: le temps minimal d'assemblage
    """
    if tab == None:
        tab = {}
    elif (i, j) in tab:
        return tab[(i, j)]
    if i > 0:
        if j == 1:
            # si valeur est déjà calculée dans tab, renvoyer la valeur
            res =  min(mst_rec(i - 1, 2, data) + data[0][i] + data[2][i],
                       mst_rec(i - 1, 1, data) + data[2][i])
        elif j == 2:
            res =  min(mst_rec(i - 1, 1, data) + data[1][i] + data[3][i],
                       mst_rec(i - 1, 2, data) + data[3][i])
    else:
        if j == 1:
            res = data[0][i] + data[2][i]
        else:
            res = data[1][i] + data[3][i]

    tab[(i, j)] = res
    return res

def mst_it(i, j, data):
    """
    :param i: (int) le numéro de l'atelier
    :param j: (int) la chaine de montage
    :param data: (list) les temps
    :return: le temps minimal d'assemblage

    :CU: i >= 0 and j in {1, 2}
    """
    tab = [ [ data[0][0] + data[2][0] ],
            [ data[1][0] + data[3][0] ] ]
    for l in range(1, i+1):
        a =  min(tab[1][l - 1] + data[0][l] + data[2][l],
                 tab[0][l - 1] + data[2][l])
        b =  min(tab[0][l - 1] + data[1][l] + data[3][l],
                 tab[1][l - 1] + data[3][l])
        tab[0].append(a)
        tab[1].append(b)

    return tab[j-1][i]

```


# Eléments de la programmation dynamique

Quand peut-on envisager une solution à base de programmation dynamique ?


## Problème d'optimisation

Le cadre général est celui de l'optimisation combinatoire


## Sous-structure optimale

La programmation dynamique est envisageable lorsque que l'on peut construire une solution optimale en utilisant des solutions optimales des sous-problèmes.


## Chevauchement des sous-problèmes

Une deuxième caractéristique de la résolution par programmation dynamique et le chevauchement des sous problèmes.

Cette caractéristique induit un cout algorithmique en général exponentiel.

Pour réduire la complexité, on utilise :

-   la memoization : les valeurs des sous-problèmes optimaux sont stockées ;
-   bottom-top : on construit par longueur de problème croissante.


## Méthode

les étapes de l'élaboration d'un algorithme de programmation dynamique :

1.  Déterminer la structure d'une solution optimale ;
2.  Définir récursivement la valeur d'une solution optimale (descendante) ;
3.  Calculer la valeur d'une solution optimale de manière ascendente ;
4.  Construire une solution otimale à partir des valeurs optimales calculées.


## Reconstruction d'une solution optimale

En général, on stocke soit directement le choix effectué, soit la valeur de la solution optimale.

Dans le second cas, on peut retrouver, à l'aide des valeurs optimales, les choix à effectuer.


# Exemples de problèmes


## sous-séquence maximale commune de chaînes d'adn

Déterminer une sous séquence maximale communes d'une liste de chaînes d'adn.


## multiplication de n matrices

Comment organiser le produit de $`n`$ matrices de manière à minimiser le nombre de multlications.


## distance d'édition

distance de levenshtein

La distance de Levenshtein entre deux mots $`u`$ et $`v`$ (de longueurs potentiellement distinctes), notée $`\delta_L(u,v)`$, est le nombre minimal de caractères qu'il faut supprimer, insérer ou modifier pour passer de $`u`$ à $`v`$.


## calcul des coutures d'une image

[voir le tp](./Readme.md)


# Différences

1.  programmation dynamique : les sous-problèmes se chevauchent

2.  diviser pour régner : les sous-problèmes ont une intersection vide

3.  gloutons : la résolution d'un problème nécessite la résolution d'un seul sous-problème.