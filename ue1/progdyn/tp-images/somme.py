#!/usr/bin/env python
# coding: utf-8


tab = [ [5], [8, 10 ], [11, 3, 4], [6, 10, 7, 12 ]]


def solution_naive(t, l, r):
    """
    :param t: (list) array of numbers
    :param l: (int) line
    :param r: (int) row
    :return: (int) best cumulative sum
    
    :Example:
    >>> solution_naive([[1], [2, 3]], 0, 0)
    4
    """
    pass

def solution_dynamique(tab):
    """
    :param t: (list) array of numbers
    :param l: (int) line
    :param r: (int) row
    :return: (int) best cumulative sum
    
    :Example:
    >>> solution_dynamique([[1], [2, 3]])
    4
    """
    pass

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
