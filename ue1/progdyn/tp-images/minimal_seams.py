#!/usr/bin/env python
# coding: utf-8


from PIL import Image
import random


def delta_energy(p1, p2):
    """
    :param p1: (tuple) first pixel
    :param p2: (tuple) second pixel
    :return: (int) sum of square of difference by composante

    :Example:
    >>> delta_energy((1, 2, 3), (2, 3, 4))
    3
    """
    pass


def neighbors(coord, dim):
    """
    :param coord: (tuple) les coordonnées d'un pixel
    :param dim: (tuple) dimensions de l'image
    :return: (tuple) neighbors of coord
    :examples:
    >>> neighbors((0,0),(10,10))
    ((0, 0), (1, 0), (0, 0), (0, 1))
    >>> neighbors((10,5), (10,10))
    ((9, 5), (10, 5), (10, 4), (10, 6))
    >>> neighbors((4, 4), (10,10))
    ((3, 4), (5, 4), (4, 3), (4, 5))
    """
    pass

def image_energy(im):
    """
    :param im: (Image) an RGB image
    :return: (list of list) array of image's energy
    :CU: None
    """
    pass

def seams_energy(en):
    """
    :param en: (list of list) the array of pixel energy
    :return: (list of list) the array of energy of seams of minimum energy
    :CU: None
    """
    pass

def minimal_seam(min_energy, energy):
    """
    :param min_energy: (list of list) array of energy of minimal energy seams
    :param energy: (list of list) array of pixels energy
    :return: (list) list of coordinates of the seam
    """
    pass

def remove_from_width(im, n):
    """
    :param im: (Image) an image 
    :param n: (int) number of pixels to remove from width
    :return: (Image) the resized image
    """
    pass

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
    
