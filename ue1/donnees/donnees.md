# Les données

# Généralités


## Les données en NSI

-   en première : utilisation de tables dans des fichiers
    textuels, csv , utilisation des modules csv et pandas ;
-   en terminale : requete SQL sur une base :
    -   requetes simple (where)
    -   jointures (inner join on)
-   recul niveau master :
    -   algèbre relationnelle
    -   conception de bdd :
        -   normalisation : 1NF, 2NF, 3NF.
    -   aspect système (missions du sgbd, stockage, indiçage)


## Stockage perenne des données

On cherche à enregistrer des données. Si on utilise
des fichiers, on est confronté aux problèmes :

-   format ?
-   redondance ?
-   cohérence
-   expressivité des requêtes , optimisation
-   sécurisation et partage des données.


## Système relationnel


### Objectifs

L'objectif d'un système de gestion de données est la *persistance* des
données.

Elle est facilitée par l'indépendance entre le niveau logique et physique.

(l'utilisateur final a une vue abstraite des données, qu'il manipule à
travers une interface)


### Vue d'ensemble

![img](img/ensemble.png)


## Système de Gestion de Bases de Données

Les SGBD  = système logiciel assurant :

-   *Stockage* (sur des supports persistants) ;
-   *Indexation* : structures de données permettant des accès très
    rapides ;
-   *Interrogation et mise à jour* (traduction des requêtes en
    algorithmes efficaces) ;
-   *Accès Concurrents* ;
-   *Sécurité* :
    -   contrôle des accès ;
    
    -   assure l'*intégrité* des données ;
    
    -   tolérance aux pannes logicielles et physiques.


# Bases de données relationnelles


## Présentation

Une *base de données* est un ensemble de tables

Une *table* est un ensemble de données ayant les mêmes
attributs (représentable sous la forme d'un tableau
à deux dimensions).


## Vocabulaire

-   *Attribut* : couple (Nom,Domaine) qui permet de définir
    une colonne
-   *Domaine* : Type (Ensemble des valeurs possibles) des
    données.
-   *Tuple* : Ensemble ordonné de couples attribut/valeur
    (~ ligne)
-   *Relation* : Ensemble de tuple (~ table en SQL)


## Notion de clé

Une *clé de relation* est un attribut ou un ensemble d'attributs qui
permet d'associer à chaque clé, un unique tuple.

En général, une clé est choisie par relation. On l'appelle la *clé
primaire*.


## schéma


### Définition

Un *schéma* est

-   un ensemble ordonné d'attributs ;
-   auquel on associe des contraintes.
    
    ex : `(Nom : String , Age : int, Adresse : String)`


### contraintes

les contraintes sont de différentes natures :

-   condition booléennes ,
-   unicité ;
-   contraintes de clé primaire et étrangère.


## Un exemple

Nous allons nous appuyer sur une base de données utilisée pour
enregistrer des informations sur des films.

Elle contient les tables :

-   Film (idFilm, titre, année, genre, résumé, idRéalisateur, codePays) ;
-   Pays (code, nom, langue) ;
-   Artiste (idArtiste, nom, prénom, annéeNaiss) :
-   Rôle (idFilm, idActeur, nomRôle) ;
-   Internaute (email, nom, prénom, région) ;
-   Notation (email, idFilm, note) .


# Les langages


## généralités

Le langage permet d'interragir avec les données par l'intermédiaire du
sgbd.

Le sgbd prend en entrée une ou plusieurs requêtes, agissant sur une
table, et renvoie en sortie une table.


## deux paradigmes

En matière de sgbd, on distingue deux langages :

1.  un langage *déclaratif*, basé sur la logique ;
2.  un langage *procédural*, basé sur la théorie des ensembles.


## Déclaratif / procédural

un langage déclaratif permet de spécifier les résultats que l'on
souhaite obtenir, sans décrire les opérations permettant d'aboutir au
résultat.

un langage procédural permet d'écrire un ensemble d'opérations
permettant de fournir le résultat.


## SQL

SQL est un langage déclaratif, utilisé depuis les années 1970. Une
requête formulée en SQL spécifie les propriétés que doivent avoir les
résultats.

On distingue les éléments du langage permettant :

-   La définition (de la structure) des données (DDL) ;
-   La manipulation des données (DML) ;
-   L'interrogation des données.


# Syntaxe SQL

La première standardisation de SQL date de 1987 (ISO 9075-1987). La
dernière de 2016 (ISO/IEC 9075).

Certains acteurs reconnaissent ce standard, mais l'adaptent également
à leur besoin, aboutissant à des différences de syntaxe.

On se basera sur la grammaire fournie par sqlite :
<https://www.sqlite.com/lang.html>


## create

![img](./img/create.svg)


## Spécification des contraintes

![img](img/constraint.svg)


## contrainte de clé étrangère

![img](img/fk_constraint.svg)


## drop

![img](./img/drop.svg)


## alter

![img](./img/alter.svg)


## Insert

![img](./img/insert.svg)


## update

![img](./img/update.svg)


## delete

![img](./img/delete.svg )


# Opérateurs de l'algèbre relationnelle


## Traduction

Comment le SGBD convertit-il la requête en un algorithme de calcul ?

En fait, il utilise un langage intermédiaire : celui de l'algèbre
relationnelle. Cette algèbre est munie d'un certain nombre
d'opérateurs :

-   prenant en entrée une ou plusieurs tables ;
-   produisant en sortie une table.


## Projection


### Définition

On ne conserve que certaines colonnes d'une relation.

La projection d'une relation `table` sur l'ensemble `colonnes` est
notée $\pi_{\rm colonnes}(table)$


### Procédure

    def projection(relation: list[dict], attributs: list)-> list[dict]:
        """
        :param table:  une liste de dictionnaires
        :param colonnes: une liste d'attributs
        :CU: all( all(att in t for att in colonnes) for t in table )
        """
        res = []
        for t in relation:
            new_tuple = {}
            for att in attributs:
                if type(att) == str:
                    new_tuple[att] = t[att]
                elif type(att) == function:
                    new_tuple[] = att[t]
            res.append(new_tuple)
        return res


### En sql

on utilise `SELECT` et  `FROM`

    SELECT attr_1, ..., attr_n FROM table;  


## Sélection


### Définition

-   Seule une partie des lignes m'intéressent.
    On veut pouvoir sélectionner les lignes vérifiant
    une certaine condition.
-   Notation dans l'algèbre relationnelle :
    $$ \sigma_{\rm condition}(table) $$
    = ensemble des tuples $t$ tels condition(t) est vraie


### Procédure

    def selectionne(table: list[dict], cond: function) -> list[dict]:
        """
        :param table: une relation sous la forme d'une liste de dictionnaires
        :param condition: la condition à vérifier pour être dans le résultat.
        :type condition: dict -> bool
        :return: les tuple de la relation qui satisfont la condition
        """
        res = []
        for t in table:
            if cond(t):
                res.append(t)
        return res


### En SQL

    SELECT * FROM table WHERE condition;    


## Exemples

Opérateurs possibles dans la grammaire des contraintes:

-   appartenance à un intervalle : x BETWEEN a AND b
-   test de vacuité : IS NULL
-   appartenance à une autre relation/ensemble : IN


## Renommage


### Définition

-   Renommer des colonnes
-   **Notation** $$ \rho_{A\to B}(table) $$
-   **Exemple** Renommer la quantité population/area en
    densite :
    $$ \rho_{population/area\to densite} $$


### Procédure

    def renomme(table: list[dict], att_i: str, att_f: str)->list[dict]:
        """
        :param table: une relation sous la forme d'une liste de dictionnaires
        :param att_i: (str) un attribut de la table initiale
        :param att_f: (str) l'attribut dans la table finale
        :return: une table renommée
        """
        return [{(att_f if att==att_i else att):t[att]
                 for att in t} for t in table]


### SQL

mot-clé AS

    select * from Film as f
    where f.Titre = 'Memento';


## Union


### définition

$A\cup B$

-   Tous les tuples qui appartiennent à A ou à B
-   A et B doivent avoir le même schéma


### procédure

    def union(table_a: list[dict], table_b: list[dict])->list[dict]:
        """
        :param table_a: une relation
        :param table_b: une autre relation
        :return: la relation AUB
        :CU: les éléments de table_a et table_b ont les même clés
        """
        return table_a + table_b


### SQL

`UNION`

    SELECT ... FROM A WHERE ... UNION SELECT ... FROM
    B WHERE ...


## Intersection


### définition

$A\cap B$

-   Tous les tuples qui appartiennent à A et à B
-   A et B doivent avoir le même schéma


### procédure

    def intersection(table_a: list[dict], table_b: list[dict])->list[dict]:
        """
        :param table_a: une relation
        :param table_b: une relation
        :CU: les tables ont le même schémas
        :return: l'ensemble des tuple appartenant à A et à B 
        """
        return [t for t in table_a if t in table_b]


### SQL

`INTERSECT`

    SELECT ... FROM A WHERE ... INTERSECT SELECT ... FROM
    B WHERE ...


## Différence


### définition

Il s'agit des tuples appartenant à $A$ ou à $B$, mais pas au deux.


### procédure

    def difference(table_a: list[dict], table_b: list[dict])->list[dict]:
        """
        :param table_a: une relation
        :param table_b: une relation
        :return: l'ensemble des tuples appartenant à A ou B  mais pas au deux 
        """
        return ([t for t in table_a if t not in table_b] +
                [t for t in table_b if t not in table_a])


### SQL

    SELECT ... FROM A WHERE EXCEPT SELECT ... FROM B WHERE


## Produit cartésien


### définition

$A\times B$

-   ensemble de tous les tuples possibles qui se décomposent en un tuple
    de A concaténé avec un tuple de B
-   Si $\vert A\vert = n$ et $\vert B\vert = p$, alors $\vert A\times
      B\vert=$ n.p (règle du produit)


### procédure

    def produit(A: list[dict], B: list[dict])->list[dict]:
        """
        :param A: une relation
        :param B: une relation
        :return: les tuples de A concaténés avec ceux de B
        """
        res = []
        for ta in A:
            for tb in B:
                t = {}
                t.update(ta)
                t.update(tb)
                res.append(t)
        return res


### SQL

on spécifie les tables du produit en les séparant par des virgules

    SELECT * from Rôle, Artiste;

**peu d'intérêt**:  pas de mise en relation. Une sélection est nécessaire


## Jointure


### définition

La **jointure** est une opération qui porte sur deux relations A et
B. C'est la composé du produit cartésien de A et de B, puis une
sélection.

On la note $A \bowtie_c B$

**exemple** $Artiste \bowtie_{\rm idArtiste = idActeur} Rôle$


### Procédure

    def jointure(A: list[dict], B: list[dict],
                 cond: function)-> list[dict]:
        """
        :param A: une relation
        :param B: une relation
        :param cond: une fonction du type Dict -> bool
        :return: la liste des tuples du produit cartésien vérifiant la condition
        """
        return selectionne(produit(A, B), cond)


### en SQL

    A JOIN B ON c;


## Fonction d'agrégat


### Intérêt

Les fonctions d'agrégation :

-   spécifient comment sont regroupées les tuples : on regroupe les
    tuples ayant les mêmes valeurs d'attributs pour un certain ensemble
    d'attributs
-   applique une fonction sur cet ensemble d'attribut


### Définition

Ces fonctions ne peuvent être représentées par les opérateurs de
l'algèbre relationnelle : Il faut étendre cette algèbre et définir un
nouvel opérateur :

$${}_{a_1, \ldots , a_n}\gamma_{F(a_1), \ldots, F(a_n)}$$

Ces fonctions agissent sur un ensemble de
lignes : count(),max(),min(),sum(),avg()


### exemple

    select idArtiste, nom, prénom, count(titre) as nbFilms
    from   Artiste join Film on Film.idRéalisateur = Artiste.idArtiste
    group by  idArtiste


### quelques fonctions

    from functools import cmp_to_key
    
    def count(table: list[dict])->int:
        """
        :param table: une relation
        :return: le nombre de tuples de la relation
        """
        return len(table)
    
    def max(table: list[dict], att: str)->float:
        """
        :param table: une relation
        :param att: un attribut
        :return: la valeur maximal de l'attribut
        """
        return max(t[att] for t in table)
    
    def min(table: list[dict], att: str)->float:
        """
        :param table: une relation
        :param att: un attribut
        :return: la valeur minimal de l'attribut
        """
        return min(t[att] for t in table)
    
    def sum(table: list[dict], att: str)->float:
        """
        :param table: une relation
        :param att: un attribut
        :return: la somme des attributs
        """
        return sum(t[att] for t in table)
    
    def avg(table: list[dict], att: str)->float:
        """
        :param table: une relation
        :param att: un attribut
        :return: la valeur moyenne des attributs des tuples
        """
        S, n = 0, 0
        for t in table:
            S = S + t[att]
            n = n + 1
        return S / n


### agregation

    from functools import cmp_to_key
    
    def ordonne_attribut(t, l_att):
        return tuple(t[att] for att in l_att)
    
    def aggregate(table: list[dict], l_att: list['str'],
                  aggregate_function: 'function',
                  att : 'str') -> list[dict]:
        # on trie la table selon l'ordre lexicographique sur les attributs
        # puis on applique la fonction correspondante sur les tuples
        # ayant les même valeurs d'attribut.
        sorted_table = sorted(table, key = lambda d: ordonne_attribut(d, l_att))
        # on parcourt la table triée
        groupe = []
        res = []
        for i in range(len(sorted_table)):
            if groupe == [] or sorted_table[i] == groupe[0]:
                groupe.append(sorted_table[i])
            else:
                # on applique la fonction d'aggregat à groupe
                res_f = aggregate_func(groupe, att)
                # on ne conserve que les attributs égaux
                t = { att: groupe[0][att] for att in l_att }
                t['func'] = res_f
                res.append(t)
                groupe = []
        return res


### Filtrer les résultats

Il est possible de filtrer les résultats en ajoutant une condition sur
le résultat de la fonction d'aggrégation.

Par exemple :

    select  nom, prénom
    from   Artiste join Film
    on Film.idRéalisateur = Artiste.idArtiste
    group by nom, prénom
    having count(*) > 1


## Syntaxe du SELECT

![img](./img/select.svg)


# Normalisation d'une relation


## Redondance

Le *schéma* d'une base données est rarement constitué d'un seul schéma
de relation.

Voici un premier schéma d'une relation pour une base de données gérant des films :

`Film(titre, année, prénomRéalisateur, nomRéalisateur, annéeNaiss)`

Le défaut majeur de ce schéma est que la même information va être
enregistrée plusieurs fois : il y a *redondance*.


## Anomalies

Lorsqu'il y a redondance, cela peut entrainer des anomalies :

-   d'**insertion** : un même film peut être enregistré plusieurs fois,
    avec des réalisateurs potentiellement différents ;
-   de **modification** : la modification de la date de naissance d'un
    réalisateur peut conduire a des incohérences ;
-   de **destruction** : la suppression d'un film entraîne la suppression
    de son réalisateur.


## Dépendance fonctionnelle

Comment créer un bon schéma relationnel ? La notion centrale est celle
de dépendance fonctionnelle.

Soit un schéma de relation $R$, $S$ un sous-ensemble d'attributs de
$R$ et $A$ un attribut de $R$.  On dit que $A$ *dépend
fonctionnellement* de $S$ (et on note $S\rightarrow A$) lorsque pour
tous couples $t_1, t_2$ de la relation, l'égalité des attributs sur
$S$ implique l'égalité des attributs sur $A$.

Si `all(t1[att] == t2[att] for att in S)`, alors `t1[A] == t2[A]`.


### Exemple

-   Pour un schéma `(nom, prénom, nip, dateNaissance, adresse, email)`,
    identifier les dépendances fonctionnelles "attendues".
-   Pour le schéma `(nip, codeUE, semestre, note, intituléUE)`


### Exemple

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">A1</th>
<th scope="col" class="org-right">A2</th>
<th scope="col" class="org-right">A3</th>
<th scope="col" class="org-right">A4</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-right">2</td>
<td class="org-right">3</td>
<td class="org-right">4</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">2</td>
<td class="org-right">3</td>
<td class="org-right">5</td>
</tr>


<tr>
<td class="org-right">6</td>
<td class="org-right">7</td>
<td class="org-right">8</td>
<td class="org-right">2</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-right">1</td>
<td class="org-right">3</td>
<td class="org-right">4</td>
</tr>
</tbody>
</table>

Identifier les dépendances fonctionnelles


## Axiomes d'Amstrong

Il faut connaitre (cf sujet 2019) les axiomes d'Amstrong :

-   Réflexivité : Si $A\subset X$, alors $X\rightarrow A$.
-   Augmentation : Si $X\rightarrow Y$ , alors pour tout $Z$,
    $XZ\rightarrow Y$.
-   Transitivité : Si $X\rightarrow Y$ et si $Y\rightarrow Z$ , alors
    $X\rightarrow Z$.


## Exemple

Démontrer que `(nip, codeUE, semestre)` $\rightarrow$ `note, intituleUE`


## DF minimales et directes

Une dépendance fonctionnelle $A\rightarrow X$ est *minimale* s'il
n'existe pas d'ensemble d'attributs $B\subset A$ tel que $B\rightarrow
X$.

Une dépendance fonctionnelle $A\rightarrow X$ est *directe* si elle
n'est pas obtenue par transitivité.


## Exemple


### Des copies

-   Schéma 1

`Copie(idCopie, eleve, idCorrecteur, nom, note )`

-   Schéma 2

`Copie(idCopie, eleve, idCorrecteur, note )`
`Correcteur(idCorrecteur, nom)`

Et on donne les DF minimales et directes suivantes :

-   idCopie -> eleve, idCorrecteur, note
-   idCorrecteur -> nom


### Analyse

Ces dépendances nous donnent un moyen de caractériser les redondances
et incohérences potentielles :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">idCopie</th>
<th scope="col" class="org-left">Eleve</th>
<th scope="col" class="org-right">idCorrecteur</th>
<th scope="col" class="org-left">nom</th>
<th scope="col" class="org-right">note</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">10</td>
<td class="org-left">Philippe</td>
<td class="org-right">2</td>
<td class="org-left">Benoit</td>
<td class="org-right">16</td>
</tr>


<tr>
<td class="org-right">20</td>
<td class="org-left">Benoit</td>
<td class="org-right">3</td>
<td class="org-left">Marie</td>
<td class="org-right">10</td>
</tr>


<tr>
<td class="org-right">30</td>
<td class="org-left">Philippe</td>
<td class="org-right">2</td>
<td class="org-left">Benoit</td>
<td class="org-right">18</td>
</tr>


<tr>
<td class="org-right">10</td>
<td class="org-left">Francesco</td>
<td class="org-right">1</td>
<td class="org-left">Marie</td>
<td class="org-right">17</td>
</tr>
</tbody>
</table>

&#x2026; Que dire ?


### Clés

La notion de clé permet de décomposer les schémas.

Une clé d'une relation $R$ est un sous-ensemble minimal $C$
d'attributs tel que tout autre attribut dépend fonctionnellement de $C$.

&#x2026; Exemple


### 3NF

Un schéma de relation $R$ est normalisé quand, dans toute dépendance
fonctionnelle $S\rightarrow A$ sur les attributs de $R$, $S$ est une clé.

Exemple :

-   le schéma 1 n'est pas normalisé : il y a dépendance `idCorrecteur`
    $\rightarrow$ `nom` alors que `idCorrecteur` n'est pas une clé.
-   La relation `Copie` contient des informations qui ne sont pas en
    relation avec les copies.


### Clé étrangère : exemple

Dans le deuxième schéma l'attribut `idCorrecteur` dans la relation
`Copie` est une *clé étrangère*. Elle dépend fonctionnellement de la
clé primaire `idCopie`. Par transitivité, on peut retrouver toutes les
informations du correcteur.


### Clé étrangère : définition

Soit $R$ et $S$ deux relations de clés (primaires) respectives `idR`
et `idS`. Une clé étrangère de $S$ dans $R$ est un attribut `ce` de
$R$ dont la valeur est *toujours* identique à (exactement) une des
valeurs de `idS`.

Intuitivement, `ce` « référence » un (et un seul) tuple de $S$.

-   *Contrainte d'unicité* : une valeur de clé ne peut apparaître qu'une
    fois dans une relation.
-   *Contrainte d'intégrité référentielle* : la valeur d'une clé
    étrangère doit toujours être également une des valeurs de la clé
    référencée.


## Travail dirigé

voir le TP normalisation


# Aspects système

