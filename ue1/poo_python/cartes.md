Jouer avec les cartes
=====================


Dans ce tp, on souhaite réaliser un module `card` définissant une classe
`Card` permettant de représenter des cartes à jouer.

Une carte est caractérisée par

  - un couleur : parmi Trèfle, Coeur, Carreau, Pic ;
  - une valeur : parmi As, 2, 3, 4, 5, 6, 7, 8, 9, 10, Valet, Cavalier,
    Reine, Roi.

Pour modéliser les couleurs et les valeurs, on souhaite utiliser des
énumérations. Initialement, python ne dispose pas d'une telle
fonctionnalité. Pour définir une énumération, on utilise donc un module
dédié, le module `enum` :

``` python
from enum import Enum
```

# Couleurs des cartes

Toute nouvelle énumération doit hériter de la classe `Enum`. On y
définit des noms auxquels on associe des valeurs. Voici la définition
de la classe `Color` du module `card` :

``` python


class Color(Enum):
    """
    Color class for card game

    >>> type(Color.CLUB)
    <enum 'Color'>
    >>> len(Color)
    4
    >>> Color.SPADE.name == 'SPADE'
    True
    >>> [ col.rank() for col in Color ]
    [0, 1, 2, 3]
    """
    SPADE = chr(0x2660)
    HEART = chr(0x2665)
    DIAMOND = chr(0x2666)
    CLUB = chr(0x2663)

    def rank(self) -> int:
        """Return rank of an enum member in definition order"""
        return self.__class__._member_names_.index(self.name)

    @classmethod
    def nth(cls, n: int):
        """Return the color of a given rank"""
        return cls._member_map_[cls._member_names_[n]]
```

Les valeurs associées aux constante sont des chaînes de caractères
représentant la "couleur".

## Fonctionnalités offertes par la classe `Enum`

Les énumérations :

  - définissent un nouveau type et des constantes :

<!-- end list -->

``` python
type(Color.HEART)
```

> <enum 'Color'>
   
  - ces constantes possèdent deux attributs : leur nom (chaîne)…

<!-- end list -->

``` python
Color.SPADE.name
```
> 'SPADE'
  - et leur valeur (ici une chaîne utf-8)

<!-- end list -->

``` python
Color.SPADE.value
```
> '♠'
  - définissent une séquence ;

<!-- end list -->

``` python
for col in Color:
    print(col)
```
> Color.SPADE
> Color.HEART
> Color.DIAMOND
> Color.CLUB

Deux méthodes ont été ajoutées à l'énum Color : l'une pour obtenir le
rang d'une couleur dans l'énumération, l'autre pour obtenir la couleur
dont le rang est passé en paramètre :

``` python
print(Color.HEART, " a pour rang ", Color.HEART.rank())
print("la couleur de rang 2 est ", Color.nth(2))
```
> Color.HEART  a pour rang  1
> la couleur de rang 2 est  Color.DIAMOND

# Les hauteurs des cartes

De la même manière, les hauteurs des cartes seront représentées par une
énumération.

Les valeurs associées sont les hauteurs des cartes.

# Les cartes à jouer

Les cartes à jouer sont définies dans le module `card`. À la
construction, on initialise les deux attributs **privés** `__value` et
`__color`.

En python, lorsque le nom d'un attribut commence par deux blancs
soulignés, alors il est considéré comme privé. De manière analogue, les
attributs commençant par un seul blanc souligné sont considérés comme
`protected`.

**à faire** Renommez le fichier `card_squel.py` fourni en `card.py`.

**à faire** Complétez le constructeur et les deux accesseurs du module
`card` de manière à passer les tests

On souhaite pouvoir afficher et comparer les cartes à l'aide des
opérateurs usuels de comparaison. Python propose un moyen élégant
d'implanter ces fonctions :

  - pour personnaliser l'affichage, il suffit de surcharger la méthode
    spéciale `__str__` ;
  - pour implanter les comportements de comparaison, il suffit de
    surcharger les méthodes spéciales :
      - `__eq__`, `__neq__` (appelée lors des comparaisons `==` et `!=`)
        ;
      - `__le__`, `__li__`, `__gt__`, `__ge__` (appelée lors des
        comparaisons `<=`, `<`, `>` , `>=`)

**à faire** compléter la méthode `compare`, puis utiliser cette méthode
pour implanter les méthodes spéciales de comparaison.

``` python
from card import Card
from card_value import Value
from card_color import Color

c1 = Card(Value.JACK, Color.HEART)
c2 = Card(Value.KNIGHT, Color.SPADE)
print(c1)
print(c2)
if c1 < c2:
    print(c1, " est inférieure à ", c2)
```

> Jack of ♥
> Knight of ♠
> Jack of ♥  est inférieure à  Knight of ♠

# Le jeu de bataille

En utilisant le module `card`, réalisez un programme qui permet de jouer
à une version simplifiée du "jeu de la bataille" (sans carte masquée en
cas de "bataille").

Dans cette version, les joueurs ont initialement un nombre égal de
cartes distribuées aléatoirement. À chaque tour, les joueurs jouent
chacun la première de leur carte en la déposant sur la table. Celui qui
a déposé la carte de plus haute valeur remporte toutes les cartes sur la
table et les range à la fin de son tas de cartes. Si les hauteurs des
deux cartes jouées sont égales, il y a alors "**bataille**" et ces
cartes sont laissées sur la table. Le tour suivant peut alors commencer.

Le jeu s'arrête dès que l'un des joueurs n'a plus de carte, dans ce cas
il a perdu la partie (NB : des parties infinies sont possibles, on ne
cherchera pas à détecter ces situations).

Écrivez votre programme de manière qu'il puisse prendre en paramètre sur
la ligne de commande le nombre de cartes que possède chaque joueur au
début de la partie. Ce nombre devra être inférieur ou égal à 16 qui
sera aussi la valeur par défaut.
