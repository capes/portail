Jouer avec les expressions arithmétiques
========================================

On souhaite modéliser des expressions aritmétiques en python. Les
expressions que l'on veut pouvoir représenter comportent :

  - des constantes littérales (les entiers naturels, des variables)

  - des opérateurs unaires ;

  - des opérateurs binaires.
    
    Parmi les fonctionnalités, on veut pouvoir :
    
      - évaluer une expression ;
    
      - afficher une expression.


Une expression doit donc implémenter une interface `Expression`
disposant des précédentes opérations. En python, aucun mot-clé du
langage ne permet de déclarer des interfaces ou des classes abstraites.
On se contente de définir des méthodes dont les corps ne comportent
aucune instruction :

``` python
class Expression:
    def eval(self, context: dict = None) -> float:
        """Renvoie la valeur d'une expression."""
        pass

    def __str__(self) -> str:
        """Renvoie une chaine représentant l'expression."""
        pass

    __repr__ = __str__
```

Commentaires :

  - Le mot-clé `pass` permet de ne pas écrire le corps d'une fonction ;
  - Les méthodes `__str__` et `__repr__` sont des **méthodes magiques**
    : elles définissent des comportements spéciaux. Si `e` est un objet,
    alors :
      - la méthode `__str__` est appelée lorsque python évalue
        `print(e)`
      - la métohde `__repr__` est appelée lorsque l'interpréteur python
        souhaite obtenir une représentation de l'objet. Par défaut, il
        s'agit de l'identifiant de l'objet.

Une analyse du problème nous conduit à envisager une solution implantant
le diagramme de classes suivant :

![](media/images/expression.png)

# Les constantes

  - Leur valeur est initialisée à la construction ;
  - La fonction `__str__` renvoie une représentation textuelle de la
    valeur ;
  - La fonction `eval` renvoie la valeur.

# Les variables

  - Leur identifiant (`x`, `y`, `pi`, …) est initialisé à la
    construction;
  - La fonction `__str__` se contente de renvoyer l'identifiant ;
  - La fonction `eval` cherche l'identifiant parmi les clés du
    dictionnaire `context` passé en paramètre. Si la clé est présente,
    alors elle renvoie la valeur associée ; sinon on déclenche une
    exception.

# Les operateurs unaires

  - Il servent à modéliser les fonctions d'arité un : `-x`, `sin(x)`, …
  - à la construction, on initialise l'opérande, le symbole de
    l'opération ainsi qu'un booléen indiquant si l'expression est
    préfixée.
  - si l'expression est préfixée, alors sa représentation textuelle sera
    de la forme `-(x)` ou `cos(x)` ; sinon elle sera postfixée, comme
    `x!`.

# Les opérateurs binaires

  - Ils servent à modéliser les opérations arithmétiques usuelles
  - à la construction, on initialise le symbole de l'operation,
    l'opérande gauche et l'opérande droite.
  - la représentation textuelle de l'expression sera de la forme `repr_g
    op repr_d` où `repr_g` et `repr_d` désignent des représentations
    textuelles des opérandes gauche et droite et où `op` désigne
    l'opération.

**à faire** : Renommer le fichier `expression_squel.py` en `expression.py`

**à faire** : Implanter ces classes dans un module `expression`.

**à faire** : Créer un objet `expr` représentant l'expression
arithmétique `(1 + x)*(6 + y)`, afficher `expr` :

  - dans un interpréteur ;
  - à l'aide de `print`

**à faire** évaluer l'expression précédente
