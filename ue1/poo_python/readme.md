Éléments de programmation objet en python
=========================================

L'initiation de la programmation orientée objet fait partie du programme de NSI.

Afin de vous (re)familiariser avec les spécificités de la POO en python, nous vous proposons deux activités de travaux pratiques :

  - un module de [gestion de cartes](./cartes.md) à jouer dans lequel nous aborderons les *enums* et les *méthodes spéciales* ;
  - un module de modélisation d'[expressions arithmétiques](expressions.md) dans lequel nous aborderons les *interfaces*, les classes abstraites et *l'héritage*.
  
