"""
:mod:`card` module

:author: ` Équipe pédagogique Master MEEF NSI
         Univ. Lille <http://portail.fil.univ-lille.fr>`_

:date: 2022, september. Last revision: 2022, september


"""


from enum import Enum
import random


class Color(Enum):
    """
    Color class for card game

    >>> type(Color.CLUB)
    <enum 'Color'>
    >>> len(Color)
    4
    >>> Color.SPADE.name == 'SPADE'
    True
    >>> [ col.rank() for col in Color ]
    [0, 1, 2, 3]
    """
    SPADE = chr(0x2660)
    HEART = chr(0x2665)
    DIAMOND = chr(0x2666)
    CLUB = chr(0x2663)

    def rank(self) -> int:
        """Return rank of an enum member in definition order"""
        return self.__class__._member_names_.index(self.name)

    @classmethod
    def nth(cls, n: int):
        """Return the color of a given rank"""
        return cls._member_map_[cls._member_names_[n]]


class Value(Enum):
    """
    Value class for card game

    >>> type(Value.SEVEN)
    <enum 'Value'>
    >>> len(Value)
    13
    >>> Value.KING.name == 'KING'
    True
    """
    ACE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    HEIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13

    @classmethod
    def nth(cls, n: int):
        """Return the value of a given rank"""
        return cls._member_map_[cls._member_names_[n]]


class Card(object):
    """
    Cards are defined by a value and a color.


    >>> c1 = Card(Value.ACE, Color.HEART)
    >>> c1.get_color()
    <Color.HEART: '♥'>
    >>> c1.get_value()
    <Value.ACE: 1>
    >>> c2 = Card.random()
    >>> c2.get_value() in Value
    True
    >>> c2.get_color() in Color
    True
    >>> c1 == c1
    True
    >>> c1 != c1
    False
    >>> c1 < c1
    False
    >>> c1 <= c1
    True
    """

    BASE_UTF8 = {Color.SPADE: 0x1f0a0,
                 Color.HEART: 0x1f0b0,
                 Color.DIAMOND: 0x1f0c0,
                 Color.CLUB: 0x1f0d0}

    def __init__(self, value: Value, color: Color):
        """
        creates a card with value and color
        """
        pass

    def get_color(self) -> Color:
        """
        returns the color of the card

        :return: the color of the card
        :UC: none
        :Example:

        >>> c = Card(Value.ACE, Color.HEART)
        >>> c.get_color()
        <Color.HEART: '♥'>
        """
        pass

    def get_value(self) -> Value:
        """
        returns the value of the card

        :returns:  the value of the card
        :UC: none
        :Example:

        >>> c = Card(Value.ACE, Color.HEART)
        >>> c.get_value()
        <Value.ACE: 1>
        """
        pass

    def compare(self, other: "Card") -> int:
        """
         compares two cards values

         :param other: the other card
         :return:

            * a positive number if self's value is greater than card's
            * a negative number if self's value is lower than card's
            * 0 if self's value is the same greater than card's
         :UC: none
         :Example:

         >>> c1 = Card(Value.ACE, Color.HEART)
         >>> c2 = Card(Value.KING, Color.HEART)
         >>> c3 = Card(Value.ACE, Color.SPADE)
         >>> c1.compare(c2) < 0
         True
         >>> c2.compare(c1) > 0
         True
         >>> c3.compare(c1) == 0
         True
        """
        pass

    def __eq__(self, other: "Card") -> bool:
        """
        return True if self equals other
               False otherwise
        """
        pass

    def __neq__(self, other: "Card") -> bool:
        """
        return True if self don't equal other
               False otherwise
        """
        pass

    def __lt__(self, other: "Card") -> bool:
        """
        return True if self is strictly inferior to other
               False otherwise
        """
        pass

    def __le__(self, other: "Card") -> bool:
        """
        return True if self is inferior or equal to other
               False otherwise
        """
        pass

    def __gt__(self, other: "Card") -> bool:
        """
        return True if self is strictly superior to other
               False otherwise
        """
        pass

    def __ge__(self, other: "Card") -> bool:
        """
        return True if self is superior or equal to other
               False otherwise
        """
        pass

    def __str__(self) -> str:
        """
        Return a string representation of the card

        >>> c = Card(Value.KING, Color.CLUB)
        >>> str(c)
        'King of ♣'
        >>> c = Card(Value.TWO, Color.HEART)
        >>> str(c)
        'Two of ♥'
        """
        pass

    def __repr__(self) -> str:
        """
        Return a string representation of the card
        """
        return chr(self.BASE_UTF8[self.get_color()] + self.get_value().value)

    def random() -> "Card":
        """
        create a card whose value and color are randomly chosen

        :returns: *(card)* -- a randomly chosen card

        >>> c = Card.random()
        >>> c.get_value() in Value
        True
        >>> c.get_color() in Color
        True
        """
        pass


if __name__ == '__main__':
    import doctest
    doctest.testmod()
