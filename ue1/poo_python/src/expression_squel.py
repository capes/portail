"""
:mod: `expression` module

:author: ` Équipe pédagogique Master MEEF NSI
         Univ. Lille <http://portail.fil.univ-lille.fr>`_

:date: 2022, september. Last revision: 2022, september


"""


class Expression:
    def eval(self, context: dict = None) -> float:
        """Renvoie la valeur d'une expression."""
        pass

    def __str__(self) -> str:
        """Renvoie une chaine représentant l'expression."""
        pass

    __repr__ = __str__


class Constante(Expression):
    pass


class UndefVarError(Exception):
    """
    classe d'exception déclenchée lorsqu'une variable n'est pas dans
    l'environnement
    """
    def __init__(self, name: str):
        self.__msg = f"le contexte ne contient pas de variable {name}"
        super().__init__(self.__msg)


class Variable(Expression):
    pass


class OpUnaire(Expression):
    pass


class Oppose(OpUnaire):
    pass


class OpBinaire(Expression):
    pass


class Somme(OpBinaire):
    pass


class Produit(OpBinaire):
    pass


class Difference(OpBinaire):
    pass


class DivisionParZero(Exception):
    """Classe d'exception pour les division. Ne pas compléter"""
    pass


class Division(OpBinaire):
    pass
