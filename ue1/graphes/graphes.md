

# Le type Graphe Orienté

**Rappel** un graphe orienté $`G`$ est un triplet $`G=(V, E, w)`$, où

-   $`V`$ est l'ensemble des sommets;
-   $`E`$ est un ensemble de couple de sommets : les arêtes ;
-   $`w`$ est une fonction de $`E`$ dans $`\mathbb{R}`$ qui à chaque 
    arête associe son poids.

Ce type est muni des **opérations primitives** suivantes :

-   `successeurs(v)` qui à un sommet `v` associe l'ensemble des sommets
    $`w`$ tels que $`(v, w) \in E`$;
-   `predeccesseurs(v)` qui à un sommet `v` associe l'ensemble des sommets $`w`$
    tels que $`(w,v) \in E`$.
-   `degre_entrant(v)` et `degre_sortant(v)` qui à un sommet associent
    respectivement les degrés entrant et sortant d'un sommet.

Pour implanter les graphes en python, on ajoute les méthodes suivantes :

-   une méthode permettant d'ajouter un sommet à un graphe sous forme
    d'un entier.
-   une méthode permettant d'ajouter une arête $`(v,w, p)`$ à un
    graphe. $`v`$ et $`w`$ étant les numéros des sommets et $`p`$ le poids
    de l'arête;
-   une méthode permettant d'obtenir le poids d'une arête paramétrée par le couple de ses
    extrémités;
-   une méthode permettant d'obtenir la liste ordonnée des sommets du
    graphe.

Ceci définit une interface pour les graphes. Nous allons écrire deux
classes implémentant cette interface :

-   `MatrixGraph` basée sur une représentation par matrice de coût.
    Les instructions python `float('+inf')` et `float('-inf')`
    permettent de créer les flottants dénormalisés représentant $`+\infty`$ et $`-\infty`$.
    Cette fonction dispose d'un accesseure permettant d'obtenir la 
    matrice des coûts du graphe.
-   `ListGraph` qui utilise une représentation par listes d'adjacence.

**à faire** écrire ces deux classes en python.


# Les algorithmes de parcours

**à faire** Écrire une fonction `Dijkstra(G, s)` prenant en paramètre
 un graphe `G` dont les poids sont positifs et un sommet `s` de `G`
 et qui renvoie un couple `(d, prec)` où `d` est la liste des plus
 petite distance et `prec` une liste de prédecesseurs dans un plus
 court chemin.

**à faire** Écrire une fonction `Bellman(G, r)` prenant en paramètre
 un graphe `G` sans cycle et un sommet `r` racine de `G` et qui
 renvoie un couple `(d, prec)` où `d` est la liste des plus petite
 distance et `prec` une liste de prédecesseurs dans un plus court
 chemin.

**à faire** Écrire une fonction `chemin(G, s, e, algo)` qui renvoie un
plus court chemin de `s` à `e` dans $`G`$. `algo` est l'algorithme
utilisé Si `e` n'est pas accessible à partir de `s`, la fonction
renvoie `None`.

**à faire** Écrire une fonction `floyd(M)` prenant en paramètre la
 matrice $`M`$ des coûts d'un graphe et qui renvoie un couple $`(D, P)`$
 où $`D`$ est la matrice des plus petites distances et $`P`$ la matrice
 des prédécesseurs dans un plus court chemin.

