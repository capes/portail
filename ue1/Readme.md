UE1
===

Informatique fondamentale
-------------------------

Il s'agit de reprendre les points des programmes de SNT et  NSI avec un recul niveau master.

La section ['Textes réglementaires' de la page d'accueil du portail](../Readme.md#textes-r%C3%A9glementaires) référence ces programmes de SNT et NSI.

Informatique pour l'enseignement
--------------------------------

Préparation à la première épreuve d'admissibilité (Ecrit 1) :

  - Le sujet est composé de plusieurs problèmes
  - Toutes les notions des programmes de SNT et de NSI du lycée pourront être abordées.
  - Évaluation : capacités de raisonnement et d'argumentation ainsi que la maîtrise de la langue Française

Oral
----

Préparation à l'oral 1 "Épreuve de mise en situation professionnelle"

Cette épreuve a pour but d'évaluer la capacité du candidat à maîtriser et à 
organiser les notions correspondant au thème donné dans le sujet tiré, à les 
exposer avec clarté dans un langage adapté, à prêter aux questions posées par 
le jury toute l'attention souhaitable et enfin à répondre à ces questions de 
façon convaincante, tant d'un point de vue informatique que didactique ou pédagogique,
et avec une bonne aisance, y compris en termes de communication et de clarté
d'expression. Une très bonne maîtrise de la langue française, tant à l'écrit qu'à 
l'oral, est aussi attendue.

Liste des sujets pour la session 2020.
Les sujets sont les items numérotés de la liste. Les sous-items sont des pistes de reflexion pour
vous guider dans votre travail.
q
1. Réprésentation des données : types et valeurs de base
   - Représentation binaire des nombres : exemple et application
   - Expressions et logique booléennes, exemples et applications
   - Exemples d'activités utilisant le codage des caractères
2. Structures linéaires de données
   - Les tableaux et les listes : définition, problématiques et
     application.
   - Exemples de structures de données implémentées avec des tableaux
     ou des listes. Application
3. Traitement de données en tables
   - Exemples d'activités utilisant des données en table.
4. Arbres : structures et algorithmes
   - arbres et arborescences : définitions et propritétés
   - Exemples d'algorithmes opérant sur des arbres.
     - problèmes d'équilibrage d'un arbre
	 - Arbres binaire de recherche
5. Graphes : structures et algorithmes
   - Théorie des graphes, application aux automates.
   - Exemples d'algorithmes opérant sur des graphes.
   - Jeux et stratégies : exemples d'algorithmes
6. Bases de données relationnelles et système de gestion de bases de
   données
   - L'algèbre relationnelle
   - Les systèmes de gestion de bases de données
7. Algorithmes de tri
   - Exemples d'algorithmes de tri opérant par comparaison.
   - Les réseaux de tri : définition et application
8. Algorithmes gloutons
   - voyageur de commerce, sac à dos, rendu de monnaie
   - coloration et graphe d'intervalle
9. Méthode diviser pour régner
   - Méthode diviser pour régner : principe et exemples.
   - Exemples d'algorithmes diviser-pour-régner en mathématiques
   - Exemples d'algorithmes diviser-pour-régner en géométrie.
10. Programmation dynamique	
   - La programmation dynamique, principe et applications.
11. Recherche textuelle
	- Exemples d'algorithmes opérant sur des chaînes de caractères.
	- Exemples d'activités relevant du traitement automatique des textes
12. Constructions élémentaires des langages de programmation
    - les structures de contôle en programmation impérative 
13. Paradigmes de programmation
    - Exemples illustrant l'utilisation de différentes familles de langages de programmation
14. Fonctions
15. Récursivité
	- Les algorithmes récursifs : principe et exemples
	- Les structures récursives : principe et exemples
16. Utilisation de bibliothèques, mise au point de programmes et
    gestion de bugs
	- Exemples d'activités utilisant la programmation modulaire
	- Developpement dirigé par les tests : principe et exemple.
17. Calculabilité et décidabilité
18. Traitement sur une base de données à l'aide du langage SQL
	- Exemples d'application utilisant une base de données.
19. Architecture d'une machine
20. Principes de fonctionnement d'un système d'exploitation
21. Gestion des processus et des ressources par un système
    d'exploitation
	- Organisation et utilisation des fichiers, exemples d'algorithme
      de gestion
22. Réseau et algorithmes de routage
23. Sécurisation des communications
    - Exemple d'algorithme de chiffrement et de déchiffrement
	- Contrôle d'accès par mot de passe sur le web : principe et
      application
24. Principes de l'internet
    - Exemples d'activités autour de l'internet : structure,
      indexation et partage des données
25. Principes du web
	- Principe du web, langage de description
26. IHM sur le web : interaction avec l'utilisateur
	- Programmation événementielle : principe et application
27. Web : architecture client/serveur et interactions
	- Exemples d'activités utilisant un serveur fournissant des pages
      web
	- Exemples d'activités utilisant des web services

Leçons à travailler en priorité : 1, 2, 7, 14, 
