On s'intéresse dans ce tp aux tables de hachage. Lorsque le nombre de
clés à stocker est très grand (comme c'est le cas pour une base de
donnée), alors la taille de la table est telle qu'on ne peut plus la
stocker entiérement en mémoire, il faut l'enregistrer sur un support
perenne (disque dur, par ex.).

L'inconvénient à cela est que les temps d'accès sont alors bien plus
important. Si l'appartenance d'un élément à la table doit être évaluée
de nombreuses fois, cela peut devenir rédhibitoire.

Pour pallier ce problème, on peut ajouter une structure de données
intermédiaire qui servira d'*oracle*. On interroge cet oracle pour
savoir si un élément est dans la table :

-   si l'élément est dans la table, alors l'oracle répond oui ;
-   si l'élément n'est pas dans la table, alors l'oracle répond oui ou non.

Cette structure de données a pour objectif de limiter les requêtes
inutiles : pour tester la présence d'un élément dans la table, on
interroge d'abord l'oracle avant d'interroger la table. Il faut donc
concevoir un oracle qui réponde rarement "oui" lorsque l'élément n'est
pas dans la table.

Le type d'oracle que nous allons implanter est lui-même une table de
hachage : il s'agit des **filtres de Bloom**. On crée une table $`B`$ de
$N$ booléens. On dispose pour les clés que nous avons à ranger $`m`$
fonctions de hachage $`h_i\,,\,0\leq i < m-1`$.

Pour chaque chaque clé $`k`$ à ajouter à $`B`$, au lieu de se contenter de
mettre à vrai la cellule $`B(h(k))`$ comme on le ferait classiquement,
on va mettre à vrai toutes les cellules $`B(h_i(k))`$. Le principe étant
que la probabilité que deux clés différente aient les mêmes $`m`$ valeurs
pour leurs fonction de hachage est faible.

Par exemple, supposons que l'on souhaite entrer la clé `"timoleon"`
dans la table $`B`$ de taille 24, que nous ayons quatre fonctions de
hachage et que :

-   $`h_0("{\tt timoleon}")=2`$ ;
-   $`h_1("{\tt timoleon}")=12`$ ;
-   $`h_2("{\tt timoleon}")=3`$ ;
-   $`h_3("{\tt timoleon}")=20`$ ;

L'état de la table $`B`$ après insertion sera :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<tbody>
<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">2</td>
<td class="org-right">3</td>
<td class="org-right">4</td>
<td class="org-right">5</td>
<td class="org-right">6</td>
<td class="org-right">7</td>
<td class="org-right">8</td>
<td class="org-right">9</td>
<td class="org-right">10</td>
<td class="org-right">11</td>
<td class="org-right">12</td>
<td class="org-right">13</td>
<td class="org-right">14</td>
<td class="org-right">15</td>
<td class="org-right">16</td>
<td class="org-right">17</td>
<td class="org-right">18</td>
<td class="org-right">19</td>
<td class="org-right">20</td>
<td class="org-right">21</td>
<td class="org-right">22</td>
<td class="org-right">23</td>
</tr>


<tr>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">True</td>
<td class="org-right">True</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">True</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">True</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
<td class="org-right">False</td>
</tr>
</tbody>
</table>

Pour savoir si une clé est présente, on s'assurera que les $`m`$ cases de
la table $`B`$ correspondant aux valeurs des $`m`$ fonctions de hachage
sont positionnées à vrai.

L'objectif de ce TP est d'implanter un filtre de Bloom et de tester
l'efficacité de celui-ci en faisant varier sa taille ainsi que le
nombre de fonctions de hachage. Pour mesurer l'efficacité on estimera
le taux de faux positifs, c'est-à-dire le rapport entre le nombre de
fois où le filtre se trompe et le nombre de clé intérrogées.

**matériel fourni**

-   le fichier `bloomfilter_squel.py` , à renommer en `bloomfilter.py`

-   le fichier de test `test_squel.py` à renommer en `test.py`.

-   le fichier gnuplot `tp-bloom.plt`


# Fonctions de hachage d'une chaîne de caractères.

Nous allons supposer que nous manipulons des clés qui sont des chaînes
de caractères dont les caractères appartiennent aux 128 premiers
caractères de la table ASCII.

Afin d'obtenir différentes fonctions de hachage, nous allons attribuer
un code différents aux caractères pour chaque fonction hachage. Le
code d'un caractère sera obtenu de manière aléatoire. À cette fin, on
a créé un tableau à deux dimensions `random_tab` contenant tous ces codes :

-   Si un caractère $`c`$ a pour code ASCII $`j`$, alors  
    $`h_i(c) = {\tt random\_tab[i][j]}`$

-   Une chaine $`s`$ aura pour pour valeur $`h_i(s)`$ la somme des valeurs
    $`h_i(c)`$ pour $`c`$ caractère de $`s`.

Ce tableau peut être initialisé grâce à la fonction
`init_random_tab`. Cette fonction ne doit être appelée qu'une seule
fois lors des tests.

**à faire**

Dans le fichier `test.py`, réaliser une fonction   
`code_of_string(s: str, i:int)->int` prenant en paramètre une chaîne `s`, le numéro `i` d'une
fonction de hachage, et qui renvoie $`h_i(s)`$.


# Le module `bloomfilter`

Ce module va implanter un filtre de Bloom qui associe un booléen à une
clé. Il contient une classe disposant de deux méthodes et d'un constructeur :

-   le constructeur de la classe `BloomFilter` prend en paramètre :
    
    -   la taille $`N`$ de la table du filtre ;
    -   une fonction à deux paramètres $`s`$ et $`i`$ et qui renvoie la valeur
        de $`h_i(s)`$ ;
    -   le nombre $`m`$ de fonctions de hachage à tester.
    
    Le constructeur représentera la table par un tableau numpy de $`N`$ booléens.
-   une méthode `add` permettant d'ajouter une clé au filtre ;
-   une méthode `contains` permettant de test l'appartenance d'une clé au filtre.

**à faire**

Écrire le code de ces trois méthodes.

**à faire**

Tester vos fonctions en utilisant le programme de test fourni qui
insère le mot `"timoleon"` puis teste sa présence dans le filtre, puis
teste la présence d'un mot aléatoire.

**à faire**

Déterminer une taille du filtre pour laquelle un mot tiré au hasard
apparaît présent, ce qui veut dire qu'on a un faux positif.


# Analyse des faux positifs

Maintenant que vous disposez d'un filtre de Bloom fonctionnel, vous
allez pouvoir mesurer l'influence du nombre de fonctions de hachage et
de la taille du filtre sur le nombre de faux positif.

Pour réaliser ces tests, nous allons faire varier :

-   le nombre de fonctions de hachage de 1 à 8 ;
-   la taille du filtre de $`2^{10}`$ à $`2^{20}`$.

Pour chaque jeu de test, nous allons construire un filtre de Bloom et
y insérer $`2^{10}`$ mots tirés au hasard. Une fois ces mots insérés, on
tirera au hasard $`2^{14}`$ mots de tests dont on testera la
présence. Il ne faudra pas oublier de s'assurer que les mots de test
ne sont pas dans le jeu de mots insérés initialement. Le pseudo-code
est donc :

    creer l'ensemble des mots à inserer I
    pour n = 1 à 8 faire
      pour t = 10 à 20 faire
        creer un filtre de bloom BF de taille 2^t à n fonctions de hachage
        inserer les mots de I dans BF
        pour k = 1 to 2^14 faire
          tirer un mot au hasard U
          si U n'appartient pas à I alors
            augmenter le compteur de mots testés
            si U appartient à BF alors
              augmenter le compteur de faux positifs
            fin si
          fin si
        fin pour
        imprimer dans cet ordre:
           la taille t du filtre, le nombre de fonctions, le nombre de mots testes,
           le nombre de faux positifs, le taux de faux positifs
      fin pour
      imprimer deux lignes vides
    fin pour

**à faire**

Écrire une procédure test correspondant à l'algorithme donné
ci-dessus.

On devrait obtenir un affichage du type :

    10 1 16384 10357 0.63214111328125
    11 1 16384 6307 0.38494873046875
    12 1 16384 3668 0.223876953125
    13 1 16384 1910 0.1165771484375
    14 1 16384 1007 0.06146240234375
    15 1 16383 497 0.03033632423853995
    16 1 16384 241 0.01470947265625
    17 1 16383 142 0.008667521211011414
    18 1 16384 156 0.009521484375
    19 1 16384 143 0.00872802734375
    20 1 16383 141 0.008606482329243728
    
**à faire**

Enregistrer vos résultats dans un fichier nommé `res.txt`

**à faire**

Utiliser le programme Gnuplot `tp-bloom.plt` pour tracer la courbe des résultats obtenus.

    gnuplot < tp-bloom.plt

