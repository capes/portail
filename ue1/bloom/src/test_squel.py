# -*- coding: utf-8 -*-

""":mod:`test` module : Test module for bloomfilter analysis

:author: `FIL - Univ. Lille <http://portail.fil.univ-lille1.fr>`_

:date: 2016, january

"""
import random
from bloomfilter import BloomFilter

nb_hash_functions = 8
random_tab = [ [0 for i in range(128)]
           for _ in range(nb_hash_functions)]

def init_random_tab ():
    """
    Creates the hash functions.
    """
    global random_tab
    for i in range(nb_hash_functions):
        for j in range(128):
            random_tab[i][j] = random.randint(1,32000)

def code_of_string (s: str, n: int)->int:
    """
    For a given string, returns the hash code for the n-th hashing function.

    :param str: The string to be hashed.
    :type str: string
    :param n: The function number.
    :type n: int
    :return: A hash code
    :rtype: int

    .. note:: 
       1 <= n <= nb_hash_functions
    """
    pass

def random_word ()->str:
    """
    Returns a word with random letters whose length is between 4 and 7.

    :rtype: string
    """
    letters = [ chr(i) for i in range(ord('a'),ord('z')+1) ] + [ chr(i) for i in range(ord('A'),ord('Z')+1) ]
    length = 4 + random.randint(0,4)
    str = ""
    for i in range(length):
        str = str + random.choice(letters)
    return str

if __name__ == "__main__":
    init_random_tab()
    bf = BloomFilter(10, code_of_string, 8)
    w = random_word()
    bf.add("timoleon")
    if bf.contains("timoleon"):
        print(f"timoleon est present")
    if bf.contains(w):
        print(f"{w} est present")
