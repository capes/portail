# -*- coding: utf-8 -*-

""":mod:`bloomfilter` module : Implements a bloomfilter.

:author: `FIL - Univ. Lille <http://portail.fil.univ-lille1.fr>`_

:date: 2016, january

"""
import numpy as np

class BloomFilter:
    def __init__(self, n: int, f: "function", m: int):
        """
        Creates a new empty Bloom filter of size :math:`2^n`

        :param n: the log of the size of the filter
        :type n: int
        :param f: the hash function whose should take as input two 
                  parameters: the value to be hashed and the number 
                  of the subfunction used
        :type f: function(any,int)
        :param m: the number of functions provided by *f*
        :return: the new Bloom filter
        :rtype: dict
        """
        pass

    def add(self, e):
        """
        Adds *e* to *bf*.

        :param e: The element to be added
        :type e: Any
        """
        pass

    def contains(self, e):
        """
        Returns True if *e* is in *bf*.

        :param e: The element to be tested
        :type e: Any
        """
        return False
