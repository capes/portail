

# Définition

Un problème d'optimisation combinatoire consiste à trouver, dans un
ensemble discret, une parmi les meilleures solutions acceptables à un
problème.


## Exemples :

-   Trouver un rendu de monnaie qui minimise le nombre de pièces;
-   Trouver un ensemble d'activités maximal pouvant être réalisées sans
    incompatibilité;
-   Minimiser le temps d'assemblage sur deux chaînes de production ;
-   Trouver un des trajets les plus courts pour le TSP.


## formalisation

Un problème d'optimisation combinatoire est un quadruplet $(I, r, f, g)$ où

-   $I$ est un ensemble d'instances du problème ;
-   $r$ est une fonction qui, à chaque instance $i$, associe un ensemble
    fini de solutions réalisables $r(i)$ ;
-   $f$ est une fonction objectif : si $i$ est une instance et $s$ une
    solution de $s$, alors $f(i, s)$ est la mesure de la qualité de solution ;
-   $g$ est soit min ou max.

un problème d'optimisation combinatoire consiste à déterminer pour une
instance $i$ du problème, une solution réalisable $s$ telle que :
$f(i, s) =\texttt{g}\lbrace f(i, s') ; s'\in r(i)\rbrace$


## Exemple : TSP

Dans le cas du problème du voyageur de commerce :

-   une instance du problème peut être, par exemple, la donnée d'une 
    matrice carrée fournissant les distances entre les villes.
-   à chaque instance du problème, l'ensemble des solutions réalisables
    est une permutation des $n$ villes ;
-   La fonction objectif est la distance total à parcourir
-   $g=\texttt{min}$


## Exemple : Rendu de monnaie

Dans le cas du rendu de monnaie :

-   une instance du problème peut-être la liste $\ell$ des $n$ valeurs
    numéraire $v_i$ des pièces, ainsi que la somme $S$ à atteindre ;
-   pour chaque instance $(\ell, S)$, une solution admissible est un $n$-uplet qui
    indique pour chaque pièce $i$ le nombre $n_i$ de pièce à prendre. Il faut que
    $\sum n_i\times v_i = S$
-   $f$ est le nombre total de pièces d'une solution, c'est-à-dire $\sum n_i$
-   $g=\texttt{min}$


## Exemple : le choix d'activité

Dans le cas du choix d'activités,

-   une instance est la données de $n$ intervalles de temps $[d_i, f_i[$  :
    -   $d_i$ est la date de début de la $ième$ tâche ; $f_i$ sa date de fin
-   pour chaque instance du problème, une solution est une liste
    d'entiers indiquant les tâches à accomplir de telle sorte que deux
    tâches de cette liste ne s'exécutent pas en même temps;
-   $f$ est le nombre d'activités de la liste ;
-   $g=\texttt{max}$


## Exemple : chaine de production

Dans le cas des deux chaines de production :

-   une instance du problème est la donnée des temps d'assemblage des
    $n$ postes sur chacune des deux chaines, ainsi que les temps de transition
    entre les postes ;
-   pour une instance du problème donnée, une solution possible est une
    liste de $n$ valeur indiquant si le $ième$ travail se fait sur la
    chaîne 1 ou 2.
-   $f$ est le temps total d'assemblage
-   $g=\texttt{min}$


# Problème de décision

Tout problème d'optimisation combinatoire peut être transposé en un
problème de décision qui dit si oui ou non il y a une solution
réalisable pour une valeur particulière $k$ de la fonction d'objectif.

Par exemple, on peut transformer une instance du problème du rendu de
monnaie en :

-   y a-t-il une solution avec une pièce ?
-   y a-t-il une solution avec deux pièces ?
-   &#x2026;
-   y a-t-il une solution avec $k$ pièce ?


# Complexité des problèmes d'optimisation

On s'intéresse donc aux algorithmes permettant de résoudre, soit le
problème de décision, soit le problème initial.

On peut formaliser les choses à l'aide des machines de Turing, mais
cela devient trop fastidieux.

On considère que les algorithmes sont décrits en une suite
d'opérations élémentaires dont le temps d'exécution est constant.  Dès
lors on cherche à déterminer le nombre d'opérations élémentaires
permettant d'aboutir à une solution en fonction de la taille de
l'instance du problème.


# Classes de complexité


## $P$

Dire qu'un problème d'optimisation est de la classe $P$ signifie que
le problème de décision correspondant peut être programmé sur une
machine de Turing et fournir une réponse en temps polynomial (fonction
polynomial de la taille de l'instance)

Lorsqu'il existe un algorithme permettant de résoudre un problème
d'optimisation en temps polynomial, alors on admet le problème de
décision l'est aussi et que ce problème est dans la classe $P$.


## NP

Un problème d'optimisation est de la classe $NP$ signifie que le
problème de décision correspondant peut être programmé sur une machine
de Turing non déterministe en temps polynomial (NP = Non déterministe
Polynomial)

En pratique, étant donnée une instance d'un problème de la classe NP :

-   on ne sait pas s'il exste un algorithme permettant de le résoudre en
    temps polynomial ;
-   vérifier une solution fournie est polynomial.


## NP Complet

Parmi tous les problèmes de la classe $NP$, on distinque ceux qui sont
aussi difficiles à résoudre que tous les autres : il s'agit des
problèmes les plus durs.

Tous les autre problèmes se ramènent à ceux-ci via une réduction
polynomial.


# Méthodes de résolution

Pour résoudre un problème d'optimisation combinatoire, étant donnée
une instance $i$ il "suffit" de rechercher le minimum ou le maximum
sur l'ensemble fini $r(i$).

Toutefois le cardinal de $r(i)$ est souvent exponentiel en fonction de
la taille de $i$.


## TSP : nombre de solutions

Par exemple, pour le TSP, si on a $n$ villes, une solution réalisable
peut être représenté par une permutatation de $\lbrace 1, \ldots,
n\rbrace$ Il y a $n!$ permutations. Si on considère que la tournée est
un cycle, alors on peut imposer un sommet de départ (et d'arrivée). Il
y a donc $n-1!$ tournées.


## Rendu de monnaie : nombre de solutions

Si on a $n$ valeurs de pièces et une somme $S$, alors le nombre de
solutions réalisables est borné par $\vert S\vert^n$
&#x2026; exponentiel ou polynomial ?


## Choix d'activités

On dispose de $n$ activités avec pour chaque activité la date de début
et de fin.

Ce problème s'apparente à un problème de coloration de graphe


## Chaîne de production

Il y a $2^n$ parcours (si $n$ est le nombre de postes)

