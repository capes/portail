import random

BEST_KEPT = 5

class AlgoGen(object):

    def __init__(self, problem, population_size, crossover_rate, mutation_probability):
        '''
        build an genetic algorithm to solve problem using a population of size population_size and a probability of muation of mutation_probability

        :param problem:  the problem to solve
        :type problem: a Problem object
        :param population_size: the size of the population (must be even)
        :type population_size: int 
        :param mutation_probability: the mutation probability 
        :type mutation_probability: float 
        :UC: population_size must be even and mutation_probability must be  0<= and <1
        '''
        self.__problem = problem
        self.__population_size = population_size
        self.__population = []        
        self.__nb_crossover = crossover_rate
        self.__mutation_probability = mutation_probability

    def get_population(self):
        return self.__population

    def genesis(self):
        """
        create the population
        """
        pass

    def evaluate_population(self, population):
        """
        evalue la  population passée en paramètre
        :param population: (list) la population
        """
        pass


    def next_generation_tournament(self, population):
        '''
        organise un tournoi entre les individu d'une population .
        chaque individu choisi une et une seule fois, dans un tournoi ,
        vainqueur conservé taille résultat = 1/2 taille population

        :param population: (list) la population
        :return: (list) les vainqueurs du tournoi
        '''
        pass


    def crossover(self, population):
        '''Croise les individus d'une population.

        Chaque individu est sélectionné une et une seule fois, on
        garde le meilleur descendant à chaque fois.

        taille résultat = 1/2 taille population

        :param population: (list) a population
        :return: (list) new individuals
        '''

        pass

    def mutate(self, individuals):
        """
        mute les individus
        :param individuals: (list) une liste des individus
        """
        pass

    def one_round(self):
        '''
        applique successivement les opéréateurs 
        - selection 
        - croisement
        - mutation
        '''        
        pass


    def solve(self, nb_rounds):
        """
        tente d'approcher une solution en faisant muter la population
        :param nb_rounds: (int) nombre de génération
        """
        self.genesis()
        self.evaluate_population(self.__population)
        self.display_stat('INITIAL')        
        for step in range(nb_rounds):
            self.one_round()
            #self.display_population(step=step+1, number = 20)
            self.display_stat(step)
        #self.display_population(number = 20)  # display 20 best individuals
        return self.__problem.best_individual(self.__population)


    def display_stat(self, step):
        best =  self.__problem.best_individual(self.__population)
        all_scores = [ individual.get_score() for individual in self.__population ]
        average = sum (all_scores) / self.__population_size
        print ( ' STEP = {}  --  best = {} - avg = {} '.format(step, best, average) )


    def display_population(self, step = ' ', number = 0):
        if number <= 0:
            individuals = self.__population
        else:
            individuals = self.__population[:min(number, len(self.__population))]
        print( ' **** STEP : {} ********* '.format(step) )
        for individual in individuals :
            print ( individual )
        print( )


# tool function
def biased_wheel(alea,scores):
    '''
    return smallest i such that alea < sum(scores[k], 0 <= k < i)
    CU : 0 <= alea < sum(scores)
    '''
    current_sum = 0
    result_index = -1
    while alea > current_sum :
        result_index = result_index + 1
        current_sum = current_sum + scores[result_index]            
    return result_index
