class Problem_Interface(object):
    '''
    a prototype class for "problems" in genetic algorithm

    an Individual class must be associated to a problem to represent the individuals
    '''

    def create_individual(self):
        '''
        create a randomly generated indidivual for this problem

        :return: a randomly generated individual gfor this problem
        :rtype: an Individual object
        '''
        pass

    def evaluate_fitness(self, individual):
        '''
        compute the fitness of individual for this problem

        :param individual: the individual to consider
        :type individual: an Individual object
        :return: the fitness of individual for this problem
        :rtype: an Individual object
        '''
        pass

    def sort_population(self, population):
        '''
        sort population from best fitted to worst fitted individuals.
        Depending on the problem, it can correspond to ascending or descending order with respect to the fitness function.

        side effect : population is modified by this method

        :param population: the list of individuals to sort
        :type population: list(Individual)        
        '''
        pass

    def best_individual(self, population):
        '''
        return the best fitted individual from population.
        Depending on the problem, it can correspond to the individual with the highest or the lowest fitness value.


        :param population: the list of individuals to sort
        :type population: list(Individual)
        :return: the best fitted individual of population
        :rtype: an Individual object
        '''
        pass

    def tournament(self, first, second):
        '''
        perform a rounament between two individuals, the winner is the most fitted one, it is returned


        :param first: an individual
        :type first: an Indivdual object
        :param second: an individual
        :type second: an Individual object
        :return: the winner of the tournament
        :rtype: Individual object
        '''
        pass
