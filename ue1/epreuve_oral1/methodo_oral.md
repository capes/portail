---
title: Méthodologie pour l'épreuve de leçon
author: Préparation CAPES - MEEF NSI
date: septembre 2023
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr
---

# Objectifs 

* Réaliser un **plan** hiérarchisé et détaillé sur un sujet (cf [liste de leçons](./liste_lecons_2023.pdf))
* Ce plan doit être illustré par une **activité**

# Forme

* Un **diaporama** attendu (ou notebook jupyter) 
* Un élément **historique** (à minima) cité
* Une **activité** préparée (code à exécuter, activité débranchée)

# Ressources

* Obligation d'utiliser au moins un **livre** du supérieur pour préparer sa leçon
* Citer obligatoirement les **ressources** utilisées (à la fin du diaporama)
* Éviter les ressources en ligne : elles ne seront pas disponibles le jour de l'oral

# Construction du plan

* Eviter le **hors-sujet** : aucune partie en doublon avec les autres leçons
* Délimiter le sujet : **définitions** précises de la terminologie du sujet et de celle utilisée dans le plan
* Repérer les notions dans les **programmes**
* **Elargir** au niveau universitaire
* Les diapositives  :
    * devront être **lisibles** (ne pas les surcharger)
    * ne comporteront pas de **faute** d'orthographe
    * reprendront les titres **bien choisis** du plan

# Activités

* Une à deux au grand maximum
* Donner le **contexte** (place dans le programme, prérequis)
* Donner son **but** (quelle(s) notion(s) travaillée(s) : _ne vous dispersez pas_)
* Fournir **une trame** avec quelques questions
* Avoir préparé une **correction**

# Pendant l'oral

* Utiliser Pyzo _ou_ Jupyter _ou_ le terminal directement pour exécuter/éditer les programmes en Python
* Montrer votre capacité à écrire en direct un peu de code (pas d'improvisation)
* Expliquez certaines notions directement au tableau (débranché)
* Gérer son temps

# Quand j'assiste à l'oral

* Je ne consulte pas mon smartphone
* je prends des notes ... à minima le plan
* je réfléchis à une question
