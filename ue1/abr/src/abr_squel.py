#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = ''
__date_creation__ = '12/10/2021'
__doc__ = """
:mod:`abr_rn` module
:author: {:s} 
:creation date: {:s}
:last revision:

arbres binaires de recherche
""".format(__author__, __date_creation__)

from binary_tree import BinaryTree, BinaryTreeError

class AbrError(BinaryTreeError):
    def __init__(self, msg):
        super().__init__( msg)

class Abr(BinaryTree):
    """
    Arbre binaire de recherche

    :Exemples:

    >>> a = Abr()
    >>> a = a.insere(2) 
    >>> a = a.insere(3)
    >>> a == Abr(2, Abr(), Abr(3, Abr(), Abr()))
    True
    >>> a.recherche(4)
    False
    >>> a.recherche(3)
    True
    >>> a.maximum() == 3
    True
    >>> a = a.supprime(3)
    >>> a == Abr(2, Abr(), Abr())
    True

    """
    def __init__(self, *args):
        super().__init__(*args)
        if len(args) > 0:
            rac, sag, sad = args
            if not isinstance(sag, Abr) or not isinstance(sad, Abr):
                raise AbrError('type non conforme')
            if not sag.est_abr() or not sad.est_abr():
                raise AbrError('données non conformes')
    #
    def est_abr(self):
        """
        :return: (bool) True iff self is an abr
        """
        pass
    #
    def insere(self, elt):
        """
        insere un élément dans l'arbre
        :param elt: (any) l'élément à insérer
        :result: (Abr) un nouvel Abr contenant les éléments de self et elt
        :exemples:

        """
        pass
    #
    def recherche(self, elt):
        """
        recherche un élément dans un self
        :param elt: (any) un élément 
        :param self: (BinaryTree) un self
        :return: True si elt est un noeud de l'abre, Faux sinon
        :CU: elt doit être comparable avec les noeuds de l'arbre
        """
        pass
    #
    def minimum(self):
        """
        :param self: (BinaryTree) un arbre binaire de recherche
        :return: (int) le minimum de l'arbre
        """
        pass
    #
    def maximum(self):
        """
        :param self: (BinaryTree) un arbre binaire de recherche
        :return: (int) le maximum de l'arbre
        """
        pass
    #
    def supprime(self, elt):
        """
        :param elt: (int) un élément
        :param self: (BinaryTree) un self
        :return: (BinaryTree) an self with same elements as self, but without elt
        """
        pass

if __name__ == '__main__':
    import doctest
    doctest.testmod()
