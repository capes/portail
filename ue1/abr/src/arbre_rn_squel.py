#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'Benoit Papegay'
__date_creation__ = '12/10/2021'
__doc__ = """
:mod:`abr_rn` module
:author: {:s} 
:creation date: {:s}
:last revision:

arbres rouge-noir
""".format(__author__, __date_creation__)

from binary_tree import BinaryTree, BLACK, WHITE, escape_str
from abr import Abr
import time

REGLES_RN_INSERT = { "(h1:N, (h2:R, (h3:R, g3, d3), d2), d1)" : "(h2:R, (h3:N, g3, d3), (h1:N, d2, d1))",
                     "(h1:N, (h2:R, g2, (h3:R, g3, d3)), d1)" : "(h3:R, (h2:N, g2, g3), (h1:N, d3, d1))",
                     "(h1:N, g1, (h2:R, (h3:R, g3, d3), d2))" : "(h3:R, (h1:N, g1, g3), (h2:N, d3, d2))",
                     "(h1:N, g1, (h2:R, g2, (h3:R, g3, d3)))" : "(h2:R, (h1:N, g1, g2), (h3:N, g3, d3))"}

REGLES_RN_SUPPRIME_DROITE_OK = { "(h1:R, (h2:N, g2, d2), d1)" : "(h1:N, (h2:R, g2, d2), d1)",
                                 "(h1:N, (h2:R, g2, (h3:N, g3, d3)), d1)" : "(h2:N, g2, (h1:N, (h3:R, g3, d3), d1))" }

REGLES_RN_SUPPRIME_DROITE_KO = { "(h1:N, (h2:N, g2, d2), d1)" : "(h1:N, (h2:R, g2, d2), d1)" }

REGLES_RN_SUPPRIME_GAUCHE_OK = { "(h1:R, g1, (h2:N, g2, d2))" : "(h1:N, g1, (h2:R, g2, d2))",
                                 "(h1:N, g1, (h2:R, (h3:N, g3, d3), d2))" : "(h2:N, (h1:N, g1, (h3:R, g3, d3)), d2)" }

REGLES_RN_SUPPRIME_GAUCHE_KO = { "(h1:N, g1, (h2:N, g2, d2)" : "(h1:N, g1, (h2:R, g2, d2))" } 

REGLES_RN_SUPPRIME_OK = { "droite" : REGLES_RN_SUPPRIME_DROITE_OK,
                          "gauche" : REGLES_RN_SUPPRIME_GAUCHE_OK }

REGLES_RN_SUPPRIME_KO = { "droite" : REGLES_RN_SUPPRIME_DROITE_KO,
                          "gauche" : REGLES_RN_SUPPRIME_GAUCHE_KO }
class ArbreRN(Abr):
    """
    >>> a = ArbreRN()
    >>> for i in range(10):
    >>>    a = a.insere(i)
    >>> a.est_arbre_rn()
    True
    >>> a.supprime(9).maximum()
    8
    """
    #
    def __init__(self, *args):
        super().__init__(*args)
        if len(args) > 0:
            rac, sag, sad = args
            if not isinstance(sag, ArbreRN) or not isinstance(sad, ArbreRN):
                raise ArbreRNError('mauvais type pour constuire un arbre rouge-noir')
            self._color = 'R'
    #
    def __str__(self):
        """
        :return: (str) string representation of tree 
        """
        if self.is_empty():
            return '()'
        else:
            repr_left = str(self.get_left_subtree())
            repr_right = str(self.get_right_subtree())
            return '({:s}:{:s}, {:s}, {:s})'.format(str(self.get_data()),
                                                    self.get_color(),
                                                    repr_left, repr_right)
    #
    __repr__ = __str__
    #
    @staticmethod
    def from_prefix(prefix, substitutions):
        """
        :param prefix: (str) a prefix expression
        :param substitutions: (dict) a dictionary associating key with BinaryTree
        :return: (BinaryTree) a new tree from prefix and substitutions
        """
        if BinaryTree._is_atomic(prefix):
            res = substitutions[ prefix ]
        else:
            d, left, right = BinaryTree._sub_expressions(prefix.strip("() "))
            node, color = d.split(':')
            assert node in substitutions, "{} n'est pas défini".format(d)
            res = ArbreRN( substitutions[node],
                           ArbreRN.from_prefix(left, substitutions),
                           ArbreRN.from_prefix(right, substitutions))
            res.set_color(color)
        return res
    #
    def unify(self, expression ):
        """
        :param expression: (str) an infix expression
        :return: (tuple) a couple (unified, subst) where
                - unified (bool) is True iff expression and tree are unified
                - subst (dict) te dictionary of substitutions
        """
        res = True
        subst = {}
        if len(expression) > 0:
            if BinaryTree._is_atomic(expression):
                subst[expression] = self
            else:
                try:
                    data, left, right = BinaryTree._sub_expressions(expression.strip("() "))
                    value, color = data.split(':')
                    if color == self.get_color():
                        subst[value.strip()] = self.get_data()
                        res_l, subst_l = self.get_left_subtree().unify(left)
                        res_r, subst_r = self.get_right_subtree().unify(right)
                        res = res_l and res_r
                        if res:
                            subst.update(subst_l)
                            subst.update(subst_r)
                    else:
                        res = False
                except Exception as e:
                    res = False    
        return res, subst
    #
    def to_dot(self, background_color=WHITE):
        '''
        :return: (str) dot representation of tree
        '''
        LIEN = '\t"N({:s})" -> "N({:s})" [color="{:s}", label="{:s}", fontsize="8"];\n'
        def aux(arbre, prefix=''):
            if arbre.is_empty():
                descr = '\t"N({:s})" [color="{:s}", label=""];\n'.format(prefix,
                                                                         background_color)
            else:
                c = arbre.get_data()
                color = "#FF0000" if arbre.get_color() == "R" else "#000000"
                descr = '\t"N({:s})" [color="{:s}", label="{:s}"];\n'.format(prefix,
                                                                             color,
                                                                             escape_str(c))
                s_a_g = arbre.get_left_subtree()
                label_lien, couleur_lien = ('0', BLACK) if not s_a_g.is_empty() else ('', background_color)
                descr = (descr +
                         aux(s_a_g, prefix+'0') +
                         LIEN.format(prefix, prefix+'0', couleur_lien, label_lien))
                s_a_d = arbre.get_right_subtree()
                label_lien, couleur_lien = ('1', BLACK) if not s_a_d.is_empty() else ('', background_color)
                descr = (descr +
                         aux(s_a_d, prefix+'1') +
                         LIEN.format(prefix, prefix+'1', couleur_lien, label_lien))
            return descr
        return "/*\nBinary Tree\n\nDate: {}\n\n,*/\ndigraph G {{\n\tbgcolor=\"{:s}\";\n\n{:s}\n}}".format(time.strftime('%c'),
                                                                                                          background_color,
                                                                                                          aux(self))
    #
    def couleurs_le_long_des_chemins(self):
        """
        :return: les couleurs le long des chemins de la racine aux feuilles
        :exemples:

        >>> VIDE = ArbreRN()
        >>> a = ArbreRN(1, VIDE, VIDE)
        >>> a.set_color('N')
        >>> b = ArbreRN(2, VIDE, VIDE)
        >>> b.set_color('R')
        >>> c = ArbreRN(3, a, b)
        >>> c.set_color('N')
        >>> set(c.couleurs_le_long_des_chemins()) == { 'NR', 'NN'}
        True
        """
        pass
    #
    def est_arbre_rn(self):
        """
        :param arbre: (ArbreRN) un arbre binaire de recherche
        :return: (bool) True ssi arbre est un arbre rouge-noir
        :CU: arbre est un arbre binaire de recherche
        """
        pass
    #
    def get_color(self):
        """
        :return: (dict) le coloriage associé à l'arbre
        :exemples:

        >>> a = ArbreRN(1, ArbreRN(), ArbreRN())
        >>> a.set_color('N')
        >>> a.get_color() == 'N'
        True
        """
        return self._color
    #
    def set_color(self, color):
        """
        modifie la couleur du noeud

        :param color: (str) la couleur du noeud
        """
        self._color = color
    #
    def get_coloriage(self):
        res = {}
        res[self.get_data()] = self._color;
        res.update( self.get_left_subtree().get_coloriage() )
        res.update( self.get_right_subtree().get_coloriage() )
        return res
    #
    def insere(self, elt):
        """
        :param elt: (any) un élément
        :return: (ArbreRN) l'arbre rééquilibré
        :Exemples:

        >>> a = ArbreRN()
        >>> a.insere(1).insere(2).insere(3).est_arbre_rn()
        True
        """
        pass
    #
    def supprime(self, elt):
        """
        :param elt: (int) un élément
        :return: (BinaryTree) un arbre rouge-noir contenant les éléments
             de l'arbre, sans elt
        """
        pass

def applique_regle(arbre, depart, arrivee):
    """
    Transforme un arbre en utilisant une expression

    :param arbre: (BinaryTree) un arbre 
    :param depart: (str) l'expression infixe de départ
    :param arrivee: (str) l'expression infixe d'arrivée
    :return: (tuple) un triplet (arbre_transformé, couleurs, substitutions)
             ou None si l'arbre n'est pas unifiable avec l'expression `depart`
    """
    res = None
    unified, substitutions = arbre.unify(depart)
    if unified:
        res = ArbreRN.from_prefix(arrivee, substitutions)
    return res

def applique_regles(arbre, regles):
    """
    :param arbre: (BinaryTree) un abr
    :param regles: (disct) un ensemble de regles
    :return: (BinaryTree) l'arbre modifié ou None si aucune règle ne s'applique
    :Side-Effect: modifie le coloriage
    """
    res = arbre
    for d, a in regles.items():
        aux = applique_regle(arbre, d, a)
        if aux != None:
            res = aux
    return res

if __name__ == '__main__':
    import doctest
    doctest.testmod()
