
# Table des matières

1.  [Arbres binaires de recherche](#org43c0c8c)
2.  [Transformations d'arbres](#org6d4a102)
3.  [Arbre Rouge-Noir](#org60d87dc)
    1.  [Le prédicat `est_arbre_rn`](#org6f5b79e)
    2.  [Insertion d'un élément](#org2d664b6)
    3.  [Suppression](#org72c941d)
4.  [Comparaisons des opérations](#org89e9a19)
    1.  [Comparaison des insertions](#org3409efa)
    2.  [Comparaison des recherches](#org5849247)
    3.  [Comparaison des suppressions](#orgeedd004)



<a id="org43c0c8c"></a>

# Arbres binaires de recherche

Un arbre binaire de recherche est un arbre étiqueté tel que, pour
tout noeud \(v\) de l'arbre :

-   Les éléments de tous les noeuds du sous-arbre gauche de \(v\) sont
    inférieurs ou égaux à l'élément contenu dans \(v\) ;
-   et les éléments de tous les noeuds du sous-arbre droit de \(v\) sont
    supérieurs à l'élément contenu dans \(v\).

**matériel fourni**

Le fichier [binary_tree.py](./src/binary_tree.py) contient la définition d'une classe
`BinaryTree` permettant de manipuler des arbres binaires. Il s'agit
de la classe de base des classes programmées aujourd'hui.

 Elle fournit notamment les méthodes `__eq__`, `__str__` et `show()`
permettant respectivement de 

-   tester l'égalité ;
-   d'obtenir une représentation textuelle ;
-   d'afficher l'arbre sous forme graphique.

Le fichier [abr_squel.py](./src/abr_squel.py) à renommer en `abr.py` contient la définition
(partielle) d'une classe `Abr` permettant de manipuler les arbres
binaire de recherche.

**à faire** Compléter les méthodes :

-   `maximum`, `minimum`, en utilisant uniquement la structure récursive des arbres
    binaires de recherche (c'est-à-dire sans effectuer de comparaisons entre les valeurs
    des noeuds) ;
-   `est_abr` en utilisant `maximum` et `minimum` ;
-   `insere`, `recherche` et `supprime`.

<a id="org6d4a102"></a>

# Transformations d'arbres

Les algorithmes d'équilibrage des arbres rouge-noir sont assez
délicats à implanter. Grossièrement, il s'agit de remplacer un arbre
binaire, par un arbre binaire de recherche davantage équilibré sur la
base d'un coloriage. On se propose ici d'écrire un algorithme
permettant de réécrire une partie d'un arbre.

Ainsi, les règles sur les arbres rouge noir sont formulées 
ainsi :

"(h1:N, (h2:R, (h3:R, g3, d3), d2), d1)" : "(h2:R, (h3:N, g3, d3), (h1:N, d2, d1))"

signifiant qu'un arbre dont la racine est noire et les deux fils et petit-fils
gauche sont rouges est transformé en un arbre de hauteur 1.

On choisit d'écrire les règles en utilisant des expressions
préfixées.  Le problème est d'unifier correctement les variables
d'une expression préfixée avec les noeuds et sous-arbre de l'arbre
lorsque cela est possible.

La classe `BinaryTree` contient des méthodes utiles
permettant :

-   d'unifier un arbre à une expressions prefixes (`unify`) ;
-   de produire des arbres à partir des mêmes expressions. (`from_prefix`).

Par exemple :

```{.python}
>>> VIDE = BinaryTree()
>>> BinaryTree(1, BinaryTree(2, VIDE, VIDE), BinaryTree(3, VIDE, VIDE)).unify("(a, b, c)")
(True, {'a': 1, 'b': (2, (), ()), 'c': (3, (), ())})
```

La méthode `unify` renvoie un couple dont la première composante est
un booléen indiquant si l'unification est possible, la deuxième
composante et la substitutions permettant d'unifier l'arbre avec
l'expression, sous la forme d'un dictionnaire.


<a id="org60d87dc"></a>

# Arbre Rouge-Noir

Les arbres rouge-noir sont des arbres binaires de recherche dont on
colorie les noeuds grace à deux couleurs : Rouge et Noir. Cette
information supplémentaire (pouvant être codé sur un bit par noeud),
combinée à des règles de construction, permettent de garantir le bon
équilibrage de l'arbre.

Par construction, on cherche à obtenir un arbre 

-   dont la racine est en noir ;
-   pour lequel chaque noeud rouge ne peut avoir que des fils noirs ;
-   pour lequel le nombre de noeuds noir sur chaque chemin partant de la
    racine jusqu'aux feuilles est le même.

**matériel fourni** 

Le fichier [arbre_rn_squel.py](./src/arbre_rn_squel.py), à renommer en `arbre_rn.py`, contient la
définition d'une classe `ArbreRN` permettant de manipuler les arbres
rouge-noir.

Les méthodes `unify` et `from_prefix` ont été surchargées pour prendre
en compte les couleurs dans les expressions préfixées.


<a id="org6f5b79e"></a>

## Le prédicat `est_arbre_rn`

**à faire**

On appelle couleurs d'un chemin allant de la racine aux feuilles de
l'arbre, la suite des couleurs rencontrées lorsqu'on le
parcours. Ces couleurs sont représentées par un mot de l'alphabet
`{R,N}` sous forme d'une chaîne de caractères.

Écrire une méthode `couleurs_le_long_des_chemins`
renvoyant une liste de toutes les couleurs de chemins de l'arbre.

**à faire**

Écrire la méthode `est_arbre_rn` renvoyant `True` si, et seulement si, 
l'arbre est un arbre rouge-noir.


<a id="org2d664b6"></a>

## Insertion d'un élément

Pour insérer récursivement un élément dans un arbre rouge noir, on
procède comme dans un arbre binaire de recherche :

-   Si l'arbre est vide, alors on remplace l'arbre par un arbre dont le
    sommet est l'élément, colorié en rouge ;
-   sinon, on insère récursivement dans le sous arbre gauche ou droit ;
-   puis on applique les règles de ré-équilibrage.

Ces règles sont au nombre de 4. Elles sont représentées ci-dessous et
données sous formes d'expressions préfixées dans le fichier
(`REGLES_RN_INSERT`)

![img](./figs/fix_insert_1.png)

![img](./figs/fix_insert_2.png)

![img](./figs/fix_insert_3.png)

![img](./figs/fix_insert_4.png)

Finalement, on colorie la racine de l'arbre en noir.

**à faire** 

Compléter la méthode `insere(elt)` qui renvoie un *nouvel arbre rouge-noir*
contenant les mêmes éléments d'un arbre et l'élément fourni en paramètre.


<a id="org72c941d"></a>

## Suppression

La suppression d'un élément de l'arbre est plus délicate que
l'insertion.

On procède également récursivement. En fait, on ne supprime pas
vraiment les noeuds de l'arbre : on les remplace par les suivants
dans l'ordre des éléments induit par la structure d'arbre.

Finalement, les suppressions effectives se font aux
feuilles de l'arbre.

-   Lorsque l'on supprime une feuille rouge, il n'y a pas de
    problème : les propriétés de l'arbre sont conservées.
-   Par contre, lors de la suppression d'une feuille noire, il faut
    rééquilibrer : à chaque niveau, on cherche alors à rétablir
    l'égalité des nombres de noeuds noirs. Parfois ce rééquilibrage
    n'est pas possible au niveau considéré, et il faut effectuer
    cette opération au niveau supérieur.

Dans tous les cas de déséquilibre, il faut régler les éventuelles
conflits en appliquant les règles d'insertion.

Deux types de règles sont fournies :

-   celles permettant une résolution localement
     (`REGLES_RN_SUPPRIME_GAUCHE_OK` et
    `REGLES_RN_SUPPRIME_DROITE_OK`) ;
-   celles nécessitant un rééquilibrage du niveau supérieur
    (`REGLES_RN_SUPPRIME_GAUCHE_KO` et
    `REGLES_RN_SUPPRIME_DROITE_KO`) ;

**Bonus** 

Écrire la méthode `supprime(elt)` renvoyant un *nouvel arbre
rouge-noir* dans lequel l'élément fourni en paramètre a été
(éventuellement) supprimé.

*Attention* : Cette méthode est très délicate à programmer.


<a id="org89e9a19"></a>

# Comparaisons des opérations

Nous allons maintenant tenter de retrouver expérimentalement les
résultats énoncés en cours.

Ces résultats concernent les opérations :

-   Recherche ;
-   Insertion ;
-   Suppression (si réalisée).

dans les deux types d'arbres binaires.

Avant cela, il faut définir une méthodologie. On suppose que les types
sont munis des opérations ci-dessus, et on cherche à mesurer le temps,
ou le nombre de comparaisons effectuées lors de la recherche, de l'insertion
ou de la suppression dans un arbre créé au hasard.

**à faire**

Écrire un fonction `en_arbre(l, arbre)` prenant en paramètre une
liste `l` et un arbre `arbre` étant soit un Abr, soit un ArbreRN
vide et qui renvoie un nouvel arbre obtenu par insertion des
éléments de la liste dans l'arbre.


<a id="org3409efa"></a>

## Comparaison des insertions

Mesurer les temps d'insertion pour des listes mélangées de tailles
croissantes (comparez avec les mêmes listes).

Réalisez un graphique contenant les deux courbes (insertion dans
les abr et insertion dans les arbres rouge-noir)


<a id="org5849247"></a>

## Comparaison des recherches

Mesurer les temps de recherche d'éléments choisis au hasard dans 
des listes mélangées de tailles croissantes (comparez avec les mêmes
listes et les mêmes éléments).

Réalisez un graphique contenant les deux courbes (recherche dans
les abr et recherche dans les arbres rouge-noir).


<a id="orgeedd004"></a>

## Comparaison des suppressions

Mesurer les temps de suppression d'éléments choisis au hasard dans 
des listes mélangées de tailles croissantes (comparez avec les mêmes
listes et les mêmes éléments).

Réalisez un graphique contenant les deux courbes (recherche dans
les abr et recherche dans les arbres rouge-noir).

