"""
:mod: representation for various tree.

:author: B. Papegay
"""

from criterion import Criterion

DELTA = chr(0x0394)
TREE_CHILD = chr(0x251c)
TREE_CHILD_LAST = chr(0x2514)


class TreeError(Exception):
    """Tree Error error class."""

    def __init__(self, *args):
        """Tree Error constructor."""
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        """Return str error message."""
        if self.message:
            return f'TreeError, {self.message}'
        return 'TreeError has been raised'


class Node:
    """
    Node base class.

    >>> n = Node(1)
    >>> str(n)
    '1'
    >>> Node(1) < Node(2)
    True
    """

    def __init__(self, value):
        """Node constructor."""
        self._value = value

    def content(self):
        """Return node content."""
        return self._value

    def __str__(self):
        """Return node str conversion."""
        return f'{self._value}'

    def __lt__(self, other):
        """Compare two node."""
        return self._value < other._value

    def __eq__(self, other):
        """Return True if self == other."""
        if isinstance(other, Node):
            return self._value == other._value
        return False


class BooleanTreeNode(Node):
    """Boolean Node : node that only contain a boolean value."""

    def __init__(self, value: bool):
        """Node constructor."""
        if not isinstance(value, bool):
            raise TreeError(f'{value} is not a boolean')
        super().__init__(value)

    def __str__(self):
        """Return a string representation of node."""
        return 'T' if self._value else 'F'


class PrefixTreeNode(BooleanTreeNode):
    """A node representing a prefix."""

    def __init__(self, value: bool, prefix: str):
        """Node constructor."""
        super().__init__(value)
        self._prefix = prefix

    def get_prefix(self) -> str:
        """Return prefix of this node."""
        return self._prefix

    def __str__(self):
        """Return a string representation of node."""
        return f"{self._value}-'{self._prefix}'"


class RedBlackNode(Node):
    """
    A colored node.

    >>> n = RedBlackNode(0, 'R')
    >>> n.color()
    'R'
    >>> n.toggle_color()
    >>> n.color()
    'B'
    >>> n.set_color('R')
    >>> n.color()
    'R'
    """

    COLORS = "BR"

    def __init__(self, value: int, color: str):
        """Construct a new red black node."""
        assert color in RedBlackNode.COLORS,\
            "only red or black color are allowed"
        super().__init__(value)
        self._color = color

    def color(self) -> str:
        """Return node color."""
        return self._color

    def set_color(self, new_color: str):
        """Set the color node."""
        assert new_color in RedBlackNode.COLORS,\
            "only red or black color are allowed"
        self._color = new_color

    def toggle_color(self):
        """Toggle node color."""
        col = self.color()
        colors = RedBlackNode.COLORS
        self.set_color(colors[(colors.index(col) + 1)
                              % len(colors)])


class AbstractTree:
    """Abstract Base class for trees."""

    def is_empty(self) -> bool:
        """Return True iff self is an empty tree."""
        return len(self._content) == 0

    def root(self) -> Node:
        """Return root node."""
        if len(self._content) == 0:
            raise TreeError()
        return self._content[0]

    def subtrees(self) -> tuple["AbstractTree"]:
        """Return root childs."""
        if len(self._content) == 0:
            raise TreeError()
        return tuple(self._content[1])

    def is_leaf(self) -> bool:
        """Return True iff the tree is a leaf."""
        return not self.is_empty() and \
            all(subtree.is_empty() for subtree in self.subtrees())

    def __getitem__(self, i: int) -> "AbstractTree":
        """Return a child."""
        if not self.is_empty():
            if 0 <= i and i < len(self.subtrees()):
                return self.subtrees()[i]
            else:
                raise TreeError('no such child')
        else:
            raise TreeError('tree is empty')

    def __str__(self, prefix: str = ''):
        """Return string representation of tree."""
        if self.is_empty():
            return prefix + DELTA
        else:
            node = self.root()
            subtrees = self.subtrees()
            base_prefix = prefix.replace(TREE_CHILD, '|')\
                                .replace(TREE_CHILD_LAST, ' ')
            if base_prefix == '':
                base_prefix = '\n'
            child_prefix = base_prefix + TREE_CHILD
            child_prefix_last = base_prefix + TREE_CHILD_LAST
            str_child = ""
            for i in range(len(subtrees)):
                str_child += subtrees[i].__str__(child_prefix
                                                 if i != len(subtrees) - 1
                                                 else child_prefix_last)
            return "{}{}{}".format(prefix, node, str_child)

    def __len__(self) -> int:
        """Return tree size."""
        if self.is_empty():
            return 0
        else:
            return 1 + sum(len(ssa) for ssa in self.subtrees())

    def height(self) -> int:
        """Return tree height."""
        if self.is_empty():
            return -1
        elif len(self.subtrees()) == 0:
            return 0
        else:
            return 1 + max(ssa.height() for ssa in self.subtrees())

    def __repr__(self):
        """Get a representation of a tree."""
        return str(self)

    def __eq__(self, o):
        """Return True iff self == o."""
        if isinstance(o, AbstractTree):
            if self.is_empty():
                return o.is_empty()
            else:
                if not o.is_empty():
                    sst = self.subtrees()
                    ost = self.subtrees()
                    return self.root() == o.root() and \
                        len(sst) == len(ost) and \
                        all(sst[i] == ost[i] for i in range(len(sst)))
                else:
                    return False
        else:
            return False


class Tree(AbstractTree):
    """
    Base class for mutable trees.

    >>> t = Tree()
    >>> len(t)
    0
    >>> t1 = Tree(Node(1), (t,))
    >>> t1.is_leaf()
    True
    >>> t2 = Tree(Node(2), (t1, Tree()))
    >>> t2.is_leaf()
    False
    >>> len(t2)
    2
    >>> t2.height()
    1
    >>> t2.append(Tree(Node(3), (Tree(Node(4), ()), Tree())))
    >>> len(t2)
    4
    """

    def __init__(self, *args):
        """Tree constructor."""
        if len(args) == 0:
            self._content = ()
        elif len(args) == 2:
            if not isinstance(args[0], Node):
                node = Node(args[0])
            else:
                node = args[0]
            try:
                if not all(isinstance(ss_arbre, AbstractTree)
                           for ss_arbre in args[1]):
                    raise TreeError(f'{args[1]} is not iterable of trees')
            except TypeError:
                raise TreeError(f'{args[1]} is not iterable')
            self._content = (node, list(args[1]))
            self._labels = {}
        else:
            raise TreeError('bad number of argument')

    def append(self, atree: AbstractTree, label: str = ""):
        """Append a new child."""
        if len(self._content) == 0:
            raise TreeError()
        st_count = len(self.subtrees())
        self._content[1].append(atree)
        self._labels[st_count] = label if label != "" else str(st_count)

    def __setitem__(self, i, child: AbstractTree):
        """Set a child."""
        if not self.is_empty():
            if 0 <= i < len(self.subtrees()):
                self._content[1][i] = child
            else:
                raise TreeError('no such child')
        else:
            raise TreeError('tree is empty')

    def set_labels(self, labels: dict):
        """Modify labels of a node."""
        self._labels.update(labels)

    def append_bfs(self, x: Node):
        """
        Append a node to the first available place in bfs order.

        :param x: (Node) a node
        """
        resq = []
        resq.append(self)
        fini = False
        while resq and not fini:
            current_node = resq.pop(0)
            if current_node.is_empty():
                fini = True
                current_node._content = (x, [])
            else:
                subtrees = self.subtrees()
                if len(subtrees) == 0:
                    subtrees.append(AbstractTree(x, []))
                else:
                    for st in self.subtrees():
                        resq.append(st)


class BinaryTree(AbstractTree):
    """
    A class representing binary tree.

    >>> VIDE = BinaryTree()
    >>> VIDE.is_empty()
    True
    >>> f = BinaryTree(Node(1), VIDE, VIDE)
    >>> f.is_leaf()
    True
    >>> t = BinaryTree(Node(2), f, BinaryTree(Node(3), VIDE, VIDE))
    >>> len(t)
    3
    >>> t.height()
    1
    >>> t.left() is f
    True
    >>> t.right() == BinaryTree(Node(3), VIDE, VIDE)
    True
    """

    def __init__(self, *args):
        """Binary tree constructor."""
        if len(args) == 0:
            self._content = ()
        elif len(args) == 3:
            if isinstance(args[0], Node):
                node = args[0]
            else:
                node = Node(args[0])
            if not all(isinstance(a, BinaryTree) for a in args[1:]):
                raise TreeError('second and third argument are not trees')
            self._content = (node, [args[1], args[2]])
        else:
            raise TreeError('0 or 2 arguments for BinaryTree')

    @staticmethod
    def leaf(node: Node) -> "BinaryTree":
        """
        Return a new leaf.

        :param n: (Node) a node
        :return: (Tree) a leaf
        """
        return BinaryTree(node, BinaryTree(), BinaryTree())

    def left(self) -> "BinaryTree":
        """Return the sub left tree."""
        if len(self._content) == 0:
            raise TreeError()
        left, _ = self._content[1]
        return left

    def right(self) -> "BinaryTree":
        """Return the sub right tree."""
        if len(self._content) == 0:
            raise TreeError()
        _, right = self._content[1]
        return right


class DecisionTree(BinaryTree):
    """
    A decision tree.

    >>> a = DecisionTree()
    """

    def __init__(self, *args):
        """Decision tree constructor."""
        super().__init__(*args)
        if len(args) == 3:
            if not self.is_leaf():
                if not isinstance(self.root().content(),
                                  Criterion):
                    raise TreeError('internal nodes must be conditions')

    def decision(self, datas: dict):
        """Get the decision from a decision tree and datas."""
        current_node = self
        while not current_node.is_leaf() and not current_node.is_empty():
            crit = current_node.root().content()
            current_node = current_node.right() if crit.accept(datas)\
                else current_node.left()
        return current_node.root() if current_node.is_leaf() else None


class TreeDecorator(AbstractTree):
    """A decorable tree."""

    def __init__(self, *args):
        """Construct a new decorable tree."""
        if len(args) == 1 and isinstance(args[0], AbstractTree):
            self.set_tree(args[0])
        else:
            self.set_tree(Tree(*args))

    def set_tree(self, atree: AbstractTree):
        """Set tree."""
        self._tree = atree

    def get_tree(self) -> AbstractTree:
        """Return the tree."""
        return self._tree

    def is_empty(self) -> bool:
        """Return True iff self is an empty tree."""
        return self.get_tree().is_empty()

    def root(self) -> Node:
        """Return root node."""
        return self.get_tree().root()

    def subtrees(self) -> tuple:
        """Return root childs."""
        return self.get_tree().subtrees()

    def is_leaf(self) -> bool:
        """Return True iff the tree is a leaf."""
        return self.get_tree().is_leaf()

    def __len__(self) -> int:
        """Return tree size."""
        return len(self.get_tree())

    def __getitem__(self, i: int) -> "AbstractTree":
        """Return a child."""
        return self.get_tree()[i]

    def __setitem__(self, i: int, new_tree: AbstractTree):
        """Set child."""
        self.get_tree()[i] = new_tree

    def height(self) -> int:
        """Return tree height."""
        return self.get_tree().height()


def main():
    """Run tests."""
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE |
                    doctest.ELLIPSIS, verbose=True)


if __name__ == '__main__':
    main()
    t = Tree()
    t1 = Tree(Node(1), (t, ))
    t2 = Tree(Node(2), (Tree(), Tree()))
    t1.append(t2)
    print(t, t1, t2, sep='\n')
