"""A module used to embed latex tikz code in application."""

from os import getcwd, chdir, environ, pathsep
import subprocess
import hashlib
import sys
import os


class AbstractTikZFigure:
    """Abstract base class to be inherited by every TikZ exportable object."""

    def __init__(self, env_needed: bool = False):
        """Construct a new tikz exportable object."""
        self._env_is_needed = env_needed

    def to_tikz(self, **optns) -> str:
        """Return a tikz representation of the object."""
        pass

    def tikz_env_needed(self) -> bool:
        """Return if whenever a tikz environment is needed."""
        return self._env_is_needed


class TikZ:
    """A class embedding tikz code."""

    def __init__(self, code: str,
                 **optns):
        """Construct a new tikz code."""
        try:
            packages = optns['packages']
        except KeyError:
            packages = []
        try:
            optn = optns['tikz_optn']
        except KeyError:
            optn = None
        try:
            optn = optns['env']
        except KeyError:
            env = True
        if isinstance(code, AbstractTikZFigure):
            self._code = code.to_tikz()
            self._code_env = code.tikz_env_needed()
        elif isinstance(code, str):
            self._code = code
            self._code_env = env
        else:
            self._code = f"""
\\draw (0,0) node {{ {str(code)} }};
"""
            self._code_env = env
        try:
            self._filename = "".join(optns['filename'].split('.')[:-1])
            self._keep = True
        except KeyError:
            self._filename = hashlib.sha1(self._code.encode()).hexdigest()
            self._keep = False
        tex = []
        tex.append("""
\\documentclass[tikz, convert={
        outext=.png,
        convertexe={pdftoppm},
        command=\\unexpanded{\\convertexe\\space \\infile\\space -png > \\outfile}}]{standalone}
""")
        if optn:
            tex.append(f"\\usepackage{{tikz}}{{ {optn} }}")
        else:
            tex.append("\\usepackage{tikz}")
        for package in packages:
            tex.append(f'\\usepackage{{ {package} }}')
        tex.append("\\usepackage{forest}")
        tex.append("\\usetikzlibrary{arrows,matrix, positioning,fit}")
        tex.append("\\usetikzlibrary{backgrounds}")
        tex.append("\\usetikzlibrary{shapes.geometric}")
        tex.append("\\begin{document}")
        if self._code_env:
            tex.append("""
\\begin{tikzpicture}[show background rectangle,
            background rectangle/.style={fill=white}]
""")
        tex.append(self._code)
        if self._code_env:
            tex.append("\\end{tikzpicture}")
        tex.append("\\end{document}")
        self._latex = "\n".join(tex)
        self._run_latex()

    def _run_latex(self, encoding: str = 'utf-8'):
        """Run latex."""
        with open(f'{self._filename}.tex', 'w', encoding=encoding) as logfile:
            logfile.write(self._latex)
        current_dir = getcwd()

        ret_log = False
        log = None

        # Set the TEXINPUTS environment variable, which allows the tikz code
        # to refence files relative to the notebook (includes, packages, ...)
        env = environ.copy()
        if 'TEXINPUTS' in env:
            env['TEXINPUTS'] = current_dir + pathsep + env['TEXINPUTS']
        else:
            env['TEXINPUTS'] = '.' + pathsep + current_dir + pathsep*2
            # note that the trailing double pathsep will insert the standard
            # search path (otherwise we would lose access to all packages)

        try:
            LATEX_CMD = "yes x | pdflatex -interaction=errorstopmode \
            --shell-escape {}.tex"
            retcode = subprocess.call(LATEX_CMD.format(self.filename()),
                                      stdout=subprocess.DEVNULL,
                                      stderr=subprocess.DEVNULL,
                                      shell=True,
                                      env=env)
            if retcode != 0:
                print("LaTeX terminated with signal",
                      -retcode,
                      file=sys.stderr)
                ret_log = True
        except OSError as err:
            print("LaTeX execution failed:", err, file=sys.stderr)
            ret_log = True
        # in case of error return LaTeX log
        if ret_log:
            try:
                logfile = open(f'{self._filename}.log', 'r')
                log = logfile.read()
                logfile.close()
            except IOError:
                print("No log file generated.", file=sys.stderr)
        else:
            try:
                os.remove(f"{self._filename}.tex")
                os.remove(f"{self._filename}.pdf")
                os.remove(f"{self._filename}.log")
                os.remove(f"{self._filename}.aux")
            except OSError as err:
                log = err.strerror
        chdir(current_dir)
        return log

    def _repr_png_(self):
        """Return an open file containing png image."""
        return open(self._filename + '.png', 'rb').read()

    def __repr__(self):
        """Return a string representation of a code."""
        return f"TikZ({self._code})"

    def filename(self) -> str:
        """Return code base filename."""
        return self._filename

    def file_content(self) -> str:
        """Return file content (tikz code)."""
        return self._latex

    def __del__(self):
        """Clean the mess."""
        try:
            os.remove(f"{self._filename}.tex")
            os.remove(f"{self._filename}.log")
            os.remove(f"{self._filename}.aux")
            os.remove(f"{self._filename}.pdf")
        except OSError:
            pass
        if not self._keep:
            try:
                os.remove(f"{self._filename}.png")
            except OSError:
                pass


if __name__ == "__main__":
    import doctest
    doctest.testmode()
