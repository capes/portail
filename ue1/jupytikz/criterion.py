"""
:mod: un module implantant des conditions booléennes.

:author: B. Papegay
"""


class Criterion:
    """Criterion abstract class."""

    def __init__(self, att: str, value: float):
        """Create a new criterion."""
        self._att = att
        self._value = value

    def accept(self, obj: dict) -> bool:
        """Return True iff obj satisfied the criterion."""
        ...


class EqualCriterion(Criterion):
    """Criterion representing an equality."""

    def accept(self, obj: dict) -> bool:
        """Return True iff attribute is equal to value."""
        return obj[self._att] == self._value

    def __str__(self):
        """Return string representation of object."""
        return f"{self._att} == {self._value}"


class GreaterThanCriterion(Criterion):
    """Criterion representing an inequality."""

    def accept(self, obj: dict) -> bool:
        """Return True iff obj satisfy the criterion."""
        return obj[self._att] > self._value

    def __str__(self):
        """Return string representation of the criterion."""
        return f"{self._att} > {self._value}"


class LessThanCriterion(Criterion):
    """Criterion representing an inequality."""

    def accept(self, obj: dict) -> bool:
        """Return True iff obj satisfy the criterion."""
        return obj[self._att] < self._value

    def __str__(self):
        """Return string representation of the criterion."""
        return f"{self._att} < {self._value}"
