"""
Une classe permettant de décorer des arbres.
"""

from tree import AbstractTree, Tree, Node
from tikz import AbstractTikZFigure
from dot import AbstractDotFigure, WHITE, BLACK, escape_str
import time


class TreeDecorator(AbstractTree):
    """A decorable tree."""

    def __init__(self, *args):
        """Construct a new decorable tree."""
        if len(args) == 1 and isinstance(args[0], AbstractTree):
            self.set_tree(args[0])
        else:
            self.set_tree(Tree(*args))

    def set_tree(self, atree: AbstractTree):
        """Set tree."""
        self._tree = atree

    def get_tree(self) -> AbstractTree:
        """Return the tree."""
        return self._tree

    def is_empty(self) -> bool:
        """Return True iff self is an empty tree."""
        return self.get_tree().is_empty()

    def root(self) -> Node:
        """Return root node."""
        return self.get_tree().root()

    def subtrees(self) -> tuple:
        """Return root childs."""
        return self.get_tree().subtrees()

    def is_leaf(self) -> bool:
        """Return True iff the tree is a leaf."""
        return self.get_tree().is_leaf()

    def __len__(self) -> int:
        """Return tree size."""
        return len(self.get_tree())

    def __getitem__(self, i: int) -> "AbstractTree":
        """Return a child."""
        return self.get_tree()[i]

    def __setitem__(self, i: int, new_tree: AbstractTree):
        """Set child."""
        self.get_tree()[i] = new_tree

    def height(self) -> int:
        """Return tree height."""
        return self.get_tree().height()


class TikZTree(TreeDecorator, AbstractTikZFigure):
    """A Drawable tree."""

    def __init__(self, *args):
        """Construct a tikz tree."""
        super().__init__(*args)
        AbstractTikZFigure.__init__(self, False)

    def _forest_node(node: str,
                     node_label: str,
                     st: str,
                     **optns) -> str:
        """Help function to build a node."""
        if 'draw_edge_label' in optns:
            draw_edge_label = optns['draw_edge_label']
        else:
            draw_edge_label = True
        if draw_edge_label:
            return """[ {{\\small {}}},
            edge label={{node[midway,fill=white,font=\\tiny]{{ {} }} }}
            {}]""".format(node, node_label, st)
        else:
            return "[ {{\\small {}}} {}]".format(node, st)

    def _forest_helper(self,
                       edge_label: str,
                       **optns) -> str:
        """Help function to build a forest."""
        if self.is_empty():
            res = "[ ,phantom ]"
        else:
            node = str(self.root())
            if self.is_leaf():
                res = TikZTree._forest_node(node, edge_label, "",
                                            **optns)
            else:
                l_sttext = []
                i = 0
                for st in self.subtrees():
                    l_sttext.append(TikZTree._forest_helper(st,
                                                            str(i),
                                                            **optns))
                    i += 1
                res = TikZTree._forest_node(node,
                                            edge_label,
                                            "\n".join(l_sttext),
                                            **optns)
        return res

    def to_tikz(self, **optns) -> str:
        """Return a tikz string representation of the tree."""
        forest_param = optns['forest_param'] \
            if 'forest_param' in optns else ''
        tikz_code = """ \\begin{{forest}}
 for tree={{%
 s sep=.5cm,
 {} }} %
 {}
 \\end{{forest}}"""
        return tikz_code.format(forest_param,
                                self._forest_helper('', **optns))


class DotTree(TreeDecorator, AbstractDotFigure):
    """A dot drawable tree."""

    def __init__(self, *args):
        """Construct a new dot tree."""
        super().__init__(*args)

    def to_dot(self, background_color=WHITE) -> str:
        """Renvoie  la description au format dot de self."""
        LIEN = '\t"N({})" -> "N({})" [color="{}", label="{}", fontsize="8"];\n'
        INV_NODE = '\t"N({})" [color="{}", label=""];\n'
        NODE = '\t"N({})" [label="{}"];\n'

        def aux(arbre, prefix=''):
            if arbre.is_empty():
                descr = INV_NODE.format(prefix, background_color)
            else:
                c = arbre.root()
                descr = NODE.format(prefix, escape_str(c))
                i = 0
                for st in arbre.subtrees():
                    label = str(i)
                    if st.is_empty():
                        llabel = ""
                        couleur = background_color
                    else:
                        llabel = label
                        couleur = BLACK
                    descr += aux(st, prefix + label)
                    descr += LIEN.format(prefix,
                                         prefix + label,
                                         couleur,
                                         llabel)
                    i += 1
            return descr

        return """/*
  Binary Tree

  Date: {}

*/

digraph G {{
\tbgcolor="{:s}";

{:s}
}}
""".format(time.strftime('%c'), background_color, aux(self))


if __name__ == "__main__":
    import doctest
    doctest.testmod()

    t = Tree()
    t1 = Tree(Node(1), (Tree(Node(0), ()), ))
    t2 = Tree(Node(2), (Tree(), Tree()))
    t1.append(t2)
    d = DotTree(t1)
