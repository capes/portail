# Securisation des communications

## Une activité élève

-   [Activité élèveDéroulé](./activite.md)


## Cryptosystèmes

- [attaque de Vigenere](./attaque_vigenere.md)
- [attaque de Enigma](./enigma.md)


## Les certificats

-   [source du diaporama](./certificat.md)
-   à compiler avec :
```sh
pandoc -s -t revealjs -V theme=beige -o certificat.html certificat.md
```


## Les documents présentés en cours 

- [cryptographie militaire](./crypto_militaire_1.pdf)
- [rsa](./Rsapaper.pdf)



## Compléments 

### La TLS

-   [source du diaporama disponible](./tls.md)
-   à compiler avec :
```sh
pandoc -s -t revealjs -V theme=beige -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/ -o tls.html tls.md
```

### Analyse d'une session de connexion

-   [video présentée](http://www.fil.univ-lille1.fr/~papegay/diu/securisation/wireshark.ogv).



### Travaux pratiques

-   [autour de Fermat (calepin jupyter)](./autour_fermat.ipynb)
-   [chiffrement rsa (calepin jupyter)](./rsa2.ipynb)
-   [attaque de Enigma](./enigma.md)
-   [Utilisation de openssl](./openssl.md)

