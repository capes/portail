
# Table des matières

1.  [Algèbre de Boole](#org972bd09)
    1.  [Définition](#org9c4a97b)
        1.  [Mathématique / Type abstrait](#org1a01600)
        2.  [Langage de programmation](#org8bf4fec)
        3.  [Représentation en mémoire physique](#orgf26a199)
2.  [Fonctions booléennes](#orgfc0b33b)
    1.  [Définition](#org9e2cfba)
    2.  [Circuits logiques](#org67ebb02)
    3.  [Tables de vérité](#orge032508)
    4.  [Formes normales](#org6372632)
    5.  [Une notation pour les formes normales](#org336d39c)
    6.  [Passage aux formes normales](#org2cf980c)
        1.  [à partir d'une expression](#org9ace7c2)
        2.  [à partir de la table de vérité](#orgc4fcd63)
    7.  [Méthode des tableaux de Karnaugh](#orgfcf3b4b)
        1.  [Principes généraux](#org13f9544)
        2.  [Construction du tableau](#orgd3ca6e8)
        3.  [Identification des blocs](#orgdf8ecc5)
        4.  [Détermination de l'expression](#orgd66ffd1)



<a id="org972bd09"></a>

# Algèbre de Boole

L'algèbre de boole est la théorie mathématique dont l'objet est une
approche algébrique de la logique : On définit des opérations et des
fonctions sur des variables logiques.

Elle fut inventée par le mathématicien **George Boole**
(*1815-1863*). Aujourd'hui l'algèbre de Boole trouve de nombreuses
applications en informatique et dans la conception des circuits
électroniques, notamment par **Claude Shannon** (*1916-2001*).


<a id="org9c4a97b"></a>

## Définition


<a id="org1a01600"></a>

### Mathématique / Type abstrait

On appelle ensemble des booléens ou type booléen un ensemble
constitué de deux éléments appelés **valeurs de vérité** et muni des
opérations **non**, **et** et **ou** qui vérifient certaines propriétés
(qui font de cet ensemble une algèbre). On le note \(\matcal B\).

Avant de donner les propriétés, voici les notations couramment utilisées :

-   Les éléments de \(\mathcal B\) sont notés \(\lbrace \mbox{VRAI, FAUX}
          \rbrace\) ou \(\lbrace \mbox{False, True}\rbrace\), ou \(\lbrace
          \bot, \top,\rbrace\) ou encore \(\lbrace 0, 1\rbrace\).
-   Les opérations admettent également différentes notations :
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Français</th>
    <th scope="col" class="org-left">Logique</th>
    <th scope="col" class="org-left">Python</th>
    <th scope="col" class="org-left">Electronique</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">\(\lbrace \mbox{V,F}\rbrace\)</td>
    <td class="org-left">\(\lbrace\bot,\top\rbrace\)</td>
    <td class="org-left">True, False</td>
    <td class="org-left">\(\lbrace 0, 1\rbrace\)</td>
    </tr>
    
    
    <tr>
    <td class="org-left">NON</td>
    <td class="org-left">\(\lnot\)</td>
    <td class="org-left">not</td>
    <td class="org-left">\(\overline{a}\)</td>
    </tr>
    
    
    <tr>
    <td class="org-left">ET</td>
    <td class="org-left">\(\land\)</td>
    <td class="org-left">and</td>
    <td class="org-left">\(\cdot\)</td>
    </tr>
    
    
    <tr>
    <td class="org-left">OU</td>
    <td class="org-left">\(\lor\)</td>
    <td class="org-left">or</td>
    <td class="org-left">\(+\)</td>
    </tr>
    </tbody>
    </table>

Dans ce cours, nous utiliserons la notation \((\lbrace 1, 0\rbrace, \overline{x}, \land, \lor)\). 

On définit les opérations par leur **table de vérité**:

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">a</th>
<th scope="col" class="org-left">\(0\)</th>
<th scope="col" class="org-left">\(1\)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">\(\overline{a}\)</td>
<td class="org-left">\(1\)</td>
<td class="org-left">\(0\)</td>
</tr>
</tbody>
</table>

\(a\lor b\)

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">a\b</th>
<th scope="col" class="org-left">\(0\)</th>
<th scope="col" class="org-left">\(1\)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">\(0\)</td>
<td class="org-left">\(0\)</td>
<td class="org-left">\(1\)</td>
</tr>


<tr>
<td class="org-left">\(1\)</td>
<td class="org-left">\(1\)</td>
<td class="org-left">\(1\)</td>
</tr>
</tbody>
</table>

\(a\land b\)

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">a\b</th>
<th scope="col" class="org-left">\(0\)</th>
<th scope="col" class="org-left">\(1\)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">\(0\)</td>
<td class="org-left">\(0\)</td>
<td class="org-left">\(0\)</td>
</tr>


<tr>
<td class="org-left">\(1\)</td>
<td class="org-left">\(0\)</td>
<td class="org-left">\(1\)</td>
</tr>
</tbody>
</table>

1.  Propriétés

    -   Éléments neutres 
        \(a \lor 0 = a\) et \(a \land 1 = a\)
    
    -   Associativité :
        
        \(a \lor (b \lor c) = (a \lor b) \lor c\)
        
        \(a \land (b \land c) = (a \land b) \land c\)
    
    -   Commutativité :
        
        \(a \lor b = b \lor a\)  et \(a \land b = b \land a\)
    
    -   Idempotence 
        
        \(a \lor a = a\)
        
        \(a \land a = a\)
    
    -   Double négation
        \(\overline{\overline{a}} = a\)
    
    -   Lois de **De Morgan**
        
        \(\overline{ a \lor b } = \overline{a} \land \overline{b}\) 
        
        \(\overline{ a \land b } = \overline{a} \lor \overline{b}\)
    
    -   Distributivités
        \(a \land (b \lor c) = (a \land b) \lor (a \land c)\)
        
        \(a \lor (b \land c) = (a \lor b) \land (a \lor c)\)
    
    -   Optimisations :
        
        \(a \lor (\overline{a}\land b) = a\lor b\) 
        
        \(a \lor (b\land c) = (a\lor b)\land(a\lor c)\)


<a id="org8bf4fec"></a>

### Langage de programmation

1.  Statut

    Dans les langages de programmation, le statut des booléens est
    très variable. En voici quelques exemples :
    
    1.  En C
    
        -   Pas de type spécifique
        -   Représenté par des nombres : un nombre non nul est
            vrai, un nombre nul est faux
    
    2.  En Python
    
        -   Type spécifique (`Bool`)
        -   Cast implicite des expressions vers le type booléen :
            -   tout ce qui est non nul ou non vide vaut `True`
            -   tout ce qui est nul ou vide vaut `False`.
    
    3.  En Haskel
    
        -   Type spécifique
        -   Pas de cast implicite vers les booléens

2.  Évaluation

    L'aspect opérationnel est également à prendre en compte : Comment
    sont **évaluées** les expressions booléennes ?
    Souvent, il faut tenir compte de la **séquentialité** des opérateurs.
    
    Par exemple, lors de l'évaluation d'une expression du type `exp1 or
       exp2` par l'interpréteur python `exp2` ne sera évaluée que si le
    résultat de l'évaluation de `exp1` aboutit à `False`.
    
    De même, lors de l'évaluation d'une expression du type `exp1 and
       exp2` par l'interpréteur python `exp2` ne sera évaluée que si le
    résultat de l'évaluation de `exp1` aboutit à `True`.
    
    De fait, il y a une différence entre le modèle abstrait et sa
    traduction dans les langages de programmation. Par exemple, les
    opérateurs `and` et `or` de python ne sont plus commutatifs, on dit
    qu'ils sont paresseux.
    
    **Exercice :** Donner des exemples de situations dans lesquelles la
    non commutativité des opérateurs booléens est exploitée.


<a id="orgf26a199"></a>

### Représentation en mémoire physique

On peut représenter les booléen dans la mémoire de l'ordinateur
par un bit (binary digit, chiffre binaire).


<a id="orgfc0b33b"></a>

# Fonctions booléennes


<a id="org9e2cfba"></a>

## Définition

Soit \(p\geq 0\). Une fonction booléenne \(f\) est une fonction de
\(\mathcal B^p\) dans \(\mathcal B\).

\(p\) est l'arité de la fonction \(f\).

Les fonctions booléennes peuvent être définies par :

-   une expression de ses variables (expression booléenne)
-   une table de vérité.

À une expression donnée, il existe une et une seule fonction
définie par l'expression. Par contre, à une fonction \(f\) donnée,
il existe une infinité d'expressions correspondant à \(f\).

Par exemple, la fonction \(f\) définie par
\(f(a, b) = a \lor \non{b}\)
peut également être définie par l'expression 
\((a\land b)\lor(a\land\overline{b})\lor(\overline{a}\land\overline{b})\)

**Exemple :** \(f(a, b)= a\land \overline{b}\)

**Exercice :** Combien y a-t-il de fonctions booléennes d'arité \(p\) ?


<a id="org67ebb02"></a>

## Circuits logiques

Il y a une correspondance biunivoque entre les expressions
booléennes utilisant \(p\) variables différentes et les circuits à
\(p\) entrées et une sortie utilisant les portes ET, OU et NON.


<a id="orge032508"></a>

## Tables de vérité

-   **Pté** une fonction logique est entièrement déterminée par sa
    table de vérité.
-   deux fonctions logiques \(f\) et \(g\) sont égales si :
    -   on peut montrer en utilisant certaines propriétés qu'elles
        peuvent être définies par la même expression ;
    -   leurs tables de vérité sont identiques.


<a id="org6372632"></a>

## Formes normales

**Définition :** Un *minterme* des variables \((a_i)\) est une
conjonction de termes dans lequel chaque \(a_i\) apparait une et une
seule fois sous forme positive (\(a_i\)) ou négative (\(\overline{a_i}\)).

**Exemple de minterme** des deux variables \(a\) et \(b\) : \(a\land b\), \(a\land\overline{b}\), &#x2026;

**Exercice :** combien de mintermes à deux variables (à l'ordre près
des variables) , à \(p\) variables ?

**Définition :** Un *maxterme* des variables \((a_i)\) est une
disjonction de termes dans lequel chaque \(a_i\) apparait une et une
seule fois sous forme positive (\(a_i\)) ou négative (\(\overline{a_i}\)).

**Définition :** Une expression est en forme normale disjonctive si
 elle est sous forme 

-   d'une disjonction de mintermes;
-   d'une somme de produits

**Définition :** Une expression est en forme normale conjonctive si
 elle est sous forme 

-   d'une conjonction de maxtermes;
-   d'un produit de sommes

**Théorème :** Toute fonction booléenne \(f\) peut être représentée par
une forme normale disjonctive et par une forme normale conjonctive
(à l'ordre près des termes et des facteurs).

**Exercice :** Déterminer les formes normales de la fonction \(f\)
définie par sa table :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">a</th>
<th scope="col" class="org-right">b</th>
<th scope="col" class="org-right">c</th>
<th scope="col" class="org-right">\(f(a, b, c)\)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>
</tbody>
</table>

**Exercice :** Donner une utilité de la forme normale conjonctive.


<a id="org336d39c"></a>

## Une notation pour les formes normales

Soit \(p\) l'arité d'une fonction \(f\). On choisit un ordre sur les
\(p\) variables.

Soit \(k\) un entier compris entre 0 et \(2^p-1\). On
note \(m_k\) le minterme dans lequel la $i$-ième variable est 

-   positive si le $i$-ieme bit de l'écriture binaire de \(k\) est à 1
-   négative si le $i$-ieme bit de l'écriture binaire de \(k\) est à 0

Par exemple, sur 3 variables \(a, b\) et \(c\), on a :

-   \(m_0 = \overline{a}\land\overline{b}\land\overline{c}\)
-   \(m_1 = \overline{a}\land\overline{b}\land c\)
-   \(m_2 = \overline{a}\land b\land \overline{c}\)
-   &#x2026;

Réciproquement, pour chaque minterme \(m\), il existe un entier \(k\)
compris entre 0 et \(2^p-1\) tel que \(m=m_k\).

Une forme normale disjonctive s'écrit alors 
\(m_{i_1}\lor m_{i_2}\lor \ldots \lor m_{i_l}\). 
On la note \(\Sigma(i_1, i_2, \ldots , i_l)\).

Soit \(k\) un entier compris entre 0 et \(2^p-1\). On
note \(M_k\) le maxterme dans lequel la $i$-ième variable est 

-   positive si le $i$-ieme bit de l'écriture binaire de \(k\) est à 1
-   négative si le $i$-ieme bit de l'écriture binaire de \(k\) est à 0

Par exemple, sur 3 variables \(a, b\) et \(c\), on a :

-   \(M_0 = \overline{a}\lor\overline{b}\lor\overline{c}\)
-   \(M_1 = \overline{a}\lor\overline{b}\lor c\)
-   \(M_2 = \overline{a}\lor b\land \overline{c}\)
-   &#x2026;

Réciproquement, pour chaque maxterme \(M\), il existe un entier \(k\)
compris entre 0 et \(2^p-1\) tel que \(M=M_k\).

Une forme normale conjonctive s'écrit alors 
\(M_{i_1}\land M_{i_2}\land \ldots \land M_{i_l}\).
On la note \(\Pi(i_1, i_2, \ldots , i_l)\).


<a id="org2cf980c"></a>

## Passage aux formes normales


<a id="org9ace7c2"></a>

### à partir d'une expression

1.  Forme normale disjonctive

    -   Partir de la fonction et la transformer pour faire apparaître
        des mintermes ou des maxtermes.
    -   On utilise les propriétés de l'algèbre de Boole , notamment
        l'invariant \(a\lor \overline{a} = 1\).
    
    **Exemple :** Soit \(f(a, b, c) = ab + \overline{b}c+a\overline{c}\)

2.  Forme normale conjonctive

    -   On détermine la forme normale disjonctive de \(\overline{f}\)
    -   On utilise les lois de De Morgan pour en déduire la forme
        normale conjonctive de \(f\).


<a id="orgc4fcd63"></a>

### à partir de la table de vérité

1.  Forme normale disjonctive

    -   Pour chaque valeur des variables \(x_i\) pour lesquelles \(f(x_i)=1\), 
        on définit un minterme dans lequel chaque variable \(x_i\) est :
        -   positive si sa valeur est \(1\),
        -   négative si sa valeur est \(0\).
    -   La forme normale de \(f(x_i)\) est la disjonction de ces mintermes.

2.  Forme normale conjonctive

    -   Pour chaque valeurs des variables \(x_i\) pour lesquelles \(f(x_i)=0\), 
        on définit un maxterme dans lequel chaque variable \(x_i\) est :
        -   positive si sa valeur est \(1\),
        -   négative si sa valeur est \(0\).
    -   La disjonction de ces mintermes est \(\overline{f(x_i)}}\).
    
    **Exercice :** Déterminer les formes normales conjonctives et
    disjonctives de la fonction \(f\) d'arité 3 définie par la table 
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-right" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-right">a</th>
    <th scope="col" class="org-right">b</th>
    <th scope="col" class="org-right">c</th>
    <th scope="col" class="org-right">f(a,b,c)</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-right">0</td>
    <td class="org-right">0</td>
    <td class="org-right">0</td>
    <td class="org-right">0</td>
    </tr>
    
    
    <tr>
    <td class="org-right">0</td>
    <td class="org-right">0</td>
    <td class="org-right">1</td>
    <td class="org-right">1</td>
    </tr>
    
    
    <tr>
    <td class="org-right">0</td>
    <td class="org-right">1</td>
    <td class="org-right">0</td>
    <td class="org-right">0</td>
    </tr>
    
    
    <tr>
    <td class="org-right">0</td>
    <td class="org-right">1</td>
    <td class="org-right">1</td>
    <td class="org-right">0</td>
    </tr>
    
    
    <tr>
    <td class="org-right">1</td>
    <td class="org-right">0</td>
    <td class="org-right">0</td>
    <td class="org-right">1</td>
    </tr>
    
    
    <tr>
    <td class="org-right">1</td>
    <td class="org-right">0</td>
    <td class="org-right">1</td>
    <td class="org-right">1</td>
    </tr>
    
    
    <tr>
    <td class="org-right">1</td>
    <td class="org-right">1</td>
    <td class="org-right">0</td>
    <td class="org-right">1</td>
    </tr>
    
    
    <tr>
    <td class="org-right">1</td>
    <td class="org-right">1</td>
    <td class="org-right">1</td>
    <td class="org-right">1</td>
    </tr>
    </tbody>
    </table>
    
    **Exercice :** Déterminer les formes normales conjonctives et
     disjonctives de la fonction \(f\) d'arité 3 définie par l'expression
     \(f(a,b,c) = (\overline{a}\land b)\lor \overline{c}\lor (a\land b\land\overline{c})\)
     en utilisant la méthode algébrique et la méthode tabulaire.


<a id="orgfcf3b4b"></a>

## Méthode des tableaux de Karnaugh

Souvent les formes normales sont trop lourdes (pour être implanté
en circuit, par exemple).

La méthode des tableaux de Karnaugh fournie souvent une expression
plus simple.


<a id="org13f9544"></a>

### Principes généraux

-   On représente la table de vérité de la fonction \(f\) sous uneque
    forme particulière.
-   On cherche dans cette table des rectangle de 1 dont les
    dimensions sont des puissances de 2 (2, 4, 8, &#x2026;)
-   On déduit de ces rectangles une expression simplifiée de \(f\).


<a id="orgd3ca6e8"></a>

### Construction du tableau

-   Tableau à deux dimensions : chaque dimension concerne une ou
    deux variables.
-   Le tableau est torique : la première et la dernière colonne sont
    adjacentes, la première et la dernière ligne également.
-   On passe d'une colonne à une colonne adjacente ou d'une ligne à
    une ligne adjacente en ne modifiant qu'une seule variable (1 bit
    modifié).
-   les cellules du tableau contiennent les valeurs de la fonction.


<a id="orgdf8ecc5"></a>

### Identification des blocs

-   On identifie les rectangles de 1 
    -   de dimensions une puissance de deux ( 1x1, 1x2, 1x4, 2x1, 2x2,
        2x4, &#x2026; )
    -   tels que chaque valeur 1 appartient à au moins un rectangle
        (éventuellement plusieurs)
    -   tels que les rectangles soient les plus grands possibles.


<a id="orgd66ffd1"></a>

### Détermination de l'expression

À chaque rectangle, on associe un terme formé comme suit :

-   Lors de l'examen des celulles du rectangle, si une variable
    change de valeur, alors elle n'appartient pas au terme.
-   Si une variable ne change pas de valeur, alors elle apparait
    dans le terme : si elle reste à 1, elle apparait sous sa forme 
    positive, si elle reste à 0, alors elle apparait sous sa forme 
    négative.
-   le terme est le **et** logique des variables apparaissant.

La fonction booléenne simplifiée est le **ou** de tous termes associés
aux rectangles.

**Exercice :** Déterminer, à l'aide de la méthode des tableaux de
Karnaugh, une expression simplifiée de la fonction \(f\), définie
par la table de vérité

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">a</th>
<th scope="col" class="org-right">b</th>
<th scope="col" class="org-right">f(a,b)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>
</tbody>
</table>

**Exercice :** Déterminer, à l'aide de la méthode des tableaux de
Karnaugh, une expression simplifiée de la fonction \(g\), définie
par la table de vérité

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">a</th>
<th scope="col" class="org-right">b</th>
<th scope="col" class="org-right">c</th>
<th scope="col" class="org-right">f(a,b,c)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>
</tbody>
</table>

**Exercice :** Déterminer, à l'aide de la méthode des tableaux de
Karnaugh, une expression simplifiée de la fonction \(h\), définie
par la table de vérité

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">a</th>
<th scope="col" class="org-right">b</th>
<th scope="col" class="org-right">c</th>
<th scope="col" class="org-right">d</th>
<th scope="col" class="org-right">f(a,b,c,d)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>
</tbody>
</table>

