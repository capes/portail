# [[file:../lancer.org::*Écran][Écran:1]]
from geometry import PointVecteur
from rayon import Rayon
from scene import Scene
from PIL import Image
from math import floor


def float_to_uint8(x: float) -> int:
    """Convertit x en octet."""
    return max(0, min(255, floor(255 * x)))


def couleur_en_tuple(couleur: PointVecteur) -> tuple[int]:
    """Convertit une couleur en triplet rvb."""
    return tuple(float_to_uint8(c)
                 for c in couleur)


class Ecran:
    """Une classe représentant un écran."""

    def __init__(self, omega: PointVecteur, delta: int, N: int):
        """
        Initialise un écran.

        :param omega: (PointVecteur) l'origine des rayons
        :param delta: (int) dimension de l'écran en pixel
        :param N: (int) dimension de l'écran en cellules
        """
        self._omega = PointVecteur(omega)
        self._delta = delta
        assert N % 2 == 0, "N n'est pas pair"
        self._N = N
        self._larg = self._delta / self._N
        self._fond = PointVecteur(0, 0, 1)

    def dimension(self):
        """Renvoie la dimension de l'écran."""
        return self._N

    def point_grille(self, i: int, j: int) -> PointVecteur:
        """
        Renvoie un point de l'écran.

        :param i: (int) abscisse d'une cellule de l'écran
        :param j: (int) ordonnée d'une cellule de l'écran
        :return: (PointVecteur) le point de l'écran dans l'espace
        """
        ...

    def rayon(self, i: int, j: int) -> Rayon:
        """
        Renvoie un rayon passant par un point de l'écran.

        :param i: (int) abscisse d'une cellule de l'écran
        :param j: (int) ordonnées d'une cellule de l'écran
        :return: (Rayon) le rayon allant jusqu'au centre de la cellule
        """
        return Rayon.from_points(self._omega, self.point_grille(i, j))

    def lancer(self, scene: Scene, reflexion: int = 0) -> Image:
        """Génère l'image représentant la scene."""
        res = Image.new('RGB',
                        (self.dimension(), self.dimension()),
                        (0, 0, 255))
        for i in range(self.dimension()):
            for j in range(self.dimension()):
                r = self.rayon(i, j)
                couleur = scene.couleur(r, self._fond, reflexion)
                res.putpixel((j, i), couleur_en_tuple(couleur))
        return res
# Écran:1 ends here
