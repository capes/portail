# [[file:../lancer.org::*Géométrie][Géométrie:2]]
from geometry import PointVecteur


class Rayon:
    """
    Une classe représentant un rayon lumineux.

    >>> S = PointVecteur(1, 1, 0)
    >>> direction = PointVecteur(1, 1, 1)
    >>> r = Rayon(S, direction)
    >>> r.origin() == S
    True
    >>> r.direction() == direction.unitaire()
    True
    >>> r.point(2) == PointVecteur(3, 3, 2)
    True
    >>> A = PointVecteur(0, 0, 1)
    >>> B = PointVecteur(2, 1, -1)
    >>> r2 =  Rayon.from_points(A, B)
    >>> r2.origin() == A
    True
    >>> r2.direction() == (B-A).unitaire()
    True
    """

    def __init__(self, *args):
        """
        Initialise un rayon.

        :param args: couple de deux points
        """
        if len(args) == 2:
            if isinstance(args[0], PointVecteur):
                self._origin = args[0]
                if isinstance(args[1], PointVecteur):
                    self._direction = args[1].unitaire()
                else:
                    raise ValueError('bad argument type : {}'.format(args[1]))
            else:
                raise ValueError('Bad argument type : {}'.format(args[0]))
        else:
            raise ValueError('bad arguments number')

    def origin(self) -> PointVecteur:
        """Renvoie l'origine d'un rayon."""
        return self._origin

    def direction(self) -> PointVecteur:
        """Renvoie la direction du rayon."""
        return self._direction

    @classmethod
    def from_points(cls, A: PointVecteur, B: PointVecteur) -> "Rayon":
        """
        Crée un nouveau rayon d'origine A et de direction A->B.

        :param A: (PointVecteur) l'origine du rayon
        :param B: (PointVecteur) un autre point du rayon
        :return: (Rayon) le rayon d'origine A et de direction A->B
        :CU: A != B
        """
        ...

    def point(self, t: float) -> PointVecteur:
        """
        Repère un point sur un rayon.

        :param r: un rayon lumineux
        :param t: un réel
        :return: le point S+tu
        """
        ...


def main():
    """Programme principal."""
    import doctest
    doctest.testmod(verbose=True)


if __name__ == '__main__':
    main()
# Géométrie:2 ends here
