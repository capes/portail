# [[file:../lancer.org::*les sphères][les sphères:1]]
from gobject import GObject
from rayon import Rayon
from geometry import PointVecteur
from math import sqrt


class Sphere(GObject):

    def __init__(self, *args):
        """
        Initialise une sphère.

        :param center: le centre de la sphère (triplet ou PointVecteur) ;
        :param radius: le rayon de la sphère (nombre ou vecteur) ;
        :param color: diffusion des couleurs à la surface de la sphère\
        (triplet ou PointVecteur)
        """
        if len(args) not in {3, 4}:
            raise ValueError('Bad number of arguments')
        else:
            center = args[0]
            radius = args[1]
            color = args[2]
            if len(args) > 3:
                reflexion = args[3]
            else:
                reflexion = 1.0
            super().__init__(center, color, reflexion)
            if isinstance(radius, (int, float)):
                self._radius = args[1]
            elif isinstance(radius, PointVecteur):
                self._radius = abs(PointVecteur(radius))
            else:
                raise ValueError('Bad agument type {}'.format(args[1]))

    def rayon(self, P: PointVecteur = None) -> float:
        """Renvoie le rayon de la sphère."""
        return self._radius

    def intersection(self, r: Rayon) -> tuple[PointVecteur, float] or None:
        """
        Renvoie le point d'intersection le plus proche entre le rayon\
        et cette sphère.

        Si le rayon ne traverse pas la sphère, alors renvoie None.

        :param r: un rayon lumineux
        :return: le couple (P, d) où P est le premier point
                 du rayon appartenant à la sphère et
                 d la distance de l'origine du rayon à P

        :CU: l'origine du rayon est à l'extérieur de la sphere
        """
        ...
# les sphères:1 ends here
