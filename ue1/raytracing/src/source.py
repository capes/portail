# [[file:../lancer.org::*Optique][Optique:1]]
from geometry import PointVecteur


class Source:
    """
    A class to handle light source.

    >>> pos = PointVecteur(0, 0, 0)
    >>> color = PointVecteur(0.5, 0.5, 0.5)
    >>> s = Source(pos, color)
    >>> s.couleur() == color
    True
    >>> s.position() == pos
    True
    """

    def __init__(self, position: PointVecteur, couleur: PointVecteur):
        """
        Initialise une source.

        :param position: (PointVecteur) la position de la source
        :param couleur: (PointVecteur) la couleur de la source
        """
        if isinstance(position, tuple) and len(position) == 3:
            position = PointVecteur(*position)
        else:
            raise ValueError('bad argument type')
        self._position = position
        if isinstance(couleur, tuple) and len(couleur) == 3:
            couleur = PointVecteur(*couleur)
        else:
            raise ValueError('bad argument type')
        self._couleur = couleur

    def couleur(self) -> PointVecteur:
        """Renvoie la couleur de la source."""
        return self._couleur

    def position(self) -> PointVecteur:
        """Renvoie la position de la source."""
        return self._position


if __name__ == '__main__':
    import doctest
    doctest.testmod()
# Optique:1 ends here
