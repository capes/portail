# [[file:../lancer.org::*Les objets][Les objets:1]]
from geometry import PointVecteur
from rayon import Rayon
from source import Source


class GObject:
    """Classe implantant les objets solides."""

    def __init__(self, center: PointVecteur,
                 diffusion: PointVecteur,
                 reflexion: float = 1):
        """
        Initialise un objet graphique.

        :param center: (PointVecteur) le centre de l'objet
        :param diffusion: (PointVecteur) les coefficients de diffusion
        """
        if isinstance(center, tuple) and len(center) == 3:
            center = PointVecteur(*center)
        if not isinstance(center, PointVecteur):
            raise ValueError('wrong type for center')
        if isinstance(diffusion, tuple) and len(diffusion) == 3:
            diffusion = PointVecteur(*diffusion)
        if not isinstance(diffusion, PointVecteur):
            raise ValueError('wrong type for diffusion')
        if not isinstance(reflexion, (int, float)):
            raise ValueError('wrong type for reflexion')
        self._center = center
        self._diffusion = diffusion
        self._reflexion = reflexion

    def center(self, P: PointVecteur = None) -> PointVecteur:
        """Renvoie le centre d'un objet."""
        return self._center

    def diffusion(self) -> PointVecteur:
        """Renvoie les coefficients de diffusion de l'objet."""
        return self._diffusion

    def reflexion(self) -> float:
        """Renvoie le coefficient de réflexion de l'objet."""
        return self._reflexion

    def intersection(self, r: Rayon) -> tuple[PointVecteur, float] or None:
        """
        Renvoie le point d'intersection de cet objet avec le rayon.

        :return: un couple (P, d) où
        - P est un point de l'objet et
        - d sa distance par rapport à l'origine du rayon.

        Si le rayon ne n'intersecte pas l'objet, renvoie None.
        """
        ...

    def est_vu(self, P: PointVecteur, src: Source) -> bool:
        """
        Renvoie True si l'objet P est vu de la source src, False sinon.

        :param P: un point sur l'objet
        :param src: une source
        :return: True ssi src peut être vu de P
        :CU: l'object est CONVEXE et P appartient à l'objet
        """
        ...

    def rayon_reflechi(self, P: PointVecteur, r: Rayon) -> Rayon:
        """
        Crée le rayon réfléchi d'un rayon en un point.

        :param P: (PointVecteur) un PointVecteur de l'objet
        :param r: (PointVecteur) un rayon
        :return: (Rayon) le Rayon réfléchi
        """
        ...

    def couleur_diffusee(self, P: PointVecteur,
                         src: Source) -> PointVecteur:
        """Détermine la couleur diffusée par une source en un point."""
        ...
# Les objets:1 ends here
