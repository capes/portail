# [[file:../lancer.org::*un exemple][un exemple:1]]
from scene import Scene
from ecran import Ecran
from sphere import Sphere
from source import Source

sc = Scene('test_scene')
for center, rayon, couleur, ref in (((0, 5.5, -6), 3, (0, 1, 0), 0.8),
                                    ((-0.5, 2.5, -2), 1, (1, 1, 1), 1),
                                    ((2, -3, -3), 3, (0, .8, 1), 0.1),
                                    ((-4, 0, -3), 2, (1, 1, 0), 0.4)):
    sc.add_obj(Sphere(center, rayon, couleur, ref))

# Définition des sources lumineuses (positions)
for center, couleur in (((-1, 0, 3), (1, 1, 1)),
                        ((3, 0, 3), (0, 0, 1))):
    sc.add_src(Source(center, couleur))

sc2 = Scene('scene2')
for c, r, coul, ref in (((0, -1, -3), 2, (1, 1, 1), 1),
                        ((-1.5, 2, -3), 1.3, (.2, .2, .2), 0),
                        ((1.5, 2, -3), 1.3, (.2, .2, .2), 0),
                        ((0, -1.1, -.8), .2, (0, 0, 0), 0)):
    sc2.add_obj(Sphere(c, r, coul, ref))
for c, coul in (((2, 2, 3), (1, 1, 1)),
                ((1, 1, 1), (1, 1, 1))):
    sc2.add_src(Source(c, coul))

# Résolution de l'image et dimension de la grille
N = 512
DELTA = 10
ec = Ecran((0, 0, 4), DELTA, N)
im = ec.lancer(sc)
im.save('../images/quatre_boules_diffusion.png')
im = ec.lancer(sc, 3)
im.save('../images/quatre_boules_reflexion.png')
# un exemple:1 ends here
