# [[file:../lancer.org::*Géométrie][Géométrie:1]]
from __future__ import annotations
from math import sqrt


class PointVecteur:
    """
    A class to represent three dimensionnal point, vector or color.

    >>> p1 = PointVecteur(3.0, 1.4, 1.5)
    >>> p2 = PointVecteur(1.0, 2.0, 3.0)
    >>> p1 - p2 == PointVecteur(2.0, -.6, -1.5)
    True
    >>> abs(abs(p2) - sqrt(14)) < PointVecteur.EPS
    True
    >>> abs(p1.dot(p2) == 10.8) < PointVecteur.EPS
    True
    >>> p1 * p2 == PointVecteur(3, 2.8, 4.5)
    True
    >>> p1 + p2 == PointVecteur(4, 3.4, 4.5)
    True
    >>> PointVecteur(3, 4, 0).unitaire() == PointVecteur(3 / 5, 4 / 5, 0)
    True
    >>> PointVecteur.direction(p1, p2) == (p2 - p1).unitaire()
    True
    >>> tuple(PointVecteur(1, 2, 3)) == (1, 2, 3)
    True
    """
    __slots__ = ['x', 'y', 'z', '_counter']

    EPS = 1e-6

    def __init__(self, *args):
        """Initialise un nouveau point/vecteur/couleur."""
        if len(args) == 3:
            if all(isinstance(e, (int, float)) for e in args):
                self.x, self.y, self.z = args
            else:
                raise ValueError('Bad argument value')
        elif len(args) == 2:
            A, B = args
            if not isinstance(A, PointVecteur):
                raise ValueError('Bad argument : {}'.format(A))
            if not isinstance(B, PointVecteur):
                raise ValueError('Bad argument : {}'.format(B))
            self.x = B.x - A.x
            self.y = B.y - A.y
            self.z = B.z - A.z
        elif len(args) == 1:
            A = args[0]
            if isinstance(A, PointVecteur):
                self.x = A.x
                self.y = A.y
                self.z = A.z
            elif isinstance(A, tuple) and len(A) == 3:
                if all(isinstance(e, (int, float)) for e in A):
                    self.x, self.y, self.z = A
                else:
                    raise ValueError('Bad argument value')
            else:
                raise ValueError('Bad argument value or number')
        else:
            raise ValueError('Bad argument number')

    def __add__(self, other):
        """Ajoute deux vecteurs/couleurs."""
        return PointVecteur(self.x + other.x,
                            self.y + other.y,
                            self.z + other.z)

    def __neg__(self):
        """Renvoie l'opposé d'un vecteur."""
        return PointVecteur(-self.x, -self.y, -self.z)

    def __sub__(self, other):
        """Renvoie la différence de deux vecteurs."""
        return PointVecteur(self.x - other.x,
                            self.y - other.y,
                            self.z - other.z)

    def __mul__(self, val):
        """Multiplie deux vecteurs, ou d'un nombre et un vecteur."""
        if isinstance(val, (int, float)):
            return PointVecteur(val * self.x, val * self.y, val * self.z)
        elif isinstance(val, PointVecteur):
            return PointVecteur(self.x * val.x,
                                self.y * val.y,
                                self.z * val.z)
        else:
            raise ValueError("can't multiply by {}".format(val))

    def __eq__(self, other) -> bool:
        """Test l'égalité de deux vecteurs/point."""
        if isinstance(other, PointVecteur):
            return abs(self - other) < PointVecteur.EPS
        else:
            return False

    def __iter__(self):
        """Renvoie un itérateur."""
        self._counter = 0
        return self

    def __getitem__(self, i: int):
        """Renvoie la coordonnée d'indice i."""
        if i == 0:
            res = self.x
        elif i == 1:
            res = self.y
        elif i == 2:
            res = self.z
        else:
            raise IndexError()
        return res

    def __next__(self):
        """Renvoie la coordonnée suivante."""
        try:
            res = self[self._counter]
        except IndexError:
            raise StopIteration()
        self._counter += 1
        return res

    def dot(self, other) -> float:
        """Renvoie le produit scalaire de deux vecteurs."""
        if not isinstance(self, PointVecteur):
            raise ValueError('{} is not a vector'.format(self))
        if not isinstance(other, PointVecteur):
            raise ValueError('{} is not a vector'.format(other))
        return self.x * other.x + self.y * other.y + self.z * other.z

    def __abs__(self) -> float:
        """Renvoie la norme euclidienne d'un vecteur."""
        return sqrt(self.dot(self))

    def unitaire(self):
        """
        Renvoie un vecteur unitaire de même sens et direction.

        :return: un PointVecteur unitaire colinéaire
        :CU: self ne doit pas être le vecteur nul
        """
        n = abs(self)
        if n > PointVecteur.EPS:
            t = 1/abs(self)
            return self * t
        else:
            raise ValueError("vector is null")

    @classmethod
    def direction(cls, A, B):
        """
        Renvoie la direction A->B.

        :param A: un PointVecteur
        :param B: un PointVecteur
        :return: la direction de A vers B
        """
        return PointVecteur(A, B).unitaire()


if (__name__ == '__main__'):
    import doctest
    doctest.testmod(verbose=True)
# Géométrie:1 ends here
