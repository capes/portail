# [[file:../lancer.org::*Les scènes][Les scènes:1]]
from rayon import Rayon
from geometry import PointVecteur
from gobject import GObject
from source import Source


class Scene:
    """Une classe représentant une scène."""

    def __init__(self, name: str):
        """Construit une nouvelle scene."""
        self._name = name
        self._objets = []
        self._sources = []

    def add_obj(self, obj: GObject):
        """
        Ajoute un objet à la scène.

        :param obj: (GObject) l'objet
        """
        self._objets.append(obj)

    def add_src(self, src: Source):
        """
        Ajoute une source à l'image.

        :param src: (Source) une source
        """
        self._sources.append(src)

    def interception(self, r: Rayon, eviter=-1) -> (PointVecteur, int) or None:
        """Renvoie s'il existe le point le plus proche d'un objet interceptant\
         le rayon.

        :param r: (Rayon) un rayon
        :return: le couple (PointVecteur, int)

        """
        mini = float('inf')
        res = None
        for i in range(len(self._objets)):
            if i != eviter:
                t = self._objets[i].intersection(r)
                if t is not None:
                    P, d = t
                    if d < mini:
                        res = P, i
                        mini = d
        return res

    def couleur_diffusion(self, j: int, P: PointVecteur) -> PointVecteur:
        """
        Détermine la couleur diffusée.

        :param j: (int) l'indice de l'objet
        :param P: (PointVecteur) le point de l'objet
        :return: (PointVecteur) la couleur diffusée par les sources
        """
        ...

    def couleur(self, r: Rayon, fond, nreflexion: int = 0) -> PointVecteur:
        """Renvoie la couleur de l'éventuel point frappé par r."""
        x = self.interception(r)
        if x is None:
            couleur = fond
        else:
            P, i = x
            couleur = self.couleur_diffusion(i, P)
        return couleur

    def visible(self, j: int, P: PointVecteur, src: Source) -> bool:
        """
        Détermine si un point est visible d'une source.

        Il faut pour cela que le point soit au dessus de l'horizon\
        de l'objet et qu'aucun autre objet n'intercepte les rayons lumineux.

        :param j: l'indice de l'objet
        :param P: le point appartenant à l'objet
        :param src: la source du Rayon
        """
        res = False
        if self._objets[j].est_vu(P, src):
            res = True
            # détermine si le rayon émis par la source en direction
            # de P est intercepté par un autre objet.
            # il faut créer ce rayon et calculer les intersections
            # ne pas oublier la spécification de la méthode
            # intersection.
            ...
        return res
# Les scènes:1 ends here
