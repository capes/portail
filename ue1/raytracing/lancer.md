---
author: méef nsi
date: 26/03/23
title: Lancer de rayon
---

Ce travail a pour objectif de découvrir une méthode de génération d'image de synthèse : celle du
lancer de rayon.


# Géométrie

La classe `PointVecteur` définit un type permettant de manipuler des points et des vecteurs de
l'espace, mais aussi des couleurs représentées sous forme de triplets.

Dans l'espace, un point est repéré par ses coordonnées $(x, y, z)$ relatives à un repère orthonormé
$(O; \overrightarrow{i}, \overrightarrow{j}, \overrightarrow{k})$ .

Pour fixer les idées :

-   la direction de $\overrightarrow{i}$ est une direction horizontale face à l'observateur ;
-   celle de $\overrightarrow{j}$ une direction verticale face à l'observateur et
-   celle de $\overrightarrow{k}$ une direction horizontale traduisant l'éloignement du point.

Dans toute la suite, $\overrightarrow{u}\cdot\overrightarrow{v}$ désigne le produit scalaire des
vecteurs $\overrightarrow{u}$ et $\overrightarrow{v}$, il s'agit donc d'un nombre réel ;
$\overrightarrow{u}\odot\overrightarrow{v}$ désigne le produit composante à composante des vecteurs
$\overrightarrow{u}$ et $\overrightarrow{v}$, il s'agit donc d'un vecteur.

Les objets de cette classe disposent des méthodes suivantes :

-   `dot` qui renvoie le produit scalaire de deux vecteurs ;

-   `unitaire` qui renvoie un vecteur unitaire colinéaire à celui sur lequel porte la méthode ;

-   `direction` : méthode statique renvoyant la direction sous forme de vecteur unitaire pour aller
    d'un point à un autre ;

-   des opérations `*,+, -, ==` qui implantent respectivement la multiplication, l'addition, la
    soustraction et l'égalité.

**à faire** étudier le code la classe `PointVecteur`, puis répondez aux questions suivantes :

-   quelles multiplications sont implantées pour les vecteurs ?
-   si `u` désigne une instance de la classe `PointVecteur`, quelles expressions valent le double de
    `u` ? le triple ?
-   l'expression `-u` est-elle valide ? Pourquoi ?


# La lumière


## les sources

Dans ce tp, nous allons considérer dans un premier temps des rayons lumineux issus de source
lumineuse. Une source lumineuse $S$ est un couple $(P, c)$ où $P$ est la position de la source dans
l'espace et $c$ sa couleur. Ces deux paramètres sont représentés par des objets de la classe
`PointVecteur`.

Les sources lumineuses émettent dans toutes les directions et sont représentées par des objets de la
classe `Source`.

Une source lumineuse est caractérisée par :

-   une position : celle de la source d'où seront émis les rayons ;
-   une couleur : la couleur de la lumière émise.

Les couleurs sont représentées par des triplets de flottants dont les trois composantes codent
respectivement le niveau de rouge, vert et bleu (composantes RVB). Chaque composante est comprise
entre 0 et 1. Plus la valeur est élevée, plus la composante contribue fortement à la couleur. Ainsi
la couleur $(1, 1, 1)$ correspond au blanc, $(0,0,0)$ au noir, $(0.5, 0.5, 0.5)$ désigne un gris
moyen et $(0.6, 1, 0.6)$ un vert pastel. En python, on utilisera la classe `PointVecteur` pour
représenter une couleur.

La classe `Source` dispose d'un constructeur permettant de créer une source à partir d'un point et
d'une couleur et des accesseurs correspondants.

## les rayons lumineux

Un rayon lumineux issu du point $S$ est une demi-droite d'origine $S$ Ce rayon est représenté en
python par des objets de la classe `Rayon`. Cette classe dispose :

-   d'un constructeur permettant de créer un rayon à partir d'un couple $(S,\overrightarrow{u})$ où
    $S$ est l'origine du rayon et $\overrightarrow{u}$ le vecteur unitaire donnant la direction et
    le sens de propagation du rayon lumineux. Ces deux ;
-   de deux sélecteurs `origin` et `direction` ;
-   d'une méthode statique `from_points` permettant de créer un rayon à partir de deux points.

Un point $M$ appartient au rayon si, et seulement si, il existe un réel positif $t$ tel que
$\overrightarrow{SM}=t \overrightarrow{v}$. La méthode `point` prend en paramètre un réel $t$ et
renvoie le point $M$ du rayon correspondant.

**à faire** : Écrire le code de la méthode statique `from_points`.

**à faire** : Écrire le code de la méthode `point`.


# Les objets

Les rayons lumineux ne sont pas susceptibles d'apparaitre dans un rendu. Seuls les objets solides le
sont. Les objets solides ont la capacité de renvoyer la lumière, et c'est ainsi que l'oeil les
perçoit. Dans cette simulation, nous nous limiterons à des sphères, mais on pourrait envisager des
objets d'autres natures (cube, tores, &#x2026;.)

On définit une classe abstraite `GObject` permettant de modéliser les objets solides présents dans
une scène.

Ces objets disposent :

-   de trois attributs et des sélecteurs correspondants :
-   `center` qui représente le centre de l'objet,
-   `diffusion` qui contient les trois coefficients de diffusion de la surface de l'objet (ces
    coefficients seront expliqués dans la prochaine section) et
-   `reflexion` nombre flottant représentant la capacité de l'objet à réfléchir la lumière.

-   d'une méthode `est_vu(P, src)` qui renvoie `True` si, et seulement si, une source lumineuse peut
    être vue d'un point $P$ appartenant à l'objet.

-   d'une méthode `rayon_reflechi` que l'on complétera dans la prochaine section ;

-   d'une méthode `couleur_diffusee` que l'on complétera dans la prochaine section ;

-   d'une méthode *abstraite* `intersection` qui renvoie les points d'intersection d'un objet avec
    un rayon.


## les sphères

Dans cette première version, les seuls objets que nous allons implanter sont des sphères.

Par définition, une sphère de centre $C$ et de rayon $r>0$ est l'ensemble des points de l'espace
situés à une distance $r$ du point $C$.

En python, on représente une sphère par un objet de la classe `Sphere`, héritant de `GObject` et
disposant :

-   d'un constructeur prenant en paramètre :
-   son centre ;
-   son rayon ;
-   ses coefficients de diffusion (sa couleur);
-   son coefficient de reflexion.
-   de l'accesseur correspondant au rayon ;
-   d'une méthode `intersection` implantant la méthode abstraite de la classe parente.

Une rayon d'origine $A$ et de vecteur directeur $\overrightarrow{u}$ (unitaire) et une sphère $(C,r)$ sont séquents si, et seulement si, l'équation du second degré d'inconnue $t$ :

$$
t^2+2t(\overrightarrow{u}\cdot\overrightarrow{CA}) +
\vert\vert\overrightarrow{CA}\vert\vert^2-r^2=0
$$

admet des solutions réelles. Dans ce cas, la plus petite des solutions $t_0$, qui est par ailleurs
positive, est **égale à la distance** entre $A$ et le point d'intersection, qui peut donc être
calculé par $P = A + t_0\times \overrightarrow{u}$

**à faire** : en déduire la méthode `intersection` de la classe `Sphere`. (*revoir la résolution de
l'équation du second degré* $at^2+bt+c=0$)


# Optique

En optique, les objets sont perçus par l'oeil en fonction de la manière dont ils absorbent certaines
longueurs d'onde de la lumière. Les rayons lumineux circulent en ligne droite, et lorsqu'ils
rencontrent un objets, ils sont déviés en fonction de leur longueur d'onde ou absorbés.

Nous n'allons pas implanter un lancer de rayon basé sur les longueurs d'onde (cela pourrait faire
l'objet d'un autre tp). On se contentera de prendre en compte deux phénomènes :

-   la diffusion ;
-   la réflexion.


## Visibilité

Une source lumineuse ponctuelle $S$ ne peut être vue d'un point $P$ d'un objet régulier et convexe
de centre $C$ que si la source est au dessus de l'horizon de $P$, défini comme le plan tangent à
l'objet en $P$.

![visibilité](images/parcours-lumiere.png)

Une condition géométrique pour que la source $S$ soit au-dessus de l'horizon du point $P$ est que

$$s\overrightarrow{PS} \cdot \overrightarrow{N} > 0$$

où $\cdot$ désigne le produit scalaire. Pour une sphère, on pourra prendre $\overrightarrow{N}=\overrightarrow{CP}$ où $C$ est le centre de la sphère.


On considère plusieurs objets et une source lumineuse. Pour que la source soit visible de $P$ (point 
 d'un objet), il faut que :

1.  cette source soit au-dessus de l'horizon du point $P$ ;
2.  aucun autre objet ne la cache.

**à faire** Écrire la méthode `est_vu` de la classe `GObject` qui implante la condition 1. (on
utilisera la 2. plus tard)


## Diffusion

On considère un point $P$, à la surface d'un objet, éclairé sous l'incidence $\theta$ par une source
lumineuse ponctuelle de couleur $C_S=(R_S, V_S, B_S)$. On note $\overrightarrow{N}$ le vecteur
unitaire normal à la sphère en $P$, dirigé vers l'extérieur de la sphère et $\overrightarrow{u}$ le
vecteur unitaire du rayon lumineux qui éclaire $P$ en provenance de la source. On considère que le
point $P$ diffuse la lumière de la source de manière isotope dans tout le demi-espace délimité par
le plan tangent à la sphère en $P$ qui contient la source.

Autrement dit, le point $P$ diffuse la lumière de la source dans toutes les directions
$\overrightarrow{w}$ telles que $\overrightarrow{w}\cdot\overrightarrow{N}\geq 0$ et la lumière
diffusée ne dépend pas de la direction d'observation $\overrightarrow{w}$, en particulier, elle ne
dépend pas de $\theta'$.

La faculté d'un objet à diffuser la lumière est modélisée par ses coefficient $k_{dr}, k_{dv}$ et
$k_{db}$ qui mesurent son aptitude à réémettre les composantes rouge, vert et bleue. Chaque
coefficient est un réel compris entre $0$ et $1$. On représente les coefficients de diffusion d'un
objet par un triplet $k_d=(k_{dr}, k_{dv}, k_{db})$ de flottants.

La couleur $C_d=(R_d, V_d, B_d)$ de la lumière diffusée par le point $P$ éclairé par la source $S$
suit alors de Lambert :

$$
C_d=\cos \theta \cdot (k_d\odot C_S)\quad \text{soit}\quad (R_d, V_d, B_d) =
(k_{dr}R_S\cos\theta\,, k_{dv}V_S\cos\theta\,, k_{db}B_S\cos\theta)
$$

**à faire** : Écrire la méthode `couleur_diffusee` de la classe `GObject` qui implante cette
formule. Le cosinus de deux vecteurs $\overrightarrow{u}$ et $\overrightarrow{v}$ pourra être
calculé grace à la formule :

$$
\cos(\overrightarrow{u}, \overrightarrow{v}) = \frac{1}{\vert \vert
\overrightarrow{u}\vert\vert}\times \frac{1}{\vert\vert \overrightarrow{v}\vert\vert}\times
\overrightarrow{u}\cdot\overrightarrow{v}$
$$


## Réflexion

Si la surface de l'objet est réfléchissante, au phénomène de diffusion s'ajoute le phénomène de
réflexion. Lorsqu'un rayon lumineux $(S,\overrightarrow{u})$ issu d'un point $S$ atteint un point
$P$ de la surface, il donne naissance au rayon réfléchi $(P,\overrightarrow{w})$. Les lois de la
réflexion de Descartes stipulent que, avec les notations de la figure ci-dessous :

![img](images/parcours-lumiere.png)

-   $\overrightarrow{u}, \overrightarrow{w}$ et $\overrightarrow{N}$ sont coplanaires ;
-   $(\overrightarrow{u}+\overrightarrow{w})\cdot \overrightarrow{N} = 0$, ce qui correspond à
    $\theta' = \theta$.

On en déduit alors une expression facile à calculer de $\overrightarrow{w}$ :

$$
\overrightarrow{w} = \overrightarrow{u}-2\times (\overrightarrow{u}\cdot \overrightarrow{N})\times
\overrightarrow{N}
$$


**à faire** : Écrire la méthode `rayon_reflechi` de la classe `GObject` qui implante cette formule.


# Les scènes

La classe scène dispose de méthodes pour

-   ajouter et retirer des objets d'une scène ;
-   ajouter et retirer des sources lumineuse d'une scène ;
-   calculer d'intersection d'un rayon lumineux avec l'un des objets de la scène ;
-   déterminer si une source est visible d'un point ;
-   déterminer la couleur diffusée par un point sur un des objets de la scène.


# Algorithme du lancer de rayon

Cette partie implante l'algorithme qui génère l'image 2D de la scène 3D à visualiser.

Pour des raisons d'efficacité, il est préférable de construire le parcours de la lumière **de l'oeil
vers les sources** : c'est l'oeil qui lance les rayons lumineux !

Cela peut paraître étrange, mais cette méthode est parfaitement rigoureuse en raison du principe de
retour inverse de la lumière (conséquence du principe de Fermat) :

> Le trajet suivi par la lumière pour aller d'un point à un autre ne dépend pas du sens de
> propagation de la lumière.

La lumière parcourt donc le même chemin, qu'elle aille de $A$ vers $B$ ou de $B$ vers $A$. Comme
seuls les rayons arrivant à l'oeil nous interessent, nous n'allons considérer que ceux partant de
l'oeil et qui arrivent à une source.

Un écran est un carré dans le plan $(O; \overrightarrow{i}, \overrightarrow{j})$ dont le centre est
l'origine $O$ du repère :

![img](images/scene-ecran.png)


## Écran

L'écran est un carré de côté $\delta$ divisé en $N\times N$ cases ($N$ pair) qui représentent les
pixels de l'image finale.

La classe `Ecran` représente un écran carré. Elle permet de générer des rayons qui partent de l'oeil
de l'utilisateur et qui passent par les points de l'écran.

Son constructeur prend en paramètre :

-   un point `omega` représentant la position de l'oeil ;
-   la dimension `delta` de l'écran en pixel ;
-   la dimension `N` de l'écran en cellules.

(la largeur de chaque cellule est dic `delta/N`).

**à faire** : compléter la méthode `point_grille` de la classe `Ecran`. Cette méthode renvoie un
rayon partant de `omega` et passant par le point de la grille situé à la $i$eme ligne, $j$ieme
colonne (voir la figure).


## couleur d'un pixel

Dans un premier temps, les objets de la scène à représenter sont supposés parfaitement mats. Ils se
contentent donc de diffuser la lumière des sources sans la réfléchir.

On considère un point $P$ d'un objet de la scène atteint par un rayon issu de l'oeil. On note $E$ le
point d'intersection de ce rayon avec l'écran. La couleur $C_d=(R_d, V_d, B_d)$ diffusée par $P$ est
la somme des couleurs diffusées par ce point, dues à l'éclairement des sources visibles du point $P$
:

$$C_d=\sum_{S_i\mbox{ visibles } de P}(k_d\odot C_{S_i})\cos\theta_i$$

On suppose que les couleurs ne saturent pas : toutes les composantes de $C_d$ restent inférieures
à 1. La couleur de $C_d$ est affectée au point $E$ de l'écran.

**à faire** Compléter la méthode `visible` de la classe `Scene`. Cette méthode prend en paramètre
l'indice `j` d'un objet, un point `P` sur cet objet et une source lumineuse `src`. Elle renvoie
`True` ssi la source `src` est visible du point `P` ; c'est-à-dire ssi :

-   `P` est vu de `src`.
-   aucun des rayons partant de `src` et allant jusqu'à `P` ne rencontre d'autre objet (on pourra
    calculer l'intersection d'un tel rayon avec les autres objets et comparer la distance obtenue
    avec la distance entre l'origine de la source et `P`.

**à faire** : compléter la méthode `couleur_diffusion` de la classe `Scene`.

**à faire** : analyser le code de la méthode `lancer` de la classe `Ecran`.


# un exemple

```python
from scene import Scene
from ecran import Ecran
from sphere import Sphere
from source import Source

sc = Scene('test_scene')
for center, rayon, couleur, ref in (((0, 5.5, -6), 3, (0, 1, 0), 0.8),
                                    ((-0.5, 2.5, -2), 1, (1, 1, 1), 1),
                                    ((2, -3, -3), 3, (0, .8, 1), 0.1),
                                    ((-4, 0, -3), 2, (1, 1, 0), 0.4)):
    sc.add_obj(Sphere(center, rayon, couleur, ref))

# Définition des sources lumineuses (positions)
for center, couleur in (((-1, 0, 3), (1, 1, 1)),
                        ((3, 0, 3), (0, 0, 1))):
    sc.add_src(Source(center, couleur))

sc2 = Scene('scene2')
for c, r, coul, ref in (((0, -1, -3), 2, (1, 1, 1), 1),
                        ((-1.5, 2, -3), 1.3, (.2, .2, .2), 0),
                        ((1.5, 2, -3), 1.3, (.2, .2, .2), 0),
                        ((0, -1.1, -.8), .2, (0, 0, 0), 0)):
    sc2.add_obj(Sphere(c, r, coul, ref))
for c, coul in (((2, 2, 3), (1, 1, 1)),
                ((1, 1, 1), (1, 1, 1))):
    sc2.add_src(Source(c, coul))

# Résolution de l'image et dimension de la grille
N = 512
DELTA = 10
ec = Ecran((0, 0, 4), DELTA, N)
im = ec.lancer(sc2)

```

![img](images/quatre_boules_diffusion.png)


## avec la réflexion

**à faire** modifier le code pour prendre en compte la réflexion de la lumière.

![img](images/quatre_boules_reflexion.png)
