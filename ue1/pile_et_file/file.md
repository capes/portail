
# Table des matières

1.  [Motivation](#org7a55deb)
    1.  [Jeu du taquin](#org55acb95)
    2.  [Déterminer une expression postfixée](#org53a68db)
2.  [Les files](#org4eef976)
3.  [Implémentation des files](#org8c7c568)
4.  [Résolution du taquin](#org5a6e315)



<a id="org7a55deb"></a>

# Motivation


<a id="org55acb95"></a>

## Jeu du taquin

On dispose d'une classe [Taquin](./src/taquin.py) permettant de jouer au taquin :

    >>> g = Taquin(4)
    >>> g
         1  2  3
      4  5  6  7
      8  9 10 11
     12 13 14 15
    
    >>> g.is_solved()
    True
    >>> g.move(Taquin.RIGHT)
    >>> g
      1     2  3
      4  5  6  7
      8  9 10 11
     12 13 14 15
    
    >>> g.is_solved()
    False
    >>> g.move(Taquin.DOWN)
    >>> g
      1  5  2  3
      4     6  7
      8  9 10 11
     12 13 14 15
    
    >>> g.random()
    >>> g
         4  6  3
      5  2  1  7
      8  9 13 15
     12 11 10 14

On désire écrire un programme permettant de résoudre le jeu du taquin, c'est-à-dire 
déterminer la plus petite séquence de déplacement permettant de remettre le jeu
dans son état initial.

Pour cela on peut remarquer qu'on passe d'un état du jeu au suivant en effectuant
un déplacement.

Pour déterminer la plus courte séquence, on peut parcourir tous les états accessibles
en un déplacement, puis tous les états accessibles en deux déplacements, puis en trois,
&#x2026; jusqu'à revenir à l'état initial (et sans visiter des états déjà examinés). 

Une manière d'effectuer cela est appelé **parcours en largeur** et utilise une file.


<a id="org53a68db"></a>

## Déterminer une expression postfixée

On peut utiliser une pile et une file pour transformer une expression
arithmétique en expression postfixée.

    - Calculer 2+6*7
    - Calculer (2+3)*(6-2)

Voir [ici](infixe_postfix.md)

<a id="org4eef976"></a>

# Les files

Une *file* est une structure de données linéaire admettant les *opérations primitives*
suivantes :

-   création d'une file vide,
-   enfilement d'un élément dans la file,
-   défilement d'un élément de la file,
-   test de vacuité de la file.

Le principe d'une pile est le suivant : 
L'élément renvoyé par la méthode `defile` est le premier élément enfilé. On dit que la file est
une structure **FIFO** (*First In First Out*).


<a id="org8c7c568"></a>

# Implémentation des files

Le fichier [myqueue<sub>squel.py</sub>](./src/myqueue_squel.py) contient le squelette de l'implémentation d'une file.

**à faire**

Compléter le fichier myqueue.

<a id="org5a6e315"></a>

# Résolution du taquin

    file <- file vide
    visité <- ensemble vide
    file.enfiler(état)
    Tq la file n'est pas vide et pas résolu:
       état = f.défiler()
       vérifier si l'état est résolu
       visité.ajoute(état)
       Pour chaque état v accessible non visité:
          file.enfiler(v)
       Fin Pour
    Fin Tq

**Rq :** en place des états, on a tout intérêt à considérer les chemins depuis l'état initial.

