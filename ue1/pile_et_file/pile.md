
# Table des matières

1.  [Motivation](#org36f4832)
    1.  [fichier au format html bien formés](#org96718f5)
    2.  [Évaluateur d'expressions postfixées](#org0ed12c0)
    3.  [Résolution d'un labyrinthe](#org40886af)
    4.  [Dérécursivation d'un algorithme](#org9f54cb5)
        1.  [Algorithme de la goutte d'eau](#orgcb592df)
        2.  [Parcours en profondeur d'un graphe](#org6728968)
2.  [Opération primitives sur les piles](#org2f0fe4b)
3.  [Implémentation](#org89d4a33)



<a id="org36f4832"></a>

# Motivations


<a id="org96718f5"></a>

## fichier au format html bien formés

Le html est un format de fichier utilisé par les navigateurs web. Les fichiers
au format html (et plus généralement au format xml) sont des fichiers texte dans 
lesquels on trouve des balises 

-   ouvrantes de la forme `<nom attributs>`
-   fermantes de la forme `</nom>`

où `nom` désigne le nom de la balise et `attributs` une liste de couples `clé=valeur`.

Dans la pratique, `nom=div, p, html, body, head, ...`

Dans cette activité, on considère qu'un document html est bien formé si :

-   à chaque balise ouvrante correspond une balise fermante
-   on ne peut fermer une balise que si toutes les balises situées entre les 
    deux balises ouvrantes et fermantes  sont fermées.

Par exemple, les documents [ex1.html](./data/ex1.html) et [ex4.html](./data/ex4.html) sont bien formés,
les documents [ex2.html](./data/ex2.html) et [ex3.html](./data/ex3.html) sont mal formés.

On souhaite écrire un programme permettant de déterminer si un fichier html est bien
formé.

On dispose d'un [parser](./src/myhtml_parser.py) de fichier html, permettant de parcourir séquentiellement les balises :

```python
from myhtml_parser import MyHTMLParser

parser = MyHTMLParser("<!DOCTYPE html><html lang=\"fr\"><div class=\"resultat\">Timoléon</div></html>")
while parser.has_tag():
    print(parser.next_tag())
```

    <html>
    <div>
    </div>
    </html>

On veut écrire un prédicat renvoyant `True` si un texte est un document html bien formé et `False`
dans le cas contraire.


<a id="org0ed12c0"></a>

## Évaluateur d'expressions postfixées

On veut créer un évaluateur d'expressions postfixées. Les expressions postfixées sont
des expressions dans lesquelles, les opérandes précédent les opérateurs. Par exemple, 
l'expression arithmétique \(1+2\) est représentée par l'expression postfixée \(1 2 +\).

On cherche à réaliser un évaluateur d'expressions postfixées :

```python
postfix = PostfixEvaluator("1 2 3 * +")
print(postfix.evaluate())
```
    7

<a id="org40886af"></a>

## Résolution d'un labyrinthe

Un labyrinthe est dit parfait si, pour tout couple d'emplacements \(A\) et \(B\) du
labrynthe, il existe un seul chemin permettant de relier \(A\) à \(B\).

Étant donné un point \(A\) et un point \(B\), on cherche à déterminer ce chemin, c'est
à dire la liste des celulles visitées.


<a id="org9f54cb5"></a>

## Dérécursivation d'un algorithme


<a id="orgcb592df"></a>

### Algorithme de la goutte d'eau

    def colorier(x,y):
        if get_color(x, y) == WHITE:
            set_color(x,y, RED)
            for a,b in voisins(x,y):
                colorier(a,b)
    
    def colorier_stack(x,y):
        P = Stack()
        P.push((x,y))
        while not P.is_empty():
            x, y = P.pop()
            if get_color(x, y) == WHITE:
                set_color(x, y, RED)
                for a, b in voisins(x,y):
                    P.push((a,b))


<a id="org6728968"></a>

### Parcours en profondeur d'un graphe


<a id="org2f0fe4b"></a>

# Opération primitives sur les piles

Les *piles* sont des structures de données linéaires admettant les *opérations primitives* 
suivantes :

-   Création d'une pile vide
-   empilement d'un élément sur une pile
-   dépilement du sommet d'une pile
-   test de vacuité d'une pile.

Le principe d'une pile est le suivant : on ne peut accéder un élément \(e\) de la pile qu'en ayant
enlevé d'abord tous les éléments empilés après \(e\).

L'élément renvoyé par la méthode `pop` est le dernier élément empilé. On dit que la pile est
une structure **LIFO** (*Last In First Out*).


<a id="org89d4a33"></a>

# Implémentation

Le fichier [stack<sub>squel.py</sub>](./src/stack_squel.py) contient le squelette de l'implémentation d'une pile.

**à faire**

Renommez le et implémentez votre propre structure de pile.

**à faire**

Puis implémentez le vérificateur html.

