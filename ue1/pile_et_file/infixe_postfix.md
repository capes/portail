
# Table des matières

1.  [Objectif](#org0977f8b)
2.  [Le code](#org169864b)



<a id="org0977f8b"></a>

# Objectif

Utiliser une pile pour transformer une expression infixée en
expression postfixée. La file sert ici pour stocker le résultat.

<a id="org169864b"></a>

# Le code

Il faut tenir compte des ordres de priorité des opérateurs :

-   On ne peut empiler un opérateur que si sa priorité est supérieure
    à celle du sommet. Si ce n'est pas le cas, on dépile l'opérateur 
    et on l'ajoute au résulat.
-   le cas de la parenthèse ouvrante est particulier : elle s'empile
    toujours et on peut tout empiler dessus.
-   la parenthèse fermante produit le dépilage jusqu'à rencontrer dans
    la pile une parenthèse ouvrante.

On peut supposer pour commencer que l'expression est syntaxiquement
correcte.

Il faut pouvoir gérer les priorités des opérateurs. Pour cela, on
peut utiliser un dictionnaire : 

```python
OPERATORS = { '+' : 1, '-' : 1,
              '*' : 2, '/' : 2,
              '(' : 0, ')' : 0 }
```

Voici un exemple d'utilisation avec l'état de la pile affichée à
chaque itération :

```python
print(infix2postfix("( 1 + 3 * 2 ) * ( 5 * 3 + a )"))
print(infix2postfix("6 / 2 + 5"))
```
    ['(']
    ['(', '+']
    ['(', '+', '*']
    []
    ['*']
    ['*', '(']
    ['*', '(', '*']
    ['*', '(', '+']
    ['*']
    1 3 2 * + 5 3 * a + *
    ['/']
    ['+']
    6 2 / 5 +

