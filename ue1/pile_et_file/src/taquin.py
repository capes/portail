from random import choice

class Taquin:
    LEFT = (-1,0)
    RIGHT = (1,0)
    UP = (0,-1)
    DOWN  = (0,1)
    MOVES = [ LEFT, RIGHT, UP, DOWN ]

    def __init__(self, n):
        """
        :param n: (int) board dimension
        """
        self.__content = { (x,y) :  y*n+x for y in range(n)  for x in range(n) }
        self.__size = n

    def get_size(self):
        """
        :return: (int) la taille du jeu
        """
        return self.__size

    def get_coordinates(self, c):
        """
        :param c: (int) une celulle
        :return: (coupleof int) les coordonnées de c
        :CU: 0 <= c < size
        """
        y = 0
        found = False
        while not found and y < self.__size:
            x = 0
            while not found and x < self.__size:
                found = self.__content[ (x,y) ] == c
                x += 1
            y += 1
        return (x-1, y-1)

    def _is_valid_coordinates(self, x, y):
        """
        :param x, y: (int) des coordonnées
        :return: (bool) True si les coordonnées sont celles d'une cellule, False sinon
        """
        return 0 <= x < self.__size and 0 <= y < self.__size

    def neighborhood(self, x, y):
        """
        :param x, y: (int) des coordonées
        :return: (list) liste des cellule voisines
        """
        return [ (x + dx , y + dy) for dx, dy in Taquin.MOVES
                 if self._is_valid_coordinates(x + dx, y + dy) ]

    def __getitem__(self, coordinates):
        """
        :param x, y: (list) des coordonnées
        :return: (int) la cellule aux coordonnées
        """
        return self.__content[coordinates]

    def move_cell(self, c, m):
        """
        :param c: (int) une cellule
        :param m: (couple of int) un mouvement
        :CU: c est une cellule valide et m un mouvement valide
        """
        x, y = self.get_coordinates(c)
        dx, dy = m
        k, l = x + dx, y + dy
        if self._is_valid_coordinates(k, l):
            self.__content[(x, y)] = self.__content[(k, l)]
            self.__content[(k, l)] = c

    def move(self, m):
        """
        déplace la cellule 0 dans la direction
        :param m: un mouvement
        """
        self.move_cell(0, m)

    def moveable_cell(self):
        """
        :return: (list) liste des cases pouvant être déplacées
        """
        x, y = self.get_coordinates(0)
        return { self.__getitem__((a, b)) for a, b in self.neighborhood(x, y) }

    def random(self, n = 100):
        """
        mélange la grille
        """
        for _ in range(n):
            self.move(choice(Taquin.MOVES))

    def is_solved(self):
        """
        :return: (bool) True si le jeux est résolu, False sinon
        """
        solved = True
        y = 0
        while solved and y < self.__size:
            x = 0
            while solved and x < self.__size:
                solved = solved and self.__content[(x,y)] == x + y * self.__size
                x += 1
            y += 1
        return solved

    def __hash__(self):
        """
        :return: (int) a hash of the game
        """
        res = 0
        for y in range(self.__size):
            for x in range(self.__size):
               res = res * 100 + self.__content[(x,y)]
        return res

    def from_hash(self, h):
        """
        :param hash: (int) a hash for a game
        """
        res = h
        for y in range(self.__size-1,-1,-1):
            for x in range(self.__size-1,-1,-1):
               self.__content[(x,y)] = res % 100
               res //= 100

    def __repr__(self):
        """
        :return: (str) a représentation of the game
        """
        res = ""
        l = len(str(self.__size ** 2 -1))
        strf = " {:"+str(l)+"d}"
        for y in range(self.__size):
            for x in range(self.__size):
                c = self.__getitem__((x, y))
                if c == 0:
                    res += " " * (l+1)
                else:
                    res += strf.format(c)
            res += "\n"
        return res
    __str__ = __repr__

if __name__ == "__main__":
    g = Taquin(3)
    print(g)
    g.random()
    print(g)
