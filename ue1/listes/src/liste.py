# [[file:../listes.org::*Le module ~liste~][Le module ~liste~:9]]
#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'FIL - Faculté des Sciences et Technologies -  \
Univ. Lille <http://portail.fil.univ-lille1.fr>_'
__date_creation__ = 'Sep 23 2020'
__doc__ = """
:mod: module
:author: {:s}
:creation date: {:s}
:last revision:

Définition d'une classe Liste pour des listes récursives


Deux constructions et 3 méthodes primitives
>>> l1 = Liste()
>>> l1.est_vide()
True
>>> l2 = Liste(1, l1)
>>> l2.est_vide()
False
>>> l2.tete()
1
>>> l2.reste().est_vide()
True
>>> l3 = Liste(3, Liste(2, l2))
>>> print(l3)
[3, 2, 1]
>>> print(l3.reste())
[2, 1]


Deux méthodes pour les listes mutables
>>> l3.change_tete(4)
>>> print(l3)
[4, 2, 1]
>>> l3.change_reste(l1)
>>> print(l3)
[4]

""".format(__author__, __date_creation__)


class ListeError(Exception):
    """
    Exception relative aux listes.

    Exception déclenchée par :
    - la construction de listes
    - et l'utilisation des méthode tete et reste sur listes vides
    """

    def __init__(self, msg):
        """Construit une liste."""
        self.message = msg


class Liste():
    """Classe liste récursive."""

    def __init__(self, *args):
        """
        Crée une nouvelle liste.

        :param tete: la tete de la liste
        :type tete: any
        :param reste: le reste de la liste
        :type reste: Liste
        :Exemples:

        >>> l = Liste()
        >>> isinstance(l, Liste)
        True
        >>> g = Liste(1, l)
        >>> isinstance(g, Liste)
        True
        """
        pass

    def est_vide(self):
        """
        Prédicat de vacuité.

        :return: True si la liste est vide, False sinon
        :rtype: bool
        :Exemples:

        >>> l = Liste()
        >>> l.est_vide()
        True
        >>> g = Liste(1, l)
        >>> g.est_vide()
        False
        """
        pass

    def tete(self):
        """
        Renvoie la tete de la liste.

        :return: la tete de la liste
        :rtype: any
        :raise: ListeError si la liste est vide
        :Exemples:

        >>> l = Liste(1, Liste())
        >>> l.tete()
        1
        """
        pass

    def reste(self):
        """
        Renvoie le reste de la liste.
        
        :return: le reste de la liste
        :rtype: Liste
        :raise: ListeError si la liste est vide
        :Exemples:
        
        >>> l = Liste(1, Liste())
        >>> g = Liste(2, l)
        >>> g.reste() == l
        True
        """
        pass

    def __str__(self):
        """
        renvoie une représentation textuelle de la liste
        
        :return: une représentation textuelle de la liste
        :rtype: str
        :Exemples:
        
        >>> l = Liste()
        >>> str(l)
        '[]'
        >>> g = Liste(1, Liste(2, Liste(3, l)))
        >>> str(g)
        '[1, 2, 3]'
        """
        pass

    def __len__(self):
        """
        renvoie la longueur de la liste
        
        :return: la longueur de la liste
        :rtype: int
        
        :Exemples:
        
        >>> l = Liste()
        >>> len(l)
        0
        >>> g = Liste(1, l)
        >>> len(g)
        1
        >>> h = Liste(2, g)
        >>> len(h)
        2
        """
        pass

def from_python( l ):
    """
    :param l: une liste python
    :type l: list
    :return: une liste récursive
    :rtype: Liste
    :Exemples:
    
    >>> from_python([]).est_vide()
    True
    >>> l = from_python( [ i for i in range(10) ])
    >>> str(l)
    '[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]'
    """
    pass
# Le module ~liste~:9 ends here

# [[file:../listes.org::*Le module ~liste~][Le module ~liste~:12]]
def ajoute_en_fin(l, el):
    """
    renvoie une nouvelle liste contenant les éléments de `l`, suivis de `el`.
    
    :param l: une liste récursive
    :type l: Liste
    :param el: un élément 
    :type el: any
    :return: une nouvelle liste représentant `l`U{`el`}
    :Exemples:
    
    >>> l = Liste()
    >>> for c in 'MEEF':
    ...     l = ajoute_en_fin(l, c)
    >>> str(l)
    '[M, E, E, F]'
    """
    pass
# Le module ~liste~:12 ends here

# [[file:../listes.org::*Ordre décroissant][Ordre décroissant:2]]
def cree_liste_entiers_decroissante(n):
    """
    :param n: un entier
    :type n: int
    :return: la liste des entiers de `n` à 1
    :rtype: Liste
    :CU: n>=0
    :Exemples:

    >>> l = cree_liste_entiers_decroissante(0)
    >>> l.est_vide()
    True
    >>> l = cree_liste_entiers_decroissante(4)
    >>> str(l)
    '[4, 3, 2, 1]'
    """
    pass
# Ordre décroissant:2 ends here

# [[file:../listes.org::*Ordre croissant][Ordre croissant:2]]
def cree_liste_entiers_croissante(n):
    """
    :param n: un entier
    ;type n: int
    :return: la liste des entiers de 1 à `n`
    :rtype: Liste
    :CU: n >= 0
    :Exemples:

    >>> l = cree_liste_entiers_croissante(0)
    >>> l.est_vide()
    True
    >>> l = cree_liste_entiers_croissante(3)
    >>> str(l)
    '[1, 2, 3]'
    """
    pass
# Ordre croissant:2 ends here

# [[file:../listes.org::*Tracer des courbes][Tracer des courbes:4]]
if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=True)
# Tracer des courbes:4 ends here
