
# Table des matières

1.  [Le module `liste`](#org6d76ffd)
2.  [Mesurer la construction des listes](#orgf670f5f)
    1.  [Ordre décroissant](#org31361f9)
    2.  [Ordre croissant](#org6535541)
    3.  [À partir d'une liste python](#org793579d)
    4.  [insertion dans la liste triée](#org7656b81)
3.  [Mener une expérimentation](#org5fa66e5)
    1.  [Mesure du temps : le module `timeit`](#org78697fc)
    2.  [Calcul des temps pour un ensemble de longueurs de listes](#org22adc6e)
    3.  [Tracer des courbes](#org65999e6)

**matériel founi** 

Le [fichier liste.py](./src/liste.py) contenant les signatures et documentations du module liste.


<a id="org6d76ffd"></a>

# Le module `liste`

**à faire**

    Réaliser le module `liste` vu en TD implantant les listes récursives

**à faire**

Réaliser une fonction récursive `ajoute_en_fin(l)` prenant en paramètre une
liste récursive `l` et un élément `el`, et qui renvoie une nouvelle
liste égale à  `l`, suivie de l'élément `el`.

```python
def ajoute_en_fin(l, el):
    """
    renvoie une nouvelle liste contenant les éléments de `l`, suivis de `el`.

    :param l: une liste récursive
    :type l: Liste
    :param el: un élément 
    :type el: any
    :return: une nouvelle liste représentant `l`U{`el`}
    :Exemples:

    >>> l = Liste()
    >>> for c in 'MEEF':
    ...     l = ajoute_en_fin(l, c)
    >>> str(l)
    '[M, E, E, F]'
    """
    pass
```

<a id="orgf670f5f"></a>

# Mesurer la construction des listes


<a id="org31361f9"></a>

## Ordre décroissant

**à faire**

Réaliser une fonction récursive
`cree_liste_entiers_decroissante(n)` prenant en paramètre un entier
`n` et qui renvoie la liste récursive des entiers de `n` à 1 dans
cet ordre.


<a id="org6535541"></a>

## Ordre croissant

**à faire** 

Réaliser une fonction récursive `cree_liste_entiers_croissante(n)` prenant
en paramètre un entier `n` et qui renvoie la liste récursive
contenant les entiers de $`1`$ à $`n`$ dans cet ordre.


<a id="org793579d"></a>

## À partir d'une liste python

**à faire**

Réaliser une fonction `from_python(l)` prenant en paramètre une liste
python `l` et qui renvoie une liste récursive contenant les éléments
de `l` dans le même ordre.

**à faire**

En utilisant la fonction précédente, réaliser une fonction
`cree_liste_aleatoire(n)` prenant en paramètre un entier `n`, et
qui renvoie une liste récursive contenant une permutation choisie
aléatoirement des entiers de $`1`$ à $`n`$.


<a id="org7656b81"></a>

## insertion dans la liste triée

**Fonction de comparaison**

Une fonction de comparaison est une fonction de signature :
`cmp : T -> T -> Union[int,float]` prenant en paramètre
deux éléments de même type `T` et qui renvoie un nombre :

-   positif si `a` est considéré comme supérieur à `b` ;
-   négatif si `a` est considéré comme inférieur à `b` ;
-   nul si `a` et `b` sont équivalents.

**à faire**

-   Réaliser une fonction `cmp_number` permettant de comparer des nombres ;
-   Réaliser une fonction `cmp_string` permettant de comparer des
    chaînes suivant leur longueur.

**à faire**

Réaliser un prédicat `est_triee(l, cmp)` prenant en paramètre une liste récursive `l`
d'éléments comparables avec `cmp` et qui renvoie :

-   `True` si la liste est triée selon la fonction de comparaison `cmp` ;
-   `False` sinon.
    
```python
def est_triee(l, cmp):
    """
    :param l: une liste 
    :type l: Liste 
    :param cmp: une fonction de comparaison
    :type cmp: function
    :return: True si les éléments de `l` sont triés selon `cmp`, False sinon.
    :rtype: bool
    :Exemples:

    >>> est_triee(from_python([1, 2, 3]), cmp_number)
    True
    >>> est_triee(from_python([3, 2, 1]), cmp_number)
    False
    >>> est_triee(List())
    True
    """
    pass
```

**à faire**

Réaliser une fonction `insert_dans_liste_triee(l, el, cmp)` prenant
en paramètre une liste triée `l` , un élément `el` comparable avec
les éléments de `l` selon la fonction `cmp` et qui renvoie une
nouvelle liste triée contenant exactement les éléments de `l` et
l'élément `el`.


<a id="org5fa66e5"></a>

# Mener une expérimentation


<a id="org78697fc"></a>

## Mesure du temps : le module `timeit`

Le module `timeit` de Python permet de prendre une mesure de temps
d'évaluation d'une expression, en secondes. Ce module fournit une
fonction `timeit` qui accepte en entrée trois paramètres :

-   `setup` permet de préciser à `timeit` les modules à charger pour
    permettre l'exécution correcte de l'expression (y compris donc le
    module qui contient la fonction à mesurer) ;
-   `stmt` , pour *statement*, est l'expression dont l'évaluation sera
    mesurée. (donc avec tous ses arguments) ;
-   `number` est le nombre de fois où l'instruction `stmt` sera
    exécutée. Le temps mesuré sera le temps **cumulé** pour toutes ces
    exécutions.

**Remarque :**

Les deux paramètres `setup` et `stmt` sont donnés sous forme d'une
chaîne de caractères.

Par exemple, aprés avoir importé le module `timeit` :

```python
from timeit import timeit
```

On peut mesurer le temps d'exécution de création de la liste des
entiers de $`n`$ à $`1`$ : 

```python
timeit(setup=from liste import cree_liste_entiers_decroissante',
       stmt='cree_liste_entiers_decroissante(10)',
       number=100)
```

<a id="org22adc6e"></a>

## Calcul des temps pour un ensemble de longueurs de listes

**à faire**

Écrire une fonction Python, à deux paramètres : un entier $`n`$ et le
nom d'une fonction de création de liste (`cree_liste_...`), et qui
renvoie la liste des mesures de temps de création pour les listes de
longueurs comprises entre 1 et $`n`$.


<a id="org65999e6"></a>

## Tracer des courbes

Étant donné un ensemble de valeurs dans une liste `l`, on peut
utiliser le module `pylab` de matplotlib pour tracer une courbe où
chaque valeur de la liste `l` à l'indice `i` est comprise comme le
point de coordonnées `i`, `l[i]`.

Par exemple,

```python
import pylab

l = [1,2,4,8,16]
pylab.plot(l)
pylab.show()
```
    
**Remarque :** en général, la méthode `show` de `pylab` ouvre une
fenêtre pour présenter le graphique obtenu, et elle est bloquante,
i.e. elle ne permet plus de dialoguer avec l'interpréteur Python. Il
faut fermer cette fenêtre pour pouvoir poursuivre le dialogue. Avant
la fermeture de cette fenêtre, certaines actions sont possibles sur le
graphique. L'une d'elles est la sauvegarde du graphique dans un
fichier image.

Si on veut que les abscisses soient différents, alors on fournit deux
listes : celles des abscisses et celle des ordonnées :

```python
x = [1,2,3,4,5]
y = [1,2,4,8,16]
pylab.plot(x,y)
pylab.show()
```
On peut améliorer la qualité du graphique produit en spécifiant des
titres, et même une grille qui facilite la lecture :

```python
NBRE_ESSAIS = 100
pylab.title('Temps de création (pour {:d} essais)'.format(NBRE_ESSAIS))
pylab.xlabel('taille des listes')
pylab.ylabel('temps en secondes')
pylab.grid()
pylab.show()
```

**à faire**

Générer un graphique permettant de comparer les coûts de création des
trois types de listes.
