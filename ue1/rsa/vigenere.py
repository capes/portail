#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`vigenere` module
:author: FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>_
:date: janvier 2019
:dernière modification: janvier 2019

Chiffrement de Vigenère

"""
from alphabet import Alphabet, ALPHABETS

def secret_to_key(s, alphabet=ALPHABETS['CAPITAL_LATIN']):
    """
    :param s: (string) a secret word 
    :param alphabet: (Alphabet) used alphabet
    :return: (tuple) secret key 
    """
    return tuple(alphabet.index(c) for c in s)

def key_to_secret(k, alphabet=ALPHABETS['CAPITAL_LATIN']):
    """
    :param k: (tuple of int) tuple of shift
    :param alphabet: (Alphabet) an alphabet
    :return: (str) the secret word used
    """
    return "".join(alphabet[i] for i in k)

def chiffre_message(msg, key, alphabet=ALPHABETS['CAPITAL_LATIN'] ):
    """
    :param msg: (str) message to cipher
    :param key: (tuple) secret key
    :param alphabet: (Alphabet) alphabet used by message
    :return: (str) the ciphered message
    :CU: set(msg).issubset(alphabet)
    """
    l, n = len(key), len(alphabet)
    return "".join([ alphabet[ (alphabet.index(msg[i]) + key[i%l]) % n ]
                     for i in range(len(msg)) ])

def reversed_key( key, alphabet ):
    """
    :param key: (tuple) a secret key
    :param alphabet: (Alphabet) alphabet used
    :return: (tuple) the reverse key
    """
    n = len(alphabet)
    return tuple( -s % n for s in key )

def dechiffre_message(msg, key, alphabet = ALPHABETS['CAPITAL_LATIN']):
    """
    :param msg: (str) the ciphered message 
    :param key: (tuple) secret key
    :param alphabet: (Alphabet) alphabet used by message
    :return: (str) the message
    :CU: set(msg).issubset(alphabet)
    """
    return chiffre_message(msg, reversed_key, alphabet)
