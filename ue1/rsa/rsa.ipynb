{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# RSA : un système de chiffrement à clé publique"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Principe du chiffrement à clé publique"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Comment deux personnes peuvent-elles utiliser un système de chiffrement symétrique pour échanger des messages confidentiels si ces deux personnes n'ont pas de canal de communication sûr pour échanger une clé secrète commune ?\n",
    "\n",
    "Une réponse possible à cette question est d'utiliser un système de chiffrement à clé publique."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Un système de chiffrement à clé publique est un système dans lequel deux clés sont utilisées :\n",
    "\n",
    "* une clé *publique* pour chiffrer un message\n",
    "* et une clé *privée* pour déchiffrer le message chiffré. \n",
    "\n",
    "\n",
    "Dans le cadre d'utilisation d'un système de chiffrement à clé publique, chaque protagoniste dispose d'une paire de clés publique/privée..\n",
    "Par définition la clé publique est publique, elle est donc connue de tous. Seule la clé privée est un secret connu de son seul propriétaire.\n",
    "\n",
    "Le fait que deux clés entrent en jeu, l'une pour chiffrer et l'autre pour déchiffrer, conduit à qualifier les systèmes de chiffrement à clé publique de systèmes *asymétriques* par opposition aux systèmes classiques que l'on qualifie de systèmes *symétriques*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Supposons donc qu'Alice souhaite envoyer un message confidentiel à Bob mais qu'elle n'a pas convenu avec Bob d'un système de chiffrement symétrique et a fortiori ne partage pas de clé secrète avec lui. Supposons que Bob possède une paire de clés publique/privée d'un système de chiffrement asymétrique. Alice se procure la clé publique de Bob (en la lui demandant ou en consultant un annuaire de clés), chiffre son message avec cette clé et envoie à Bob le message chiffré. Lorsqu'il reçoit le message chiffré d'Alice, Bob déchiffre ce message avec sa clé privée et prend connaissance du message d'Alice. Comme il est le seul à posséder la clé privée, il est le seul à pouvoir lire le message d'Alice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le concept de chiffrement à clé publique a été introduit par Diffie et Hellman au milieu des années 1970. De très nombreux systèmes asymétriques ont été, et sont encore, proposés. L'un des plus anciens et des plus largement utilisé de nos jours est le système RSA, élaboré en 1977 par Rivest, Shamir et Adleman. C'est ce système que nous présentons dans la suite."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Génération des clés RSA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Une paire de clés publique/privée RSA se construit à partir de nombres premiers.\n",
    "\n",
    "La **clé publique** est un couple de deux nombres entiers $(n, e)$, $n$ étant le produit de deux nombres premiers distincts $p$ et $q$, et $e$ étant un nombre quelconque premier avec le nombre $\\phi(n) = (p-1)(q-1)$.\n",
    "\n",
    "La **clé privée** est un nombre $d$ inverse de $e$ modulo $\\phi(n)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour construire une paire de clés RSA il faut suivre la procédure suivante :\n",
    "\n",
    "1. trouver deux nombres premiers $p$ et $q$ distincts ;\n",
    "2. calculer $n=p\\cdot q$ ;\n",
    "3. calculer $\\phi(n) = (p-1)(q-1)$ ;\n",
    "4. trouver un nombre $e$ tel que $\\mathrm{pgcd}(\\phi(n), e) = 1$ ;\n",
    "5. calculer $d \\equiv e^{-1} \\pmod{\\phi(n)}$ ;\n",
    "6. rendre publique le couple $(n, e)$ ;\n",
    "7. conserver précieusement $d$, $p$, $q$, $\\phi(n)$.\n",
    "\n",
    "Le nombre $n$ est appelé *modulus*, le nombre $e$ est l'*exposant de chiffrement* et le nombre $d$ l'*exposant de déchiffrement*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Un petit exemple"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On commence par choisir deux nombres premiers distincts à partir desquels on peut calculer le modulus $n$ et le nombre $\\phi(n)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:49.816320Z",
     "start_time": "2019-04-02T09:29:49.811729Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "n = 21, phi(n) = 12\n"
     ]
    }
   ],
   "source": [
    "p, q =  3, 7\n",
    "n = p * q\n",
    "phi =(p - 1)*(q - 1)\n",
    "print('n = {:d}, phi(n) = {:d}'.format(n, phi))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "variables": {
     "phi": "12"
    }
   },
   "source": [
    "Ensuite il nous faut trouver un exposant de chiffrement $e$ qui soit premier avec $\\phi(n)=${{phi}}. Pour cela l'algorithme d'Euclide est d'un grand secours pour calculer le pgcd. Cherchons donc quels sont les candidats potentiels inférieurs à 10."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:50.010009Z",
     "start_time": "2019-04-02T09:29:50.001756Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1, 5, 7, 11, 13, 17, 19]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from arithmetique import *\n",
    "e_candidats = [e for e in range(1, n) if pgcd(e, phi) == 1]\n",
    "e_candidats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "variables": {
     "len([e for e in range(1,n) if pgcd(e, phi) == 1])": "7"
    }
   },
   "source": [
    "Nous avons {{len([e for e in range(1,n) if pgcd(e, phi) == 1])}} candidats. Écartons le premier (on verra pourquoi lorsqu'on abordera le chiffrement) et prenons le second."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:50.191089Z",
     "start_time": "2019-04-02T09:29:50.188343Z"
    }
   },
   "outputs": [],
   "source": [
    "e  = e_candidats[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculons maintenant l'exposant de déchiffrement $d=e^{-1}\\pmod{\\phi(n)}$. C'est l'algorithme d'Euclide étendu qui est cette fois d'un grand secours, puisque $d$ se calcule à partir du coefficient de Bezout de $e$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:50.385561Z",
     "start_time": "2019-04-02T09:29:50.382104Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "d = 5\n"
     ]
    }
   ],
   "source": [
    "d = inverse_mod(e, phi)\n",
    "print('d = {:d}'.format(d))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "variables": {
     "(n, e)": "(21, 5)"
    }
   },
   "source": [
    "La clé publique est le couple $(n, e)$ = {{(n, e)}}."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:50.540337Z",
     "start_time": "2019-04-02T09:29:50.535763Z"
    }
   },
   "outputs": [],
   "source": [
    "cle_publique = (n, e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "variables": {
     "d": "5"
    }
   },
   "source": [
    "La clé privée est le nombre $d$ = {{d}}. Elle doit être tenue secrète par son propriétaire qui ne doit la communiquer à personne.\n",
    "\n",
    "Les nombres premiers $p$ et $q$ ainsi que le nombre $\\phi(n)$ ne servent (en principe) plus à rien. Ils doivent être détruits ou au moins maintenus eux aussi secrets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On considère une paire de clés RSA quelconque : clé publique $(n, e)$, clé privée $d$.\n",
    "1. Montrez que ni l'exposant de chiffrement $e$, ni l'exposant de déchiffrement $d$ ne peuvent être des nombres pairs.\n",
    "2. Montrez qu'en connaissant $n$ et $\\phi(n)$ il est possible de trouver les facteurs premiers $p$ et $q$ de $n$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Chiffrement/déchiffrement RSA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Chiffrement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les messages qu'on chiffre avec une clé publique RSA $(n, e)$ sont des nombres $m\\in\\mathbb{Z}/n\\mathbb{Z}$, autrement dit des nombres compris entre 0 et $n-1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le chiffré correspondant au message $m$ se calcule comme étant\n",
    "\n",
    "$$ c \\equiv m^e\\pmod{n}.$$\n",
    "\n",
    "C'est pourquoi le nombre $e$ est appelé exposant de chiffrement. (c'est aussi pourquoi dans notre exemple de construction d'une paire de clés RSA, nous avons rejeté le candidat 1 pour $e$.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exemple :** Chiffrons le message $m=10$ avec la clé publique $(n, e) = (527, 7)$.\n",
    "\n",
    "\\begin{align*}\n",
    "   10^7 &\\equiv (10^2)^3\\times 10\\pmod{527}\\\\\n",
    "        &\\equiv 100^3\\times 10\\pmod{527}\\\\\n",
    "        &\\equiv 100^2\\times 100\\times 10\\pmod{527}\\\\\n",
    "        &\\equiv 100^2\\times 473\\pmod{527}\\\\\n",
    "        &\\equiv 514\\times 473\\pmod{527}\\\\\n",
    "        &\\equiv 175\n",
    "\\end{align*}\n",
    "\n",
    "Le message chiffré est donc $c=175$.\n",
    "(Notons qu'il a été obtenu en faisant 3 multiplications modulaires.)\n",
    "\n",
    "Vérifions ce résultat avec Python :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:51.666335Z",
     "start_time": "2019-04-02T09:29:51.661439Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "19\n"
     ]
    }
   ],
   "source": [
    "m = 10\n",
    "c = expo_mod_rapide(m, e, n)\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Déchiffrement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour déchiffrer un message, on effectue la même opération que pour le chiffrement, mais en utilisant la clé privée $d$ à la place de l'exposant de chiffrement :\n",
    "$$ m \\equiv c^d\\pmod{n}.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exemple :** Déchiffrons le message $c=175$ obtenu avec la clé publique $(n,e)=(7, 527)$. Nous devons donc utiliser la clé privée correspondante $d=343$.\n",
    "\n",
    "\\begin{align*}\n",
    "   175^{343} &\\equiv (175^2)^{171}\\times 175\\pmod{527}\\\\\n",
    "        &\\equiv 59^{171}\\times 175\\pmod{527}\\\\\n",
    "        &\\equiv (59^2)^{85}\\times 59\\times 175\\pmod{527}\\\\\n",
    "        &\\equiv 319^{85}\\times 312\\pmod{527}\\\\\n",
    "        &\\equiv (319^2)^{42}\\times 319\\times 312\\pmod{527}\\\\\n",
    "        &\\equiv 50^{42} \\times 452 \\pmod{527} \\\\\n",
    "        &\\equiv (50^2)^{21} \\times 452\\pmod{527}\\\\\n",
    "        &\\equiv 392^{21} \\times 452\\pmod{527}\\\\\n",
    "        &\\equiv (392^2)^{10} \\times 392\\times 452\\pmod{527}\\\\\n",
    "        &\\equiv 307^{10} \\times 112\\pmod{527}\\\\\n",
    "        &\\equiv (307^2)^5 \\times 112\\pmod{527}\\\\\n",
    "        &\\equiv 443^5 \\times 112\\pmod{527}\\\\\n",
    "        &\\equiv (443^2)^2 \\times 443\\times 112\\pmod{527}\\\\\n",
    "        &\\equiv 205^2 \\times 78 \\pmod{527}\\\\\n",
    "        &\\equiv 392 \\times 78 \\pmod{527}\\\\\n",
    "        &\\equiv 10\n",
    "\\end{align*}\n",
    "\n",
    "On retrouve donc bien le message clair $m=10$.\n",
    "(Notons qu'il a été obtenu en effectuant 14 multiplications modulaires.)\n",
    "\n",
    "Vérifions ce résultat en Python :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:51.987141Z",
     "start_time": "2019-04-02T09:29:51.981973Z"
    },
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "expo_mod_rapide(c, d, n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercices "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Programmez une fonction ``chiffre(m, kpub)`` qui renvoie le message ``m`` chiffré avec la clé publique ̀``kpub``. Votre fonction devra déclencher une exception ``AssertionError: message incorrect pour la clé`` si le message ``m`` n'est pas dans le domaine autorisé par la clé publique ``kpub``. \n",
    "2. Programmez une fonction ``dechiffre(c, kpriv)`` qui renvoie le message ``c`` déchiffré avec la clé publique ̀``kpriv``. Votre fonction devra déclencher la même exception que la précédente dans les mêmes circonstances."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sécurité RSA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sur quoi repose la sécurité du système RSA ? Essentiellement deux problèmes mathématiques selon ce qu'un adversaire cherche à faire.\n",
    "\n",
    "1. L'adversaire peut chercher à décrypter un message $c$ qui a été chiffrée avec la clé publique $(n, e)$, son problème consiste alors à résoudre l'équation\n",
    "   $$c\\equiv x^e\\pmod{n},$$\n",
    "   où $x$ est l'inconnue. Autrement dit, il s'agit pour lui d'extraire la racine $e$-ème de $c$ modulo $n$.\n",
    "   \n",
    "   Malgré les recherches de nombreux mathématiciens, aucun algorithme efficace n'est connu pour effectuer ce calcul.\n",
    "2. L'adversaire peut aussi chercher à calculer la clé privée $d$ correspondant à une clé publique $(n, e)$. S'il y parvient, il est en mesure de décrypter tous les messages chiffrés avec cette clé publique.\n",
    "   \n",
    "\n",
    "Il est prouvé que ce second problème est équivalent à celui de la factorisation du modulus. Il est en effet clair que si l'adversaire sait factoriser $n$ et ainsi connaître ses facteurs premiers $p$ et $q$ alors il est en mesure de calculer $\\phi(n)$ puis $d$.  Inversement, on peut montrer que si on connaît la clé privée $d$ correspondant à une clé publique $(n,e)$ alors on peut (par une méthode probabiliste) factoriser $n$.\n",
    "\n",
    "Ainsi la sécurité du système RSA est très liée à la factorisation des entiers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dernier record de factorisation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Voici le dernier record de factorisation d'un modulus RSA. Il a été établi par une équipe internationale de cinq chercheurs (dont trois français) qui a annoncé la factorisation du nombre RSA-220 en mai 2016.\n",
    "\n",
    "Ce nombre RSA-220 fait partie d'une liste de défis de factorisation lancés à la communauté internationale. C'est un nombre de 220 chiffres décimaux (d'où son nom) ou encore de 729 bits en binaire.\n",
    "\n",
    "Voici ce nombre :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:52.557558Z",
     "start_time": "2019-04-02T09:29:52.554897Z"
    }
   },
   "outputs": [],
   "source": [
    "RSA_220 = 2260138526203405784941654048610197513508038915719776718321197768109445641817966676608593121306582577250631562886676970448070001811149711863002112487928199487482066070131066586646083327982803560379205391980139946496955261"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "et sa factorisation en deux nombres premiers :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:52.650922Z",
     "start_time": "2019-04-02T09:29:52.647061Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p_220 = 68636564122675662743823714992884378001308422399791648446212449933215410614414642667938213644208420192054999687\n",
    "q_220 = 32929074394863498120493015492129352919164551965362339524626860511692903493094652463337824866390738191765712603\n",
    "RSA_220 - p_220 * q_220"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour en savoir plus sur les records de factorisations de modulus RSA consultez la [page Wikipedia](https://en.wikipedia.org/wiki/RSA_Factoring_Challenge)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vérification de la primalité de `p_220` et `q_220` avec le test fourni par le module `sympy`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:53.135841Z",
     "start_time": "2019-04-02T09:29:52.847691Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from sympy import ntheory\n",
    "ntheory.primetest.isprime(p_220)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:53.144140Z",
     "start_time": "2019-04-02T09:29:53.137706Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ntheory.primetest.isprime(q_220)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conclusion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La sécurité RSA n'est pas connue. Néanmoins RSA est très largement utilisé aujourd'hui (cartes bancaires, Web sécurisé, ...).\n",
    "Aujourd'hui (avril 2017), il est recommandé d'utiliser des clés RSA dont le modulus a une taille d'au moins **2048 bits** (voir par exemple le site de l'[Agence Nationale de la Sécurité des Systèmes d'Informations (ANSSI)](http://www.ssi.gouv.fr))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Utilisation pratique de RSA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Générer des nombres premiers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comme on vient de le voir, les clés RSA doivent être d'une taille d'au moins 2048 bits. Ce qui signifie que le modulus doit être produit de deux nombres d'au moins 1024 bits. De tels nombres sont très grands. De plus les nombres premiers intervenant dans la construction d'une clé ne doivent figurer dans aucune table connue.\n",
    "\n",
    "Il va donc falloir être en mesure de \n",
    "\n",
    "* trouver de très grands nombres premiers pêchés au hasard ;\n",
    "* tester la (pseudo) primalité de ces nombres."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On importe une fonction de tirage de nombre au hasard et un module contenant un test probabiliste de non primalité dû à Miller-Rabin."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:53.450878Z",
     "start_time": "2019-04-02T09:29:53.448160Z"
    }
   },
   "outputs": [],
   "source": [
    "from random import randrange\n",
    "from math import log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Partons à la «pêche» aux nombres premiers de taille 2048 bits.\n",
    "\n",
    "Pour cela, fixons la taille de la clé à 2048 bits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:53.548734Z",
     "start_time": "2019-04-02T09:29:53.546063Z"
    }
   },
   "outputs": [],
   "source": [
    "t = 2048"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Prenons un nombre impair au hasard de taille $\\frac{t}{2} = 1024$ et testons s'il est premier selon le test de Miller-Rabin. S'il ne l'est pas on lui ajoute 2 et on refait le test. Et on recommence tant qu'on n'a pas un nombre premier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:53.654224Z",
     "start_time": "2019-04-02T09:29:53.649301Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p trouvé = 139209683548939488889276999796633608465872823680477685898413613966627650897061868029580366988708071826712000721498809384275322837813195037532059028361254177049730712312271838513168390577871788575245940620332322031198770826908612445616874539533951271838885194959133139738064239677162187652186527309185839426405\n",
      "Taille de p : 1024\n"
     ]
    }
   ],
   "source": [
    "tsur2 = t // 2\n",
    "p = 2 * randrange(2**(tsur2 - 2), 2**(tsur2 - 1)) + 1\n",
    "while not(ntheory.primetest.isprime):\n",
    "    p += 2\n",
    "print('p trouvé = {:d}'.format(p))\n",
    "print('Taille de p : {:d}'.format(int(log(p)/log(2)) + 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Voilà donc une première prise : un nombre premier de la taille voulue. Il reste à repartir à la pêche d'un autre nombre premier de même taille et nous pourrons produire une paire de clés RSA."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Réalisez une fonction ``genere_cles_RSA(t)`` qui renvoie un quintuplet $(n,e,d,p,q)$ constitué d'une clé RSA publique $(n,e)$, d'une clé RSA privée $d$, et des deux facteurs premiers $p$ et $q$ de $n$. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Quels messages chiffre-t-on avec RSA ?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les opérations de chiffrement/déchiffrement RSA sont des exponentiations modulaires de très grands nombres (quelques milliers de bits). Ce sont donc des opérations assez coûteuses en temps de calcul. On estime qu'il existe un facteur 1000 entre la vitesse de chiffrement/déchiffrement de RSA et celle d'un système symétrique (comme l'AES). On n'utilise donc pas RSA pour chiffrer de grosses données.\n",
    "\n",
    "Alors que chiffre-t-on avec RSA ? Essentiellement des clés de systèmes symétriques.\n",
    "\n",
    "Par exemple, supposons qu'Alice et Bob correspondent de manière confidentielle en utilisant un système de chiffrement symétrique (donc rapide). Alice et Bob ne se sont jamais rencontrés pour convenir de la clé à utilser avec ce système.\n",
    "Alice possède une paire de clés RSA. Voici une solution à leur problème :\n",
    "\n",
    "1. Alice envoie sa clé publique à Bob.\n",
    "2. Bob crée une clé $k$ pour le système symétrique. Cette clé est constituée de quelques dizaines/centaines de bits tirés au hasard. \n",
    "3. Il chiffre la clé $k$ avec la clé publique d'Alice, et envoie à Alice le résultat de ce chiffrement.\n",
    "4. Ayant reçu de Bob cette clé chiffrée, Alice utilise sa clé privée pour la déchiffrer. Elle obtient donc la clé $k$.\n",
    "5. À partir de ce moment, Alice et Bob peuvent correspondre de manière confidentielle en chiffrant/déchiffrant leurs messages avec la clé $k$ qu'ils sont les seuls à connaître."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge RSA de 1977"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Défi publié dans la revue Scientific American en 1977, avec 100$ de récompense :\n",
    "\n",
    "Chiffré :\n",
    "      \n",
    "     9686 9613 7546 2206 1477 1409 2225 4355 \n",
    "     8829 0575 9991 1245 7431 9874 6951 2093\n",
    "     0816 2982 2514 5708 3569 3147 6622 8839\n",
    "     8962 8013 3919 9055 1829 9451 5781 5154\n",
    "     \n",
    "Clé publique :\n",
    "\n",
    "     N = 114381625757888867669235779976146\n",
    "         612010218296721242362562561842935\n",
    "         706935245733897830597123563958705\n",
    "         058989075147599290026879543541\n",
    "         \n",
    "     e = 9007"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.294087Z",
     "start_time": "2019-04-02T09:29:54.291711Z"
    }
   },
   "outputs": [],
   "source": [
    "N = 114381625757888867669235779976146612010218296721242362562561842935706935245733897830597123563958705058989075147599290026879543541"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.365262Z",
     "start_time": "2019-04-02T09:29:54.300297Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "128.0583562650788"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "log(N, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$N$ est un entier de 129 chiffres. Peu d'espoir de le factoriser avec des méthodes élémentaires."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Résolu en 1994, en factorisant $N$ par la méthode du [crible quadratique](https://fr.wikipedia.org/wiki/Crible_quadratique) avec une quantité de travail de 5000 MIPS-année"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.492442Z",
     "start_time": "2019-04-02T09:29:54.485591Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "True\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p = 3490529510847650949147849619903898133417764638493387843990820577\n",
    "q = 32769132993266709549961988190834461413177642967992942539798288533\n",
    "print(ntheory.isprime(p))\n",
    "print(ntheory.isprime(q))\n",
    "N - p*q"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "** Exercice : ** Déchiffrez le message du challenge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.643069Z",
     "start_time": "2019-04-02T09:29:54.638428Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "106698614368578024442868771328920154780709906633937862801226224496631063125911774470873340168597462306553968544513277109053606095"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "e = 9007\n",
    "phi = (p-1)*(q-1)\n",
    "d = inverse_mod(e, phi)\n",
    "d"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.714027Z",
     "start_time": "2019-04-02T09:29:54.648934Z"
    }
   },
   "outputs": [],
   "source": [
    "chiffre = 96869613754622061477140922254355882905759991124574319874695120930816298225145708356931476622883989628013391990551829945157815154"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.783394Z",
     "start_time": "2019-04-02T09:29:54.715746Z"
    }
   },
   "outputs": [],
   "source": [
    "clair = expo_mod_rapide(chiffre, d, N)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.895947Z",
     "start_time": "2019-04-02T09:29:54.785483Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "200805001301070903002315180419000118050019172105011309190800151919090618010705"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "clair"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-04-02T09:29:54.966214Z",
     "start_time": "2019-04-02T09:29:54.899029Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE'"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from alphabet import ALPHABETS\n",
    "clair_str = str(clair)\n",
    "''.join(ALPHABETS['CAPITAL_LATIN_SPACE'][int(clair_str[i:i+2])-1] for i in range(0, len(clair_str), 2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "280px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  },
  "widgets": {
   "state": {},
   "version": "1.1.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
