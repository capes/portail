import matplotlib.pyplot as plt
from secret_msg import CYPHER
from alphabet import ALPHABETS

# A COMPLTER

def indice(msg, alphabet = ALPHABETS['PRINTABLE_ASCII']):
    """
    :param msg: (str) a message
    :param alphabet: (Alphabet) the used alphabet
    :return: (float) 
    """
    pass

def cle_cesar(msg, freq_car, alphabet = ALPHABETS['PRINTABLE_ASCII'] ):
    """
    :param msg: (str) a message 
    :param freq_car: (str) character most present in msg

    """
    d = {}
    max_car = 0
    for l in msg:
        if l in d:
            d[l] = d[l]+1
        else:
            d[l] = 1
        if d[l] > max_car:
            max_car = d[l]
            cars = set()
        if d[l] == max_car:
            cars.add(l)
    return tuple( (alphabet.index(c) - alphabet.index(freq_car)) % len(alphabet)
             for c in cars )

X = list(range(1, 30))

# A COMPLETER
Y = [ ]


plt.plot( X, Y, color = 'blue', label = 'indice de coincidence')
plt.legend(loc = "upper left")
plt.show()

