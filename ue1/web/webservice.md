Conception et utilisation d'un webservice
=========================================

Ce document détaille certains des concepts de conception d'un service
web. En particulier, il s'intéresse à la création d'un service sous
forme d'une **api rest**.

# définitions

une api est une interface : il s'agit du contrat que deux programmeurs
doivent respecter pour que le programme écrit par l'un puisse être
exécuté par celui écrit par l'autre.

un web service est une api qui utilise le réseau pour que les deux
programmes communiquent.

  - REpresentational State Transfer est une manière de concevoir des
    webservices.
  - il permet à un client d'accéder et de modifier des ressources
    situées sur un serveur en utilisant le protocole http.
  - Chaque ressouce est identifiée par une URI
  - Les états (state) sont les propriétés courantes de la ressource

![](images/rest_model.png)

# échanges sur http

## structure d'une requête

une requête est composée :

  - d'une méthode au sens http
  - d'une url : adresse de la requête + paramètres
  - d'une entête : métadata de la requête
  - d'un corps : contenu de la requête , optionel

![](images/http_req.png)

``` bash
curl -Ivs http://www.fil.univ-lille.fr
```

## méthodes http

les méthodes http sont utilisées pour spécifier l'action à réaliser. Les
méthodes les plus utilisées sont :

| méthode | action                                       |
| ------- | -------------------------------------------- |
| GET     | lit une information concernant une ressource |
| POST    | crée une nouvelle ressource                  |
| PUT     | met à jour une ressource                     |
| DELETE  | supprime une ressource                       |

## structure de l'url

Une url est composée :

  - du nom du protocole : `http`
  - suivi de `://`
  - suivi du nom de domaine , par exemple `www.exemple.org`
  - suivi du chemin de la resource, par exemple `/api/v1/user/`
  - suivi des paramètres, par exemple `1`

le chemin (ou route) de la resource comprend habituellement :

  - un identifiant le séparant d'un éventuel site internet, par exemple
    `api`
  - la version de l'api, par exemple `v1`
  - le nom de la ressource, par exemple `user`

dans l'architecture rest, les paramètres font partie de l'url. Il ne
doivent pas être confondus avec les paramètres post ou get.

## codes de réponse http

La réponse du serveur comprend :

  - le code de statut ;
  - une entête : les métadonnées associées à la réponse ;
  - un corps : le contenu de la réponse.

### codes de statut

Le code de statut spécifie le statut de la requête. Il s'agit d'un
nombre dont l'écriture décimale comprend trois chiffres, et dont le
premier chiffre indique la classe de la réponse.

| classe de réponse   | description                          | codes communs               |
| ------------------- | ------------------------------------ | --------------------------- |
| 1XX : international | la requete est reçue et              | 100 - continue              |
|                     | est en cours de traitement           |                             |
| 2XX : succès        | la requête a été reçue, comprise     | 200 - OK                    |
|                     | et acceptée                          | 201 - created               |
|                     |                                      | 202 - accepted              |
|                     |                                      | 204 - no content            |
| 3XX - Redirection   | Une action supplémentaire est        | 302 - found                 |
|                     | nécessaire pour compléter la requête |                             |
| 4xx - client erreur | La requete contient une erreur de    | 400 - bad request           |
|                     | de syntaxe ou son traitement est     | 401 - unauthorized          |
|                     | impossible                           | 403 - forbidden             |
|                     |                                      | 404 - not found             |
| 5xx - server error  | le serveur ne parvient pas à traiter | 500 - internal server error |
|                     | la requête valide                    | 503 - service unavailable   |
|                     |                                      |                             |

# les api rest

## caractéristiques

1.  interface cohérente :  
    *les requêtes sur une même ressource partagent une même url*
2.  séparation client-serveur.
3.  communication sans état entre le client et le serveur :  
    *l'état du client est inconnu de celui du serveur, et
    réciproquement*
4.  mise en cache :  
    lorsque cela est possible, les données peuvent être mises en cache
    pour répondre aux requête le plus rapidement possible.
5.  système en couche des entités identifiées et séparées réalisent les
    différentes tâches :

![](images/rest_principles.png)

à noter que le webservice implantant l'architecture rest doit
**toujours** renvoyer une réponse valide, constituée :

  - d'un objet json
  - d'un code de renvoi http même en cas d'erreur.

# Création d'une api rest

Nous allons créer une api rest permettant de gérer une collection de
pokemon.

Les pokemon disposent :

  - d'un nom
  - d'un type
  - d'un nombre de points d'attaque
  - d'un nombre de points de défense
  - d'un nombre de points d'attaque spéciale
  - d'un nombre de points de défense spéciale
  - d'un nombre représentant leur vitesse.

les types peuvent être l'un des types suivants :

| method | url              | description              |
| ------ | ---------------- | ------------------------ |
| GET    | /pokemons        | renvoie tous les pokemon |
| GET    | /pokemons/\<id\> | renvoie un pokemon       |
| POST   | /pokemons        | créé un pokemon          |
| PUT    | /pokemons/\<id\> | mets à jour un pokemon   |
| DELETE | /pokemons/\<id\> | supprime un pokemon      |

## la couche de données

Le serveur de donnée sera modélisé par une classe implantant les
méthodes précédentes en manipulant directement la base de données.

Les pokemons sont stockés dans une base de données relationnelle
contenant une seule table. La commande de création :

``` sql
create table if not exists pokemons (
id integer primary key autoincrement,
name character varying(25) not null,
type  character varying(25) not null,
hp integer not null,
attack integer not null,
defense integer not null,
spAttack integer not null,
spDefense integer not null,
speed integer not null );
```

Les fonctions de la classe `PokeDataLayer` permettent :

  - de créer la base de données ;
  - d'ajouter un pokemon (Create) ;
  - d'obtenir tous ou un pokemon (Retrieve) ;
  - de modifier un pokemon (Update);
  - de supprimer un pokemon (Delete).

Voici le "main" du module `pokemon_data_layer`. Il crée la base de
données à partir du fichier csv `pokemon.csv` situé dans le même
répertoire, mais cela peut être modifié:

``` python
if __name__ == "__main__":
    POKE_CSV_FILE = 'pokemon.csv'
    POKE_DB_FILE = 'pokemon.db'
    from csv import DictReader
    database = PokeDataLayer(POKE_DB_FILE)
    database.create_database()
    with open(POKE_CSV_FILE) as f:
        for pokemon in DictReader(f, delimiter=";"):
            print(f"insert {pokemon} in {database}")
            database.create(pokemon['Name'], pokemon["Type"],
                            pokemon["HP"],
                            pokemon["Attack"], pokemon["Defense"],
                            pokemon["Special Attack"],
                            pokemon["Special Defense"],
                            pokemon["Speed"])
```

**à faire** : écrire le module `pokemon_data_layer` et le tester avec le
main ci-dessus et dbbrowser.

## le serveur de l'api rest

Le serveur utilisera le module `flask`. `flask` est une bibliothèque
permettant d'implanter un serveur http très rapidement.

`flask` fonctionne en décorant des fonctions par les routes
correspondant aux chemins de l'url. La syntaxe autorise que ces chemins
soient paramétrés.

Le fichier `pokemon_rest_api_squel.py` contient le squelette du web
service.

**à faire** : compléter le fichier pour implanter le webservice

**à faire** : utiliser `curl` ou `postman` pour tester les
fonctionnalités du service.

# Du javascript pour interroger le service

On veut créer une page html permettant d'interragir avec le webservice
que nous avons écrit.

On veut concevoir cette page de la manière la plus **dynamique**
possible :

  - aucun appuie de bouton ou événement ne provoque le rafraîchissement
    de la page.
  - À la place :
    1.  une fonction javascript est évaluée.
    2.  Lorsque cela est nécessaire, cette fonction interroge le
        webservice.
    3.  La réponse du webservice déclenche l'exécution d'une deuxième
        fonction …
    4.  … chargée de modifier la page.


## structure de la page et chargement du javascript.

Voici la structure de la page :

```{.html}
<html>
  <head>
    <title>Attrapez les tous</title>
    <meta charset="utf-8" />
    <script defer src="{{url_for('static', filename='js/pokemon.js')}}"></script>
  </head>
  <body>
    <div id="message"></div>
    <section>
      <h1>consultez le pokedex</h1>
      <form method="GET" id="pokeget" action="">
        <input type="number" id="pid" required="" name="" value="" />
        <button type="submit">examiner</button>
      </form>
    </section>
    <section>
      <h1>ajouter un pokemon</h1>
      <form method="POST" id="pokepost" action="">
        <input type="text" name="pname" required=""/>
        <select name="ptype" id="ptype">
          <option value="GRASS">Herbe</option>
          <option value="POISON">Poison</option>
          <option value="FIRE">Feu</option>
          <option value="WATER">Eau</option>
          <option value="BUG">Insecte</option>
          <option value="NORMAL">Normal</option>
          <option value="FLYING">Air</option>
          <option value="GROUND">Terre</option>
        </select>
        <input type="number" id="pattack" required="" name="pattack" value="" />
        <input type="number" id="pdefense" required="" name="pdefense" value="" />
        <input type="number" id="php" required="" name="php" value="" />
        <input type="number" id="pspattack" required="" name="pspattack" value="" />
        <input type="number" id="pspdefence" required="" name="pspdefense" value="" />
        <input type="number" id="pspeed" required="" name="pspeed" value="" />
        <button type="submit">ajouter</button>
      </form>
    </section>
    <section>
      <h1>liste de tous les pokemons</h1>
      <form method="GET" id="pokegetall" action="">
        <button type="submit">obtenir</button>
      </form>
      <div id="result"></div>
    </section>
    <section>
      <h1></h1>
    </section>
  </body>
</html>
```

**analyse** :

1.  la page charge le script `pokemon.js` dans lequel sera réalisé le
    traitement.
2.  notez la présence de l'attribut `defer`, qui permet de différer le
    chargement du script après que l'arbre DOM soit créé.
3.  le div `message` permettra d'afficher les messages tels que :
    "insertion effectuée", "suppression effectuée", ….
4.  Cette page contient deux formulaires :
    1.  un formulaire `GET` permet d'afficher l'ensemble des pokemon ;
    2.  un autre permet d'insérer un nouveu pokemon.
5.  le `div` dont l'identifiant est `result` permet d'afficher les
    résultats.

**à faire** : compléter le formulaire avec les autres caractéristiques
des pokemon. Les champ `name` des éléments html doivent correspondre aux
clés des dictionnaires utilisées par le service.

## installation des gestionnaires

chaque bouton déclenche une requête, chaque réponse à une requête doit
déclencher une modification de l'affichage.

On utilise la programmation événementielle :

``` javascript
document.forms.pokegetall.addEventListener("submit", getAllPokemons);

function getAllPokemons(ev) {
    ev.preventDefault();
    let url = 'http://127.0.0.1:5000/pokemons';
    fetch(url, { method: "GET", }).
        then( response => console.log(response));
}
```

La méthode `addEventListener` permet d'ajouter un gestionnaire
d'événement au bouton `submit`. La fonction `preventDefault` permet
d'inhiber le comportement par défaut (envoie du formulaire).

la fonction `fetch` est une fonction asynchrone : elle sollicite une
requete, installe un ou plusieurs gestionnaires pour traiter la réponse,
puis se termine. Le traitement par le gestionnaire n'est réalisé qu'à la
reception du la réponse, permettant de ne pas bloquer le navigateur.

L'exemple précédent fonctionne si, et seulement si, le serveur d'api
gère les requêtes CORS. CORS est un mécanisme permettant de spécifier
comment des ressources peuvent être accéder à partir d'un autre site
web. Ici notre javascript est considéré comme un "étranger" de la part
de notre service web.

Pour gérer le mécanisme CORS, il faut :

1.  installer le module `flask-cors` :

<!-- end list -->

``` bash
pip install flask-cors
```

1.  Dans le fichier `pokemon_rest_api.py`, charger la fonction `CORS` et
    lui fournir notre application en paramètre :

<!-- end list -->

``` python
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
```

Dès que le gestionaire est installé, le serveur répondra aux requêtes formulées par le script
javascript.

L'exploitation des résultats renvoyés par le web service doit être effectué dans une
fonction spécifique. Vous pouvez constater que le code précédent affiche dans la console
javascript un objet complexe, dans lequel on peut retrouver nos données.

Enrichissons le traitement de la réponse :

``` javascript
document.forms.pokegetall.addEventListener("submit", getAllPokemons);

function processAnswer(response) {
    if (!response.ok) {
        // on ne déclenche pas une exception
        // mais on utilise reject. Comme cela
        // on peut passer l'objet réponse en paramètre
        // à la fonction utilisée dans catch
        return Promise.reject(response);
    }
    return response.json();
}

function showResponseError(response) {
    response.json().then(obj => {
        let div = document.getElementById('message');
        div.innerHTML = obj.error;
        });
}


function getAllPokemons(ev) {
    ev.preventDefault();
    let url = 'http://127.0.0.1:5000/pokemons';
    fetch(url, { method: "GET" })
        .then(processAnswer)
        .then(obj => obj.pokemons)
        .then(pokemons => console.log(pokemons))
        .catch(showResponseError);
}
```

le code précédent permet d'afficher dans la console le tableau des pokemons \!

L'api fetch autorise l'utilisation de plusieurs gestionnaires fournis en paramètre au
méthode `then` : l'argument fourni à l'une de ces méthodes est la valeur renvoyée par la
fonction précédente. En cas d'erreur, c'est la fonction fournie à `catch` qui est
exécutée.

Nous allons créer un tableau html contenant nos résultats, un pokemon par ligne :

``` javascript
function displayPokemons(pokemons) {
    let div = document.getElementById('result');
    div.textContent = "";
    let table = document.createElement('table');
    let row = table.createTHead().insertRow();
    row.insertCell().textContent = 'nom';
    row.insertCell().textContent = 'type';
    row.insertCell().textContent = 'attack';
    row.insertCell().textContent = 'defense';
    row.insertCell().textContent = 'hp';
    let body = table.createTBody();
    pokemons.forEach( function (pokemon) {
        let row = body.insertRow();
        row.insertCell().textContent = poke.name;
        row.insertCell().textContent = poke.type;
        row.insertCell().textContent = poke.attack;
        row.insertCell().textContent = poke.defense;
        row.insertCell().textContent = poke.hp;
    });
    div.appendChild(table);
}
```

**à faire** installer le gestionnaire en remplaçant `console.log` par `displayPokemons`.

Chaque pokemon est affiché sous la forme d'un tableau contenant ses caractéristiques et
proposant les boutons `supprimer` et `modifier`.

Le dernier élément à considérer est la possibilité de créer des requêtes exploitant les
données d'un formulaire. Pour cela, l'api fetch permet d'envoyer les données du formulaire
dans le champ `body` de la requête :

``` javascript
function postPokemon(ev) {
    ev.preventDefault();
    let url = 'http://127.0.0.1:5000/pokemons';
    let data = new FormData(this);
    let obj = {};
    data.forEach((value, name) => obj[name] = value);
    fetch(url, { method: "post",
                 headers: {
                     'Accept': 'application/json, text/plain, */*',
                     "Content-Type" : "application/json"
                 },
                 body: JSON.stringify(obj),
                 credentials: 'same-origin' })
        .then(processAnswer)
        .then( (obj) => {
            let div = document.getElementById('result');
            div.innerHTML = "pokemon " + obj.created + " ajouté";
        }).catch(showResponseError);
}
```

Cette fonction crée la requête post avec les données du formulaire. Dans le contexte de la
fonction `this` désigne l'élément formulaire. Étant donné que notre service attend une
requete d'entête "application/json", il convient de transformer l'objet html de type
`FormData` en texte json grace à la méthode `JSON.stringify`.

À la réception de la réponse, elle affiche l'identifiant du pokemon ajouté.

**à faire** : compléter le script pour implanter l'ensemble des comportements.

**à faire** : créer une css pour améliorer le rendu visuel de la page ... concours de beauté ?


