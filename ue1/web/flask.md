Premiers pas avec flask
=======================

Une architecture client-serveur est d'une manière générale, un modèle
dans lequel un ou plusieurs programmes (les clients), situé(s) sur une
ou plusieurs machine(s) utilisent le réseau pour :

  - demander un service à un programme (le serveur) ;
  - recevoir des données en retour.

On peut programmer un serveur à différents niveaux du modèle,
correspondant à différentes abstractions :

  - en utilisant des socket TCP (module `socket` de python) ;
  - en utilisant le protocole http.

Nous allons utiliser cette dernière possibilité. Là encore, les
possibilités sont multiples, correspondant à différents niveaux
d'abstraction : de la manipulation des flux http aux modules de haut
niveau. Ici nous allons manipuler un module de haut niveau : `flask`.

Flask est un framework python permettant d'écrire rapidement un serveur
web. Flask est disponible dans la distribution fournie sur le site du
capes.

# Installation de Flask

La méthode la plus simple est d'utiliser `pip` :

``` shell
pip install Flask
```

# Ecriture d'un serveur

La mise en place d'un serveur web avec Flask est très simple :

1.  On crée un objet de type `Flask` ;
2.  On écrit des fonctions qui seront appelées lors de l'accès aux
    différentes ressources ;
3.  Pour indiquer les ressources liées à ces fonctions, on utilise un
    décorateur à paramètre, le paramètre étant le chemin (virtuel) vers
    la ressource ;
4.  On entre dans la boucle principale du serveur grace à la méthode
    `run` de la classe `Flask`.

Voici un exemple de serveur (sauvegardé en `hello.py`) qui fournit une
page de démonstration :

``` python
from flask import Flask

app = Flask("Timoléon")

@app.route('/')
def hello ():
    return "Hello World !"

@app.route('/security/server/toto.html')
@app.route('/m2meef')
def ma_fonction():
    return "Hello Méef"

if __name__ == "__main__":
    app.run()
```

Pour démarrer le serveur, on execute le script (dans une console, c'est
sans doute mieux), puis on peut tester avec son navigateur ou `curl` :

``` shell
cd src
python3 hello.py 2 > /dev/null &
curl -i http://localhost:5000
kill -0 $!
```

# Du html

Le serveur `hello.py` se contente de renvoyer un message (en utilisant
le protocole http) sans format. Mais `Flask` peut être utilisé pour
renvoyer des pages html.

Évidemment, il est possible de créer des pages en renvoyant du code html
sous forme de chaîne des caractères :

``` python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return """
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>ma page</title>
   </head>
   <body>
      <h1>Hello World !</h1>
   </body>
</html>"""

if __name__ == "__main__":
    app.run()
```

Cela fonctionne mais :

  - on ne sépare pas les logiques de traitement (le code python et le
    code html sont mélangés) ;
  - l'utilisation de variables est facile, mais peu standardisé.

On peut faire mieux en utilisant des **modèles (ou template)** html :
cela permet d'écrire des fichiers `html` dans lesquelles certaines
parties seront remplacées par `flask`.

Plus précisément, `Flask` utilise le module `jinja2` pour générer les
pages html à partir de modèles.

Voici un exemple de modèle :

``` html
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    <title>{% block title %} {% endblock %}</title>
    <!-- <script src="/static/js/notes.js"></script> -->
    <!-- <link rel="stylesheet" type="text/css" href="/static/css/gest_notes.css"> -->
  </head>
  <body>
    <div class="container">
        {% block content %} {% endblock %}
    </div>
  </body>
</html>
```

Ce modèle peut servir de base à l'ensemble des pages. À l'instar de la
programmation orientée objet, on peut faire hériter certaines pages de
la structure de la page de base (ou d'un autre modèle).

Les directives `{% block title %} {% endblock %}` définissent des blocs
nommés qui peuvent être redéfinis par les pages héritantes :

Les emplacements de la forme `{{ var }}` seront remplacés par le contenu
de la variable python `var`.

La page suivante utilise la page de base, et définit le titre et le
contenu comme deux variables de même nom.

``` html
{% extends 'base.html' %}

{% block content %}
    <h2>{% block title %} {{ titre }} {% endblock %}</h2>
    <p>{{ contenu }}</p>
{% endblock %}
```

On peut même itérer :

``` html
{% extends 'base.html' %}

{% block content %}
    <h2>{% block title %} {{ titre }} {% endblock %}</h2>
    <ul>
      {% for nom, prenom in persons %}
         <li> <span>{{ prenom }}</span><span>{{ nom }}</li>
      {% endfor %}
    </ul>
{% endblock %}
```

La fonction `render_template` permet de servir les pages générées à
partir de ces modèles :

``` python
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def get_meef():
    return render_template('meef.html', 
                           titre='Mon Joli Titre',
                           contenu = 'jinja permet de remplacer des variables, d\'itérer et de tester' )


@app.route('/ue')
def get_meef_persons():
    return render_template('meef_ue.html', 
                           titre='Mon Joli Titre',
                           persons = (('oleon', 'tim'),
                                      ('calbuth', 'Raymond')))

if __name__ == "__main__":
    app.run()
```

``` shell
cd src
python3 hello_template.py >/dev/null &
sleep 2
curl -i http://localhost:5000
curl -i http://localhost:5000/ue
kill $!
```
