interactions client-serveur
===========================

<!-- pandoc -s -t revealjs -V theme=beige -V revealjs-url=https://unpkg.com/reveal.js/ --slide-level 2 -o client_serveur.html client_serveur.md -->

# Communication entre deux programmes

  - deux programmes échangent des données ;
  - roles asymétriques ;
  - client / serveur ;
  - indépendance.

<div class="notes">

  - On parle d'interaction client serveur lorsque deux programmes,
    potentiellement implantés dans des machines distinctes, doivent
    échanger des données.
  - Les deux programmes ne jouent pas le même rôle : l'un souhaite
    accéder à une ressource présente sur l'autre.
  - Le programme qui réclame la ressource est appelé le client, celui
    qui va la lui fournir est appelé le serveur.
  - Indifféremment, ces programmes peuvent être exécutés sur des
    machines géographiquement éloignées … ou pas. En effet, les
    technologies réseaux traitent indifferemment ces cas.

</div>

# Empilement de protocoles

Modèle OSI / Modèle Internet

![](./images/osi.png)

# Quelles abstractions ?

  - différentes abstractions ;
  - niveau le plus abstrait : react ;
  - niveau intermédiaire : serveur http ;
  - niveau basique : connecteur.

<div class="notes">

  - Lorsque deux programmes communiquent, on peut voir cet échange de
    manière plus ou moins abstraite.
  - au niveau le plus élevé, les deux programmes utilisent des
    bibliothèques assurant la communication de manière transparente.
  - au niveau intermédiaire, on utilise on utilise des biblothèques
    permettant de manipuler le serveur ;
  - au niveau le plus bas, on manipule le protocole tcp via des
    connecteurs.

</div>

# Utilisation de connecteurs en python

## Définition d'un connecteur

  - prog : connecteur = interface vers TCP/UDP ;
  - logique : connecteur = extrémités d'un canal ;
  - homogéneité avec les interfaces fichier.

<div class="notes">

  - Les connecteurs fournissent une interface permettant de manipuler
    les protocoles de transport comme TCP et UDP.
  - Un connecteur représente l'extrémité d'un canal de communication,
    associé à un port.
  - Lorsque la communication est établie, on utilise les mêmes
    primitives que celle des fichiers pour communiquer (`read,
                            write`).

</div>

## UDP /TCP

  - TCP : mode connecté. Garantit :
      - La fiabilité ;
      - L'ordre ;
      - Le contrôle du débit ;
      - Le contrôle de la congestion.
  - UDP : mode non connecté.

<div class="notes">

  - TCP : une liaison est établie entre deux hotes, et ensuite un flot
    d'octets est échangé sur cette liaison (mode connecté). Le protocole
    garantit :
      - La fiabilité ;
      - L'ordre ;
      - Le contrôle du débit ;
      - Le contrôle de la congestion.
  - UDP : aucune liaison n'est préalablement établie.

</div>

## Un connecteur client

``` python
import socket
SERVER = ('localhost', 8080)
connecteur_client = socket.socket(socket.AF_INET,
                                  socket.SOCK_STREAM)
connecteur_client.connect(SERVER)
try:
    message = b"Blablabla"
    connecteur_client.sendall( message )          # attend la réponse
    recu = 0
    while recu < len(message):
        data = connecteur_client.recv(16)
        print("recu {}".format(data))
        recu += len(data)
finally:
    connecteur_client.close()
```

## remarques

  - 16 octets maximum ;
  - rien ne garantit le minimum.

## Connecteur serveur

Manipuler un connecteur serveur nécessite quatre opérations :

  - la création du connecteur ;
  - l'association du connecteur avec l'adresse de la machine ;
  - la configuration ;
  - l'attente de connexion.

## connecteur serveur : code

``` python
import socket
from serveur_client import dialogue_avec_client

SERVER_ADDR = ('localhost', 8080 )
LISTEN_LOG_SIZE = 5
print("listen on {}:{}".format(*SERVER_ADDR))
connecteur_serveur = socket.socket(socket.AF_INET, # associé à une ipv4
                                   socket.SOCK_STREAM) # utilise TCP
connecteur_serveur.bind( SERVER_ADDR )
connecteur_serveur.listen( LISTEN_LOG_SIZE )
while True:
    (connexion, client) = connecteur_serveur.accept() # appel bloquant
    print("connexion avec {}:{}".format(*client))
    dialogue_avec_client( connexion, client )
    connexion.close()
```

## dialogue avec le client

``` python
def dialogue_avec_client( connexion, client ):
    data = connexion.recv(16)
    print(f"reçu {data}")
    if data:
        print(f"renvoie le message à {client}")
        connexion.sendall(data.replace(b'a', b'o'))
    else:
        print(f"plus de donnée de {client}")
```

## finalement …

  - On doit tout gérer ;
  - Longueur des transferts ;
  - Avec plusieurs clients ?
  - Exemple : réalisation d'un serveur répéteur.

# Utilisation du protocole http

## Protocole http

  - protocole de transfert hypertexte :
  - utilise TCP ;
  - clients : navigateurs ;
  - plusieurs commandes :
      - GET ;
      - HEAD ;
      - POST ;
      - PUT, DELETE.

## http 1.0

### format des requêtes

``` example
Ligne de commande (Commande, Chemin, Version de protocole)  
En-tête de requête  
[Ligne vide]  
Corps de requête  
```

``` example
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
```

### format des réponses

``` example
Ligne de statut (Version, Code-réponse, Texte-réponse)
En-tête de réponse
[Ligne vide]
Corps de réponse
```

``` example
< HTTP/1.0 200 OK
< Server: BaseHTTP/0.6 Python/3.8.10
< Date: Wed, 13 Oct 2021 16:23:47 GMT
< Content-type: text/html
< 

    <html>
      <head><title>Master MEEF</title></head>
    <body>
    </body>
    </html>
```

## module python

### http.server

Le module python `http.server` permet de mettre en place un serveur
basique.

``` python
from http.server import BaseHTTPRequestHandler, HTTPServer
adresse_serveur = ( "localhost", 8080)
class MonGestionnaire(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("""
        <html><head><title>Master MEEF</title></head>
        <body>
        <p>Request: {}</p>
        <p>Un exemple de serveur http</p>
        </body></html>
        """.format(self.path), "utf-8"))

if __name__ == "__main__":
    serveur_web = HTTPServer(adresse_serveur, MonGestionnaire)
    print("Serveur démarré http://{}:{}".format(*adresse_serveur))
    try:
        serveur_web.serve_forever()
    except KeyboardInterrupt:
        pass
    serveur_web.server_close()
    print("Serveur arrêté.")
```

# Utilisation de bibliothèques de haut niveau

## serveur flask

voir [cours sur flask](./flask.md)

## full stack javascript

à venir … construction d'une simulation d'une machine de Von Neumann ?
