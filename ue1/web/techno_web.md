**Objectifs** : utiliser les technologies du web pour réaliser une
 application de gestion de notes simplifiée.


# Description

Les technologies du web sont un ensemble constitués de protocoles, de
langages, de programmes, d'usages &#x2026; permettant de d'organiser la
mise à disposition et la diffusion d'une information située sur une
machine serveur à une machine cliente.

Évidemment, cette définition tout à fait générique &#x2026;

Ces informations sont le plus souvent des documents au format `html`,
mais aussi des documents `xml`, `svg`, `json` ,&#x2026;

Le web a été créé pour permettre à la communauté scientifique de
partager du contenu.

Il a fallu definir :

-   un langage pour décrir les documents : c'est le langage `html`
-   un protocole pour requérir ces documents et les fournir : c'est le
    protocole `http`

Ces échanges nécessitaient l'utilisation de logiciel adaptés :

-   un navigateur permettant d'effectuer les requetes, de recevoir les
    résultats et de les afficher sous une forme graphique ;
-   un logiciel (le serveur) se mettant à l'écoute de nouvelles
    requetes et fournissant les contenus demandés.

à l'origine les pages étaient 'statiques' : il s'agissait de
fichiers présents sur la machine exécutant le serveur et renvoyés
tel quel.

Très rapidement, la notion de formulaire a fait son apparition. Il
s'agissait de requérir une page tout en transmettant de
l'information du client vers le serveur. Le résultat de la requête
devait être calculé en fonction des données fournies.

Le serveur devait être capable d'exécuter des programmes. Des serveurs
génériques ont permis de rendre cela facile. On peut citer le couple
apache/php largement utilisé, mais aussi les script cgi ou les
frameworks disponibles dans les différents langages de programmation.

L'étape suivante a été de ne pas recharger la page à chaque envoi de
données : favorisé l'apparition de javascript et des navigateurs
compatibles, les standards ont émergés : AJAX, Json.

On souhaite créer une application balayant l'ensemble de ces
technologies. Elle s'affiche dans une page web permettant de gérer
des notes.

Dans cette version simplifiée, on peut :

-   ajouter un devoir ;
-   ajouter un élève ;
-   attribuer une note à un élève.

L'application s'exécutera en mode client-serveur :

-   la partie client gérant l'affichage dans le navigateur ;
-   la partie serveur s'exécutera en python et servira :
    -   les pages html ;
    -   les données grace à des services.

Les données seront stockés dans une base de donnée SQL.


# La couche de données, SQL

**à faire** 

Identifier les relations, leur clé primaire et les clés étrangères.

**à faire**

Donner les commandes sql permettant de créer la base de données

**à faire**

Écrire un module python permettant de gérer la base de données. On
s'appuiera sur le module `sqlite3`.

Les fonctions implantées doivent permettrent, à minima, de :

-   créer la base de données ;
-   ajouter un élève ;
-   ajouter un devoir ;
-   ajouter une note ;
-   récupérer la liste des élèves;
-   récupérer la liste des notes d'un élève;
-   récupérer la liste de toutes les notes.

Les méthodes utiles de la classe `Curssor` sont :

-   `execute` et `executescript` pour exécuter des commandes `sql` ;
-   `lastrowid` qui vaut l'identifiant de la dernière valeur insérée par
    le curseur ;
-   `rowcount` qui vaut le nombre de ligne affectées par la dernière
    commande exécutée par le curseur.
-   `fetchone` et `fetchall()` renvoie les données d'un SELEC.


# Le serveur http

Une architecture client-serveur est d'une manière générale, un modèle
dans lequel un ou plusieurs programmes (les clients), situé(s) sur une ou
plusieurs machine(s) utilisent le réseau pour :

-   demander un service à un programme (le serveur) ;
-   recevoir des données en retour.

On peut programmer un serveur à différents niveaux du modèle,
correspondant à différentes abstractions :

-   en utilisant des socket TCP (module `socket` de python) ;
-   en utilisant le protocole http.

Nous allons utiliser cette dernière possibilité. Là encore, les
possibilités sont multiples, correspondant à différents niveaux
d'abstraction : de la manipulation des flux http aux modules de haut
niveau. Ici nous allons manipuler un module de haut niveau  : `flask`.

Flas est un framework python permettant d'écrire rapidement un
serveur web. Flask est disponible dans la distribution fournie sur
le site du capes.


## Installation de Flask

La méthode la plus simple est d'utiliser `pip` :

```shell
pip install Flask

```


## Ecriture d'un serveur

La mise en place d'un serveur web avec Flask est très simple : 

1.  On crée un objet de type `Flask` ;
2.  On écrit des fonctions qui seront appelées lors de l'accès aux
    différentes ressources ;
3.  Pour indiquer les ressources liées à ces fonctions, on utilise
    un décorateur à paramètre, le paramètre étant le chemin
    (virtuel) vers la ressource ;
4.  On entre dans la boucle principale du serveur grace à la méthode
    `app` de la classe `Flask`.

Voici un exemple de serveur qui sert une page de démonstration :

```python
from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello World !"

if __name__ == "__main__":
    app.run()

```

Pour démarrer le serveur, on execute le script (dans une console,
c'est sans doute mieux), puis on peut tester avec son navigateur ou
`curl` :

```shell
cd src
python3 hello.py 2 > /dev/null &
curl -i http://localhost:5000
kill -0 $!

```

    186b6e36b269bbc86b1adbfa335b24a8


## Du html !!!

Le serveur `hello.py` se contente de renvoyer un message (en
utilisant le protocole http) sans format. Mais `Flask` peut être
utilisé pour renvoyer des pages html.

Évidemment, il est possible de créer des pages en renvoyant du code
html :

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return """
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>ma page</title>
   </head>
   <body>
      <h1>Hello World !</h1>
   </body>
</html>"""

if __name__ == "__main__":
    app.run()

```

Cela fonctionne mais :

-   on ne sépare pas les logiques de traitement (le code python et le
    code html sont mélangés) ;
-   l'utilisation de variables est facile, mais peu standardisé.

On peut faire mieux en utilisant des **modèles** html : cela permet
d'écrire des fichiers `html` dans lesquelles certaines parties
seront remplacées par `flask`.

Plus précisément, `Flask` utilise le module `jinja2` pour générer
les pages html à partir de modèles.

Voici un exemple de modèle :

```html
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{% block title %} {% endblock %}</title>
    <script src="/static/js/notes.js"></script>
    <link rel="stylesheet" type="text/css" href="/static/css/gest_notes.css">
  </head>
  <body>
    <div class="container">
        {% block content %} {% endblock %}
    </div>
  </body>
</html>

```

Ce modèle peut servir de base à l'ensemble des pages. À l'instar de
la programmation orienté objet, on peut faire hériter certaines
pages de la structure de la page de base (ou d'un autre modèle).

Les directives `{% block title %} {% endblock %}` définissent des 
blocs nommés qui peuvent être redéfinis par les pages héritantes :

Les emplacements de la forme `{{ var }}` seront remplacés par le
contenu de la variable python `var`.

```html
{% extends 'base.html' %}

{% block content %}
    <h2>{% block title %} {{ titre }} {% endblock %}</h2>
    <p>{{ contenu }}</p>
{% endblock %}

```

On peut même itérer :

```html
{% extends 'base.html' %}

{% block content %}
    <h2>{% block title %} {{ titre }} {% endblock %}</h2>
    <ul>
      {% for nom, prenom in persons %}
         <li> <span>{{ prenom }}</span><span>{{ nom }}</li>
      {% endfor %}
    </ul>
{% endblock %}

```

```python
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/', methods=[ 'GET' ])
def get_meef():
    return render_template('meef.html', 
                           titre='Mon Joli Titre',
                           contenu = 'jinja permet de remplacer des variables, d\'itérer et de tester' )


@app.route('/ue')
def get_meef_persons():
    return render_template('meef_ue.html', 
                           titre='Mon Joli Titre',
                           persons = (('oleon', 'tim'),('calbuth', 'Raymond')))

if __name__ == "__main__":
    app.run()

```

```shell
cd src
python3 hello_template.py >/dev/null &
sleep 2
curl -i http://localhost:5000
curl -i http://localhost:5000/ue
kill $!

```

    6f5ab2bf59c363d7dd1245a06adcd15d


# Les services

Un service est une entité autonome qui utilise `http` pour envoyer
des données, le plus souvent aux formats `xml` ou `json`. Le service
renvoie toujours une réponse bien formée, même en cas d'erreur.

Nous allons programmer cinq services. Ces cinq services :

1.  Correspondent aux cinq méthodes manipulant la base de données ;
2.  Utilisent la méthode http POST pour récupérer leurs arguments ;
3.  Renvoie une réponse sur la sortie standard sous forme d'un objet
    json.


## Écriture d'un service

Voici un exemple de service :

```python
from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/eleves', methods=[ 'GET', 'POST' ])
def get_all_students():
    return jsonify([ { 'nom' : 'Tim', 'prenom' : 'Oleon'},
                     { 'nom' : 'Calbuth', 'prenom' : 'Raymond' }])


if __name__ == "__main__":
    app.run()

```


## test du service

On peut utiliser curl pour interroger le service :

```shell
cd src
python3 example_server.py >/dev/null &
sleep 2
curl -i http://localhost:5000/eleves
kill $!

```

    HTTP/1.0 200 OK
    Content-Type: application/json
    Content-Length: 70
    Server: Werkzeug/1.0.1 Python/3.8.5
    Date: Tue, 02 Feb 2021 12:25:44 GMT
    
    [{"nom":"Tim","prenom":"Oleon"},{"nom":"Calbuth","prenom":"Raymond"}]


## Services paramétrés

Souvent, les scripts et les services doivent être paramétrés. La
lecture des paramètres se fait en important le sous-module
`request`. La fonction `jsonify` permet quant à elle de sérialiser
des objets en objet json.

Voici un exemple de service prenant en paramètre un nom (variable
`nom` de `POST`) et qui renvoie la note de la personne.

```python
from flask import Flask, jsonify, request

app = Flask(__name__)

NOTES = { 'Tim' : 16, 'Raymond' : 20 }

@app.route('/note', methods = ['GET', 'POST'])
def get_student_notes():
    res = {"status" : "missing"}
    if request.method == 'POST':
        nom = request.form['nom']
        if nom in NOTES:
            res =  { 'status' : 'ok', 
                     'notes' : str(NOTES[nom]) } 
        else:
            res =  { 'status' : 'unknown' } 
    return jsonify(res)

if __name__ == "__main__":
    app.run()

```

```shell
cd src
python3 service_note.py >/dev/null &
sleep 2
curl -d "nom=Tim" http://localhost:5000/note 
kill $!

```

    {"notes":"16","status":"ok"}


## Récupération des paramètres

Les attributs de l'objet `request` permettent d'accéder aux
paramètres :

-   `request.args`: the key/value pairs in the URL query string
-   `request.form` : the key/value pairs in the body, from a HTML
    post form, or JavaScript request that isn't JSON encoded
-   `request.files` : the files in the body, which Flask keeps separate
    from form. HTML forms must use enctype=multipart/form-data or files
    will not be uploaded.
-   `request.values` : combined args and form, preferring args if
    keys overlap
-   `request.json` : parsed JSON data. The request must have the
    application/json content type, or use
    `request.get_json(force=True)` to ignore the content type.

All of these are MultiDict instances (except for json). You can access values using:

-   `request.form['name']` : use indexing if you know the key exists
-   `request.form.get('name')` : use get if the key might not exist
-   `request.form.getlist('name')` : use getlist if the key is sent
    multiple times and you want a list of values. get only returns
    the first value.


# Page principale

La page principale contient le tableau des notes et trois
formulaires pour ajouter :

-   un élève ;
-   un devoir ;
-   une note.

Il convient d'organiser la page en section :

-   une section contenant le tableau ;
-   une section contenant les formulaire (une pou)

Le tableau sera créé en javascript, le contenu de sa section est
donc vide.

Il en va de même pour les `select` du formulaire permettant
d'ajouter une note.

L'application est composée d'une page principale et de six services.

Voici une description fonctionnelle des six services :

-   `/services/ajouteDevoir` : ajoute un nouveau devoir
    -   paramètre (POST) : intitule, coeff
-   `/services/ajouteEleve` : ajoute un nouvel élève
    -   paramètre (POST) : nom, prenom
-   `/services/ajouteNote` : ajoute une nouvelle note
    -   paramètre (POST) : intitule, id<sub>eleve</sub>, note
-   `/services/Notes` : renvoie la liste de toutes les notes
    -   aucun paramètre
    -   renvoie la liste sous la forme d'une liste d'objet json 
        `{ nom, prenom, notes = { ... }}`
-   `/services/Devoirs` : renvoie la liste de tous les devoirs
    -   aucun paramètre
    -   renvoie la liste sous la forme d'une liste d'objets json 
        `{ id, matiere, coeff }`
-   `/services/Eleves` : renvoie la liste de tous les élèves
    -   aucun paramètre
    -   renvoie la liste sous la forme d'une liste d'objets json 
        `{ id_eleve, nom, prenom}`

**Écrire** le script python installant le serveur html et les six services. 

**Remarque** les pages statiques (telles que les scripts javascript)
doivent être installées dans le dossier `templates/static/js/`. Il
faut informer flask de cela en utilisant la commande :

```python
import os

app._static_folder = os.path.abspath("templates/static/")

```

```shell
cd src
python3 gestion_notes.py >/dev/null &
sleep 2
curl -d "id_eleve=1" http://localhost:5000/services/Notes
curl  http://localhost:5000/services/Notes
kill $!


```

    {"status":"ok","value":{"ds0":{"coeff":2,"value":9.0},"ds1":{"coeff":2,"value":3.0},"ds2":{"coeff":2,"value":5.5},"ds3":{"coeff":2,"value":12.5},"ds4":{"coeff":2,"value":7.5}}}
    {"status":"ko","value":"arg missing"}


# L'interactivité

Pour rendre la page dynamique, nous allons :

-   mettre à jour le tableau à intervalle de temps régulier ;
-   remplacer toutes les actions réalisées lors de l'appui sur des
    boutons par des appels au service correspondant.

tout cela en utilisant javascript.

Les méthodes/fonctions/Objets javascript utiles sont :

-   `o.addEventListener(evt, callback)` qui permet d'associer à
    l'événement `evt` (str) d'un objet `o` la fonction `callback`.
-   `fetchFromJson(url)` qui permet d'interroger un service.
-   `document.getElementById( identifiant )` permet de récupérer un
    objet représentant l'élément html dont l'indentifiant est fourni
    sous la forme d'une chaîne de caractère.
-   `document.createElement(type)` crée un objet d'un certain type,
    fourni sous la forme d'une chaîne de caractères, dans le document.
-   `obj1.appendchild(obj2)` ajoute l'objet 2 à l'objet 1.

`createTHead()`, `createTBody`, `insertRow()`, `insertCell()`
permettent de manipuler les tableaux.

On fournit les fonctions `processAnswer` et `fetchFromJson`, qui
permettent respectivement d'analyser une réponse d'un service et
d'envoyer une requête et de récupérer une réponse au format `json`.

```javascript
"use strict";
/*
 *   get query string from FormData object
 *   fd : FormData instance
 *   returns : query string with fd parameters (without initial '?')
 */
function formDataToQueryString (fd){
    return Array.from(fd).map(function(p){return encodeURIComponent(p[0])+'='+encodeURIComponent(p[1]);}).join('&');
}

{
    let makeFetchFunction = function (type) {
        let processResponse = function(response) {
            if (response.ok)
                return response[type]();
            throw Error(response.statusText);
        };
        return function(...args) {
            return fetch(...args).then(processResponse);
        };
    };
    /*
     *   Fetch functions :
     *      same arguments as fetch()
     *      each function returns a Promise resolving as received data
     *      each function throws an Error if not response.ok
     *   fetchText : returns Promise resolving as received data, as string
     *   fetchObject : returns Promise resolving as received data, as object (from JSON data)
     *   fetchFromJson : fetchFromObject alias
     *   fetchBlob : returns Promise resolving as received data, as Blob
     *     ...
     */
    var fetchObject = makeFetchFunction('json');
    var fetchFromJson = fetchObject;
    var fetchBlob = makeFetchFunction('blob');
    var fetchText = makeFetchFunction('text');
    var fetchArrayBuffer = makeFetchFunction('arrayBuffer');
    var fetchFormData = makeFetchFunction('formData');
}

function processAnswer(answer) {
    console.log(answer);
    if (answer.status == "ok")
        return answer.value;
    else
        throw new Error(answer.value);
}

```


## Le gestionnaire d'événements

Chaque appui sur le bouton d'un formulaire exécute une fonction
javascript qui se charge d'appeler le service correspondant et
d'analyser sa réponse.

Au chargement de la page, on installe les gestionnaires
d'événement :

```javascript
window.addEventListener('load', initForms);

function initForms(ev) {
    document.forms.form_eleve.addEventListener('submit', sendAddEleve);

```

Voici un exemple de gestionnaire :

```javascript
function sendAddEleve(ev) {
    ev.preventDefault();
    let url = 'services/ajouteEleve';
    let options = { method : 'post',
                    body : new FormData(this),
                    credentials : 'same-origin' };
    fetchFromJson(url, options)
        .then(processAnswer)
        .then(getNotes, displayError);
}

```

**Écrire** des gestionnaires d'événement pour chacun des services.


## Le tableau et les select

**Écrire** une fonction javascript `displayNotes(devoirs, notes)`
 prenant en paramètre les intitulé des devoirs et les notes sous la
 forme d'une liste d'objet d'attributs nom, prenom et d'intitulés
 de devoir, et qui crée le tableau html correspondant dans la
 section appropriée.

**Écrire** les fonction javascript `selectEleves(eleves)` et
 `selectDevoirs(devoirs)` prenant respectivement en paramètre les
 élèves et les devoirs sous la forme d'une liste d'objets, et qui
 remplis les deux champ select du formulaire `ajoute_note` (les
 valeurs associées sont les identifiants dans la BDD).

Les méthodes permettant de manipuler les tableaux :

-   `createTHead()` : crée une entete
-   `createTBody()` : crée le corps de la table
-   `insertRow()` : crée et insère une ligne
-   `insertCell()` : crée et insère une cellule

Ensuite &#x2026; liez tous cela : un ajout doit provoquer le
rafraichissement du tableau et des select.


# CSS

Les css sont à placer dans le dossier `templates/static/css`.

