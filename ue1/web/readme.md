Éléments de programmation web
=============================

- généralités sur les interactions [client-serveur](./client_serveur.md) ; avec des illustrations sur les méthodes moins abstraites ;
- utilisation de [flask](./flask.md) pour implanter un site internet ;
- utilisation de sqlite et de flask pour implanter un [webservice](./webservice.md)
- programmation html et javascript d'un site utilisant le webservice.
