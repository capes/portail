from flask import Flask, jsonify, request, render_template, make_response
from flask_cors import CORS
from pokemon_data_layer import PokeDataLayer
import os

HTTP_SUCCESS = 200
HTTP_BAD_REQUEST = 400
HTTP_SERVER_ERROR = 500


def str_not_null(arg) -> bool:
    return type(arg) == str and arg != ''


def int_not_null(arg: str) -> bool:
    return type(arg) == str and arg.isdigit()

POKE_FORM_PROPERTIES = {'pname': str_not_null,
                        'ptype': str_not_null,
                        'php': int_not_null,
                        'pattack': int_not_null,
                        'pdefense': int_not_null,
                        'pspattack': int_not_null,
                        'pspdefense': int_not_null,
                        'pspeed': int_not_null}

def check_pokemon_form_values():
    for key, check in POKE_FORM_PROPERTIES.items():
        assert key in request.json, f"{key} is missing"
        value = request.json[key]
        assert check(value), f"{value} is not correct for {key}"

app = Flask(__name__)
app._static_folder = os.path.abspath("templates/static") # pour le js
CORS(app)
data = PokeDataLayer()

@app.route('/', methods=['GET'])
def getIndex() -> str:
    # fichier html à créer dans templates
    # le js dans templates/static/js
    return render_template('pokedex.html')

@app.route('/pokemons', methods=["GET"])
def getPokemons() -> str:
    try:
        res = jsonify({'pokemons': data.retrieve_all()})
        return res
    except:
        make_response(jsonify({'error': 'erreur de bdd'}),
                      HTTP_SERVER_ERROR)


@app.route('/pokemons/<int:pid>', methods=["GET"])
def getPokemonById(pid: int) -> str:
    try:
        ...
    except:
        ...


@app.route('/pokemons', methods=["POST"])
def createPokemon():
    try:
        ...
    except:
        ...


@app.route('/pokemons/<int:pid>', methods=["PUT"])
def updatePokemonById(pid: int):
    try:
        ...
    except:
        ...


@app.route('/pokemons/<int:pid>', methods=["DELETE"])
def deleteById(pid: int):
    try:
        ...
    except:
        ...


if __name__ == "__main__":
    app.run(debug=True)

