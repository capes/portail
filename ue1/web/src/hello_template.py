from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def get_meef():
    return render_template('meef.html', 
                           titre='Mon Joli Titre',
                           contenu = 'jinja permet de remplacer des variables, d\'itérer et de tester' )


@app.route('/ue')
def get_meef_persons():
    return render_template('meef_ue.html', 
                           titre='Mon Joli Titre',
                           persons = (('oleon', 'tim'),
                                      ('calbuth', 'Raymond')))

if __name__ == "__main__":
    app.run()
