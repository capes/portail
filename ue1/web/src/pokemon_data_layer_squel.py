import sqlite3


class PokeDataLayer:

    def __init__(self, fname="pokemon.db"):
        """create an new pokemon datalayer
        Initialiser la connexion à la base avec le paramètre
        check_same_thread à True
        """
        pass

    def create_database(self):
        """create database"""
        pass

    def create(self, name: str, ptype: str, hp: int,
               attack: int, defense: int,
               sp_attack: int, sp_defense: int,
               speed: int) -> int:
        """insert a new pokemon in the database"""
        pass

    def retrieve(self, pid: int) -> dict:
        """retrieve a pokemon given a pokemon id"""
        pass

    def retrieve_all(self) -> list[dict]:
        """retrieve all pokemons"""
        pass

    def update(self, pid: int, properties: dict):
        """update a pokemon"""
        pass

    def delete(self, pid: int):
        """delete a pokemon given a pokemon id"""
        pass


if __name__ == "__main__":
    POKE_CSV_FILE = 'pokemon.csv'
    POKE_DB_FILE = 'pokemon.db'
    from csv import DictReader
    database = PokeDataLayer(POKE_DB_FILE)
    database.create_database()
    with open(POKE_CSV_FILE) as f:
        for pokemon in DictReader(f, delimiter=";"):
            print(f"insert {pokemon} in {database}")
            database.create(pokemon['Name'], pokemon["Type"],
                            pokemon["HP"],
                            pokemon["Attack"], pokemon["Defense"],
                            pokemon["Special Attack"],
                            pokemon["Special Defense"],
                            pokemon["Speed"])
