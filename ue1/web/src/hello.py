from flask import Flask

app = Flask("Timoléon")

@app.route('/')
def hello ():
    return "Hello World !"

@app.route('/security/server/toto.html')
@app.route('/m2meef')
def ma_fonction():
    return "Hello Méef"

if __name__ == "__main__":
    app.run()
