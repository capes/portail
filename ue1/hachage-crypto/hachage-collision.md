Un MAC ou *Message Authentication Code* est un code accompagnant des données dans le but de
s'assurer de l'intégrité de ces dernières, en permettant de vérifier qu'elles n'ont subi aucune
modification, après une transmission par exemple.

Il s'agit ici de calculer un petit bloc authentificateur de taille fixe. Ce bloc est calculé à
partir du message et d'une clé secrète.

![img](img/hmac.png)

Le MAC permet non seulement de vérifier l'intégrité du message, mais également l'authentification de
l'émetteur, détenteur de la clé secrète.

Un **HMAC** ou *Hash based message authentication code* (code d'authentification de message basé sur
le hachage) est un MAC dont le calcul repose sur une fonction de hachage. Toute fonction de calcul
itérative de hachage, comme sha-256 ou sha-512 peut être utilisée dans le calcul du HMAC. Le nom de
l'algorithme résultant est `HMAC-SHA-256, ~HMAC-SHA-512`, &#x2026;

Seuls les participants à la conversation connaissent la clé secrète, et le résultat de la fonction
de hachage dépend des données d'entrées et de la clé secrète. Cela permet d'empêcher les attaques de
type "man-in-the-middle".

La fonction HMAC est définie par :

$HMAC_k(m)=h\left((K\oplus opad)\vert\vert h\left((K\oplus ipad)\vert\vert m\right)\right)$

où

-   $h$ est une fonction de hachage itérative ;
-   $k$ est la clé secrète, hachée par la fonction $h$ si plus longue que sa taille de bloc, puis
    complétée avec des zéros pour qu'elle atteigne la taille de bloc de la fonction $h$;
-   $m$ : le message à authentifier ;
-   $\vert\vert$ désigne la concaténation ; $\oplus$ le "ou" exclusif ;
-   $ipad$ et $opad$ chacune de la taille d'un bloc sont défiinies par $ipad =0\times 3636\ldots
    3636$ et $opad=0\times 5c5c\ldots 5c5c$. Donc si la taille de bloc est 512 bits, $ipad$ et
    $opad$ sont respectivement 64 répétitions des octets 0x36 et 0x5c.

Dans ce tp, nous allons utiliser la fonction de hachage SHA1. En pratique celle-ci ne devrait plus
être utilisée au profit de SHA-256 ou SHA-512.

En python, le module [hashlib](https://docs.python.org/3/library/hashlib.html) implante les
fonctions de hachage et fait partie de la bibliothèque standard. La plupart des messages manipulés
par les fonctions du module hashlib sont fournies sous forme de tableau d'octets. Elles peuvent être
définies :

-   littéralement : `m = b"A\x00\x01"` ;
-   en convertissant une chaîne de caractères; il faut alors préciser l'encodage utilisé : `m =
    bytes("Bonjour", 'utf-8')`

Les chaines d'octets sont indiçables. Le résultat de `ch[i]` est l'entier correspondant au i-eme
octet de la chaine. L'entier dont l'écriture binaire est celle des deux derniers octets de `ch` est
donc

`ch[-2] << 8 | ch[-1]`


# Étude des collisions

1.  En étudiant la documentation du module `hashlib`, trouver comment calculer le condensé SHA1 d'un
    message.
2.  Écrire une fonction python permettant de calculer le condensé SHA1 d'une chaîne de caractères.
3.  Le résultat est tableau de 20 octets = 160 bits. Modifier votre fonction pour qu'elle prenne un
    deuxième paramètre entier $n$, et qu'elle renvoie les $n$ derniers bits du condensé. C'est cette
    fonction que l'on utilisera comme fonction de hachage dans le calcul des collisions.
4.  On fournit le module `lexique` contenant la variable `LEXIQUE`. Cette dernière est une liste de
    139719 mots de la langue française. Écrire une fonction `collision(n)` paramétrée par le nombre
    de bit utilisés pour le calcul du condensé et permettant de trouver l'ensemble des collisions
    (structure de données à définir).
5.  Pour chaque taille de condensé de 6 à 15 bit, calculer le nombre moyen de collisions :

```
6 bits : 2183.109375 collisions en moyenne
7 bits : 1091.5546875 collisions en moyenne
8 bits : 545.77734375 collisions en moyenne
9 bits : 272.888671875 collisions en moyenne
10 bits : 136.4443359375 collisions en moyenne
11 bits : 68.22216796875 collisions en moyenne
12 bits : 34.111083984375 collisions en moyenne
13 bits : 17.0555419921875 collisions en moyenne
14 bits : 8.528812110853377 collisions en moyenne
15 bits : 4.32392535512023 collisions en moyenne
```


# un HMAC

Le module [hmac](https://docs.python.org/3/library/hmac.html) fournit des fonctions pour calculer des HMAC.

1.  Lire la documentation du module pour comprendre l'utilisation du HMAC.
2.  En utilisant la clé "crypto" et le message "ceci est mon premier HMAC SHA1", calculer la sortie
    HMAC sur ces deux entrées, en utilisant la fonction de hachage SHA1. Afficher le résultat en
    hexadécimal.
3.  Modifier la clé et observer le résultat. Combien d'octets les deux chaînes ont-elles en commun ?
4.  Reprendre la clé crypto, mais changer le message en "ceci est ton premier HMAC SHA1". Refaire
    les calculs de la question 2 ; combien d'octets les deux chaînes ont-elles en commun ?
5.  Remplacer SHA1 par SHA-512 et comparer.
