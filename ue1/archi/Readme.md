Architecture des ordinateurs
============================

Au menu, à la carte
-------------------

Vers les circuits logiques
* algèbre de Boole
* portes logiques
* réalisation physique - les transistors
* circuits logiques
* simulateur de circuits logiques

Voire
* au delà de la logique combinatoire : la logique séquentielle

Architecture von Neumann
* machine informatique, ordinateur 
* jeu d'instructions
* assembleur
* modèle von Neumann

Systèmes sur puce
* composants, intégration

Informatique embarquée
* actionneur, capteur
* robotique (robotique scolaire)

Dans les programmes
-------------------

[Programmes et ressources NSI - eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-gt)
* [programme NSI 1re - BO](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)
* [programme NSI Tale - BO](https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf)
* _également sur cette page_ :
  - ressource [Modèle d'architecture de von Neumann - educsol](https://cache.media.eduscol.education.fr/file/NSI/76/9/RA_Lycee_G_NSI_arch_von_neu_1170769.pdf)
  - sitographie pour le programme de Terminale - [Composants intégrés d'un système sur puce](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-gt#summary-item-20) 

[Programmes et ressources SNT - eduscol](https://eduscol.education.fr/1670/programmes-et-ressources-en-sciences-numeriques-et-technologie-voie-gt)
* [programme SNT - BO](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf)
* _également sur cette page_ :
  - ressources d'accompagnement [Thème Informatique embarquée et objets connectés](https://eduscol.education.fr/1670/programmes-et-ressources-en-sciences-numeriques-et-technologie-voie-gt#summary-item-7)

[Site du jury du CAPES NSI](https://capes-nsi.org/)
* [épreuves orales](https://capes-nsi.org/index.php?id=epreuves-orales) 
  - [liste des leçons session 2021](https://capes-nsi.org/data/uploads/2021/liste_lecons_2021.pdf) 

La pile débranchée
------------------

* [Pile débranchée](../pile_debranchee/Readme.md) 
  - appel de fonction, la pile d'exécution
  - activité d'informatique sans ordinateur

M999 - le processeur débranché
------------------------------

* [M999 - une architecture von Neumann](m999.md) - support de travaux dirigés
  - [m10.svg](m10.svg) - M-10 la machine débranchée 
  - [m10-io.svg](m10-io.svg) - M-10io la machine débranchée, entrées/sorties
  - [m999-memoire.svg](m999-memoire.svg) - mémoire de la machine M999 
  - [m999-exo1.svg](m999-exo1.svg) - exo1, une mémoire de M999 initialisée
  - [m999-isa.md](m999-isa.md) - jeu d'instruction de M999

Architecture von Neumann
------------------------

* [Architecture des ordinateurs](2020-01-capes-archi.pdf) - support de présentation 

Ressources bibliographiques (_PDF disponibles sur demande_)

* _First draft of a report on the EDVAC_,
  J. von Neumann,
  Moore School of Elec. Eng., U. of Pennsylvania, Philadelphia, Pa.,
  June 30, 1945 (101 pp.).
  IEEE Annals of the History of Computing, Volume 15, Issue 4, 1993
  [doi.org/10.1109/85.238389](https://doi.org/10.1109/85.238389)
* _Von Neumann's First Computer Program_,
  Donald E. Knuth, 
  ACM Computing Surveys, Volume 2, Issue 4, Dec. 1970 pp 247–260
  [doi.org/10.1145/356580.356581](https://doi.org/10.1145/356580.356581)
* _The computer as von Neumann planned it_
  M.D. Godfrey; D.F. Hendry
  IEEE Annals of the History of Computing, Volume 15, Issue 1, 1993
  [doi.org/10.1109/85.194088](https://doi.org/10.1109/85.194088)

Pour mémoire
------------

* [UE2 - Fonctions booléennes et circuits](../../ue2/logisim/Readme.md) 
* [UE2 - Circuits séquentiels](../../ue2/sequentiel/readme.md) 

<!-- eof -->

