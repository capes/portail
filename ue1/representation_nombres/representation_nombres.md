---
title: Représentation des nombres
author: Éric Wegrzynowski
date: septembre 2019
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.1.7
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---




# Bases de numération


<!-- #region -->
On écrit usuellement les nombres entiers en utilisant les 10 chiffres 0, 1, 2, ..., 9. Dans l'écriture décimale chaque chiffre utilisé représente un nombre dont la valeur dépend de la position dans l'écriture. 

$$ 2019 = 2\times 10^3 + 0\times 10^2 + 1\times 10^1 + 9\times 10^0.$$


Représenter un nombre en base $b$, $b$ étant n nombre entier au moins égal à 2, c'est 

* se donner $b$ symboles différents, appelés *chiffres*, chacun d'eux associé à une valeur entière comprise entre 0 et $b-1$,
* et décomposer le nombre à représenter en une somme de puissances de la base coefficientées par un nombre compris entre 0 et $b-1$.

Par exemple, en considérant le nombre $n=86$ et la base $b=3$ on a
$$ n = 81 + 3 + 2 = 1\times 3^4 + 0\times 3^3 + 0\times 3^2 + 1\times 3^1 + 2\times 3^0,$$
et par conséquent en utilisant les trois chiffres $\mathtt{0}$, $\mathtt{1}$ et $\mathtt{2}$, le nombre $n$ peut s'écrire en base 3
$$ 86 = \overline{\mathtt{10012}}_3.$$

Le même nombre décomposé en base 2 donne
$$ 86 = 64 + 16 + 4 + 2 = 1\times 2^6 + 0\times 2^5 + 1\times 2^4 + 0\times 2^3 + 1\times 2^2 + 1\times 2^1 + 0\times 2^0,$$
et ainsi ce nombre s'écrit en base 2
$$ 86 = \overline{\mathtt{1010110}}_2.$$


De manière générale, quelque soit la base $b\geq 2$ et l'entier naturel $n$, il existe une suite de nombres $c_0, c_1, \ldots, c_{p-1}$ tous compris entre 0 et $b-1$ tels que

$$ n = \sum_{k=0}^{p-1}c_kb^k.$$

Les nombres $c_k$ sont les *chiffres* de l'écriture en base $b$ de l'entier $n$. 
<!-- #endregion -->

## Trouver l'écriture d'un entier naturel dans une base donnée 


L'algorithme classique pour déterminer l'écriture en base $b$ d'un nombre entier $n$ consiste à effectuer des divisions euclidiennes successives par $b$, jusqu'à obtenir un quotient nul. Les restes successifs, depuis le dernier jusqu'au premier donnent l'écriture, de gauche à droite, de l'entier $n$ en base $b$.

Reprenons les exemples précédents avec $n= 86$ et $b=3$ d'abord puis $b=2$ ensuite.

\begin{align*}
  86 &= 3\times 28 + 2\\
  28 &= 3\times 9 + 1\\
   9 &= 3\times 3 + 0\\
   3 &= 3\times 1 + 0\\
   1 &= 3\times 0 + 1
\end{align*}
En regroupant les restes obtenus en «remontant» on retrouve l'écriture $86=\overline{\mathtt{10012}}_3$.

\begin{align*}
  86 &= 2\times 43 + 0\\
  43 &= 2\times 21 + 1\\
  21 &= 2\times 10 + 1\\
  10 &= 2\times 5 + 0\\
   5 &= 2\times 2 + 1\\
   2 &= 2\times 1 + 0\\
   1 &= 2\times 0 + 1
\end{align*}
En regroupant les restes obtenus en «remontant» on retrouve l'écriture $86=\overline{\mathtt{1010110}}_2$.


<!-- #region -->
**Exercice** Programmez une fonction qui renvoie la représentation en base $b$ d'un entier naturel $n$ sous forme d'une liste d'entiers compris entre 0 et $b-1$, chiffres de l'écriture en base $b$ dans l'ordre croissant des puissances de $b$.

NB : pour l'entier $n=0$ la liste renvoyée est la liste vide.

```python
def en_base(nbre, base):
    '''
    :param nbre: (int) le nombre à convertir
    :param base: (int) la base souhaitée
    :return: (list) une liste de chiffres représentant le nombre 
             dans la base demandée
    :CU: nbre >= 0 et 2 <= base
    :Exemples:
    
    >>> en_base(86, 10)
    [6, 8]
    >>> en_base(86, 3)
    [2, 1, 0, 0, 1]
    >>> en_base(86, 2)
    [0, 1, 1, 0, 1, 0, 1]
    >>> en_base(86, 16)
    [6, 5]
    >>> en_base(86, 32)
    [22, 2]
    >>> en_base(0, 10)
    []
    '''
```
<!-- #endregion -->

```python
def en_base(nbre, base):
    '''
    :param nbre: (int) le nombre à convertir
    :param base: (int) la base souhaitée
    :return: (list) une liste de chiffres représentant le nombre 
             dans la base demandée
    :CU: nbre >= 0 et 2 <= base
    :Exemples:
    
    >>> en_base(86, 10)
    [6, 8]
    >>> en_base(86, 3)
    [2, 1, 0, 0, 1]
    >>> en_base(86, 2)
    [0, 1, 1, 0, 1, 0, 1]
    >>> en_base(0, 10)
    []
    
    version itérative
    '''
    res = []
    while nbre > 0:
        nbre, r = divmod(nbre, base)
        res.append(r)
    return res
```

```python jupyter={"source_hidden": true}
[en_base(86, base) for base in (2, 3, 10, 16, 32)]
```

```python jupyter={"source_hidden": true}
[en_base(k, 2) for k in (0, 1, 2, 4, 5, 8, 9, 15, 16, 17)]
```

```python jupyter={"source_hidden": true}
def en_base2(nbre, base):
    '''
    :param nbre: (int) le nombre à convertir
    :param base: (int) la base souhaitée
    :return: (list) une liste de chiffres représentant le nombre 
             dans la base demandée
    :CU: nbre >= 0 et 2 <= base
    :Exemples:
    
    >>> en_base2(86, 10)
    [6, 8]
    >>> en_base2(86, 3)
    [2, 1, 0, 0, 1]
    >>> en_base2(86, 2)
    [0, 1, 1, 0, 1, 0, 1]
    >>> en_base2(0, 10)
    []
    
    version récursive
    '''
    if nbre == 0:
        return []
    else:
        q, r = divmod(nbre, base)
        return [r] + en_base2(q, base)
```

```python jupyter={"source_hidden": true}
[en_base2(86, base) for base in (2, 3, 10, 16, 32)]
```

```python jupyter={"source_hidden": true}
[en_base2(k, 2) for k in (0, 1, 2, 4, 5, 8, 9, 15, 16, 17)]
```


## Trouver l'entier naturel correspondant à une représentation donnée


**Exercice** Programmez la fonction réciproque de la précédente. Elle doit prendre une base et une liste de chiffres de cette base en paramètres et renvoyer l'entier représenté par cette liste dans cette base.

```python jupyter={"source_hidden": true}
def en_entier1(l_chiffres, base):
    '''
    :param l_chiffres: (list) liste de chiffres
    :param base: (int) base de représentation
    :return: (int) l'entier représenté par l_chiffres en base base
    :CU: l_chiffres ne contient que des entiers positifs ou nuls inférieurs 
         strictement à base
         2 <= base
    :Exemples:
    
    >>> en_entier([6, 8], 10)
    86
    >>> en_entier([2, 1, 0, 0, 1], 3)
    86
    >>> en_entier([0, 1, 1, 0, 1, 0, 1], 2)
    86
    >>> en_entier([], 10)
    0
  
    '''
    res = 0
    for k in range(len(l_chiffres)):
        res += l_chiffres[k] * base**k
    return res
```

```python jupyter={"source_hidden": true}
[en_entier1(n, b) for n,b in (([6, 8], 10), ([2, 1, 0, 0, 1], 3), 
                          ([0, 1, 1, 0, 1, 0, 1], 2), ([], 10))]
```

**Remarques sur l'algorithme utilisé par la fonction `en_entier1`**

* l'algorithme est évidemment correct puisque la variable `res` calculée a une valeur finale qu'on peut exprimer par

$$ \mathtt{res} = \sum_{k=0}^{p-1} \mathtt{l\_chiffres[k]}\times \mathtt{base}^k,$$

$p$ désignant la longueur de la liste `l_chiffres`. Cette somme n'est rien d'autre que l'expression de la décomposition en base `base` de res`.

* Quel est le nombre d'opérations arithmétiques effectuées ?
  
  À chaque itération, nous avons une addition, une multiplication et une exponentiation. Supposons que Python calcule une exponentiation $x^k$ en effectuant $k$ multiplications. Les nombres $A$ d'additions et $M$ de multiplications pour calculer `res` sont donc donnés par les formules
  
  \begin{align*}
    A &= \sum_{k=0}^{p-1} 1 = p\\
    M &= \sum_{k=0}^{p-1} (1 + k) = \frac{p(p+1)}{2}.
  \end{align*}
  
* Est-il possible de réduire le nombre de multiplications ?


La réponse à cette dernière question est oui. L'idée de la réduction du nombre de multiplications repose sur le fait que si on a calculé $b^k$, il n'est pas nécessaire d'effectuer $k+1$ multiplications pour calculer ensuite $b^{k+1}$ : une multiplication supplémentaire suffit en multipliant $b^k$ par $b$.

Et on peut systématiser simplement cette idée de la façon suivante illustrée sur le calcul du nombre écrit $\overline{\mathtt{10012}}_3$ en base 3.

\begin{align*}
  \overline{\mathtt{10012}}_3 &= 1\times 3^4 + 0\times 3^3 + 0\times 3^2 + 1\times 3^1 + 2\times 3^0\\
  &= (1\times 3^3 + 0\times 3^2 + 0\times 3^1 + 1\times 3^0)\times 3 + 2\\
  &= ((1\times 3^2 + 0\times 3^1 + 0\times 3^0)\times 3 + 1)\times 3 + 2\\
  &= (((1\times 3^1 + 0\times 3^0)\times 3 + 0)\times 3 + 1)\times 3 + 2\\
  &= (((1\times 3 + 0)\times 3 + 0)\times 3 + 1)\times 3 + 2\\
\end{align*}

On s'aperçoit alors qu'une seule addition et une seule multiplication suffisent à chaque étape de l'itération. Si le nombre d'étapes est $p$, les nombres d'additions et de multiplications valent tous deux $p$.

L'algorithme sous-jacent à cette idée est un cas particulier d'un algorithme connu sous le nom de [méthode de Horner](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Ruffini-Horner). 

```python jupyter={"source_hidden": true}
def en_entier(l_chiffres, base):
    '''
    :param l_chiffres: (list) liste de chiffres
    :param base: (int) base de représentation
    :return: (int) l'entier représenté par l_chiffres en base base
    :CU: l_chiffres ne contient que des entiers positifs ou nuls inférieurs 
         strictement à base
         2 <= base
    :Exemples:
    
    >>> en_entier([6, 8], 10)
    86
    >>> en_entier([2, 1, 0, 0, 1], 3)
    86
    >>> en_entier([0, 1, 1, 0, 1, 0, 1], 2)
    86
    >>> en_entier([], 10)
    0
    
    algorithme de Horner version itérative
    '''
    res = 0
    for i in range(len(l_chiffres)-1, -1, -1):
        res = base*res + l_chiffres[i]
    return res
```

```python jupyter={"source_hidden": true}
[en_entier(lchiffres, base) for lchiffres, base in (([6, 8], 10), 
                                                    ([2, 1, 0, 0, 1], 3), 
                                                    ([0, 1, 1, 0, 1, 0, 1], 2),
                                                    ([], 10))]
```

```python jupyter={"source_hidden": true}
def en_entier2(l_chiffres, base):
    '''
    :param l_chiffres: (list) liste de chiffres
    :param base: (int) base de représentation
    :return: (int) l'entier représenté par l_chiffres en base base
    :CU: l_chiffres ne contient que des entiers positifs ou nuls inférieurs 
         strictement à base
         2 <= base
    :Exemples:
    
    >>> en_entier([6, 8], 10)
    86
    >>> en_entier([2, 1, 0, 0, 1], 3)
    86
    >>> en_entier([0, 1, 1, 0, 1, 0, 1], 2)
    86
    >>> en_entier([], 10)
    0
    
    algorithme de Horner version récursive
    '''
    if l_chiffres == []:
        return 0
    else:
        return l_chiffres[0] + base * en_entier2(l_chiffres[1:], base)
```

```python jupyter={"source_hidden": true}
[en_entier2([0, 0, 1], base) for base in (10, 2, 16)]
```

## Passer de la base 2 à la base 8 ou 16 et réciproquement


Connaissant l'écriture binaire d'un nombre entier, il est facile d'en déterminer l'écriture en base 8 ou 16, ou toute autre base qui est une puissance de 2.
Il suffit pour cela de regrouper les bits par paquet de 3 bits pour la base 8, ou quatre bits pour la base 4, ou plus généralement par paquets de $k$ bits si on veut la base $2^k$, et ceci en parcourant les bits en commençant par le bit de poids faible. Chaque paquet correspond à l'écriture binaire d'un chiffre de la base voulue.

Par exemple, l'écriture binaire du nombre 

$$ 86 = \overline{\mathtt{1010110}}_2$$
se décompose en trois paquets de trois bits : $\mathtt{1}$, $\mathtt{010}$ et $\mathtt{110}$, correspondant respectivement aux chiffres $\mathtt{1}$, $\mathtt{2}$ et $\mathtt{6}$ de la base 8.
Ainsi on trouve sans effort que 
$$ 86 = \overline{\mathtt{126}}_8.$$

L'écriture binaire du même nombre se décompose en deux paquets de quatre bits : $\mathtt{101}$ et $\mathtt{0110}$, correspondant respectivement aux chiffres $\mathtt{5}$ et $\mathtt{6}$ de la base 16. Ainsi on trouve l'écriture hexadécimale du même nombre :
$$ 86 = \overline{\mathtt{56}}_{16}.$$

```python
def base2_a_2puissancek(l_bits, k):
    '''
    :param l_bits: (list) une liste de bits représentant l'écriture binaire d'un entier
    :param k: (int) un entier
    :return: (list) une liste de nombres représentant le même nombre entier en base 2^k
    :CU: 1 <= k et l_bits ne contient que des 0 ou 1
    :Exemples:
    
    >>> base2_a_2_puissancek([0, 1, 1, 0, 1, 0, 1], 3)
    [6, 2, 1]
    >>> base2_a_2_puissancek([0, 1, 1, 0, 1, 0, 1], 4)
    [6, 5]
    
    version iterative
    '''
    res = []
    for i in range(0, len(l_bits), k):
        res.append(en_entier(l_bits[i: i+k], 2))
    return res
```

```python jupyter={"source_hidden": true}
base2_a_2puissancek([0, 1, 1, 0, 1, 0, 1], 3)
```

## Taille d'un entier


Le nombre de chiffres nécessaires à l'écriture d'une entier $n$ dans une base $b$ est appelé *taille de $n$ en base $b$* et nous noterons $|n|_b$ cette taille.

À titre d'exemple, voici les tailles de l'entier $n=86$ dans les bases 2, 3, 10, et 16 :

\begin{align*}
  |86|_{2} &= 7\\
  |86|_{3} &= 5\\
  |86|_{10} &= 2\\
  |86|_{16} &= 2.\\
\end{align*}

**À noter** que dans la définition de cette taille on ne tient compte que des chiffres **nécessaires** à son écriture. On peut toujours ajouter le chiffre 0 en tête d'une écriture. Ainsi le nombre 86 peut s'écrire en base 10 $\overline{\mathtt{86}}_{10}$, $\overline{\mathtt{086}}_{10}$, $\overline{\mathtt{0086}}_{10}$, ... La taille est donc le nombre de chiffres dans une écriture sans zéros superflus en tête.

**Remarque :** quid de $n=0$ ? Dans toute base ce nombre s'écrit  $\overline{\mathtt{0}}_b$. Il faut un chiffre et ce chiffre est 0. C'est donc une exception à la règle énoncée ci-dessus. 


Comment calculer la taille d'un entier ? Comment varie la taille d'un entier ?


**Exercice** Réalisez une fonction qui calcule la taille d'un entier $n$ en base $b$, ces deux nombres étant passés en paramètres en utilisant l'une des fonctions précédentes.

```python jupyter={"source_hidden": true}
# première version s'appuyant sur la fonction en_base précédemment définie
def taille1(nbre, base):
    '''
    :param nbre: (int) un entier naturel non nul
    :param base: (int) une base
    :return: (int) la taille de nbre en base base
    :CU: nbre > 0, 2 <= base
    :Exemples:
    
    >>> taille1(100, 2)
    7
    >>> taille1(100, 10)
    3
    >>> taille1(100, 100)
    2
    >>> taille1(100, 101)
    1
    '''
    return len(en_base(nbre, base))
```

```python jupyter={"source_hidden": true}
[taille1(100, base) for base in (2, 10, 100, 101)]
```

**Exercice** Soit $p$ un nombre entier non nul, et $b$ une base quelconque. Quels sont les nombres entiers naturels $n$ tels que $|n|_b \leq p$ ? Combien y en a-t-il ?

<!-- #region -->
**Réponses**

La réponse à la seconde question est $b^p$. En effet un nombre dont la taille au maximum $p$ en base $b$ peut s'écrire avec un mot de longueur $p$ constitué de chiffres qui peuvent prendre chacun $b$ valeurs possibles (on s'autorise des zéros superflus en tête).

Quels sont ces nombres ? Ce sont les nombres entiers compris entre 0 et $b^p-1$, bornes incluses. On peut justifier cette affirmation 

1. par un argument de dénombrement : il y a $b^p$ nombres entiers dans l'intervalle $[0, b^p-1]$.

2. par un argument algébrique : le plus petit nombre est évidemment 0. Le plus grand est le nombre qui s'écrit avec $p$ fois le chiffre représentant le nombre $b-1$.
   Ce nombre s'écrit
   $$ n = \sum_{k=0}^{p-1}(b-1)b^k,$$
   et le calcul donne
   \begin{align*}
     n &= (b-1)\sum_{k=0}^{p-1}b^k\\
       &= (b-1)\frac{b^p - 1}{b-1}\\
       &= b^p - 1.
   \end{align*}


Si maintenant on cherche le nombre de mots de taille exactement $p$, alors le chiffre de poids fort (le plus à gauche) ne peut pas être $\mathtt{0}$. Ce chiffre de poids fort ne peut être que l'un des $b-1$ autres chiffres. Les autres pouvant être quelconques on en déduit que le nombre de nombres entiers de taille $p$ en base $b$ est égal à $(b-1)b^{p-1} = b^p - b^{p-1}$. Et ces nombres sont ceux compris entre $b^{p-1}$ et $b^p-1$, bornes incluses.

<!-- #endregion -->

D'après ce qui précède, la taille d'un entier $n$ non nul est l'unique entier $p$ tel que 
$$ b^{p-1} \leq n < b^p.$$
On peut calculer la taille d'un entier en cherchant cet entier $p$ par une exploration systématique des entiers 1, 2, 3, ... jusqu'à trouver l'entier qui convient.

```python jupyter={"source_hidden": true}
def taille2(nbre, base):
    '''
    :param nbre: (int) un entier naturel non nul
    :param base: (int) une base
    :return: (int) la taille de nbre en base base
    :CU: nbre > 0, 2 <= base
    :Exemples:
    
    >>> taille2(100, 2)
    7
    >>> taille2(100, 10)
    3
    >>> taille2(100, 100)
    2
    >>> taille2(100, 101)
    1
    '''
    res = 1
    bpuissp = base
    while nbre >= bpuissp:
        res += 1
        bpuissp *= base
    return res
```

```python jupyter={"source_hidden": true}
[taille2(100, base) for base in (2, 10, 100, 101)]
```

**Remarques :**

* cette seconde version du calcul de la taille est meilleure que la première parce qu'elle effectue moins de calculs : $p-1$ incrémentations et  multiplications (1 par étape de l'itération) pour la seconde version alors que la première version demande le calcul de la liste des chiffres et par conséquent $p$ divisions euclidiennes. 

* ni la première, ni la seconde méthode de calcul ne nous renseignent sur comment croît la taille quand l'entier croît.


**À la recherche d'une formule** 

Soit $n$ un entier naturel non nul de taille $p$ en base $b$. Peut-on exprimer $p$ en fonction de $n$ ?
On sait qu'on a l'encadrement
$$ b^{p-1} \leq n < b^p.$$
En passant au logarithme de base $b$ qui est une fonction strictement croissante, on obtient
$$ (p-1)\log_b{b} \leq \log_b{n} < p\log_b{b},$$
et donc
$$ p-1 \leq \log_b{n} < p.$$
Ainsi $\log_b{n}$ est un nombre réel compris entre les deux entiers consécutifs $p-1$ et $p$, $p$ étant exclus. Par conséquent la partie entière de $\log_b{n}$ est égale à $p-1$. Autrement dit $p=\lfloor\log_b{n}\rfloor + 1$, et comme $p$ est la taille de $n$ en base $b$, on peut conclure que

$$|n|_b= \lfloor\log_b{n}\rfloor + 1.$$

On retiendra que la taille d'un entier est approximativement égale au logarithme en base $b$ de cet entier:

$$ |n|_b \sim \log_b{n}.$$



**Exercice** Programmez une troisième version du calcul de la taille en exploitant la formule établie ci-dessus.

```python jupyter={"source_hidden": true}
from math import log, floor
def taille3(nbre, base):
    '''
    :param nbre: (int) un entier naturel non nul
    :param base: (int) une base
    :return: (int) la taille de nbre en base base
    :CU: nbre > 0, 2 <= base
    :Exemples:
    
    >>> taille3(100, 2)
    7
    >>> taille3(100, 10)
    3
    >>> taille3(100, 100)
    2
    >>> taille3(100, 101)
    1
    '''
    return floor(log(nbre, base)) + 1
```

```python jupyter={"source_hidden": true}
[taille3(100, base) for base in (2, 10, 100, 101)]
```

**Remarque :**

À la différence des deux premières versions, le calcul de la taille se fait ici par l'intermédiaire de fonctions amenant à des calculs sur des nombres flottants. Les calculs sur de tels nombres n'ont pas l'exactitude de ceux effectués sur des nombres entiers.


**Rapport de tailles**

Quel est le rapport entre la taille d'un entier en $b$, et la taille du même entier en base $b'$ ?

$$ \frac{|n|_b}{|n|_{b'}} \sim \frac{\log_b{n}}{\log_{b'}{n}} = \log_b{b'}.$$

Autrement dit le rapport de ces tailles est approximativement constant. Voici quelques valeurs 

* $b=2$, $b'=16$ : $\frac{|n|_2}{|n|_4}\sim 4$. 
* $b=2$, $b'=10$ : $\frac{|n|_2}{|n|_4}\sim \log_2{10} \approx 3.32$. 


## Fonctionnalités offertes par Python


En plus de la base 10 usuelle, Python permet d'exprimer littéralement des entiers en base 2, 8 et 16. Il suffit pour cela d'exprimer ces entiers littéraux en les préfixant de $\mathtt{0b}$, $\mathtt{0o}$ et $\mathtt{0x}$. 

```python jupyter={"source_hidden": true}
0b1010110, 0o126, 0x56
```

```python jupyter={"source_hidden": true}
0o126 + 0x56
```

Trois fonctions pour les bases 2, 8 et 16

```python jupyter={"source_hidden": true}
bin(100), oct(100), hex(100)
```

Inversement, conversion d'une chaîne de caractères en entier. Bases limitées à l'intervalle [2, 36].

```python jupyter={"source_hidden": true}
int('1100100', 2), int('144', 8), int('64', 16)
```

```python jupyter={"source_hidden": true}
int('WEGRZYNOWSKI', 36)
```

Avec la méthode format des chaînes de caractères

```python jupyter={"source_hidden": true}
'{:b}'.format(100), '{:o}'.format(100), '{:d}'.format(100), '{:X}'.format(100)
```

**Exercice** Programmez une fonction qui à partir d'un entier $n\geq 0$ et une base $2\leq b\leq 16$ renvoie une chaîne de caractères donnant l'écriture en base $b$ de $n$.

```python jupyter={"source_hidden": true}
CHIFFRES = '0123456789ABCDEF'
def en_chaine(nbre, base):
    '''
    :param nbre: (int) le nombre à convertir
    :param base: (int) la base souhaitée
    :return: (str) une liste de chiffres représentant le nombre dans la base demandée
    :CU: nbre >= 0 et 2 <= base <= 16
    :Exemples:
    
    >>> en_chaine(100, 10)
    '100'
    >>> en_chaine(100, 2)
    '1100100'
    >>> en_chaine(100, 16)
    '64'
    >>> en_chaine(0, 10)
    '0'
    '''
    if nbre == 0:
        res = '0'
    else:
        res = ''
        while nbre != 0:
            nbre, r = divmod(nbre, base)
            res = CHIFFRES[r] + res
    return res
```

```python jupyter={"source_hidden": true}
[en_chaine(100, base) for base in (10, 2, 16)]
```

<!-- #region {"toc-hr-collapsed": false} -->
# Entiers en machine
<!-- #endregion -->

<!-- #region -->
En informatique, il faut distinguer la représentation des nombres au niveau logiciel de celle au niveau matériel.

Au niveau logiciel, il est possible de travailler avec des nombres entiers de taille quelconque (du moins en théorie). C'est le cas en Python. Pour information les nombres entiers en Python sont représentés comme une
suite de chiffres en base 2<sup>30</sup>. Plus [d'informations ici](https://rushter.com/blog/python-integer-implementation/).



Mais au niveau du microprocesseur, la taille des registres impose une limitation.

Avec des registres de $p$ bits, $p$ désignant un entier positif dont la valeur est fixée (couramment 8, 16, 32 ou 64 bits), il est possible de représenter $2^p$ nombres entiers. Si ces nombres sont positifs ou nuls, on peut coder les entiers de 0 à $2^p - 1$ par leur écriture binaire.

Mais quid des entiers signés ?
<!-- #endregion -->

## Représentation des entiers signés


Quelle représentation binaire pour des entiers signés au niveau d'un registre de $p$ bits ? 


### Représentation signe + valeur absolue


Une réponse vient immédiatement à l'esprit : réserver un bit pour le signe, et garder les $p-1$ bits restant pour la représentation binaire de la valeur absolue.

Par exemple, avec $p=3$ bits, et en réservant le bit de poids fort pour représenter le signe du nombre, avec la convention $\mathtt{0}$ pour les nombres positifs et $\mathtt{1}$ pour les négatifs, on peut représenter les nombres

|$n$ | signe + va |
|----|-----  |
| 0  | $\mathtt{000}$ |
| 1  | $\mathtt{001}$ |
| 2  | $\mathtt{010}$ |
| 3  | $\mathtt{011}$ |
| 0  | $\mathtt{100}$ |
| -1  | $\mathtt{101}$ |
| -2  | $\mathtt{110}$ |
| -3  | $\mathtt{111}$ |

Avec une telle représentation on peut représenter tous les entiers compris entre $-(2^{p-1}-1)$ à $2^{p-1}-1$.


Mais cette représentation n'est pas celle usuellement retenue car elle présente plusieurs défauts :

* 0 possède deux représentations distinctes, ce qui complique les comparaisons ;
* additionner deux nombres n'est pas simple.


### Représentation en complément à 2


Commençons par considérer quelques «registres» d'appareils anciens comme les compteurs kilométriques ou affichages de calculatrices mécaniques. La photo ci-dessous montre un registre à 8 chiffres décimaux d'une calculatrice de bureau utilisée dans les années 1950-60.

![registre de calculatrice](registre_calculatrice.jpg)

Ajouter 1 au nombre affiché dans le registre revient à faire tourner d'un dixième de tour la roue la plus à droite. Lorsqu'une roue passe du chiffre 9 au chiffre 0, elle entraîne la roue située à sa gauche d'un dixième de tour : c'est la retenue.

Que se passe-t-il si on fait tourner la roue dans l'autre sens ? La vidéo ci-dessous illustre cela avec un compte-à-rebours de neuf étapes en partant de 5.

```python
%%HTML
<iframe src="registre_calculatrice_cpt_a_rebours.mp4" />
```

Comme on peut le constater une fois que les huit chiffres du registre affichent $\mathtt{0}$, à l'étape suivante, tous les chiffres se changent en $\mathtt{9}$. Là où un affichage aurait pu afficher le nombre -1 en utilisant le signe -, le registre affiche le nombre 99999999. Puis le compte-à-rebours se poursuivant, le registre affiche successivement 99999998, 99999997, et 99999996. Ainsi, pour cette machine, les nombres -1, -2, -3 et -4 sont représentés par ces quatre nombres à huit chiffres débutant par $\mathtt{9}$. 

On peut interpréter cela mathématiquement en constatant que 

\begin{align*}
  99999999 &\equiv -1 \pmod{10^8}\\
  99999998 &\equiv -2 \pmod{10^8}\\
  99999997 &\equiv -3 \pmod{10^8}\\
  99999996 &\equiv -4 \pmod{10^8}\\
\end{align*}


La représentation en complément à 2 des entiers signés repose sur le même principe.

Dans un registre de taille $p$, le bit de poids fort indique le signe du nombre ($\mathtt{0}$ pour les positifs et $\mathtt{1}$ pour les négatifs. 

Lorsque le bit de poids fort d'un nombre $n$ est $\mathtt{0}$, alors ce nombre est positif et les $p-1$ bits suivants sont la représentation binaire de ce nombre. Par exemple, avec $p=8$, le nombre représenté par $\mathtt{01110111}$ est le nombre $n=119$.

Lorsque le bit de poids fort est $\mathtt{1}$, alors ce nombre est négatif et est égal à $n=n'-2^{p}$, où $n'$ est le nombre positif dont la représentation binaire est donnée par les $p$ bits du registre. Par exemple, avec $p=8$, le registre $\mathtt{11100110}$ représente un nombre négatif égal à $n = 230 - 256 = -26$.

Dans une telle représentation on peut représenter tous les nombres de l'intervalle $[-2^{p-1}, 2^{p-1}-1]$. 


**Avantages :**

* unicité de la représentation de chacun des nombres de l'intervalle $[-2^{p-1}, 2^{p-1}-1]$ ;
* l'addition de deux nombres se fait très simplement.

```python
def complement_a_2(n,p):
    '''
    :param n: (int) nbre à représenter
    :param p: (int) taille du registre
    :return: (list) liste de p bits de la représentation en complément à 2 de n sur p bits
    :CU:  -2^{p-1} <= n < 2^{p-1}
    :Exemples:
    
    >>> complement_a_2(0, 8)
    [0, 0, 0, 0, 0, 0, 0, 0]
    >>> complement_a_2(1, 8)
    [1, 0, 0, 0, 0, 0, 0, 0]
    >>> complement_a_2(-1, 8)
    [1, 1, 1, 1, 1, 1, 1, 1]
    >>> complement_a_2(-128, 8)
    [0, 0, 0, 0, 0, 0, 0, 1]
    
    '''
    if n >= 0:
        res = en_base(n, 2)
        res = res + [0]*(p - len(res))
    else:
        res = en_base(n+2**p, 2)
    return res
```

```python
complement_a_2(-119, 8)
```

**Remarque :** la représentation en complément à 2 des entiers signés telle que présentée ici n'a de sens que pour des représentations dans des registres de taille fixée. Pour des entiers de taille illimitée comme ceux de Python, on peut «imaginer» que les nombres sont représentés dans des registres de taille infinie à gauche. Les nombres positifs ayant une infinité de bits $\mathtt{0}$ à gauche, et les négatifs une infinité de bits $\mathtt{1}$ à gauche.


## Opérations logiques


En assimilant le bit $\mathtt{0}$ à la valeur booléenne Faux (`False`) et le bit $\mathtt{1}$ à la valeur Vrai (`True`), on peut définir sur deix bits $b$ et $b'$ les opérations logiques usuelles.

* l'opération **et** notée $\&$ :
\begin{align*}
b ~\& ~b' &= \mathtt{1} & \mbox{si }b=b'=\mathtt{1}\\
        &= \mathtt{0} & \mbox{sinon.}
\end{align*}

* l'opération **ou** notée $|$ :
\begin{align*}
b ~| ~b' &= \mathtt{0} & \mbox{si }b = b'=\mathtt{0}\\
        &= \mathtt{1} & \mbox{sinon.}
\end{align*}

* l'opération **ou exclusif** notée $\oplus$ :
\begin{align*}
b ~\oplus ~b' &= \mathtt{1} & \mbox{si }b \neq b'\\
        &= \mathtt{0} & \mbox{sinon.}
\end{align*}



Ces trois opérations peuvent être étendues à n'importe quels nombres entiers. 

Considérons les deux nombres $n=230=\overline{\mathtt{11100110}}_2$ et $m = 119 = \overline{\mathtt{01110111}}_2$. 

Voici le calcul de $n~\&~m$ :
$$
\begin{array}{ccccccccc}
   & 1 & 1 & 1 & 0 & 0 & 1 & 1 & 0\\
\& & 0 & 1 & 1 & 1 & 0 & 1 & 1 & 1\\
\hline
=  & 0 & 1 & 1 & 0 & 0 & 1 & 1 & 0
\end{array}
$$
et on trouve donc $n~\&~m=\overline{01100110}_2 = 102$.

Voici le calcul de $n~|~m$ :
$$
\begin{array}{ccccccccc}
   & 1 & 1 & 1 & 0 & 0 & 1 & 1 & 0\\
|  & 0 & 1 & 1 & 1 & 0 & 1 & 1 & 1\\
\hline
=  & 1 & 1 & 1 & 1 & 0 & 1 & 1 & 1
\end{array}
$$
et on trouve donc $n~|~m=\overline{11110111}_2 = 247$.

Voici enfin le calcul de $n \oplus m$ :
$$
\begin{array}{ccccccccc}
   & 1 & 1 & 1 & 0 & 0 & 1 & 1 & 0\\
\oplus & 0 & 1 & 1 & 1 & 0 & 1 & 1 & 1\\
\hline
=  & 1 & 0 & 0 & 1 & 0 & 0 & 0 & 1
\end{array}
$$
et on trouve donc $n\oplus m =\overline{10010001}_2 = 145$.


En Python ces opérations sont définies par les opérateurs `&`, `|` et `^`.

```python
n = 230
m = 119
n & m, n | m, n ^ m
```

L'opération logique unaire de négation, notée $\neg$ est aussi définie pour les entiers. Elle consiste à changer la valeur de chacun des bits de la représentation binaire du nombre. 

Par exemple pour $n = 230 = \overline{\mathtt{11100110}}_2$, on a $\neg n = \overline{\mathtt{...11100011001}}_2 = -231$.


En Python, l'opérateur de négation logique est `~`.

```python
~230, ~-230
```

## Décalages


Soit $n$ et $k$ deux entiers. Les deux opérations de décalages à gauche et à droite sont définies par

* décalage à gauche : $n << k = n \times 2^k$. Autrement dit $n << k$ est un nombre dont l'écriture binaire est décalée de $k$ positions vers la gauche, en introduisant $k$ nouveaux bits de poids faibles nuls.

* décalage à droite : $n >> k = n \div 2^k$. Autrement dit $n >> k$ est un nombre dont l'écriture binaire est décalée de $k$ positions vers la droite, les $k$ bits de poids faibles de $n$ étant perdus.



En Python les opérateurs de décalages sont notés `<<` et `>>`. 

```python
n << 1, n << 2, n >> 1, n >> 2
```

## Exercices


**Exercice** Comment tester la parité d'un nombre entier en ne s'autorisant que des opérations logiques ?

```python
import timeit

timeit.timeit(stmt='119757576587098098796785 & 2', number=10**6)
```

**Exercice** Montrez que pour tout entier $n\in\mathbb{Z}$, $\neg n = -(n+1)$.


**Exercice** Soit $n$ un nombre entier naturel. Que dire de l'entier $n~\&~(n - 1)$ ?


**Exercice** On appelle *poids de Hamming*, ou plus simplement *poids*, d'un nombre entier, le nombre de bits $\mathtt{1}$ dans son écriture binaire.

Donnez plusieurs versions d'une fonction calculant le poids d'un entier.

```python jupyter={"source_hidden": true}
# avec des divisions euclidiennes (fonction divmod)
def poids1(nbre):
    '''
    :param nbre: (int) un entier
    :return: (int) le poids de cet entier
    :CU: aucune
    :Exemples: 
    
    >>> poids1(0)
    0
    >>> [poids1(2^k) for k in range(6)]
    [1, 1, 1, 1, 1, 1]
    >>> poids1(100)
    3
    '''
    res = 0
    while nbre != 0:
        nbre, r = divmod(nbre, 2)
        res += r
    return res
```

```python jupyter={"source_hidden": true}
[poids1(n) for n in (0, 1, 2, 4, 8, 16, 100)]
```

```python jupyter={"source_hidden": true}
# avec des divisions euclidiennes programmées par opérations logiques
def poids2(nbre):
    '''
    :param nbre: (int) un entier
    :return: (int) le poids de cet entier
    :CU: aucune
    :Exemples: 
    
    >>> poids2(0)
    0
    >>> [poids2(2^k) for k in range(6)]
    [1, 1, 1, 1, 1, 1]
    >>> poids2(100)
    3
    '''
    res = 0
    while nbre != 0:
        nbre, r = nbre >> 1, nbre & 1
        res += r
    return res
```

```python jupyter={"source_hidden": true}
[poids2(n) for n in (0, 1, 2, 4, 8, 16, 100)]
```

```python jupyter={"source_hidden": true}
# avec l'opération & et la soustraction
def poids3(nbre):
    '''
    :param nbre: (int) un entier
    :return: (int) le poids de cet entier
    :CU: aucune
    :Exemples: 
    
    >>> poids3(0)
    0
    >>> [poids3(2^k) for k in range(6)]
    [1, 1, 1, 1, 1, 1]
    >>> poids3(100)
    3
    '''
    res = 0
    while nbre != 0:
        nbre = nbre & (nbre - 1)
        res += 1
    return res
```

```python jupyter={"source_hidden": true}
[poids3(n) for n in (0, 1, 2, 4, 8, 16, 100)]
```

<!-- #region {"toc-hr-collapsed": true} -->
# Les nombres flottants
<!-- #endregion -->

## Les calculs sont inexacts


Les nombres flottants permettent de représenter des valeurs approchées des nombres réels.

Chacun sait que des nombres comme $\frac{1}{3}$, $\sqrt{2}$ et $\pi$ ont une écriture décimale infinie, qu'il n'est donc pas possible d'écrire dans un espace fini.

Par conséquent lorsqu'on doit faire intervenir de tels nombres dans un programme, le plus souvent on recourt à une approximation de ces nombres.


Mais même des calculs effectués avec de simples nombres décimaux (nombres dont l'écriture décimale est finie) peuvent conduire à des résultats surprenants.

Par exemple si 0,3 est bien mathématiquement égal à la somme de 0,2 et 0,1, il n'en est rien en Python (et en bien d'autres langages de programmation d'ailleurs).

```python
0.3 - (0.2 + 0.1)
```

La différence entre les deux est très petite mais elle n'est pas nulle.

Cela explique pourquoi en programmation cela n'a aucun sens de tester l'égalité de deux nombres flottants.

```python
0.3 == 0.2 + 0.1
```

Pire encore ! Certaines propriétés bien connues des opérations arithmétiques usuelles ne sont plus vraies avec les nombres flottants.

C'est le cas par exemple de la propriété d'associativité de l'addition qui dit que pour tout nombre $a$, $b$ et $c$ on a

$$ (a + b) + c = a + (b + c),$$
et qui permet finalement de se dispenser de placer des parenthèses.

```python
(0.3 + 0.2) + 0.1 == 0.3 + (0.2 + 0.1)
```

```python
0.3 + 0.2 + 0.1 == 0.3 + (0.2 + 0.1)
```

```python
0.3 + 0.2 + 0.1 == (0.3 + 0.2) + 0.1
```

<!-- #region {"slideshow": {"slide_type": "subslide"}} -->
Ci-dessous une fonction qui conformément à une identité remarquable classique devrait valoir 0 pour tout nombre :
<!-- #endregion -->

```python
def f(x):
    return (x + 1)**2 - x**2 - 2*x - 1
```

Elle donne de bonnes valeurs pour n'importe quel nombre entier, mais pas pour n'importe quel nombre flottant.

```python
n = 10**8
x = float(n)
print('f({:d}) = {:d}'.format(n, f(n)))
print('f({:f}) = {:f}'.format(x, f(x)))
```

## Norme IEEE 754


En Python, comme dans beaucoup d'autres langages, les nombres flottants sont représentés selon une norme appelée IEEE-754. Cette norme prévoit que les nombres flottants soient représentés en binaire avec un nombre fixé de bits qui peut être (entre autres)

* 32 bits (simple précision) 
* 64 bits (double précision)

Cette représentation repose sur le fait que tout nombre réel $x$ non nul peut se mettre de façon unique sous la forme :

$$ x = (-1)^s\times 2^E \times M,$$
où

* $s \in\{0, 1\}$ selon le signe de $x$,
* $E\in\mathbb{Z}$, exposant,
* et $M\in[1, 2[$, ce nombre étant appelé *mantisse*.
Ainsi la connaissance des trois nombres $s$, $E$ et $M$ définit entièrement $x$. Par exemple, avec $x=-\frac{1}{3}$ on a 

$$ x = (-1)^1\times 2^{-2}\times \frac{4}{3},$$
d'où

* $s=-1$ puisque $x$ est négatif,
* $E=-2$,
* et $M=\frac{4}{3}$.


La norme IEEE-754 prévoit que ces trois nombres soient représentés sur un espace limité selon le tableau ci-dessous, dans lequel $e$ et $m$ désignent le nombre de bits réservés pour représenter l'exposant $E$ et la mantisse $M$.

| Taille représentation | signe | $e$  |  $m$  | 
|:---------------------:|:-----:|:----:|:-----:|
|    32                 |   1   |  8   |  23   |
|    64                 |   1   |  11  |  52   |

Dans la représentation, le bit de signe est le bit le plus à gauche (bit de poids fort), puis viennent les $e$ bits représentant l'exposant, puis viennent les $m$ bits représentant la mantisse comme on peut le voir sur la figure ci-dessous (source [Wikipedia](https://fr.wikipedia.org/wiki/IEEE_754)).

![norme IEEE 754](norme_IEEE-754.png)


**Codage de l'exposant $E$ :** les $e$ bits réservés à l'exposant ne sont pas directement la représentation binaire de $E$, mais du nombre $E' = E + 2^{e-1} - 1$. Ainsi comme on ne dispose que de $e$ bits pour coder l'entier $E'$, on a théoriquement
$$ 0\leq E' \leq 2^e-1,$$
mais pour des raisons techniques (représentation de 0, de l'infini, ...) on restreint de chaque côté de l'encadrement
$$ 1\leq E' \leq 2^e-2,$$
et par conséquent
$$ -2^{e-1} + 2 \leq E \leq 2^{e-1}-1.$$

* **sur 32 bits**, avec $e=8$,
  $$ -126 \leq E \leq 127,$$
* et **sur 64 bits**, avec $e=11$
  $$ -1022 \leq E \leq 1023.$$
  
Avec notre exemple $x=-\frac{1}{3}$, l'exposant $E=-2$ est codé sur 64 bits par l'entier $E'=-2+2^{10}-1=1021$, et donc par les onze bits : `01111111101`.


**Codage de la mantisse $M$ :** la mantisse $M$ est codée à partir de son écriture en binaire. Avec notre exemple $x=-\frac{1}{3}$, on a $M=\frac{4}{3}$, et comme on peut écrire

$$ M=\frac{4}{3}=\frac{1}{1−\frac{1}{4}},$$

par conséquent on a
$$ M=\sum_{k=0}^{+\infty}\frac{1}{4^k},$$

d'où l'on peut déduire l'écriture binaire de $M$
$$M=1,010101010101...$$
développement binaire périodique de période 01.

La mantisse étant toujours un nombre réel compris entre 1 (inclus) et 2 (exclu), le bit avant la virgule est toujours un 1, et il est inutile de le coder puisqu'on le connaît. Seuls les bits après la virgule sont codés.

Et comme on ne dispose que de $m$ bits pour représenter la mantisse, on tronque et on ne garde que les $m$ premiers bits après la virgule.

Ainsi, dans un codage sur 64 bits, la mantisse de notre réel $x=-\frac{1}{3}$
est codée avec 52 bits alternant 0 et 1 :

    0101010101010101010101010101010101010101010101010101.

La représentation complète de $x$ sur 64 bits est finalement

    1011111111010101010101010101010101010101010101010101010101010101

```python
# Vérification avec la fonction pack du module struct de Python
import struct
x = -1/3
repr_x = struct.pack('>d', x) # chaînes de 8 octets (type bytes en Python)
```

```python
repr_x
```

```python
list(repr_x)
```

```python
repr_bin_x = ''.join('{:08b}'.format(oct) for oct in repr_x) # construction d'une chaîne de caractères binaires
print('taille : {:d} bits.  {:s}'.format(len(repr_bin_x), repr_bin_x))
```

```python

```

## Exercices


**Exercice** Déterminer la représentation en double précision du nombre entier $n=2019$ et vérifiez avec la fonction `pack` du module `struct` de Python. 


Le nombre $n=2019$ peut s'écrire sous la forme :

$$ n = (-1)^0\times 2^{10}\times \frac{2019}{1024}.$$

On en déduit que 

* le bit de signe est $\mathtt{0}$ ;
* l'exposant $E=10$ est représenté par l'écriture binaire de $E' = E + 2^{10} - 1 = 1033$ c'est à dire $\mathtt{10000001001}$ ;
* et la mantisse $M=\frac{2019}{1024}$ qui se décompose en

  $$ M = 1 + \frac{995}{1024} = 1 + \frac{512 + 256 + 128 + 64 + 32 + 2 + 1}{1024},$$
  s'écrit en binaire avec 10 bits après la virgule
  
  $$ M = \overline{\mathtt{1.1111100011}}_2.$$
  
On en déduit que le codage binaire en double précision du nombre $n=2019$ est

$$\mathtt{0100000010011111100011000000000000000000000000000000000000000000}$$

```python jupyter={"source_hidden": true}
''.join('{:08b}'.format(octet) for octet in struct.pack('>d', float(2019)))
```

**Exercice** Quel est le plus petit entier positif qui ne peut pas être représenté exactement par un flottant codé selon la norme IEEE 754 double précision (64 bits) ?


Tout entier pouvant être codé en binaire sur 53 bits peut être réprésenté exactement avec les 52 bits de la mantisse (le bit de poids fort n'étant pas codé).

Le premier entier nécessitant 54 bits est $2^{53}$ qui est évidemment représentable exactement, avec une mantisse $M=1$ codée par 52 bits nuls et un exposant $E=53$.

En revanche l'entier $2^{53} + 1$ ne peut pas être représenté exactement puisque la mantisse $M = 1 + \frac{1}{2^{53}}$ nécessite 53 bits pour être représentée exactement.

```python jupyter={"source_hidden": true}
N = 2**53
floor(float(N)) == N
```

```python jupyter={"source_hidden": true}
floor(float(N+1)) == N+1
```

**Exercice** Existe-t-il un nombre entier strictement positif dont l'écriture binaire, interprétée comme un flottant codé selon la norme IEEE 754 simple précision (32 bits), soit celle de ce même nombre en flottant ?

```python jupyter={"source_hidden": true}
# Attention : le calcul ci-dessous nécessite plusieurs minutes 
n = 1
while n < 1<<31 and struct.pack('f', float(n)) != struct.pack('i', n):
    n += 1
n
```

```python jupyter={"source_hidden": true}
# Vérification
print(''.join('{:08b}'.format(k) for k in struct.pack('>f', n)))
print('{:032b}'.format(n))
```

```python jupyter={"source_hidden": true}

```

<!-- #region {"slideshow": {"slide_type": "slide"}} -->
### Flottants dans d'autres bases
<!-- #endregion -->

<!-- #region {"slideshow": {"slide_type": "slide"}} -->
La norme IEEE 754 présentée ci-dessus permet de donner une représentation de l'écriture binaire (tronquée) d'un nombre réel.

Mais il est aussi possible de donner une représentation de l'écriture décimale d'un nombre réel. Ainsi, si $x$ est un nombre réel non nul il existe un unique triplet $(s, E, m)$ tel que
$$ x = (-1)^s\times 10^E\times m $$
où 

* $s\in\{0, 1\}$ détermine le signe de $x$,
* $E\in\mathbb{Z}$
* $m$ est un réel compris entre 1 (inclus) et 10 (exclu).
<!-- #endregion -->

<!-- #region {"slideshow": {"slide_type": "subslide"}} -->
Le codage de ces trois nombres se fait comme pour le cas binaire

* 1 bit pour $s$
* un certain nombre de bits pour $E$
* et pour $m$, on tronque l'écriture décimale à un nombre fixé de chiffres après la virgule, et on code chacun des chiffres (y compris celui avant la virgule) en binaire (4 bits suffisent).
<!-- #endregion -->

<!-- #region {"slideshow": {"slide_type": "subslide"}} -->
Le codage de ces trois nombres se fait comme pour le cas binaire

* 1 bit pour $s$
* un certain nombre de bits pour $E$
* et pour $m$, on tronque l'écriture décimale à un nombre fixé de chiffres après la virgule, et on code chacun des chiffres (y compris celui avant la virgule) en binaire (4 bits suffisent).
<!-- #endregion -->

<!-- #region {"slideshow": {"slide_type": "subslide"}} -->
**Comment reconnaître la base de représentation des flottants ?**

Les lignes de code qui suivent permettent de déterminer la base de représentation.
(testé sur PC avec Python 3.6 et sur TI 83 premium)
<!-- #endregion -->

```python
a = 1.0
b = 1.0
while ((a + 1.0) - a) - 1.0 == 0.0 : a = 2*a
while ((a + b) -a) - b != 0.0: b = b + 1.0
b
```

```python jupyter={"source_hidden": true}
struct.pack?
```

```python jupyter={"source_hidden": true}
struct?
```

```python jupyter={"source_hidden": true}

```
