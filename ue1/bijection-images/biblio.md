# Bibliographie

-   *Images brouillées, images retrouvées*, Jean-Paul Delahaye &
    Philippe Mathieu, Pour la science n°242, décembre 1997.
-   *Une scytale informatique*, Jean-Paul Delahaye, Pour la Science
    n°359, septembre 2007.
