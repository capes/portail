# Travail à réaliser

## Le module PIL

Pour travailler en Python sur des images vous utiliserez la bibliothèque
`PIL` (Python Imaging Library) qui pour la version 3 de Python se nomme
`Pillow`. Cette bibliothèque est installée dans les salles de TP[^1].

Dans ce projet, vous n\'aurez besoin que d\'une toute petite partie des
fonctionnalités proposées par cette bibliothèque, toutes contenues dans
le module `Image`. Vous aurez donc besoin de charger ce module

``` python
from PIL import Image
```

Voici la liste des fonctions, attributs et méthodes utiles avec
lesquelles vous devez vous familiariser :

1.  fonction `open` : charge en mémoire une image contenue dans un
    fichier.

    ``` python
    img = Image.open('calbuth.png')
    ```

2.  attribut `mode` : donne sous forme d\'une chaîne de caractères le
    *mode* de l\'image :

    -   `'L'` : image en niveaux de gris
    -   `'P'` : image avec palette de couleurs
    -   `'RGB'` : image en couleurs décrites avec le codage
        [RVB](https://fr.wikipedia.org/wiki/Rouge_vert_bleu)

    ``` python
    img.mode
    ```

3.  attribut `size` : donne les dimensions de l\'image sous forme d\'un
    tuple (largeur, hauteur)

    ``` python
    img.size
    ```

4.  méthode `show` : montre l\'image dans une fenêtre

    ``` python
    img.show()
    ```

5.  fonction `new` : crée une nouvelle image d\'un mode, de dimensions
    et de couleur donnés

    ``` python
    img2 = Image.new(img.mode, img.size, 'black')
    ```

6.  méthodes `get_pixel` et `put_pixel` : donne la valeur d\'un pixel,
    attribue une valeur à un pixel. La valeur dépend du mode de
    l\'image. Pour les modes `'L'` et `'P'` la valeur est souvent un
    nombre entier compris entre 0 et 255. Pour le mode `'RGB'` c\'est un
    triplet d\'entiers compris entre 0 et 255.

    ``` python
    img2.putpixel((x, y), img.getpixel((x, y)))
    ```

7.  méthode `save` : sauvegarde l\'image dans un fichier dont le nom est
    passé en paramètre.

    ``` python
    img2.save('calbuth_2.png')
    ```

## Transformations d\'une image

Toutes les transformations bijectives d\'image peuvent être programmées
selon le même schéma :

> **Entrée :** une image $Img$ à transformer et une transformation $T$
>
> **Sortie :** une image transformée $Img'$.
>
> 1.  **Pour** chaque pixel $Img(x, y)$ de l\'image à transformer
> 2.  $\quad$ calculer les coordonnées $(x', y')= T(x,y)$
> 3.  $\quad$ attribuer au pixel $Img(x', y')$ la valeur du pixel
>     $Img(x, y)$
> 4.  **Fin Pour**

Plutôt que de programmer plusieurs fois le même algorithme pour chacune
des transformations, il est préférable de concevoir une fonction
générique prenant deux arguments :

1.  une transformation $T$ sous forme d\'une fonction
2.  et une image $img$

qui renvoie une nouvelle image résultant de la transformation de $img$
par $T$.

## Programmation des permutations

Toutes les permutations des pixels peuvent être programmées par des
fonctions ayant la même spécification :

Paramètre coord

:   (couple d\'entiers) coordonnées d\'un pixel

Paramètre dim

:   (couple d\'entiers) dimensions de l\'image

Valeur renvoyée

:   (couple d\'entiers) coordonnées de la position du pixel d\'après la
    permutation

CU

:   coordonnées valides d\'après les dimensions + éventuellement CU
    spécifique à une permutation

À titre d\'exemple voici le code de la permutation identité selon cette
spécification (sans la docstring)

``` python
def identite(coord, dim):
    x, y = coord
    larg, haut = dim
    assert 0 <= x < larg and 0 <= y < haut, 'coordonnées non valides'
    return coord
```

## Détermination des cycles d\'une permutation

Toutes les permutations se décomposent de manière unique en cycles
disjoints.

La connaissance de ces cycles permet de calculer le nombre d\'étapes
nécessaires dans une transformation itérée pour retrouver l\'image
initiale.

Vous réaliserez donc une fonction qui calcule tous les cycles d\'une
permutation passée en paramètre. Puis une fonction qui calcule l\'ordre
d\'une permutation.

## Calculer loin

La connaissance des cycles d\'une permutation permet aussi de calculer
la transformation itérée $n$ fois d\'une image sans avoir à calculer
toutes les images intermédiaires.

Vous réaliserez donc une fonction qui calcule la transfromation d\'une
image par une permutation itérée $n$ fois.

Voici par exemple l\'image

![](images/calbuth.png){.align-center width="250px"}

transformée par la transfromation concentrique itérée
8976867543544768979080989867546535436987 fois :

![](images/calbuth_CONCENTRIQUE_8976867543544768979080989867546535436987.png){.align-center
width="250px"}

## Dissimuler un message

![](images/msg_BOUL1_BOUS10000_CONC9868756755876_PM5_CONC765765764675587676.png){.align-right
width="250px"}

L\'image ci-contre a été obtenu en transformant une image successivement
par

1.  une transformation du boulanger
2.  10000 transformations du boustrophédon consécutives
3.  9868756755876 transformations concentrique consécutives
4.  5 transfromations du photo-maton consécutives
5.  et finalement 765765764675587676 transformations concentriques.

Sauriez-vous retrouver l\'image d\'origine ?

### Notes

[^1]: Si vous voulez l\'installer sur vos postes personnels assurez-vous
    de bien installer la version
    [Pillow](https://pypi.python.org/pypi/Pillow) pour Python 3. Par
    exemple en utilisant l\'utilitaire `pip`

    ``` bash
    pip install pillow
    ```
