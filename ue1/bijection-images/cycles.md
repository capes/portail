# Cycles d\'une permutation

![](images/calbuth_PHOTO_MATON_complet.png){.align-right width="175px"}

![](images/galetsBOULANG.png){.align-right width="175px"}

## Transformations itérées

Soit $img$ et $T$ une permutation des pixels de cette image. Avec ces
données on peut définir une suite (récurrente) d\'images en posant

```math
\begin{array}{rcl}
img_0 &= img\\
img_{n+1} &= T(img_n)
\end{array}
```

Ainsi l\'image $img_n$ est obtenue en itérant $n$ fois la transformation
$T$ depuis l\'image $img$.

### Un constat

1.  Il est évident que si on itère deux fois une symétrie (axiale ou
    centrale) sur une image, on retrouve l\'image de départ.
2.  Il n\'est pas surprenant que si on itère $\ell.h$ fois la
    transformation du boustrophédon, $\ell$ et $h$ désignant la largeur
    et la hauteur de l\'image, on retrouve l\'image de départ.
3.  Il est plus étonnant de constater que pour la transformation du
    photo-maton itérée huit fois sur une image $256\times 256$ redonne
    l\'image initiale.
4.  Il en est de même avec la transformation du boulanger qui itérée 17
    fois sur une image $256\times 256$ redonne l\'image initiale.

### Deux questions

1.  Est-ce toujours le cas pour n\'importe quelle permutation ?

    La réponse est oui.

2.  Peut-on calculer le nombre d\'étapes nécessaires pour retrouver
    l\'image de départ ?

    La réponse est oui.

## Décomposition en cycles et périodicité

**Résultat 1**

Toute permutation des pixels d\'une image peut être décomposée en cycles
disjoints.

![](images/tikz_cycles.png){.align-right width="300px"}

La figure ci-contre illustre les cycles d\'une permutation des pixels
d\'une image de $4\times 4$ pixels. Cette permutation comprend quatre
cycles :

-   un cycle de longueur trois : `(0, 0) -> (1, 0) -> (0, 1) -> (0, 0)`
    (en bleu) ;
-   deux cycles de longueur quatre :
    `(0, 2) -> (0, 3) -> (1, 3) -> (1, 2) -> (1, 2)` (en magenta) et
    `(2, 2) -> (2, 3) -> (3, 3) -> (3, 2) -> (2, 2)` (en rouge) ;
-   un cycle de longueur cinq :
    `(2, 0) -> (1, 1) -> (2, 1) -> (3, 1) -> (3, 0) -> (2, 0)` (en
    vert).

Ci-dessous on peut observer l\'effet de cette permutation itérée sur une
image de $4\times 4$ pixels symbolisés par les seize chiffres
hexadécimaux :

``` text
+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+
| 0 | 1 | 2 | 3 |          | 4 | 0 | 3 | 7 |        | 1 | 4 | 7 | 6 |        | 0 | 1 | 6 | 5 |
+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+
| 4 | 5 | 6 | 7 |    1     | 1 | 2 | 5 | 6 |    2   | 0 | 3 | 2 | 5 |    3   | 4 | 7 | 3 | 2 |   4
+---+---+---+---+   -->    +---+---+---+---+   -->  +---+---+---+---+   -->  +---+---+---+---+  -->
| 8 | 9 | A | B |          | 9 | D | B | F |        | D | C | F | E |        | C | 8 | E | A | 
+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+
| C | D | E | F |          | 8 | C | A | E |        | 9 | 8 | B | A |        | D | 9 | F | B |
+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+

+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+
| 4 | 0 | 5 | 2 |          | 1 | 4 | 2 | 3 |        | 0 | 1 | 3 | 7 |        | 4 | 0 | 7 | 6 |
+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+
| 1 | 6 | 7 | 3 |    5     | 0 | 5 | 6 | 7 |    6   | 4 | 2 | 5 | 6 |    7   | 1 | 3 | 2 | 5 |   8
+---+---+---+---+   -->    +---+---+---+---+   -->  +---+---+---+---+   -->  +---+---+---+---+  -->
| 8 | 9 | A | B |          | 9 | D | B | F |        | D | C | F | E |        | C | 8 | E | A | 
+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+
| C | D | E | F |          | 8 | C | A | E |        | 9 | 8 | B | A |        | D | 9 | F | B |
+---+---+---+---+          +---+---+---+---+        +---+---+---+---+        +---+---+---+---+

                           +---+---+---+---+                                 +---+---+---+---+
                           | 0 | 1 | 7 | 6 |                                 | 1 | 4 | 2 | 3 |
                           +---+---+---+---+                                 +---+---+---+---+
                    12     | 4 | 3 | 2 | 5 |                            20   | 0 | 5 | 6 | 7 |   
     ...            -->    +---+---+---+---+   -->        ...           -->  +---+---+---+---+  -->
                           | 8 | 9 | A | B |                                 | 8 | 9 | A | B | 
                           +---+---+---+---+                                 +---+---+---+---+
                           | C | D | E | F |                                 | C | D | E | F |
                           +---+---+---+---+                                 +---+---+---+---+

                                                    +---+---+---+---+        +---+---+---+---+
                                                    | 1 | 4 | 5 | 2 |        | 0 | 1 | 2 | 3 |
                                                    +---+---+---+---+        +---+---+---+---+
                                               59   | 0 | 6 | 7 | 3 |   60   | 4 | 5 | 6 | 7 |   
     ...            -->          ...           -->  +---+---+---+---+   -->  +---+---+---+---+
                                                    | C | 8 | E | A |        | 8 | 9 | A | B | 
                                                    +---+---+---+---+        +---+---+---+---+
                                                    | D | 9 | F | B |        | C | D | E | F |
                                                    +---+---+---+---+        +---+---+---+---+
```

On peut noter

-   qu\'après la première étape aucun pixel ne se trouve à sa position
    d\'origine ;
-   de même après la deuxième étape.
-   Après la troisième étape les pixels de coordonnées (0, 0), (1, 0) et
    (0, 1) se retrouvent à leur position d\'origine. Cela provient du
    fait qu\'à eux trois ils forment un cycle. Aucun autre pixel n\'est
    à sa place initiale.
-   Après la quatrième étape, les huit pixels situés dans les deux
    lignes du bas sont à leur place originale. C\'est normal car chacun
    de ces pixels fait partie de l\'un ou l\'autre des deux cycles de
    longueur 4. Aucun autre pixel n\'est à sa place.
-   Après le cinquième étape, les cinq pixels du cycle de longueur 5
    sont à leur position originale, les autres non.
-   Après la douzième étape, seuls les pixels du cycle de longueur 5 ne
    sont pas situés à leur place initiale.
-   Après la vingtième étape, c\'est au tour des trois pixels du cycle
    de longueur 3 d\'être les seuls pixels non situés à leur place
    d\'origine.
-   Finalement ce n\'est qu\'après la soixantième étape que tous les
    pixels se retrouvent à leur posisition initiale, et qu\'on retrouve
    donc l\'image d\'origine.

**Résultat 2**

La période de la suite d\'images définie par une transformation est
égale au ppcm des longueurs des cycles de cette transformation.


Ci-dessous la période de l\'itération à partir d\'une image
$256\times 256$ pour les différentes transformations présentées
précédemment :

| Transformation | Période                                                                                                                                   |
|----------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| Symétries      | 2 (indépendant de la taille de l\'image)                                                                                                  |
| Boulanger      | 17                                                                                                                                        |
| Photo-Maton    |                                                                                                                                           |
| Boustrophédon  | 65536                                                                                                                                     |
| Concentrique   | 260538918857747541744672011746506021097973820451713310155553963930295146498906764615494790073109501333431500 $\approx 2.6\times 10^{107}$ |
|                |                                                                                                                                           |
