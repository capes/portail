# Transformations bijectives d\'images

## Présentation

Le but de ce projet est de travailler sur une famille de transformations
d\'images appelées *transformations bijectives* ou encore
*permutations*.

Une *transformation bijective*, ou *permutation*, des pixels d\'une
image $Img$ est une transformation qui produit une image $Img'$ de même
dimension $\ell\times h$ que $Img$, telle que le pixel de coordonnées
$(x, y)$ soit envoyé sur le pixel de coordonnées $f(x, y)$ de l\'image
$Img'$, l\'application $f$ étant une bijection de
$[0, \ell-1]\times[0, h-1]$ sur lui-même.

1.  Ces transformations ne font que déplacer les pixels d\'une image.
    Elles ne modifient pas leur couleur (ou nuance de gris).
2.  Ces transformations ne modifient pas non plus les dimensions de
    l\'image : une image de $\ell\times h$ pixels produit une image de
    $\ell\times h$ pixels.

## Transformation du boulanger

![](images/galetsBOULANG.png){.align-right width="250px"}

Le boulanger pour préparer une pâte feuilletée commence par étirer la
pâte puis la replie sur elle-même. Il répète ces deux opérations un
certain nombre de fois.

La *transformation du boulanger* d\'une image de dimension
$\ell\times h$, par exemple celle-ci ($\ell=h=256$) :

![](images/galets.png){.align-center width="250px"}

consiste à

1.  obtenir une image de dimension $2\ell\times \frac{h}{2}$

    ![](images/galets_BOULANG_etape1.png){.align-center width="500px"}

2.  découper cette image en deux pour obtenir deux images de dimensions
    $\ell\times\frac{h}{2}$

    ![](images/galets_BOULANG_etape2-1.png){.align-center width="250px"}

    et

    ![](images/galets_BOULANG_etape2-2.png){.align-center width="250px"}

3.  rassembler verticalement les deux images ainsi obtenues après avoir
    fait une demi-tour avec celle de droite.

    ![](images/galets_BOULANG_complet.png){.align-center width="250px"}

L\'image obtenue à la fin de ce processus possède les mêmes dimensions
$\ell\times h$ et les mêmes pixels que l\'image d\'origine. Hormis le
pixel situé en haut à gauche, tous les pixels ont été déplacés.

Comme le boulanger pour sa pâte, on peut itérer le processus pour
obtenir les images :

-   après une deuxième transformation :

    > ![](images/galets_BOULANG_000002.png){.align-center width="250px"}

-   après une troisième :

    > ![](images/galets_BOULANG_000003.png){.align-center width="250px"}

-   après une dixième :

    > ![](images/galets_BOULANG_000010.png){.align-center width="250px"}

## Transformation du photo-maton

![](images/calbuth_PHOTO_MATON_complet.png){.align-right width="250px"}

La transformation du photo-maton est nommée en référence au nom de
certains automates qui vous tirent le portait pour des pièces
d\'identité.

Elle consiste à «reproduire» une image de dimension $\ell\times h$ en
quatre images de dimensions $\frac{\ell}{2}\times\frac{h}{2}$ et à
placer ces quatre reproductions aux quatre coins de l\'image.

Par exemple, avec cette image ($\ell=h=256$) :

![](images/calbuth.png){.align-center width="250px"}

la transformation du photo-maton produit cette image :

![](images/calbuth_PHOTO_MATON_000001.png){.align-center width="250px"}

et en itérant on obtient les images :

-   après une deuxième transformation :

    ![](images/calbuth_PHOTO_MATON_000002.png){.align-center width="250px"}

-   après une troisième :

    ![](images/calbuth_PHOTO_MATON_000003.png){.align-center width="250px"}

## Autres transformations

Il existe de très nombreuses transformations bijectives d\'une image
(combien ?). En voici quelques autres illustrées avec l\'image
($\ell=h=256$) :

![](images/joconde.png){.align-center width="250px"}

### Symétrie horizontale ou verticale

En voici deux très simples :

-   symétrie d\'axe horizontal :

    ![](images/joconde_SYMETRIE_HORIZONTAL_000001.png){.align-center width="250px"}

-   symétrie d\'axe vertical :

    ![](images/joconde_SYMETRIE_VERTICAL_000001.png){.align-center width="250px"}

### Symétrie centrale (demi-tour)

Et encore une autre :

-   symétrie centrale :

    ![](images/joconde_SYMETRIE_CENTRALE_000001.png){.align-center width="250px"}

### Défilement horizontal ou vertical

Les deux transformations décrites ici consistent à déplacer les colonnes
(resp. les lignes) d\'une image vers le bas (resp. la droite).

-   défilement horizontal appliqué une fois

    ![](images/joconde_DEFIL_HORIZONTAL_000001.png){.align-center width="250px"}

-   défilement horizontal appliqué cent fois

    ![](images/joconde_DEFIL_HORIZONTAL_000100.png){.align-center width="250px"}

-   défilement vertical appliqué une fois

    ![](images/joconde_DEFIL_VERTICAL_000001.png){.align-center width="250px"}

-   défilement vertical appliqué cent fois

    ![](images/joconde_DEFIL_VERTICAL_000100.png){.align-center width="250px"}

### Concentrique

Dans cette transformation, il faut concevoir l\'image comme une
succession de rectangles emboîtés. Les pixels se déplacent sur ces
rectangles dans le sens horaire.

-   transformation concentrique appliquée une fois

    ![](images/joconde_CONCENTRIQUE_000001.png){.align-center width="250px"}

-   transformation concentrique appliquée cent fois

    ![](images/joconde_CONCENTRIQUE_000100.png){.align-center width="250px"}

### Boustrophédon

**Un peu de culture**

Une écriture *boustrophédon* est un système qui change alternativement
le sens du tracé ligne après ligne, à la manière du bœuf marquant les
sillons dans un champ, allant de droite à gauche puis de gauche à
droite. Le terme vient de l\'adverbe grec ancien **βουστροφηδόν**
(boustrophêdón), de **βοῦς** (boũs) « bœuf » et **στροφή** (strophế) «
action de tourner ». \[\...\]

Le boustrophédon a été principalement utilisé à des stades anciens
d\'écritures avant que celles-ci ne se fixent dans un sens précis : le
grec, par exemple, s\'est d\'abord écrit de droite à gauche, comme le
phénicien dont il est issu, puis en boustrophédon et enfin de gauche à
droite. Le passage par le boustrophédon marque donc une transition

(source [Wikipedia](https://fr.wikipedia.org/wiki/Boustroph%C3%A9don))


La transformation *boustrophédon* consiste à déplacer les pixels d\'une
position vers la droite pour une ligne sur deux, et d\'une position vers
la gauche pour les autres lignes. Pour les pixels en bout de ligne on
les déplce vers la ligne du dessous sauf pour la dernière ligne pour
laquelle on revient à la première ligne.

-   transformation boustrophédon appliquée une fois

    ![](images/joconde_BOUSTROPHEDON_000001.png){.align-center width="250px"}

-   transformation boustrophedon appliquée cent fois

    ![](images/joconde_BOUSTROPHEDON_000100.png){.align-center width="250px"}
