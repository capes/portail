# Transformations bijectives d\'images

![](images/calbuth_PHOTO_MATON_complet.png)

- [transformations](./transformation.md)
- [cycles](./cycles.md)
- [à réaliser](./a_realiser.md)
- [biblio](./biblio.md)
