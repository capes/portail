# Spécification des recherches


## différentes recherches

-   rechercher dans une collection peut revêtir plusieurs significations. Par exemple :
    -   rechercher le maximum, le minimum, la médiane d'une séquence d'entiers ;
    -   rechercher l'élément le plus ancien dans la structure (gestion des logs) ;
    -   rechercher un élément connaissant une clé :
        -   nom dans un annuaire ;
        -   mot dans un dictionnaire.
-   ce dernier type de recherche est appelé **recherche associative**.


## spécification de la recherche associative.


### Collection v0.1

```python
class Collection:
    def __init__(self):
        """
        crée une collection vide
        """
        pass

    def add(self, e: Element):
        """
        ajoute l'élément e à la collection
        """
        pass
```


### appartenance

-   Comment spécifier `add` ? l'appartenance à une collection ?
-   Nécessité d'un prédicat `is_in`
-   Signature
-   Axiome


### Collection v0.2

```python
class Collection:
    def __init__(self):
        """
        crée une collection vide
        """
        pass

    def add(self, e: Element):
        """
        ajoute l'élément e à la collection
        """
        pass
    def is_in(self, e: Element) -> bool:
        pass
```


### doublons

-   Le problème : J'ajoute deux fois le même élément :
    -   J'ajoute deux fois le même entier dans une liste &#x2026;
    -   J'ajoute deux homonymes dans le dictionnaire
    -   Quelle est l'idée ? peut-on l'axiomatiser avec `is_in` ?
-   un nouvel "inspecteur" : `count`
    -   signature
    -   spécification


### Collection v0.3

```python
class Collection:
    def __init__(self):
        """crée une collection vide"""
        pass
    def key(e: Element) -> Key:
        """renvoie la clé d'un élément"""
        pass
    def is_in(self, e: Element) -> bool:
        """renvoie True ssi la collection contient un élément e
        """
        pass
    def add(self, e: Element):
        """ajoute l'élément e à la collection"""
        pass
    def remove(self, k: Key):
        """supprime l'élément de la collection"""
        pass
    def get(self, k: key) -> Element:
        """renvoie l'élément correspondant à la clé"""
        pass
    def count(self, k: key) -> int:
        """renvoie le nombre d'élément de clé k dans la collection"""
        pass
```


### exercice

-   Écrire tous les axiomes


### recherche

-   recherche positive : que renvoyer ?
-   recherche négative.
-   Spécification :
    -   pré-condition ;
    -   axiomes :
        -   `find` / `key`
        -   `find` / `is_in`
        -   `find` / `add`
        -   `find` / `remove`


# Méthodes de hachage


## propriétés

-   en pratique, on utilise telle ou telle structure de données pour ranger des éléments en fonction de :
    -   du types des éléments ;
    -   de leur nombre ;
    -   de la complexité attendue ;
    -   des propriétés des clés des éléments.


## objectif

dans les méthodes de hachage, la place d'un élément est calculée **uniquement à partir de sa clé**.

$h\,:\,\mathcal U \rightarrow \mathcal I$ ; $k \longmapsto h(k)$ où $h(k)$ désigne une adresse, un indice, &#x2026;


## exemple

-   $E = \lbrace nicolas, jordane, théo, aurélien, lucas, cindy, lucas, gabriel, amélie, paul, nassim \rbrace$
-   La clé est l'élément lui-même ;
-   Quelle fonction de hachage ?

```python
from functools import reduce

ALPHABET = "abcdeéfghijklmnopqrstuvwxyz"
E = ["nicolas", "théo", "aurélien", "lucas", "cindy", "lucas", "gabriel", "amélie",
   "paul", "nassim"]

N = 23
def h(mot):
    """renvoie le résultat du hachage
    """
    global N
    return (len(mot) + reduce(lambda x,y: x+y,
                  map(ALPHABET.index, mot))) % N


l = list(map(h, E))
```


## Le tableau

```python
for i in range(0, N):
    try:
        k = l.index(i)
        print(f"{i} | {E[k]}")
    except:
        print(f"{i} | -")
```

    0 | -
    1 | -
    2 | -
    3 | amélie
    4 | -
    5 | -
    6 | théo
    7 | paul
    8 | -
    9 | nicolas
    10 | -
    11 | nassim
    12 | cindy
    13 | lucas
    14 | -
    15 | -
    16 | -
    17 | -
    18 | -
    19 | -
    20 | -
    21 | -
    22 | aurélien


## principe

-   On doit gérer une collection $C$ dont les clés appartiennent à un univers très grand $U$; la taille probable de $C$ est connue et est relativement petite par rapport à la taille de $U$.
-   une place pour chaque élément de $U$ ?
-   On utilise une **fonction de hachage** $$ h\,:\,U \rightarrow [\![0, m-1]\!] $$


## hachage uniforme

-   on cherche à ce que les éléments de $U$ se répartissent uniformément dans $[0, m-1]$.
-   $\mathbb{P}(h(x) = i) = \frac{1}{m}$


## injectivité

-   il est souhaitable que la fonction de hachage soit injective.
-   cependant, si le hachage est uniforme, quelle est la probabilité que $h$ soit injective ?
    
    Si $k = \vert C\vert$ , alors
    
    -   nombre de hachages injectifs : $m(m-1)\ldots (m-k+1)$
    -   nombre de hachages possibles : $m^k$
    
    ```python
    
    
    def proba(k, m):
        N = 1
        D = 1
        for i in range(k):
            N *= m-i
            D *= m
        return N/D
    
    print("""probabilité qu'au moins personnes soient nés le même jour dans une
    assemblée de 23 personnes :""", 1-proba(23, 365))
    ```
    
        probabilité qu'au moins personnes soient nés le même jour dans une
        assemblée de 23 personnes : 0.5072972343239854


## collisions

-   résolution des collisions par chaînage : on chaîne les éléments pour lesquels $h(c)$ sont égaux (où $c$ est la clé). On parle de *chaînage indirect* ;
-   résolution des collisions par calcul : en cas de collision, on calcule une nouvelle place.


## fonctions de hachage

Supposons que :

-   les clés soient des entiers positifs ;
-   représentés par des mots binaires.
    
    La fonction de hachage peut être considérée comme une fonction $$h \, : \, \lbrace 0, 1\rbrace ^* \rightarrow [0, m-1]$$


## Exemple :

```python
CLES = ("ET", "OU", "NI", "IL")

def car_to_code(l: str)->int:
    """
    :param l: (str) un caractère
    :return: (int) renvoie le codage de ce caractère 

    >>> bin(car_to_code('E'))
    '0b10'
    """
    return ord(l) & 0b1111

def bit(p, n):
    """
    :param p: (int) le numero du bit
    :param n: (int) un entier
    :return: le bit numero p de n
    """
    return (n >> p) & 1
```


### par extraction

-   on extrait $p$ bits de la représentation binaire, puis on les concatène. On se ramène donc à l'intervalle $[0,2^p-1]$.
-   Exemple : extraction du sous mot formé par les numérotés 1, 2, 7 et 8 (en comptant à partir de la droite)

```python

def h_extract(cle, liste_bits):
    """
    :param cle: (str) une clé
    :param liste_bits: (tuple) la liste des bits utilisé
    """
    res = 0
    for c in cle:
        code = car_to_code(c)
        for no in liste_bits:
            res = res << 1 | bit(no, code)
    return res

print([bin(h_extract(c, (7, 6, 1, 0))) for c in CLES ])
```

    ['0b10000', '0b110001', '0b100001', '0b10000']

-   Avantage : Facile , rapide
-   désavantage : ne dépend pas de la totalité de la clé.
    
    *une bonne fonction de hachage doit faire intervenir tous les bits*


### par compression

-   on coupe la chaîne de bits en morceaux de longueur égale, puis on effectue une opération binaire entre ces morceaux :
    -   et
    -   ou
    -   xor
-   on effectue un `xor` entre certaines parties de la représentation binaire.
-   avantage du xor :
    -   pas de retenue
    -   pas d'accumulation :
        -   x and x <= x
        -   x or x >= x

```python

def xor(a, b):
    return a ^ b

def h_compression(cle):
    """
    :param cle: (str) une clé
    :param liste_bits: (tuple) la liste des bits utilisé
    """
    res = 0
    for c in cle:
        code = car_to_code(c)
        res = res ^ code
    return res

print([bin(h_compression(c)) for c in CLES ])
```

    ['0b1', '0b1010', '0b111', '0b101']

-   désavantage : les permutations d'un même mot sonts hachés de la même façon
    
    *une bonne fonction de hachage doit briser les sous-chaînes de bits*


### par division (modulaire)

-   $h(c) = c\,mod\, m$

($m$ est la taille du tableau)

-   avantage : rapide à calculer
-   inconvénients : dépend de la valeur de $m$. Si par ex. $m$ est pair, toutes les clés paires vont dans des indices
    
    Ex: $m = 37$


### par multiplication

$$h(c) = \lfloor {\tt frac}(e \times \theta) \times m\rfloor$$ avec $0<\theta<1$


# Résolution des collisions par chaînage (méthodes indirectes)


## Hachage avec chaînage séparé

Dans le tableau, on ne conserve que des références vers les listes chaînées : tous les élément en collision sont chaînés entre eux à l'extérieur du tableau de hachage.

Pour ajouter un élément :

-   on effectue un test d'appartenance à la liste chaînée.
-   On réalise l'ajout en fin de liste (à l'issue du test précédent, on possède une référence vers l'élément de queue)

Exemple : $e_i$ avec $h(e_i) = l[i]$ avec $l = [3,1,4,1,5,9,2,6,5,3,5]$


### complexité

Les algorithmes de recherche, ajout et suppression effectue

-   un calcul de la valeur de hachage
-   des comparaisons avec les éléments de la liste déterminée.

1.  Complexité en moyenne d'une recherche négative

    -   le cout de la recherche négative a le même cout que l'ajout.
    
    -   La fonction $h$ est uniforme : il y a même probabilité $1/m$ d'effectuer la recherche dans chacune des $m$ listes.
    -   Pour un tableau $T$, Si $\lambda_i$ est la longueur de la liste $\ell_i$, alors le cout moyen d'une recherche négative est
        
        $$C_{\tt rech^-}(T)= \frac{1}{m}\sum_{i=1}^m\lambda_i$$
        
        Par conséquent, $C_{\tt rech}^-(T) = \frac{n}{m} = \alpha$ (taux de remplissage)
    -   Pour tous les tableaux : le cout ne dépend pas du tableau. Donc $C_{{\tt rech}^-}(T)=\alpha$ (il suffit de considérer que les $m^n$ tableaux différents)

2.  Complexité en moyenne d'une recherche positive

    -   le coût d'une recherche positive est égal, en nombre de comparaisons d'éléments, au cout de l'ajout de cet élément, augmenté de 1.
    -   Si l'élément recherché a été le $(i+1)^e$ ajouté, alors son ajout a eu le même coût qu'une recherche négative parmi $i$ éléments. Soit en moyenne
        
        $$C_{\tt rech-}(m, i) = \frac{i}{m}$$
        
        Si les $n$ éléments présents ont la même probabilité d'être recherchés, alors on a
        
        $$\begin{array}{rcl} C^+_{\tt rech}(m, n) &=& \frac{1}{n}\sum_{i=0}^{n-1}C^-_{\tt rech}(m, i)+1\\ &=& \frac{1}{n}\sum_{i=0}^{n-1}\frac{i}{m}+1\\ &=&\frac{1}{nm}\frac{n(n-1)}{2}+1 = \frac{\alpha}{2}-\frac{1}{2m}+1 \end{array}$$


# Résolution des collisions par calcul : méthodes directes

-   résolution des collisions par *calcul* à l'intérieur du tableau.

-   repose sur une fonction `essai`
    
    $${\tt essai}\,:\, U\longrightarrow \lbrace 1, 2, \ldots , m\rbrace^m $$
    
    qui à chaque élément $x$ de l'ensemble des clés l'$U$, associe une permutation de {1, 2, &#x2026;m}.


## hachage linéaire

Dans cette méthode, lorsqu'il y a collision sur une case d'indice $v$, alors on essaie la case d'indice $v+1$ du tableau :

${\tt essai}_1(x) = h(x)$ ${\tt essai}_2(x) = (h(x) + 1) % m$ $\ldots$ ${\tt essai}_i(x) = (h(x) + (i-1)) % m$


### exemple

hachage primaire :

| e1 | e2 | e3 | e4 | e5 | e6 | e7 | e8 | e9 |
|--- |--- |--- |--- |--- |--- |--- |--- |--- |
| 6  | 4  | 7  | 4  | 8  | 2  | 5  | 9  | 8  |

On répartit ces éléments dans un tableau à 10 éléments.

On obtient la répartition suivante :

| 1  | 2  | 3 | 4  | 5  | 6  | 7  | 8  | 9  | 10 |
|--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |
| e9 | e6 |   | e2 | e4 | e1 | e3 | e5 | e7 | e8 |


### complexité

-   pour une recherche positive : $C^+_{\tt rech}(m, n) \sim \frac{1}{2}(1+\frac{1}{1-\alpha})$
-   pour une recherche négative : $C^-_{\tt rech}(m, n) \sim \frac{1}{2}(1+\frac{1}{(1-\alpha)^2})$


## Double hachage

On prend comme suite d'essais :

${\tt essai}_i(x) = h(x) + k(i-1) \pmod{m}$

-   Si $k$ et $m$ sont premiers entre eux, alors la suite des $m$ essais comporte toutes les places du tableau
-   Pour éviter les groupements de $k$ en $k$, il faut que $k$ dépende de l'élément.

Il vient la fonction suivante :

${\tt essai}_i(x) = h(x) + d(x)(i-1) \pmod{m}$

La fonction $d$ doit être telle que pour tout x de U, la suite des $m$ essais fournit toutes les valeurs du tableau.

C'est le cas si, et seulement si, $d(x)$ est premier avec m.

-   si $m$ est un nombre premier, alors il suffit que $d$ soit à valeur dans $[1, m-1]$.
-   si $m=2^p$ , alors il suffit que d soit impair. On peut prendre $d(x) = 2d'(x) +1$, ou d' est à valeurs dans $[0, 2^{p-1}-1]$


### Complexité

$$C^-_{\tt rech}(m, n) \sim \frac{1}{1-\alpha}$$

$$C^+_{\tt rech}(m, n) \sim \frac{1}{\alpha}\log\frac{1}{1-\alpha}$$