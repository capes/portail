"""A module used to compile dot code."""

from abc import abstractmethod
import hashlib
import os
import graphviz

WHITE = '#FFFFFF'
BLACK = '#000000'
RED = '#FF0000'


def escape_str(obj):
    """
    Convertit l'objet obj en une chaîne de caractères ASCII.

    fct utile pour méthode to_dot des BinaryTree
    """
    chaine = str(obj)
    chaine_echap = ''
    for c in chaine:
        n = ord(c)
        if 32 <= n <= 126 and c != '"':
            chaine_echap += c
        else:
            chaine_echap += '\\x{:04X}'.format(n)
    return chaine_echap


class AbstractDotFigure:
    """An abstraction of an exportable dot figure."""

    def __init__(self, **optns):
        """Construct a new figure."""
        self._background_color = optns.get('background_color', WHITE)

    @abstractmethod
    def to_dot(self, **optns) -> str:
        """Return a dot representation of a figure."""
        pass


class Dot:
    """A class embedding dot code."""

    def __init__(self, code, **optns):
        """Construct a new dot code."""
        self._background_color = optns.get('background_color', WHITE)
        filename = optns.get('filename', '')
        if isinstance(code, AbstractDotFigure):
            self._code = code.to_dot(**optns)
        elif isinstance(code, str):
            self._code = code
        else:
            raise ValueError("invalid code object")
        if filename == "":
            self._filename = hashlib.sha1(self._code.encode()).hexdigest()
        else:
            self._filename = filename

    def show(self):
        """
        Show the figure and produce two files.

        le premier contenant la description de l'arbre au format dot,
        le second contenant l'image au format PNG.
        """
        graphviz.Source(self._code, format='png').view(filename=self._filename)

    def savefig(self) -> str:
        """
        Sauvegarde l'arbre et produit deux fichiers : filename et filename.png.

        le premier contenant la description de l'arbre au format dot,
        le second contenant l'image au format PNG.
        """
        graphviz.Source(self._code,
                        format='png').render(filename=self._filename)
        return f'{self._filename}.png'

    def _repr_png_(self):
        """Return a descriptor on the open png file."""
        return open(self.savefig(), 'rb').read()

    def __del__(self):
        """Clean the mess."""
        try:
            os.remove(f"{self._filename}")
            os.remove(f"{self._filename}.png")
        except OSError:
            pass


if __name__ == "__main__":
    import doctest
    doctest.testmod()
