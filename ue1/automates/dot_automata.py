"""Dot drawable automata class."""

from automata import AutomataDecorator, SequentialAutomataDecorator
from dot import AbstractDotFigure

EPSILON_CHAR = chr(0x3b5)


class DotAutomata(AutomataDecorator, AbstractDotFigure):
    """A class that represent a dot drawable automata."""

    def __init__(self, *args):
        """Initialise a new dot automata."""
        AbstractDotFigure.__init__(self)
        super().__init__(*args)

    def determinize(self):
        return DotAutomata(super().determinize())

    def __state_to_dot(self, state) -> str:
        """Return dot representation of a state."""
        color, shape = "black", "circle"
        if state in self.final_states():
            shape = "doublecircle"
        if state in self.current_states():
            color = "red"
        res = f"\"{state}\" [shape={shape}, color={color}, fontcolor=black];"
        return res

    def to_dot(self, **optns) -> str:
        """Return a dot representation of automata."""
        nodes = "\n".join(self.__state_to_dot(state)
                          for state in self.states())
        transitions = "\n".join("\"{}\" -> \"{}\"[label=\"{}\"];"
                                .format(a,
                                        state,
                                        l if l != "" else EPSILON_CHAR)
                                for a, l in self.decorated().transitions()
                                for state in self.decorated().transitions()[a, l])
        return """
digraph {{
  bgcolor="transparent";
  rankdir=LR;
  {}
  start [shape=point, style=invis];
  start -> {};
  {}
}}""".format(nodes, self.initial(), transitions)


class DotSequentialAutomata(SequentialAutomataDecorator, AbstractDotFigure):
    """a class that represents a dot drawable sequential automata."""

    def __init__(self, *args):
        """Initialise a new dot automata."""
        AbstractDotFigure.__init__(self)
        super().__init__(*args)

    def __state_to_dot(self, state) -> str:
        """Return dot representation of a state."""
        color, shape = "black", "circle"
        # if state in self.final_states():
        #     shape = "doublecircle"
        if state in self.current_states():
            color = "red"
        res = f"{state} [shape={shape}, color={color}, fontcolor=black];"
        return res

    def to_dot(self, **optns) -> str:
        """Return a dot representation of the sequential automata."""
        nodes = "\n".join(self.__state_to_dot(state)
                          for state in self.states())
        transitions = "\n".join("{} -> {}[taillabel=\"{}\",label=\"{}\"];"
                                .format(a, state, l,
                                        self.decorated().impressions()[(a, l)])
                                for (a, l), states in self.decorated().transitions().items()
                                for state in states)

        return """
digraph {{
  rankdir=LR;
  node [shape=point, color=white, fontcolor=white]; start;
  {}
  start -> {};
  {}
}}""".format(nodes, self.initial(), transitions)
