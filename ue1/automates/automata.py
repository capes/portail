#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'B. Papegay'
__date_creation__ = 'Nov 2021'
__doc__ = """
:mod:`automata` module
:author: {:s}
:creation date: {:s}
:last revision: Dec 2023

Define a class for automata
""".format(__author__, __date_creation__)

from random import choice
from typing import Dict, Set, Tuple


class StateDict:
    """A state dictionnary."""

    def __init__(self, automata: "Automata", state: str):
        """
        Construct a new state dictionnary.

        :param automata: (Automata) an automata
        :param state: (int) the state
        """
        self._automata = automata
        self._state: str = state

    def __setitem__(self, key: str, item: str):
        """
        Set item to key.

        :param key! (any) the key
        :param item: (any) the state
        """
        self._automata[(self._state, key)] = item

    def __getitem__(self, key: str):
        """
        Get state associated to key.

        :param key: (any) le symbole
        """
        return self._automata[(self._state, key)]

    def __delitem__(self, key):
        """
        Remove key association.

        :param key! (any) le symbole
        """
        del self._automata[(self._state, key)]


class Automata:
    """A class representing an automata."""

    def __init__(self, initial_state: str,
                 final_states: Set[str],
                 transitions: Dict[Tuple[str, str], str] = {}):
        """
        Construct a new automata.

        - CU set(transitions.keys()) == states and
             all(set(transitions[q].keys()) == alphabet)
        """
        self._states = set()
        self._initial_state = initial_state
        self._states.add(initial_state)
        self._final_states = final_states
        self._states = self._states.union(self._final_states)
        self._transitions: Dict[Tuple[str, str], Set[str]] = {}
        self._alphabet: Set[str] = set()
        self._current_states: Set[str]
        if transitions is not None:
            for state, link in transitions.items():
                self[state] = link
        self.reset()

    def state(self):
        """
        Return an automata current state.

        if the automate is deterministic, then return the current state.
        otherwise, one should considere using current_states.
        """       
        return choice(list(self._current_states))

    def current_states(self):
        """
        Return the current automata actives states.
        """
        return self._current_states

    def set_final(self, state):
        """Set a state final."""
        self._states.add(state)
        self._final_states.add(state)

    def is_final(self):
        """Return True ssi the automata is in final state."""
        return len(self._current_states.intersection(self._final_states)) > 0


    def is_stuck(self):
        """Return True iff the automata is stuck."""
        return len(self.current_states()) == 0

    def reset(self):
        """Réinitialise l'état."""
        self._current_states = self.closure({self._initial_state})

    def closure(self, states: Set[str]) -> Set[str]:
        """compute a states set closure."""
        res = set()
        agenda = set(states)
        while len(agenda) > 0:
            st = agenda.pop()
            res.add(st)
            if (st, "") in self._transitions:
                for nst in self._transitions[(st, "")]:
                    if nst not in res:
                        agenda.add(nst)
        return res

    def next(self, symbol: str):
        """Compute next state."""
        next_states = set()
        for state in self._current_states:
            if (state, symbol) in self._transitions:
                for new_state in self._transitions[(state, symbol)]:
                    next_states.update(self.closure({new_state}))
        self._current_states = next_states
        return self

    def accept(self, word) -> bool:
        """
        Return True si l'automate accepte le mot, False sinon.

        :param word: (iterable) un iterable de symboles
        """
        self._current_states = {self._initial_state}
        for symbol in word:
            self.next(symbol)
        res = self.is_final()
        return res

    def __setitem__(self, key: Tuple[str, str],
                    item: str):
        """
        add transition

        :param key: (any) un état
        :param item: (dict) les transitions
        :Exemple:

        >>> auto = Automata('a', 'b')
        >>> auto['a'] = { 0 : 'b'}
        >>> auto['a'][0] == 'b'
        True
        """
        if isinstance(key, tuple) and len(key) == 2:
            state, link = key
            self._states.add(state)
            self._states.add(item)
            self._alphabet.add(link)
            if (state, link) in self._transitions:
                self._transitions[(state, link)].add(item)
            else:
                self._transitions[(state, link)] = {item}

    def __getitem__(self, key: tuple[str,str]|str):
        """
        Renvoie une transition.

        :param key: (any) un état
        :return: (dict) les transitions pour cet état
        """
        if isinstance(key, tuple) and len(key) == 2:
            state, link = key
            res = self._transitions[(state, link)]
        else:
            res = StateDict(self, key)
        return res

    def __delitem__(self, key):
        """
        Supprime une association.

        :param key: (tuple) un couple (etat, symbol) à supprimer
        """
        if isinstance(key, tuple) and len(key) == 2:
            del self._transitions[key]

    def final_states(self) -> Set[str]:
        """Return final states."""
        return set(self._final_states)

    def states(self) -> Set[str]:
        """Return all automata states."""
        return set(self._states)

    def transitions(self) -> Dict[Tuple[str, str],Set[str]]:
        return self._transitions

    def initial(self) -> str:
        """return initial state."""
        return self._initial_state

    def letters(self, start: str, ending: str) -> set[str]:
        """return letters from start state to ending state """
        return {letter for letter in self._alphabet
                if (start, letter) in self._transitions and
                ending in self._transitions[(start, letter)]}

    def is_normalized(self) -> bool:
        return len(self.final_states()) == 1 and \
            self.initial() not in self.final_states() and \
            all(len(self.transitions()[(st, l)])==0
                for st in self.final_states()
                for l in self._alphabet)

    def is_deterministic(self) -> bool:
        """Return True iff this automata is deterministic."""
        return all(len(states) == 1 for states in self._transitions.values()) and \
            "" not in {letter for _, letter in self._transitions}

    @staticmethod
    def _states_to_str(states: Set[str]) -> str:
        return ",".join(sorted(list(states)))

    def determinize(self):
        """Return a new deterministic automata that recognise the same language as self."""
        res = Automata(self.initial(), set())
        queue = [self.closure(set(self.initial()))]
        while len(queue) > 0:
            states = queue.pop(0)
            for letter in self._alphabet:
                new_states = set()
                is_final = False
                for st in states:
                    if (st, letter) in self.transitions():
                        new_states.update(self.closure(self.transitions()[(st, letter)]))
                        if st in self.final_states():
                            is_final=True
                new_state = Automata._states_to_str(new_states)
                if new_state not in res.states():
                    queue.append(new_states)
                res[(Automata._states_to_str(states), letter)] = new_state
                if is_final:
                    res.set_final(new_state)
        return res

    def _raffine(self, aset: set[str], letter: str) -> tuple[set[str], set[str]]:
        assert self.is_deterministic(), "the automata should be deterministic."
        inset, outset = set(), set()
        for state in aset:
            if self.transitions()\
            .get((state, letter), set())\
            .issubset(aset):
                inset.add(state)
            else:
                outset.add(state)
        return (inset, outset)

    def minimize(self):
        """Return a minimize automata equivalent to self."""
        self_det = self if self.is_deterministic() else self.determinize()
        partitions = [self_det.final_states(),
                      self_det.states() ^ self.final_states()]
        finished = False
        while not finished:
            finished = True
            for letter in self._alphabet:
                # rafine partitions through letter
                for aset in partitions:
                    inset, outset = self_det._raffine(aset, letter)

                    if len(outset) > 0:
                        finished = False



class UnionAutomata(Automata):

    def __init__(self, left_automata, right_automata):
        assert isinstance(left_automata, Automata) and \
            isinstance(right_automata, Automata), \
            "only automata parameters"
        assert left_automata.is_normalized(), \
            "the first automata is not normlized"
        assert right_automata.is_normalized(), \
            "the second automata is not normlized"
        super().__init__('i', {'f'})
        for k, vs in left_automata.transitions().items():
            state, letter = k
            for v in vs:
                self[(f"l{state}", letter)] = f"l{v}"
        for k, vs in right_automata.transitions().items():
            state, letter = k
            for v in vs:
                self[(f"r{state}", letter)] = f"r{v}"
        self[('i', "")] = f"l{left_automata.initial()}"
        self[('i', "")] = f"r{left_automata.initial()}"
        for state in left_automata.final_states():
            self[(f'l{state}', '')] = 'f'
        for state in right_automata.final_states():
            self[(f'r{state}', '')] = 'f'
        self.reset()

class ConcatAutomata(Automata):

    def __init__(self, left_automata, right_automata):
        assert isinstance(left_automata, Automata) and \
            isinstance(right_automata, Automata), \
            "only automata parameters"
        assert left_automata.is_normalized(), \
            "the first automata is not normlized"
        assert right_automata.is_normalized(), \
            "the second automata is not normlized"
        super().__init__('i', {'f'})
        for k, vs in left_automata.transitions().items():
            state, letter = k
            for v in vs:
                self[(f"l{state}", letter)] = f"l{v}"
        for k, vs in right_automata.transitions().items():
            state, letter = k
            for v in vs:
                self[(f"r{state}", letter)] = f"r{v}"
        self[('i', "")] = f"l{left_automata.initial()}"
        for state in left_automata.final_states():
            self[(f"l{state}", '')] = f"r{right_automata.initial()}"
        for state in right_automata.final_states():
            self[(f"r{state}", '')] = 'f'
        self.reset()
                

class StarredAutomata(Automata):

    def __init__(self, automata):
        assert isinstance(automata, Automata), \
            "only automata parameters"
        assert automata.is_normalized(), \
            "the automata is not normalized"
        super().__init__('i', {'f'})
        for link, states  in automata.transitions().items():
            start_state, letter = link
            for end_state in states:
                self[(f"e{start_state}", letter)] = f"e{end_state}"
        self[('i', '')] = f"e{automata.initial()}"
        for state in automata.final_states():
            self[(f"e{state}", '')] = f"e{automata.initial()}"
            self[(f"e{automata.initial()}", '')] = f"e{state}"
            self[(f"e{state}", '')] = state
        self.reset()


class LetterAutomata(Automata):

    def __init__(self, letter: str):
        """Initialize a new letter automata."""
        super().__init__('i', {'f'})
        self[('i', letter)] = 'f'
        self.reset()

class AutomataDecorator(Automata):
    """A decorator for automata."""

    def __init__(self, *args):
        """Construct a new automata decorator."""
        if len(args) == 1 and isinstance(args[0], Automata):
            self._decorated_automata = args[0]
        else:
            self._decorated_automata = Automata(*args)

    def decorated(self):
        return self._decorated_automata

    def state(self):
        """Return the automata state."""
        return self._decorated_automata.state()

    def current_states(self):
        """
        Return the current automata actives states.
        """
        return self._decorated_automata.current_states()

    def set_final(self, state):
        """Set a state final."""
        self._decorated_automata.set_final(state)

    def is_final(self):
        """Return True ssi l'automate est dans un état final."""
        return self._decorated_automata.is_final()

    def reset(self):
        """Réinitialise l'état."""
        self._decorated_automata.reset()

    def next(self, symbol):
        """Compute next state."""
        self._decorated_automata.next(symbol)
        return self

    def transitions(self):
        return self._decorated_automata.transitions()

    def accept(self, word) -> bool:
        """
        Return True iff word is accepted by automata.

        :param word: (iterable) un iterable de symboles
        """
        return self._decorated_automata.accept(word)

    def __setitem__(self, key, item):
        """
        Ajoute des transitions pour un état.

        :param key: (any) un état
        :param item: (dict) les transitions
        :Exemple:

        >>> auto = Automata('a', 'b')
        >>> auto['a'] = { 0 : 'b'}
        >>> auto['a'][0] == 'b'
        True
        """
        self._decorated_automata[key] = item

    def __getitem__(self, key):
        """
        Renvoie une transition.

        :param key: (any) un état
        :return: (dict) les transitions pour cet état
        """
        return self._decorated_automata[key]

    def __delitem__(self, key):
        """
        Supprime une association.

        :param key: (tuple) un couple (etat, symbole) à supprimer
        """
        del self._decorated_automata[key]

    def final_states(self):
        """Return final states."""
        return self._decorated_automata.final_states()

    def states(self) -> Set[str]:
        """Return all automata states."""
        return self._decorated_automata.states()

    def initial(self) -> str:
        return self._decorated_automata.initial()

    def letters(self, start: str, ending: str) -> set[str]:
        """return letters from start state to ending state """
        return self._decorated_automata.letters(start, ending)

    def is_normalized(self) -> bool:
        return self._decorated_automata.is_normalized()

    def is_deterministic(self) -> bool:
        return self._decorated_automata.is_deterministic()

    def determinize(self):
        return self._decorated_automata.determinize()

class SequentialAutomata(Automata):
    """A class representing a sequential machine."""

    def __init__(self, *args):
        if len(args) == 1 and isinstance(args[0], str):
            super().__init__(args[0], set())
            self._output_table = dict()
            self.reset_output()
        else:
            raise ValueError('invalid argument number or type')

    def __setitem__(self, key: str, item: tuple[str, str]):
        """add transition."""
        if isinstance(key, tuple) and\
           len(key) == 2 and\
           isinstance(item, tuple) and\
           len(item) == 2:
            state, link = key
            next_state, out_car = item
            self._states.add(state)
            self._states.add(next_state)
            self._alphabet.add(link)
            if (state, link) in self._transitions:
                self._transitions[(state, link)].add(next_state)
            else:
                self._transitions[(state, link)] = {next_state}
            self._output_table[(state, link)] = out_car

    def next(self, symbol):
        """Compute next state."""
        old_state = self.state()
        res = super().next(symbol)
        self._output += self._output_table[(old_state, symbol)]
        return res

    def output(self) -> str:
        """Return machine's current output."""
        return self._output

    def reset_output(self):
        """Reset machine output."""
        self._output = ""

    def reset(self):
        """Reset the machine."""
        self.reset_output()
        super().reset()

    def impressions(self):
        """Get outputs table."""
        return self._output_table


class SequentialAutomataDecorator(SequentialAutomata):
    """A decorator for sequential automata."""

    def __init__(self, *args):
        """Construct a new automata decorator."""
        if len(args) == 1 and isinstance(args[0], SequentialAutomata):
            self._decorated_automata = args[0]
        else:
            self._decorated_automata = SequentialAutomata(*args)

    def output(self) -> str:
        return self._decorated_automata.output()

    def reset_output(self):
        self._decorated_automata.reset_output()

    def decorated(self):
        return self._decorated_automata

    def state(self):
        """Return the automata state."""
        return self._decorated_automata.state()

    def current_states(self):
        """Return the automata state."""
        return self._decorated_automata.current_states()

    def reset(self):
        """Réinitialise l'état."""
        self._decorated_automata.reset()

    def next(self, symbol):
        """Compute next state."""
        return self._decorated_automata.next(symbol)

    def accept(self, word) -> bool:
        """
        Return True iff word is accepted by automata.

        :param word: (iterable) un iterable de symboles
        """
        return self._decorated_automata.accept(word)

    def __setitem__(self, key, item):
        """
        Ajoute des transitions pour un état.

        :param key: (any) un état
        :param item: (dict) les transitions
        :Exemple:

        >>> auto = Automata('a', 'b')
        >>> auto['a'] = { 0 : 'b'}
        >>> auto['a'][0] == 'b'
        True
        """
        self._decorated_automata[key] = item

    def __getitem__(self, key):
        """
        Renvoie une transition.

        :param key: (any) un état
        :return: (dict) les transitions pour cet état
        """
        return self._decorated_automata[key]

    def __delitem__(self, key):
        """
        Supprime une association.

        :param key: (tuple) un couple (etat, symbole) à supprimer
        """
        del self._decorated_automata[key]

    def states(self) -> Set[str]:
        """Return all automata states."""
        return self._decorated_automata.states()

    def initial(self) -> str:
        return self._decorated_automata.initial()

    def impressions(self):
        return self._decorated_automata.impressions()


class TuringMachine(Automata):
    """A class representing a Turing machine."""

    def __init__(self, *args):
        if len(args) == 2 and \
           isinstance(args[0], str) and \
           isinstance(args[1], set) and \
           all(isinstance(etat, str) for etat in args[1]):
            super().__init__(args[0], args[1])
            self._output_table = {}
            self._movements = {}
            self.set_conf(" ", 0)
        else:
            raise ValueError('invalid argument number or type')

    def set_conf(self, word, index):
        self._tape = list(word)
        self._index = index

    def get_conf(self):
        return ("".join(self._tape), self._index)

    def __setitem__(self, key: Tuple[str,str],
                    item: Tuple[str, str, int]):
        """
        """
        if isinstance(key, tuple) and\
           len(key) == 2 and\
           isinstance(item, tuple) and\
           len(item) == 3:
            state, link = key
            next_state, out_car, dep = item
            self._states.add(state)
            self._states.add(next_state)
            self._alphabet.add(link)
            if (state, link) in self._transitions:
                self._transitions[(state, link)].add(next_state)
            else:
                self._transitions[(state, link)] = {next_state}
            self._output_table[(state, link)] = out_car
            self._movements[(state, link)] = dep

    def next(self, symbol):
        """Compute next state."""
        res = self
        if not self._stopped:
            old_state = self.state()
            res = super().next(symbol)
            self._stopped = self.is_final()
            self._tape[self._index] = self._output_table[(old_state, symbol)]
            if self._movements[(old_state, symbol)] > 0:
                if len(self._tape) == self._index + 1:
                    self._tape.append(' ')
                    self._index += 1
            else:
                if self._index > 0:
                    self._index -= 1
                else:
                    self._stopped = True
        return res


if __name__ == "__main__":
    import doctest
    doctest.testmod()
