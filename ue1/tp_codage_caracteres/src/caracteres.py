#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>_'
__date_creation__ = '<2020-09-19 sam.>'
__doc__ = """
:mod:`caracteres` module
:author: {:s} 
:creation date: {:s}
:last revision:

Provides functions to converts file from iso-8859-1 to utf-8 and from
utf-8 to iso-8859-1
""".format(__author__, __date_creation__)

def isolatinchar_to_utf8(byte):
    """
    Converts an ISO-859-1 character to an UTF-8 one
    
    :param byte: (int) a single byte representing an ISO-859-1 character
    :return: (list) One or two bytes representing the same character as `byte` but in UTF-8
    :CU: 0<= byte < 128 and 160 <= byte < 256
    :Examples:
    
    >>> isolatinchar_to_utf8(65)
    [65]
    >>> isolatinchar_to_utf8(201)
    [195, 137]
    >>> isolatinchar_to_utf8(160)
    [194, 160]
    >>> isolatinchar_to_utf8(255)
    [195, 191]
    """
    pass

def isolatin_to_utf8(instream, outstream):
    
    pass

def convert_file_isolatin_utf8(source, dest):
    """
    converts `source` file from ISO-8859-1 encoding to UTF-8.
    The output is written to the `dest` file.
    """
    with open(source, 'rb') as input_stream:
        with open(dest, 'wb') as output_stream:
            isolatin_to_utf8(input_stream, output_stream)

def utf8char_to_isolatin(two_bytes):
    """
    Converts an UTF-8 character to a ISO-8859-1 one.
    :param two_bytes: (list) The number of bytes to represent a character is variable. two_bytes may contain one or two bytes in a list (the list will be of length 1 or 2). The bytes are given as integers (between 0 and 255).
    :return: (list)	A list containing only one byte representing the ISO-8859-1 character
    :CU: 1 <= len(two_bytes) <= 2 and each element of the tuple is a byte (value between 0 and 255)
    :Examples:	
    
    >>> utf8char_to_isolatin([65])
    [65]
    >>> utf8char_to_isolatin([0xC3, 0x89])
    [201]
    >>> utf8char_to_isolatin([194, 160])
    [160]
    >>> utf8char_to_isolatin([195, 191])
    [255]
    """
    pass

def utf8_to_isolatin(instream, outstream):
    """
    converts an UTF-8 input stream to a ISO-859-1 output stream character.
    
    :param instream: an opened stream
    :param outstream: an opened stream
    :return: None
    """
    pass

def convert_file_utf8_isolatin(source, dest):
    '''
    Converts `source` file from UTF-8 encoding to ISO-8859-1.
    The output is written in the `dest` file.
    '''
    with open(source, 'rb') as input_stream:
        with open(dest, 'wb') as output_stream:
            utf8_to_isolatin(input_stream, output_stream)

import doctest
doctest.testmod(verbose = True)
