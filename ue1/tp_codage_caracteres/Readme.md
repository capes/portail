
# Table des matières

- [Table des matières](#table-des-matières)
- [Lecture et écriture de fichiers](#lecture-et-écriture-de-fichiers)
  - [Modes d'ouverture d'un fichier](#modes-douverture-dun-fichier)
  - [Lecture de fichier](#lecture-de-fichier)
  - [Écriture de fichier](#écriture-de-fichier)
  - [Le module `binary_IO`](#le-module-binary_io)
- [Représentation des caractères ISO-8859-1 et UTF-8](#représentation-des-caractères-iso-8859-1-et-utf-8)
  - [Matériel fourni](#matériel-fourni)
  - [La commande `iconv`](#la-commande-iconv)
  - [Conversion d'un fichier du format ISO-8859-1 au format UTF-8](#conversion-dun-fichier-du-format-iso-8859-1-au-format-utf-8)
  - [Conversion d'un fichier du format UTF-8 au format ISO-8859-1](#conversion-dun-fichier-du-format-utf-8-au-format-iso-8859-1)

# Lecture et écriture de fichiers

## Modes d'ouverture d'un fichier

Un fichier peut être lu dans deux modes différents avec Python :

-   Soit il s'agit d’un fichier texte et dans ce cas les fonctions de
    Python renverront, des caractères (qu’ils soient stockés sur 1,
    2, 3 ou 4 octets) ;
-   Soit il s’agit d’un fichier dit binaire (bien que cette appellation
    ait peu de sens, car les données sont toujours représentées en
    binaire) et dans ce cas les fonctions de Python liront et renverront
    des octets.

Nous allons lire un fichier en utilisant ces deux modes pour bien
cerner la différence. Le mode de lecture est déterminé par le second
paramètre passé à la fonction `open`. Le mode de lecture par défaut est le
mode texte.  
Pour lire (ou écrire un fichier) en mode binaire, il faut
ajouter un `b` à la chaîne de caractères correspondant au mode d’ouverture
du fichier. Donc pour ouvrir un fichier en lecture en mode binaire, il
faut utiliser le mode `rb` et pour l’ouvrir en mode écriture binaire il
faut utiliser le mode `wb`.

## Lecture de fichier

**à faire** 

Ouvrez le fichier `data` en lecture en mode texte ainsi qu'en mode
binaire. Vous stockerez le résultats des appels à la fonction
`open` dans deux variables `stream_text` et `stream_bin`
respectivement pour le fichier ouvert en mode texte et celui en
mode binaire.

Ces deux variables correspondent à un *flux* ouvert en lecture. Il
est ensuite possible de lire un tel flux avec la méthode `read` et
d'écrire un flux (s'il a été ouvert en écriture) grace à la méthode
`write`.

Un flux peut être, comme ici, un fichier ouvert mais également une
connexion réseau ou encore une chaîne de caractères. Le flux permet
justement d'abstraire les opérations de lecture et d'écriture.

**à faire** 

Lisez tout le flux en un seul appel à la méthode `read()` et stockez
le résultat dans une variable `content_text` (pour le flux ouvert en
mode texte) et une variable `content_bin` (pouve le flux ouvert en
mode binaire). Donnez la longueur lue pour les deux modes de
lecture. Expliquez les résultats obtenus.

Pour rappel, en mode texte les fichiers sont par défaut ouvert en
mode UTF-8 sous Linux. Il est possible de modifier ce comportement
en utilisant le paramètre `encoding` de la méthode `open`.

**à faire**

Quel est le type de la variable `content_bin` ?

**à faire**

Comment accéder au deuxième octet lu en mode binaire ?

## Écriture de fichier

Nous allons maintenant écrire un fichier en mode binaire. Là encore
le nom du mode est ambigu. Cela ne signifie que l'écriture
s'effectue bit à bit. Au contraire, la fonction d'écriture prend en
entrée des octets.

Par exemple, si `output` est un flux ouvert en écriture en mode
binaire, `output.write(bytes([65, 66]))` écrit les octets 65 et 66
dans le fichier (ce qui correspond aux caractères A et B).

**à faire** 

Ouvrir un nouveau fichier nommé `data.out` en écriture en mode
binaire et écrire à l'intérieur les octets 195 et 137. Il faut
ensuite penser à fermer le fichier.

**à faire**

Ouvrez maintenant le fichier avec un éditeur de texte (ce qui
revient à l'ouvrir en mode texte). Combien de caractères
possède-t-il ? Comment l'expliquez-vous ?

## Le module `binary_IO`

Afin de simplifier les problématiques de lecture et écriture dans
une flux binaire, nous vous fournissons le module [binary<sub>IO</sub>](./src/binary_IO.py). Ce
module travaille exclusivement avec les listes d'octets : soit les
octets lus dans le fichier (`Binary_IO.Reader`), soit les octets à
écrire dans le fichier (`Binary_IO.Writer`).

L’exemple que vous trouverez en tête du fichier Binary<sub>IO</sub> (et
auquel on a également accès en faisant `help(Binary_IO)` dans
l’interpréteur Python) montre un cas où on commence à ouvrir un
fichier en écriture (avec un `Writer`) pour créer un fichier nommé
`foo` et dans lequel on écrit 4 octets (de valeur 100, 90, 32 et
10).  
Le fichier est ensuite fermé, puis ouvert en lecture avec un
`Reader`. On demande alors à lire dans le fichier 3 octets, qui sont
renvoyés dans une liste. Le processus est renouvelé : un seul octet
est renvoyé dans la liste (car il n’y en a pas d’autres à
lire).  
Enfin, si on recommence à nouveau : la liste renvoyée est
vide, car la totalité du fichier a été lu. Vous noterez aussi qu’un
`Reader` (ou un `Writer`) peut prendre en entrée un flux déjà ouvert,
pour peu qu’il soit appelé avec le paramètre `stream=`.

Dans la suite, vous utiliserez ce module afin de simplifier la
lecture et l’écriture d’octets dans des fichiers.

    stream_bin.close()
    stream_text.close()

<a id="org7647f23"></a>

# Représentation des caractères ISO-8859-1 et UTF-8

<a id="orged9108f"></a>

## Matériel fourni

-   Un fichier texte au format ISO-8859-1 contenant la fable [La Cigale et la Fourmi](cigale-ISO-8859-1.txt) ;
-   Un fichier texte au format UTF-8 contenant la fable [La Cigale et la Fourmi](./cigale-UTF-8.txt) ;
-   Le module [caracteres.py](src/caracteres.py) à compléter ;
-   La table des caractères [ISO-8859-1](codeISO-8859.md) ;
-   le principe du code [UTF-8](codeUTF-8.md).

## La commande `iconv`

Sous Unix, la commande `iconv` permet de convertir un fichier d'un
codage dans un autre.

Ainsi, pour convertir un fichier texte du format ISO-8859-1 dans le
format UTF-8, on utilise la commande :

    iconv --from-code ISO-8859-1 --to-code UTF-8 --output fichier_converti fichier_a_convertir

où `fichier_converti` est le nom du fichier résultat et
`fichier_a_convertir` est le nom du fichier que l'on désire
convertir.

Pour connaitre la liste de tous les codages connus par cette commande 

    iconv --list

Le but du TP est de réaliser deux programmes ; l'un pour convertir
de l'ISO-8859-1 vers l'UTF-8, l'autre pour la conversion dans
l'autre sens (si elle est possible !).

**à faire**

Utilisez la commande `iconv` pour 

1.  convertir le fichier `cigale-ISO-8859-1.txt` en un équivalent codé en UTF-8 ;
2.  convertir le fichier `cigale-UTF-8.txt` en un équivalent codé en ISO-8859-1.

Vérifiez le résultat :

-   en visualisant le contenu des deux fichiers obtenus à l'aide d'un
    éditeur de textes ;
-   puis en comparant les fichiers obtenus avec ceux qui vous sont
    fournis. Pour cela utilisez la commande `diff`;
    
        diff cigale-ISO-8859-1.txt moncigale-ISO-8859-1.txt
-   et enfin en utilisant la commande `file`.
    
        file cigale-UTF-8.txt
    
        file cigale-ISO-8859-1.txt

**à faire**

Comparez les tailles (exprimés en octets) des deux fichiers
`cigale-ISO-8859-1.txt` et `cigale-UTF-8.txt`. Expliquez la
différence constatée.

Pour connaitre la taille d'un fichier,
utilisez la commande `ls` avec l'option `-l`. Par exemple, pour le
fichier `cigale-UTF-8.txt`, on obtient

    ls -l cigale-UTF-8.txt

    -rw-rw-r-- 1 bpapegay bpapegay 639 oct.  16  2019 cigale-UTF-8.txt

qui montre que le fichier a une taille de 639 octets.

## Conversion d'un fichier du format ISO-8859-1 au format UTF-8

La conversion d'un fichier texte du format ISO-8859-1 au format
UTF-8 peut se faire simplement par lecture du fichier de départ
octet par octet, et en écrivant dans le fichier d'arrivée un ou
deux octets selon les cas suivants :

1.  si l'octet lu a un bit de poids fort nul, alors c'est un
    caractère ASCII et on le recopie tel quel dans le fichier
    d'arrivée ;
2.  sinon, l'octet a une valeur comprise entre 160 et 255, et on
    doit recopier deux octets dans le fichier d'arrivée
    
    `110xxxxx 10xxxxxx`
    
    où les 11 `x` sont à remplacer par les bits de l'octet lu (les
    trois premiers `x` étant des `0`).  
    Par exemple, le caractère `É`
    a pour code `C9 = 11001001` en ISO-8859-1. Son code en UTF-8 est
    donc `11000011 10001001 = C3 89`.

**à faire**

Comment transformer un octet de valeur comprise entre 160 et 255 en
deux octets conformes à la description donnée plus haut, uniquement
en utilisant des opérations logiques sur les entiers ?

**à faire**

Réalisez la fonction `isolatinchar_to_utf8` du module `caracteres`.

Votre fonction ne devra utiliser que des opérations logiques, pas
d'opérations arithmétiques et **surtout pas de chaînes de caractères**.

**à faire**

Réaliser la fonction `isolatin_to_utf8` qui lit un flux d'entrée,
utilise la fonction précédente et écrit le résultat dans un flux de
sortie.

**à faire** 

Testez votre travail sur le fichier [cigale-ISO-8859-1.txt](cigale-ISO-8859-1.txt), en
lançant la procédure `convert_file_isolatin_utf8`. Vérifiez que
vous obtenez le fichier attendu.

## Conversion d'un fichier du format UTF-8 au format ISO-8859-1

La conversion d’un fichier du format UTF-8 au format ISO-8859-1
n’est pas toujours possible. Nous ne considérerons donc pour ce TP
que les fichiers pour lesquels cette conversion est possible.

**à faire**

Réalisez la fonction `utf8char_to_isolatin`.

**à faire** 

Réalisez la fonction `utf8_to_isolatin`.

**à faire**

Testez votre travail en utilisant la procédure `convert_file_utf8_isolatin`
sur le fichier [cigale-UTF-8.txt](cigale-UTF-8.txt).