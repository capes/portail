Codage de Huffman
=================


Sujet de travaux pratiques
--------------------------

Le sujet rédigé dans un notebook jupyter est accessible [ici](codage_huffman.md).

* dans un fichier source Python [tp_codage_huffman.py](tp_codage_huffman.py)

Vous travaillez à votre préférence dans un notebook ou Thonny.

Les fichiers annexes suivants sont également fournis :

* [binary_tree.py](binary_tree.py)
* [binary_IO.py](binary_IO.py)
* [cigale-UTF-8.txt](cigale-UTF-8.txt)
* [exple_huffman_tree.png](exple_huffman_tree.png)

