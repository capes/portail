---
title: Codage de Huffman
author: | 
        | DIU Enseigner l'informatique au lycée
        | Univ. Lille
thanks: Équipe pédagogique DIU EIL Lille. Ce document est mis à disposition selon les termes de la licence Creative Commons (CC-BY-4.0) \protect ![](cc-by.png){width=10%}
date: décembre 2019
lang: fr
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.1.7
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

Le codage de Huffman est un codage binaire des symboles d'une source d'information qui s'appuie sur les fréquences des symboles. L'idée du codage consiste à coder les symboles les plus fréquents par des mots binaires plus courts que les symboles les moins fréquents. Les codages de Huffman sont *optimaux* dans le sens qu'aucun autre codage binaire des symboles ne permet d'obtenir une source encodée avec moins de bits. 

Le but de ce TP est de réaliser une implantation du codage de Huffman.

1. On commence par calculer le codage binaire optimal permettant de coder une chaîne de caractères en construisant l'arbre de Huffman d'un tel codage
2. Puis une fois construit cet arbre, on en déduit une table de codage
3. On s'intéresse ensuite au décodage d'une chaîne de bits produite par le codage d'une source.
4. Enfin on envisage le cas où la source est un fichier :
   * fichier texte d'abord,
   * fichier quelconque ensuite.


Nous aurons besoin d'utiliser des arbres binaires. On importe donc la classe `BinaryTree` du module `binary_tree`.

```python
from binary_tree import BinaryTree as BT
```

La fonction suivante pourra être utile pour visualiser des arbres.

```python
import graphviz
from IPython.display import display
def show_tree(arbre):
    display(graphviz.Source(arbre.to_dot()))
```

# Codage d'une source


Nous allons considérer que les sources à coder sont des chaînes de caractères. Il est très facile d'étendre l'algorithme à d'autres sources, en particulier à des fichiers considérés comme des séquences d'octets (ce qui sera envisagé dans la dernière partie).


Voici un texte qui nous servira d'exemple pour la suite. Cette chaîne de caractères peut bien évidemment être remplacée par toute autre chaîne.

```python
texte_source = '''La Cigale et la Fourmi

La Cigale, ayant chanté
Tout l'été,
Se trouva fort dépourvue
Quand la bise fut venue :
Pas un seul petit morceau
De mouche ou de vermisseau.
Elle alla crier famine
Chez la Fourmi sa voisine,
La priant de lui prêter
Quelque grain pour subsister
Jusqu'à la saison nouvelle.
Je vous paierai, lui dit-elle,
Avant l'Oût, foi d'animal,
Intérêt et principal. 
La Fourmi n'est pas prêteuse :
C'est là son moindre défaut.
Que faisiez-vous au temps chaud ?
Dit-elle à cette emprunteuse.
Nuit et jour à tout venant
Je chantais, ne vous déplaise.
Vous chantiez ? j'en suis fort aise.
Eh bien! dansez maintenant.
''' 
```

## Nombre d'occurrences des symboles de la source


La première étape consiste à déterminer le nombre d'occurrences de chacun des symboles de la source.


**À faire**
Réalisez la fonction spécifiée ci-dessous, et appliquez la à notre exemple de texte source. 

```python
def sequence_to_occurrences(seq):
    '''
    :param seq: une séquence de symboles 
    :return: (dict) un dictionnaire donnant le nombre 
          d'occurrences de chacun des symboles contenu dans seq
    :CU: les symboles de seq doivent pouvoir être une clé d'un dictionnaire.
    :Exemple:
    
    >>> sequence_to_occurrences('Codage de Huffman') == {'C': 1, 'o': 1, 'd': 2, 'a': 2, 'g': 1, 'e': 2, ' ': 2, 'H': 1, 'u': 1, 'f': 2, 'm': 1, 'n': 1}
    True
    '''
    pass
```

```python
occ_source = sequence_to_occurrences(texte_source)
```

## Construction de l'arbre de Huffman d'une source


Connaissant les nombres d'occurrences (ou les fréquences) des différents symboles de la source à encoder, il s'agit maintenant de construire l'arbre d'un codage optimal de cette source en suivant l'algorithme de Huffman.


### Un exemple « à la main »


Commençons par une petit exemple traité «à la main», dans lequel le texte source contient cinq fois la lettre `a`, deux fois la lettre `b` et une fois chacune des lettres `c` et `d`.

```python
exple_occ = {'a':5, 'b':2, 'c':1, 'd':1}
```

Les arbres binaires auront les symboles de la source aux feuilles. Tous les nœuds seront étiquetés par un couple dont 
* la première composante est un symbole de la source pour les feuilles, une chaîne de caractères vide pour les nœuds internes, et 
* la seconde composante est la fréquence du symbole pour les feuilles, la somme des fréquences des symboles situés sous le nœud pour les nœuds internes. 

Voici un exemple d'abre pour la source exemple ci-dessus.

![exemple d'arbre de Huffman](exple_huffman_tree.png)


Nous appellerons *poids d'un arbre* le nombre qui figure dans le couple qui étiquette la racine de l'arbre.


**À faire** Réalisez la fonction suivante qui renvoie le poids d'un arbre.

```python
def weight(tree):
    '''
    :param tree: (BinaryTree) un arbre binaire étiqueté par des couples (truc, nombre)
    :return: (int ou float) le poids de l'arbre, (ie le nombre composante droite du couple étiquette de l'arbre)
    '''
    pass
```

L'algorithme de Huffman construisant l'arbre d'un codage binaire optimal est un exemple d'algorithme glouton qui opère sur une forêt d'arbres en remplaçant les deux arbres dont la racine porte les fréquences les plus petites par un nouvel arbre obtenu en regroupant ces deux arbres en un seul.


On commence avec une forêt constituée d'arbres réduits à une feuille.

```python
VIDE = BT()
foret = [BT(('a', exple_occ['a']), VIDE, VIDE), BT(('b', exple_occ['b']), VIDE, VIDE),
         BT(('c', exple_occ['c']), VIDE, VIDE), BT(('d', exple_occ['d']), VIDE, VIDE)]
```

**À faire** Réalisez la fonction suivante.

```python
def insert(tree, forest):
    '''
    :param tree: (BinaryTree) un arbre étiqueté par des couples (truc, nombre)
    :param forest: (list) une liste d'arbres binaires étiquetés par des couples (truc, nombre) rangés dans l'ordre décroissant des poids
    :return: (NoneType)
    :side effect: modifie la foret en insérant l'arbre à la première position qui maintient la forêt rangée en ordre décroissant
    '''
    pass
```

On retire les deux arbres les « moins lourds » de la forêt et on les regroupe en un seul qu'on remet dans la forêt.

```python
arb1 = foret.pop()
arb2 = foret.pop()
insert(BT(('', weight(arb1) + weight(arb2)), arb1, arb2), foret)
```

À ce stade la forêt contient trois arbres rangés dans l'ordre décroissant de leurs poids. Il aurait pu en être autrement.


On réitère les mêmes opérations qu'auparavant pour obtenir une forêt de deux arbres.

```python
arb1 = foret.pop()
arb2 = foret.pop()
insert(BT(('', weight(arb1) + weight(arb2)), arb1, arb2), foret)
```

Encore une dernière fois pour obtenir une forêt d'un seul arbre.

```python
arb1 = foret.pop()
arb2 = foret.pop()
insert(BT(('', weight(arb1) + weight(arb2)), arb1, arb2), foret)
```

```python
exple_arb_huffman = foret[0]
show_tree(exple_arb_huffman)
```

**Remarque** Lorsque dans une étape de la construction on réunit deux arbres de la forêt en un seul, savoir lequel de ces deux arbres on place à gauche et lequel on place à droite importe peu. Cela implique qu'il n'y a pas unicité de l'arbre de Huffman (et donc du codage optimal) pour une source donnée. L'emploi de l'article défini dans le titre de cette section est donc impropre.


### Une fonction pour la construction


**À faire** Programmez la fonction suivante qui à partir d'un dictionnaire d'occurrences construit l'arbre d'un codage de Huffman pour une source décrite par le dictionnaire. Appliquez cette fonction à la source définie par notre texte exemple.

Pour s'assurer d'avoir toujours un même arbre pour un même dictionnaire d'occurrences (cf remarque précédente), il est important de trier les clés du dictionnaire avant de le parcourir (`sorted(occ.keys())`).

```python
def huffman_tree(occ):
    '''
    :param occ: (dict) dictionnaires d'occurrences
    :return: (BinaryTree) arbre de codage de Huffman
    '''
    pass
```

```python
huffman_source = huffman_tree(occ_source)
show_tree(huffman_source)
```

## Construction de la table de Huffman


Un arbre binaire construit par la méthode de Huffman détermine un codage binaire optimal des symboles de la source. 

Avec l'exemple illustré par la figure ci-dessus nous avons la table de codage ci-dessous dans laquelle figurent à côté de chaque symbole de la source le mot binaire qui lui est associé par le codage :

     symb | code
     -----------
        a | 1
        b | 00
        c | 010
        d | 011


**À faire** Réalisez la fonction suivante qui à partir d'un arbre de Huffman produit la table de codage sous forme d'un dictionnaire.

Appliquez cette fonction à l'exemple du texte source.

```python
def tree_to_coding(tree):
    '''
    :param tree: (BinaryTree) un arbre de Huffman
    :return: (dict) une table de codage
    '''
    pass
```

```python
codage_source = tree_to_coding(huffman_source)
for symb in codage_source:
    print('{:s} : {:s}'.format(symb, codage_source[symb]))
```

## Encodage de la source


Disposant de la table de codage optimal de la source, il ne reste qu'à encoder la source avec ce codage.


**À faire** Réalisez la fonction suivante. 

Encodez le texte source.

```python
def encode(source, coding_table):
    '''
    :param source: (str) texte source à encoder
    :param coding_table: (dict) une table de codage
    :return: (str)
    '''
    pass
```

```python
texte_encode = encode(texte_source, codage_source)
```

**À faire**  Calculez le rapport des longueurs du texte encodé et du texte source. Interprétez le résultat obtenu.

```python
len(texte_encode)/len(texte_source)
```

Ce rapport donne le nombre moyen de bits nécessaires à l'encodage des symboles de la source. 
Ici, chaque caractère du texte source est encodé par le codage de Huffman avec en moyenne 4,65 bits. Or le texte source, lorsqu'il est encodé en UTF-8 est encodé avec 8 bits pour chacun des caractères non accentués et 16 bits pour les caractères accentués.


# Décodage 


Quel est le meilleur outil pour décoder un texte encodé avec un codage de Huffman ?

Si la table de codage permet facilement l'opération de codage en associant à chaque symbole de la source un mot binaire, elle n'est pas d'une grande utilité pour le décodage. En effet, pour le décodage il s'agit de déterminer un symbole de la source à partir d'un mot binaire. Mais étant donné que les mots d'un codage optimal n'ont pas tous la même longueur (c'est le but d'un codage optimal), le problème c'est qu'on ne sait pas quel mot il faut tenter de décoder.

Le bon outil pour le décodage est l'arbre de codage. Avec cet arbre, en parcourant la chaîne de bits à décoder on descent à gauche si le bit courant est un 0, ou à droite si c'est un 1. Lorsqu'on arrive sur une feuille, on a décodé un symbole. Il reste à poursuivre le décodage en repartant de la racine de l'arbre de codage.


**À faire** Réalisez la fonction de décodage décrite ci-dessous en suivant la méthode esquissée ci-dessus.

```python
def decode(encoded_source, tree):
    '''
    :param encoded_source: (str) la chaîne binaire à décoder
    :param tree: (BinaryTree) l'arbre de Huffman du codage utilisé
    :return: (str) le message source décodé
    '''
    pass
```

```python
decode(texte_encode, huffman_source) == texte_source
```

# Source dans des fichiers


Dans cette partie, nous allons mettre en œuvre l'algorithme de Huffman en vue de compresser des fichiers. Les sources ne seront donc plus nécessairement des textes (chaînes de caractères) mais des fichiers considérés comme des séquences d'octets indépendamment de la signification de ces octets.

La méthode de compression envisagée consiste à

1. établir le nombre d'occurrences des octets contenus dans le fichier à compresser ;
2. établir un codage de Huffman correspondant aux nombres d'occurrences déterminés au point précédent ;
3. utiliser ce codage pour encoder la séquence d'octets du fichier ;
4. écrire la chaîne binaire obtenue dans un fichier, au format binaire.



## Complétion


Dans un fichier la quantité atomique d'information qu'il est possible de lire ou écrire est l'octet, c'est-à-dire un nombre entier qui s'écrit en binaire sur huit bits. Pour traiter le dernier point listé ci-dessus, il va falloir découper la chaîne de bits issue du point précédent en facteurs de huit bits. Mais que faire si la longueur de la chaîne n'est pas multiple de huit ?


Par exemple, le texte encodé précédemment a une longueur qui n'est pas multiple de 8.

```python
q, r = divmod(len(texte_encode), 8)
print(q, r)
```

La chaîne de bits représentant le texte encodé peut donc être découpée en 363 facteurs de huit bits avec un reliquat de trois bits.

```python
texte_encode[-3:]
```

Une solution pour pouvoir coder ce reliquat consiste à

* le compléter en ajoutant suffisamment de bits pour en avoir huit
* de façon à pouvoir identifier facilement, lors du décodage, les bits inutiles.

Par exemple, on ajoute au reliquat un mot de la forme `10...0` le nombre de zéros étant choisi de sorte que la longueur du reliquat ajoutée à la longueur de ce mot soit égale à huit.

Avec le reliquat `010` on obtient le mot `O1O1OOOO`.

Au décodage, le dernier mot binaire de longueur huit peut être débarrassé des bits inutiles en repérant le `1` le plus à droite et en éliminant tous les bits à partir de ce `1`.

Pour que cette règle de décodage puisse s'appliquer dans tous les cas, il importe de compléter la chaîne binaire codant une source même si sa longueur est multiple de huit. Dans ce cas on lui ajoute les huit bits inutiles `10000000`.



**À faire** Réalisez la fonction de complétion suivante.

```python
def complete(binary_str):
    '''
    :param binary_str: (str) une chaîne de bits
    :return: (str) une chaîne de bits complétée avec un mot de la forme '10...0' de sorte que la chaîne soit de longueur 8
    :CU: len(binary_str) < 8
    :Exemples:
    
    >>> complete('')
    '10000000'
    >>> complete('010')
    '01010000'
    >>> complete('1010111')
    '10101111'
    '''
    pass
```

```python
[complete(ch) for ch in ('', '010', '1010111')]
```

**À faire** Réalisez la fonction de décomplétion suivante. Vous pourrez utiliser avec profit la méthode `rindex` des chaînes de caractères qui donne l'indice le plus grand d'une occurrence d'un caractère dans la chaîne.

```python
def uncomplete(binary_str):
    '''
    :param binary_str: (str) une chaîne de bits
    :return: (str) une chaîne de bits débarrassée du bit 1 le plus à droite ainsi que des bits qui suivent.
    :CU: binary_str contient au moins un 1.
    :Exemples:
    
    >>> uncomplete('1000000000000000')
    ''
    >>> uncomplete('01010000')
    '010'
    >>> uncomplete('10101111')
    '1010111'
    '''
    pass
```

```python
[uncomplete(ch) for ch in ('10000000000', '01010000', '10101111')]
```

## Conversion d'une chaîne de huit bits en entier


La fonction int permet facilement de convertir toute chaîne de bits en l'entier représenté en binaire par cette chaîne. Si on l'applique sur une chaîne de longueur huit, on obtient un octet, c'est-à-dire un nombre entier compris entre 0 et 255 (inclus).

```python
int('01010000', 2)
```

## Lecture/écriture d'octets dans un fichier


Nous aurons besoin de lire et écrire des octets dans un fichier. Pour cela le module binary_IO nous sera utile.

```python
import binary_IO
```

### Lecture d'octets

```python
reader = binary_IO.Reader('cigale-UTF-8.txt')
source = reader.get_bytes()
reader.close()
print(source)
```

### Écriture d'octets

```python
writer = binary_IO.Writer('copie_cigale.txt')
writer.write_bytes(source)
writer.close()
```

```bash
diff cigale-UTF-8.txt copie_cigale.txt
```

**Remarque** l'ouverture d'un canal d'écriture vers un fichier existant entraîne une demande de confirmation. Si la confirmation n'est pas donnée, une exception FileExistsError est déclenchée.

```python
writer = binary_IO.Writer('copie_cigale.txt')
writer.close()
```

## Sauvegarde et lecture d'un dictionnaire d'occurrences dans un fichier


Afin de pouvoir décoder un fichier il nous faut connaître le codage utilisé pour l'encodage de ce fichier. Or ce codage est entièrement déterminé par le fichier source original que le plus souvent n'avons pas.

Par conséquent, il va être nécessaire de coder l'information permettant de reconstruire le codage. Cette information est par exemple le dictionnaire d'occurrences des octets du fichier d'origine.

```python
exple_occ = {0: 3, 32 : 10, 65 : 20, 97 : 256}
```

On peut enregistrer ce dictionnaire dans un fichier au format texte.

```python
with open('essai_sauv_dico_txt.occ', 'wt') as writer:
    for symb in exple_occ:
        writer.write('{:d}:{:d}\n'.format(symb, exple_occ[symb]))
```

```python
%cat essai_sauv_dico_txt.occ
```

```python
%ls -l essai_sauv_dico_txt.occ
```

**À faire** Réalisez la fonction suivante.

```python
def save_occurrencies(occ, filename):
    '''
    :param occ: (dict) un dictionnaire d'occurrences d'octets
    :param filename: (str) nom du fichier de sauvegarde du dictionnaire
    :return: (NoneType)
    :side effect: crée un nouveau fichier nommé filename codant le dictionnaire occ
    :CU: droit d'écriture du fichier
    '''
    pass
```

```python
save_occurrencies(exple_occ, 'essai_sauv_dico.occ')
```

```python
%ls -l essai_sauv_dico.occ
```

```python
%cat essai_sauv_dico.occ
```

**À faire** Réalisez la fonction de lecture d'un dictionnaire dans un fichier.

```python
def read_occurrencies(filename):
    '''
    :param filename: (str) nom du fichier contenant un dictionnaire d'occurrences
    :return: (dict) le dictionnaire d'occurrences lu dans le fichier filename
    :CU: droit de lecture du fichier + conformité du format.
    '''
    pass
```

```python
read_occurrencies('essai_sauv_dico.occ')
```

## Encodage d'un fichier


**À faire** Réalisez le fonction d'encodage d'un fichier suivante.

```python
def source_encoding(in_filename, out_filename):
    '''
    :param in_filename: (str) nom du fichier à encoder
    :param out_filename: (str) nom du fichier à produire
    :return: (NoneType)
    :side effect: produit deux nouveaux fichiers nommés
       * out_filename contenant une version optimalement codée de in_filename
       * out_filename.occ contenant une description des nombres d'occurrences des octets du fichier in_filename
    :CU: droit de lecture de in_filename, droit d'écriture de out_filename + out_filename inexistant
    '''
    pass 
```

```python
source_encoding('cigale-UTF-8.txt', 'cigale.huff')
```

```python
%ls -l cigale*
```

**À faire** Traitez d'autres exemples.

```python

```

```python

```

```python

```

```python

```

## Décodage d'un fichier


**À faire** Réalisez la fonction de décodage d'un fichier suivante.

```python
def file_decoding(in_filename, out_filename):
    '''
    :param in_filename: (str) nom du fichier à décoder
    :param out_filename: (str) nom du fichier à produire
    :return: (NoneType)
    :side effect: produit un nouveau fichier nommé
       * out_filename contenant le fichier in_filename décodé selon les nombres d'occurrences
         déterminés par le fichier in_filename.occ 
    :CU: droit de lecture de in_filename, droit d'écriture de out_filename + out_filename inexistant
    '''
    pass
```

```python
file_decoding('cigale.huff', 'cigale.decode')
```

```bash
diff cigale-UTF-8.txt cigale.decode
```

**À faire** Traitez d'autres exemples.

```python

```

```python

```

```python

```

```python

```

```python
source_encoding('origine_especes.txt', 'origine_especes.huff')
```

```python
%ls -l origine_especes.*
```

```python
file_decoding('origine_especes.huff', 'origine_especes.decode')
```

```bash
diff origine_especes.txt origine_especes.decode
```

```python

```
