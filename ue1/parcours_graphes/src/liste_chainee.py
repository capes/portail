from __future__ import annotations

"""
Un module permettant de représenter des listes chainées
"""

RIGHTARROW = chr(8594)
BACKSLASH = "\\"

class Node:
    """
    Une classe permettant de représenter les noeuds
    """

    def __init__(self, content : int, suivant : Node = None ) -> None:
        """
        crée un nouveau noeud pour entier
        :param content: (int) le contenu du noeud
        """
        self._content = content
        self._next = suivant

    def contenu(self) -> int:
        """
        :return: (int) le contenu du noeud
        """
        return self._content

    def suivant(self) -> Node:
        """
        :return: (Node) le noeud suivant
        """
        return self._next

    def set_suivant(self, nouveau_noeud : Node) -> None:
        """
        modifie le suivant du noeud

        :param current: (Node) un noeud
        """
        self._next = nouveau_noeud

class ListeChainee:
    """
    Une classe permettant de représenter des listes  simplement chainées

    Exemples :

    >>> l = ListeChainee.from_native([1, 3, 5])
    >>> for i in l:
    ...    print(i)
    1
    3
    5
    >>> l.ajoute(7)
    >>> l
    1→3→5→7→\\
    >>> l.insere(2, 1)
    >>> l
    1→2→3→5→7→\\
    >>> l.insere(0, 0)
    >>> l
    0→1→2→3→5→7→\\
    >>> l.insere_dans_l_ordre(4, lambda a, b : a-b)
    >>> l
    0→1→2→3→4→5→7→\\
    >>> len(l)
    7
    """

    def __init__(self):
        """
        crée une nouvelle liste vide
        """
        self._premier = None
        self._current = None


    def est_vide(self) -> bool:
        """
        :return: (bool) True ssi la liste est vide
        """
        return self._premier is None

    def longueur(self) -> int:

        """
        :return: (int) la longueur de la liste

        Exemple

        >>> l = ListeChainee()
        >>> for i in range(3):
        ...    l.ajoute(i)
        >>> l.longueur()
        3
        """
        res, current = 0, self._premier
        while current is not None:
            res += 1
            current = current.suivant()
        return res

    def insere(self, element : int, k : int) -> None:
        """
        insère l'élément element à la position k

        :param element: (any) un élément
        :param k: (int) un entier
        """
        assert 0 <= k <= self.longueur()
        current = self._premier
        prec = None
        for _ in range(k):
            prec = current
            current = current.suivant()
        if prec is None:
            self._premier = Node(element, current)
        else:
            prec.set_suivant(Node(element, current))

    def insere_dans_l_ordre(self, element : int, cmp : function ) -> None:
        """
        insère l'élément element à la position k

        :param element: (any) un élément
        :param k: (int) un entier
        :CU: la liste est triée selon cmp
        """
        current = self._premier
        prec = None
        while current is not None and cmp(current.contenu(), element) < 0:
            prec = current
            current = current.suivant()
        if prec is not None:
            prec.set_suivant(Node(element, current))
        else:
            self._premier = Node(element, current)

    def supprime(self, indice : int) -> None:
        """
        supprime l'élément situé à l'indice `indice`

        :param indice: (int) un entier
        """
        assert 0 <= k < self.longueur()
        prec = None
        current = self._premier
        for _ in range(k):
            prec = current
            current = current.suivant()
        if prec is None:
            self._premier = current.suivant()
        else:
            prec.set_suivant(current.suivant())

    def ajoute(self, element) -> None:
        """
        ajoute l'élément element à la fin de la liste
        """
        self.insere(element, self.longueur())

    @staticmethod
    def from_native(liste_python : list) -> ListeChainee:
        """
        :param l: (list) une liste python
        :return: (ListeChainee) une liste chainée contenant tous
                 les éléments de l dans le même ordre
        """
        res = ListeChainee()
        for i in range(len(liste_python)-1, -1, -1):
            res.insere( liste_python[i], 0 )
        return res

    def ieme(self, indice : int) -> int:
        """
        renvoie le contenu du k-ieme noeud
        :param indice: (int) un entier
        :CU: 0 <= indice < self.longueur()
        """
        assert 0 <= indice < self.longueur()
        current = self._premier
        for _ in range(indice):
            current = current.suivant()
        return current.contenu()

    def indice(self, valeur : int) -> int:
        """
        :param valeur: (int) un entier
        :return: (bool) True si valeur appartient à la liste, False sinon

        Exemple :
        >>> ListeChainee().contient(1)
        False
        >>> ListeChainee.from_native( [1, 2, 3] ).contient(2)
        True
        """
        current = self._premier
        i = 0
        while current is not None and current.contenu() != valeur:
            current = current.suivant()
            i += 1
        if current is not None:
            return i
        raise ValueError("l'élément {} n'appartient pas à la liste".format(valeur))

    def contient(self, valeur : int) -> bool:
        """
        :param valeur: (int) un entier
        :return: (bool) True si valeur appartient à la liste, False sinon

        Exemple :
        >>> ListeChainee().contient(1)
        False
        >>> ListeChainee.from_native( [1, 2, 3] ).contient(2)
        True
        """
        current = self._premier
        while current is not None and current.contenu() != valeur:
            current = current.suivant()
        return current is not None

    def __str__(self) -> str:
        """
        :return: (str) une représentation textuelle de la liste
        """
        res = ""
        current = self._premier
        while current is not None:
            res += str(current.contenu())
            res += RIGHTARROW
            current = current.suivant()
        res += BACKSLASH
        return res

    def __iter__(self) -> Iterator:
        """
        :return: (Iterator) un iterateur
        """
        # pas safe si plusieurs iterateur
        # sinon il faut créer une nouvelle classe
        # pour enregistrer l'état courant comme un attribut
        # de chaque itérateur
        self._current = self._premier
        return self

    def __next__(self) -> int:
        """
        :return: (int) l'entier courant, puis passe au suivant
        """
        if self._current is None:
            raise StopIteration
        else:
            res = self._current.contenu()
            self._current = self._current.suivant()
            return res

    __len__ = longueur

    __repr__ = __str__

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = True)
