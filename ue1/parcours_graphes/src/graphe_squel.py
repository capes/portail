from __future__ import annotations
from typing import List, Tuple
import graphviz

"""
Un module permettant de manipuler les graphes

représentation : 
"""

class GrapheOriente:
    """
    Implantation d'une structure de graphe

    Exemples :
    """

    def __init__(self,  n : int ) -> None:
        """
        construit un graphe d'au plus n sommets
        :param n: (int) le nombre maximum de sommets
        """

    def sommets(self) -> list[int]:
        """
        :return: (list of int) la liste triée des sommets du graphe
        """

    def arcs(self) -> List[Tuple[int, int]]:
        """
        :return: (list of couple) une liste des arcs du graphe
        """

    def ajoute_sommet(self, i : int) -> None:
        """
        ajoute un sommet au graphe

        :param i: (int) un sommet
        """
        assert not self.est_sommet(i)

    def retire_sommet(self, i : int) -> None:
        """
        retire un sommet au graphe
        :param i: (int) un sommet
        """
        assert self.est_sommet(i)

    def ajoute_arc(self, i : int, j : int) -> None:
        """
        ajoute l'arc i->j au graphe.
        ajoute les sommets s'ils ne sont pas dans le graphe
        """
        assert not self.est_arc(i, j)

    def est_sommet(self, i : int) -> bool:
        """
        :param i: (int) un sommet
        :return: (bool) True si i est un sommet du graphe, False sinon
        """

    def est_arc(self, i : int, j : int) -> bool :
        """
        ;param i: (int) un entier
        :param j: (int) un entier
        :return: (bool) True ssi l'arc i -> j est un arc du graphe
        """

    def succ(self, sommet : int) -> list[int]:
        """
        :param i: (int) un sommet
        :return: (list of int) la liste des successeurs de i
        """
        assert self.est_sommet(sommet)

    def degre_entrant(self, i : int) -> int:
        """
        :param i: (int) un sommet
        :return: (int) le degré entrant de i
        """
        assert self.est_sommet(i)

    def degre_sortant(self, i : int) -> int:
        """
        :param i: (int) un sommet
        :return: (int) le degré sortant de i
        """
        assert self.est_sommet(i)

    def to_dot(self) -> str:
        """
        :return: (str) a dot representation of this graph
        """
        res = "digraph {\n"
        for sommet in self.sommets():
            res += f"   {sommet}\n"
        for deb, fin in self.arcs():
            res += f"   {deb} -> {fin}\n"
        res += "}"
        return res

    def view(self, filename = 'graphe') -> None:
        """
        view this graph
        """
        graphviz.Source(self.to_dot(), format='png').view(filename=filename)
