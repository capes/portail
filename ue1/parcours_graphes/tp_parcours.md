**Objectifs :** Mesurer expérimentalement les complexités des parcours
 de graphe.


# Implantation des graphes

**Matériel fourni**

-   Le module [liste<sub>chainee.py</sub>](src/liste_chainee.py) contient l'implantation d'une liste
    chainée en python ;
-   Le module [graphe<sub>squel.py</sub>](src/graphe_squel.py) contient le squelette d'une implantation
    des graphes, conforme au type abstrait spécifié dans le cours d'ue
    a1.

Le travail sera réalisé dans un module `tp_parcours.py`.

1.  Consulter le module `liste_chainee.py`. En particulier, observer
    les méthodes spéciales rendant itérable la classe.
2.  Consulter le module `graphe_squel.py`.
3.  Renommer le fichier `graphe_squel.py` en `graphe_matrice.py` puis
    compléter ce fichier pour implanter la structure demandée en
    utilisant une matrice.
4.  Renommer le fichier `graphe_squel.py` en `graphe_liste.py` puis
    compléter ce fichier pour implanter la structure demandée en
    utilisant des listes d'adjacence.


# Graphes sur demande

1.  Réaliser une fonction `poussière` de signature
    `def poussiere(ordre : int) -> GrapheOriente`
    Qui crée un graphe d'ordre donné dont tous les sommets sont isolés.
2.  Réaliser une fonction `complet` de signature
    `def complet(ordre : int) -> GrapheOriente`
    Qui crée un graphe complet d'ordre donné.
3.  Réaliser une fonction `arbre_aleatoire` de signature  
     `def arbre_aleatoire(ordre : int) -> GrapheOriente`  
     Qui crée un arbre aléatoire d'ordre donné (On pourra discuter
    d'un algorithme avec votre enseignant)
4.  Réaliser une fonction `graphe_connexe_aleatoire` de signature  
    `def graphe_connexe_aleatoire(ordre : int) -> GrapheOriente`  
    Qui crée un graphe connexe aléatoire d'ordre donné.

Vous testerez vos fonctions à la fois en utilisant la représentation
matricielle :

    from graphe_matrice import GrapheOriente

et la représentation en liste d'adjacence :

    from graphe_liste import GrapheOriente


# Implanter les parcours

1.  La procédure `parcours` va nous servir de procédure principale :
    
        def parcours(graphe : GrapheOriente,
                     procedure_parcours : 'function',
                     traite : 'function'  ) -> None:
            """
            procédure principale de parcours 
            parcours un graphe en utilisant un type de parcours
        
            :param graphe: (GrapheOriente) un graphe
            :param procedure_parcours: (function) une procédure de parcours 
                                       à appeler sur les sommets du graphe
            :param traite: (function) une fonction de traitement des sommets
            """
            visites = [ False for _ in graphe.sommets() ]
            for sommet in graphe.sommets():
                if not visites[sommet]:
                    procedure_parcours(graphe, sommet, visites, traite)
    
    La fonction de traitement `traite` sera appelée sur chaque sommet
    visité, sa signature est
    
        def traite(sommet : int) -> None:
    
    Recopier le code de la procédure `parcours` dans votre module,
    puis réaliser une fonction de traitement qui affiche le sommet
    visité.

2.  Réaliser une procédure **récursive** `parcours_profondeur_rec` de
    signature 
    
        def parcours_profondeur_rec(graphe : GrapheOriente,
                                    sommet : int,
                                    visites : list[bool],
                                    traite : 'function') -> None:
    
    Qui réalise un parcours du graphe en profondeur d'abord en
    partant du sommet \`sommet\`.
    
    La fonction de traitement peut être appelée avant de parcourir
    les successeurs, ou après les avoir parcourus.

3.  Observez l'exécution de l'algorithme de parcours en profondeur
    sur quelques exemples, et en variant les cas d'exécution (On peut
    utiliser comme fonction de traitement une fonction qui imprime le
    sommet).
    
        >>> graphe = graphe_connexe_aleatoire(20, 40)
        >>> parcours(graphe, parcours_profondeur_rec, imprime_sommet)
        0
        1
        8
        6
        11
        12
        9
        4
        15
        5
        18
        14
        17
        7
        2
        3
        10
        19
        16
        13

1.  Réaliser une procédure itérative `parcours_profondeur_ite` de
    signature
    
        def parcours_profondeur_ite(graphe : Graphe, sommet : int,
                                    visites : list[bool],
                                    traite : 'function') -> None
    
    Qui réalise un parcours du graphe en profondeur d'abord.
2.  Réaliser une procédure itérative `parcours_largeur` de signature 
    
        def parcours_largeur(graphe : Graphe,
                             sommet : int,
                             visites : list[bool],
                             traite : 'function') -> None
    
    Qui réalise un parcours du graphe en largeur d'abord.
3.  Observez l'exécution de l'algorithme de parcours en largeur
    d'abord sur quelques exemples, (On peut utiliser comme fonction
    de traitement une fonction qui imprime le sommet).


# Mesurer les temps de parcours

On souhaite comparer les temps de parcours d'un même graphe, lorsque
celui-ci est représenté sous forme d'une matrice et lorsque
celui-ci est représenté par une liste d'adjacence.


## Passer d'une représentation à l'autre

Pour ce faire, la fonction suivante renvoie une copie d'un graphe
utilisant des listes d'adjacence.

    import graphe_listes
    import graphe_matrice
    
    def en_liste(graphe : GrapheOriente) -> GrapheOriente:
        """
        :param graphe: (GrapheOriente) un graphe
        :return; (GrapheOriente) un graphe représenté par des listes d'adjacence
        """
        sommets = graphe.sommets()
        res = graphe_listes.GrapheOriente( len(sommets) )
        for sommet in sommets:
            res.ajoute_sommet(sommet)
        for deb, fin in graphe.arcs():
            res.ajoute_arc(deb, fin)
        return res


## Générer des listes de graphes

Pour chaque taille de graphe \(n\) comprise entre 2 et
N :

1.  choisir un nombre d'arêtes p compris entre \(n-1\) et \(n^2\) ;
2.  générer un graphe aléatoire à \(n\) sommets et \(p\) arêtes en
    utilisant une représentation matricielle ;
3.  ajouter la référence à ce graphe dans la liste `GRAPHES_MATRICES` ;
4.  créer le même graphe dans une représentation en liste d'adjacence ;
5.  ajouter la référence à ce graphe dans la liste `GRAPHES_LISTES`.

    GRAPHES_MATRICES = []
    GRAPHES_LISTES = []
    N = 20
    
    for ordre in range(2, N):
        nb_aretes = choice(range(ordre - 1, ordre ** 2))
        graphe = graphe_connexe_aleatoire(ordre, nb_aretes)
        GRAPHES_MATRICES.append( graphe )
        try:
            GRAPHES_LISTES.append( en_liste(graphe) )
        except:
            graphe.view()
            break

Puis, à l'aide du module `timeit` effectuer pour chaque graphe :

-   une mesure du temps de parcours en profondeur en utilisant un
    graphe représenté sous forme de matrice ;
-   une mesure du temps de parcours en profondeur en utilisant un
    graphe représenté par des listes d'adjacence ;
-   une mesure du temps de parcours en largeur en utilisant un
    graphe représenté sous forme de matrice ;
-   une mesure du temps de parcours en largeur en utilisant un
    graphe représenté par des listes d'adjacence ;

