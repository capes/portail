# Exercices javascript 

## Diaporama

Réalisez en utilisant les langages HTML, CSS et javascript un document web qui permet l'affichage d'un diaporama. 
Ce diaporama permet l'affichage successif de différentes images dont les sources sont contenues dans une liste (dont la valeur poura être définie statiquement) ;

Les fonctionnalités de ce diaporama doivent être les suivantes&nbsp;:

 - les images sont affichées une à la fois ;
 - le numéro de l'image actuellement affichée est visible dans un	 champ de saisie. Il est possible de saisir manuellement dans ce champ le numéro d'une image pour afficher celle-ci ;
 - deux boutons permettent respectivement de passer à l'image suivante et l'image précédente du diaporama. On considère que la première image est la suivante de la dernière image qui elle-même est la précédente de la première ;
 - un bouton "Start" permet de démarrer la lecture automatique du diaporama avec affichage de l'image suivante toutes les *n* secondes.
	La valeur de *n* est indiquée dans un champ de saisie. Il est possible de modifier la valeur de ce champ. Dans ce cas, si la lecture automatique était en cours, la durée d'enchainement des diapositives est aussitôt adaptée.
 - une fois la lecture automatique démarrée, le texte du bouton de démarrage est remplacé par "Stop". Son comportement est modifié : ce bouton permet alors de mettre fin à la lecture automatique. Cela aura pour effet de remettre le bouton dans son état initial.
 

### Démo
 une [démonstration possible](./demo-diaporama.zip) des fonctionnalités à obtenir

## Saisie de code

Réalisez en utilisant les langages HTML, CSS et javascript un document web qui permet la saisie d'un code de 4 chiffres.
Le comportement désiré est le suivant :

Chaque chiffre est saisi par en cliquant sur le bouton qui lui correspond. Dans la page l'ordre d'affichage des boutons représentant les chiffres est modifié aléatoirement à chaque chargement de la page.
La zone d'affichage du code saisi contient  initialement la chaîne <code>****</code> dont les <code>\*</code> sont remplacés au fur et à  mesure de la saisie du code.
Lorsque 4 chiffres ont été saisis, les boutons avec les chiffres sont désactivés.



### Démo
 une [démonstration possible](./demo-saisie-code.zip) des fonctionnalités à obtenir

 
