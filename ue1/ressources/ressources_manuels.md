Manuels en ligne fournis par éditeurs
=====================================

SNT
---

* [BORDAS 3.0](https://fr.calameo.com/read/004956979deb4ae43d10b)

* [DELAGRAVE](https://www.libmanuels.fr/demo/9782206103563/0?title=Sciences%20num)

* [DIDIER](https://fr.calameo.com/read/005419417c4e2c49ef113)

* [HACHETTE](https://fr.calameo.com/read/0048229533224e0294d2d)

* [NUMWORKS](https://www.numworks.com/fr/ressources/snt/book.pdf)

* [Nathan SNT](https://biblio.nathan.fr/specimen/9782091729138/?openBook=9782091729138%3fdXNlck5hbWU9cGQ1ZHZhT1c2VnR1cTQyWkNBL1hqUT09JnVzZXJQYXNzd29yZD1XalB3YkZzdmZ2RmNDSHNXUmgyemt3PT0mZGVtbz10cnVlJndhdGVybWFyaz0=)


NSI
---

* [BORDAS 3.0 Cahier](https://fr.calameo.com/read/004956979cca2a747ef28)


Manuels d'intérets disponibles à l'oral
=======================================

* NSI 1ère et Terminale (Ellipses) : Balabonski, Conchon, Filliâtre, Nguyen
* NSI Prépabac 1ère et Terminale (Hatier) : Connan, Signac, Rosavolgyi, Adobet, Petrov

Des exemplaires sont empruntables aux bibliothèques de l'Université de l'INSPE.