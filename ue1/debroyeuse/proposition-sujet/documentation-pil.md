Utilisation du module PIL
=========================

Nous manipulerons les images avec le module python `PIL`. 

Nous allons utiliser uniquement les primitives de traitement d'image en chargeant le module `Image` :

```{python}
from PIL import Image
```

voici un exemple commenté de programme PIL utilisant les primitives dont vous aurez besoin dans ce projet :

```{python}
from PIL import Image

RED = (255, 0, 0)

def luminosite(pixel):
    """
    Renvoie la luminosité d'un pixel.
    
    CU : les pixels sont codés par un triplet d'entiers compris entre 
         0 et 255
    """
    return sum(pixel) / (3 * 255)
    
########################################################################
# charge le fichier "shred-complainte-0.png" dans la variable img_left
img_left = Image.open("images/shred-complainte-0.png")
# idem pour shred-complainte-8.png
img_right = Image.open("images/shred-complainte-8.png")

# convertit les deux images en RGB

img_left = img_left.convert("RGB")
img_right = img_right.convert("RGB")

####################
# affiche les images
img_left.show()
img_right.show()

################################
# colle les images verticalement

# on extrait les tailles
xsizel, ysizel = img_left.size
xsizer, ysizer = img_right.size 

# on crée une image pour recevoir le résultat 
imjoin = Image.new("RGB", (xsizel+xsizer, ysizel) )

# on recopie les images vers la destination
imjoin.paste(img_left, (0,0) )
imjoin.paste(img_right, (xsizel,0) )

# on affiche les images recollées
imjoin.show()


##########################################################################
# fait une copie de imjoin dans imjoin2 (permet de ne pas modifier imjoin)
imjoin2 = imjoin.copy()

#####################################
# transforme en rouge tous les pixels dont la luminosité est inférieure à 0.8
# récupère la taille de l'image
xsize2,ysize2 = imjoin2.size

# parcourt et traite les pixels
for x in range(xsize2):
    for y in range(ysize2):
        # récupère la couleur du pixel (x,y)
        couleur = imjoin2.getpixel( (x,y) )
	    # La couleur d'un pixel est donnée par un triplet d'entiers entre 0 et 255.
        if luminosite(couleur) <= 0.8:
            # on modifie la couleur du pixel en rouge
            imjoin2.putpixel( (x,y), RED )

#############################
# affiche l'image transformée
imjoin2.show()

################################
# sauvegarde l'image transformée
imjoin2.save("joined-slices-red.png")

##############################
# on découpe l'image avec crop
# Attention : crop fonctionne un peu comme l'extraction de sous-listes,
# les coordonnées du deuxième coin sont exclues, ce qui
# signifie que l'image ci-dessous a une taille 100x100 et
# non pas 101x101 comme on pourrait s'y attendre

subim = imjoin2.crop( (0,0,100,100) );
subim.show()
```
