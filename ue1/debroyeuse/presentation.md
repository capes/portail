Introduction
============

Le but de ce projet est de reconstituer un document passé à la broyeuse. 

Une broyeuse est un appareil qui permet de découper verticalement une (ou plusieurs) feuille de
papier en fines bandelettes, dans le but de faire disparaître l'information contenue sur la
feuille. 

Toutefois, avec beaucoup de patience, un humain peut reconstituer le document en repositionnant les
bandelettes pour qu'elles correspondent bien entre elles.

L'Allemagne a d'ailleurs lancé en 2000 un programme à grande échelle pour accélerer la
reconstitution de très nombreux documents broyés par la Stasi ([Article
RFI](http://www.rfi.fr/europe/20120114-ordinateur-archives-anciens-stasi-est-allemande-allemagne)).

Votre mission, si vous l'acceptez, sera d'écrire un programme permettant de reconstituer une page
ayant été découpée en bandelettes.

Nous ferons quand même des hypothèses simplificatrices :

-   les bandelettes sont déjà **verticales** et **orientées** dans le bon sens ;
-   les bandelettes proviennent d'**une seule feuille**.

Voici 10 bandelettes (numérotées de 0 à 9)
------------------------------------------

Ces bandelettes ont été mélangées. Vous trouverez les fichiers correspondant dans le répertoire [`images`](./images/)

<table class="bande">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>0</p></th>
<th class="head"><p>1</p></th>
<th class="head"><p>2</p></th>
<th class="head"><p>3</p></th>
<th class="head"><p>4</p></th>
<th class="head"><p>5</p></th>
<th class="head"><p>6</p></th>
<th class="head"><p>7</p></th>
<th class="head"><p>8</p></th>
<th class="head"><p>9</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><a class="reference internal" href="./images/shred-complainte-0.png"><img alt="i0" class="align-bottom" src="./images/shred-complainte-0.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-1.png"><img alt="i1" class="align-bottom" src="./images/shred-complainte-1.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-2.png"><img alt="i2" class="align-bottom" src="./images/shred-complainte-2.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-3.png"><img alt="i3" class="align-bottom" src="./images/shred-complainte-3.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-4.png"><img alt="i4" class="align-bottom" src="./images/shred-complainte-4.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-5.png"><img alt="i5" class="align-bottom" src="./images/shred-complainte-5.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-6.png"><img alt="i6" class="align-bottom" src="./images/shred-complainte-6.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-7.png"><img alt="i7" class="align-bottom" src="./images/shred-complainte-7.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-8.png"><img alt="i8" class="align-bottom" src="./images/shred-complainte-8.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
<td><p><a class="reference internal" href="./images/shred-complainte-9.png"><img alt="i9" class="align-bottom" src="./images/shred-complainte-9.png" style="width: 45.0px; height: 344.0px;" /></a></p></td>
</tr>
</tbody>
</table>

Reconstitution
--------------

Ces bandelettes peuvent être permutées. En utilisant la permutation `[9,5,7,2,0,8,1,4,6,3]`, on obtient :

![image](images/complainte.png){.width="300px"}

Objectif
--------

Réaliser un script Python permettant de reconstituer l'image d'origine.

voir une [proposition de sujet](./proposition-sujet/sujet.md)
