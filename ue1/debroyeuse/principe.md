Principe général
================

Le principe repose sur une opération clé : savoir si deux bandelettes données coïncident ou pas. Si
on sait détecter cela, on peut rassembler une par une les bandelettes et reconstituer la feuille.

Afin de tester si deux bandelettes coïncident, on définit sur les bandelettes une notion de
score.


Calcul des scores entre les bandelettes
---------------------------------------

Comment calculer un score entre deux bandelettes nommées `g` (comme gauche) et `d` (comme droite) ?

Plus le score sera elevé, meilleure sera la correspondance entre les deux bandelettes.

Plusieurs méthodes sont possibles :

- en comparant les pixels situés sur la bordure droite de `g` avec ceux situés sur la bordure gauche
  de `d`.
- en collant les deux bandes et en identifiant les motifs ainsi créés : 
    - on construit une base contenant les caractéristiques des motifs (lettres) situés à l'intérieur
      des bandes ;
    - on compte le nombre de motifs créés lorsque l'on colle `g` à `d` et qui "ressemblent" à des
      motifs de la base.

La première méthode est la plus simple et donne de bons résultats (à condition de s'y prendre
correctement !)

On place la bandelette `g` à gauche de la bandelette `d` de telle sorte que
les deux bords se touchent.

* si un pixel noir du bord droit de `g` touche un pixel noir du bord
gauche de `d`, alors le score doit croître ;
* si un pixel blanc du bord droit de `g` touche un pixel noir du bord
  gauche de `d` ou si un pixel noir du bord droit de `g` touche un pixel
  blanc du bord gauche de `d`, alors le score doit décroître ;
* si un pixel blanc du bord droit de `g` touche un pixel blanc du bord
gauche de `d`, alors le score n'est pas modifié.  
  (Cela se justifie par le fait que les bandelettes proviennent de feuilles blanches sur lesquelles des caractères (noirs) sont imprimés. La majorité des pixels sont donc blancs et leur alignement n'est pas significatif).

### → à faire

Proposer des méthodes de calcul des scores.

Table des scores
----------------

Maintenant que l'on sait calculer un score entre deux bandelettes, on peut calculer les scores de tous les couples de bandelettes différents.  
Il faut bien remarquer aussi que le score entre deux bandelettes `b1` et `b2`, est différent de celui entre `b2` et `b1`.

Supposons que nous disposions de 10 bandelettes représentées par une liste `bandelettes`. 
Si on fait varier l'indice de ligne `i` entre 0 et 9, et l'indice de colonne `j` entre 0 et 9, on peut représenter l'ensemble des scores entre `bandelettes[i]` et `bandelettes[j]` par une table des scores : 

|   | 0    | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    |
|---|------|------|------|------|------|------|------|------|------|------|
| 0 | 0.00 | 0.12 | 0.50 | 0.05 | 0.28 | 0.12 | 0.03 | 0.38 | 0.74 | 0.00 |
| 1 | 0.11 | 0.00 | 0.35 | 0.03 | 2.67 | 0.12 | 0.04 | 0.07 | 0.33 | 0.00 |
| 2 | 0.89 | 0.27 | 0.00 | 0.03 | 0.18 | 0.17 | 0.03 | 0.24 | 0.08 | 0.00 |
| 3 | 0.00 | 0.00 | 0.00 | 0.00 | 0.00 | 0.00 | 0.00 | 0.00 | 0.00 | 0.00 |
| 4 | 0.26 | 0.09 | 0.09 | 0.12 | 0.00 | 0.22 | 3.56 | 0.04 | 0.08 | 0.00 |
| 5 | 0.15 | 0.11 | 0.22 | 0.03 | 0.05 | 0.00 | 0.01 | 1.86 | 0.05 | 0.00 |
| 6 | 0.03 | 0.00 | 0.04 | 0.33 | 0.00 | 0.04 | 0.00 | 0.02 | 0.04 | 0.00 |
| 7 | 0.02 | 0.14 | 0.88 | 0.00 | 0.27 | 0.08 | 0.04 | 0.00 | 0.31 | 0.00 |
| 8 | 0.34 | 2.23 | 0.14 | 0.00 | 0.12 | 0.16 | 0.07 | 0.56 | 0.00 | 0.00 |
| 9 | 0.85 | 0.23 | 0.01 | 0.07 | 0.09 | 1.16 | 0.16 | 0.02 | 0.01 | 0.00 |
 
Reconstitution
--------------

Cette table des scores permet de retrouver l'ordre des bandelettes par la méthode suivante :

- on choisit un numéro `n0` de bandelette candidate pour être la bandelette de gauche : il s'agit de
  la bandelette ayant la colonne de zéros (ou une colonne ayant des scores les plus faibles
  possibles);
- on cherche l'indice du maximum des scores sur la ligne `n0` : cela fournit `n1`
- on cherche l'indice du maximum des scores sur la ligne `n1` : cela fournit `n2`
- ...
- on répète l'opération précédente jusqu'à épuiser l'ensemble des bandelettes

### exemple

On choisit `n0`=9

- Le maximum de la ligne 9 se trouve à l'indice `n1`=5.
- Le maximum de la ligne 5 se trouve à l'indice `n2`=7.
- Le maximum de la ligne 7 se trouve à l'indice `n3`=2.
- Le maximum de la ligne 2 se trouve à l'indice `n4`=0.
- Le maximum de la ligne 0 se trouve à l'indice `n5`=8.
- Le maximum de la ligne 8 se trouve à l'indice `n6`=1.
- Le maximum de la ligne 1 se trouve à l'indice `n7`=4.
- Le maximum de la ligne 4 se trouve à l'indice `n8`=6.
- Le maximum de la ligne 6 se trouve à l'indice `n9`=3.
   
On a parcouru toutes les bandelettes, et on a trouvé une solution potentielle :
`[9,5,7,2,0,8,1,4,6,3]`.
   
### remarques

Cet algorithme est un algorithme glouton qui fonctionne bien avec les images de textes. Dans certains cas, il peut conduire à une solution erronée.

- quels jeux de données amènent une telle solution. Sont-ils probables dans le contexte qui nous
 intéresse ?
- quelles modifications à envisager pour prendre en compte de telles situations ?
