from math import log

from exemples_codages import *

def est_codage_binaire_prefixe(codage):
    '''
    :param codage: (dict) un codage alphabétique binaire
    :return: (bool)
      - True si codage est préfixe
      - False sinon
    :CU: codage est un codage binaire
    
    >>> candidats = (codage1_1, codage1_2, codage1_3, codage1_4, codage2_1, codage2_2, codage2_3)
    >>> all(est_codage_binaire_prefixe(codage) for codage in candidats)
    True
    '''
    def est_langage_prefixe(mots):
        if len(mots) <= 1:
            return True
        else:
            mots0 = [mot[1:] for mot in mots if mot.startswith('0')]
            mots1 = [mot[1:] for mot in mots if mot.startswith('1')]
            if '' in mots0 and len(mots0) >= 2:
                return False
            elif '' in mots1 and len(mots1) >= 2:
                return False
            else:
                return est_langage_prefixe(mots0) and est_langage_prefixe(mots1)
        
    return est_langage_prefixe(list(codage.values()))

def longueur_moyenne(source, codage):
    '''
    :param source: (dict) une source
    :param codage: (dict) un codage binaire
    :return: (float) longueur moyenne des mots du codage
    :CU: source et codage doivent avoir les mêmes clés
    '''
    return sum(source[symb]*len(codage[symb]) for symb in source)


def huffman(source):
    '''
    :param source: (dict) une source d'information
    :return: (dict) un codage optimal de cette source

    pas d'arbre explicitement géré dans cette version
    à refaire ensuite avec des arbres ?
    '''
    foret = [({symb: ''}, source[symb]) for symb in source]
    foret.sort(key=lambda x: x[1], reverse=True)
    while len(foret) > 1:
        d1, p1 = foret.pop()
        d2, p2 = foret.pop()
        for symb in d1: d1[symb] = '0' + d1[symb]
        for symb in d2: d2[symb] = '1' + d2[symb]
        d1.update(d2)
        foret.append((d1, p1 + p2))
        foret.sort(key=lambda x: x[1], reverse=True)
    return foret[0][0]

def quantite_information(symb, source):
    '''
    :param symb: (any hashable type) un symbole
    :param source: (dict) une source
    :return: (float) quantité d'information contenue dans le symbole symb de la source
    :CU: symb est un symbole de la source
    '''
    return -log(source[symb], 2)

def entropie(source):
    '''
    :param source: (dict) une source d'information
    :return: (float) l'entropie de la source
    '''
    return sum(quantite_information(symb, source)*source[symb] for symb in source)


if __name__ == '__main__':
    import doctest
    doctest.testmod()

    source = source3
    
    codage_optimal = huffman(source)

    print('Codage trouvé :')
    for symb in sorted(source.keys()):
        print('\t{:s} : {:6.5f} : {:4.2} : {:s}'.format(symb, source[symb],
                                                        quantite_information(symb, source),
                                                        codage_optimal[symb]))
    print()
    print('Longueur moyenne du codage : {:f}'.format(longueur_moyenne(source, codage_optimal)))
    print('Entropie de la source : {:f}'.format(entropie(source)))
