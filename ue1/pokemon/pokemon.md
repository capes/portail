
# Table des matières

1.  [Matériel fourni](#orgd5e45e2)
    1.  [Fichiers csv](#orga0c085a)
    2.  [Le module `pokemon.py`](#orge73ebac)
    3.  [Le module `knn_pokemon`](#org5236539)
2.  [Travail à réaliser](#org26f65aa)
3.  [Pour aller plus loin](#org17f2986)

Les pokemons sont des animaux imaginaires inventés par Nintendo. Ils possèdent tous
des caractéristiques différentes :

-   Un nom (`name`)
-   Un nombre de points d'attaque (`attack`)
-   Un nombre pas pour le faire éclore (`base_egg_steps`)
-   Un type principal (`type1`)
-   Un éventuel type secondaire (`type2`)
-   Un nombre de points de vie (`hp`)
-   Un nombre de points de défense (`defense`)
-   Un nombre de points d'attaque spéciale (`sp_attack` )
-   Un nombre de points de défence spéciale (`sp_defense` )
-   Un nombre caractérisant sa vitesse (`speed`)
-   Un entier indiquant la génération du pokemon (`generation`)
-   Un booléen indiquant si le pokemon est légendaire ou non (`is_legendary`)

Ces données proviennent du jeu Pokemon Go. Les pokemons sont consignés dans des fichiers `csv`. 
Ces fichiers contiennent les données sous forme de table :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<tbody>
<tr>
<td class="org-left">"name"</td>
<td class="org-right">"attack"</td>
<td class="org-right">"base<sub>egg</sub><sub>steps</sub>"</td>
<td class="org-right">"base<sub>happiness</sub>"</td>
<td class="org-right">"capture<sub>rate</sub>"</td>
<td class="org-left">"classfication"</td>
<td class="org-right">"defense"</td>
<td class="org-right">"experience<sub>growth</sub>"</td>
<td class="org-left">"height<sub>m</sub>"</td>
<td class="org-right">"hp"</td>
<td class="org-right">"pokedex<sub>number</sub>"</td>
<td class="org-right">"sp<sub>attack</sub>"</td>
<td class="org-right">"sp<sub>defense</sub>"</td>
<td class="org-right">"speed"</td>
<td class="org-left">"type1"</td>
<td class="org-left">"type2"</td>
<td class="org-left">"weight<sub>kg</sub>"</td>
<td class="org-right">"generation"</td>
<td class="org-right">"is<sub>legendary</sub>"</td>
</tr>


<tr>
<td class="org-left">"Magmar"</td>
<td class="org-right">95</td>
<td class="org-right">6400</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Spitfire Pokémon"</td>
<td class="org-right">57</td>
<td class="org-right">1000000</td>
<td class="org-left">"1.3"</td>
<td class="org-right">65</td>
<td class="org-right">126</td>
<td class="org-right">100</td>
<td class="org-right">85</td>
<td class="org-right">93</td>
<td class="org-left">"fire"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"44.5"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Pinsir"</td>
<td class="org-right">155</td>
<td class="org-right">6400</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Stagbeetle Pokémon"</td>
<td class="org-right">120</td>
<td class="org-right">1250000</td>
<td class="org-left">"1.5"</td>
<td class="org-right">65</td>
<td class="org-right">127</td>
<td class="org-right">65</td>
<td class="org-right">90</td>
<td class="org-right">105</td>
<td class="org-left">"bug"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"55.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Tauros"</td>
<td class="org-right">100</td>
<td class="org-right">5120</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Wild Bull Pokémon"</td>
<td class="org-right">95</td>
<td class="org-right">1250000</td>
<td class="org-left">"1.4"</td>
<td class="org-right">75</td>
<td class="org-right">128</td>
<td class="org-right">40</td>
<td class="org-right">70</td>
<td class="org-right">110</td>
<td class="org-left">"normal"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"88.4"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Magikarp"</td>
<td class="org-right">10</td>
<td class="org-right">1280</td>
<td class="org-right">70</td>
<td class="org-right">255</td>
<td class="org-left">"Fish Pokémon"</td>
<td class="org-right">55</td>
<td class="org-right">1250000</td>
<td class="org-left">"0.9"</td>
<td class="org-right">20</td>
<td class="org-right">129</td>
<td class="org-right">15</td>
<td class="org-right">20</td>
<td class="org-right">80</td>
<td class="org-left">"water"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"10.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Gyarados"</td>
<td class="org-right">155</td>
<td class="org-right">1280</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Atrocious Pokémon"</td>
<td class="org-right">109</td>
<td class="org-right">1250000</td>
<td class="org-left">"6.5"</td>
<td class="org-right">95</td>
<td class="org-right">130</td>
<td class="org-right">70</td>
<td class="org-right">130</td>
<td class="org-right">81</td>
<td class="org-left">"water"</td>
<td class="org-left">"flying"</td>
<td class="org-left">"235.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Lapras"</td>
<td class="org-right">85</td>
<td class="org-right">10240</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Transport Pokémon"</td>
<td class="org-right">80</td>
<td class="org-right">1250000</td>
<td class="org-left">"2.5"</td>
<td class="org-right">130</td>
<td class="org-right">131</td>
<td class="org-right">85</td>
<td class="org-right">95</td>
<td class="org-right">60</td>
<td class="org-left">"water"</td>
<td class="org-left">"ice"</td>
<td class="org-left">"220.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Ditto"</td>
<td class="org-right">48</td>
<td class="org-right">5120</td>
<td class="org-right">70</td>
<td class="org-right">35</td>
<td class="org-left">"Transform Pokémon"</td>
<td class="org-right">48</td>
<td class="org-right">1000000</td>
<td class="org-left">"0.3"</td>
<td class="org-right">48</td>
<td class="org-right">132</td>
<td class="org-right">48</td>
<td class="org-right">48</td>
<td class="org-right">48</td>
<td class="org-left">"normal"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"4.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Eevee"</td>
<td class="org-right">55</td>
<td class="org-right">8960</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Evolution Pokémon"</td>
<td class="org-right">50</td>
<td class="org-right">1000000</td>
<td class="org-left">"0.3"</td>
<td class="org-right">55</td>
<td class="org-right">133</td>
<td class="org-right">45</td>
<td class="org-right">65</td>
<td class="org-right">55</td>
<td class="org-left">"normal"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"6.5"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Vaporeon"</td>
<td class="org-right">65</td>
<td class="org-right">8960</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Bubble Jet Pokémon"</td>
<td class="org-right">60</td>
<td class="org-right">1000000</td>
<td class="org-left">"1.0"</td>
<td class="org-right">130</td>
<td class="org-right">134</td>
<td class="org-right">110</td>
<td class="org-right">95</td>
<td class="org-right">65</td>
<td class="org-left">"water"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"29.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Jolteon"</td>
<td class="org-right">65</td>
<td class="org-right">8960</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Lightning Pokémon"</td>
<td class="org-right">60</td>
<td class="org-right">1000000</td>
<td class="org-left">"0.8"</td>
<td class="org-right">65</td>
<td class="org-right">135</td>
<td class="org-right">110</td>
<td class="org-right">95</td>
<td class="org-right">130</td>
<td class="org-left">"electric"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"24.5"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Flareon"</td>
<td class="org-right">130</td>
<td class="org-right">8960</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Flame Pokémon"</td>
<td class="org-right">60</td>
<td class="org-right">1000000</td>
<td class="org-left">"0.9"</td>
<td class="org-right">65</td>
<td class="org-right">136</td>
<td class="org-right">95</td>
<td class="org-right">110</td>
<td class="org-right">65</td>
<td class="org-left">"fire"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"25.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Porygon"</td>
<td class="org-right">60</td>
<td class="org-right">5120</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Virtual Pokémon"</td>
<td class="org-right">70</td>
<td class="org-right">1000000</td>
<td class="org-left">"0.8"</td>
<td class="org-right">65</td>
<td class="org-right">137</td>
<td class="org-right">85</td>
<td class="org-right">75</td>
<td class="org-right">40</td>
<td class="org-left">"normal"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"36.5"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Omanyte"</td>
<td class="org-right">40</td>
<td class="org-right">7680</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Spiral Pokémon"</td>
<td class="org-right">100</td>
<td class="org-right">1000000</td>
<td class="org-left">"0.4"</td>
<td class="org-right">35</td>
<td class="org-right">138</td>
<td class="org-right">90</td>
<td class="org-right">55</td>
<td class="org-right">35</td>
<td class="org-left">"rock"</td>
<td class="org-left">"water"</td>
<td class="org-left">"7.5"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Omastar"</td>
<td class="org-right">60</td>
<td class="org-right">7680</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Spiral Pokémon"</td>
<td class="org-right">125</td>
<td class="org-right">1000000</td>
<td class="org-left">"1.0"</td>
<td class="org-right">70</td>
<td class="org-right">139</td>
<td class="org-right">115</td>
<td class="org-right">70</td>
<td class="org-right">55</td>
<td class="org-left">"rock"</td>
<td class="org-left">"water"</td>
<td class="org-left">"35.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Kabuto"</td>
<td class="org-right">80</td>
<td class="org-right">7680</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Shellfish Pokémon"</td>
<td class="org-right">90</td>
<td class="org-right">1000000</td>
<td class="org-left">"0.5"</td>
<td class="org-right">30</td>
<td class="org-right">140</td>
<td class="org-right">55</td>
<td class="org-right">45</td>
<td class="org-right">55</td>
<td class="org-left">"rock"</td>
<td class="org-left">"water"</td>
<td class="org-left">"11.5"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Kabutops"</td>
<td class="org-right">115</td>
<td class="org-right">7680</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Shellfish Pokémon"</td>
<td class="org-right">105</td>
<td class="org-right">1000000</td>
<td class="org-left">"1.3"</td>
<td class="org-right">60</td>
<td class="org-right">141</td>
<td class="org-right">65</td>
<td class="org-right">70</td>
<td class="org-right">80</td>
<td class="org-left">"rock"</td>
<td class="org-left">"water"</td>
<td class="org-left">"40.5"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Aerodactyl"</td>
<td class="org-right">135</td>
<td class="org-right">8960</td>
<td class="org-right">70</td>
<td class="org-right">45</td>
<td class="org-left">"Fossil Pokémon"</td>
<td class="org-right">85</td>
<td class="org-right">1250000</td>
<td class="org-left">"1.8"</td>
<td class="org-right">80</td>
<td class="org-right">142</td>
<td class="org-right">70</td>
<td class="org-right">95</td>
<td class="org-right">150</td>
<td class="org-left">"rock"</td>
<td class="org-left">"flying"</td>
<td class="org-left">"59.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Snorlax"</td>
<td class="org-right">110</td>
<td class="org-right">10240</td>
<td class="org-right">70</td>
<td class="org-right">25</td>
<td class="org-left">"Sleeping Pokémon"</td>
<td class="org-right">65</td>
<td class="org-right">1250000</td>
<td class="org-left">"2.1"</td>
<td class="org-right">160</td>
<td class="org-right">143</td>
<td class="org-right">65</td>
<td class="org-right">110</td>
<td class="org-right">30</td>
<td class="org-left">"normal"</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">"460.0"</td>
<td class="org-right">1</td>
<td class="org-right">0</td>
</tr>


<tr>
<td class="org-left">"Articuno"</td>
<td class="org-right">85</td>
<td class="org-right">20480</td>
<td class="org-right">35</td>
<td class="org-right">3</td>
<td class="org-left">"Freeze Pokémon"</td>
<td class="org-right">100</td>
<td class="org-right">1250000</td>
<td class="org-left">"1.7"</td>
<td class="org-right">90</td>
<td class="org-right">144</td>
<td class="org-right">95</td>
<td class="org-right">125</td>
<td class="org-right">85</td>
<td class="org-left">"ice"</td>
<td class="org-left">"flying"</td>
<td class="org-left">"55.4"</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>


<tr>
<td class="org-left">"Zapdos"</td>
<td class="org-right">90</td>
<td class="org-right">20480</td>
<td class="org-right">35</td>
<td class="org-right">3</td>
<td class="org-left">"Electric Pokémon"</td>
<td class="org-right">85</td>
<td class="org-right">1250000</td>
<td class="org-left">"1.6"</td>
<td class="org-right">90</td>
<td class="org-right">145</td>
<td class="org-right">125</td>
<td class="org-right">90</td>
<td class="org-right">100</td>
<td class="org-left">"electric"</td>
<td class="org-left">"flying"</td>
<td class="org-left">"52.6"</td>
<td class="org-right">1</td>
<td class="org-right">1</td>
</tr>
</tbody>
</table>

Dans ce TP, on cherche à répondre à cette question :

Étant données les caractéristiques d'un pokemon dont on ne sait s'il est légendaire ou non, 
peut-on pédire si ce pokemon est légendaire ou nom ?


<a id="orgd5e45e2"></a>

# Matériel fourni


<a id="orga0c085a"></a>

## Fichiers csv

Quatre jeux de données sont fournis :

-   le fichier `pokemon_train.csv` contient les pokemons sur lesquels va se baser l'apprentissage.
-   le fichier `pokemon_test.csv` contient les pokemons grace auxquels nous allons tester la qualité de l'apprentissage.
-   le fichier `pokemon_small.csv` contient les caractéristiques de 20 pokemons, utilisés pour les doctests.
-   le fichier `pokemon_suspect1.csv` contient les caractéristiques de certains pokemons.
-   le fichier `pokemon_suspect2.csv` contient les caractéristiques de certains pokemons.


<a id="orge73ebac"></a>

## Le module `pokemon.py`

Le fichier `pokemon_squel.py` contient :

-   la définition du type  `Pokemon` ,
-   la fonction `read_pokemon_csv` permettant de lire le fichier de données.
-   la fonction `split_pokmeons` permettant de séparer l'ensemble des données en deux sous-ensembles.
-   la fonction `min_max_values` permettant de calculer les valeurs minimales et maximale d'un attribut numérique d'une liste 
    de pokemons.
-   les fonctions  `pokemon_euclidian_distance` (à compléter) et `pokemon_manhattan_distance` permettant de calculer
    la distance entre deux pokemons. Pour des questions de normalisation, ces méthodes prennent également en paramètre un dictionnaire
    associant à chaque attribut numérique le couple (mini, maxi) des valeurs minimales et maximale de l'attribut.


<a id="org5236539"></a>

## Le module `knn_pokemon`

Le fichier `knn_pokemon_squel.py` contient les entêtes des fonctions nécessaires à l'implémentation
de l'algorithme des \(k\) plus proches voisins (`knn`). 


<a id="org26f65aa"></a>

# Travail à réaliser

-   Renommer le fichier `pokemon_squel.py` fourni en `pokemon.py`, puis compléter la fonction de 
    distance `pokemon_euclidian_distance`. Les attributs utilisés pour calculer la distance sont
    fournis dans la variable `POKE_PROP_USED_FOR_DISTANCE`
-   Renommer le fichier `knn_pokemon_squel.py` fourni en `knn_pokemon.py`, puis compléter les fonctions :
    -   `nearest_neighbors` : renvoie la liste des \(k\) plus proches pokemons.   
        Vous pourrez par exemple construire la liste des couples \((distance, voisin)\), puis la trier selon
        la première clé.
    -   `knn` : effectue la prédiction de l'attribut en déterminant l'attribut majoritaire dans le voisinage.

-   Utiliser les fonctions précédentes pour compléter le tableau suivant :
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-left" />
    
    <col  class="org-left" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Fichier</th>
    <th scope="col" class="org-left">Meilleur \(k\) pour la distance euclidienne</th>
    <th scope="col" class="org-left">Meilleur \(k\) pour la distance de Manhattan</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">`pokemon_test.csv`</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    </tr>
    
    
    <tr>
    <td class="org-left">`pokemon_suspect1.csv`</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    </tr>
    
    
    <tr>
    <td class="org-left">`pokemon_suspect2.csv`</td>
    <td class="org-left">&#xa0;</td>
    <td class="org-left">&#xa0;</td>
    </tr>
    </tbody>
    </table>
    
    **Méthodologie :** 
    
    On charge les quatre jeux de données :
    
        train = read_pokemon_csv('pokemon_train.csv')
        test = read_pokemon_csv('pokemon_test.csv')
        suspect1 = read_pokemon_csv('pokemon_suspect1.csv')
        suspect2 = read_pokemon_csv('pokemon_suspect2.csv')
    
    -   Première ligne : utiliser la fonction `knn_data` avec `train` et `test` pour estimer les 
        meilleur \(k\). : 
        
            [ knn_data(test, train, k , ...) for k in range(5, 20) ]
    
    -   Deuxième ligne :   
        utiliser la fonction `knn_data` avec `suspect1` et `test`. Que constate-t-on ? Pourquoi ?
    -   Troisième ligne :   
        utiliser la fonction `knn_data` avec `suspect2` et `test`. Que constate-t-on ? Pourquoi ?  
        (remarque : c'est moins flagrant ici, la méthode continue de bien classer les pokemons. Il
         faudrait modifier les attributs utilisés dans la fonction distance pour constater une 
         dégradation)


<a id="org17f2986"></a>

# Pour aller plus loin

-   Ajouter d'autres attributs dans le calcul de la distance. Constatez que cela peut dégrader le 
    résultat.
-   Modifier la méthode de vote dans la fonction `knn` pour que les voisins les plus proches 
    aient davantage de poids.
-   Modifier la fonction de distance en attribuant un poids à chaque attribut.

