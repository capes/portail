#+title: Arbre de décision
* Caractéristiques

 - Algorithme de classification.
 - Prédiction d'une classe discrète.
 - Probabilité d'appartenance à chaque classe.
   
* Exemple

  [[file:images/arbre_exemple.png][file:~/git/CAPES/ue1/supports/classification/images/arbre_exemple.png]]

* L'échantillon

  #+INCLUDE: "exemple1.csv"

* Lecture de l'arbre

  - Rectangles : tests sur un attributs.
  - Arcs : valeur de l'exemple pour le test.
  - Feuilles (ellipses) : classe.
  

* Utiliser l'arbre pour classer un exemple

  - Partir de la racine.
  - Répondre aux questions en suivant la branche correspondante.
  - Lire la valeur de la feuille atteinte.
  

* Exemple à classer 
  
  - Aspect du ciel : Soleil
  - Humidité : Normale
  - Vent : Faible
  - Température : 30

  [[file:images/arbre_utilisation.png][file:~/git/CAPES/ue1/supports/classification/images/arbre_utilisation.png]]


* Remarques

  - Pour un exemple, tous les attributs ne pas utilisés.
  - Selon l'exemple, des attributs différents sont utilisés.
  - La température n'intervient pas.
  

* Construire des arbres 

  - Algorithmes construisant un arbre à partir d'une base
    d'apprentissage.
  - Déterminer le meilleur arbre, au sens ou il classe bien tous 
    les enregistrements de la base.
    + Si deux éléments identiques de la base ont les mêmes classes,
      alors un "meilleur" arbre existe, mais il n'est pas forcément
      intéressant (pas de pouvoir de généralité).
    + Tester tous les arbres possibles : exponentiel, impraticable.


* Construction de l'arbre

  On se contente d'un *bon* arbre :
  - Compact : éviter les peignes (listes de décisions) ;
  - Taux d'erreur faible ;
  - Lisible, compréhensible par un non-expert ;
  - Complexité acceptable.
  

* Algorithme ID3 C4.5

  Fonction $\mbox{CréerNoeud}(\mathcal B, \ell)$

  *Entrées*
  - $S$ : un ensemble d'exemples.
  - $\ell$ : un tuple d'attributs non encore utilisés.
  
  *Sortie* Un arbre de décision
    
* Algorithme ID3 C4.5

  Fonction $\mbox{CréerNoeud}(S, \ell)$
  1. Si tous les exemples sont de la même classe, créer et renvoyer
     une feuille etiquetée par cette classe ;
  2. Sinon :
     1. Trouver le meilleur attribut $a$ dans $\ell$
     2. Répartir les exemples de $S$ en $n$ sous-ensembles $S_i$,
        selon leur valeur pour cet attribut
     3. Allouer un noeud étiqueté par $a$
     4. *Pour* toutes les valeurs de $a$:
        1. $f\leftarrow \mbox{CréerNoeud}(S_i, \ell-\lbrace a\rbrace)$
        2. ajouter $f$ comme fils du noeud étiqueté par $a$.
     5. *Fin Pour*
     

* Remarques

  - *But* : Obtenir des feuilles où tous les exemples sont de la même classe;
  - *Moyen* :
    - Trouver le *meilleur* attribut pour chaque neud.
    - Pouvoir mesurer le *degré de mélange* d'un ensemble d'exemples.


* Mesure du degré de mélange

  - $n$ exemples, deux classes : $\frac{n}{2}$ exemples de classe
    $c_1$ => mélange maximal
  - $n$ exemples, deux classes : $n-1$ exemples de classes $c_1$.
    + Il s'agit d'un ensemble presque pur ;
    + mélange minimal.


* Mesurer le mélange

  - Fonction de $p_1, \ldots , p_n$ , les proportions d'exemples pour
    chaque classes.
  - Maximale quand tous les $p_i$ sont égaux
  - Minimale quand tous les exemples sont de la même classe/


  *Exemple pour deux classes*
  - Fonction de $p_1=p, p_2 = 1-p$
  - Maximale pour $p = \frac{1}{2}$
  - Minimale quand $p=0$ ou $p=1$.


* Mesurer le mélange

  - Polynômes : Paraboles, quadriques;
  - Fonction de Gini :
    $$2p(1-p)$$
    $$1-\sum_{i=1}^np_i^2$$
  - Entropie
    $$-p\log_2(p)-(1-p)\log_2(1-p)$$
    $$-\sum_ip_i\log_2(p_i)$$


* Mesurer le mélange 

  #+begin_src python :results file :exports results
    import matplotlib.pyplot as plt
    from numpy import arange, log

    log_2 = lambda x : log(x)/log(2)

    x = arange(0, 1, .01)
    plt.plot(x, 2*x*(1-x), label='Gini')
    plt.plot(x, -x*log_2(x)-(1-x)*log_2(1-x), label='Entropie')
    plt.legend()

    NAME = 'entropie.png'
    plt.savefig(NAME)
    return NAME
  #+end_src

  #+RESULTS:
  [[file:entropie.png]]


* Entropie

  - Thermodynamique
  - Théorie de l'information : Nombre moyen de bits nécessaires transmettre sans ambiguité
    un symbole.


* Utiliser l'entropie 

  [[file:images/entropie_utilisation.png][file:~/git/CAPES/ue1/supports/classification/images/entropie_utilisation.png]]


* Utiliser l'entropie

  - Au départ : $N$ enregistrement, $k$ classes, entropie $E$.
  - Pour l'attribut testé : $q$ branches.
  - $N_i$ enregistrements suivent la branche $i$.
  - Dans la branche $i$, la répartition des classes est donnée par 
    $p_{i,1},p_{i, 2},\ldots,p_{i, k}$
  

  *Questions*
  - Que nous apprend de plus, pour la classe, le fait de connaître la
    valeur de l'enregistrement pour l'attribut testé ?
  - Quel est le *gain* associé à cet attribut ?
  

* Gain

  *Définition* Le gain d'un ensemble d'enregistrements $S$ pour un
   attribut $a$ est défini par :

   $$\mbox{gain}(S, a) = \mbox{Entropie}(S)-\sum_{i=1}^q\frac{\vert S_i\vert}{\vert S\vert}\times \mbox{Entropie}(S_i)$$

   où :
   - $S_i$ est le sous-ensemble des enregistrements suivant la branche $i$
   - $\vert S_i\vert$ cardinal de $S_i$
   - $q$ nombre de branches du test

* Gain : Justification    

  Plus le gain est élevé, plus le test mininise en moyenne l'entropie
  des $S_i$. 
  - ie : Dans les $S_i$, on a "moins de mélange" (en moyenne);
  - ie : Le test est selectif

  ex : si le gain est nul, cela signifie qu'on a les mêmes
  proportions dans les sous-ensembles, c'est-à-dire qu'on a rien
  appris.

  Si l'entropie des $S_i$ est nulle (on connait la classe avec
  certitude), le gain est maximum.

  *Choix* : L'attribut choisi par l'algorithme est celui qui
  maximise le gain


* Gain : exemple de calcul

  - 10 enregistrements, deux classes ($c_1$ et $c_2$)
    (5 enregistrements de chaque classe)
  - deux attributs à tester $a$ (deux valeurs $a_1$ et $a_2$) et $b$
    (deux valeurs $b_1$ et $b_2$)
    
    |       | $a_1$ | $a_2$ |
    |-------+-------+-------|
    | $c_1$ |     4 |     1 |
    | $c_2$ |     1 |     4 |
    
    |       | $b_1$ | $b_2$ |
    |-------+-------+-------|
    | $c_1$ |     3 |     2 |
    | $c_2$ |     2 |     3 |
    
    

* Gain : exemple de calcul
  
  $$E(S) = -\frac{5}{10}\times \log_2(\frac{5}{10}) -\frac{5}{10}\times \log_2(\frac{5}{10}) = 1$$

  $$\mbox{gain}(S, a)=$$

  $$\mbox{gain}(S, b)=$$

  #+begin_src python :tangle src/ex.py :results output :exports none
    from math import log
    
    # Systeme initial
    S = (5/10, 5/10)

    def entropie( t ):
        """
        :param t: (tuple) les proportions
        de classes dans l'exemple
        >>> entropie((1/2, 1/2))
        1.0
        """ 
        return sum(-p*log(p, 2)
                   for p in t)
    # attribut a
    gain_a = 1 - (5/10)*entropie((4/5, 1/5)) - (5/10)*entropie((1/5, 4/5))
    print(gain_a)
    # attribut b
    gain_b = 1 - (5/10)*entropie((3/5, 2/5)) - (5/10)*entropie((2/5, 3/5))
    print(gain_b)
  #+end_src

  #+RESULTS:
  : 0.2780719051126377
  : 0.02904940554533142

* Gain : exemple de calcul

  - L'attribut $a$ conduit au meilleur gain : les deux fils du noeud
    sont plus purs.
  - Connaître la valeur d'un enregistrement pour l'attribut $a$
    m'apporte en moyenne plus d'information sur sa classe que sa
    valeur pour l'attribut $b$.
  - Cette connaissance me permet de diminuer l'erreur de
    classification.
  

* Aspect du ciel 
 
 Créer l'arbre pour les données "aspect du ciel"

 
* En Python

#+begin_src python :session decision :results output :exports none
    import csv
    data = list(csv.DictReader(open('exemple1.csv')))
    for row in data:
        print(row)
  
#+end_src

#+RESULTS:
#+begin_example
{'Ciel': 'Soleil', ' Température': 'Chaud', 'Humidité': 'Forte', 'vent': 'faible', 'Jouer ?': 'Non'}
{'Ciel': 'Soleil', ' Température': 'Chaud', 'Humidité': 'Forte', 'vent': 'Fort', 'Jouer ?': 'Non'}
{'Ciel': 'Couvert', ' Température': 'Chaud', 'Humidité': 'Forte', 'vent': 'faible', 'Jouer ?': 'Oui'}
{'Ciel': 'Pluie', ' Température': 'Doux', 'Humidité': 'Forte', 'vent': 'faible', 'Jouer ?': 'Oui'}
{'Ciel': 'Pluie', ' Température': 'Frais', 'Humidité': 'Normale', 'vent': 'faible', 'Jouer ?': 'Oui'}
{'Ciel': 'Pluie', ' Température': 'Frais', 'Humidité': 'Normale', 'vent': 'Fort', 'Jouer ?': 'Non'}
{'Ciel': 'Couvert', ' Température': 'Frais', 'Humidité': 'Normale', 'vent': 'Fort', 'Jouer ?': 'Oui'}
{'Ciel': 'Soleil', ' Température': 'Doux', 'Humidité': 'Forte', 'vent': 'faible', 'Jouer ?': 'Non'}
{'Ciel': 'Soleil', ' Température': 'Frais', 'Humidité': 'Normale', 'vent': 'faible', 'Jouer ?': 'Oui'}
{'Ciel': 'Pluie', ' Température': 'Doux', 'Humidité': 'Normale', 'vent': 'faible', 'Jouer ?': 'Oui'}
{'Ciel': 'Soleil', ' Température': 'Doux', 'Humidité': 'Normale', 'vent': 'Fort', 'Jouer ?': 'Oui'}
{'Ciel': 'Couvert', ' Température': 'Doux', 'Humidité': 'Forte', 'vent': 'Fort', 'Jouer ?': 'Oui'}
{'Ciel': 'Couvert', ' Température': 'Chaud', 'Humidité': 'Normale', 'vent': 'faible', 'Jouer ?': 'Oui'}
{'Ciel': 'Pluie', ' Température': 'Doux', 'Humidité': 'Forte', 'vent': 'Fort', 'Jouer ?': 'Non'}
#+end_example

  #+begin_src python :session decision
    from math import log

    def entropie(data, attribut_classe):
        dico = {}
        for row in data:
            classe = row[attribut_classe]
            if classe in dico:
                dico[classe] += 1
            else:
                dico[classe] = 1
        total = sum(dico.values())
        return sum( -n/total * log(n/total, 2) for n in dico.values() )

    def gain(data, attribut, attribut_classe):
        gain = entropie(data, attribut_classe)
        # détermine les valeurs pour l'attribut
        att_values = set(row[attribut] for row in data)
        # filtre selon les valeurs
        for v in att_values:
            # les lignes ayant pour attribut v
            sub_data = [ row for row in data if row[attribut] == v ]
            # l'entropie de ces lignes pour la classe
            sub_ent = entropie(sub_data, attribut_classe)
            # calcul du gain obtnu
            gain -= (len(sub_data)/len(data))*sub_ent
        return gain
  #+end_src

  #+RESULTS:

  
* Remarques

- Lecture aisée, possibilité d'extraire des règles :
  *si (Aspect=Soleil) et (Humidité=Normale) alors oui*
- Permet de reprérer les attributs les plus déterminants pour la
  classification
- Complexité : À chaque profondeur, pour chaque attribut possible, on
  parcourt l'ensemble *complet* des exemples : $O(n\times h\times p)$

* remarques 
- Algorithme glouton :
  - Quand un attribut est choisi, on restreint l'espace de recherche ;
  - On ne remet pas en cause les choix précédents ;
- On ne parcourt pas l'ensemble de tous les arbres possibles ;
- En sélectionnant les attributs les plus discriminants d'abord,
  on engendre des arbres moins hauts.

* Attributs continus 

  À partir de cet attribut continu, obtenir un test binaire
  ($n$-aire). Couper l'intervalle des valeurs de l'attribut en ce
  noeud en $n$ sous-intervalle : discrétiser
  - Combien d'intervalle ?
  - Où effectuer les coupures ?
  - Pas de solution trop coûteuse ! (ne pas explorer toutes les
    coupures en $n$ intervalles)

* Attributs continus

  
  - Exemples 

    | Exemple | E1 | E2 | E3 | E4 | E5 | E6 |
    |---------+----+----+----+----+----+----|
    | valeur  |  2 |  3 |  1 |  0 |  0 |  5 |
    | classe  |  1 |  2 |  2 |  2 |  1 |  1 |
    
  - Ordonner selon les valeurs de l'attribut

    | Exemple | E4 | E5 | E3 | E1 | E2 | E6 |
    |---------+----+----+----+----+----+----|
    | valeur  |  0 |  0 |  1 |  2 |  3 |  5 |
    | classe  |  2 |  1 |  2 |  1 |  2 |  1 |
    

* Attributs continus 
  
  *7 coupures binaires possibles* :
  - -0,5 - 0,5 - 1,5 - 2,5 - 3,5 - laquelle apporte le meilleur gain
  ?

  *Remarque* L'entropie initiale étant toujours la même, on se
   contente de minimiser l'entropie des fils.
   - coupure 1 : un seul fils (3 c1, 3 c2) : entropie moyenne nulle,
     pas de gain.
   - coupure 2 : $\frac{1}{6}\times E(0,1)+\frac{5}{6}\times E(3, 2)=0,8$
   - coupure 3 : 
   - coupure 4 :
   - couupre 5 :
   - coupure 6 :
   - coupure 7 :
   

* Attributs continus 

  - Complexité : coût d'un tri
  - Complexité de l'algorithme complet : $O(n\log n)$
  - Seuil choisi = information supplémentaire sur les données

