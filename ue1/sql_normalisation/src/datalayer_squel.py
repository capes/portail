import sqlite3


class DataLayer:
    def __init__(self, fname: str = 'jo.db'):
        self._conn = sqlite3.connect(fname)
        self._cur = self._conn.cursor()

    def cree_bases(self):
        """
        create all schémas in database
        """
        self._cur.executescript("""
#script à compléter
""")

    def commit(self):
        self._conn.commit()

    # méthodes de modification / selection

    # execution une requete arbitraire
    def exec_req(self, req: str):
        # tentative de sécurisation ( non suffisant )
        assert 'DROP' not in req.upper()
        assert 'DELETE' not in req.upper()
        assert req.strip().upper().startswith("SELECT")
        req = req.split(";")[0]
        self._cur.execute(req)
        return self._cur.fetchall()
