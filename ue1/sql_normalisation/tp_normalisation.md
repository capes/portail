**Objectifs** :

1.  Utiliser des données accessibles sur le web ;
2.  Normaliser un schéma relationnel ;
3.  Utiliser les fichiers csv ;
4.  Concevoir une interface web pour exécuter des requêtes.

Dans ce tp, on chercher à concevoir une interface web comme on peut la trouver [ici](http://deptfod.cnam.fr/bd/tp/).

# Obtenir des données

De nombreux sites proposent des données en accès public. Par exemple :

- <https://www.data.gouv.fr/fr/>
- <https://opendata.lillemetropole.fr/pages/home/>
- <http://www.kaggle.com>
- <https://data.world/>

1.  Identifier le système de licence du site `kaggle.com`
À quelles licences sont soumises les données disponibles ?  
Comment sont elles gérées ?

2.  Télécharger [ce fichier csv](./athlete_events.csv.zip) issu de kaggle.

# Normaliser une relation

1.  Utiliser les règles de normalisation pour normaliser les données du fichier.
    
    -   **1NF** : Les attributs ont des valeurs atomiques ;
    -   **2NF** : La clé est **minimale**. C'est-à-dire qu'aucun autre
        attribut ne doit dépendre d'un sous-ensemble strict de la clé ;
    -   **3NF** : Dans toute dépendance fonctionnelle $S\rightarrow A$, $S$ est une clé.

**En déduire** un ensemble de relations.  
**Préciser** les clés primaires et étrangères, ainsi qu'un maximum de contraintes.
    
    **conseil :** Le nom des athlètes doit être unique.

2.  En utilisant le module `sqlite3`, réaliser une classe python `DataLayer` (un squelette vous est fourni [`datalayer_squel.py`](./src/datalayer_squel.py) disposant :

-  d'une méthode `cree_bases` permettant de créer les relations. Voici un exemple
        de code sql avec contraintes :

```sql
    CREATE TABLE IF NOT EXISTS Res(
        athID integer NOT NULL,
        year integer NOT NULL,
        evID integer NOT NULL,
        medal text CHECK (medal in ('Gold', 'Bronze', 'Silver')),
        FOREIGN KEY(athID) REFERENCES Athlete(athID)
        on delete cascade,
        FOREIGN KEY(year) REFERENCES Games(year)
        on delete cascade,
        FOREIGN KEY(evID) REFERENCES Event(evID)
        on delete cascade
        );
```

-   de méthodes permettant d'insérer des tuples (au sens relationnel) dans chaque relation.  
Par exemple, si la base de données contient la relation `Game(*Year*, season, city)`,  
alors vous implanterez la méthode de signature  
`def insert_game(self, year: int, season: str, city: str):`  
On veillera à ce que ces méthodes renvoient la clé relative au dernier tuple enregistré :

```python
    return self._cur.lastrowid
```

-   d'une méthode permettant d'effectuer une requête dans la base. (voir code fourni)

3. La fonction `reader` du module `csv` permet de parcourir un fichier csv.
    Voici un exemple de lecture du fichier csv fourni :

```python
import csv
with open('athlete_events.csv') as csvfile:
    jo_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    print(f'entete : {next(jo_reader)}') # la fonction next "consomme" une ligne
    athletes = set()
    for row in jo_reader:
        athletes.add(row[1]) # le deuxième élément de row est le nom de l'athlete
    print(f'il y a {len(athletes)} athletes dans le fichier')
```

Dans un script `feed_database.py`, utiliser cette classe pour :

- parcourir les lignes du fichiers ;
- ajouter au besoin les données dans la base (attention aux redondances). On pourra être amené à ajouter à la classe`DataLayer` plusieurs fonctions python permettant d'obtenir la clé d'un tuple si celui-ci est dans une relation.

À l'issue de cette question, vous devez avoir toutes les données enregistrées dans le fichier dans votre base de données.

**conseil :** si vous voulez que la base de donnée soit synchronisée
    avec le fichier, utilisez la méthode `commit`.

# Réalisation d'une interface web

Nous souhaitons créer une interface web à destination des élèves pour qu'ils puissent interroger la base de données.

1. Nous allons utiliser le module flask pour créer notre formulaire. Décompresser le [serveur flask](./server.zip) minimal proposé.

Le serveur de formulaire se lance *en exécutant* le script `flask_formulaire.py`. Il est consultable à l'adresse `http://localhost:5000/`