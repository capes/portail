Système d'exploitation
======================

Préliminaires - Shell et Gitlab
-------------------------------

Mise à niveau / rappel shell Unix et Git/Gitlab 

* [Initiation à UNIX, à l'interpréteur de commandes](../shell/shell.md) -
support de TP, point d'entrée de la séance 
* [Brève introduction à UNIX, à l'interpréteur de commandes](../shell/shell-bref.md) 
* [Git(Lab) : pour bien commencer](../git/gitlab.md)

Dans les programmes
-------------------

[Programmes et ressources NSI - eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-gt)
* [programme NSI 1re - BO](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf) \
  → extrait dans [bo-nsi.md](./bo-nsi.md)
* [programme NSI Tale - BO](https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf)  \
  → extrait dans [bo-nsi.md](./bo-nsi.md)

* _également sur cette page éduscol_ :
  - ressource [Systèmes de type UNIX : structures de données et algorithmes — educsol](https://cache.media.eduscol.education.fr/file/NSI/76/7/RA_Lycee_G_NSI_arch_systemes_approfondis_1170767.pdf)
  - ressource [Systèmes de type UNIX : le point de vue utilisateur — educsol](https://cache.media.eduscol.education.fr/file/NSI/76/8/RA_Lycee_G_NSI_arch_systemes_utilisateurs_1170768.pdf)
  - sitographie pour le programme de Terminale — [Gestion des processus et des ressources par un système d’exploitation](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-gt#summary-item-21)

[Site du jury du CAPES NSI](https://capes-nsi.org/)
* [épreuves de leçon](https://capes-nsi.org/orales/#epreuve-de-lecon) 
  - [liste des leçons session 2021](https://capes-nsi.org/pdf/2021/liste_lecons_2021.pdf) 
  - [liste des leçons session 2022](https://capes-nsi.org/pdf/2022/liste_lecons_2022.pdf) 
  - [liste des leçons session
    2023](https://capes-nsi.org/pdf/2023/liste_lecons_2023.pdf)
    (identique à 2022)

Système d'exploitation
----------------------

* [Système d'exploitation](system.pdf) - support de présentation générale  
  (sont également disponibles
  [version 4 pages par page](system-4up.pdf), 
  [fichier source Markdown](system.md))
  * système d'exploitation 
  * processus 
  * fichier et système de fichiers
  * avec le shell

* Permission UNIX :
  [page Wikipédia](https://fr.wikipedia.org/wiki/Permissions_UNIX) 
  - notion d'utilisateur 
  - différents droits 
  - représentation `rwx`
  - représentation _octale_

* Manipulation des processus 
  * [support de présentation](process-howto.pdf)  
    [version 4 pages par page](process-howto-4up.pdf), 
    [fichier source Markdown](process-howto.md))
  * [travaux pratiques](process-tp.md)

* Synchronisation et interblocage
  * synchronisation
  * interblocage
  * [support de présentation](synchro.pdf)  
    [version 4 pages par page](synchro-4up.pdf), 
    [fichier source Markdown](synchro.md))
  * [manipulation de verrous `flock`](https://www.fil.univ-lille.fr/~marquet/cuisine/flock.html)
* Le système cuisine 
  * _Le système cuisine_ - activité d'informatique sans ordinateur 
  * [Activité, le système cuisine](https://www.fil.univ-lille.fr/~marquet/cuisine/activite.html)
  * [TP, la cuisine du système](https://www.fil.univ-lille.fr/~marquet/cuisine/)


Autres ressources
------------------

* Annales de type BAC 
  - [exercice 1](annale-nsi-1.pdf)
  - [exercice 2](annale-nsi-2.pdf)
  - [exercice 3](annale-nsi-3.pdf)

<!-- eof -->

