Manipulation de processus
=========================

* [Support de présentation](process-howto.pdf)  
  [version 4 pages par page](process-howto-4up.pdf), 
  [fichier source Markdown](process-howto.md))

Travaux pratiques
-----------------

### Afficher PID

Écrire et exécuter un programme qui affiche son numéro de processus et termine.

* Exécuter plusieurs fois ce programme.
* Que remarque-t-on ?
  Expliquer. 

### Boucle infinie 

* Proposer un programme Python qui exécute une boucle infinie.
* Lancer une exécution en arrière-plan de ce programme.

Le lancement de ce programme génère des affichages dans le terminal.

* À quoi correspondent ces informations ? Qui a affiché ces informations ? 
* Utiliser ces informations pour interrompre l'exécution de ce programme.

### La commande `ps`

* Quelles informations affiche la commande `ps` ?
* Lancer cette commande sur votre système. 
* Essayer l'option `ps -l`.
* À quoi correspond la colonne `PPID` ? 
  Qu'ont en commun les processus qui partagent une même valeur de PPID ? 

### PID parent

* Trouver dans le manuel la fonction Python qui permet d'afficher le PID du processus parent.
* Proposer un programme qui affiche son PID, le PID de son parent, et termine.
* Exécuter plusieurs fois ce programme depuis un terminal.
* Que remarque-t-on ?
  Comment expliquer cette situation ? 

### Interrompre une boucle infinie 

* Proposer un programme qui affiche son PID puis boucle à l'infini.
* Lancer ce programme.
* Que peut-on faire pour interrompre l'exécution de ce programme ?

### Processus en attente

* Écrire un programme Python qui affiche son PID puis rentre dans une boucle de calcul sans fin.
* Lancer une exécution de ce programme depuis un terminal.  
  Dans un autre terminal, observer avec `ps` le temps de calcul consommé par le processus.
* Dans quel état est ce processus ?
* Écrire un second programme Python qui affiche son PID puis attend une saisie clavier avec `input()`.
* Lancer une exécution de ce programme et observer le temps de calcul consommé.
* Que peut-on conclure ?




