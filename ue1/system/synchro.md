---
title: Synchronisation et interblocage
author: |
    | Philippe Marquet
    | Univ. Lille 
date: |
    | master MEÉF informatique
    | mars 2021
    | 
    | 
    | \protect ![](fig/cc){width=3%}![](fig/by){width=3%}
lang: fr
...


Menu

* synchronisation
* interblocage

Synchronisation
===============

Système d'exploitation multitâche
---------------------------------

- Multiprogrammation : plusieurs activités *simultanées*
- Activité d'exécution
    - processus (*process*), tâche (*task*), processus léger
        (*thread*), etc. 
- Activités sous le contrôle d'un ordonnanceur
    - virtualise le processeur
    - chaque activité détient le processeur tour à tour
    - ordonnanceur décide du tour à tour
- Raisonner sur des activités concurrentes
    - aucune d'hypothèse sur l'ordre relatif des exécutions
    - raisonnement vrai quelque soit l'ordonnancement
- Raisonner sur des activités concurrentes
    - prise en compte de l'exécution interne des activités
    - prise en compte des dépendances entre les activités :
        synchronisation

Nécessité de contrôle de la concurrence
---------------------------------------

- Toute action ne peut être faite concurremment
  ```
      counter++;                 LOAD @"COUNTER" Ri
                                 INCR Ri
                                 STOR Ri @"COUNTER"
  ```
  - deux activités, un processeur
  ```
      counter++;                          counter++;

      LOAD @"COUNTER" R1
                                          LOAD @"COUNTER" R2
                                          INCR R2
                                          STOR R2 @"COUNTER"
      INCR R1
      STOR R1 @"COUNTER"
   ```
        on a perdu une incrémentation !

- Il faut contrôler la concurrence : mécanismes ad hoc

------------------------------

Exécutions possibles de nos incrémentations
-------------------------------------------

```
    LOAD @"COUNTER" R1
    INCR R1
    STOR R1 @"COUNTER"
                                        LOAD @"COUNTER" R2
                                        INCR R2
                                        STOR R2 @"COUNTER"
```
ou
```
                                        LOAD @"COUNTER" R2
                                        INCR R2
                                        STOR R2 @"COUNTER"
    LOAD @"COUNTER" R1
    INCR R1
    STOR R1 @"COUNTER"
```
	  
- Garantir le résultat = opérations exécutés de manière indivisible 

                LOAD @"COUNTER" Ri
                INCR Ri
                STOR Ri @"COUNTER"


Section critique
----------------

- Section critique
    - section de code exécutée de manière indivisible
    - exécution *atomique*
    - exécutée en exclusion mutuelle

\

- Règle de la section critique
    - à tout instant, une activité, au plus, peut être dans la section
        critique

Mécanismes classiques de synchronisation
----------------------------------------

- Mécanismes fournis au programmeur par les systèmes d'exploitation
    - relativement haut niveau !

- Verrou
    - réalisation de l'exclusion mutuelle
    - aussi nommé *mutex*

- Sémaphore
    - gestion de jetons (compteur)
    - associé à une file d'attente
    - exemple : accès multiples (mais bornés) à une ressource

\

- Aussi : conditions, moniteurs de Hoare, etc. 

Synchronisation par verrous
---------------------------

- Entité protégée par un verrou
    - une section critique, ou
    - une donnée : exemple tous les accès à un tableau

- Notion de propriétaire d'un verrou
    - seul le propriétaire a accès à l'entité protégée par le verrou

- Primitives pour l'utilisation de verrous
    - création / initialisation / destruction
    - verrouillage `mutex_lock()`
        - devenir propriétaire
        - bloquant

    - déverrouillage `mutex_unlock()`
        - verrou devient libre

Synchronisation par sémaphores – I
------------------------------

- Sémaphores = structure de données
    - un compteur : « nombre de jetons libres »
    - une file d'activités en attente d'un jeton

- Primitives pour l'utilisation de sémaphores
    - (*Speekt U nederlands?*)
    - création / initialisation : nombre initial de jetons /
        destruction
    - attendre `sem_down()`
        - classiquement noté $P$ (puis-je ?)
        - attribue un jeton à l'activité demandeuse s'il en reste un
        - la bloque sinon
    - poster `sem_up()`
        - classiquement noté $V$ (vas-y !)
        - rend un jeton
        - débloque une activité s'il en existe une

Synchronisation par sémaphores – II
------------------------------

- Compteur associé au sémaphore
    - nombre de « jetons »
    - valeur positive = nombre d'activités pouvant acquérir librement
        la ressource
    - valeur négative = nombre d'activités bloquées en attente de la
        ressource

Deux utilisations typiques des sémaphores – I
-----------------------------------------

- protection d'une ressource partagée
- séquencement d'activités

### Protection d'une ressource partagée par sémaphore ###

- ressource partagée ?
    - une variable, une structure, une imprimante\...

- sémaphore initialisé au nombre d'activités pouvant concurremment
  accéder à la ressource

            semaphore_init(s, <n>)

- chaque accès à la ressource est encadré d'un couple

            semaphore_down(s);
                <accès à la ressource>
            semaphore_up(s);

Deux utilisations typiques des sémaphores – II
-----------------------------------------

### Séquencement d'activités par sémaphore ###

- une activité doit en attendre une autre pour continuer ou
    commencer son exécution
- on associe un sémaphore à l'événement
- par exemple `findupremier`
- initialisé à 0 ; l'événement n'a pas eu lieu

```
activité 1:                           activité 2:
    <action 1>                            semaphore_down(findupremier);
    semaphore_up(findupremier);           <action 2>
```

Problème du producteur consommateur
-----------------------------------

Deux types d'activités

- producteur et consommateur
- reliés par un tampon de taille bornée

Contraintes 

- producteur ne peut pas produire si le tampon est plein
- consommateur ne peut pas consommer si le tampon est vide
- producteur et consommateur ne doivent pas travailler sur le même élément

Problème classique

- mécanismes systèmes basés sur le producteur/consommateur
    - tubes POSIX, *pipe* et *fifo*
    - queues de messages POSIX

Producteur consommateur à base de sémaphores {.allowframebreaks}
--------------------------------------------

Plusieurs sémaphores

- un sémaphore d'exclusion mutuelle d'accès au tampon
  - initialisé à 1 : l'accès est libre
- un sémaphore d'accès en écriture au tampon
  - initialisé à $N$ : le nombre d'emplacements libres
- un sémaphore d'accès en lecture au tampon
  - initialisé à 0 : le nombre d'emplacements occupés


```{.c}
#define N 100                       /* nombre de places dans le tampon */

semaphore_t mutex, vide, plein; 

semaphore_init(&mutex, 1);          /* contrôle d'accès au tampon */ 
semaphore_init(&vide, N);           /* nb de places libres */
semaphore_init(&plein, 0);          /* nb de places occupées */
```


```{.c}
void producteur (void)
{
  objet_t objet ; 

  while (1) {
    produire_objet(&objet);         /* produire l'objet suivant */
    semaphore_down(&vide);          /* dec. nb places libres */ 
    semaphore_down(&mutex);         /* entrée en section critique */ 
    mettre_objet(objet);            /* mettre l'objet dans le tampon */ 
    semaphore_up(&mutex);           /* sortie de section critique */ 
    semaphore_up(&plein);           /* inc. nb place occupées */ 
  }
} 
```

```{.c}
void consommateur (void) 
{
  objet_t objet ; 

  while (1) {
    semaphore_down(&plein);         /* dec. nb emplacements occupés */ 
    semaphore_down(&mutex);         /* entrée section critique */ 
    retirer_objet (&objet);         /* retire un objet du tampon */ 
    semaphore_up(&mutex);           /* sortie de la section critique */ 
    semaphore_up(&vide);            /* inc. nb emplacements libres */ 
    utiliser_objet(objet);          /* utiliser l'objet */
  }
}
```

Interblocage
============

Les 5 philosophes {.allowframebreaks}
-----------------

- cinq philosophes attablés en cercle autour d'un plat de spaghettis
  - mangent et pensent alternativement sans fin (faim ?)
- une fourchette entre chaque couple de philosophes voisins
- manger nécessite les deux fourchettes


Chacun des philosophes =  une activité

Chacune des fourchettes  = une ressource, protégée par un verrou 

\

Une solution triviale peut mener à un interblocage

- aucun des philosophes ne peut progresser


```{.c}
struct mutex_s forks[5];

void 
philo(int me)
{
    for(;;) {
        think();
    
        mutex_lock(forks[me]);
        mutex_lock(forks[(me+1)%5]);
    
        eat()
    
        mutex_unlock(forks[me]);
        mutex_unlock(forks[(me+1)%5]);
    }
}

```

Prévenir les interblocages
--------------------------

### Prévenir ###

Corriger ces "erreurs" de programmation

- pas toujours facile

Autres "blocages" 

- les famines

### Prévention des interblocages ###

- imposer un ordre total sur les ressources.
- un processus demande des ressources dans un ordre croissant

Ce n'était pas le cas de la solution triviale des philosophes

Détecter les interblocages
--------------------------

Au niveau du système d'exploitation

- rarement mis en place
  - difficile, coûteux
  - que faire une fois un interblocage détecté ?
  
Exercice "académique" (ou "scolaire"...)

### Détection des interblocages ###

- demande (éventuellement bloquante) d'accès à une ressource
- accès (exclusif) à la ressource
- libération de la ressource

Graphe d'allocation des ressources
----------------------------------

Graphe biparti

- deux types de nœuds
  - processus
  - ressources 
- arc ressource → processus
  - la ressource est allouée au processus
- arc processus → ressource
  - le processus est bloqué en attente de la ressource

Un cycle dans le graphe $\Longrightarrow{}$ interblocage

Les 5 philosophes — détection interblocage
------------------------------------------

Un ordonnancement menant à un interblocage

* changement de contexte après avoir pris sa fourchette de droite

Interblocage ?

* production du graphe ; détection d'un cycle 

\

Détection _dynamique_ lors de l'exécution

* _vs_ détection _statique_ à la lecture du code

Programmeur : son objet d'étude est le code

* analyse « pour tous les ordonnancements possibles » ...

Les 5 philosophes — prévention des interblocages I
------------------------------------------------

### Règles d'acquisition ascendente des ressources ###

Ordre sur les ressources

* indice des fourchettes

\

Solution triviale

```{.c}
    mutex_lock(forks[me]);
    mutex_lock(forks[(me+1)%5]);
```

* respect pour les philosophes 0 à 4
* non respect pour le philosophe 4

→ modification du code pour le philosophe 4

\

Prouver – se persuader – de la correction ?

Les 5 philosophes — prévention des interblocages II
------------------------------------------------

### Alternative ###

Règles d'acquisition ascendente des ressources non nécessaire

\

Une solution pour les philosophes

* philosophe de rang pair
    - aquisition fourchette droite puis gauche
* philosophe de rang pair
    - aquisition fourchette gauche puis droite


<!-- eof --> 

<!--  LocalWords:  Hoare
 -->
