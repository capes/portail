Système d'exploitation dans les programmes NSI
==============================================

[Programmes et ressources NSI - eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-gt)
* [programme NSI 1re - BO](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)

    > Les systèmes d’exploitation gèrent et optimisent l’ensemble des fonctions de la machine, de l’exécution des programmes aux entrées-sorties et à la gestion d’énergie. 
    
    > **Contenu** Systèmes d’exploitation 
    >
    > **Capacités attendues**
    > - Identifier les fonctions d’un système d’exploitation.
    > - Utiliser les commandes de base en ligne de commande.
    > - Gérer les droits et permissions d’accès aux fichiers.
    >
    > **Commentaires**
    > - Les différences entre systèmes d’exploitation libres et propriétaires sont évoquées.
    > - Les élèves utilisent un système d’exploitation libre.
    > - Il ne s’agit pas d’une étude théorique des systèmes d’exploitation.

* [programme NSI Tale - BO](https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf)

    > Toute machine est dotée d’un système d’exploitation qui a pour fonction de charger les programmes depuis la mémoire de masse et de lancer leur exécution en leur créant des processus, de gérer l’ensemble des ressources, de traiter les interruptions ainsi que les entrées-sorties et enfin d’assurer la sécurité globale du système. 

    > **Contenus** Gestion des processus et des ressources par un système d’exploitation.
    >
    > **Capacités attendues**
    > - Décrire la création d’un processus, l’ordonnancement de plusieurs processus par le système.
    > - Mettre en évidence le risque de l’interblocage (_deadlock_).
    >
    > **Commentaires**
    > - À l’aide d’outils standard, il s’agit d’observer les processus actifs ou en attente sur une machine.
    > - Une présentation débranchée de l’interblocage peut être proposée.

<!-- eof -->

