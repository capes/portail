---
title: Manipulation de processus
author: |
    | Philippe Marquet
    | Univ. Lille 
date: |
    | master MEÉF informatique
    | septembre 2022
    | 
    | 
    | \protect ![](fig/cc){width=3%}![](fig/by){width=3%}
lang: fr
output:
  beamer_presentation:
    slide_level: 2
---

Notion de processus 

* abstraite...

Programme NSI 

* peu de savoir-faire dans les capacités attendues 

### $\Rightarrow{}$ ?? Quelles activités en TP pour des élèves de NSI ? ###

::: notes 
* Je pose la question de : 
  - quelles activités en TP  pour des élèves de NSI ? 
:::

. . .

### ?? Comment créer un processus ? ###

. . .

* (pourquoi ne pas) introduire `fork()` ? 

Créer des processus — I
-----------------------

::: notes 
* dérouler cours NSI manuel terminale 
  - 7.1 une commande = un processus
  - 7.2 mes propres programmes 
  - 7.3 depuis un programme Python 
  - 7.4 identification des processus 
  - 7.5 terminer les processus
:::

### Une commande = un processus ###

Dans un terminal 

```shell
$ grep import prgm.py
```

* crée un processus qui va exécuter la commande `grep`.
* le terminal attend la terminaison de cette commande \
  puis affiche à nouveau le prompt

. . . 

```shell
$ grep import prgm.py &
```

* crée un processus qui va exécuter `grep`
* poursuit sans en attendre la terminaison

→ processus en tâche de fond, en arrière-plan, ou encore en _background_. 

. . . 

### Plusieurs commandes ###

```shell
$ grep import prgm.py & cp prgm.py prgm-save.py & ls 
```

Créer des processus — II 
------------------------

### Ses propres programmes (Pyhthon) ###

Invoquer l'interpréteur Python avec le nom du script en paramètre 

```shell
$ python3 prgm.py
```

Plusieurs commandes, plusieurs fois la même commande, et en arrière-plan :

```
$ python3 prgm.py & python3 prgm.py & python3 exemple.py & 
```

Shebang - I 
-------

* ne pas invoquer l'interpréteur Python depuis la ligne de commande 
* exécuter "directement" le script depuis le terminal 

### Script Python ###

* script = suite de commandes à prendre en charge par un interpréteur donné
* indiquer au système quel interpréteur utiliser
  - première ligne du fichier `#!` 
    - _she_ – de _sharp_ – pour le `#` 
    - _bang_ pour le point d'exclamation
  - suivi du chemin d'accès à l'interpréteur. 
  
. . . 
  
* souvent un shell `/bin/sh`
* mais aussi un interpréteur Python, par exemple `/usr/bin/python3`

. . . 

* chemin de l'interpréteur Python varie d'une installation à l'autre
* plusieurs versions de l'interpréteur installées sur une machine
* utilisation de la commande `env` pour identifier l'interpréteur
* `env` sous `/usr/bin/`. 

→ 1re ligne d'un script Python : 

```python
#! /usr/bin/env python3
```

Shebang - II
------------

### Exécutable ###

* rendre le script Python exécutable
* notion de droits et les commandes associées au programme de première NSI

. . . 

Exemple : 

```shell
$ chmod +x prgm.py 
```

Puis 

```shell
$ ./prgm.py
```

→ création d'un processus 

* qui invoque l'interpréteur Python
* pour prendre en charge l'exécution du programme 

Exécution possible de plusieurs commandes, plusieurs fois la même commande, et en arrière-plan :

```shell
$ ./prgm.py & ./prgm.py & ./exemple.py & 
```

Créer des processus — III
-------------------------

_on en revient à la création de processus_

### Depuis un programme Python ###

Primitive `system()` de la bibliothèque `os` 

```python
import os

repertoire = "essai"
os.system("mkdir " + repertoire)
```

Identification des processus
----------------------------

_PID_, _processus ID_

* affiché sur le terminal lors du lancement de programmes en arrière-plan
* `getpid()` de la bibliothèque `os` Python

<!-- eof --> 
