Préparation CAPES informatique/NSI
==================================

Univ. Lille, département informatique 

Portail (public) du parcours informatique du master MEÉF 2d degré.
Voir aussi http://portail.fil.univ-lille.fr/fe/capes-info

Suivre le master MEÉF informatique, préparer le CAPES NSI ? [→ voyez la FAQ](faq.org)

Réunions de rentrée - septembre 2023
------------------------------------

### 1re et 2eme année de master ###

* réunion de rentrée **jeudi 7 septembre 2023 13h00**
  * Bâtiment M3, Amphi Turing
  * [lien vers le document projeté](./doc/23-24/pre-rentree-23.pdf)
  
Inscriptions
------------

* **inscription au capes** prévues du 3/10 au 9/11
  [lien vers les inscriptions](http://www.devenirenseignant.gouv.fr/cid106023/inscriptions-concours-second-degre.html)
* **dispense d'anglais** pour les étudiants ayant déjà un niveau CLES B2
  [lien vers le document](https://gitlab-fil.univ-lille.fr/capes/portail/blob/master/doc/Anglais_Fiche%20Choix%20formule%20cours%20MEEF%20titulaires%20CLESB2%20sem%20impair.odt)
  
Contenu et calendrier
---------------------

### 1re année ###

* contenu et calendrier spécifiques à l'informatique sur la [page dédiée](calendrier-m1.md)
* [calendrier inspé M1](./doc/23-24/Calendrier 23-24 Master 1 MEEF 2D degré V3.xlsx) *mise à jour : sept 2023*

### 2e année ###

* calendrier sur la [page dédiée](calendrier-m2.md)
* [calendrier inspé M2](./doc/23-24/Calendrier 23-24 Master 2 MEEF 2D degré V2.xlsx) *mise à jour : sept 2023*

### Entrainements oraux épreuve 1 ###

* passages prévus sur la [page dédiée](./passages-oraux-23.md)

Ressources
----------

 * Python : nous utiliserons la version 3 de Python
   * [le site officiel de Python](https://www.python.org/)
   * _Apprendre à programmer avec Python 3_, livre de Gérard Swinnen  
   accessible à partir de
   [inforef.be/swi/python.htm](http://inforef.be/swi/python.htm) /
   [téléchargement direct PDF](http://inforef.be/swi/download/apprendre_python3_5.pdf)
   * [l'environnement de programmation Thonny](https://thonny.org/)

Textes réglementaires
---------------------

* Modalités d'organisation des concours du certificat d'aptitude au
professorat du second degré, Section numérique et sciences informatiques 
  * [legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038808876&categorieLien=id](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038808876&categorieLien=id) \
  [legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000038808876](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000038808876) (PDF, 3 pages)
* Programme de sciences numériques et technologie de seconde générale et technologique
  * [education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138143](https://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138143) \
	[cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf) (PDF, 19 pages)
* Programmes d'enseignement de spécialité de NSI — numérique et sciences informatiques
  * programme de la classe de première de la voie générale \
	[education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138157](http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138157) \
    [cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf) (PDF, 9 pages)
  * programme de la classe de terminale de la voie générale \
	[www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=144028](https://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=144028) \
	[cache.media.education.gouv.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf](https://cache.media.education.gouv.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf) (PDF, 9 pages)


Contacts
--------

Responsable de la formation : Benoit Papegay, secondé par Philippe Marquet \
2e étage de l'extension du bâtiment M3 \
[📧 capes-info@univ-lille.fr](mailto:capes-info@univ-lille.fr)

Secrétariat pédagogique : 
[📧 Lisa Badaoui](mailto:lisa.badaoui@univ-lille.fr)




