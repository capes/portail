Calendrier M1
=============

B3 avec M. Guichard : 30/03 ; 06/04 ; 27/04 ; 04/05

Anglais : 24 février, 24 mars, 31 mars, 7 avril, 28 avril, 5 mai et 12 mai
en SUP 110


### Mardi 10 mai

Deux séances d'oraux :

| Quand       | Où        | Quoi  | Qui                     |
|-------------|-----------|-------|-------------------------|
| 8:30-10:00  | M3 211    | UE A1 | Ph. Marquet, P. Thibaud |
| 10:15-11:45 | M3 Turing | UE A1 | B. Papegay, P. Thibaud  |

Semaine 18
----------

### Lundi 2 mai ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M5 A13 | UE A2 | P. Thibaud |

### Mardi 3 mai ###

| Quand       | Où     | Quoi  | Qui                     |
|-------------|--------|-------|-------------------------|
| 8:30-10:00  | M3-226 | UE A1 | B. Papegay , P. Thibaud |
| 10:15-11:45 | M3 226 | UE A1 | B. Papegay , P. Thibaut |


### Mercredi 4 mai ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M3 211       | UE B3 | M. Guichard |
| 10:15-13:15 | Amphi Turing | DS A2 | P. Thibaud  |

### Jeudi 5 mai ###

| Quand       | Où           | Quoi    | Qui         |
|-------------|--------------|---------|-------------|
| 12:30-15:30 | Amphi-Turing | DS A1   | B. Papegay  |
| 15:45-17:45 | SUP 110      | Anglais | C. Demailly |

Semaine 17
----------

### Lundi 25 avril ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M5 A13 | UE A2 | P. Thibaud |
| 13:00-16:00 | M3 226 | UE A1 | B. Papegay |

*notes :*
- cours de l'après-midi : [programmation dynamique](ue1/progdyn/progdyn.md)

### Mardi 26 avril ###

| Quand       | Où     | Quoi  | Qui                      |
|-------------|--------|-------|--------------------------|
| 8:30-10:00  | M3-226 | UE A1 | Laurent Noé , P. Thibaud |
| 13:00-14:30 | M3 226 | UE A1 | B. Papegay               |

*notes :*
- cours de l'après-midi : programmation dynamique


### Mercredi 27 avril ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M3 211       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |

### Jeudi 28 avril ###

| Quand       | Où           | Quoi    | Qui         |
|-------------|--------------|---------|-------------|
| 10:15-11:45 | Amphi Turing | UE A1   | B. Papegay  |
| 13:00-15:30 | M5-A4        | UE A1/2 | B. Papegay  |
| 15:45-17:45 | SUP 110      | Anglais | C. Demailly |

*notes :*
- cours du matin : algorithmes gloutons
- tp : [programmation dynamique](ue1/progdyn/Readme.md)

Semaine 16
----------

Interruption pédagogique

Semaine 15
----------

Interruption pédagogique

Semaine 14
----------

### Lundi 4 avril ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 13:00-16:00 | M1 Fatou | UE A1 | B. Papegay |

*notes :*
- cours de après-midi : logique

### Mardi 5 avril ###

| Quand       | Où       | Quoi  | Qui                     |
|-------------|----------|-------|-------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | Ph. Marquet, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay              |

*notes :*
- cours de l'après-midi : les problèmes d'optimisation


### Mercredi 6 avril ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M3 211       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |

### Jeudi 7 avril ###

| Quand       | Où          | Quoi    | Qui         |
|-------------|-------------|---------|-------------|
| 10:15-11:45 | M3-Delattre | UE A1   | B. Papegay  |
| 13:00-15:30 | M5-A4       | UE A1/2 | B. Papegay  |
| 15:45-17:45 | SUP 110     | Anglais | C. Demailly |

*notes :*
- cours : optimisation sous contrainte
- tp : [knn et les arbres de décision](ue1/knn_vs_classifier/tp_hp_knn.md)

Semaine 13
----------

*à faire* 
- dossier ue4
- recherches uec (séance possible)

### Lundi 28 mars ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 10:15-11:45 | M5 A13   | UE A2 | P. Thibaud |
| 13:00-16:00 | M1 Fatou | UE A1 | B. Papegay |

*notes :*
- cours de après-midi : logique

### Mardi 29 mars ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*notes :*
- cours de l'après-midi : correction du ds 


### Mercredi 30 mars ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M3 211       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |

### Jeudi 31 mars ###

| Quand       | Où          | Quoi    | Qui         |
|-------------|-------------|---------|-------------|
| 10:15-11:45 | M3-Delattre | UE A1   | B. Papegay  |
| 13:00-15:30 | M5-A4       | UE A1/2 | B. Papegay  |
| 15:45-17:45 | SUP 110     | Anglais | C. Demailly |

*notes :*
- cours : logique
- tp : [knn et les arbres de décision](ue1/knn_vs_classifier/tp_hp_knn.md)

Semaine 12
----------

### Lundi 21 mars ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 10:15-11:45 | M5 A13       | UE A2 | P. Thibaud |

*notes :*
- après-midi : redaction rapport ue4 **à faire tout de suite**

### Mardi 22 mars ###

| Quand       | Où           | Quoi  | Qui                    |
|-------------|--------------|-------|------------------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226       | UE A1 | B. Papegay             |

*notes :*
- cours de l'après-midi : correction du ds 


### Mercredi 23 mars ###

*à faire* 
- dossier ue4
- recherches uec (séance possible)

### Jeudi 24 mars ###

| Quand       | Où          | Quoi    | Qui         |
|-------------|-------------|---------|-------------|
| 10:15-11:45 | M3-Delattre | UE A1   | B. Papegay  |
| 13:00-15:30 | M5-A4       | UE A1/2 | B. Papegay  |
| 15:45-17:45 | SUP 110     | Anglais | C. Demailly |

*notes :*
- cours : la compression jpeg
- tp : [knn et les arbres de décision](ue1/knn_vs_classifier/tp_hp_knn.md)

Semaine 11
----------

stage en établissement

Semaine 10
----------

stage en établissement

Semaine 9
---------

### Lundi 28 février ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 10:15-11:45 | M5 A13   | UE A2 | P. Thibaud |
| 13:00-16:00 | M1 Fatou | UE A1 | B. Papegay |

*notes :*
- cours de l'après-midi : logique, UEC

### Mardi 1 mars ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*notes :*
- cours de l'après-midi : la classification

### Mercredi 2 Mars  ###

DS A2 au P1 Fresnel , 8h00-13h00

### Jeudi 3 Mars  ###

DS A1 et A2 (rattrapage S1), SUP Amphi Galilée, 8h00-12h00


Semaine 8
---------

### Lundi 21 février ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 10:15-11:45 | M5 A13       | UE A2 | P. Thibaud |
| 13:00-16:00 | Amphi Turing | UE A1 | B. Papegay |

*notes :*
- attention : m1 fermé
- cours de l'après-midi : logique

### Mardi 22 Février ###

| Quand       | Où           | Quoi  | Qui                    |
|-------------|--------------|-------|------------------------|
| 8:30-10:00  | Amphi Turing | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226       | UE A1 | B. Papegay             |

*notes :*
- cours de l'après-midi : la classification


### Mercredi 23 février ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M3 211       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |


### Jeudi 24 février ###

| Quand       | Où           | Quoi    | Qui         |
|-------------|--------------|---------|-------------|
| 10:15-11:45 | Amphi Turing | UE A1   | B. Papegay  |
| 13:00-15:30 | M5-A4        | UE A1/2 | B. Papegay  |
| 15:45-17:45 | SUP 110      | Anglais | C. Demailly |

*notes :*
- cours : logique
- tp : icewalker

Semaine 7
---------



Interruption pédagogique

Semaine 6
---------

### Lundi 7 février ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 10:15-11:45 | M5 A13   | UE A2 | P. Thibaud |
| 13:00-16:00 | M1 Fatou | UE A1 | B. Papegay |

- cours : les arbres, les abr

### Mardi 8 Février ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*Note*
- cours de l'après-midi : le compte est bon

### Mercredi 9 février ###

DS A1 au A5 , 8h00-13h00

### Jeudi 10 février ###

| Quand       | Où     | Quoi    | Qui        |
|-------------|--------|---------|------------|
| 10:15-11:45 | M1-202 | UE A1   | B. Papegay |
| 13:00-14:30 | M5-A4  | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4  | UE A1/2 | B. Papegay |

Semaine 5
---------

### Lundi 31 janvier ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 10:15-11:45 | M5 A13   | UE A2 | P. Thibaud |
| 13:00-16:00 | M1 Fatou | UE A1 | B. Papegay |

- cours : les arbres, les abr

### Mardi 1 Février ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*Note*
- cours de l'après-midi : le hachage

### Mercredi 2 janvier ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M1-202       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |

### Jeudi 3 Février ###

| Quand       | Où    | Quoi    | Qui        |
|-------------|-------|---------|------------|
| 13:00-14:30 | M5-A4 | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4 | UE A1/2 | B. Papegay |

*Note*
- Jury S1-S3 le matin
- TP : [Icewalker](./ue1/icewalker/icewalker.md)

Semaine 4
---------

### Lundi 24 janvier ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 10:15-11:45 | M5 A13   | UE A2 | P. Thibaud |
| 13:00-16:00 | M1 Fatou | UE C3 | B. Papegay |

*Note*
- cours : les arbres 

### Mardi 25 janvier ###

| Quand       | Où       | Quoi  | Qui                    |
|-------------|----------|-------|------------------------|
| 8:30-10:00  | M1 Fatou | UE A1 | B. Papegay, P. Thibaud |
| 13:00-14:30 | M3 226   | UE A1 | B. Papegay             |

*Note*
- cours de l'après-midi : le hachage

### Mercredi 26 janvier ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M1-202       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |

### Jeudi 27 janvier ###

| Quand       | Où          | Quoi    | Qui                    |
|-------------|-------------|---------|------------------------|
| 8:30-11:45  | M3-Delattre | UE C3   | B. Papegay/M. Guichard |
| 13:00-14:30 | M5-A4       | UE A1/2 | B. Papegay             |
| 14:45-16:15 | M5-A4       | UE A1/2 | B. Papegay             |

*Note*
- tp : [Huffman](./ue1/huffman/Readme.md)

Semaine 3
---------

### Lundi 17 janvier ###

| Quand       | Où       | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 10:15-11:45 | M5 A13   | UE A2 | P. Thibaud |
| 13:00-16:00 | M1 Fatou | UE C3 | B. Papegay |

cours : les arbres 

### Mardi 18 janvier ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 8:30-10:00  | M1 Fatou  | UE A1 | B. Papegay |
| 13:00-14:30 | M3 Turing | UE A1 | B. Papegay |

*Note*
- cours de l'après-midi : le hachage

### Mercredi 19 janvier ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M1-202       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |

### Jeudi 20 janvier ###

| Quand       | Où    | Quoi    | Qui        |
|-------------|-------|---------|------------|
| 8:30-11:45  | M5-A4 | UE C3   | B. Papegay, M. Guichard |
| 13:00-14:30 | M5-A4 | UE A1/2 | B. Papegay |
| 14:45-16:15 | M5-A4 | UE A1/2 | B. Papegay |

Notes :
- cours : les arbres.
- tp : [arbres](./ue1/arbres/arbre2.md).
- la suite : 
    - abr ;
    - [huffman](./ue1/arbres/huffman/Readme.md).


Semaine 2
----------

### Mardi 11 janvier ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M3 226 | UE A1 | P. Thibaud |
| 10:15-11:45 | M3 226 | UE A1 | B. Papegay |
| 13:00-14:30 | M3 226 | UE A1 | B. Papegay |

Notes
- cours du matin : le hachage ;
- cours de l'après-midi : les arbres.

### Mercredi 12 janvier ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard  |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |

Notes :
Les cours de M. Guichard sont sous réserve.

### Jeudi 13 janvier ###

| Quand       | Où        | Quoi    | Qui        |
|-------------|-----------|---------|------------|
| 10:15-11:45 | M3-Turing | UE A1   | B. Papegay |
| 13:00-14:30 | M5-A4     | UE A1/2 | P. Thibaud |
| 14:45-16:15 | M5-A4     | UE A1/2 | P. Thibaud |

Notes :
- cours : les arbres.

Semaine 1
----------

### Mardi 4 janvier ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M3 226 | UE A1 | P. Thibaud |
| 10:15-11:45 | M3 226 | UE A1 | B. Papegay |
| 13:00-14:30 | M3 226 | UE A1 | B. Papegay |

- [Cours sur les bases de données](./ue1/donnees/donnees.md)

### Jeudi 6 janvier ###

| Quand       | Où        | Quoi    | Qui        |
|-------------|-----------|---------|------------|
| 10:15-11:45 | M3-Turing | UE A1   | B. Papegay |
| 13:00-14:30 | M5-A4     | UE A1/2 | P. Thibaud |
| 14:45-16:15 | M5-A4     | UE A1/2 | P. Thibaud |

- [TP SQL 1](./ue1/sql1/Readme.md)
- [TP SQL 2](./ue1/sql2/Readme.md)
- [TP Normalisation](./ue1/sql4/tp_normalisation.md)

Semaine 50
----------

### Mardi 14 décembre ###

| Quand       | Où         | Quoi  | Qui         |
|-------------|------------|-------|-------------|
| 10:30-12:00 | M3-ext-226 | UE B3 | M. Guichard |

Avec les enseignants suivant le DIU :

* Algorithme min-max - cours 
* Jeux à deux joueurs, min-max, TP 

| quand       | qui      | quoi    | où         | avec qui               |

| ----------- | -------- | ------- | ---------- | ---------------------- |
| 13h30-14h30 | tous     | cours   | M5-Bacchus | Jean-Christophe        |
| 14h45-16h45 | groupe A | TP jeux | M5-A15     | Jean-Christophe        |
|             | groupe B | TP jeux | M5-A16     | Patricia, Patrice      |
|             | groupe C | TP jeux | M5-A11     | Philippe (à confirmer) |

### Mercredi 15 décembre ###

ds d'ue A1
8h - 13h, M1 Galois


### Jeudi 16 décembre ###

ds d'ue A2
8h30 - 12h30, M3 Turing

Semaine 49
----------

### Mardi 7 décembre ###

| Quand       | Où         | Quoi  | Qui         |
|-------------|------------|-------|-------------|
| 8:30-10:00  | M1-LIE     | UE A1 | P. Thibaud  |
| 10:15-11:45 | M3-ext-226 | UE C  | Ph. Marquet |

### Mercredi 8 décembre ###

| Quand       | Où          | Quoi  | Qui                        |
|-------------|-------------|-------|----------------------------|
| 8:30-10:00  | M1-302      | UE B3 | M. Guichard                |
| 10:15-11:45 | Fabricarium | UE C  | P. Thibaud et F. De Comité |
| 13:00-14:30 | M5 A6       | UE C  | F. De Comité               |

### Jeudi 9 décembre ###

| Quand       | Où         | Quoi | Qui         |
|-------------|------------|------|-------------|
| 8:30-10:00  | M3-Turing  | UE C | B. Papegay  |
| 10:15-11:45 | M3-Turing  | UE C | B. Papegay  |
| 13:00-14:30 | M5-A15     | UE C | M. Pupin    |
| 14:45-16:15 | M3-ext-226 | UE C | Ph. Marquet |

* [Sécurisation des communications, rsa](ue1/securisation/Readme.md)

Semaine 48
----------

Stage

Semaine 47
----------

Stage 

Semaine 46
----------


### Mardi 16 novembre ###

| Quand       | Où         | Quoi  | Qui         |
|-------------|------------|-------|-------------|
| 8:30-10:00  | M1-LIE     | UE A1 | P. Thibaud  |
| 10:15-11:45 | M3-ext-226 | UE A1 | Ph. Marquet |

* [Architecture des ordinateurs](ue1/archi/Readme.md)

### Mercredi 17 novembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard  |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M5 A6        | UE A1 | F. De Comité |

### Jeudi 18 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M1-LIE | UE A1 | B. Papegay |
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | UE A1 | B. Papegay |

UE C : [tp analyse des tris](./ue1/analyse_tris/Readme.md)

### Vendredi 19 novembre ###

inspe selon calendrier

Semaine 45
----------

### Lundi 8 novembre ###

| Quand       | Où          | Quoi    | Qui        |
|-------------|-------------|---------|------------|
| 10:00-11:30 | M3-226      | UE A2   | P. Thibaud |
| 13:00-14:30 | M3-Delattre | UE A1   | B. Papegay |
| 15:30-17:30 | SUP-CRL     | Anglais | N. Chapel  |

### Mardi 9 novembre ###

| Quand       | Où         | Quoi  | Qui        |
|-------------|------------|-------|------------|
| 8:30-10:00  | M1-LIE     | UE A1 | P. Thibaud |
| 10:15-11:45 | M3-ext-226 | UE A1 | L. Noé     |

### Mercredi 10 novembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard  |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M5 A6        | UE A1 | F. De Comité |

### Vendredi 12 novembre ###

inspe selon calendrier

Semaine 44
----------

Vacances d'automnes

Semaine 43
----------

### Lundi 25 octobre ###

| Quand       | Où          | Quoi    | Qui        |
|-------------|-------------|---------|------------|
| 13:00-14:30 | M3-Delattre | UE A1   | B. Papegay |
| 15:30-17:30 | SUP-CRL     | Anglais | N. Chapel  |

### Mardi 26 octobre ###

| Quand       | Où         | Quoi  | Qui         |
|-------------|------------|-------|-------------|
| 8:30-10:00  | M1-LIE     | UE A1 | P. Thibaud  |
| 10:15-11:45 | M3-ext-226 | UE A1 | Ph. Marquet |

### Mercredi 27 octobre ###

repos

### Jeudi 28 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | UE A1 | P. Thibaud |

### Vendredi 29 octobre ###

inspe selon calendrier

Semaine 42
----------

### Lundi 18 octobre ###

| Quand       | Où          | Quoi    | Qui        |
|-------------|-------------|---------|------------|
| 13:00-14:30 | M3-Delattre | UE A1   | B. Papegay |
| 15:30-17:30 | SUP-CRL     | Anglais | N. Chapel  |

### Mardi 19 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M1-LIE | UE A1 | P. Thibaud |
| 10:15-11:45 | M1-LIE | UE A1 | L. Noé     |


### Mercredi 20 octobre ###

| Quand      | Où        | Quoi     | Qui        |
|------------|-----------|----------|------------|
| 8:00-13:00 | C15 - 006 | DS UE A1 | B. Papegay |

### Jeudi 21 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | UE C  | M. Pupin   |

UE C : [tp processing](./ue3/py5/Readme.md)

### Vendredi 22 octobre ###

inspe selon calendrier

Semaine 41
----------

### Lundi 11 octobre ###

| Quand       | Où          | Quoi    | Qui        |
|-------------|-------------|---------|------------|
| 13:00-14:30 | M3-Delattre | UE A1   | B. Papegay |
| 15:30-17:30 | SUP-CRL     | Anglais | N. Chapel  |

### Mardi 12 octobre ###

| Quand       | Où     | Quoi  | Qui         |
|-------------|--------|-------|-------------|
| 8:30-10:00  | M1-LIE | UE A1 | P. Thibaud  |
| 10:15-11:45 | M1-LIE | UE A1 | Ph. Marquet |

### Mercredi 13 octobre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard  |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M5 A6        | UE A1 | F. De Comité |

### Jeudi 14 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | TP    | P. Thibaud |

### Vendredi 15 octobre

inspe selon calendrier

Semaine 40
----------

### Lundi 4 octobre ###

| Quand       | Où          | Quoi    | Qui        |
|-------------|-------------|---------|------------|
| 13:00-14:30 | M3-Delattre | UE A1   | B. Papegay |
| 15:30-17:30 | SUP-CRL     | Anglais | N. Chapel  |

### Mardi 5 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M1-LIE | UE A1 | P. Thibaud |
| 10:15-11:45 | M1-LIE | UE A1 | L. Noé     |

### Mercredi 6 octobre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard  |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M5 A6        | UE A1 | F. De Comité |

### Jeudi 7 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | TP    | B. Papegay |

- TP : [Piles et files](./ue1/pile_et_file/readme.md)

### Vendredi 8 octobre

inspe selon calendrier

Semaine 39
----------

### Lundi 27 septembre ###

| Quand       | Où          | Quoi    | Qui        |
|-------------|-------------|---------|------------|
| 13:00-14:30 | M3-Delattre | UE A2   | P. Thibaud |
| 15:30-17:30 | SUP-CRL     | Anglais | N. Chapel  |

### Mardi 28 septembre ###

| Quand       | Où     | Quoi  | Qui         |
|-------------|--------|-------|-------------|
| 8:30-10:00  | M1-LIE | UE A1 | P. Thibaud  |
| 10:15-11:45 | M1-LIE | UE A1 | Ph. Marquet |

* [Système d'exploitation, 2e séance](ue1/system/Readme.md)

### Mercredi 29 septembre ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud  |
| 13:00-14:30 | M5 A6        | UE C  | M. Pupin    |
| 14:45-16:15 | M5 A6        | UE C  | M. Pupin    |

### Jeudi 30 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M1-LIE | UE A1 | B. Papegay |
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | TP    | B. Papegay |

- TP : [listes récursives](./ue1/listes/listes.md)

### Vendredi 1 octobre

inspe selon calendrier

Semaine 38
----------

### Lundi 20 septembre ###

| Quand       | Où          | Quoi    | Qui        |
|-------------|-------------|---------|------------|
| 13:00-14:30 | M3-Delattre | UE A1   | B. Papegay |
| 15:30-17:30 | SUP-CRL     | Anglais | N. Chapel  |

### Mardi 21 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8:30-10:00  | M1-LIE | UE A1 | P. Thibaud |
| 10:15-11:45 | M1-LIE | UE A1 | L. Noé     |

### Mercredi 22 septembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard  |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M5 A6        | UE A1 | F. De Comité |

### Jeudi 23 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | UE A2 | B. Papegay |

- TP : [Phrases de passe](./ue1/phrases_de_passe/readme.md)

### Vendredi 24 septembre ###

**Inspe** : selon planning

Semaine 37
----------

### Lundi 13 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13:00-14:30 | M5-A15 | UE A1 | B. Papegay |

### Mardi 14 septembre ###

| Quand       | Où     | Quoi  | Qui         |
|-------------|--------|-------|-------------|
| 8:30-10:00  | M1-LIE | UE A1 | P. Thibaud  |
| 10:15-11:45 | M5-A4  | UE A1 | Ph. Marquet |

* [Système d'exploitation, 1re séance](ue1/system/Readme.md)

### Mercredi 15 septembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 8:30-10:00  | M1-302       | UE B3 | M. Guichard  |
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M5 A6        | UE A1 | F. De Comité |

### Jeudi 16 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 10:15-11:45 | M1-LIE | UE A1 | B. Papegay |
| 13:00-14:30 | M5-A15 | UE A2 | B. Papegay |


### Vendredi 17 septembre ###

**Inspe** : selon planning

Semaine 36
----------

### Lundi 6 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13:00-14:30 | M5-A15 | UE A1 | B. Papegay |

### Mardi 7 septembre ###

| Quand       | Où    | Quoi  | Qui        |
|-------------|-------|-------|------------|
| 8:30-10:00  | M5-A9 | UE A1 | P. Thibaud |
| 10:15-11:45 | M5-A9 | UE A1 | L. Noé     |


### Mercredi 8 septembre ###

| Quand       | Où           | Quoi  | Qui          |
|-------------|--------------|-------|--------------|
| 10:15-11:45 | Amphi Turing | UE A2 | P. Thibaud   |
| 13:00-14:30 | M5 A6        | UE A1 | F. De Comité |

### Jeudi 9 septembre ###

| Quand       | Où                       | Quoi       | Qui        |
|-------------|--------------------------|------------|------------|
| 10:15-11:45 | M1-LIE                   | UE A1      | B. Papegay |
| 14h00-      | Amphithéâtre de l'IRCICA | Conférence |            |

**Conférence :**
Le manque de femmes en informatique n’est pas une fatalité
Isabelle Collet, professeure à l'Université de Genève


pour y aller : https://ircica.univ-lille.fr/plan-dacces
Jeudi 9 septembre, **14h**

### Vendredi 10 septembre ###

**Inspe** : selon planning

Semaine 35
----------
    
### Jeudi 2 septembre ###

  - pré-rentrée (Amphi Turing, Bâtiment M3) à 14h00
  


