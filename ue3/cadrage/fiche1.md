
# Fiche nº

## Description de l'article

### Titre de l'article :

### Lien de l'article :

### Liste des auteurs :

### Affiliation des auteurs :

### Nom de la conférence / revue :

### Classification de la conférence / revue :

### Nombre de citations de l'article (quelle source ?) :

## Synthèse de l'article

### Problématique

### Pistes possibles (pointés par les auteurs)

### Question de recherche

### Démarche adoptée

### Implémentation de la démarche

### Les résultats
