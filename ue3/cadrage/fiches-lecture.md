---
title: rédaction des fiches de lecture
author: meef nsi
---

# Fiches de lecture

Chaque fiche de lecture doit  respecter le plan suivant :

**Article nºX**  

## Description factuelle de l'article :

- Titre :
- Liste des auteurs : 
- Conférence / Revue (même si c'est indiqué dans la citation) :
- Classification de la conférence / Revue : 
- Nombre de citations (quelle source ?) :
- URL de l'article :
 
## Résumé de l'article :

- Problématique
- Pistes possibles (pointés par les auteurs)
- Question de recherche
- Démarche adoptée
- Implémentation de la démarche
- Les résultats

Chaque fiche de lecture doit comporter au moins 4–5 pages avec une description de la
problématique qui doit couvrir au moins une demi page.  Le template LaTeX mis à votre
disposition comporte une mise en forme des fiches.

## Fiche de synthèse

Un document de synthèse du sujet adressé d’une page minimum comportant :

- La grille de synthèse des articles
- Une description de la problématique adressée.



