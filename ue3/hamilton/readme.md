Recherche de cycles hamiltoniens sur des polyèdres
==================================================

Un cycle hamiltonien sur un graphe est un chemin qui passe une fois et une seule par chaque sommet
du graphe. Le nom vient de William Roman Hamilton, astronome irlandais du XIXème siècle, qui inventa
un jeu où il fallait chercher un tel cycle sur un dodécaèdre.

Le but du tp est d'écrire un programme qui cherchera les cycles hamiltoniens sur un polyèdre
passé en paramètre. La solution sera fournie sous un format lisible par un humain, afin de permettre
à celui-ci d'en réaliser un modèle physique.  Plusieurs possibilités de réalisations concrètes
seront discutées.

# Définitions

## Cycles hamiltoniens

Un cycle hamiltonien sur un graphe est un chemin qui passe une fois et une seule par chaque sommet
de ce graphe. Il existe des graphes qui n'ont pas de cycles hamiltoniens, tandis que d'autres en ont
un grand nombre. Il n'est pas simple de décider si un graphe possède un cycle hamiltonien: c'est
même plutôt difficile (le problème est {\em NP-Complet}). On dira qu'un graphe est hamiltonien s'il
possède un cycle hamiltonien.  Par contre, vérifier qu'un cycle est hamiltonien se fait
rapidement\dots

Du point de vue de la programmation, le fait que le problème soit compliqué a plusieurs conséquences qu'il vaut mieux connaître avant de se lancer :
- Plus le graphe sera grand, plus la recherche prendra de temps.
- Si le graphe n'est pas hamiltonien, on devra attendre que tous les chemins aient été explorés
avant de le savoir.
- Pour certains graphes, le nombre de cycles hamiltoniens différents est
très grand, et il serait illusoire de tous les énumérer.

## Polyèdres

Un polyèdre est un solide dont toutes les faces sont des polygones (et un polygone est une surface
limitée par des segments de droite).

A chaque polyèdre est associé un graphe : celui qui connecte les sommets du polyèdre en suivant ses
arêtes. On peut donc parler de cycles hamiltoniens sur des polyèdres.

Il en existe plusieurs familles de polyèdres (allez voir sur Wikipedia si vous voulez les listes,
les noms et les images) :
- Les polyèdres platoniciens : toutes les faces sont les mêmes. Il en existe cinq : le tétraèdre ,
l'icosaèdre, l'octaèdre, (les faces de ces trois polyèdres sont des triangles équilatéraux)le cube
(les faces sont des carrés) et le dodécaèdre (les faces sont des pentagones). Ils sont tous
hamiltoniens.
- Les polyèdres archimédiens : les faces sont toujours des polygones réguliers, mais on peut
utiliser plusieurs polygones différents, en maintenant le plus de symétries possibles. Par exemple,
le ballon de football (plus formellement l'icosaèdre tronqué), composé de 12 pentagones et de 20
hexagones.  Il y a 13 polyèdres archimédiens, ils sont tous hamiltoniens.
- Les solides de Johnson : c'est une généralisation des polyèdres archimédiens, mais on a levé la contrainte d'avoir des sommets équivalents. Il y en a 92 différents. Au moins  72 sont hamiltoniens. Je n'ai pas testé pour les 20 restant.
- Les solides de Catalan. Ce sont les duals des polyèdres archimédiens : toutes les faces sont
égales, mais ce ne sont plus des polygones réguliers.  Les arêtes peuvent avoir des longueurs
différentes. Seulement sept sur les treize sont hamiltoniens. On n'étudiera pas ces polyèdres dans
ce projet.

Pour ce projet, nous ne considérons que les polyèdres dont toutes les faces sont des polygones réguliers, c'est à dire les platoniciens et les archimédiens.

# Objectifs

Pour chacun de ces polyèdres, on fournit un fichier contenant sa description au format `.off`
décrit ci-après.
Votre programme, qui aura comme paramètre un nom de polyèdre, devra :
- Ouvrir le fichier correspondant à ce polyèdre et en extraire les informations dont il aura besoin.
- Rechercher tous les cycles hamiltoniens sur ce polyèdre (ou bien dire qu'il n'y en a pas).
- Pour chaque cycle trouvé, générer un fichier de description `dot` (voir figure) qui permettra
de visualiser le graphe et le cycle en utilisant des couleurs différentes pour les arêtes du graphe
(voir figure).

# Lire les fichiers .off

Les fichiers dont le suffixe est ".off" contiennent chacun la description d'un polyèdre, dont le nom
est aussi celui du fichier. Tous ces fichiers sont construits de la même façon, étudions celui qui
décrit le dodécaèdre :

    # Dodecahedron
    # Data: Exact Mathematics
    20 12 30
     0.5773502691896  0.5773502691896  0.5773502691896
    -0.0000000000000  0.9341723589627 -0.3568220897731
    -0.5773502691896  0.5773502691896  0.5773502691896
    -0.0000000000000  0.9341723589627  0.3568220897731
    -0.9341723589627  0.3568220897731 -0.0000000000000
    -0.5773502691896  0.5773502691896 -0.5773502691896
     0.9341723589627 -0.3568220897731 -0.0000000000000
     0.9341723589627  0.3568220897731 -0.0000000000000
     0.5773502691896  0.5773502691896 -0.5773502691896
     0.5773502691896 -0.5773502691896  0.5773502691896
     0.3568220897731  0.0000000000000  0.9341723589627
    -0.3568220897731  0.0000000000000  0.9341723589627
    -0.9341723589627 -0.3568220897731 -0.0000000000000
    -0.5773502691896 -0.5773502691896  0.5773502691896
    -0.0000000000000 -0.9341723589627  0.3568220897731
     0.3568220897731  0.0000000000000 -0.9341723589627
     0.5773502691896 -0.5773502691896 -0.5773502691896
    -0.0000000000000 -0.9341723589627 -0.3568220897731
    -0.3568220897731  0.0000000000000 -0.9341723589627
    -0.5773502691896 -0.5773502691896 -0.5773502691896
    5 2 4 5 1 3
    5 0 7 6 9 10
    5 8 15 16 6 7
    5 8 7 0 3 1
    5 5 18 15 8 1
    5 0 10 11 2 3
    5 6 16 17 14 9
    5 11 13 12 4 2
    5 11 10 9 14 13
    5 19 17 16 15 18
    5 19 12 13 14 17
    5 18 5 4 12 19
    0 3
    0 7
    0 10
    1 3
    1 5
    1 8
    2 3
    2 4
    2 11
    4 5
    4 12
    5 18
    6 7
    6 9
    6 16
    7 8
    8 15
    9 10
    9 14
    10 11
    11 13
    12 13
    12 19
    13 14
    14 17
    15 16
    15 18
    16 17
    17 19
    18 19

- la première ligne du fichier donne le nom du polyèdre (en anglais). Le symbole \# définit les
commentaires.
- Peuvent suivre une ou plusieurs lignes de commentaires (et donc commençant par un \#).
- Les trois nombres figurant à la ligne 3 sont respectivement le nombre de sommets, de faces et
d'arêtes.
- Viennent ensuite les coordonnées 3D de chacun des 20 sommets. De façon implicite, cette première
liste attribue un numéro à chaque sommet (celui de la ligne 4 sera repéré par la suite comme étant
le sommet d'indice 0, et celui de la ligne 23 aura l'indice 19).
- Les 12 lignes suivantes décrivent les faces. Chaque ligne commence par un entier $n$, qui est le
nombre de cotés de la face considérée, suivi de $n$ nombres entiers, qui sont les indices des
sommets formant la face.
- Les 30 lignes suivantes décrivent chacune des arêtes, en indiquant les indices des sommets que
joint cette arête.


Utiliser un type graphe (cf tps ue1) pour lire un fichier `.off` et le représenter en mémoire

# rechercher les cycles

Implanter les algorithmes :

- posa-ran ;
- minran.

pour déterminer un cycle hamiltonien.

# Générer une visualisation du chemin

L'algorithme engendrera un chemin ou une liste de chemins, chacun étant un cycle hamiltonien, qu'il
nous faudra ensuite représenter par des arcs de couleur et d'épaisseur différente des autres arcs du
graphe.

![](./images/dodecneto.jpg)

# Dessiner le graphe

[GraphViz](https://www.graphviz.org) met à votre disposition des outils de description et de visualisation de graphes.
Par exemple, le graphe précédent a été obtenu à l'aide de la commande shell

```shell
neato -Tjpeg dodecahedron0.dot -o imagedugraphe.jpg
```

# Matériel fourni

Le fichier [polyèdres.zip](./polyèdres.zip) contient les fichier `.off` de nombreux polyèdres.
