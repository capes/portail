Éléments de statistique
========================

**définition :**

La statistique est la science de l'étude des propriétés d'une
population. On veut soit

  - décrire un aspect de la population connu ;
  - déterminer des caractéristiques manquantes.


Le premier point est le calcul d'une série d'indicateurs relatifs à la
population, ainsi que la production de graphiques.

Le deuxième point est le domaine des statistiques inférentielles : on
formule des hypothèses sur les caractéristiques, et on examine si ces
hypothèses sont plausibles.

# la statistique descriptive

Les populations peuvent être de nature très différentes :

  - les élèves d'une classe ;
  - les expériences réalisées ou simulées ;
  - des biens ou des objets.

Dans cette population, on étudie un **caractère**. Il existe deux grand
types de caractères : les caractères quantitatifs et les caractères
qualitatifs

## les caractères qualitatifs

On mesure dans la population une "qualité" : il s'agit d'une mesure qui,
pour chaque individu, ne peut prendre qu'un nombre fini de valeurs non
numériques, appelés les modes ou modalités.

Par exemple, la couleur des yeux, aimer l'informatique ou pas

Dans la population, on mesure l'**effectif** (ou frequencies) de chaque
modalité.

On peut représenter la série par

  - un diagramme en barre (sans axe des abscisses) ;
  - un "camembert".

Eventuellement, on peut mesurer les **fréquences** de chaque modalités.

ihm : checkbox, radio bouton, liste déroulante

## caractère quantitatif

Les caractères quantitatifs sont ceux que l'on peut **mesurer** par un
nombre. Il en existe deux types

### les caractères quantitatifs discréts

Cela signifie que les valeurs mesurées sont séparées les une des autres
et les valeurs intermédiaires n'ont pas de sens.

Par exemple, le nombre d'enfants par famille, le nombre de cases
parcourures, …

On représente ce type de caractère par un diagramme en barre avec axe
des abscisses.

ihm : spin button, radio, …

### caractères quantitatifs continus

Dans ce cas, les caractères peuvent prendre n'importe quelle valeur dans
un **intervalle**.

Par exemple, la taille, le poids, les temps d'exécution,

On représente ce type de caractère par

  - soit un histogramme : on regroupe les valeurs dans des **classes**,
    qui sont des intervalles, puis on trace des rectangle de largeur la
    largeur de l'intervalle et dont l'**aire** est proportionnel à
    l'effectif de la classe… il n'y a donc en théorie pas d'axe des
    ordonnées.

  - soit le polygone de ses effectifs cumulés croissants (ou fréquences)
    
    ihm : slider, curseur

### paramètres d'une série (caractère quantitatif)

Lorsque le caractère est quantitatif, on peut calculer :

  - la moyenne (mean) ;
  - variance / écart-type (standart deviation)
  - la médiane (median)
  - les quartiles \(Q_1\) et \(Q_3\) , l'écart interquartile
    \(e = Q_3-Q_1\)
  - le mode : la valeur la plus fréquente, l'étendue : caratère max -
    caractère min

On résume les quartiles, la médiane et l'étendu dans un diagramme en
boite à moustache, qui permet de comparer des séries entre elles

# la statistique inférentielle

## principe

La notion centrale autour de la statistique inférentielle est celle
d'**échantillon**. Un échantillon est une série de \(n\) caractères
mesurés sur des individus de la population choisis au hasard et sans
remise.

Sous certaines conditions (normalité, indépendance, taille suffisante),
les moyennes ou les proportions des échantillons suivent des modèles
(c'est-à-dire un espace probabilisé).

Par exemple,

  - considérons un tirage aléatoire avec remise d'une boule dans une
    urne contenant 7 rouges et 3 noires ;
  - puis considérons un échantillon de 20 tirages, dans lequel on mesure
    \(f_{20}\) la fréquence de boules rouges ;
  - intuitivement, il y aura dans l'échantillon 14 boules rouges ;
  - si on réalise plusieurs fois l'expérience.

Dans ce cas, la proportion de boules rouges dans chaque échantillon suit
approximativement une loi normale :

``` python
from random import choice
import numpy as np
import matplotlib.pyplot as plt

URNE = 'R'*7+'N'*3
NECHANTILLONS = 1000
N = 20
P = URNE.count('R')/len(URNE)

def tirage() -> str:
    return choice(URNE)

def echantillon(size: int=N) -> tuple[str]:
    return tuple(tirage() for _ in range(size))

def gaussian_density(x, mu, sigma):
    return 1/(sigma * np.sqrt(2 * np.pi)) * \
        np.exp( -(x - mu)**2 / (2 * sigma**2))

x = np.linspace(0, 20, 21)
res = { k: 0 for k in x }
for _ in range(NECHANTILLONS):
    k = echantillon().count('R')
    res[k] += 1
y = [ res[k] / NECHANTILLONS for k in x ]
plt.clf()
plt.bar(x, y)

mu = N*P
sigma = np.sqrt(N*P*(1-P))
x2 = np.linspace(0, 20, 100)
plt.plot(x2, gaussian_density(x2, mu, sigma), color='red')
plt.savefig(fn)
fn
```

![](images/urne_1.png)

Cette adéquation a une loi normale est assez répandue : dès que les
échantillons sont indépendants et suffisamment grands, la moyenne ou la
proportion du caractère étudié suit une loi normale. Cette omni présence
de la loi normale nous amène à pouvoir aborder la question réciproque :
"si je connais le comportement des échantillons, alors que puis-je en
déduire sur la population dont il provient".

Il existe deux types de tests :

  - les tests paramétriques : ils s'utilisent lorsque les données sont
    distribuées, c'est-à-dire qu'elles suivent une distribution connue
    (Normale, Poisson, Binomiale, …)
  - les test non paramétriques ne reposent pas sur une distribution
      - avantages : nécessitent moins d'hypothèses sur la population
      - inconvénients : plus susceptibles d'échouer à rejeter une une
        hypothèse nulle invalide (les tests paramétriques sont plus
        **puissants**)

## Comment choisir le bon test ?

1.  Identification des variables :
    1.  Qualitative
    2.  Quantitative
2.  Identifier les besoins : Veut-on savoir …
    1.  … si des ensembles de résultats sont significativement
        différents ?
    2.  … si les variables sont corrélées ?
    3.  … quelles sont variables qui influencent le résultat ?
3.  Choisir le test

### Question 1 : Y a-t-il une différence entre des séries de mesures non appariées (indépendants) ?

Pour les caractères quantitatifs, il est important de savoir s'ils
suivent une distribution normale. Dans ce cas, des tests paramétriques
sont utilisés. Si on ne peut statuer sur la distribution, alors on
choisira un test non paramétrique.

  - Paramètre quantitatif
      - Si on sait que la distribution est normale, alors test
        paramétrique
          - 2 groupes :test \(t\) de Student
            [2.3.2.1.1](#*non%20apparié)
          - plus de 2 groupes : Analyse de variance (ANOVA)
            [2.3.2.2.1](#*standard)
      - sinon
          - 2 groupes : [2.3.3.2](#*test%20de%20Mann-Whitney-Wilcoxon)
          - plus de 2 groupes :
            [2.3.3.3](#*test%20de%20H%20de%20Kruskall-Wallis)
  - Paramètre qualitatif
      - 2 groupes :
          - test du \(\chi_2\)
            [2.3.3.1.1](#*test%20du%20$\\chi_2$%20de%20contingence)
          - test exact de Fischer
      - plus de 2 groupes
          - test du \(\chi_2\) répété

### Question 2 : Y a-t-il une différences entre des séries de mesures appariées (non indépendantes) ?

  - caractère quantitatif
      - paramétrique :
          - 2 groupes : test \(t\) de Student [2.3.2.1.2](#*apparié)
          - plus de 2 groupes : ANOVA avec mesures répétées
      - sinon :
          - 2 groupes : [2.3.3.4](#*test%20de%20Wilcoxon) matched pair
            signed rank test
          - plus de 2 groupes : [2.3.3.5](#*test%20de%20Friedman)
  - caractère qualitatif
      - 2 groupes : [2.3.3.6](#*test%20de%20Mc%20Nemar)
      - plus de deux groupes : [2.3.3.7](#*test%20Q%20de%20Cochran)

### Question 3 : Les variables sont-elles corrélées ?

  - Caractère quantitatif
      - les variables sont toutes quantitatives : test \(r\) de Pearson
      - sinon :
          - test \(\rho\) de Spearman
          - test \(\tau\) de Kendall
  - Caractère qualitatif
      - tableau de contingence \(2\times 2\) :
          - test de la valeur du risque relatif
          - rapport des chances
      - sinon
          - test du \(\chi_2\) pour la tendance
          - test de regression logistique

## les tests

Les exemples des tests nécessitent :

  - scipy ;
  - statsmodels

que l'on peut installer via `pip`.

### description d'un test

Lors d'un test, on formule deux hypothèses : une hypothèse nulle, notée
\(H_0\) et une hypothèse alternative notée \(H_1\), selon ce que l'on
veut valider. Par exemple :

  - \(H_0\,:\, m_0 = m_1\) et \(H_1\,:\,m_0\neq m_1\)
  - \(H_0\,:\,m_0 \leqslant m_1\) et \(H_1\,:\,m_0 > m_1\)
  - \(H_0\,:\,m_0 \geqslant m_1\) et \(H_1\,:\,m_0 < m_1\)

En général, l'hypothèse nulle signifie que les moyennes des groupes sont
égales, et l'hypothèse alternative signifie que les moyennes sont
différentes ou donne une "direction" au test.

1.  Si \(H_0\) est vraie, alors une on connait la loi d'une certaine
    variable aléatoire.
2.  Sur les échantillons, on peut mesurer la valeur de cette variable :
    il s'agit de la statistique du test \(t_{\rm test}\) (par exemple,
    différence entre les moyennes, …)
3.  Si \(H_0\) est vraie, alors on peut calculer la
    \(p=P(X\leqslant t_{\rm test})\) et cette valeur doit être élevée,
    puisque la valeur \(t_{\rm test}\) est "normale".
4.  On fixe alors la règle de décision suivante :
    1.  Si \(p\) est inférieure au seuil de risque, alors on rejette
        \(H_0\);
    2.  Sinon on ne rejette pas \(H_0\), \(H_0\) est donc considérée
        comme **valide**.

### les tests paramétriques

Les tests paramétriques sont plus "puissants" que les autres tests dans
le sens où si les autres tests rejettent l'hypothèse, alors ils la
rejette également.

Par contre, ils nécessitent que le caractère étudié est la réalisation
d'une variable aléatoire dont on sait qu'elle suit une **loi normale**.

1.  \(t\)-test de Student
    
    1.  non apparié
        
        Les échantillons sont considérés ici comme indépendants : la
        réalisation de l'un n'influence pas celle de l'autre. Par
        exemple, on étudie les résultats de deux classes différentes
        (dont les résultats sont normaux).
        
        ``` python
        from math import sqrt
        import numpy as np
        import scipy.stats as stat
        ALPHA = 0.05
        
        # génére deux échantillons indépendants
        np.random.seed(101)
        notes_classe1 = np.random.normal(9, 16, 50)
        notes_classe2 = np.random.normal(12, 20, 50)
        notes = [notes_classe1, notes_classe2]
        
        # compare les échantillons
        result = stat.ttest_ind(*notes,
                                equal_var=False,
                                alternative="greater")
        # paramètres de ttest_ind :
        # equal_var (booléen) : initialiser à True si les échantillons ont des variances différentes
        # alternative (str) : positionne la direction du test, "two-sided", "less", "greater"
        print(f"la statistique observée est de {result.statistic}")
        print(f"la probabilité que la va soit <= à {result.statistic} est {result.pvalue}")
        print(f"donc, au seuil de risque {ALPHA}", end="")
        if result.pvalue < ALPHA:
            print(", on rejette", end="")
        else:
            print(", on accepte", end="")
        print(" l'hypothèse selon laquelle les moyennes de la classe 1 sont supérieures ou égales à celles de la classe 2")
        ```
    
    2.  apparié
        
        Le test apparié suppose que les variables ne sont pas
        indépendantes : par exemple les résultats des devoirs d'une
        même classe.
        
        ``` python
        from math import sqrt
        import numpy as np
        import scipy.stats as stat
        ALPHA = 0.05
        
        # génére deux échantillons indépendants
        np.random.seed(101)
        notes_dsi = np.random.normal(9, 10, 20)
        notes_dsf = np.array([note + 2 + float(np.random.normal(0, 2, 1))
                                  for note in notes_dsi])
        notes = [notes_dsi, notes_dsf]
        
        # compare les échantillons
        result = stat.ttest_rel(*notes)
        print(f"la statistique observée est de {result.statistic}")
        print(f"la probabilité que la va soit <= à {result.statistic} est {result.pvalue}")
        print(f"donc, au seuil de risque {ALPHA}", end="")
        if result.pvalue < ALPHA:
            print(", on rejette", end="")
        else:
            print(", on accepte", end="")
        print(" l'hypothèse selon laquelle les moyennes du dsi sont égales à celle du dsf")
        ```

2.  ANOVA
    
    1.  standard
        
        On considère la situation suivantes : on a collecté les notes de
        20 étudiants de trois classes différentes.
        
        On veut savoir si la moyenne des trois échantillons est la même
        au seuil de risque de 5%.
        
        Le test ANOVA est possible lorsque les échantillons proviennent
        de populations ayant **la même variance**.
        
        Le test de `levene` peut valider une hypothèse de ce type.
        
        On effectue ensuite le test ANOVA grace à la fonction
        `f_oneway`.
        
        ``` python
        import scipy.stats as stat
        import numpy as np
        ALPHA = 0.05
        
        # crée les échantillons
        np.random.seed(101)
        notes_classe1 = np.random.normal(14, 8, 20)
        notes_classe2 = np.random.normal(9, 8, 20)
        notes_classe3 = np.random.normal(12, 8, 20)
        notes = [notes_classe1, notes_classe2, notes_classe3]
        
        # on peut dessiner les diagrammes en barre
        
        # calcule les variances
        # 1. comparaisons manuelles
        print("\n".join(f"variance de la classe {i+1} : {np.var(notes[i])}"
                       for i in range(3)))
        
        # 2. test de levene pour valider que les variances sont égales
        levene_result = stat.levene(*notes)
        print(f"{levene_result.statistic}, {levene_result.pvalue}.")
        if levene_result.pvalue >= ALPHA:
            print("on ne rejette pas l'hypothèse")
        else:
            print("on rejette l'hypothèse")
        print("selon laquelle les données proviennent de population dont les variances égales")
        
        # 3. test anova
        anova_result = stat.f_oneway(*notes)
        print(f"{anova_result.statistic}, {anova_result.pvalue}.")
        if anova_result.pvalue >= ALPHA:
            print("on ne rejette pas l'hypothèse")
        else:
            print("on rejette l'hypothèse")
        print("selon laquelle les données proviennent de population dont les moyennes sont égales")
        ```
    
    2.  avec mesures répétées

3.  test de Pearson

### les tests non paramétriques

Les test non paramétriques s'utilisent lorsque les échantillons sont des
réalisations de variables aléatoires ne suivant pas une loi normale (ou
dont on ignore la loi).

1.  test du \(\chi_2\)
    
    1.  test du \(\chi_2\) de contingence
        
        On a demandé à 200 élèves de terminale leur bi-spécialité :
        
          - A : Maths-SVT
          - B : Maths-NSI
          - C : Maths-Physique
          - D : Physique-SVT
        
        |         | A  | B  | C  | D  | Total |
        | ------- | -- | -- | -- | -- | ----- |
        | Garçons | 20 | 10 | 40 | 30 | 100   |
        | Fille   | 44 | 6  | 22 | 28 | 100   |
        | Total   | 64 | 16 | 62 | 58 | 200   |
        

        On veut savoir si le choix d'un couplage dépend du genre. Notre
        hypothèse nulle est ici : "le choix est **indépendant** du
        genre".
        
        Pour valider ou rejeter notre hypothèse, on utilise la fonction
        `chi2_contingency`
        
        ``` python
        import numpy as np
        import scipy.stats as stat
        ALPHA = 0.05
        
        donnees = [[20, 10, 40, 30],
                   [44, 6, 22, 28]]
        
        # test du chi 2
        
        result = stat.chi2_contingency(donnees)
        
        print(f"statistique : {result[0]}, p-value : {result[1]}")
        
        if result[1] < ALPHA:
            print("on rejette l'hypothèse")
        else:
            print("on valide l'hypothèse")
        print("selon laquelle le genre n'influence pas le choix d'un couplage")
        ```
    
    2.  test du \(\chi_2\) de Friedmann
        
        Il s'agit d'une alternative du test ANOVA à mesures répétés. Il
        est utilisé pour déterminer s'il existe une différence
        significative entre les moyennes d'au moins trois groupes.
        
        Les données sont appariées et non nécessairement normales.
        
        L'hypothèse nulle est que les moyennes sont toutes égales;
        l'hypothèse alternative est qu'au moins la moyenne d'un des
        groupes diffère des autres.

2.  test de Mann-Whitney-Wilcoxon
    
    Le test de Mann-Whitney-Wilcoxon ou test U de Mann-Whitney est
    utilisé lorsque la distribution n'est pas normale ou lorsque la
    taille des échantillons est petite (\(n <30\) en pratique). Il
    permet de comparer les moyennes de deux groupes.
    
    Là encore l'hypothèse nulle est que la moyenne des deux groupes est
    égale : \(H_0\,;\,m_1 = m_2\). L'hypothèse alternative peut être :
    
      - \(H_1\,:\,m_1\neq m_2\) (two-sided)
      - \(H_1\,:\,m_1 > m_2\) (greater)
      - \(H_1\,:\,m_1 < m_2\) (less)
    
    <!-- end list -->
    
    ``` python
    from math import sqrt
    import numpy as np
    import scipy.stats as stat
    ALPHA = 0.05
    
    # génére deux échantillons indépendants
    # echantillons petits
    # distribution non normale
    notes_classe1 = np.random.randint(5, 15, 20)
    notes_classe2 = np.random.randint(7, 20, 20)
    notes = [notes_classe1, notes_classe2]
    
    # compare les échantillons
    alt = "two-sided"
    result = stat.mannwhitneyu(*notes, alternative=alt)
    print(f"la statistique observée est de {result.statistic}")
    print(f"la probabilité que la va soit <= à {result.statistic} est {result.pvalue}")
    print(f"donc, au seuil de risque {ALPHA}", end="")
    if result.pvalue < ALPHA:
        print(", on rejette", end="")
    else:
        print(", on accepte", end="")
    conclusion = { "two-sided" : "egales",
                   "less": "supérieures",
                   "greater": "inférieures"}
    print(f" l'hypothèse selon laquelle les moyennes de la classe 1 sont {conclusion[alt]} à celles de la classe 2")
    ```

3.  test de H de Kruskall-Wallis
    
    Le test H de Kruskall-Wallis est utilisé lorsque la distribution
    n'est pas normale ou lorsque la taille des échantillons est petite
    (\(n <30\) en pratique) et que l'on dispose de plus de deux
    échantillons. Il permet de comparer les moyennes des groupes.
    
    Là encore l'hypothèse nulle est que les échantillons proviennent de
    groupes dont les moyennes sont égales :
    \(H_0\,:\,\forall i, j\,m_i = m_j\) ; l'hypothèse alternative est
    que certaines de ces moyennes sont (significativement) différentes :
    \(H_1\,:\,\exists i\neq j\,m_i\neq m_j\)
    
    ``` python
    from math import sqrt
    import numpy as np
    import scipy.stats as stat
    ALPHA = 0.05
    
    # génére deux échantillons indépendants
    # echantillons petits
    # distribution non normale
    notes_classe1 = np.random.randint(5, 15, 20)
    notes_classe2 = np.random.randint(10, 20, 15)
    notes_classe3 = np.random.randint(8, 20, 18)
    notes = [notes_classe1, notes_classe2, notes_classe3]
    
    # compare les échantillons
    alt = "two-sided"
    result = stat.kruskal(*notes)
    print(f"la statistique observée est de {result.statistic}")
    print(f"la probabilité que la va soit <= à {result.statistic} est {result.pvalue}")
    print(f"donc, au seuil de risque {ALPHA}", end="")
    if result.pvalue < ALPHA:
        print(", on rejette", end="")
    else:
        print(", on valide", end="")
    print(f" l'hypothèse selon laquelle les moyennes des classes sont égales")
    ```

4.  test de Wilcoxon
    
    Le test de Wilcoxon est utilisé lorsque la distribution n'est pas
    normale ou lorsque la taille des échantillons est petite (\(n <30\)
    en pratique). Il permet de comparer les moyennes de deux groupes
    **appariés** ou **dépendants**.
    
    Là encore l'hypothèse nulle est que la moyenne des deux groupes est
    égale : \(H_0\,;\,m_1 = m_2\). L'hypothèse alternative peut être :
    
      - \(H_1\,:\,m_1\neq m_2\) (two-sided)
      - \(H_1\,:\,m_1 > m_2\) (greater)
      - \(H_1\,:\,m_1 < m_2\) (less)
    
    <!-- end list -->
    
    ``` python
    from math import sqrt
    import numpy as np
    import scipy.stats as stat
    ALPHA = 0.05
    
    # génére deux échantillons appariés
    # echantillons petits
    # distribution non normale
    notes_classe1 = np.random.randint(5, 15, 20)
    notes_classe2 = [ n + float(np.random.normal(0, 3, 1)) for n in notes_classe1 ]
    notes = [notes_classe1, notes_classe2]
    
    # compare les échantillons
    alt = "two-sided"
    result = stat.wilcoxon(*notes, alternative=alt)
    print(f"la statistique observée est de {result.statistic}")
    print(f"la probabilité que la va soit <= à {result.statistic} est {result.pvalue}")
    print(f"donc, au seuil de risque {ALPHA}", end="")
    if result.pvalue < ALPHA:
        print(", on rejette", end="")
    else:
        print(", on accepte", end="")
    conclusion = { "two-sided" : "egales",
                   "less": "supérieures",
                   "greater": "inférieures"}
    print(f" l'hypothèse selon laquelle les moyennes de la classe 1 sont {conclusion[alt]} à celles de la classe 2")
    ```

5.  test de Friedman
    
    Le test de Friedman s'utilise lorsque les échantillons sont des
    réalisations de variables aléatoires dont la distribution est
    quelconque. Ces séries sont appariées et leur nombre est au moins
    trois.
    
    L'hypothèse nulle est que les moyenne des séries sont égales ;
    l'hypothèse alternative est qu'au moins un des échantillons a une
    moyenne différente.
    
    ``` python
    from math import sqrt
    import numpy as np
    import scipy.stats as stat
    ALPHA = 0.05
    
    # génére deux échantillons appariés
    # echantillons petits
    # distribution non normale
    notes_classe1 = np.random.randint(5, 15, 20)
    notes_classe2 = [ n + float(np.random.normal(0, 3, 1)) for n in notes_classe1 ]
    notes_classe3 = [ n + float(np.random.normal(0, 4, 1)) for n in notes_classe1 ]
    notes = [notes_classe1, notes_classe2, notes_classe3]
    
    # compare les échantillons
    result = stat.friedmanchisquare(*notes)
    print(f"la statistique observée est de {result.statistic}")
    print(f"la probabilité que la va soit <= à {result.statistic} est {result.pvalue}")
    print(f"donc, au seuil de risque {ALPHA}", end="")
    if result.pvalue < ALPHA:
        print(", on rejette", end="")
    else:
        print(", on accepte", end="")
    print(f" l'hypothèse selon laquelle les moyennes des trois classes sont égales")
    ```

6.  test de Mc Nemar
    
    Permet de déterminer si les proportions d'un certain caractère
    qualitatif observées dans deux groupe appariés sont égales.
    
    Supposons que l'on veuille savoir si la présentation d'une
    spécialité influence l'opinion qu'en ont les élèves.
    
    On effectue des mesures :
    
      - avant la présentation, 10 élèves ont une bonne opinion de la
        nsi, 40 n'en ont pas une bonne opinion
      - après la présentation, 30 élèves ont une bonne opinion de la
        nsi, 20 n'en ont pas une bonne opinion.
    
    <!-- end list -->
    
    ``` python
    from statsmodels.stats.contingency_tables import mcnemar
    
    data = [[10, 40],
            [30, 20]]
    print(mcnemar(data, exact=False))
    print(mcnemar(data, exact=False, correction=False))
    ```
    
    dans les deux cas (avec ou sans correction de continuité),
    l'hypothèse selon laquelle la proportion d'individu ayant une bonne
    opinion de la spé nsi n'a pas changé ne peut être rejeté : les
    proportions ne sont pas significativement différentes.

7.  test Q de Cochran
    
    Permet de déterminer si les proportions d'un certain caractère
    qualitatif observées dans au moins trois groupes appariés sont
    égales.
    
    On peut l'utiliser par exemple pour déterminer si la proportion
    d'étudiants ayant réussi un test préparé à l'aide de trois méthodes
    d'apprentissage différentes sont égales.
    
    Pour chacun des 20 élèves, on les fait étudier en utilisant une
    méthode, puis on leur fait passer un devoir d'une même difficulté.
    On mesure leur succès à l'examen (1) ou leur echec (0). Les
    résultats sont consignés dans un tableau de 20 lignes et de trois
    colonnes.
    
    ``` python
    from statsmodels.stats.contingency_tables import cochrans_q
    
    datas = [[1, 1, 0], [1, 0, 0], [1, 1, 1], [1, 1, 0], [1, 0, 1], [0, 0, 0],
             [0, 1, 0], [0, 1, 1], [1, 0, 0], [1, 1, 0], [1, 0, 1], [1, 0, 1],
             [0, 1, 1], [1, 0, 0], [0, 1, 0], [0, 1, 1], [0, 0, 1], [0, 1, 1]]
    
    print(cochrans_q(datas))
    ```
    
    Donc l'hypothèse nulle (les proportions sont égales dans les trois
    groupes) ne peut être rejeté : il n'y a aucune preuve significative
    que les techniques d'apprentissage influence la proportion d'élèves
    ayant réussi le test.
