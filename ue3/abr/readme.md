Les Arbres Binaires de Recherche
================================

- lire le [chapitre 12](./abr.pdf) de "Introduction à l'algorithmique" (*Cormen*) ;
- implanter les opération décrites en python.
