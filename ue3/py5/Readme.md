---
title: Découverte du module py5 pour les dessins et animations en Python 
author: |
    | UE C, M1 et M2 MEEF, Univ. Lille 
date: |
    | octobre 2021
    |
    | \protect ![CC-BY](https://licensebuttons.net/l/by/4.0/88x31.png)
lang: fr
---

[py5](http://py5.ixora.io/index.html) est un module Python de dessin
et animations inspiré de [Processing](http://www.processing.org). 
Vous pouvez l'utiliser pour construire des animations ou avec vos élèves
pour une utilisation ludique et créative de la programmation. 

Une toile (_sketch_) est créée. Des fonctions permettent de dessiner
dedans en mode soit statique, soit interactif, soit animé. Le résultat
peut être exporté dans un fichier de dessin.

Ce module doit être importé à l'aide de l'instruction : `import py5`{.py}
pour éviter certains conflits.

Le code pour produire le dessin doit être écrit dans la définition 
de 3 fonctions :
- `settings()` : fonction appelée une seule fois au début du programme 
pour initialiser les variables et options du dessin (exemple : taille de la toile) ;
- `setup()` : fonction appelée une seule fois, après `settings`, pour
réaliser un dessin statique et définir certaines options du dessin 
(une exception JAVA est lévée si l'option n'est pas dans la bonne
fonction);
- `draw()` : fonction appelée après les 2 autres, en boucle avec une 
fréquence qui peut être définie dans la fonction `setup()`. Elle permet de
créer des animations et des dessins interactifs.

La ligne suivante, mise à la fin du programme, lance l'appel des 3 fonctions
précédentes : `py5.run_sketch(block=False)`{.py}

Vous trouverez dans ces 3 fichiers des exemples de code pour 3 typees d'oeuvres :

- un [dessin statique](exemple_dessin_statique.py)
- une [animation](exemple_animation.py) qui génère un gif animé.
- un [dessin interactif](exemple_dessin_interactif.py)


Obersvez ces programmes pour comprendre comment fonctionne py5 et découvrir
quelques fonctions. 

Les figures dessinées sur une toile de taille (largeur, hauteur) 
sont placées à l'aide de coordonnées (x, y) où (voir figure ci-dessous) :

- x est la position horizontale (abscisse) de 0 (à gauche) à largeur (à droite)
- y la position verticale (ordonnée) de 0 (en haut) à hauteur (en bas).

![Repère py5](repere.png)

Inspirez vous des codes fournis et consultez la [documentation](http://py5.ixora.io/reference/sketch.html), 
afin de créer un dessin static puis l'animer. Vous pouvez dès maintenant
travailler sur votre animation.

