# Exemple de dessin statique avec py5
# auteure : Maude Pupin
# Inspiré de l'exemple construit pour L Décodent l'@venir
# octobre 2021


import py5

# Fonction appelée 1 fois, au début du programme
# Initialisation des variables et options du dessin
def settings():
    py5.size(400,500)    # taille de la toile: largeur=500, hauteur=400

# Fonction appelée 1 fois, après settings
# Dessin statique
def setup():

    py5.background(255) # Fond blanc - niveaux de gris si 1 seul paramètre

    py5.rect(20, 10, 40, 50)    # rectangle en (x=20,y=10) largeur=40, hauteur=50

    py5.stroke(233, 100, 150)  # Couleur du pinceau
    py5.stroke_weight(5)  # Epaisseur du trait
    py5.line(200, 120, 300, 170)        # ligne de (xdeb=200,ydeb=120) à (xfin=300,yfin=170)

    py5.triangle(30,210,90,100,130,200) # triangle entre les points (x1=30,y1=210), (x2=90,y2=100) et (x3=130,y3=200)


    py5.no_stroke()          # Pas de contour
    py5.fill(150, 100, 233)  # Couleur de remlissage des formes
    py5.ellipse(200, 50, 30, 20)  # ellipse centrée en (x=200,y=50), rayon horizontal=30, rayon vertical=20

    py5.text_size(30)                  # taille du texte : 30
    py5.text("We love code!",150,250) # We love code! s'affiche en commençant à la position (x=150,y=250)

# Lance les fonctions py5
py5.run_sketch()

