# Exemple d'animation avec py5
# auteure : Maude Pupin
# octobre 2021

import py5, py5_tools

largeur = 1000
hauteur = 1000
x = 10
y = 10
change = True

# Fonction appelée 1 fois, au début du programme
# Initialisation des variables et options du dessin
def settings():
    py5.size(largeur, hauteur)          # taille de la toile


# Fonction appelée 1 fois, après settings
# Dessin statique
def setup():
    # Nombre d'images affichées par seconde, à utiliser dans setup
    py5.frame_rate(30)

    # Un texte statique, options par défaut
    py5.text("We love code!",largeur/2, hauteur/2)


# Fonction appelée ensuite, en boucle, selon la fréquence
# de l'animation définie dans les options.
# Par défaut, 60 images par seconde sont affichées
def draw():
    global x, y, change
    
    x = (x + 10) % largeur
    y = (y + 35) % hauteur
    py5.fill(x%250, x%250, y%250)
    py5.text("We love code!", x, y) # We love code! s'affiche en commençant à la position x=150,y=250

    if change:
        change = False
        py5.text_size(30)
    else:
        change = True
        py5.text_size(15)

# Lance les fonctions py5
# block=False : les instructions qui suivent le run sont exécutées,
# pendant que le sketch est affiché
py5.run_sketch(block=False)

# Crée un fichier gif animé appelé animated.gif dans le répertoire courant
# avec 10 captures du sketch prises toutes les 1 sec,
# et le gif a une fréquence d'affichage de la nouvelle image de 0.5 sec
py5_tools.animated_gif('animated.gif', 10, 1, 0.5)



