# Exemple de dessin interactif avec py5
# auteure : Maude Pupin
# Inspiré d'exemples de la documentation de py5
# octobre 2021

import py5

largeur = 1000
hauteur = 1000

# Fonction appelée 1 fois, au début du programme
# Initialisation des variables et options du dessin
def settings():
    py5.size(largeur, hauteur)          # taille de la toile
    
def setup():
    py5.background(204)
    
def draw():
    py5.line(py5.mouse_x, 20, py5.mouse_x, 80)
    if py5.is_mouse_pressed and py5.mouse_button == py5.LEFT:
        py5.stroke(255, 0, 0) # Pinceau rouge
        py5.line(py5.mouse_x, py5.mouse_y-20, py5.mouse_x, py5.mouse_y+30)
    else:
        py5.stroke(255)  # Pinceau blanc

# Lance les fonctions py5
py5.run_sketch()
