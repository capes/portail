"""
Ajustement linéaire par la méthode de Mayer
"""

def point_moyen(Mi: list[tuple]) -> tuple[float]:
    """
    Renvoie les coordonnées du point moyen.
    """
    n = len(Mi)
    return (sum(M[0] for M in Mi)/n,
            sum(M[1] for M in Mi)/n)

def mayer(xi: list[float], yi: list[flot]) -> tuple[float]:
    """
    Renvoie les coefficients de la droite d'ajustement de Mayer.
    """
    Mi = [(xi[i], yi[i]) for i in range(len(xi))]
    n = len(Mi)
    Mi.sort()
    xa, ya = point_moyen(Mi[:n//2])
    xb, yb = point_moyen(Mi[n//2:])
    a = (yb - ya) / (xb - xa)
    b = ya - a*xa
    return a, b
