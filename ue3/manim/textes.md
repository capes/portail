Les textes
==========

Il existe plusieurs classes permettant de représenter et d'animer des
chaines de caractères :

  - la classe `Text` ;
  - la classe `MarkupText` , qui utilise un format propre à Pango ;
  - les classes `Tex` et `MathTex` qui utilisent le format LaTeX.

# La classe `Text`

La classe `Text` hérite de `SVGMobject`. Le programme **Pango** est
utilisé pour produire l'affichage à partir de la chaîne ; ce programme
utilise les polices de caractères installées sur le système.

La commande shell suivante permet d'afficher la liste des polices de
caractères :

``` bash
python3 -c "import manimpango; from pprint import pprint; pprint(manimpango.list_fonts())"
```

On peut ensuite filter les résultats :

``` bash
python3 -c "import manimpango; from pprint import pprint; pprint(manimpango.list_fonts())" | grep -i arial
```

# La classe `Text`

## Création

La classe `Text` héritant de `SVGMobject`, toutes les méthodes des
objets sont utilisables : `stroke_width`, `fill_opacity`, …

Le paramètre nommé `font` permet de spécifier la police à utiliser à la
construction :

``` python
from manim import *

class TextCreation1(Scene):
    def construct(self):
        t = Text("Hello Méef", font="Arial Black")
        self.add(t)
```

## indiçage

Les objets de la classe `Text` sont des `VGroup`. Ils sont donc
indiciables : L'élément d'indice \(i\) d'un objet `t` représentant la
chaine `ch` est l'objet représentant le caractère `ch[i]`. Ainsi, si
`t=Text(ch)`, alors `t[i]` représente `ch[i]`.

``` python

class TextIndice(Scene):
    def construct(self):
        t = Text("Hello Méef", font="Arial")
        t[0].set_color(RED)
        t[3].set_color(TEAL)
        self.add(t)
```

## interligne

Si un texte contient plusieurs lignes, la valeur de l'interligne peut
être modifié grace au paramètre `line_spacing` :

``` python

class TextInterLigne(Scene):
    def construct(self):
        t = Text(
            """
            Un algorithme glouton constuit une solution pour un problème en
            effectuant une série de choix. Pour chaque décision à prendre, le
            choix qui semble le meilleur à l'instant est effectué.
            """,
            line_spacing = 1.3 )
        t.width = config.frame_width - 1
        self.add(t)
```

L'attribut width permet de modifier la largeur du texte, et de
l'initialiser à la largeur de l'image.

## style et poids

Le style d'une police peut être spécifié grace au paramètre `slant`. Ce
dernier peut prendre les valeurs NORMAL, ITALIC ou OBLIQUE :

``` python

class TextSlant(Scene):
    def construct(self):
        t = Text("""
        Un algorithme glouton constuit une solution pour un problème en
        effectuant une série de choix. Pour chaque décision à prendre, le
        choix qui semble le meilleur à l'instant est effectué.
        """,
                 line_spacing=1.3,
                 slant=ITALIC)
        t.width = config.frame_width - 1
        self.add(t)
```

les différents poids peuvent peuvent être obtenu à l'aide du module
`manimpango` : Ce dernier contient un `enum` `Weight` fournissant
l'ensemble des poids.

``` python

class TextWeights(Scene):
    def construct(self):
        import manimpango as mp
        weight_list = sorted( mp.Weight, key = lambda w: w.value )
        grp = VGroup(*[Text(w.name, weight=w.name, font="Open Sans")
                       for w in weight_list]).arrange(DOWN)
        grp.height=config.frame_height - 1
        self.add(grp)
```

## utilisation des tranches

Les paramètres :

  - `t2c` (text to color) ;

  - `t2f` (text to font) ;

  - `t2g` (text to gradient) ;

  - `t2w` (text to weight);

  - `disable_ligature` permettent de modifier l'apparence du texte, ou
    d'une partie du texte.
    
    Les quatre premiers s'associe à un dictionnaire spécifiant l'attibut
    à modifier en fonction de la tranche (sous forme de chaîne de
    caractères). Le dernier est un paramètre booléen permettant de
    (des)activer les ligatures.

<!-- end list -->

``` python
class TextSlices(Scene):
    def construct(self):
        grp = VGroup(
            # Text to color
            Text("Hello",t2c={"[1:-1]": BLUE}),
            Text("World",t2c={"rl": RED}),
            # Text to font
            Text("Manim",t2f={"an": "Open Sans"}),
            Text("Manim",t2f={"[2:-1]": "Open Sans"}),
            # Text to gradient
            Text("Hello",t2g={"[1:-1]": (RED,GREEN)}),
            Text("World",t2g={"World": (RED,BLUE)}),
            # Text to slant
            Text("Manim",t2s={"an": ITALIC}),
            Text("Manim",t2s={"[2:-1]": ITALIC}),
            # Text to weight
            Text("Manim",t2w={"an": THIN}, font="Open Sans"),
            Text("Manim",t2w={"[2:]": HEAVY}, font="Open Sans"),
            # Ligature
            Text("fl ligature",font_size=40),
            Text("fl ligature", disable_ligatures=True, font_size=40),
        ).arrange_in_grid(cols=2).scale(1.4)

        self.add(grp)
```

# La classe `MarkupText`

La classe `MarkupText` utilise une synthaxe proche de **html** pour
modifier le texte.

``` python
class CreateMarkupText(Scene):
    def construct(self):
        text = MarkupText(
            f'Normal <i>Italic</i> <b>Bold</b> <u>Underline</u> <span foreground="{BLUE}">Blue text</span>')
        self.add(text)
```

La [documentation
officielle](https://docs.manim.community/en/stable/reference/manim.mobject.text.text_mobject.MarkupText.html#manim.mobject.text.text_mobject.MarkupText)
de manim fournit davantage d'informations sur les manipulations
autorisées.

# La classe `Tex`

La classe `Tex` permet de créer des images de textes en utilisant le
logiciel LaTeX. Les images sont compilées en utilisant un source latex.

Le constructeur de cette classe prend en charge deux paramètres nommés :

  - `tex_template` permet d'utiliser un autre modèle que celui par
    défaut. Par défaut, `manim` utilise ce modèle :

<!-- end list -->

``` tex
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
```

La classe `TexTemplate` permet de créer des modèles personnalisés :

``` python
my_template = TexTemplate()
my_template.add_to_preamble(r"\newcommand{\st}[2]{{\tt S}_{\rm #1}^{\rm #2}}")
# Create a new subclass with your template
class MyTex(Tex):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tex_template=my_template, **kwargs)

class TestMyTexTemplate(Scene):
    def construct(self):
        text = MyTex("$\st{sub-index}{super-index}$").scale(3)
        self.add(text)
```

  - `tex_environment` permet de spécifier l'environnement à utiliser :

<!-- end list -->

``` python
class MyTex(Tex):
    def __init__(self, *args, j_width=4, **kwargs):
        super().__init__(*args,
                         tex_environment=f"\\begin{{tabular}}{{p{{ {j_width} cm}}}}",
                         **kwargs)

class TestEnviroment(Scene):
    def construct(self):
        TEX = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
        t1 = MyTex(TEX)
        t2 = MyTex(TEX, j_width=6)
        t3 = MyTex(TEX, j_width=9)
        grp = VGroup(t1,t2,t3).arrange(DOWN)
        grp.height = config.frame_height - 1
        self.add(grp)
```

## `Tex` comme des tableaux

Comme `Text`, les objets de la classe `TeX` sont des container :

``` python
def construct(self):
    t1 = Tex("Hello world!")
    #              |  Notez l'espace
    #              v
    t2 = Tex("Hello ","world","!") 
    grp = VGroup(t1,t2).arrange(DOWN,aligned_edge=LEFT)
    self.add(grp)
```

# `MathTex`
