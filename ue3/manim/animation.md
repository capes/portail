Les animations
==============

Les animations sont des objets qui, lorsqu'ils sont fournis à la méthode
`play`, permettent de générer des animations vidéos.

Toutes les animations héritent de la classe `Animation`.

# Hiérarchie des animations

![](media/images/anim_hierarchy.png)

# La méthode `play`

La méthode `Scene.play` perend en paramètre une ou plusieurs animations.

Les animations sont regroupées en quatre types :

  - les créations du module `creation` qui permettent d'animer
    l'apparition ou la disparition d'un objet. Les animations suivantes
    sont disponibles :
      - `Write` / `UnWrite` : permet d'afficher un texte en simulant son
        écriture ;
      - `AddTextLetterByLetter` : permet d'afficher un texte lettre par
        lettre ;
      - `Create` / `Uncreate` : apparition , disparition ;
      - `DrawBorderThenFil` : affiche les contours, puis remplit
        l'intérieur ;
      - `SipralIn` : apparition spiralée ;
      - `ShowIncreasingSubsets` : affiche les sous-objets un par un de
        manière incrémental ;
      - `ShowSubmobjectsOneByOne` : affiche les objets un par un et
        séparément.

<!-- end list -->

``` python
from manim import *

class CreationModule(Scene):
    def construct(self):
        squares = [Square() for _ in range(7)]
        creations = [ Write, AddTextLetterByLetter, Create,
                      Uncreate, DrawBorderThenFill,
                      ShowIncreasingSubsets, ShowSubmobjectsOneByOne ]
        VGroup(*squares[0:4]).set_x(0).arrange(buff=1.9).shift(UP)
        VGroup(*squares[4:7]).set_x(0).arrange(buff=2.6).shift(2 * DOWN)
        names = [ Text(creations[i].__name__,
                       font_size=(22 if i > 4 else 24)).next_to(squares[i], UP)
                  for i in range(len(squares)) ]
        self.add(*squares, *names)
        texts = [ Text("manim", font_size=29).move_to(squares[i])
                  for i in range(2) ]
        self.add(*texts)
        objs = [ ManimBanner()\
                 .scale(0.25)\
                 .move_to(squares[i+2].get_center()) for i in range(5) ]
        self.add(*objs)
        self.play(
            *[ creations[i]( texts[i] if i in (0, 1) else objs[i-2] )
               for i in range(7) ],
            run_time = 3 )
        self.wait()
```

**à faire** : lire le code précédent et prédire son effet, puis vérifier
en l'exécutant.

  - les apparitions / disparitions : `FadeIn` / `FadeOut` du module
    `fading` ;

  - les aggrandissements du module `growing`
    
      - `GrowFromPoint`
    
      - `GrowFromCenter`
    
      - `GrowArrow`
    
      - `SpinInFromNothing`

**à faire** : lire le code précédent et prédire son effet, puis vérifier
en l'exécutant.

  - les indications qui permettent d'animer la mise en valeur d'un
    objet. Les animations suivantes sont disponibles :
      - `ApplyWave` : effet de vague ;
      - `Circumscribe` : circonscription par un rectangle ;
      - `Focus` : effet lumineux ;
      - `FocusOn` : effet circulaire ;
      - `Indicate` : changement temporaire de taille et de couleur ;
      - `ShowPassingFlash` : souligne en utilisant une autre forme ;
      - `Wiggle` : gigotage.

Voici un exemple d'utilisation des transformations :

[file:./media/videos/animation/720p30/Indications.mp4](./media/videos/animation/720p30/Indications.mp4)

**à faire** : réaliser cette animation. **conseil** : les
`AnimationGroup` sont des groupes d'animation permettant d'effectuer
plusieurs animations dans le même temps.

  - les transformations (`Transform`) qui permettent de modifier un
    objet.
