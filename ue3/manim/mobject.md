attributs des objets
===================

# Position relative

## `to_edge`

  - La méthode `to_edge` aligne un coté de l'objet avec un bord de
    l'écran.
  - L'espace entre le bord de l'écran et celui de l'objet peut être
    modifié grace au paramètre nommé `buff`

## `to_corner`

Cette méthode prend en paramètre un vecteur, et déplace l'objet dans le
coin correspondant de l'écran.

## `next_to`

Cette méthode permet de placer un objet relativement à un autre. Son
entête est :

`Mobject.next_to(REFERENCE_MOBJECT_OR_POINT, DIRECTION, buff=BUFFER,
aligned_edge=EDGE)`

Par exemple,

``` python
from manim import *

class SceneNextTo(Scene):
    def construct(self):
        ref = Rectangle()
        red_dot = Dot(color=RED)
        blue_dot = Dot(color=BLUE)
        green_dot = Dot(color=GREEN)
        t = Text("Mon texte")
        red_dot.next_to(ref, LEFT)
        blue_dot.next_to(ref, LEFT, buff=0)
        green_dot.next_to(ref, DR, buff=0)
        t.next_to(ref, DOWN, aligned_edge=LEFT)

        self.add(ref, red_dot, blue_dot, green_dot, t)
```

**à faire** supprimer le paramètre `aligned_edge`, le modifier à `RIGHT`
et observer les changements.

## `align_to`

La méthode `align_to` permet d'aligner un bord d'un objet par rapport au
même bord d'un autre, en effectuant une translation horizontale ou
verticale, sans changer les autres attributs.

Par exemple, le rectangle créé sera aigné à droite avec le cercle :

``` python
class SceneAlignTo(Scene):
    def construct(self):
        c = Circle()
        c.move_to(RIGHT * 3 + UP * 1.5)

        r = Rectangle()
        r.align_to(c, RIGHT)

        self.add(c, r)
```

Cette méthode fonctionne également avec les coins :

``` python
class SceneAlignToCorner(Scene):
    def construct(self):
        r = Rectangle()
        c.move_to(RIGHT * 3 + UP * 1.5)

        t = Text("Hello")
        t.align_to(r, UR)

        self.add(r, t)
```

# Largeur et hauteur

## attributs `width` et `height`

Les attributs `width` et `height` peuvent être modifié directement. De
plus, les objets en trois dimensions disposent d'un attribut `depth`.

Par exemple,

``` python
class WidthAndHeightScene(Scene):
    def construct(self):
        c = Circle()
        r = Rectangle()

        c.width = 3
        r.width = 3
        self.add(r, t)
```

**à faire** remplacer `width` par `height` et observer

Une autre manière d'affecter ces attributs est d'utiliser la méthode
`set` :

``` python
class SetScene(Scene):
    def construct(self):
        c = Circle()
        r = Rectangle()

        c.set(width=3)
        r.set(width=3)
        self.add(r, t)
```

## Étirement horizontal, vertical

Lors d'une modification d'une largeur ou d'une hauteur, les proportions
de l'objet ne sont en général pas conservées. Les méthodes
`stretch_to_fit_width` et `stretch_to_fit_height` permettent de modifier
respectivement la largeur et la hauteur d'un objet tout en conservant
les proportions.

Par exemple,

``` python
class StretchScene(Scene):
    def construct(self):
        c = Circle()
        t = Triangle()
        r = Rectangle()

        t.stretch_to_fit_height(c.height)
        r.stretch_to_fit_width(c.width)

        t.move_to(c.get_center())

        self.add(c, r, t)
```

# transformations

## Agrandissement/Réduction (Homothétie)

``` python
class ScaleScene(Scene):
    def construct(self):
        c1 = Circle(color=RED)
        c2 = Circle(color=WHITE)
        c2.scale(2)
        c3 = Circle(color=BLUE)
        c3.scale(3)
        c4 = Circle(color=GREEN)
        c4.scale(1/3)

        self.add(c1, c2, c3, c4)
```

## modification matricielle

Toute transformation peut être représentée par une matrice de dimension
\(3\times 3\) agissant sur les coordonnées homogènes.

Par exemple, la matrice :

```math
\left(
\begin{matrix} 
cos  θ & -sin θ & 0  \\
sin θ & cos θ & 0  \\
0 & 0 & 1
\end{matrix} \right)
```

permet d'effectuer une rotation. La matrice

```math
\left(
\begin{array}{ccc} 
1 & tan θ & 0 \\
0 & 1 & 0  \\
0 & 0 & 1
\end{array}
\right)
```

est celle d'une transvection

``` python
class MatrixScene(Scene):
    def construct(self):
        sq_w = Square()
        sq_r = Square(color=RED)

        ANGLE = PI / 6
        POINT = sq.get_corner(DL)

        matrix = [
            [1, np.tan(ANGLE), 0],
            [0, 1, 0],
            [0, 0, 1]
        ]

        sq_r.apply_matrix(matrix, about_point=POINT)
        self.add(sq_w, sq_r)
```

# attributs des `VMobject`

Si tous les objets de manim disposent des attributs et méthodes
précédentes, les `VMobject` disposent d'attributs spécifiques. Les
`VMobject` sont des courbes, et ils disposent d'attributs et de méthodes
permettant d'affecter les paramètres du tracé de cette courbe.

## couleur

La couleur des `VMobject` peut être spécifiée par :

  - une chaîne contenant la valeur hexadécimale représentant la couleur
    (triplet \((r, g, b)\) ).
  - une liste contenant le triplet `[r, g, b]` dont les valeurs sont
    dans l'intervalle \([0,1]\).

Par exemple,

``` python
from manim import *

class ColorsShowScene(Scene):
    def construct(self):
        red_dot = Dot(color=RED).scale(4).to_edge(UP)
        blue_e_dot = Dot(color=BLUE_E).scale(4).to_edge(DOWN)
        hex_dot = Dot(color="#FE298D").scale(4).to_edge(LEFT)
        rgb_dot = Dot(color=rgb_to_color([0.2,0.9,0])).scale(4)

        self.add(
            red_dot, blue_e_dot,
            hex_dot, rgb_dot
        )
```

## Largeur du tracé, remplissage et opacité

``` python
from manim import *

class StrokeOptions(Scene):
    def construct(self):
        background_square = Square(
            fill_opacity=1,
            fill_color=WHITE,
        )
        background_square.scale(1.5)

        circle = Circle(
            # options du tracé
            stroke_width=20,
            stroke_color=TEAL,
            stroke_opacity=0.5, # 0 <= stroke_opacity <= 1
            # options de remplissage
            fill_opacity=0.5,   # 0 <= fill_opacity <= 1
            fill_color=ORANGE
        )


        self.add(
            background_square,
            circle
        )
```

## Points, début et fin

Les `VMobject` sont des courbes de bézier. Ces dernières sont des
courbes définies par des *points de controle*. Ces points peuvent être
visualisés facilement :

``` python

class ControlPointScene(Scene):
    def construct(self):
        c = Circle()
        # c.points sont les points de controle
        for p in c.points:
            d = Dot().move_to(p)
            self.add(d)

        self.add(c)
```

Les points peuvent également être modifiés :

``` python

class ModControlPointScene(Scene):
    def construct(self):
        c = Circle()
        c.points[4] += LEFT

        for p in c.points:
            d = Dot().move_to(p)
            self.add(d)

        self.add(c)
```

Souvent, il peut être utile de récupérer les coordonnées de début et/ou
de la fin d'un tracé. Ceci peut être réalisé facilement en utilisant :

  - la liste `.points`
  - les méthodes `get_start()` et `get_end()`

<!-- end list -->

``` python

class StartEndScene(Scene):
    def construct(self):
        arrow = Arrow(LEFT,UR)
        arrow.shift(LEFT+DOWN)

        arrow_start = arrow.get_start() # similaire à arrow.points[0]
        arrow_end   = arrow.get_end()   # similaire à arrow.points[-1]

        dot_start = Dot(color=RED).move_to(arrow_start)
        dot_end   = Dot(color=BLUE).move_to(arrow_end)

        self.add(arrow, dot_start, dot_end)
```

# Dupliquer les objets

Les objets `Mobject` peuvent être dupliqués en utilisant la méthode
`m.copy()` cette dernière crée un nouvel objet de même type et
d'attributs égal à l'objet `m`.

``` python

class ObjectCopyScene(Scene):
    def construct(self):
        original_circle = Circle(
            radius=2,
            stroke_color=PINK,
            stroke_width=30,
            stroke_opacity=0.4,
            fill_opacity=0.6,
            fill_color=ORANGE
        )
        original_circle.to_edge(LEFT)

        copy_circle = original_circle.copy()
        copy_circle.to_edge(RIGHT)
        copy_circle.set_color(RED)
        copy_circle.set_stroke(color=TEAL,width=50,opacity=1)
        copy_circle.set_fill(color=PURE_BLUE,opacity=1)

        another_copy_circle = copy_circle.copy()
        another_copy_circle.move_to(ORIGIN)

        another_copy_circle.set_style(
            stroke_width=30,
            stroke_color=WHITE,
            stroke_opacity=0.5,
            fill_color=PURE_GREEN,
            fill_opacity=0.3,
        )
```

**Exercices**

1.  Créer une grille permettant d'afficher les coordonnées
    ![GridScene<sub>ManimCEv0</sub>.16.0.post0.png](media/images/grid/GridScene_ManimCE_v0.16.0.post0.png)
2.  Dessiner le symbole du Yin-Yang
    ![](media/images/yinyang/YinYang_ManimCE_v0.16.0.post0.png)

# Hiérarchie des objets

Voici un diagramme de la hiérarchie de classe des `VMobject` et des
`VGroup`:

## objets élémentaires

Les classes héritantes de `VMobject` sont des courbes. D'autres objets
peuvent (comme des images bitmap) ne sont pas des courbes.

![](media/images/vmobject_hierarchy.png)

## objets composés

Les `VGroup` sont des objets composés de plusieurs `VMobject`.

![](media/images/vgroup_hierarchy.png)
