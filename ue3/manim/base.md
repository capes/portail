`ManimCE` se décline en trois version, chacune d'entre elle utilisant un
moteur de rendu différent :

  - PyCairo ;
  - OpenGL ;
  - WebGL.

Les deux dernières versions n'étant pas encore stables, nous utiliserons
celle utilisant `PyCairo`.

# comment `ManimCE` fonctionne ?

  - manim est une bibliothèque python permettant de créer des animations
    sous forme de vidéos ;
  - il ne s'agit pas d'un outil généraliste de création : au contraire,
    les objets que l'on peut manipuler sont de nature essentiellement
    géométrique ;

ManimCE + PyCairo fonctionne de la manière suivante :

  - python et le module `manim` convertissent les appels aux fonctions
    et méthodes en appels `PyCairo` et construisent l'animation image
    par image.
  - Lorsque toutes les images sont générées, `Manim` utilise `ffmpeg`
    pour les assembler dans une vidéo.

En schéma :

![](media/images/manim.png)

# installation de manim

`manim` peut s'installer dans votre répertoire utilisateur en utilisant
`pip` :

``` bash
pip install manim
```

cette commande installe le module python dans le répertoire
`$HOME/.local/lib/python3.xx/site-package/manim`

**à faire** : utiliser la commande shell `tree` pour visualiser
l'arborescence du module.

un script exécutable `manim` est également installé dans le répertoire
`$HOME/.local/bin`

# créer une première animation

Pour créer une nouvelle animation, il faut :

1.  créer un fichier python dans votre éditeur favori, par exemple
    `mon_animation.py`.

2.  importer l'ensemble des classes de la bibliothèque `manim` :
    
    ``` python
    from manim import *
    ```

3.  créer une classe héritant de la classe `Scene`
    
    ``` python
    class MyScene(Scene):
    ```

4.  le code permettant de spécifier l'animation doit être placé dans la
    méthode abstraite `construct`. Voici un exemple d'animation :

<!-- end list -->

``` python
from manim import Scene, Circle

class MyScene(Scene):
    def construct(self):
        # define Mobjects
        circle = Circle()
        # Scene.add  adds MObject instantly
        self.add(circle)
        self.wait()
```

Ce programme se contente de :

  - créer un Cercle ;
  - l'ajouter à l'animation ;
  - attendre (1 seconde par défaut).

## compiler l'animation

### dans un terminal en ligne de commande

le module `manim` fournit un exécutable du même nom permettant de créer
l'animation.

Il prend en paramètres :

  - le nom du fichier python : `mon_animation.py` par exemple
  - le nom de la classe contenant l'animation à générer : On peut placer
    dans un fichier python plusieures animations.

Plusieurs options peuvent modifier le comportement du binaire :

  - `-q` permet de spécifier la qualité de l'animation. Par exemple
    `-qm` générera une animation de qualité `médium`.
  - `-p` permet de jouer l'animation immédiatement après sa création
  - `-v WARNING` permet d'inhiber les avertissements
  - `-s` permet de ne générer que la dernière image de l'animation
    (utile pour intégrer dans un document)

Par exemple :

``` bash
manim src/manim/mon_animation.py MyScene -q m -v WARNING 
```

**attention** l'exécutable `manim` se situe dans le répertoire `~/.local/bin`. 
La commande précédente suppose que ce répertoire est présent dans la
variable d'environnement `$PATH`. Si ce n'est pas le cas, il faut l'ajouter
en exportant une nouvelle variable :

```bash
$ export PATH=$PATH:~/.local/bin
```

### emplacement des fichiers

La commande `manim` crée plusieurs fichiers dans le répertoire `media` 

``` bash
tree media
```

On y retrouve un emplacement pour les images générées et un dossier
portant le nom du fichier python. Ce dernier contient un fichier vidéo
du nom de la classe fournie en argument, et qui contient l'animation. :

![](./media/videos/mon_animation/720p30/MyScene.mp4)

### avec jupyter

*à rédiger*

### en tant que module

il est possible d'inclure les commandes de création de l'animation
directement dans le code python grace aux modules `os` et `pathlib` :

``` python
from manim import Scene, Circle
from pathlib import Path
import os

FLAGS = "-qm -p"
SCENE = "MyScene"


class MyScene(Scene):
    def construct(self):
        # define Mobjects
        circle = Circle()
        # Scene.add  adds MObject instantly
        self.add(circle)
        self.wait()


if (__name__ == "__main__"):
    script_name = f"{Path(__file__).resolve()}".replace(" ", r"\ ")
    os.system(f"manim {script_name} {SCENE} {FLAGS}")
    print(script_name)
```

# utiliser les Mobject

## hiérarchie des `Mobject`

Les objets de la classe `Mobject` sont les objets pouvant être affichés
à l'écran dans l'animation. La classe `Mobject` est abstraite, `manim`
fournit donc des classes spécialisées :

![](media/images/manim_object.png)

Nous allons expliciter les classes suivantes :

  - `VMobject` : il s'agit d'une classe permettant de créer des courbes
    (de Bézier) ;
  - `ImageMobject` : classe permettant de créer des images matricielles
    (matrice de pixels)
  - `Group` : container permettant de disposer des `Mobject` (de tous
    types).
  - `VGroup` : container permettant de disposer uniquement des
    `VMobject`.

Dans cette partie, nous allons nous utiliser le plus souvent des
sous-classes de la classe `VMobject`. Il s'agit :

  - des lignes (`Line`) ;

  - des cercles (`Circle`);

  - des carrés (`Square`);

  - des textes (`Text`);

  - …
    
    Pour générer des textes sous forme d'image, `manim` utilise le
    programme `LaTeX` présent sur la machine pour générer une
    représentation du texte sous forme d'image au format `svg`, puis
    cette image est transformée en courbe `VMobject`.

## Ajouter des `Mobject` à l'écran

L'ajout d'un objet à une animation peut s'effectuer :

  - soit en utilisant la méthode `add` de la classe `Scene` ; l'ajout
    est alors effectué directement à l'écran de la scène. Cet ajout n'a
    pas de durée. Par exemple, le code suivant :
    
    ``` python
    from manim import Scene, Square
    
    class SceneWithoutDuration(Scene):
        def construct(self):
            sq = Square()
            self.add(sq)
    ```
    
    S'il est compilé avec les options `-pql` , il ne produira pas de
    fichier vidéo, car l'animation n'a pas de durée. Par contre l'option
    `-ps` produira une image
    
    ``` bash
    manim src/manim/scene_without_duration.py SceneWithoutDuration -ps
    ```

  - soit en utilisant une animation …

## Les animations

Les animations peuvent être créées de deux manières différentes dans
`manim` :

  - Soit en utilisant la méthode `play` de la classe `Scene` ;
  - Soit en utilisant des `updaters`. Les `updaters` seront présentés
    ultérieurement.

### La méthode `play`

La méthode `Scene.play` prend en argument une ou plusieurs animations.
Ces dernières peuvent être

  - Soit des méthodes de la classe `Mobject` vues comme des animations ;
  - Soit des objets héritants de la classe abstraite `Animation`.

<!-- end list -->

1.  Les différentes animations
    
    Les objets animations sont divisés en quatre groupes :
    
    1.  Les animations du module `création` permettent d'animer l'ajout
        d'un objet ;
    2.  Les animations du module `indication` permettent d'attirer
        l'attention sur un objet ;
    3.  les animations du module `transform` permettent de modifier la
        forme ou la position d'un objet. On y trouve les animations des
        déplacements, des mises à l'échelle et des rotations ;
    4.  Les animations du module `remove` permettent d'animer la
        suppression d'un objet.

2.  Attributs des animations
    
    Les animations possèdent toutes les attributs suivants :
    
      - `run_time` *(float)* : durée de l'animation ;
      - `rate_func` *(function)* variation de la vitesse de l'animation.
        Pour la plupart des animations, la fonction utilisée est
        `smooth`, mais on peut également utiliser les fonctions
        suivantes :
          - `linear`
          - `rush_into`
          - `rush_from`
          - `slow_into`
          - `double_smooth`
          - `there_and_back`
          - `there_and_back_with_pause`
          - `running_start`
          - `not_quite_there`
          - `wiggle`
          - `squish_rate_func`
      - `remover` *(bool)* `True` si et seulement si l'objectif de
        l'animation est la suppression d'un objet. L'attribut `remover`
        des animations `FadeOut` et `Uncreate` est égal à `True`.
      - `Mobject` *(Mobject)*. L'objet sur lequel s'exerce l'animation.
        Certaines animations ne peuvent utiliser que des `VMobject`,
        comme par exemple `write` ; d'autre accepte tous les `Mobject`
        (comme `FadeIn`).
    
    Il y a quelques règles à connaitre préalablement à l'utilisation de
    `Scene.play`. La première est que l'on ne peut animer deux fois un
    même objet dans le même appel à `play`.
    
    Dans l'exemple suivant, seule une animation à la fois est possible :
    
    ``` python
    
    
    class BasicAnimation(Scene):
        def construct(self):
            text = Text("Hello Tim")
            self.play(Write(text), FadeIn(text))
            self.wait()
    ```
    
    La méthode `Scene.play` permet d'animer plusieurs objets en même
    temps :
    
    ``` python
    
    
    class MultipleAnimationAtSameTime(Scene):
        def construct(self):
            """Construct a new scene."""
            square = Square()
            circle = Circle()
            self.play(Create(square), FadeIn(circle))
            self.wait()
    ```
    
    Certains attributs peuvent être initialisés à la construction de
    l'animation. C'est le cas de `run_time` et de `rate_func`. L'exemple
    suivant crée une animation durant 3 seconde avec une accélération
    linéaire.
    
    ``` python
    
    
    class ChangeDuration(Scene):
        def construct(self):
            """Construct a new animation."""
            self.play(Create(Circle()),FadeIn(Square()),
                      runt_time=3,
                      rate_func=linear)
            self.wait()
    ```
    
    avec plusieurs animations :
    
    ``` python
    
    
    class ChangeDurationMultipleAnimations(Scene):
          def construct(self):
              self.play(
                  Create(
                      Circle(),
                      run_time=3,
                      rate_func=smooth
                  ),
                  FadeIn(
                      Square(),
                      run_time=2,
                      rate_func=there_and_back
                  ),
                  GrowFromCenter(
                      Triangle()
                  )
              )
              self.wait()
    ```
    
    ``` bash
    manim src/manim/basics.py ChangeDurationMultipleAnimations -pql 
    ```

# attributs des Mobjects

## Dimensions de la caméra

Avant d'étudier les attibuts de la classe `Mobject`, attardons nous sur
les caractéristiques de la caméra. La caméra permet de représenter un
ensemble d'objets situées dans l'espace en les projetant sur un écran.

Par défaut, cet écran est un rectangle de 8 unités de haut et de rapport
16/9. Ces 8 unités sont indépendantes de la résolution finale de
l'animation (480p, 720p, …) permettant ainsi d'abstraire cette
résolution.

Les coordonnées des objets sont des tableaux de longueur 3. Le point de
coordonnées `[0, 0, 0]` se projette au centre de l'écran.

Pour l'instant, nous n'utiliserons pas la 3ieme dimension, seulement les
coordonnées \(x\) et \(y\).

## attributs de position

Tous les objets de la classe `Mobject` possèdent les attributs suivants
:

  - Position
  - Width
  - Height
  - Z Index

Commençons par créer un rectangle :

![](media/images/attributes/AttributesRectangle_ManimCE_v0.16.0.post0.png)

### position

manim fournit quelques constantes permettant de positionner les objets :

  - `ORIGIN` : il s'agit des coordonnées de l'origine ;
  - vecteurs unitaires et leurs opposés :
      - `UP`
      - `DOWN`
      - `LEFT`
      - `RIGHT`
  - vecteurs "diagonaux" :
      - `UR` : Up Right
      - `UL` : Up Left
      - `DR` : Down Right
      - `DL` : Down Left

À sa création, les coordonnées d'un objet sont toujours initialisées à
l'origine.

Deux systèmes de coordonnées permettent de modifier la position :

  - position absolue : utilise le système de coordonnées de la caméra.
      - `Mobject.move_to()`
      - `Mobject.shift()`
  - position relative : utilise un autre objet ou des coordonnées comme
    origine.
      - `Mobject.to_edge()`
      - `Mobject.to_corner()`
      - `Mobject.next_to()`
      - `Mobject.align_to()`

<!-- end list -->

1.  position absolue
    
    ``` python
    
    
    class AttributesMoveToSimple(Scene):
        def construct(self):
            rec = Rectangle()
            rec.move_to([-3, 2, 0])
            self.add(rec)
    ```
    
![](media/images/attributes/AttributesMoveToSimple_ManimCE_v0.16.0.post0.png)
    
En général, il est conseillé de spécifier les coordonnées en
utilisant le type `array` de `numpy` ou d'utiliser une combinaison
linéraire des vecteurs constants.
    
``` python


class AttributesMoveMultiple(Scene):
    def construct(self):
        r = Rectangle()
        c = Circle()
        e = Ellipse()
        # bonne pratique
        r.move_to( np.array([-3, 2, 0]))
        # ou alors
        c.move_to(LEFT * 3 + UP *2)
        e.move_to(UL * 2 + LEFT)
        self.add(r, c, e)
```

``` bash
manim src/manim/attributes.py AttributesMoveMultiple -qm -s
```
    
![AttributesMoveMultiple](media/images/attributes/AttributesMoveMultiple_ManimCE_v0.16.0.post0.png)
    
La méthode `shift` permet de translater l'objet dans une direction
fournie en paramètre :

``` python


class AttributesShift(Scene):
    def construct(self):
        s = Square()
        c = Circle()
        for _ in range(4):
            s.move_to(RIGHT)

        for _ in range(4):
            c.shift(RIGHT)

        self.add(s, c)
```
    
![AttributesShift](media/images/attributes/AttributesShift_ManimCE_v0.16.0.post0.png)
    
*explications* : sur les quatre positionnements du carré, trois sont
redondants. Par contre, chacun des quatre appels à la méthode
`shift` modifie les coordonnées du cercle.

*à faire* : réaliser une animation illustrant ce principe.

Les méthodes suivantes permettent d'accéder aux attributs de
positions :

  - `get_center()` : renvoie les coordonnées du centre de l'objet ;
  - `get_right()` : renvoie les coordonnées d'un point situé sur le
    bord droit de l'objet ;
  - `get_left()`
  - `get_top()`
  - `get_bottom()`
  - `get_corner()` : renvoie les coordonnées d'un coin de l'objet.
    Les arguments autorisés pour cette méthode sont `UR, UL, DR,
    DL`.

*à faire* : réaliser cette animation
    
![](media/videos/attributes/720p30/AttributesEveryAnchor.mp4)

2.  position relative à un autre objet
    
    `to_edge()` : Cette méthode permet d'aligner un objet avec un bord
    de l'écran. Par exemple, l'animation suivante produit un rectangle
    situé sur le côté gauche de l'écran (le bord gauche de l'objet est
    disposé près du bord gauche de l'écran).
    
``` python


class AttributesAlignToEdge(Scene):
    def construct(self):
        r = Rectangle()
        self.add(r)
        r.to_edge(LEFT)
```
    
![](media/images/attributes/AttributesAlignToEdge_ManimCE_v0.16.0.post0.png)
