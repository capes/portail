Les groupes
===========

Il est souvent nécessaire de grouper plusieurs objets pour leur
appliquer des traitements.

Par exemple, pour translater plusieurs objets, il est bien sûr possible
d'itérer sur une structure séquentielle contenant ces objets. Toutefois,
appliquer la translation au groupe contenant ces objets sera plus
rapide, aura davantage de sens et le groupe pourra être utilisé pour
plusieurs opérations.

Les `Group` peuvent contenir n'importe quels objets `Mobject`. Par
contre, les `VGroup` ne peuvent contenir que des `VMobject`.

# `Group`

Les objets de la classe `Group` implantent les méthodes des `Mobject`,
comme : `shift`, `scale`, `move_to`, `next_to`, `align_to`, … Les
méthodes spécifiques aux `VMobject` , comme `set_stroke` ou `set_fill`
ne sont par contre implantées que par les `VGroup`.

Deux méthodes sont spécifiques aux groupes : `arrange` et
`arrange_in_grid`. Elles permettent respectivement de disposer les
objets les uns par rapport aux autres en séquence ou en grille.

# dispositions

Prenons l'exemple d'un carré, d'un cercle et d'un triangle :

``` python
class GroupWithoutGroup(Scene):
    def construct(self):
        figures = [Square(color=PURPLE),
                   Circle(),
                   Triangle(),
                   Rectangle(color=GREEN)]
        self.add(*figures)
        self.wait()
```

![](media/images/groups/GroupWithoutGroup_ManimCE_v0.16.0.post0.png)

Ces éléments peuvent être groupés, et des opérations peuvent être
appliquées à ce groupe :

``` python
class GroupOperationsExample(Scene):
    def construct(self):
        figures = [Square(color=PURPLE),
                   Circle(),
                   Triangle(),
                   Rectangle(color=GREEN)]
        group = Group(*figures).scale(.6).move_to(2*UP)
        self.add(group)
        self.wait()
```

![](media/images/groups/GroupOperationsExample_ManimCE_v0.16.0.post0.png)

Les objets du groupes peuvent être disposés verticalement :

``` python
class GroupArrangeVertEx1(Scene):
    def construct(self):
        figures = [Square(color=PURPLE),
                   Circle(),
                   Triangle(),
                   Rectangle(color=GREEN)]
        group = Group(*figures).scale(.6).move_to(2*UP)
        group.arrange(DOWN)
        group2 = group.copy()
        group2.arrange(DOWN, buff=0)
        group.move_to(2*LEFT)
        group2.move_to(2*RIGHT)
        self.add(group, group2)
        self.wait()
```

![](media/images/groups/GroupArrangeVertEx1_ManimCE_v0.16.0.post0.png)

Par défaut, l'alignement est effectué au mileu, mais cela peut être
modifié grace au paramètre `aligned_edge` :

``` python
class GroupArrangeVertEx2(Scene):
    def construct(self):
        figures = [Square(color=PURPLE),
                   Circle(),
                   Triangle(),
                   Rectangle(color=GREEN)]
        group = Group(*figures).scale(.6).move_to(2*UP)
        group.arrange(DOWN, aligned_edge=LEFT)
        group2 = group.copy()
        group2.arrange(DOWN, buff=0, aligned_edge=RIGHT)
        group.move_to(2*LEFT)
        group2.move_to(2*RIGHT)
        self.add(group, group2)
        self.wait()
```

![](media/images/groups/GroupArrangeVertEx2_ManimCE_v0.16.0.post0.png)

De même, les objets peuvent être disposés horizontalement (`LEFT`,
`RIGHT`) et alignés par leur bordure haute ou basse.

La dernière méthode de disposition est la la méthode `arrange_in_grid`
qui permet de disposer les objets selon une grille :

``` python
class GroupArrangeInGrid(Scene):
    def construct(self):
        figures = [Square(color=PURPLE),
                   Circle(),
                   Triangle(),
                   Rectangle(color=GREEN)]
        group = Group(*figures).scale(.6).move_to(2*UP)
        group.arrange_in_grid(rows = 2)
        self.add(group)
        self.wait()
```

![](media/images/groups/GroupArrangeInGrid_ManimCE_v0.16.0.post0.png)

``` python
class GroupArrangeInGridCols(Scene):
    def construct(self):
        figures = [Square(color=PURPLE),
                   Circle(),
                   Triangle(),
                   Rectangle(color=GREEN)]
        group = Group(*figures).scale(.6).move_to(2*UP)
        group.arrange_in_grid(cols = 3)
        self.add(group)
        self.wait()
```

# utilisation d'expressions en compréhension

L'utilisation d'expressions en compréhension permet de simplifier le
code. La plupart des méthodes prennent en effet un nombre variable de
paramètre, comme `add`.

Comment utiliser une liste en compréhension comme argument d'une telle
fonction ?

Le problème est que la fonction attend plusieurs arguments d'un certain
type, et pas une liste les contenant.

La solution à ce problème est l'utilisation de l'opérateur `*`. Lorsque
ce dernier est utilisé comme préfixe d'une liste ou d'un tuple, il
déplie les valeurs de l'itérable et les fournit comme arguments à la
fonction.

Par exemple supposons que l'on veuille afficher à l'aide de `print` les
chaînes contenues dans une liste `jours`, nous pouvons utiliser
l'instruction `print(*jours)` :

``` python
jours = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi']
print(*jours)
```

**à faire** : Générer une grille de \(12\times 20\) cercles en (4 lignes
max.)

![](media/images/groups/ManyCircleInGrid_ManimCE_v0.16.0.post0.png)\]
