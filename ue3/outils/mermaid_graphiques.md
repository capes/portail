`mermaid` permet de créer facilement des graphiques pour les intégrer dans
des documents, essentiellement `html` (via un javascript) et `md` (grace
aux filtres pandoc).

Voici un petit tour d'ensemble des type de graphiques disponibles dans
manin

# commentaires

une ligne commençant par deux signes `%` : `%%` sera ignorée.

# graphes

Le mot-clef `flowchart` permet de déclarer un graphe ou un diagrame de
flot.

Une option permet de spécifier la direction du diagramme : `TD` pour du
haut (Top) en bas (Down) ; LR pour gauche (Left) à Droite (Right) …

Les sommets sont déclarés par un identifiant. Le contenu du sommet est
par défaut l'identifiant, mais on peut fournir une autre valeur en
l'encadrant par des crochets immédiatement placés après l'identificateur
du noeud :

![](images/compil.png)

## formes des sommets

On peut spécifier la forme des sommets, en remplaçant les crochets
`[...]` par :

  - des parenthèses `(...)` : rectangles arrondis ;
  - des crochets encadrés de parenthèses `([...])` : forme ovoïde ;
  - deux crochets `[[...]]` ;
  - des parenthèses encadrées de crochets `[(...)]` : cylindre (bdd) ;
  - deux (ou trois) parenthèses `((...))` : forme circulaire (doublée) ;
  - noeud asymétrique : `>...]`
  - une paire d'accolades `{...}` : un losange ;
  - deux paires d'accolades `{{...}}` : un hexagone ;
  - des barres obliques encadrées par des crochets :
      - `[/.../]` , `[\...\]` : un parallélogramme,
      - `[\.../]` , `[/...\]` : des trapèzes ;

## types d'arc

les arcs peuvent être :

  - orientés `A-->B` ou non `A---B`
  - contenir un texte `A-- tokenizer --B` ou `A-->|tokenizer| B`
  - pointillés : `A-.->B`
  - épais : `A ==> B`

les extrémités possibles :

  - `A --o B` ; `A o--o B`

  - `B --x C` ; `B x--x C`

  - `A --> B` ; `A <--> B`
    
    La longueur des arcs peut être contrôlée en ajoutant des caractères
    :

![](images/tant_que.png)

observez la longueur de l'arc allant vers le noeud `E`.

## sous-graphe

des sous-graphes peuvent être obtenus grâce à la syntaxe suivante :

> subgraph NOM direction DIR … (contenu du sous graphe) end

où NOM est l'identificateur du sous-graphe et DIR sa direction (TD, LR,
…)

Les sous-graphes ainsi déclarés peuvent par la suite être considérés
comme des sommets et reliés par des arcs : `sous1 --> sous2`

## fontawesone

Certaines icônes de
[fontawesome](https://fontawesome.com/search?f=classic&o=r) sont
disponibles :

![](images/fa_graphe.png)

# diagramme de classe :

Les diagrammes de classe permettent d'illustrer une modélisation
orientée objet. Ces diagrammes permettent d'inclure les propriétés, les
méthodes et les relations entre les objets.

Le mot-clef `classDiagram` permet de déclarer un diagramme de classe :

## définir une classe

Une classe est définie :

  - explicitement grace au mot-clef `class`
  - implicitement grâce à une relation : `Vehicle <|-- Car`

## membres d'une classe

les attributs et les méthodes d'une classe peuvent être déclarés :

  - en préfixant l'attribut ou la méthode par le nom de la classe, suivi
    d'une espace , de deux points `:` et d'une espace.
  - en les encadrant par une paire d'accolades `{...}` qui suit la
    déclaration de la classe :

![](images/classes_member.png)

Les méthodes sont distinguées des attributs par l'utilisation de
parenthèses. Les type du retour des méthodes sont simplement séparées
une espace du nom de la méthode.

La visibilité des attributs et des méthodes peut être spécifiée en
précédant la déclaration par :

  - `+` : publique
  - `-` : privé
  - `#` : protected
  - `~` : interne au paquetage

Les méthodes abstraites sont obtenues en ajoutant une étoile `*`
**après** la déclaration de la méthode.

Les méthodes statiques sont obtenues en ajoutant `$` **après** la
déclaration de la méthode.

## Types génériques

Les types génériques sont obtenus en encadrant le paramètre du type par
des tilde `~`.

Par exemple `class List~Shape~`

## relations

Les relations entre deux classes sont déclarées en utilisant la syntaxe

`nom_classe_A ["card1"] type_relation ["card2"] nom_classe_B [:label]`

où `nom_classe_A` et `nom_classe_B` sont les noms de deux classes et ou
les zones entre crochets sont optionnelles.

Les types de relations possibles sont

| `<\vert--` | héritage          |
| ---------- | ----------------- |
| `*--`      | composition       |
| `o--`      | agrégation        |
| `-->`      | association       |
| `--`       | lien (continu)    |
| `..>`      | dépendance        |
| `..\vert>` | réalisation       |
| ..         | lien (pointillés) |

![](images/class_links.png)

Les cardinalités sont optionnelles et encadrées par des guillemets
double.

## Annotations de classes

Les annotations peuvent précéder le nom d'une classe. Quatre valeurs
sont acceptées :

  - `<<Interface>>` une interface
  - `<<Abstract>>` une classe abstraite
  - `<<Service>>` un service
  - `<<Enumeration>>` une énumération

Elles peuvent être fournies dans le bloc entre accolades définissant la
classe, ou en tant que préfixe d'une classe.

![](images/class_annotations.png)

# Camemberts

`mermaid` ne permet de représenter énormément de types de graphiques
statistiques. Il ne permet que de dessiner des camemberts.

La syntaxe est simple (consulter le source)

``` mermaid
pie showData
title Où les chats dorment-ils ?
"Dans leur panier" : 2
"Par terre" : 2
"Sur le canapé" : 10
"Sur un plaid" : 7
"Sur votre pull noir" : 79
```

# autres types de graphiques

`mermaid` permet de produire d'autres types de graphiques :

  - diagramme de séquence ;
  - diagramme d'état ;
  - diagramme entité/relation ;
  - planning journalier ;
  - gantt ;
  - diagramme de dépendance ;
  - diagramme git ;
  - diagramme C4C.
