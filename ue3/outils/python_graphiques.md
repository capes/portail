Utiliser python pour produire ses graphiques
============================================

le sous-module python `pyplot` du module `matplotlib` est suffisant pour
couvrir la plupart des besoins de tracé.

# installation

Le module `matplotlib` est disponible via `pip` :

``` bash
pip install matplotlib
```

installera la dernière version dans votre répertoire utilisateur.

# Exemples de tracés

## nuage de points

La fonction `scatter` permet de tracer des nuages de points (scatter
plot). On peut spécifier la couleur, le type de points utilisé et les
paramètres du tracé :

``` python
plt.clf()
x = [1, 2, 3, 4, 5]
y1 = [1, 2, 3, 4, 5]
y2 = [1, 4, 9, 16, 25]
y3 = [25, 16, 9, 4, 1]
plt.scatter(x, y1,
            s = 130,
            color = 'yellow',
            marker = '*',
            edgecolors = 'green')
plt.scatter(x, y2,
            s = 50,
            color = 'red',
            marker = '+',
            linewidth = 3)
plt.scatter(x, y3,
            s = 50,
            color = 'cyan',
            marker = 'o',
            edgecolors = 'none')
plt.title('trois nuages de points')
fn = 'images/scatter.png'
plt.savefig(fn)
fn
```

le paramètre `s` permet de spécifier la taille des symboles, `marker`
spécifie le symbole utilisé, `color` sa couleur et `edgecolors` la
couleur du bord du symbole.

## courbe

La fonction `plot` permet de tracer une ou plusieurs courbe sur le même
graphique. Cette fonction prend en paramètre un itérable sur les valeurs
des abscisses, ainsi qu'un ou plusieurs itérables donnant les valeurs
des ordonnées.

Ces itérables peuvent être des listes python, des tableaux `numpy` ou
encore des `dataframe` de `pandas`.

Le code suivant réalise le tracé de la courbe de la fonction sinus :

``` python
import numpy as np
import matplotlib.pyplot as plt
import sys

x = np.linspace(-4, 4, 100)
y = np.sin(x)
z = np.log(x)
plt.clf()
plt.plot(x, y, label="sinus")
plt.plot(x, z, label="log")
plt.xlabel("x")
plt.ylabel("y")
plt.title("deux graphiques")
plt.legend(loc="upper left")
plt.savefig(f, format='png', dpi=100, transparent=True)
f
```

## en barre

Les diagrammes en barre permettent d'afficher des séries pour lesquelles
les valeurs de l'axe horizontal sont des modalités qualitatives ou
quantitative discrètes.

### exemple

``` python
barWidth = 0.4
y1 = [1, 2, 4, 3]
y2 = [3, 4, 4, 3]
r1 = range(len(y1))
r2 = [x + barWidth for x in r1]

plt.clf()
plt.bar(r1, y1,
        width = barWidth,
        color = ['yellow' for i in y1],
        edgecolor = ['blue' for i in y1],
        linewidth = 2)
plt.bar(r2, y2,
        width = barWidth,
        color = ['pink' for i in y1],
        edgecolor = ['green' for i in y1],
        linewidth = 2)
plt.xticks([r + barWidth / 2 for r in range(len(y1))],
           ['A', 'B', 'C', 'D'])
plt.savefig(fn)
fn
```

### superposition de deux séries

On utiliise le paramètre `bottom` pour spécifier la valeur de départ :

``` python
barWidth = 0.8
y1 = [1, 2, 4, 3]
y2 = [3, 4, 4, 3]
r = range(len(y1))

plt.clf()
plt.bar(r, y1, width = barWidth,
           color = ['yellow' for i in y1],
           edgecolor = ['blue' for i in y1],
           linestyle = 'solid',
           hatch ='/',
           linewidth = 3)
plt.bar(r, y2, width = barWidth,
           bottom = y1,
           color = ['pink' for i in y1],
           edgecolor = ['green' for i in y1],
           linestyle = 'dotted',
           hatch = 'o',
           linewidth = 3)
plt.xticks(range(len(y1)), ['A', 'B', 'C', 'D'])
plt.savefig(fn)
fn
```

### barres horizontales

on utilise la fonction `barh`, puis on spécifie la hauteur avec `height`
et la valeur de départ avec `left`.

``` python
barWidth = 0.8
y1 = [1, 2, 4, 3]
y2 = [3, 4, 4, 3]
r = range(len(y1))
plt.clf()
plt.barh(r, y1, height = barWidth,
         color = ['yellow' for i in y1],
         edgecolor = ['blue' for i in y1],
         linestyle = 'solid',
         hatch ='/',
         linewidth = 3)
plt.barh(r, y2, height = barWidth,
         left = y1,
         color = ['pink' for i in y1],
         edgecolor = ['green' for i in y1],
         linestyle = 'dotted',
         hatch = 'o',
         linewidth = 3)
plt.yticks(range(len(y1)), ['A', 'B', 'C', 'D'])
plt.savefig(fn)
fn
```

## boîte à moustache

``` python
plt.clf()
plt.subplot(121)
plt.boxplot([[1, 2, 3, 4, 5, 13],
             [6, 7, 8, 10, 10, 11, 12],
             [1, 2, 3]])
plt.ylim(0, 14)
plt.title('boxplot avec sequence')

plt.subplot(122)
plt.boxplot(np.array([[1, 2, 3],
                      [2, 7, 8],
                      [1, 3, 10],
                      [2, 5, 12]]))
plt.ylim(0, 14)
plt.title('boxplot avec array 2d')

plt.savefig(fn)
fn
```

## histogramme

Les histogrammes sont utilisés lorsque l'axe des abscisses, quantité
mesurée dans la série, est un caractère quantitatif continu.

### exemple simple

``` python
plt.clf()
x = [1, 2, 2, 3, 4, 4, 4, 4, 4, 5, 5]
plt.hist(x, range = (0, 5), #intervalle
        bins = 5, # nombre de barres
        color = 'lightgray',
        edgecolor = 'black')
plt.xlabel('valeurs')
plt.ylabel('nombres')
plt.title('Exemple d\' histogramme simple')
fn = 'images/histogramme1.png'
plt.savefig(fn)
fn
```

### histogrammes côte à côte

``` python
plt.clf()
x1 = [1, 2, 2, 3, 4, 4, 4, 4, 4, 5, 5]
x2 = [1, 1, 1, 2, 2, 3, 3, 3, 3, 4, 5, 5, 5]
bins = [x + 0.5 for x in range(0, 6)]
plt.hist([x1, x2], bins = bins,
         color = ['yellow', 'green'],
         edgecolor = 'red',
         hatch = '\\',
         label = ['x1', 'x2'],
         histtype = 'bar') # bar est le defaut
plt.ylabel('valeurs')
plt.xlabel('nombres')
plt.title('2 series')
plt.legend()
fn = 'images/histogramme2.png'
plt.savefig(fn)
fn
```

## camembert

La fonction `pie` permet de tracer des camemberts. Ses paramètres :

  - `normalize` (booléen) permet de dessiner un camembert complet si la
    somme des valeurs n'est pas égale à 1.
  - `colors` : liste des couleurs
  - `labeldistance` : distance des labels au centre
  - `explode` : permet d'éloigner certaines part d'une fraction du rayon
  - `autopct` : fonction associant au pourcentage d'une part son
    affichage. Par exemple `autopct=lambda x: str(round(x, 2)) + "%"`
  - `pctdistance` : distance du pourcentage précédent au centre.
  - `shadow` : (booléen) permet d'afficher une ombre

<!-- end list -->

``` python
plt.clf()
plt.figure(figsize = (8, 8))
x = [1, 2, 3, 4, 10]
plt.pie(x, labels = ['A', 'B', 'C', 'D', 'E'],
           colors = ['red', 'green', 'yellow'],
           explode = [0, 0.2, 0, 0, 0],
           autopct = lambda x: str(round(x, 2)) + '%',
           pctdistance = 0.7, labeldistance = 1.4,
           shadow = True)
plt.legend()
fn = "images/pie.png"
plt.savefig(fn, dpi=100)
fn
```

# Données externes

Voici un exemple d'utilisation avec des données externes, contenues dans
un fichier `csv`.

Le fichier de données est un fichier contenant des notes d'étudiants
fictifs. Il a été généré à l'aide du module `faker` :

``` python
from faker import Faker
from random import gauss

fake = Faker('fr_FR')

NMARK = 5
NSTUDENTS = 10

def row() -> str:
    """
    renvoie une nouvelle ligne du fichier
    """
    identity = [ fake.last_name(), fake.first_name() ]
    marks = [ str(int(gauss(20, 6)) / 2) for _ in range(NMARK) ]
    return ";".join(identity + marks)

with open("datas/notes.csv", 'wt') as f:
    f.write("nom;prénom;" +
            ";".join(f"ds{i}" for i in range(NMARK)) + '\n')
    for _ in range(NSTUDENTS):
        f.write(row() + "\n")
```

le fichier est chargé grace à pandas :

``` python
import pandas as pd 
data = pd.read_csv('datas/notes.csv', sep=";")
# les notes obtenues au ds 1 :
n = len(data)
for i in range(n):
    print("{} a obtenu {} au ds1".format( data['prénom'][i],
                                          data['ds1'][i]))

# résultats du ds1
ds1 = data['ds1']
print("ds1 : moyenne = {}, min = {}, max = {}".format(ds1.mean(),
                                                      ds1.min(),
                                                      ds1.max()))
```

**à faire** représenter les résultats obtenus au ds 1 par un
histogramme.

**à faire** représenter les résultats d'un étudiant sous forme de
diagramme en barre.

# intégration avec un Makefile

L'intégration des outils précédents dans un `Makefile` ne pose pas de
problème particulier :

  - définir les noms des fichiers à générer dans une variable
    `MPLOT_DIAGRAMS_PNG` Pour éviter les confusions, il est préférables
    que ces fichiers soient tous dans un répertoire qui leur est dédié,
    par exemple `diagrams/python`
  - ajouter cette variable à la liste des pré-requis à satisfaire pour
    la cible `png`
  - ajouter une règle pour produire la cible `diagrams/python/%.png` ;
    cette règle a pour pré-requis les éventuels fichiers de données
    (bien rangés \!), et appelle le ou les scripts python (à ranger
    également dans un dossier spécifique) permettant leur création.
