installation de mermaid
=======================

Mermaid est un outil permettant de créer des schéma en en fournissant
une description textuelle. Il est écrit en javascript et peut être
intégré à une page web. Lorsqu'il s'agit de produire un document d'une
autre nature, il convient de l'installer sur la machine chargée de
compiler le document.

# Installation en tant que filtre pour pandoc

si les diagrammes sont **inclus directement dans** les documents sources
au format markdown, et que l'on utilise `pandoc` pour effectuer la
conversion vers un autre format, on peut installer le filtre mermaid
pour pandoc. Ce filtre est disponible en tant que paquet pour `node`. Il
faut avoir l'utilitaire `npm` (node package manager) pour réaliser
l'installation :

``` bash
npm i -g mermaid-filter
```

Cette commande installe le filtre dans le répertoire de l'utilisateur
courant ( `~/.npm-packages/lib`).

> La commande `npm` n'est pas installée sur les machine du FIL.   
> L'installation dans l'espace utilisateur est possible via un script fourni. 
> Voir la documentation sur l'intranet du FIL à [intranet.fil.univ-lille.fr/2020/04/09/nodejs-et-npm/](https://intranet.fil.univ-lille.fr/2020/04/09/nodejs-et-npm/). 

Après installation de ce filtre, on peut convertir des documents `.md` contenant des
figures mermaid.

```` markdown
Mon document
===========

# une première figure

```{.mermaid height=5%}
classDiagram
A <|-- B 
A : int size
A : equals()
A : getSize()

B : getSize()
```

# une deuxième figure

```mermaid
pie 
    title quel animal préférez-vous ?
    "chien" : 52
    "chat" : 45
    "hamster" : 2
    "ne se prononce pas" : 1
```
````

puis :

``` bash
pandoc -F mermaid-filter -o testmd.pdf testmd.md
```

# en tant que commande

`mermaid` peut également être utilisé en tant que programme indépendant,
permettant de produire une image à partir d'un fichier contenant le code
de la figure.

la commande d'installation :

``` bash
npm i -g @mermaid-js/mermaid-cli
```

cette commande installe l'utilitaire `mmdc` dans le répertoire
`~/.npm-packages/bin/`.

``` bash
~/.npm-packages/bin/mmdc -h
```

Un fichier mermaid peut alors être compilé. Par exemple, le fichier
`loop.mermaid`

``` mermaid
flowchart TD
    A[i = 0] --> B{i < n ?}
    B -->|oui| C[instructions]
    C --> D[i = i+1] --> B
    B ---->|non| E[suite du code]
```

peut être compilé :

``` bash
~/.npm-packages/bin/mmdc -i loop.mermaid -o loop.svg
```

Cette commande génère un fichier `loop.svg`

On peut alors intégrer la commande dans le Makefile et ajouter des
règles pour la production des figures mermaid.
