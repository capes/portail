UE C - Recherche
================

Présentation
------------

L’UEC est une partie essentielle de la formation du Master MEEF,
notamment lors de la deuxième année car vous devrez rédiger un mémoire
ayant à la fois un contenu disciplinaire et de recherche et un lien
avec votre pratique professionnelle.
  
Propositions de sujets
-----------------------
Nous avons classé les sujets en les groupant par thèmes (liste non exhaustive) :

premier groupe :

* Réseaux de neurones (avec ou sans deep learning)
* Apprentissage par renforcement
* Ordinateur qui apprend à jouer (go, echecs)
* Surfaces développables : calculs exacts et approchés (application : 
  création de maquettes)
* Calculabilité
* Machine de Turing
* Complexité
* Problème de l'arrêt
* Architecture von Neumann
* Automates (finis)
* P = NP
* Multiplication matricielle et complexité

second groupe :

* PageRank
* Impression 3D
* Les paradigmes de programmation
* Interprète et compilateur
* Labyrinthe (algo de Pledge)
* Reconnaissance d'images
* Traduction automatique
* Compression d'images
* Compression vidéo

troisième groupe :

* Vote électronique
* Informatique et enjeux environnementaux
* Open data
* Informatique et vie privée/démocratie
* Les biais dans les jeux de données utilisés en intelligence artificielle

quatrième groupe :

* Comment enseigner la récursivité ?
* Comment enseigner les structures de contrôle ?
* Analyse des traces produites lors des activités de programmation
* Ordinateurs sans électronique
* Informatique sans ordinateur (informatique débranchée)

Modalités
---------

* En M1 :

    * Au S1 :
      1. réaliser deux synthèse/activités autour de deux thèmes imposés

    * Au S2 :
      1. choisir un sujet de recherche
      2. Réaliser trois fiches de lecture autour de trois articles scientifique autour du thème
      3. Définir une problématique et le plan du mémoire 
      4. Rédiger le rapport d'étape

* En M2 :

    * Au S3
      1. Rédiger l'état de l'art en s'appuyant sur les fiches de lecture
      2. Concevoir les activités en fonction de :
         
         - la problématique 
         - la classe et le tuteur ;-)
         - le programme

    * Au S4
      1. Expérimenter avec les élèves
      2. Présenter les résultats d'expérimentation
      3. Analyser les résultats

