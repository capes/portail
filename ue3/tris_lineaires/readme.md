Les tris en temps linéaires
===========================

Dans le cadre de l'uec, nous vous proposons une lecture d'un
[chapitre](./tri_lineaire.pdf) du "Cormen" traitant des tris linéaires.

Ce chapitre est extrait de l'ouvrage "Introduction à l'algorithmique" de
*Cormen, Leiserson et Rivest*. Il s'agit d'un ouvrage de référence en
algorithmique.

**à faire**

  - Lire le document en entier ;
  - En réaliser une synthèse ;
  - Choisir un des tris abordés ;
  - Illustrer le tri choisi par un exemple (schéma et/ou animation) ;
  - Implanter le tri et effectuer des mesures ;
  - valider les résultats;

**modalités de rendu**

  - forker ce dépôt ;
  - ajouter votre enseignant en tant que développeur ;
  - la synthèse devra être rédigée en markdown ;
  - le dépôt contiendra les sources de tous les schémas, graphiques ou
    animations, ainsi que les sources python.

****la synthèse**** La synthèse doit présenter le problème (le problème
du tri) abordé dans le chapitre. Il s'agit de donner une spécification
des tris et d'expliquer que les tri opérant par comparaison d'éléments
nécessitent un nombre de comparaisons dont l'ordre de grandeur est
\(\Omega(n\ln n)\).

Puis, la synthèse doit expliquer les différentes stratégies mises en
oeuvre pour envisager des tris linéaires.

Enfin, vous devrez y expliquer votre stratégie d'expérimentation et
analyser les résultats de vos expériences.
