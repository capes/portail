Calendrier
==========

Date des DS :

- DS6 (UE1) : 12 février 
- DS7 (UE2) : 19 février
- DS8 (UE1) : 4 mars
- DS9 (UE2) : 11 mars
- DS10 (UE1) : 18 mars

Semaine 12
----------
Révisions -> définir les modalités

### Mercredi 18 mars

| quand      | où | quoi     | avec qui                           |
|------------|----|----------|------------------------------------|
| 8:00-13:00 | A5 | DS10-UE1 | Benoit, attention : rattrapage UE1 |

Semaine 11
----------

### Lundi 09 mars

| quand       | où     | quoi           | avec qui |
|-------------|--------|----------------|----------|
| 13:30-15:00 | M3-211 | Correction DS6 | Éric     |

### Mardi 10 mars

| quand       | où     | quoi                          | avec qui |
|-------------|--------|-------------------------------|----------|
| 9:00-12:00  | M5-A4  | Leçons                        | Benoit   |
| 13:30-15:30 | M5-A12 | Correction DS7+Leçon Timothée | Benoit   |


### Mercredi 11 mars

| quand      | où | quoi    | avec qui                           |
|------------|----|---------|------------------------------------|
| 8:00-13:00 | A5 | DS9-UE2 | Benoit, attention : rattrapage UE2 |

### Jeudi 12 mars 

| quand       | où    | quoi                    | avec qui |
|-------------|-------|-------------------------|----------|
| 10:00-12:00 | M5-A4 | UE-CNT + Leçon Philippe | Benoit   |

Semaine 10
----------

### Mardi 03 mars

| quand       | où        | quoi          | avec qui |
|-------------|-----------|---------------|----------|
| 9:00-12:00  | M5-A4     | Leçons        | Benoit   |
| 14:00-16:00 | M3-Turing | Calculabilité | Éric     |


### Mercredi 04 mars

| quand      | où     | quoi    | avec qui |
|------------|--------|---------|----------|
| 8:30-12:30 | M3-211 | DS8-UE1 |          |

### Jeudi 05 mars 

| quand | où | quoi | avec qui |
|-------|----|------|----------|
|       |    |      |          |

Semaine 09
----------
Interruption pédagogique

Semaine 08
----------
leçons, DS7, programmation dynamique

### Mardi 18 février

| quand       | où     | quoi           | avec qui     |
|-------------|--------|----------------|--------------|
| 9:00-12:00  | M3-211 | Leçons         | Benoit, Éric |
| 13:30-15:30 | M3-211 | Correction DS5 | Benoit       |


### Mercredi 19 février

| quand      | où | quoi    | avec qui |
|------------|----|---------|----------|
| 8:00-13:00 | A5 | DS7-UE2 |          |

### Jeudi 20 février 

| quand       | où    | quoi                                     | avec qui     |
|-------------|-------|------------------------------------------|--------------|
| 10:00-12:00 | M5-A4 | [Programmation dynamique](./ue1/progdyn) | Éric, Benoit |


Semaine 07
----------

### Mardi 11 février

| quand       | où     | quoi              | avec qui     |
|-------------|--------|-------------------|--------------|
| 9:00-12:00  | M5-211 | Leçons            | Éric, Benoit |
| 13:30-15:30 | M5-A11 | [Chiffrement (RSA)](./ue1/rsa) | Benoit       |


### Mercredi 12 février

| quand      | où | quoi    | avec qui |
|------------|----|---------|----------|
| 8:00-13:00 |    | DS6-UE1 |          |

### Jeudi 13 février 

| quand       | où    | quoi                                    | avec qui     |
|-------------|-------|-----------------------------------------|--------------|
| 10:00-13:00 | M5-A4 | [Programmation dynamique](./ue1/progdyn) | Éric, Benoit |
 

Semaine 06
----------

### Lundi 3 février

### Mardi 4 février

| quand       | où        | quoi        | avec qui              |
|-------------|-----------|-------------|-----------------------|
| 9:00-11:00  | M3-Turing | Leçons      | Benoit,Éric,Francesco |
| 13:30-15:30 | M5-A11    | Chiffrement | Benoit                |

### Mercredi 5 février

Didapro

### Jeudi 6 février

Didapro

### Vendredi 7 Février

Didapro (si pas espé) 

Semaine 05
----------
stage

Semaine 04
----------
stage 

Semaine 03
----------

### Lundi 13 janvier

| quand       | où     | quoi           | avec qui |
|-------------|--------|----------------|----------|
| 13h30-15h30 | M3-211 | Correction DS4 | Benoit   |

### Mardi 14 janvier

| quand       | où     | quoi             | avec qui  |
|-------------|--------|------------------|-----------|
| 9:30-12:00  | M3-211 | UE2 : sujet zéro | Benoit    |


### Mercredi 15 janvier

| quand      | où | quoi       | avec qui |
|------------|----|------------|----------|
| 8h00-13h00 | A5 | DS5 (peda) |          |


### Jeudi 16 janvier

| quand       | où           | quoi           | avec qui  |
|-------------|--------------|----------------|-----------|
| 13h30-15h00 | Amphi Turing | Correction DS3 | Éric      |
| 15h45-17h45 | SUP          | Anglais        | N. Chapel |



Semaine 02
----------

### Lundi 6 janvier

| quand       | où    | quoi           | avec qui |
|-------------|-------|----------------|----------|
| 13h30-16h30 | M5-A4 | Correction DS3 | Benoit   |


### Mardi 7 janvier

Rendu UE CNT

| quand    | où                                          | quoi         | avec qui |
|----------|---------------------------------------------|--------------|----------|
| 9h30-12h | M3 extension - salle du conseil (1re étage) | archi : M999 | Philippe |

### Mercredi 8 janvier

| quand      | où         | quoi | avec qui |
|------------|------------|------|----------|
| 8h00-13h00 | SN1-Buffon | DS4  |          |

    
### Jeudi 9 Janvier

| quand       | où         | quoi                         | avec qui |
|-------------|------------|------------------------------|----------|
| 11h00-12h30 | M5-Bacchus | Conférence : la décidabilité | Éric     |
    

### Vendredi 10 Janvier

| quand | où | quoi | avec qui |
|-------|----|------|----------|
|       |    |      |          |

Semaine 51
----------

### Lundi 16 décembre

| quand       | où     | quoi                                  | avec qui      |
|-------------|--------|---------------------------------------|---------------|
| 13h30-15h00 | M3-226 | Leçon : Pierrick (architecture)       | Benoit & Éric |
| 15h20-16h50 | M3-226 | Leçon : Edouard (recherche textuelle) | Benoit        |

### Mardi 17 décembre 

Après-midi : examen oral anglais

### Mercredi 18 décembre

### Jeudi 19 décembre
| quand      | où     | quoi             | avec qui |
|------------|--------|------------------|----------|
| 8h30-9h30  | M3-226 | Leçon : Philippe | Benoit   |
| 8h30-10h30 | M3-226 | Leçon: Younes    | Benoit   |

### Vendredi 20 décembre

Matin : DS ue 4

Après-midi : DS Anglais

Semaine 50
----------

### Lundi 9 décembre

| quand       | où             | quoi | avec qui |
|-------------|----------------|------|----------|
| 10h00-12h00 | M1-Bernoulli ? | UE5  | C. Caux  |

### Mardi 10 décembre

| quand       | où    | quoi           | avec qui |
|-------------|-------|----------------|----------|
| 10:00-12:00 | M5-A5 | Corrigé du DS2 | Benoit   |

### Mercredi 11 décembre 

| quand      | où | quoi | avec qui |
|------------|----|------|----------|
| 8h00-13h00 | A5 | DS3  |          |

### Jeudi 12 décembre 

| quand       | où     | quoi                                            | avec qui           |
|-------------|--------|-------------------------------------------------|--------------------|
| 8h30-10h00  | M3-226 | Leçon : Nathan (sécurisation)                   | Benoit & Francesco |
| 10h20-11h50 | M3-226 | Leçon : Antoine (utilisation des bibliothèques) | Benoit & Philippe  |
| 13h30-15h00 | M3-226 | Leçon : Sarah (diviser pour reigner)            | Benoit & Éric      |
| 15h20-16h50 | M3-226 | Leçon : Florian                                 | Benoit & Maude     |

Semaine 49
----------

Stage

Semaine 48
----------

Stage

Semaine 47
----------

### Lundi 18 novembre

| quand       | où            | quoi          | avec qui |
|-------------|---------------|---------------|----------|
| 13h30-15h00 | Polytech C105 | Cours Réseaux | Thomas   |
| 15h20-16h50 | Polytech 304  | TD/TP Résaux  | Thomas   |

### Mardi 19 novembre

| quand       | où    | quoi   | avec qui          |
|-------------|-------|--------|-------------------|
| 10h20-11h50 | M5-A4 | Leçons | Francesco, Benoit |


### Mercredi 20 novembre 

| quand       | où     | quoi   | avec qui       |
|-------------|--------|--------|----------------|
| 8h30-10h00  | M3-211 | Leçons | Pierre, Benoit |
| 10h20-11h50 | M3-211 | Leçons | Maude, Benoit  |

### Jeudi 21 novembre 

| quand       | où           | quoi                    | avec qui    |
|-------------|--------------|-------------------------|-------------|
| 8h30-11h50  | Polytech 304 | Réseaux/syst. embarqués | Xavier      |
| 13h30-15h00 | M5-A16       | Leçons                  | Éric,Benoit |

Semaine 46
----------

### Lundi 11 novembre

Férié

### Mardi 12 novembre

| quand      | où    | quoi | avec qui |
|------------|-------|------|----------|
| 0h30-11h50 | M5-A4 | UE2  |          |

### Mercredi 13 novembre ###

| quand      | où | quoi | avec qui |
|------------|----|------|----------|
| 8h00-13h00 |    | DS2  |          |


### Jeudi 14 novembre 

| quand       | où           | quoi                    | avec qui |
|-------------|--------------|-------------------------|----------|
| 8h30-11h50  | Polytech 304 | Réseaux/syst. embarqués | Xavier   |
| 13h30-15h50 | M5-A16       | UE2 : Progression 1ere  | Benoit   |

Semaine 45
----------

### Lundi 4 novembre

| quand       | où            | quoi          | avec qui |
|-------------|---------------|---------------|----------|
| 13h30-15h00 | Polytech C105 | Cours Réseaux | Thomas   |
| 15h20-16h50 | Polytech 304  | TD/TP Résaux  | Thomas   |

### Mardi 5 novembre

Numeriqu'elle

### Mercredi 6 novembre 

| quand       | où            | quoi          | avec qui |
|-------------|---------------|---------------|----------|
| 8h30-10h00  | Polytech C105 | Cours Résaux  | Thomas   |
| 10h20-11h50 | Polytech 304  | TD/TP Réseaux | Thomas   |

### Jeudi 7 novembre

| quand       | où            | quoi                    | avec qui |
|-------------|---------------|-------------------------|----------|
| 8h30-11h50  | Polytech E304 | Réseaux/syst. embarqués | Xavier   |
| 13h30-15h50 | M5-A16        | Progression 1ere        | Benoit   |

Semaine 44
----------
Interruption pédagogique
 
Semaine 43
----------

### Lundi 21 octobre

| quand       | où     | quoi                                    | avec qui |
|-------------|--------|-----------------------------------------|----------|
| 10h00-12h00 | M3-211 | présentation ateliers                   | Maude    |
| 13h30-17h00 | M5-A4  | knn [pokemon](./ue1/pokemon/pokemon.md) |          |

### Mardi 22 octobre

| quand       | où     | quoi             | avec qui      |
|-------------|--------|------------------|---------------|
| 8h30-10h00  | M5-A4  | TP-arbres        | Francesco     |
| 10h20-11h30 | M5-A4  | Progression 1ère | Benoit        |
| 12h00-13h30 | M3-226 | Réunion Bilan    | Tout le monde |

### Mercredi 23 octobre

| quand       | où     | quoi           | avec qui |
|-------------|--------|----------------|----------|
| 8h30-10h00  | M5-A4  | Les graphes    | Benoit   |
| 10h20-12h00 | M3-211 | exos - graphes | Benoit   |


### Jeudi 24 octobre

| quand       | où        | quoi                                | avec qui |
|-------------|-----------|-------------------------------------|----------|
| 8h30-12h00  | M3-Turing | Leçons                              | Benoit   |
| 13h30-15h20 | M5-A16    | Graphes + [JS](./ue1/js1/readme.md) | Benoit   |


Semaine 42
----------

### Lundi 14 octobre

| quand       | où    | quoi                              | avec qui |
|-------------|-------|-----------------------------------|----------|
| 13h30-17h00 | M5-A4 | Fouille de données, apprentissage | Laetitia |

### Mardi 15 octobre

| quand       | où    | quoi       | avec qui  |
|-------------|-------|------------|-----------|
| 8h30-10h00  | M5-A4 | Les arbres | Francesco |
| 10h20-12h00 | M5-A4 | Jupyter !! | Benoit    |

### Mercredi 16 octobre

| quand       | où     | quoi              | avec qui |
|-------------|--------|-------------------|----------|
| 8h30-10h00  | M5-A4  | Hachage           | Benoit   |
| 10h20-12h00 | M3-211 | Exercices Hachage | Benoit   |

### Jeudi 17 octobre

| quand       | où        | quoi                                                      | avec qui |
|-------------|-----------|-----------------------------------------------------------|----------|
| 8h30-12h00  | M3-Turing | Leçons                                                    | Benoit   |
| 13h30-15h00 | M5-A16    | Problèmes de [mutabilité](./ue1/mutabilite/mutabilite.md) | Éric     |


### Vendredi 18 octobre
| quand       | où           | quoi | avec qui |
|-------------|--------------|------|----------|
| 8h30-12h30  | ESPE         | UE4  |          |
| 13h30-15h30 | M1-Bernoulli | UE3  | G. Jouve |


Semaine 41
----------

### Lundi 7 octobre 
| quand       | où    | quoi                    | avec qui |
|-------------|-------|-------------------------|----------|
| 13h30-17h00 | M5-A4 | heuristiques gloutonnes | Laetitia |

### Mardi 8 octobre
| quand       | où    | quoi               | avec qui  |
|-------------|-------|--------------------|-----------|
| 8h30-10h00  | M5-A4 | classes en Python  | Francesco |
| 10h20-12h00 | M5-A4 | Structure de liste | Francesco |

### Mercredi 9 octobre

| quand       | où     | quoi                                                                     | avec qui |
|-------------|--------|--------------------------------------------------------------------------|----------|
| 8h30-10h00  | M5-A4  | [Pile](./ue1/pile_et_file/pile.md) et [File](./ue1/pile_et_file/file.md) | Benoit   |
| 10h20-12h00 | M3-211 | Exos pile et file                                                        | Benoit   |

### Jeudi 10 octobre

| quand       | où        | quoi                             | avec qui          |
|-------------|-----------|----------------------------------|-------------------|
| 8h30-12h00  | M3-Turing | Leçons                           | Benoit, Francesco |
| 13h30-16h00 | M5-A16    | [TP Bataille](./ue1/war/card.py) | Éric    |


Semaine 40
----------
Cette semaine :
  - UE1 : 
    - Prog : exercices sur la récursivité 
    - Algo : complexité des tris récursifs
  - UE2 :
    - TICE : jupyter
    - dida : analyse du programme de terminale
	
### Lundi 30 septembre

| quand       | où             | quoi        | avec qui |
|-------------|----------------|-------------|----------|
| 9h00-12h00  | M1-Bernoulli ? | UE5         | C. Caux  |
| 13h30-17h00 | M4-A4          | [compression](./ue1/codage_textes/) | Éric     |

### Mardi 1 octobre ###

| quand       | où        | quoi                                | avec qui  |
|-------------|-----------|-------------------------------------|-----------|
| 8h30-10h00  | M3-Turing | Analyse des programmes NSI          | Philippe  |
| 10h20-12h00 | M5-A4     | [TICE : markdown](./ue2/fibo_md/) | Benoit    |
| 13h30-15h30 | SUP-CRL   | Anglais                             | N. Chapel |


### Mercredi 2 octobre ###

| quand       | où     | quoi                                                     | avec qui |
|-------------|--------|----------------------------------------------------------|----------|
| 8h30-10h00  | M3-211 | [Exercices sur la récursivité](./ue1/recursivite/exo-recursivite.md) | Benoit   |
| 10h20-11h50 | M3-211 | Les tris récursifs                                       | Benoit   |

### Jeudi 3 octobre ###

| quand       | où        | quoi           | avec qui |
|-------------|-----------|----------------|----------|
| 8h30-12h00  | M3-Turing | Leçons         | Benoit   |
| 13h30-15h30 | M5-A16    | Correction DS1 | Eric     |


Semaine 39
----------

Cette semaine :
  - UE1 :
	- codage des caractères + compression
	- récursivité
	- algo de tri : complexité des tri par sélection et par insertion
  - UE2 :
    - TICE : jupyter
    - dida : analyse du programme de terminale
	
### Lundi 23 septembre

| quand       | où           | quoi                                              | avec qui |
|-------------|--------------|---------------------------------------------------|----------|
| 9h00-12h00  | M1-Bernoulli | UE5                                               | C. Caux  |
| 13h30-17h00 | M5-A4        | Algo/prog - codage (suite) : caractères + Huffman | Eric W   |

### Mardi 24 septembre ###
 
| quand       | où      | quoi                                                 | avec qui        |
|-------------|---------|------------------------------------------------------|-----------------|
| 8h30-9:30   | M5-A4   | ["Code design"](./ue1/code_design/readme.md)         | Jean-Christophe |
| 10h00-12h00 | M5-A4   | [Bonnes Pratiques](./ue1/bonnes_pratiques/readme.md) | Francesco       |
| 13h30-15h30 | SUP CRL | Anglais                                              | N. Chapel       |


### Mercredi 25 septembre

| quand      | où          | quoi                       | avec qui |
|------------|-------------|----------------------------|----------|
| 8h00-13h00 | SN1 - MAIGE | [DS1](/ue1/ds/ds1/ds1.pdf) |          |

### Jeudi 26 septembre ###

| quand       | où           | quoi                                 | avec qui |
|-------------|--------------|--------------------------------------|----------|
| 8h30-12h00  | Amphi Turing | Leçons                               | Benoit   |
| 13h30-15h00 | M5-A16       | Algo : complexité des tris itératifs | Benoit   |

### Vendredi 27 septembre

| quand       | où           | quoi | avec qui |
|-------------|--------------|------|----------|
| 8h30-12h30  | ESPE         | UE4  |          |
| 13h30-15h30 | M1-Bernoulli | UE3  | G. Jouve |


Semaine 38
----------
Cette semaine :
  - UE1 :
     - codage des nombres
     - algorithmes de tri
  - UE2 : 
    - analyse de programmes de SNT+NSI première
    - TICE : md + jupyter
	
### Lundi 16 septembre

| quand       | où             | quoi                           | avec qui |
|-------------|----------------|--------------------------------|----------|
| 9h00-12h00  | M1-Bernoulli ? | UE5                            | C. Caux  |
| 13h30-17h00 | M5-A4          | Algo/prog : codage des nombres | Eric W.  |

### Mardi 17 septembre

| quand       | où      | quoi                                       | qui      |
|-------------|---------|--------------------------------------------|----------|
| 8h30-10h20  | M5-A4   | UE2 - analyse des programmes de SNT et NSI | Philippe |
| 10h30-12h30 | M5-A4   | Algo : Les algos de tri                    | Benoit   |
| 13h30-15h30 | SUP-CRL | Anglais                                    |          |

### Mercredi 18 septembre

| quand       | où           | quoi                                                                | qui       |
|-------------|--------------|---------------------------------------------------------------------|-----------|
| 10:00-12h00 | Amphi Turing | UE - TICE : markdown + jupyter                                      | Benoit    |
| 13h30-15h30 | M5-A4        | [Les algorithmes de tri](ue1/analyse_tris/Readme.md) : experimental | Francesco |


### Jeudi 19 septembre ###

| quand       | où           | quoi        | qui              |
|-------------|--------------|-------------|------------------|
| 8h30-12h30  | Amphi Turing | Leçons      | Benoit, Philippe |
| 13h30-15h30 | M5-A16       | recursivité | Jean-Christophe  |

### Vendredi 20 septembre

| quand      | où   | quoi | avec qui |
|------------|------|------|----------|
| 8h30-12h30 | ESPE | UE4  |          |
  
Semaine 37
----------

### Lundi 9 septembre

| quand       | où             | quoi                                                                                    | avec qui |
|-------------|----------------|-----------------------------------------------------------------------------------------|----------|
| 9h00-12h00  | M1-Bernoulli ? | UE5                                                                                     | C. Caux  |
| 13h30-17h30 | M5-A16         | Environnements ([git](ue1/git/gitlab.md), bash, thonny)                                 | Eric W.  |
|             |                | Python : type de base, listes, tuple ; Docstring, doctest ; Bonnes pratiques ; Debogage |          |
|             |                | [TP phrases de passe](ue1/phrases_de_passe/readme.md)                                   |          |

### Mardi 10 septembre

| quand      | où     | quoi                                                                 | avec qui    |
|------------|--------|----------------------------------------------------------------------|-------------|
| 8h30-12h30 | M5-A12 | Prog python : [dictionnaires](ue1/dictionnaires/dictionnaires.ipynb) | Philippe M. |
|            |        | [TP anagrammes](ue1/anagrammes/anagrammes.md)                        |             |


### Mercredi 11 septembre

| quand       | où     | quoi                                         | avec qui        |
|-------------|--------|----------------------------------------------|-----------------|
| 13h30-17h00 | M5-A12 | Algo : recherche d'un élément dans une liste | Francesco de C. |
|             |        | Prog : [fichiers](ue1/fichiers/fichiers.md)  | Maude P.        |

### Jeudi 12 septembre

| quand       | où     | quoi                                                                        | avec qui  |
|-------------|--------|-----------------------------------------------------------------------------|-----------|
| 8h30-12h30  | M5-A12 | Prog : traitement de données en table, [fichier csv](ue1/jardins/garden.md) | Benoit P. |
| 13h30-17h00 | M5-A15 | Algo/prog : décomposition d'un entier dans une base, codage                 | Eric W    |


### Vendredi 13 septembre

| quand      | où   | quoi | avec qui |
|------------|------|------|----------|
| 8h30-12h30 | ESPE | UE4  |          |


Semaine 36
----------

### Mercredi 4 septembre ###

  - pré-rentrée
