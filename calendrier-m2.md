Calendrier M2
=============

Dates des ds :

- [x] 11/10, 13h00-18h00, ue a2 au m1 Chatelet
- [x] 6/12, ue a1, 13h00-18h00, ue a1, bat. a5
- [x] 24/01, ue a2


Remarques :

ue b3 :

- [x] 25/10, pas de séance m2
- [x] 15/02 , tp en a16

soutenances :

- [x] jeudi 13 juin, matin : soutenances de mémoire
  - 8h30 : Nicolas
  - 9h30 : Lucas
  - 10h30 : Clément
  - 11h30 : Théo
- [ ] vendredi 14 juin, après-midi : eep
  - 13h00 : Théo
  - 14h00 : Clément
  - 15h00 : Lucas
  - 16h00 : Nicolas

semaine 22
----------

### mercredi 29 mai ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | sup 117 | ue a2 | B. Papegay  |
| 10h15-11h45 | sup 209 | ue b3 | M. Guichard |


### jeudi 30 mai ###

*matin* inspé 

| Quand       | Où     | Quoi         | Qui        |
|-------------|--------|--------------|------------|
| 13h00-14h30 | m3 226 | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | sup117 | ue a1 (tp)   | B. Papegay |


semaine 21
----------

### mardi 21 mai ###

*stage*

### mercredi 22 mai ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | sup 117 | ue a2 | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3 | M. Guichard |


### jeudi 23 mai ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | sup117    | ue a1 (tp)   | B. Papegay |

semaine 20
----------

### mardi 14 mai ###

*stage*

### mercredi 15 mai ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | sup 117 | ue c3 | B. Papegay  |
| 10h15-11h45 | sup 209 | ue b3 | M. Guichard |


### jeudi 16 mai ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

semaine 16
----------

### mardi 16 avril ###

*stage*

### mercredi 17 avril ###

| Quand       | Où        | Quoi          | Qui        |
|-------------|-----------|---------------|------------|
| 8h30-10h00  | sup 116   | ue a2/c3 (tp) | P. Thibaud |
| 10h15-11h45 | m3 Turing | ue a1 (oral)  | P. Thibaud |


### jeudi 18 avril ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

semaine 15
----------

### mardi 9 avril ###

*stage*

### mercredi 10 avril ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 116 | ue c          | B. Papegay  |


### jeudi 11 avril ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

semaine 14
----------

### mardi 2 avril ###

*stage*

### mercredi 3 avril ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 116 | ue c          | B. Papegay  |


### jeudi 4 avril ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

semaine 13
----------

### mardi 25 mars ###

*stage*

### mercredi 26 mars ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 116 | ue c          | B. Papegay  |


### jeudi 27 mars ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

semaine 12
----------

capes

semaine 11
----------

### lundi 11 mars ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 13h00-14h30 | sup 116   | ue c1 | B. Papegay |
| 14h45-16h15 | m3 Turing | ue a  | B. Papegay |


### mardi 12 mars ###

*stage*

### mercredi 13 mars ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |


semaine 10
---------

### lundi 4 mars ###

| Quand                 | Où      | Quoi  | Qui        |
|-----------------------|---------|-------|------------|
| :warning: 13h00-14h30 | sup 116 | ue c1 | B. Papegay |
| 14h45-16h15           | m3 226  | ue a  | B. Papegay |


### mardi 5 mars ###

*stage*

### mercredi 6 mars ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 116 | ue a          | P. Thibaud  |
| 14h45-16h15 | sup 116 | ue c          | B. Papegay  |
    
### jeudi 7 mars ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

semaine 9
---------

interruption pédagogique

semaine 8
---------

### lundi 19 février ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h30-16h30 | m5 a11 | ue c1 | B. Papegay |

- [chiffrement rsa](./ue1/chiffrement/rsa.md)

### mardi 20 février ###

*stage*

### mercredi 21 février ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 116 | ue a          | P. Thibaud  |
| 14h45-16h15 | sup 116 | ue c          | B. Papegay  |

-[Programmation fonctionnelle Diaporama](https://diu-uf-bordeaux.github.io/bloc4/prog/prog.html#/13)
- [TP PF](https://diu-uf-bordeaux.github.io/bloc4/td/functional/)

### jeudi 22 février ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |


semaine 7
---------

### lundi 12 février ###

| Quand       | Où    | Quoi  | Qui        |
|-------------|-------|-------|------------|
| 13h30-16h30 | m5 a4 | ue c1 | B. Papegay |

- [enigma](./ue1/chiffrement/enigma-etu.md)

### mardi 13 février ###

*stage*

### mercredi 14 février ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 116 | ue a          | P. Thibaud  |
| 14h45-16h15 | sup 116 | ue c          | B. Papegay  |

- [TP TechnoWeb](https://www.fil.univ-lille.fr/~thibaud/tw1/projet/js-2023_archive/mini-projet-javascript.html)
- [chiffrement rsa](./ue1/chiffrement/rsa.md)

### jeudi 15 février ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a16    | ue a1 (tp)   | B. Papegay |

semaine 6
---------

### lundi 5 février ###

| Quand       | Où    | Quoi  | Qui        |
|-------------|-------|-------|------------|
| 13h30-16h30 | m5 a4 | ue c1 | B. Papegay |

[chiffrements](./ue1/chiffrement/README.md)

### mardi 6 février ###

*stage*

### mercredi 7 février ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup116  | ue a          | P. Thibaud  |
| 14h45-16h15 | sup 116 | ue c          | B. Papegay  |

- [TP introduction scripts bash](./ue2/bash/Readme.md)

### jeudi 8 février ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |


semaine 5
---------

### lundi 29 janvier ###

| Quand       | Où    | Quoi  | Qui        |
|-------------|-------|-------|------------|
| 13h30-16h30 | m5 a4 | ue c1 | B. Papegay |


### mardi 30 janvier ###

*stage*

### mercredi 31 janvier ###

| Quand       | Où        | Quoi          | Qui         |
|-------------|-----------|---------------|-------------|
| 8h30-10h00  | sup 116   | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209   | ue b3         | M. Guichard |
| 13h00-14h30 | m3 Turing | ue a          | P. Thibaud  |
| 14h45-16h15 | sup 116   | ue c          | B. Papegay  |

- [TP analyse trames Wireshark](./ue2/reseaux/tp4_m2wireshark/tpm2_wireshark.md)

### jeudi 1 février ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

- [algorithme glouton : la debroyeuse](./ue1/debroyeuse/presentation.md)

semaine 4
---------


### lundi 22 janvier ###

| Quand       | Où    | Quoi  | Qui        |
|-------------|-------|-------|------------|
| 13h30-16h30 | m5 a4 | ue c1 | B. Papegay |


### mardi 23 janvier ###

*stage*

### mercredi 24 janvier ###

| Quand      | Où | Quoi | Qui                   |
|------------|----|------|-----------------------|
| 8h00-13h00 | a5 | ds 3 | B. Papegay/P. Thibaud |


### jeudi 25 janvier ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |


### vendredi 26 janvier ###

*stage*

semaine 3
---------


### lundi 15 janvier ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 14h30-17h30 | m5 a11 | ue c1 | B. Papegay |

[calculabilité](./ue1/calculabilite)

### mardi 16 janvier ###

*stage*

### mercredi 17 janvier ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 117 | ue a2         | P. Thibaud  |
| 14h45-16h15 | sup 116 | ue a1         | B. Papegay  |

### jeudi 18 janvier ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |


### vendredi 19 janvier ###

*stage*


semaine 2
---------


### lundi 8 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 14h30-16h30 | sup 116 | ue c1 | B. Papegay |

[calculabilité](./ue1/calculabilite)

### mardi 9 janvier ###

*stage*

### mercredi 10 janvier ###

| Quand       | Où      | Quoi          | Qui         |
|-------------|---------|---------------|-------------|
| 8h30-10h00  | sup 116 | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | sup 209 | ue b3         | M. Guichard |
| 13h00-14h30 | sup 117 | ue a2         | P. Thibaud  |
| 14h45-16h15 | sup 116 | ue a1         | B. Papegay  |

* [introduction à Flask](./ue1/web/)

### jeudi 11 janvier ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

* [écriture d'un webservice](./ue1/web/)

### vendredi 12 janvier ###

*stage*


semaine 1
---------

interruption pédagogique

semaine 52
----------

interruption pédagogique

semaine 51
----------

### lundi 18 décembre ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 13h00-14h30 | m3 Turing | ue c1 | B. Papegay |
| 14h45-16h15 | m3 Turing | ue c2 | B. Papegay |

### mardi 19 décembre ###

*stage*

### mercredi 20 décembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | sup 117  | ue a1         | B. Papegay  |


### jeudi 21 décembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

### vendredi 15 décembre ###

*stage*


semaine 50
----------

### lundi 11 décembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

### mardi 12 décembre ###

*stage*

### mercredi 13 décembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |


### jeudi 14 décembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4    | ue a1 (tp)   | B. Papegay |

### vendredi 15 décembre ###

*stage*


semaine 49
----------

### lundi 4 décembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

### mardi 5 décembre ###

*stage*

### mercredi 6 décembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 13h00-18h00 | bat a5   | ue a1         |             |

* [TP - Floyd-Warshall](./ue2/parcours_largeur/floyd.md)

### jeudi 7 décembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

* [automates et machines séquentielles](./ue1/automates/) 
  (*sous forme de notebook jupyter*)

### vendredi 8 décembre ###

*stage*

semaine 48
----------

### Lundi 27 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

[les tests statistiques](./ue3/stats/readme.md)

### Mardi 28 novembre ###


### Mercredi 29 novembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |

### Jeudi 30 novembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

* [TP mkdocs](ue2/mkdocs/Readme.md) + [Consignes rendu RushHour](ue2/rushhour/Readme.md)
* [TP automate, à utiliser avec jupyter](ue1/algo-texte/)

### Vendredi 1 décembre ###



semaine 47
----------

### Lundi 20 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |


### Mardi 21 novembre ###


### Mercredi 22 novembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |

* [TP mkdocs](ue2/mkdocs/Readme.md)

### Jeudi 23 novembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

* [TP mkdocs](ue2/mkdocs/Readme.md) + [Consignes rendu RushHour](ue2/rushhour/Readme.md)

### Vendredi 24 novembre ###

inspé selon calendrier



semaine 46
----------

### Lundi 13 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |


### Mardi 14 novembre ###


### Mercredi 15 novembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |


### Jeudi 16 novembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |


### Vendredi 17 novembre ###

inspé selon calendrier


semaine 45
----------

### Lundi 6 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

[graphiques en python](./ue3/outils/python_graphiques.md)

### Mardi 7 novembre ###


### Mercredi 8 novembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |

* [TP Sqlite](ue1/sql2/Readme.md)

### Jeudi 9 novembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

* [TP normalisation](ue1/sql_normalisation/tp_normalisation.md)

### Vendredi 10 novembre ###

inspé selon calendrier


semaine 44
----------

interruption pédagogique bien méritée

semaine 43
----------

### Lundi 23 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |


### Mardi 24 octobre ###


### Mercredi 25 octobre ###

| Quand       | Où       | Quoi          | Qui        |
|-------------|----------|---------------|------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud |
| 10h15-11h45 | m1 Riesz | ue a1         | B. Papegay |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud |

* [TP - SQL](https://mystery.knightlab.com/)

### Jeudi 26 octobre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

* TP Distance de Levenshtein

### Vendredi 27 octobre ###


semaine 42
----------

### Lundi 16 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |


### Mardi 17 octobre ###


### Mercredi 18 octobre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |


### Jeudi 19 octobre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |


### Vendredi 20 octobre ###

inspé selon calendrier


semaine 41
----------

### Lundi 9 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

- [manim, cours 2](./ue3/manim/mobject.md)

### Mardi 10 octobre ###


### Mercredi 11 octobre ###

| Quand       | Où          | Quoi          | Qui         |
|-------------|-------------|---------------|-------------|
| 8h30-10h00  | sup 116     | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz    | ue b3         | M. Guichard |
| 13h00-18h00 | m1 Châtelet | ds a2         |             |

* [TP - Bellman-Ford](./ue2/parcours_largeur/bellman.md)

### Jeudi 12 octobre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

* [TP Rush Hour](./ue2/rushhour/Readme.md)

### Vendredi 13 octobre ###

inspé selon calendrier


semaine 40
----------

### Lundi 2 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

l'algorithme de Dijkstra : énoncé, preuve et correction.

### Mardi 3 octobre ###


### Mercredi 4 octobre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |

* [TP - Dijkstra](./ue2/parcours_largeur/dijsktra.md)


### Jeudi 5 octobre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |


### Vendredi 6 octobre ###

inspé selon calendrier


semaine 39
----------

### Lundi 25 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue c1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

- [installation de mermaid](./ue3/outils/mermaid.md)
- [réaliser des schémas avec mermaid](./ue3/outils/mermaid_graphiques.md)

### Mardi 26 septembre ###


### Mercredi 27 septembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue a1         | B. Papegay  |

* [TP - déplacement du cavalier : parcours de graphe](./ue2/parcours_largeur/cavalier.md)
* [TD UE2 : les graphes en SNT](./ue2/graphes_snt/graphes_snt.pdf)

### Jeudi 28 septembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

*tp* : manim ou [représentation des graphes](./ue1/parcours_graphes/tp_parcours.md)

### Vendredi 29 septembre ###

inspé selon calendrier

semaine 38
----------

### Lundi 18 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue a1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

*tp ue c* :

- [bases manim](./ue3/manim/base.md)
- [les objets](./ue3/manim/mobject.md)
- [les groupes](./ue3/manim/groupes.md)
- [les textes](./ue3/manim/textes.md)
- [les animations](./ue3/manim/animation.md)

### Mardi 19 septembre ###


### Mercredi 20 septembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue c1         | B. Papegay  |

* [les graphes en python](./ue2/graphes/graphe.ipynb)

### Jeudi 21 septembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |

*tp* : manim ou [représentation des graphes](./ue1/parcours_graphes/tp_parcours.md)

### Vendredi 22 septembre ###

inspé selon calendrier

semaine 37
----------

### Lundi 11 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 13h00-14h30 | m5 a14 | ue a1 | B. Papegay |
| 14h45-16h15 | m5 a14 | ue c2 | B. Papegay |

### Mardi 12 septembre ###


### Mercredi 13 septembre ###

| Quand       | Où       | Quoi          | Qui         |
|-------------|----------|---------------|-------------|
| 8h30-10h00  | sup 116  | ue a2/c3 (tp) | P. Thibaud  |
| 10h15-11h45 | m1 Riesz | ue b3         | M. Guichard |
| 13h00-14h30 | m1 Riesz | ue a2         | P. Thibaud  |
| 14h45-16h15 | m1 Riesz | ue c1         | B. Papegay  |

* [les graphes en python](./ue2/graphes/graphe.ipynb)

### Jeudi 14 septembre ###

*matin* inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a15    | ue a1 (tp)   | B. Papegay |


* [Présentation épreuve de leçons](./ue1/epreuve_oral1/)
* [Planning passage leçons](./passages-oraux-23.md)

### Vendredi 15 septembre ###

inspé selon calendrier

Semaine 36
----------
    
### Jeudi 7 septembre ###

  - pré-rentrée (Amphi Turing, Bâtiment M3) à 13h00
  

