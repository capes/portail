Passages oraux
===============

mis à jour le 26/01/2023

|Leçon|Etudiant(e)|Date|
|:---|:---|:---:|
|Maxime |Représentation des données : type et valeurs de base|08/09|
|Erwan |Mise au point des programmes, documentation de programmes et gestion des bugs|15/09|
|Theo B.|Constructions élémentaires des langages de programmations, fonctions|22/09|
|Cindy  |Récursivité|29/09|
|Clement|Paradigmes de programmation|06/10|
|Lucas R.|Structures linéaires de données|13/10|
|Nicolas D.|Algorithmes de tri|20/10|
|Cindy|Interactions homme-machine sur le Web|27/10|
|Theo T.| Traitement de données en table|10/11|
|Chloe|Bases de données : représentations et applications|17/11|
|Erwan|Arbres : structures de données et algorithmes|24/11|
|Maxime|Graphes : structures de données et algorithmes|01/12|
|Pierre-Andre|Mise au point de programmes, documentation et gestion de bugs|08/12|
|Hadrien|Architecture d'une machine|15/12|
|Nicolas|Principes du Web|05/01|
|Theo T.|Récursivité|12/01|
|Cindy|Algorithmes et protocoles de routage dans les réseaux|26/01|
|Theo B.|Sécurisation des communications|02/02|
|Clément|Principes de fonctionnement des réseaux|09/02|
|Lucas|Algorithmes gloutons|16/02|
|Maxime|Principe de fonctionnement d'un système d'exploitation|02/03|
|Erwan|Calculabilité et décidabilité|09/03|
|Theo B.|Gestion des processus et des ressources par un système d'exploitation|30/03|
|Theo T.|Programmation dynamique|04/05|
|Clement |Méthode diviser pour régner|11/05|
|Pierre-André|Arbres : structures de données et algorithmes|25/05|

