Calendrier M2
=============

Planning Soutenances UE3
------------------------

| Date        | Horaire | Année | Groupe                    | Sujet                                         |                  |
|-------------|---------|-------|---------------------------|-----------------------------------------------|------------------|
| Lundi 10/05 |         |       | Exam d'anglais l'ap midi  | possibilité le matin                          |                  |
| Mardi 11/05 | 9h00    | M1    | E. DEMERVILLE, M. VINIT   | Les cryptomonnaies                            | Benoit           |
| Mardi 11/05 | 9h45    | M1    | C. BRASSART, J. BENOUWT   | Problème de l'arrêt                           | Benoit           |
| Mardi 11/05 | 10h30   | M1    | P. ZAGACKI, A. MARETTE    | Ordinateur qui apprend à jouer                | Benoit, Patrice  |
| Mardi 11/05 | 11h15   | M1    | J. HERMILLIER,            | Informatique et enjeu environnementaux        | Philippe, Benoit |
| Mardi 11/05 | 12h00   | M2    | T. DECOSTER, J. DUCHAINE, | Les variables et fonctions en info            | Philippe, Benoit |
| Mardi 11/05 | 14h00   | M2    | F. MATHIEU, N. MABRIEZ    | Apprentissage de l'informatique par les jeux  | Philippe, Benoit |
| Mardi 11/05 | 15h00   | M1    | T. BODDAERT, M. BERTILLE  | Apprentissage de l'informatique par les jeux  | Maude, Benoit    |
| Lundi 17/05 | 10h00   | M2    | P. BODDAERT, A. MERESSE   | Informatique et vie privée                    | Laetitia, Benoit |
| Lundi 17/05 | 11h00   | M2    | Y. IKKLI, E. FREMEAUX     | L'informatique et les enjeux environnementaux | Laetitia, Benoit |


Semaine 18
----------

### Mercredi 19 Mai

| Quand       | Comment    | Quoi    | Qui          |
|-------------|------------|---------|--------------|
| 13h30-15h30 | Distanciel | Anglais | C. Demailly  |

Semaine 17
----------

### Mercredi 12 Mai

| Quand       | Comment    | Quoi    | Qui          |
|-------------|------------|---------|--------------|
| 13h30-15h30 | Distanciel | Anglais | C. Demailly  |


Semaine 16
----------

_interruption pédagogique_

Semaine 15
----------

_interruption pédagogique_


Semaine 14
----------

Semaine 13
----------

### Mercredi 14 Avril

| Quand       | Comment    | Quoi    | Qui         |
|-------------|------------|---------|-------------|
| 13h30-15h30 | Distanciel | Anglais | C. Demailly |


Semaine 12
----------

### Mardi 6 Avril

_Pour assister aux oraux, il faut s'engager à en présenter un_

| Quand       | Comment  | Quoi                                            | Qui             |
|-------------|----------|-------------------------------------------------|-----------------|
| 12h00-14h00 | Présence | Oral 1 (Amélie) : Structure linéaire de données | Patrice, Benoit |


### Mercredi 7 Avril

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 14h00-15h00 | Distanciel | UE2  | B. Papegay |

### Jeudi 8 avril

| Quand       | Où       | Quoi              | Qui             |
|-------------|----------|-------------------|-----------------|
| 10h00-13h00 | M5 - A4  | UE2               | P. Thibaud      |
| 13h30-15h30 | M3 - 226 | Oral 1 (Philippe) | Patrice, Benoit |

Semaine 11
----------

### Mercredi 31 Mars

| Quand       | Comment    | Quoi    | Qui          |
|-------------|------------|---------|--------------|
| 9h00-10h00  | Distanciel | UE2     | B. Papegay   |
| 13h30-15h30 | Distanciel | Anglais | C. Demailly  |
| 15h30-17h30 | Distanciel | UE5     | C. Mieszczak |

Semaine 10
----------

### Mercredi 24 Mars

| Quand       | Comment    | Quoi | Qui          |
|-------------|------------|------|--------------|
| 14h00-15h30 | Distanciel | UE2  | B. Papegay   |
| 15h45-17h45 | Distanciel | UE1  | F. De Comité |

### Mercredi 10 Mars


| Quand       | Comment    | Quoi | Qui          |
|-------------|------------|------|--------------|
| 14h00-15h30 | Distanciel | UE1  | F. De Comité |
| 15h45-17h45 | Distanciel | UE2  | B. Papegay   |

Semaine 08
----------

__ Vacances __

Semaine 07
----------

### Mercredi 17 février


| Quand       | Comment    | Quoi | Qui          |
|-------------|------------|------|--------------|
| 10h00-12h00 | Distan     | UE1  | F. De Comité |
| 14h00-16h00 | Distanciel | UE2  | B. Papegay   |

Semaine 06
----------

### Mercredi 10 février

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 14h00-16h00 | Distanciel | UE2  | B. Papegay |

Semaine 03
----------

### Mercredi 13 janvier

| Quand       | Comment    | Quoi   | Qui           |
|-------------|------------|--------|---------------|
| 13h30-15h30 | Distanciel | UE5    | C. Mieszczak  |

Semaine 51
----------

### Mercredi 16 Décembre

DS d'UE2 en M1 : Modalités par mail.

| Quand       | Où       | Quoi | Qui          |
|-------------|----------|------|--------------|
| 13h30-15h30 | Distance | UE5  | C. Mieszczak |
| 15h30-17h30 | Distance | UE2  | B. Papegay   |

Semaine 50
----------

### Mercredi 9 Décembre


| Quand       | Où       | Quoi          | Qui                             |
|-------------|----------|---------------|---------------------------------|
| 10h20-12h20 | Distance | Anglais       | C. Demailly                     |
| 13h30-16h30 | Distance | Calculabilité | Avec le DIU (& E. Wegrzynowski) |

Semaine 49
----------

### Mercredi 2 Décembre


| Quand       | Où       | Quoi | Qui          |
|-------------|----------|------|--------------|
| 10h20-12h20 | Distance | UE1  | F. De Comité |
| 13h30-15h30 | Distance | UE5  | C. Mieszczak |

Semaine 48
----------

### Mercredi 25 novembre

| Quand       | Où       | Quoi                   | Qui         |
|-------------|----------|------------------------|-------------|
| 10h20-12h20 | Distance | Anglais                | C. Demailly |
| 13h30-16h30 | Distance | Algorithmique du texte | Avec le DIU |

Semaine 47
----------

### Mardi 17 novembre

| Quand       | Où       | Quoi    | Qui |
|-------------|----------|---------|-----|
| 13h30-16h30 | Distance | UE1/UE2 | DIU |


### Mercredi 18 novembre
q
| Quand       | Où       | Quoi | Qui          |
|-------------|----------|------|--------------|
| 10h20-12h20 |          | UE1  | F. De Comité |
| 13h30-15h30 |          | UE5  | C. Mieszczak |
| 15h30-17h30 | Distance | UE2  | B. Papegay   |

Semaine 46
----------

### Jeudi 12 novembre

| Quand       | Où       | Quoi             | Qui        |
|-------------|----------|------------------|------------|
| 13h30-16h30 | Distance | UE2, avec le DIU | B. Papegay |

Semaine 45
----------

### Mercredi 4 novembre

| Quand       | Où       | Quoi | Qui          |
|-------------|----------|------|--------------|
| 10h20-12h20 |          | UE1  | F. De Comité |
| 13h30-15h30 |          | UE5  | C. Mieszczak |
| 15h30-17h30 | Distance | UE2  | B. Papegay   |


Semaine 42
----------

### Mercredi 14 octobre ###

| Quand       | Où          | Quoi | Qui          |
|-------------|-------------|------|--------------|
| 13h30-17h30 | M3-Delattre | UE5  | C. Mieszczak |

Semaine 41
----------

### Mercredi 7 octobre ###

| Quand       | Où          | Quoi | Qui          |
|-------------|-------------|------|--------------|
| 10h20-12h20 | SUP 209     | UE1  | F. De Comité |
| 13h30-15h30 | M5-A16      | UE2  | B. Papegay   |
| 15h30-17h30 | M3-Delattre | UE5  | C. Mieszczak |

Ressources sur :
- [les arbres](./ue1/arbres/Readme.md)
- [Huffman](./ue1/huffman/Readme.md)
!!
Semaine 40
----------

### Mercredi 30 septembre ###

| Quand       | Où     | Quoi | Qui       |
|-------------|--------|------|-----------|
| 14h00-16h00 | M5-A16 | UE2  | B.Papegay |

Semaine 39
----------

### Mercredi 23 septembre ###

| Quand       | Où          | Quoi | Qui          |
|-------------|-------------|------|--------------|
| 10h20-12h20 | SUP 209     | UE1  | F. De Comité |
| 13h30-15h30 | M5-A16      | UE2  | B. Papegay   |
| 15h30-17h30 | M3-Delattre | UE5  | C. Mieszczak |

Semaine 38
----------

### Mercredi 16 septembre ###

| Quand       | Où          | Quoi | Qui                   |
|-------------|-------------|------|-----------------------|
| 13h30-15h30 | M5 - Turing | UE3  | B. Papegay/P. Marquet |
| 15h30-17h30 | C1-003      | UE5  | C. Mieszczak          |


Semaine 37
----------


### Mercredi 9 septembre ###

| Quand       | Où      | Quoi | Qui          |
|-------------|---------|------|--------------|
| 10h20-12h20 | SUP 209 | UE1  | F. De Comité |
| 13h30-16h20 | C1-003  | UE2  | B. Papegay   |

Semaine 36
----------

### Mercredi 2 septembre ###

  - pré-rentrée (Salle A1, Bâtiment M5) à 15h00
  
