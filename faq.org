#+title: FAQ - MEÉF informatique - CAPES NSI

* Le master MEÉF

** Généralités

   - MEÉF : Métiers de l'enseignement, de l'éducation et de la formation.
   - Il n'y a qu'un seul master MEÉF, mais plusieurs parcours.
   - Le parcours qui nous intéresse est /informatique/ — NSI : Numérique et Sciences Informatiques
   - C'est ce parcours qui permet de se préparer à l'enseignement des disciplines :
     - NSI (classe de première et de terminale du lycée)
     - SNT : Sciences numériques et technologique (classe de seconde)

** Candidater 
   - Informations sur le [[http://www.inspe-lille-hdf.fr/][site de l'INSPÉ Lille Nord de France]]    
   - Dépôt de candidature sur le [[https://www.monmaster.gouv.fr/]]
   - Campagne d'inscription du du 23 mars 2023 au 18 avril 2023
   - Examen des candidatures du 24/04/23 au 16/06/23
   - Transmission des propositions d'admissions : du 23/06/23 au 21/07/23
   - ((Hors campagne, nous étudions toute candidature raisonnable au
     regard du niveau et de la date.))


* Le Concours du CAPES
  
** Généralités
   - CAPES : Certificat d'aptitude pédagogique à l'enseignement secondaire ;
     comme son nom l'indique, il permet d'enseigner dans les collèges et lycées.
   - Il s'agit d'un concours, et non d'un diplôme : 
     - Un certain nombre de postes est ouvert chaque année
     - Les candidats sont classés par rapport aux épreuves :
       + d'admissibilité ;
       + d'admission
     - À l'issue de l'admission, les candidat reçoivent, en fonction
       de leur rang, une affectation dans un établissement. Ces
       affectations sont nationales.
   - Pour pouvoir s'inscrire au concours du CAPES, il faut :
     - Être titulaire d'un master *ou* être inscrit en master MEÉF
     - Être de nationalité française, ou être ressortissant d'un des
       28 États de l'Union européenne ou d'un État partie à l'accord
       sur l'Espace économique européen (Islande, Liechtenstein et
       Norvège). Les candidats qui ont la nationalité monégasque,
       andorrane ou suisse ont aussi accès aux concours.
 
   - Pour de plus amples informations, [[https://www.devenirenseignant.gouv.fr/cid99017/les-conditions-d-inscription.html][voir ici sur le site devenirenseignant.gouv.fr]].

** Les épreuves du CAPES NSI
   - Il y a deux épreuves d'admissibilité :
     - Un premier écrit d'informatique générale (durée 5h)
     - Un deuxième écrit de pédagogie de l'informatique (durée 5h)
   - Il y a deux épreuves d'admission :
     - Un premier oral d'informatique générale
     - Un deuxième oral sur un dossier pédagogique élaboré durant
       l'année.

** Nombres de postes au concours du CAPES NSI 2021

   - 2023 :
     - 50 postes au concours du CAPES externe
     - 20 postes au 3e concours
     - 6 postes CAFEP
   - 2022 :
     - 50 postes au concours du CAPES externe
     - 5 postes au CAFEP
   - 2021 :
     - 60 postes au concours du CAPES
     - 20 postes au 3e concours 
     - 20 postes au CAFEP
   - 2020 :
     - 30 postes au concours du CAPES externe
     - 8 postes au 3e concours
     - 10 postes au concours du CAFEP

* Autres questions

à venir...

** Formation à distance 

