#!/usr/bin/env python
# coding: utf-8

import random
import binary_tree as bt

vide = bt.BinaryTree()


# # Le compte est bon

# Réalisation du célèbre jeu télévisé «Le compte est bon». Pour rappel, il s'agit de réaliser un nombre (choisi au hasard entre 101 et 999) à partir d'un tirage de six nombres (eux aussi choisis au hasard dans un panier comprenenant deux fois tous les nombres de 1 à 10, et une fois chacun des nombres 25, 50, 75 et 100) en utilisant les quatre opérations arithmétiques élémentaires. 
# 
# * Première version : un prédicat qui teste si une solution existe ou pas
# * Deuxième version : une fonction qui trouve l'entier cible si une solution existe ou l'entier réalisable le plus proche dans le cas contraire
# * Troisième version : une fonction qui trouve le meilleur entier réalisable, accompagné d'une explication.


PLAQUES = tuple(range(1, 11)) * 2 +  (10, 25, 50, 75, 100)

# ## Version 1



def add(n1, n2):
    return n1 + n2

def sub(n1, n2):
    return n1 - n2 if n1 > n2 else n2 - n1

def mul(n1, n2):
    return n1 * n2

def div(n1, n2):
    if n2 != 0 and n1 % n2 == 0:
        res = n1 // n2
    elif n1 != 0 and n2 % n1 == 0:
        res = n2 // n1
    else:
        res = None
    return res

OPERATORS = (add, sub, mul, div)

def existe_solution(cible, tirage):
    '''
    :param cible: (int) le nombre à atteindre
    :param tirage: (tuple) une liste de nombres
    :return: (bool)
       - True si cible peut être calculé à partir des entiers contenus dans tirage
       - False sinon
    :CU: tirage ne contient que des entiers > 0
    :Exemples:
    
    >>> [existe_solution(k, (1, 2, 3)) for k in range(1, 11)]
    [True, True, True, True, True, True, True, True, True, False]
    >>> existe_solution(5040, (2, 3, 4, 5, 6, 7))
    True
    >>> existe_solution(5041, (2, 3, 4, 5, 6, 7))
    False
    '''
    n = len(tirage)
    trouve = cible in tirage
    i, j = 0, 1
    while i < n - 1 and j < n and not trouve:
        n1, n2 = tirage[i], tirage[j]
        tirage_restant = tirage[:i] + tirage[i+1:j] + tirage[j+1:]
        ind_op = 0
        while ind_op < len(OPERATORS) and not trouve:
            res = OPERATORS[ind_op](n1, n2)
            if res != None:
                trouve = existe_solution(cible, tirage_restant + (res,))
            ind_op += 1
        if j == n - 1:
            i = i + 1
            j = i + 1
        else:
            j = j + 1
    return trouve


# ## Version 2

def plus_proche(nbres, n):
    '''
    :param nbres: (list ou tuple) une séquence de nombres
    :param n: (int ou float) un nombre
    :return: (int ou float) le nombre de la séquence nbres le plus proche de n
    :CU: nbres ne doit pas être vide
    :Exemples: 
    
    >>> plus_proche((3, 1, 4, 1, 5, 9, 2), 8)
    9
    >>> plus_proche((3, 1, 4, 1, 5, 9, 2), 7)
    5
    '''
    res = nbres[0]
    for elt in nbres:
        if abs(elt - n) < abs(res - n):
            res = elt
    return res


def meilleure_solution(cible, nombres):
    '''
    :param cible: (int) le nombre à atteindre
    :param nombres: (tuple) une liste de nombres
    :return: (int) l'entier le plus proche de cible pouvant être calculé 
        à partir des entiers contenus dans nombres
    :CU: nombres ne contient que des entiers > 0
    :Exemples:
    
    >>> [meilleure_solution(k, (1, 2, 3)) for k in range(1, 11)]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 9]
    >>> [meilleure_solution(k, (2, 3, 4, 5, 6, 7)) for k in (5040, 5041)]
    [5040, 5040]
    '''
    n = len(nombres)
    meilleure = plus_proche(nombres, cible)
    i, j = 0, 1
    while i < n - 1 and j < n and meilleure != cible:
        n1, n2 = nombres[i], nombres[j]
        nbres_restant = nombres[:i] + nombres[i+1:j] + nombres[j+1:]
        ind_op = 0
        while ind_op < len(OPERATORS) and meilleure != cible:
            res = OPERATORS[ind_op](n1, n2)
            if res != None:
                result = meilleure_solution(cible, nbres_restant + (res,))
                if abs(result - cible) < abs(meilleure - cible):
                    meilleure = result
            ind_op += 1
        if j == n - 1:
            i = i + 1
            j = i + 1
        else:
            j = j + 1
    return meilleure


# ## Version 3

# Dans cette version, on représente les solutions du problème par des expressions arithmétiques, elles-mêmes représentées par des arbres binaires dont les nœuds sont étiquetés par des tuples :
# 
# * tuples de longueur 1 contenant uniquement un nombre pour les feuilles
# * tuples de longueur 2 pour les nœuds internes, de la forme (val, op) où val est un nombre, et op une chaîne de caractères représentant une des quatre opérations arithmétiques, le nombre val devant être la valeur de l'opération op sur les nombres val des deux fils.
# 
# Les arbres binaires représentant des expressions arithmétiques sont localement complets


def valeur(expression):
    '''
    :param expression: (BinaryTree) un arbre binaire représentant une expression arithmétique
    :return: (int) la valeur de l'expression
    '''
    return expression.get_root()[0]

SYMBOLES_OPERATEURS = {'+' : '\u002B',
                      '-' : '\u002D',
                      '*' : '\u00D7',
                      '/' : '\u00F7'}

def operateur(expression):
    '''
    :return: (str) le symbole de l'opération arithmétique situé à la racine
    :CU: expression ne doit pas être réduit à une feuille
    '''
    return expression.get_root()[1]


def plus_proche2(expressions, n):
    res = expressions[0]
    for expr in expressions:
        if abs(valeur(expr) - n) < abs(valeur(res) - n):
            res = expr
    return res


def add2(expr1, expr2):
    n1 = valeur(expr1)
    n2 = valeur(expr2)
    return bt.BinaryTree((n1 + n2, '+'), expr1, expr2)

def sub2(expr1, expr2):
    n1 = valeur(expr1)
    n2 = valeur(expr2)
    if n1 > n2:
        res = bt.BinaryTree((n1 - n2, '-'), expr1, expr2)
    else:
        res = bt.BinaryTree((n2 - n1, '-'), expr2, expr1)
    return res

def mul2(expr1, expr2):
    n1 = valeur(expr1)
    n2 = valeur(expr2)
    return bt.BinaryTree((n1 * n2, '*'), expr1, expr2)

def div2(expr1, expr2):
    n1 = valeur(expr1)
    n2 = valeur(expr2)
    if n2 != 0 and n1 % n2 == 0:
        res = bt.BinaryTree((n1 // n2, '/'), expr1, expr2)
    elif n1 != 0 and n2 % n1 == 0:
        res = bt.BinaryTree((n2 // n1, '/'), expr2, expr1)
    else:
        res = None
    return res

OPERATORS2 = (add2, sub2, mul2, div2)

def meilleure_solution2(cible, nombres):
    '''
    :param cible: (int) le nombre à atteindre
    :param nombres: (tuple) une liste de nombres
    :return: (BinaryTree) une expression arithmétique dont la valeur est
        l'entier le plus proche de cible pouvant être calculé 
        à partir des entiers contenus dans nombres
    :CU: nombres ne contient que des entiers > 0
    :Exemples:
    
    >>> [meilleure_solution2(k, (1, 2, 3)).get_root()[0] for k in range(1, 11)]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 9]
    >>> [meilleure_solution2(k, (2, 3, 4, 5, 6, 7)).get_root()[0] for k in (5040, 5041)]
    [5040, 5040]
    '''
    
    def aux(expressions):
        n = len(expressions)
        meilleure = plus_proche2(expressions, cible)
        i, j = 0, 1
        while i < n - 1 and j < n and valeur(meilleure) != cible:
            expr1, expr2 = expressions[i], expressions[j]
            expr_restantes = expressions[:i] + expressions[i+1:j] + expressions[j+1:]
            ind_op = 0
            while ind_op < len(OPERATORS2) and valeur(meilleure) != cible:
                res = OPERATORS2[ind_op](expr1, expr2)
                if res != None:
                    result = aux(expr_restantes + (res,))
                    if abs(valeur(result) - cible) < abs(valeur(meilleure) - cible):
                        meilleure = result
                ind_op += 1
            if j == n - 1:
                i = i + 1
                j = i + 1
            else:
                j = j + 1
        return meilleure
    return aux(tuple(bt.BinaryTree((nbre,), bt.BinaryTree(), bt.BinaryTree()) for nbre in nombres))


def explique(sol):
    if not sol.is_leaf():
        left_sol = sol.get_left_subtree()
        right_sol = sol.get_right_subtree()
        res = explique(left_sol)
        res = res + explique(right_sol)
        res = res + ['{:d} {:s} {:d} = {:d}'.format(valeur(left_sol), 
                                                    operateur(sol), 
                                                    valeur(right_sol), 
                                                    valeur(sol))]
        return res
    else:
        return []

def explique_beau(sol):
    if not sol.is_leaf():
        left_sol = sol.get_left_subtree()
        right_sol = sol.get_right_subtree()
        res = explique_beau(left_sol)
        res = res + explique_beau(right_sol)
        res = res + ['{:d} {:s} {:d} = {:d}'.format(valeur(left_sol), 
                                                    SYMBOLES_OPERATEURS[operateur(sol)], 
                                                    valeur(right_sol), 
                                                    valeur(sol))]
        return res
    else:
        return []

def linearise(sol):
    if sol.is_leaf():
        return str(sol.get_root()[0])
    else:
        return '({:s} {:s} {:s})'.format(linearise(sol.get_left_subtree()),
                                         SYMBOLES_OPERATEURS[operateur(sol)],
                                         linearise(sol.get_right_subtree()))

if __name__ == '__main__':
    from ap2_decorators import trace

    existe_solution = trace(existe_solution)
    existe_solution(2, (4, 8, 25))
    
