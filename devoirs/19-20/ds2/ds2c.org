#+title: DS2 - Éléments de correction 

* Problème A : le jeu du taquin

** Représentation du plateau

*** Q1

    Soit $c$ le numéro de colonne, $l$ le numéro de ligne et $n$ le numéro de case.
    On a $n=(3-c)+4\times (3-l)$. 
    Réciproquement, si $q$ et $r$ désignent respectivement le quotient et le reste 
    de $n$ dans la division euclidienne par 4, on a $c = 3-r$ et $l=3-q$

** Fonctions utiles
*** Q2
    1. ~n & 15~ désigne le reste de la division euclidienne de $n$ par 16.
    2. ~n >> 4~ désigne le quotient de la division euclidiennet de $n$ par 16.
    3. ~n << 4~ désigne le produit de $n$ par 16.
    4. ~(n << 4)|m~ désigne la somme de $16*n$ et de la valeur de $m$.
*** Q3

    #+BEGIN_SRC python :exports code :session taquin
      def seq_to_state(cells):
          """
          :param cells: (list) liste de nombre
          :return: (int) la représentation des cellules
          :CU: sorted(cells) == list(range(0,16))

          :exemples:
    
          >>> seq_to_state([3,2,5,7,15,6,9,4,0,13,8,10,11,14,12,1])
          3627639140613013185
          """
          n = 0
          for c in cells:
              n = (n << 4) | c
          return n
    #+END_SRC

    #+RESULTS:

*** Q4
    #+BEGIN_SRC python :exports code :session taquin
      def extract_four_bits(state, num):
          """
          :param state: (int) un état du plateau
          :param num: (int) le numéro de la case
          :return: (int) la valeur de la case;
          :exemples:

          >>> extract_four_bits(3627639140613013185, 4)
          10
          """
          return (state >> (4*num)) & 0xf

    #+END_SRC

    #+RESULTS:

*** Q5

    #+BEGIN_SRC python :exports code :session taquin
      def valid_state(n):
          """
          :param n: (int) un entier
          :return: (bool) True ssi n représente un taquin
          :exemples:
          >>> valid_state(3627639140613013185)
          True
          >>> valid_state(1<<64)
          False
          """
          return ((n > 0) and (n < (1 << 64)) and
                  sorted(extract_four_bits(n, c) for c in range(16))==list(range(16)))
    #+END_SRC

    #+RESULTS:

*** Q6

    #+BEGIN_SRC python :exports code :session taquin
      def search_cell_num(state, value):
          """
          :param state: (int) un état de taquin
          :param value: (int) une valeur
          :return: (int) le numéro de la case contenant value
          :CU: valid_state(state) and 0 <= value < 16
          :exemples:
          >>> search_cell_num(3627639140613013185, 0)
          7
          """
          num = 0
          while extract_four_bits(state, num) != value:
              num += 1
          return num

    #+END_SRC

    #+RESULTS:

** Une classe ~Taquin~
   
*** Q7

    le constructeur de la classe peut prendre en paramètre :
    - rien : dans ce cas le jeu résolu est créé;
    - un état sous forme d'un entier;
    - une liste de numéro.
    

*** Q8

    #+BEGIN_SRC python :exports code 
      class TaquinError(Exception):
          def __init__(self, msg):
              self.__message = msg

      class Taquin:
          def __init__(self, *args):
              if len(args) > 1:
                  raise TaquinError('Bad number of arguments for taquin creation')
              if len(args) == 0:
                  self.__state = seq_to_state(range(16))
              elif isinstance(args[0], int):
                  if valid_state(args[0]):
                      self.__state = args[0]
                  else:
                      raise TaquinError("Bad int for taquin's board representation")
              elif isinstance(args[0], list) or isinstance(args[0], tuple):
                  if sorted(args[0]) == list(range(16)):
                      self.__state = seq_to_state(args[0])
                  else:
                      raise TaquinError("list argument must contain once all numbers between 0 and 15 ")
              else:
                  raise TaquinError("Bad type argument for taquin creation")
          def get_possible_moves(self):
              """
              :return: (set) l'ensemble des coups possibles
              """
              res = set()
              num = search_cell_num(self.__state, 0)
              c, l = 3 - n % 4, 3 - n // 4
              if c > 0:
                  res.add('L')
              if c < 3:
                  res.add('R')
              if l > 0:
                  res.add('U')
              if l < 3:
                  res.add('D')
              return res
          def move(self, move):
              """
              :return: (Taquin)
              """
              if move not in self.get_possible_moves():
                  raise TaquinError('move not possible')
              num = search_cell_num(self.__state, 0)
              c, l = (3 - num % 4, 3 - num // 4)
              dep = { 'U' : (0,-1), 'D' : (0, 1), 'L' : (-1, 0), 'R' : (1, 0) }
              dc, dl = dep[move]
              dnum = (3-c+dc)+4*(3-l+dl)
              l = list(extract_four_bits(self.__state,15 - n) for n in range(16))
              l[num] = l[dnum]
              l[dnum] = 0
              return Taquin( l )

    #+END_SRC

* Problème B : Résolution des jeux à un joueur

** Parcours en largeur

*** Q1
    
    Voici deux solutions :
    - 1, 2, 3, 4, ..., 41, 42 elle est de profondeur 41
    - 1, 2, 4, 5, 10, 20, 21, 42, de profondeur 7. Si un solution de longueur 6 existait,
      comme 42 n'est pas une puissance de 2, et que les deux premiers coups donnent le même
      résultat, Le résultat sera borné par $(1\times 2+1)\times 2^4=3\times 16=48$.
      Donc 7 coups est optimal.
      En partant de 2, peut-on atteindre 42 en 5 coups ? 3\times 2^4 = 48 ne fonctionne pas. sinon, coup <= (2\times 2 +1)\times 2^3=5\times 8 = 40
    

*** Q2
    
    Soit $i\in\mathbb{N}$, et $A_i$ la valeur de $A$ après
    ième itération. Remarquons que $A_{i+1}=s(A_i)$.

    Dès lors, si le parcours en largeur renvoie ~True~, alors il existe
    $p\in \mathbb{N}$ tel que $A_p$ contient un état final. Notons le $e_p$.

    Par définition de $A_i$, pour tout $i\in p-1, ..., 0$, il existe $e_{i}\in A_i$ tel que
    $s(e_i)=e_{i+1}$.
    Le chemin $e_0e_1\ldots e_p$ est alors une solution.

    Réciproquement, si le jeu admet une solution, il existe une solution optimale $e_0e_1\ldots e_p$

    Soit $P_i$ la propriété suivante :

    $P_i\,:\,\mbox{$A_i$ contient les états accessibles à la profondeur $i$ et tous les états situés à la profondeur $i-1$ on été examinés}$

    La propriété est 
    - vrai pour $i=1$ : à l'issu de la première itération, seul l'état $e_0$ a été examiné et $A_1$
      contient tous les état à la profondeur 1.
    - inductive : si elle est vraie à l'itération $i$, alors à l'étape suivante, on examine tous
      les états de $A_i$, et $A_{i+1}$ contient tous les états situés à une profondeur $i+1$.

    Cette propriété est donc vraie à chaque itération.

    Si une solution optimale de profondeur $p$ existe, alors par optimalité, l'algorithme 
    va effectuer au moins $p-1$ itérations. À la p-ième itération, tous les états situés à 
    la profondeur $p$ sont examinés. Il existe un état final parmi ceux-ci, donc l'algorithme
    s'arrête (au premier état final rencontré) et renvoie ~True~.

*** Q3
    
    On a évidemment $a_p:=\mbox{Card}(A_p)\leq 2^p$. Soit $b_p$ le nombre d'éléments pairs de 
    $A_p$ et $c_p$ le nombre d'éléments impairs. On a:

    $b_{p+1}\geq b_p+c_p$ et $c_{p+1}=b_p$. Par conséquent, $(b_p)$ vérifie $b_{p+1}\geq b_p+b_{p-1}$
    $b_p$ est au moins égal à la suite de Fibonacci, qui est exponentielle, à fortiori $a_p$.

    
*** Q4
    
    #+BEGIN_SRC python :exports code
      def bfs(g):
          A = set(g)
          p = 0
          while len(A) != 0:
              B = set()
              for x in A:
                  if x.is_final():
                      return p
                  B.union(x.next_states())
              A = B
              p += 1
    #+END_SRC

    Lorsqu'aucune solution n'existe, la fonction ne termine pas.

*** Q5

    Si une solution existe, soit $p$ sa profondeur d'une solution optimale. On a vu que l'algorithme
    effectue alors $p+1$ itérations. La valeur calculée dans la variable ~p~ est donc $p$ et est renvoyée.

** Parcours en profondeur

   
*** Q6

    il suffit d'exécuter l'algorthme avec comme paramètres $m$, $e_0$ et $0$.

*** Q7

    La fonction est récursive. Soit $i$ le nombre d'appel récursif et $p_i$ la valeur
    du paramètre ~p~ lors du $i-ème$ appel récursif. On a $p_0=p$ et $p_{i+1}=p_i+1$.

    Si $p_i > m$, alors l'algorithme se termine. Comme $p_i=p+i$, la suite $(p_i)$ et 
    strictement croissante et tend vers l'infini. dès lors elle dépassera n'importe quel
    $m$ à partir d'un certain rang $j$.
    1. si l'algorithme se termine avant le $j$-ième appel récursif, tant mieux !
    2. sinon la première condition est remplie et il renvoie ~False~.

       
*** Q8

    Il y avait une coquille dans l'énoncé : le paramètre ~e~ était inutile.

    #+BEGIN_SRC python :exports code

      def dfs(game, m, p):
          if p > m:
              return False
          if game.is_final():
              return True
          for x in game.next_states():
              if dfs(m, x, p+1):
                  return True
          return False
    #+END_SRC

*** Q9    

    #+BEGIN_SRC python :exports code
      def iterate_dfs(game):
          m = 0
          while not dfs(game, m, 0):
              m += 1
          return m
    #+END_SRC

*** Q10

    Supposons qu'une solution existe et soit $p$ la profondeur d'une solution
    optimale.

    Il suffit de démontrer que l'algorithme ~iterate_dfs~ effectue exactement $p$ itérations.

    Soit $M$ le nombre d'itération.

    L'algorithme ~dfs(game,m, 0)~  examine tous les états 
    situés à une profondeur inférieure ou égale à $m$ en partant d'un état initial.

    - On a donc par optimalité de la solution $p\leq M$.
    - D'autre part on est certain que si l'algorithme effectue $p-1$ itération, 
      alors il s'arrête avant la $p$-ième itération (sur une des solutions optimales)
      dont $M\leq p$.

    Par conséquent $M=p$.

*** Q11
    
    On considère $n$ itérations 

   | Situation | Parcours en largeur                     | Parcours en profondeur        |
   |-----------+-----------------------------------------+-------------------------------|
   |         1 | 1 élément dans $A_p$ à chaque itération | pile des appels de taille $n$ |
   |         2 | 2^n éléments dans $A_p$                 | pile des appels de taille $n$ |

   
** Parcours avec horizon


*** Q12   

    #+BEGIN_SRC python :exports code
      def dfs_star(m, game, p):
          c = p + game.horizon(game.get_state())
          if c > m:
              if c < game.min_depth:
                  game.min_depth = c
              return False
          if game.is_final():
              return True
          for x in game.next_states():
              if dfs_start(x, p+1):
                  return True
          return False

      def iterate_dfs_star(game):
          m = game.horizon(game.get_state())
          while m < float('inf'):
              game.min_depth = float('inf')
              if dfs_star(m, game.get_state(), 0):
                  return True
              m = game.min_depth
          return False

    #+END_SRC

*** Q13

    Si $e$ et $e'$ sont deux états situés à distance $p$, alors
    $e' \leq 2^p e$. Par conséquent, $p \geq \log_2\left \frac{e'}{e}\right$.
    Ceci donne un minorant de la distance.
    
    On peut donc prendre $h(e)=\lfloor\log_2\left(\frac{42}{e}\right)\rfloor$
    comme fonction horizon.

*** Q14

    Si une solution optimale a pour profondeur $p$, alors la quantité nécessaire
    en mémoire est minorée par $2^p$ fois la taille d'un état, soit $2^{p+6}$ bits.

    Le taquin compte au plus $16!$ états (pas tous accessibles). Quelle est la profondeur
    maximale ?

    On considère le graphe des états du taquin : ils s'agit d'un graphe à $16!$ 
    sommets dont les sommets sont de degré 2 ou 4. 

    On peut penser que certains états sont à des profondeurs supérieures au milliard.
    dans ce cas stocker l'ensemble des états est impensable.

    #+BEGIN_SRC python
      def fact(n,p=1):
          if n==0:
              return p
          else:
              return fact(n-1, p*n)


      return fact(16)
    #+END_SRC

    #+RESULTS:
    : 20922789888000

    #+BEGIN_SRC python :exports none :session taquin
      if __name__ == '__main__':
          import doctest
          doctest.testmod(verbose=True)
    #+END_SRC

    #+RESULTS:
    : TestResults(failed=0, attempted=5)

*** Q15
    
    1. Si l'état $e$ est l'état final, alors $h(e)=0$.
    2. Sinon, tout déplacement du taquin ne peut réduire que de 1 la fonction $h$.
       Si a et b sont à distance de 1, alors h(a)-1 <= h(b)
       Si un état $e$ est à une distance $p$ de l'état final $f$, on a donc 
       $h(e)-p \leq h(f)=0$ et donc $h(e)\leq p$.
    


