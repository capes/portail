#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>_'
__date_creation__ = 'Fri Nov  8 16:59:46 2019'
__doc__ = """
:mod:`taquin` module
:author: {:s} 
:creation date: {:s}
:last revision:

This module provides:

* a class Taquin for taquin game board representation

""".format(__author__, __date_creation__)

def cycle_decomposition(perm):
    '''
    :param perm: (list) a list of length n containing all numbers from 0 to n-1
    :return: (set) set of disjoint cycles
    :CU: set(perm) == set(range(len(n)))
    :Examples:

    >>> cycle_decomposition(list(range(16))) == set((k,) for k in range(16))
    True
    >>> cycle_decomposition([0, 2, 3, 1]) == {(0,), (1, 2, 3)}
    True
    >>> cycle_decomposition([2, 5, 0, 7, 6, 3, 4, 1]) == {(0, 2), (1, 5, 3, 7), (4, 6)}
    True
    '''
    res = set()
    for i in range(len(perm)):
        if all(not i in cycle for cycle in res):
            cycle = (i,)
            suiv = perm[i]
            while suiv != i:
                cycle = cycle + (suiv,)
                suiv = perm[suiv]
            res.add(cycle)
    return res

def parity(perm):
    '''
    :param perm: (list) a list of length n containing all numbers from 0 to n-1
    :return: (int) parity of permutation perm : 0 if perm is even, 1 if perm is odd
    :CU: set(perm) == set(range(len(n)))
    :Examples:

    >>> parity(list(range(16)))
    0
    >>> parity([0, 2, 3, 1])
    0
    >>> parity([2, 5, 0, 7, 6, 3, 4, 1])
    1
    '''
    res = 0
    for cycle in cycle_decomposition(perm):
        res = (res + len(cycle) - 1) % 2
    return res
    
def seq_to_state(cells):
    '''
    :param cells: (sequence) a sequence of cells (ints)
    :return: (int) the natural number represented in base 16 by seq, 
           if all ints of seq are between 0 and 15 include, 
    :CU: all values of sequence
    :Examples:

    >>> seq_to_state((1, 0))
    16
    >>> seq_to_state((1, 2, 3))
    291
    >>> seq_to_state([0, 1, 2, 3])
    291
    >>> seq_to_state([])
    0
    >>> '{:016X}'.format(seq_to_state(range(16)))
    '0123456789ABCDEF'
    '''
    res = 0
    for n in cells:
        res = (res << 4) | n
    return res


def extract_four_bits(state, pos):
    '''
    >>> [extract_four_bits(seq_to_state(range(16)), pos) for pos in range(16)]
    [15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    '''
    return (state >> 4*pos) & 15

def set_four_bits(state, pos, bits):
    '''
    >>> '{:016X}'.format(set_four_bits(seq_to_state(range(16)), 3, 11))
    '0123456789ABBDEF'
    '''
    mask = ((1 << 64) - 1) ^ (15 << (4*pos))
    return (state & mask) | (bits << (4*pos))

def valid_state(state):
    '''
    >>> valid_state(-1)
    False
    >>> valid_state(291)
    False
    >>> import random
    >>> l = list(range(16))
    >>> random.shuffle(l)
    >>> valid_state(seq_to_state(l))
    True
    '''
    if state < 0:
        return False
    else:
        hexa_digits = tuple(extract_four_bits(state, pos) for pos in range(16))
        return set(hexa_digits) == set(range(16))

def search_cell_num(state, value):
    '''
    >>> search_cell_num(seq_to_state(range(16)), 0)
    15
    >>> search_cell_num(seq_to_state(range(15, -1, -1)), 0)
    0
    >>> state = seq_to_state([3, 5, 2, 7, 15, 6, 9, 4, 0, 13, 8, 10, 11, 14, 12, 1])
    >>> [search_cell_num(state, value) for value in range(16)]
    [7, 0, 13, 15, 8, 14, 10, 12, 5, 9, 4, 3, 1, 6, 2, 11]
    '''
    pos = 0
    while extract_four_bits(state, pos) != value:
        pos += 1
    return pos

class TaquinError(Exception):
    def __init__(self, msg):
        self.__message = msg

class Taquin(object):
    '''
    >>> taq1 = Taquin()
    >>> taq1.get_state()
    1311768467463790320
    >>> taq2 = Taquin(seq_to_state(15 - k for k in range(16)))
    >>> '{:016X}'.format(taq2.get_state())
    'FEDCBA9876543210'
    >>> taq3 = Taquin([3, 5, 2, 7, 15, 6, 9, 4, 0, 13, 8, 10, 11, 14, 12, 1])
    >>> '{:016X}'.format(taq3.get_state())
    '3527F6940D8ABEC1'
    >>> taq1.get_possible_moves() == {'D', 'R'}
    True
    >>> taq2.get_possible_moves() == {'D', 'R'}
    True
    >>> taq3.get_possible_moves() == {'D', 'U', 'L'}
    True
    >>> print(taq3)
    +--+--+--+--+
    | 3| 5| 2| 7|
    +--+--+--+--+
    |15| 6| 9| 4|
    +--+--+--+--+
    |  |13| 8|10|
    +--+--+--+--+
    |11|14|12| 1|
    +--+--+--+--+
    >>> taq4 = taq3.move('D')
    >>> print(taq4)
    +--+--+--+--+
    | 3| 5| 2| 7|
    +--+--+--+--+
    |  | 6| 9| 4|
    +--+--+--+--+
    |15|13| 8|10|
    +--+--+--+--+
    |11|14|12| 1|
    +--+--+--+--+
    '''
    POSSIBLE_MOVES = {
        0  : {'D', 'R'},
        1  : {'L', 'D', 'R'},
        2  : {'L', 'D', 'R'},
        3  : {'L', 'D'},
        4  : {'U', 'D', 'R'},
        5  : {'U', 'L', 'D', 'R'},
        6  : {'U', 'L', 'D', 'R'},
        7  : {'U', 'L', 'D'},
        8  : {'U', 'D', 'R'},
        9  : {'U', 'L', 'D', 'R'},
        10 : {'U', 'L', 'D', 'R'},
        11 : {'U', 'L', 'D'},
        12 : {'U', 'R'},
        13 : {'U', 'L', 'R'},
        14 : {'U', 'L', 'R'},
        15 : {'U', 'L'},
    }
    
    CELL_TO_MOVE = {'D' : 4, 'R' : 1, 'U' : -4, 'L' : -1}
    
    def __init__(self, *args):
        if len(args) > 1:
            raise TaquinError('Bad number of arguments for taquin creation')
        if len(args) == 0:
            self.__state = seq_to_state(list(range(1, 16)) + [0])
        elif isinstance(args[0], int):
            if valid_state(args[0]):
                self.__state = args[0]
            else:
                raise TaquinError("Bad int for taquin's board representation")
        elif isinstance(args[0], list) or isinstance(args[0], tuple):
            if sorted(args[0]) == list(range(16)):
                self.__state = seq_to_state(args[0])
            else:
                raise TaquinError("list argument must contain once all numbers between 0 and 15 ")
        else:
            raise TaquinError("Bad type argument for taquin creation")
        self.__pos_empty_cell = search_cell_num(self.__state, 0)
        self.__set_possible_moves = Taquin.POSSIBLE_MOVES[self.__pos_empty_cell]

        
    def get_state(self):
        return self.__state

    def is_solvable(self):
        state = self.get_state()
        perm = [(state >> (60 - 4*k) & 15) - 1 for k in range(16)]
        ind_vide = perm.index(-1)
        perm[ind_vide] = 15
        distance_vide = (3 - ind_vide//4) + (3 - ind_vide%4)
        return (parity(perm) + distance_vide) % 2 == 0
        
    def get_possible_moves(self):
        return self.__set_possible_moves

    def move(self, dir):
        if not dir in self.get_possible_moves():
            raise TaquinError("Non valid move")
        pos_empty = self.__pos_empty_cell
        pos_to_move = pos_empty + Taquin.CELL_TO_MOVE[dir]
        state = self.get_state() 
        cell_to_move = extract_four_bits(state, pos_to_move)
        state = set_four_bits(state, pos_empty, cell_to_move)
        state = set_four_bits(state, pos_to_move, 0)
        return Taquin(state)
    
    def __str__(self):
        state = self.get_state()
        trait_horizontal = '\n+' + '--+' * 4 
        res = trait_horizontal
        for i in range(4):
            ligne = '\n|'
            for j in range(4):
                n = (state >> (60 - 16*i - 4*j)) & 15
                if n != 0:
                    ligne = ligne + '{:2d}|'.format(n)
                else:
                    ligne = ligne + '  |'
            res = res + ligne + trait_horizontal
        return res

class GameTaquin(object):
    def __init__(self, taquin):
        self.__taquin = taquin
        self.min_depth = 0
        
    def initial(self):
        return self.__taquin.get_state()

    def final_states(self):
        return {seq_to_state(list(range(1, 16)) + [0])}

    def next_states(self, state):
        taq = Taquin(state)
        nexts = set()
        for dir in taq.get_possible_moves():
            nexts.add(taq.move(dir).get_state())
        return nexts

    def horizon(self, state):
        res = 0
        for value in range(1, 16):
            pos = search_cell_num(state, value)
            e_i_v = (15 - pos) // 4
            e_j_v = (15 - pos) % 4
            res += abs(e_i_v - ((value - 1) // 4)) + abs(e_j_v - ((value - 1) % 4))
        return res
        
if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
