Calendrier M1
=============

Dates des ds :

- [x] 11/10, ue a1, 13h00-18h00, m1 Chatelet
- [x] 6/12, ue a2, 13h00-18h00, bat. a5
- [x] 24/01, ue a1
- [x] 26/03, rattrapage a1 s1
- [x] 9/04, ue a2

Remarques :

Anglais :

- [x] ds mardi 19 décembre
- [x] ds 16 mai
- [x] jeudi 7/03
- [x] jeudi 14/03
- [x] jeudi 21/03
- [x] jeudi 28/03
- [x] jeudi 11/04
- [x] jeudi 18/04

ue b3 :

- [x] mardi 24 octobre, 8h30
- [x] lundi 6 mai, 13h00-16h00
- [x] lundi 13 mai, 13h00-16h00
- [x] mercredi 15 mai, 8h30-10h00
- [x] mercredi 22 mai, 8h30-10h00
- [x] mercredi 29 mai, 8h30-10h00

ue c3 :

- [x] mardi 21 mai : oral s2 (présenter son thème et les trois articles)

modules complémentaires :

- [x] mercredi 20/03
- [x] mercredi 27/03
- [x] mercredi 3/04
- [x] jeudi 4/04
- [x] mercredi 10/04

semaine 22
----------

### mercredi 29 mai ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | sup 209 | ue b3 | M. Guichard |
| 10h15-11h45 | sup 117 | ue a2 | B. Papegay  |

### jeudi 30 mai ###

| Quand       | Où      | Quoi         | Qui        |
|-------------|---------|--------------|------------|
| 13h00-14h30 | m3 226  | ue a1        | P. Thibaud |
| 14h45-16h15 | sup 117 | ue a2/a3(tp) | P. Thibaud |

[chiffrements](./ue1/chiffrement/README.md)

### vendredi 31 mai ###

matin : jury s2


semaine 21
----------

### mardi 21 mai ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 8h30-10h00  | sup 117 | ue c1/c3 | B. Papegay |
| 10h15-11h45 | sup 117 | ue c1/c3 | B. Papegay |

### mercredi 22 mai ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | sup 209 | ue b3 | M. Guichard |
| 10h15-11h45 | sup 117 | ue a2 | P. Thibaud  |

### jeudi 23 mai ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 8h30-10h00  | sup 117   | ue a1        | B. Papegay |
| 10h15-11h45 | sup 117   | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | sup 117   | ue a2/a3(tp) | P. Thibaud |

[chiffrements](./ue1/chiffrement/README.md)

semaine 20
----------

### lundi 13 mai ###

| Quand       | Où               | Quoi  | Qui         |
|-------------|------------------|-------|-------------|
| 13h00-16h00 | b09 - salle Mars | ue b3 | M. Guichard |

### mardi 14 mai ###


### mercredi 15 mai ###

| Quand       | Où      | Quoi     | Qui         |
|-------------|---------|----------|-------------|
| 8h30-10h00  | sup 209 | ue b3    | M. Guichard |
| 10h15-11h45 | sup 117 | ue c1/c3 | B. Papegay  |

### jeudi 16 mai ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 8h30-10h00  | sup 117   | ue a1        | B. Papegay  |
| 10h15-11h45 | sup 117   | ue a1        | B. Papegay  |
| 13h00-14h15 | m3 Turing | ue a1 (oral) | Ph. Marquet |
| 14h30-15h30 | m5 a4     | ue a2/a3(tp) | B. Papegay  |
| 15h45-17h45 | sup       | anglais      | C. Demailly |

semaine 19
----------

### lundi 6 mai ###

| Quand       | Où               | Quoi  | Qui        |
|-------------|------------------|-------|------------|
| 13h00-16h00 | b09 - salle Mars | ue b3 | M. Guichard |

### mardi 7 mai ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | sup 116 | ue a1 | B. Papegay |
| 10h15-11h45 | sup 116 | ue a1 | B. Papegay |
| 13h00-14h30 | sup 117 | ue c  | B. Papegay |

semaine 18
----------

interruption pédagogique

semaine 17
----------

interruption pédagogique

semaine 16
----------

### mardi 16 avril ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 10h15-11h45 | sup 116 | ue a1 | B. Papegay |
| 13h00-14h30 | sup 117 | ue a1 | B. Papegay |
| 14h45-16h15 | sup 117 | ue c  | B. Papegay |


### mercredi 17 avril ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 8h30-10h00  | sup 117   | ue c1/c3     | B. Papegay |
| 10h15-11h45 | m3 Turing | ue a1 (oral) | B. Papegay |
| 13h00-14h30 | m3 226    | ue a2        | P. Thibaud |


### jeudi 18 avril ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 8h30-10h00  | sup 11x   | ue a1        | B. Papegay  |
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay  |
| 13h00-14h15 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h30-15h30 | m5 a4     | ue a2/a3(tp) | P. Thibaud  |
| 15h45-17h45 | sup       | anglais      | C. Demailly |

[résolution du taquin](./ue1/pile_et_file/file.md)

semaine 15
----------

### mardi 9 avril ###

| Quand       | Où | Quoi  | Qui        |
|-------------|----|-------|------------|
| 13h00-18h00 | a5 | ds a2 | P. Thibaud |

### mercredi 10 avril ###

**modules complémentaires** >(

### jeudi 11 avril ###

| Quand       | Où        | Quoi         | Qui               |
|-------------|-----------|--------------|-------------------|
| 8h30-10h00  | m3 226    | ue a2        | P. Thibaud        |
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay        |
| 13h00-14h15 | m3 Turing | ue a1 (oral) | P. Thibaud        |
| 14h30-15h30 | m5 a4     | ue a2/a3(tp) | P. Thibaud        |
| 15h45-17h45 | sup       | anglais      | C. Demailly       |

[transformations bijectives d'images](./ue1/bijection-images/readme.md)

semaine 14
----------

### mardi 2 avril ###

| Quand       | Où         | Quoi  | Qui        |
|-------------|------------|-------|------------|
| 8h30-10h00  | p4 117/119 | ue a1 | B. Papegay |
| 10h15-11h45 | sup 116    | ue a1 | B. Papegay |
| 13h00-16h00 | sup 116    | ue c  | B. Papegay |

### mercredi 3 avril ###

**modules complémentaires** >(

### jeudi 4 avril ###

**modules complémentaires** ?


semaine 13
----------

### mardi 26 mars ###

| Quand       | Où         | Quoi              | Qui         |
|-------------|------------|-------------------|-------------|
| 8h30-10h00  | p4 117/119 | ue a1             | B. Papegay  |
| 10h15-11h45 | m3 Turing  | ue a1, archi      | Ph. Marquet |
| 13h00-16h00 | m3 226     | rattrapage ue1 s1 | B. Papegay  |

* [Architecture des ordinateurs, dernière séance](./ue1/archi/Readme.md) 
* [Pile débranchée](./ue1/pile_debranchee/Readme.md) 

### mercredi 27 mars ###

**modules complémentaires** >(

### jeudi 28 mars ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 8h30-10h00  | m3 Turing | ue a2        | P. Thibaud  |
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay  |
| 13h00-14h15 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h30-15h30 | m5 a4     | ue a2/a3(tp) | P. Thibaud  |
| 15h45-17h45 | sup       | anglais      | C. Demailly |

semaine 12
----------

### mardi 19 mars ###

| Quand       | Où         | Quoi               | Qui        |
|-------------|------------|--------------------|------------|
| 8h30-10h00  | p4 117/119 | ue a1              | B. Papegay |
| 10h15-11h45 | sup 116    | ue c1/c3           | B. Papegay |
| 13h00-14h30 | sup 116    | entrainement écrit | B. Papegay |

- [redaction fiche mémoire](ue3/cadrage/fiches-lecture.md)
- [knn vs classifier](ue1/knn_vs_classifier/tp_hp_knn.md)

### mercredi 20 mars ###

**modules complémentaires** >(

### jeudi 21 mars ###

| Quand       | Où      | Quoi         | Qui                  |
|-------------|---------|--------------|----------------------|
| 8h30-10h00  | m3 226  | ue a2        | P. Thibaud           |
| 10h15-11h45 | sup 209 | ue a1        | B. Papegay           |
| 13h00-15h30 | m5 a4   | ue a2/a3(tp) | B. Papegay/P.Thibaud |
| 15h45-17h45 | sup     | anglais      | C. Demailly          |

semaine 11
----------

### mardi 12 mars ###

| Quand       | Où         | Quoi               | Qui        |
|-------------|------------|--------------------|------------|
| 8h30-10h00  | p4 117/119 | ue a1              | B. Papegay |
| 10h15-11h45 | sup 116    | ue c1/c3           | B. Papegay |
| 13h00-14h30 | sup 116    | entrainement écrit | B. Papegay |

- [knn vs classifier](ue1/knn_vs_classifier/tp_hp_knn.md)

### mercredi 13 mars ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 8h30-10h00  | sup 209   | ue b3    | M. Guichard |
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1    | B. Papegay  |

### jeudi 14 mars ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay  |
| 13h00-14h15 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h30-15h30 | m5 a4     | ue a2/a3(tp) | P. Thibaud  |
| 15h45-17h45 | sup       | anglais      | C. Demailly |

semaine 10
----------

### mardi 5 mars ###

| Quand       | Où         | Quoi               | Qui        |
|-------------|------------|--------------------|------------|
| 8h30-10h00  | p4 117/119 | ue a1              | B. Papegay |
| 10h15-11h45 | sup 116    | ue c1/c3           | B. Papegay |
| 13h00-14h30 | sup 116    | entrainement écrit | B. Papegay |

### mercredi 6 mars ###

| Quand         | Où        | Quoi     | Qui         |
|---------------|-----------|----------|-------------|
| 8h30-10h00    | sup 209   | ue b3    | M. Guichard |
| 10h15-11h45   | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30   | m3 Turing | ue a1    | B. Papegay  |
| 14h45-&-16h15 | sup 116   | ue a     | B. Papegay  |


### jeudi 7 mars ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay  |
| 13h00-14h15 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h30-15h30 | m5 a4     | ue a2/a3(tp) | P. Thibaud  |
| 15h45-17h45 | sup       | anglais      | C. Demailly |

- [arbres de décision](./ue1/classification/arbre_decision.org)

semaine 9
---------

interruption pédagogique
 
semaines 6 à 8
--------------

stage

semaine 5
---------

### lundi 29 janvier ###

_si vous le souhaitez_, tp avec les m2 en m5 a4, entre 13h30 et 16h30.

### mardi 30 janvier ###

| Quand       | Où         | Quoi               | Qui        |
|-------------|------------|--------------------|------------|
| 8h30-10h00  | p4 117/119 | ue a1              | B. Papegay |
| 10h15-11h45 | sup 116    | ue c1/c3           | B. Papegay |
| 13h00-14h30 | sup 116    | entrainement écrit | B. Papegay |

[redimensionnement d'image](ue1/progdyn/tp-images/Readme.md)

### mercredi 31 janvier ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 8h30-10h00  | sup 209   | ue b3    | M. Guichard |
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30 | m3 Turing | ue a1    | B. Papegay  |
| 14h45-&-h15 | sup 116   | ue a     | B. Papegay  |


### jeudi 1 février ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a4     | ue a2/a3(tp) | P. Thibaud |

- [tp3 http](./ue2/reseaux/tp3_http/tp_http.md)

semaine 4
---------

### mardi 23 janvier ###

| Quand       | Où         | Quoi               | Qui        |
|-------------|------------|--------------------|------------|
| 8h30-10h00  | p4 117/119 | ue a1              | B. Papegay |
| 10h15-11h45 | sup 116    | ue c1/c3           | B. Papegay |
| 13h00-14h30 | sup 116    | entrainement écrit | B. Papegay |


### mercredi 24 janvier ###

| Quand      | Où | Quoi | Qui        |
|------------|----|------|------------|
| 8h00-13h00 | a5 | ds 2 | B. Papegay |


### jeudi 25 janvier ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a4     | ue a2/a3(tp) | P. Thibaud |

- [tp2 tcp/ip](./ue2/reseaux/tp2_tcp/tp2_tcp.md)
- [les filtres de Bloom](./ue1/bloom/bloom.md)

semaine 3
---------

### mardi 16 janvier ###

| Quand       | Où         | Quoi     | Qui        |
|-------------|------------|----------|------------|
| 8h30-10h00  | p4 117/119 | ue a1    | B. Papegay |
| 10h15-11h45 | sup 116    | ue c1/c3 | B. Papegay |
| 13h00-14h30 | sup 116    | ue c1/c3 | B. Papegay |


### mercredi 17 janvier ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 8h30-10h00  | sup 209   | ue b3    | M. Guichard |
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1    | B. Papegay  |

### jeudi 18 janvier ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | sup 209   | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a4     | ue a2/a3(tp) | P. Thibaud |



semaine 2
---------

### mardi 9 janvier ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 8h30-10h00  | sup 116 | ue c1/c3 | B. Papegay |
| 10h15-11h45 | sup 116 | ue a1    | B. Papegay |


### mercredi 10 janvier ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 8h30-10h00  | sup 209   | ue b3    | M. Guichard |
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1    | B. Papegay  |

* [algorithme de Huffman](./ue1/huffman/Readme.md)

### jeudi 11 janvier ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | sup 209   | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a4     | ue a2/a3(tp) | P. Thibaud |

* [tp1 filius](./ue2/reseaux/tp1_filius/tp1_filius.md)

semaine 1
---------

interruption pédagogique

semaine 52
----------

interruption pédagogique

semaine 51
----------

### Lundi 18 décembre ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | sup 120 | ue a1 | B. Papegay  |
| 10h15-11h45 | sup 120 | ue a1 | B. Papegay  |


### mardi 19 décembre ###

| Quand       | Où     | Quoi         | Qui                     |
|-------------|--------|--------------|-------------------------|
| 13h30-15h30 | sup-10 | anglais oral | Charles-Olivier Demailly |
| 15h45-16h00 | sup-10 | anglais ds   | Charles-Olivier Demailly |


### mercredi 20 décembre ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3    | M. Guichard |
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1    | B. Papegay  |

### jeudi 21 décembre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | sup 116   | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a4     | ue a2/a3(tp) | P. Thibaud |

* [arbres rouge-noir](./ue1/abr)

semaine 50
----------

### Lundi 11 décembre ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | sup 120 | ue a1 | B. Papegay  |
| 10h15-11h45 | sup 120 | ue a1 | B. Papegay  |
| 14h-16h     | m3 226  | ue a1 | Ph. Marquet |

* [Architecture des ordinateurs](./ue1/archi/Readme.md) 

### mardi 12 décembre ###

| Quand       | Où     | Quoi      | Qui                     |
|-------------|--------|-----------|-------------------------|
| 15h45-17h45 | sup-10 | anglais ? | Charles-Oliver Demailly |


### mercredi 13 décembre ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3    | M. Guichard |
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1    | B. Papegay  |

### jeudi 14 décembre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
|             |           |              |            |
| 10h15-11h45 | m1 Fatou  | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a4    | ue a2/a3(tp) | P. Thibaud |


semaine 49
----------

### Lundi 4 décembre ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | sup 120 | ue a1 | B. Papegay |
| 10h15-11h45 | sup 120 | ue a1 | B. Papegay |


### mardi 5 décembre ###

| Quand       | Où     | Quoi      | Qui                     |
|-------------|--------|-----------|-------------------------|
| 10h15-11h45 | M5-A12 | ue a1     | Ph. Marquet             |
| 13h15-15h15 | M3-226 | ue a1     | Ph. Marquet             |
| 15h45-17h45 | sup-10 | anglais ? | Charles-Oliver Demailly |


* [Système d'exploitation, dernière séance](./ue1/system/Readme.md) 
* [Architecture des ordinateurs, 1re séance](./ue1/archi/Readme.md) 

### mercredi 6 décembre ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-18h00 | a5        | ds a2    |             |


### jeudi 7 décembre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a14    | ue a2/a3(tp) | P. Thibaud |

* [tp arbres binaire](./ue1/arbres/arbre2.md)

semaine 48
----------

_stage_

semaine 47
----------

_stage_

semaine 46
----------

_stage_

semaine 45
----------

### Lundi 6 novembre ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | sup 120 | ue a1 | B. Papegay |
| 10h15-11h45 | sup 120 | ue a1 | B. Papegay |

### Mardi 7 novembre ###

| Quand       | Où       | Quoi    | Qui                     |
|-------------|----------|---------|-------------------------|
| 10h15-11h45 | m1 Fatou | ue a1   | L. Noé                  |
| 15h45-17h45 | sup-10   | anglais | Charles-Oliver Demailly |

### Mercredi 8 novembre ###

| Quand       | Où        | Quoi     | Qui         |
|-------------|-----------|----------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3    | M. Guichard |
| 10h15-11h45 | m3 Turing | ue c1/c3 | P. Thibaud  |
| 13h00-14h30 | sup 117   | ue a1    | B. Papegay  |


### Jeudi 9 novembre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | sup 118   | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a15    | ue a2/a3(tp) | P. Thibaud |

*tp sql* : [sql](./ue1/sql1/Readme.md) , ou

*tp arbre* : [arbre](./ue1/arbres/arbre2.md)

### Vendredi 10 novembre ###

*matin* : inspé selon calendrier

semaine 44
----------

interruption pédagogique bien méritée

semaine 43
----------

### Lundi 23 octobre ###

### Mardi 24 octobre ###

| Quand       | Où        | Quoi    | Qui                     |
|-------------|-----------|---------|-------------------------|
| 8h30-10h00  | m3 Turing | ue b3   | M. Guichard             |
| 10h15-11h45 | m1 Fatou  | ue a1   | Ph. Marquet             |
| 13h00-14h30 | sup 116   | ue c    | B. Papegay              |
| 15h45-17h45 | sup-10    | anglais | Charles-Oliver Demailly |


### Mercredi 25 octobre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1 | B. Papegay  |


### Jeudi 26 octobre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a14    | ue a2/a3(tp) | P. Thibaud |

*tp* : [sql1](./ue1/sql1/Readme.md)

### Vendredi 27 octobre ###

*matin* : inspé selon calendrier

semaine 42
----------

### Lundi 16 octobre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 8h30-10h00  | sup 120 | ue c1/c3 | B. Papegay |
| 10h15-11h45 | sup 120 | ue c1/c3 | B. Papegay |

*ue c* : les tris linéaires

### Mardi 17 octobre ###

| Quand       | Où       | Quoi    | Qui                     |
|-------------|----------|---------|-------------------------|
| 10h15-11h45 | m1 Fatou | ue a1   | L. Noé                  |
| 15h45-17h45 | sup-10   | anglais | Charles-Oliver Demailly |


### Mercredi 18 octobre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1 | B. Papegay  |


### Jeudi 19 octobre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a15    | ue a2/a3(tp) | P. Thibaud |

*tp* : [sql1](./ue1/sql1/Readme.md)

### Vendredi 20 octobre ###

*matin* : inspé selon calendrier

semaine 41
----------

### Lundi 9 octobre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 8h30-10h00  | sup 120 | ue c1/c3 | B. Papegay |
| 10h15-11h45 | sup 120 | ue c1/c3 | B. Papegay |

*ue c* : les tris linéaires

### Mardi 10 octobre ###

| Quand       | Où       | Quoi    | Qui                     |
|-------------|----------|---------|-------------------------|
| 10h15-11h45 | m1 Fatou | ue a1   | B. Papegay              |
| 15h45-17h45 | sup-10   | anglais | Charles-Oliver Demailly |

*a1* le tri rapide, exercice synthèse sur les tri

### Mercredi 11 octobre ###

| Quand       | Où          | Quoi  | Qui         |
|-------------|-------------|-------|-------------|
| 8h30-10h00  | m1 Fatou    | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing   | ue a2 | P. Thibaud  |
| 13h00-18h00 | m1 Châtelet | ds a1 |             |


### Jeudi 12 octobre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a15    | ue a2/a3(tp) | P. Thibaud |

*a1* les données : généralités, missions d'un sgbd, 

*tp* : [base64](./ue1/base64/readme.md)

### Vendredi 13 octobre ###

*matin* : inspé selon calendrier


semaine 40
----------

### Lundi 2 octobre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 8h30-10h00  | sup 120 | ue c1/c3 | B. Papegay |
| 10h15-11h45 | sup 120 | ue c1/c3 | B. Papegay |

- *tp* : [analyse des tris](./ue1/analyse_tris/Readme.md)


### Mardi 3 octobre ###

| Quand       | Où       | Quoi  | Qui    |
|-------------|----------|-------|--------|
| 10h15-11h45 | m1 Fatou | ue a1 | L. Noé |


### Mercredi 4 octobre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1 | B. Papegay  |

*a1* : le tri par insertion, algorithme et analyse

### Jeudi 5 octobre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a15    | ue a2/a3(tp) | P. Thibaud |

*a1* : le tri fusion
*tp* : [base64](./ue1/base64/readme.md)

### Vendredi 6 octobre ###

*matin* : inspé selon calendrier


semaine 39
----------

### Lundi 25 septembre ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | sup 117 | ue a1 | B. Papegay |
| 10h15-11h45 | sup 117 | ue c  | B. Papegay |

- [listes récursives](./ue1/listes/listes.md)

### Mardi 26 septembre ###

| Quand      | Où      | Quoi  | Qui         |
|------------|---------|-------|-------------|
| 9h45-11h45 | m5 a13  | ue a1 | Ph. Marquet |


### Mercredi 27 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1 | B. Papegay  |


### Jeudi 28 septembre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a15    | ue a2/a3(tp) | P. Thibaud |

*tp :* [poo en python](./ue1/poo_python/readme.md)

### Vendredi 29 septembre ###

*matin* : inspé selon calendrier

semaine 38
----------

### Lundi 18 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8h30-10h00  | m1 302 | ue a1 | B. Papegay |
| 10h15-11h45 | m1 302 | ue c2 | B. Papegay |


### Mardi 19 septembre ###

| Quand       | Où       | Quoi  | Qui    |
|-------------|----------|-------|--------|
| 10h15-11h45 | m1 Fatou | ue a1 | L. Noé |


### Mercredi 20 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1 | B. Papegay  |



### Jeudi 21 septembre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a15    | ue a2/a3(tp) | P. Thibaud |

* [tp - markdown](./ue2/markdown/Readme.md)

### Vendredi 22 septembre ###

*matin* : inspé selon calendrier

semaine 37
----------

### Lundi 11 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8h30-10h00  | m1 302 | ue a1 | B. Papegay |
| 10h15-11h45 | m1 302 | ue c2 | B. Papegay |


### Mardi 12 septembre ###

| Quand       | Où     | Quoi          | Qui         |
|-------------|--------|---------------|-------------|
| 10h15-11h45 | M5-A13 | ue a1 (tp)    | Ph. Marquet |
| 13h15-15h15 | M3-226 | ue a1 (cours) | Ph. Marquet |

* [Système d'exploitation, 1res séances](./ue1/system/Readme.md) 

### Mercredi 13 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m1 Fatou  | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | m1 Fatou  | ue a1 | B. Papegay  |

* [Présentation épreuve de leçons](./ue1/epreuve_oral1/)


### Jeudi 14 septembre ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 10h15-11h45 | m1 Fatou  | ue c1/c3     | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a15    | ue a2/a3(tp) | P. Thibaud |

* [Planning passage leçons](./passages-oraux-23.md)
* [tp - phrases de passe](./ue1/phrases_de_passe/readme.md)

### Vendredi 15 septembre ###

*matin* : inspé selon calendrier

Semaine 36
----------
    
### Jeudi 7 septembre ###

  - pré-rentrée (Amphi Turing, Bâtiment M3) à 13h00
  

### Vendredi 8 septembre ###

inspé selon calendrier
