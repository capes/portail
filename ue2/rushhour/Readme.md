# Objectifs

- Modéliser puis programmer en python le jeu RushHour afin de l'exploiter avec des élèves.
- Utiliser PlantUML pour concevoir des diagrammes de classe
- Utiliser MkDocs pour générer une documentation

# Présentation du jeu

*Rush Hour* (*Heure de pointe* en anglais), est un casse-tête dont le but est de faire sortir d'un terrain de jeu rectangulaire une voiture, alors que le chemin vers la sortie est encombré par d'autres véhicules.
Chaque véhicule se déplace soit horizontalement, soit verticalement (mais pas les deux).  

La voiture rouge (nommée `Z`) doit sortir par la droite du plateau (pour en savoir plus : [Wikipedia](https://fr.wikipedia.org/wiki/Rush_hour_(casse-tête)))

Le casse-tête contient une quarantaine de cartes représentant chacune une configuration à résoudre comme par exemple :

|configuration de départ|solution|
|:------:|:------:|
|<img src="rush002.jpg" width=150px/>|<img src="rush005.jpg" width=150px/>|

Le jeu se déroule sur un plateau de __6 lignes et 6 colonnes__, la voiture `Z` est toujours sur la troisième ligne.
La liste des véhicules disponibles avec le jeu est la suivante :

<img src="rush001.jpg" width=150px/>

## Quelques configurations à résoudre

|<img src="Probleme1.png" width=100px/> |<img src="Probleme2.png" width=100px/>|&nbsp;|
|:------:|:------:|:------:|
|<img src="Probleme38.png" width=100px/> |<img src="Probleme39.png" width=100px/>  |<img src="Probleme40.png" width=100px/>|

# Modélisation/Implantation

## Représentation des configurations

Une __configuration__ est définie par l'ensemble des véhicules utilisés sur le plateau munis de leur position et de leur orientation (verticale ou horizontale).

On doit pouvoir créer un plateau vide, y ajouter un véhicule, vérifier qu’une configuration est valide (pas de véhicules qui se chevauchent, la voiture rouge sur la troisième ligne).

On propose deux fichiers dans lesquels sont représentées plusieurs configurations : [niveaux débutants](./beginner.conf) et [experts](./expert.conf).

__1)__ Proposer une fonction `readconfs(file:str)->list[set[str]]` acceptant un fichier formaté comme ceux fournis et renvoyant une liste de configurations (chacune étant un ensemble).

```python
>>> readconfs("beginner.conf")[0]
{'C;4,4;H', 'O;1,0;V', 'Q;5,2;H', 'Z;2,1;H', 'P;1,3;V', 'B;4,0;V', 'R;0,5;V', 'A;0,0;H'}
```

__2)__ Quels choix s'imposent à vous sur les types et les valeurs fournies dans ce fichier.
Essayez de représenter le plateau correspondant (sur papier)

__3)__ La lecture de fichier est-elle au programme de la spécialité NSI ?  
Avez vous utilisez dans son implantation d'autres structures de données ou méthodes
non explicitement au programme (énumérez-les) ?
Quelle solution choisiriez vous alors pour quand même l'utiliser ?

## POO

On utilisera la programmation orientée objet pour modéliser le jeu de RushHour.

__3)__ Proposer un diagramme UML modélisant un Vehicule présent sur le plateau.  
Vous utiliserez le langage [PlantUML](https://plantuml.com/fr/) qui s'intégrera parfaitement à un document markdown. Par exemple :

<pre>
```puml
@startuml
class Person{
    .. Constructeur ..
    + __init__(firstname : str, lastname :str)
    .. Méthodes ..
    + get_firstname() : str
    + get_lastname() : str
}
@enduml
```
</pre>

sera rendu comme  : 
![ceci](./exempleUML.jpg)

Pensez à installer les outils décrits sur le site officiel pour générer en local les images des diagrammes. Sinon, vous pouvez toujours dans un premier temps, après avoir étudié les exemples et la documentation, utiliser des outils en ligne comme https://www.planttext.com/ pour générer une image correspondant au diagramme.

__Vous compléterez les diagrammes UML au fur et à mesure par la suite__

__4)__ Il sera nécessaire de pouvoir déplacer le véhicule sur le plateau (pas encore modélisé).

__a-__ Afin d'éviter d'écrire trop de structures conditionnelles dans votre code, proposez une façon élégante de représenter dans une structure de donnée bien choisie les changements de position liés à un déplacement élémentaire (on pourra aussi remarquer que pour une orientation donnée, on peut se limiter à des déplacements élémentaires caractéristiques dans seulement deux directions).

__b-__ Où placer cette nouvelle structure de données dans votre définition de classe (justifier)

__5)__ Proposez alors une implantation de la méthode `move(self, direction:str)` changeant la position du Véhicule en fonction de la direction fournie en paramètre : il est question d'un déplacement élémentaire.

## Plateau de Jeu

On considérera une classe `RushHour` représentant le plateau de jeu.

__6)__ Proposez un début de diagramme UML pour cette classe.
On souhaite notamment pouvoir :

* parcourir efficacement l'ensemble des véhicules présents sur le plateau
* déterminer en chaque endroit du plateau le véhicule présent( ou non d'ailleurs)

Vous justifierez notamment le choix des structures de données utilisées pour les attributs.

__7)__ Un plateau de jeu peut être généré à partir d'une configuration, elle-même issue d'un fichier.
Proposer une manière de réutiliser la fonction `readConfs` précédemment codée et fournissez le code des méthodes utiles.

__8)__ On souhaite représenter dans le terminal le plateau dans lequel les véhicules ont été précédemment placés:  

- Quelle sont les bonnes pratiques à respecter ?
- De quels informations supplémentaires aurez vous besoin ?
- Compléter le code de la classe RushHour.

__9)__ Ecrivez le code des méthodes permettant de vérifier :

* qu'une configuration est valide
* qu'une configuration associée au plateau est finale

## Résoudre une configuration initiale

### A mettre en place

Il sera utile de pouvoir :

* obtenir l'ensemble des mouvements possibles à partir d’une configuration donnée.
* vérifier facilement que deux configurations sont identiques (__ce test est à faire très souvent dans le programme de résolution, c’est important qu’il soit efficace__).

### Une recherche de solution particulière

__10)__ On veut trouver la solution comportant le moins de mouvements de véhicules possibles.

__a-__ Par quelle structure de données pourrait-on représenter l'ensemble des configurations possibles à partir de la configuration intiale ?  

__b-__ Quel type de parcours de cette structure de donnée est alors le plus adapté ? En terminale NSI, que conseilleriez vous d'utiliser ?  

__11)__ Donner le principe de cette résolution sous la forme d'un algorithme écrit en pseudo-code  

__12)__ Donner le code d'une classe `Solveur` (et son diagramme UML) permettant de résoudre une configuration décrite par un plateau de jeu (comme précédemment)

*Remarque* : certaines configurations n’ont pas de solutions, votre programme devra pouvoir vous l’indiquer.

# Remise du travail (après le TP MkDocs)

Les documents seront à rendre via le [__gitlab étudiant de l'intranet__](https://gitlab-etu.fil.univ-lille.fr).
Créer un dépôt spécifique `rushour_<votre_nom>`.
Vous Utiliserez  :  `mkdocs`, `mkdocs-kroki`, `mkdocstrings` (voir ci-après)


* Votre site support devra permettre de répondre à toutes les questions posées dans ce TP
* La documentation sera présente dans tous vos scripts et consultable via mkdocsstrings sur votre site support
* Les diagrammes UML écrits en PlantUML seront aussi intégrés
* L'ensemble de vos scripts sera bien évidemment accessible.

__Pensez à ajouter votre enseignant comme développeur de votre dépôt__

## mkdocs-kroki-plugin

Le plugin [`mkdocs-kroki`]https://pypi.org/project/mkdocs-kroki-plugin/ permet d'intégrer les diagrammes PlantUML. 

_yaml à ajouter_

```yaml
- kroki:  # kroki-diagrams
      FileTypes:
          - png
          - svg 
```

Pour insérer du code dans vos fichiers markdown utilisez un bloc de code avec le type `kroki-plantuml`

## plugin mkdocstrings

[`mkdocstrings`](https://mkdocstrings.github.io/) et `mkdocstrings-python` permettent de récupérer les docstrings de vos scripts pour les insérer directement dans vos pages 

_yaml à ajouter_

```yaml
plugins:
- mkdocstrings:
    handlers:
      python:
        import:
        - https://docs.python.org/3/objects.inv
        - https://installer.readthedocs.io/en/stable/objects.inv  # demonstration purpose in the docs
        - https://mkdocstrings.github.io/autorefs/objects.inv
        paths: [src]
        options:
          docstring_options:
            ignore_init_summary: true
          heading_level: 2
          merge_init_into_class: true
          separate_signature: true
          show_root_heading: true
          show_root_full_path: false
          show_signature_annotations: true
          show_symbol_type_heading: true
          show_symbol_type_toc: true
          signature_crossrefs: true
          summary: true

watch:
  - src
```

Vous placerez vos scripts python dans le répertoire `src` qui sera surveillé (pour le live-reloading).
Attention les docstrings doivent être rigoureusement écrites dans un des trois [styles préconisés](https://mkdocstrings.github.io/griffe/docstrings/)

Pour l'utilisation des exemples sont fournis à <https://mkdocstrings.github.io/usage/>
