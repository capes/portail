# + des identificateurs plus lisibles
# + une fonction paramétrée
# + séparation du calcul et de l'affichage : le calcul peut être réutilisé
# + de petites fonctions dont le code est facile à lire et donc à faire évoluer
# + la décomposition du code est plus explicite, les commentaires dans le code deviennent superflus
# + des constantes nommées, donc plus explicites et plus faciles à changer
# + une documentation permet la compréhension du rôle de chaque fonction et son utilisation 
# + des tests qui permettent une validation du code de chaque fonction, et en plus fournissent des exemples d'utilisation 


CODE_a = ord('a')
CODE_z = ord('z')

def est_lettre(car):
    '''Teste si car est une  lettre minuscule

    :param str car: le caractère à tester
    :return: True si car est une lettre minuscule, False sinon
    :rtype: bool
    :CU: car est une chaine de taille 1 

    :Exemples:    
    >>> est_lettre('c')
    True
    >>> est_lettre('C')
    False
    >>> est_lettre('!')
    False
    '''
    return ord(car) >= CODE_a and ord(car) <= CODE_z

def code_lettre(lettre, decalage):
    '''Fournit la decalage-ème lettre de l'alphabet après lettre considérant que
    la lettre 'a' suit la lettre 'z'    

    :param str lettre: la lettre à décaler
    :param int decalage: la valeur du décalage
    :return: la lettre décalée
    :rtype: str
    :CU: lettre est une chaine de taille 1    

    :Exemples:    
    >>> code_lettre('c',1) == 'd'
    True
    >>> code_lettre('c',25) 
    'b'
    '''
    return chr ( CODE_a + (ord(lettre) - CODE_a + decalage) % 26 )

def code_cesar_caractere(car, decalage = 1):
    '''Renvoie la lettre car décalée de decalage caractères, le caractère 'a' suivant le caractère 'z'
    si car ne représente pas une lettre, il est inchangé

    :param str car: le caractère à coder
    :param int decalage:la valeur du décalage
    :return: la version codée de car
    :rtype: str

    :Exemples:            
    >>> code_cesar_caractere('c',1) == 'd'
    True
    >>> code_cesar_caractere('c',25) == 'b'
    True
    >>> code_cesar_caractere('*',25)
    '*'
    '''
    if est_lettre(car):
        return code_lettre(car, decalage)
    else:
        return car

def code_cesar_mot(mot, decalage = 1):
    '''Fournit le mot obtenu en appliquant un code de César de decalage caractères à mot.
    On obtient le code en décalant chaque lettre de decalage caractères dans l'alphabet, 
    les caractères qui ne sont pas des lettres sont inchangés.

    :param str mot: le mot à coder
    :param int decalage:le décalage à appliquer
    :return: le mot codé
    :rtype: str

    :Exemples:    
    >>> code_cesar_mot('czAéb',2) == 'ebAéd'      
    True
    '''
    result = ''
    for lettre in mot :
        result = result + code_cesar_caractere(lettre, decalage)
    return result


LONGUEUR_MOT_MIN = 3
def code_mot(mot, decalage = 1):
    '''Fournit le mot obtenu en appliquant un code de César de decalage caractères
    à mot si sa longueur est au moins de LONGUEUR_MOT_MIN, sinon mot est inchangé

    :param str mot: le mot à coder
    :param int decalage: le décalage à appliquer
    :return: le mot codé s'il a au moins LONGUEUR_MOT_MIN caractères, mot sinon
    :rtype: str

    :Exemples:    
    >>> code_mot('czAéb',2)
    'ebAéd'
    >>> code_mot('ab',2)
    'ab'
    '''
    if len(mot) < LONGUEUR_MOT_MIN:
        return mot
    else:
        return code_cesar_mot(mot, decalage)


def code_phrase(phrase, decalage = 1):
    '''Fournit la phrase obtenue en appliquant un code de César à chacun des mots de la phrase
    
    :param str phrase: la phrase à coder
    :param int decalage: le décalage à appliquer
    :return: la phrase codée
    :rtype: str

    :Exemples:    
    >>> code_phrase('voici un test',2)
    'xqkek un vguv'
    '''
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        liste_code.append(code_mot(mot,decalage))    
    return ' '.join(liste_code)


def affiche_code_phrase(phrase, decalage = 1):
    '''Affiche la phrase et sa version codée par un code de César de decalage caractères

    :param str phrase: la phrase à coder
    :param int decalage: le décalage à appliquer
    :return: None
    :effet de bord: affiche la phrase et sa version codée sur la sortie standard

    :Exemples:    
    >>> affiche_code_phrase('voici un test',2)
    voici un test -> xqkek un vguv
    >>> affiche_code_phrase('voici un test',2)
    voici un test   ->          xqkek un vguv       
    >>> affiche_code_phrase('voici un test',2)
    voici un ...   
    '''
    # test2 pour doctest.NORMALIZE_WHITESPACE
    # test3 pour doctest.ELLIPSIS
    print( phrase + ' -> ' +   code_phrase(phrase, decalage) )
    

import doctest
doctest.testmod()
#doctest.testmod(verbose=True)
#doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)

phrase = "Aujourd'hui, nous parlons de bonnes pratiques."
affiche_code_phrase(phrase, 13)    

