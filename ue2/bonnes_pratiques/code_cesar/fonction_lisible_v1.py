# + des identificateurs plus lisibles
# + une fonction paramétrée
# - difficile à comprendre, donc à corriger ou modifier
# - mélange du calcul et de l'affichage : pas de résultat exploitable
# - pas de documentation
# - pas de test

def affiche_code_phrase(phrase, decalage = 1):
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        if len(mot) < 3:
            code_mot = mot
        else:
            code_mot = ''
            for lettre in mot :
                if lettre.isalpha():
                    code_lettre = chr ( 97 + (ord(lettre) - 97 + decalage) % 26 )
                else:
                    code_lettre = lettre
                code_mot = code_mot + code_lettre
        
        liste_code.append(code_mot)    
    print( phrase + ' -> ' +  ' '.join(liste_code) )
    
phrase = "Aujourd'hui, est le troisieme jour du DIU."
affiche_code_phrase(phrase, 13)    

