Les systèmes de numération
==========================

On compte une multitude de systèmes de numération. On a tenté ici de
classifier et de présenter ici les principaux systèmes utilisés.


<a id="org18ff4de"></a>

# Numération additive

Dans ce type de numération, on additionne les valeurs représentées
par les symboles.


<a id="org5310b2d"></a>

## Premier type

On dispose de symboles pour désigner les puissances de la base $``b``$ :

$` 1\quad b\quad b^2\quad b^3 \quad b^4 \ldots `$

Les symboles sont groupés et ordonnés.


<a id="orga997b80"></a>

### En base 10

1.  Numération Egyptienne

    -   Symboles :
        
        ![img](./figs/egyptian.png)
    
    -   -3000 à -2900
    -   Pas de zéro

2.  Numération Elamite (Iran)

    -   Symboles :
        
        ![img](./figs/elamite.png)
    
    -   -2900
    -   Pas de zéro

3.  Numération Crêtoise

    -   Symboles :
        
        ![img](./figs/cretan.png)
    
    -   -1900 à -1350
    -   Pas de zéro

4.  Numeration Hittite

    -   Symboles :
        
        ![img](./figs/hittite.png)
    
    -   -1400
    -   Pas de zéro


<a id="orgbfb8256"></a>

### En base 20

1.  Numération Aztèque

    -   Symboles :
        
        ![img](./figs/aztec.png)
    
    -   1200
    -   Pas de zéro


<a id="org168fb15"></a>

## Second type

On dispose d'une base $`b`$ et de symboles représentants les nombres

$` 1 \quad k\quad b\quad k\times b\quad b^2\quad k\times b^2\quad b^3\quad k\times b^3\ldots`$

où $`k`$ est un diviseur de la base


<a id="org54ea96e"></a>

### Base 10

1.  Grec acrophonique

    -   $`k=5`$
    -   Symboles :
    
    ![img](./figs/acrophonic_greek.png)
    
    -   -500
    -   Pas de zéro

2.  Romane

    -   $`k=5`$
    -   Symboles :
        
        ![img](./figs/roman.png)
    
    -   -500
    -   Pas de zéro


<a id="org4052c4b"></a>

### Base 60

1.  Sumérienne

    -   $`k=10`$
    -   Symboles :
        
        ![img](./figs/sumerian.png)
    
    -   -3300
    -   Pas de zéro


<a id="org5bf04fa"></a>

## Troisième type

On dispose d'une base $`b`$ et de symboles représentant les nombres :

|                 |                 |                 |            |                     |
|:----------------|-----------------|-----------------|------------|---------------------|
| 1               | 2               | 3               | $`\ldots`$ | $`b-1`$             |
| $`1\times b`$   | $`2\times b`$   | $`3\times b`$   | $`\ldots`$ | $`(b-1)\times b`$   |
| $`1\times b^2`$ | $`2\times b^2`$ | $`3\times b^2`$ | $`\ldots`$ | $`(b-1)\times b^2`$ |
| $`1\times b^3`$ | $`2\times b^3`$ | $`3\times b^3`$ | $`\ldots`$ | $`(b-1)\times b^3`$ |

<a id="org5563009"></a>

### Base 10

1.  Egyptien hieratique

    -   Symboles :
        
        ![img](./figs/hieratic_egyptian.png)
    
    -   -2500
    -   Pas de zéro

2.  Egyptien démotique

    -   Symboles
        
        ![img](./figs/demotic_egyptian.png)
    
    -   -750
    -   Pas de zéro

3.  Alphabétique grec

    -   Symboles
        
        ![img](./figs/alphabetic_greek.png)
    
    -   -400
    -   Pas de zéro

4.  Hébreu alphabétique

    -   Symboles 
        
        ![img](./figs/alphabetic_hebraic.png)
    
    -   -200
    -   Pas de zéro

5.  Alphabétique arménien

    -   Symboles 
        
        ![img](./figs/alphabetic_armenian.png)
    -   +400
    -   Pas de zéro


<a id="org438a561"></a>

# Numération hybride


<a id="orgba10c75"></a>

## Premier type

Dans ce type de numération, la valeur est déterminée par le nombre
d'unités placées à gauche des symboles représentant les
puissances de la base.


<a id="org49e2ae4"></a>

### Base 10

1.  Assyro-Babilonien

    -   symboles :
        
        ![img](./figs/assyro_babylonian.png)
    
    -   -2350

2.  Araméen

    -   symboles :
        
        ![img](./figs/arameen.png)
    
    -   -750
    -   de droite à gauche.


<a id="org12e146c"></a>

## Deuxième type

Dans ce type de numération, on dispose de 9 symboles pour désigner
les unités, que l'on place à gauche des puissances de la base


<a id="org165aa2a"></a>

### Base 10

1.  Chinois commun

    -   symboles :
        
        ![img](./figs/common_chinese.png)
    
    -   -1400
    -   pas de zéro


<a id="org8b76f1d"></a>

# Numération positionnelle

Dans ce type de numération, c'est la position des symboles qui
détermine la puissance de la base par laquelle il faut multiplier la
valeur du symbole.


<a id="org768989d"></a>

## Premier type

Dans ce type, on cumule encore les symboles des unités pour
désigner les petites valeurs


<a id="org618082c"></a>

### Base 10

1.  Chinois érudit

    -   symboles :
        
        ![img](./figs/learned_chinese.png)
    
    -   -200
    -   zéro : oui, tardif


<a id="orgd3f131c"></a>

### Base 20

1.  Maya érudit

    -   symboles : 
        
        ![img](./figs/learned_maya.png)
    
    -   400-900
    -   Existence du zéro
    -   De haut en bas.


<a id="orgbd707de"></a>

## Second type

Un symbole pour chaque valeur entre 1 et $`b-1`$.


<a id="org8d7cd41"></a>

### Base 10

-   symboles :
    
    ![img](./figs/modern.png)

-   400
-   Existence du zéro


<a id="orge8e15de"></a>

# Travail à faire

Réaliser un programme qui :

-   prend sur l'entrée standard un nombre et un système de numération
    (parmi ceux proposés)
-   Crée une image représentant le nombre.
-   Affiche cette image.

On sera amené à utiliser la bibliothèque PIL, dont voici une démonstration :


<a id="org00e8f7a"></a>

## Utilisation de la bibliothèque PIL

PIL est une bibliothèque permettant a manipulation des images sous
python. Dans l'exemple ci-dessous, on charge un fichier
représentant les symboles de la numération chinoise, puis on crée
des images correspondant à chaque caractère.
    
```python
from PIL import Image

EXAMPLE = 'common-chinese'
PATH = "./figs/"

img = Image.open(PATH +  NUMBERS[EXAMPLE]['file'] )
symbols = []
for (x1, y1, x2, y2) in NUMBERS[EXAMPLE]['symbols']:
    symbols.append( img.crop( (x1, y1, x2, y2) ) )
    symbols[-1].show()
```
    
La fonction suivante pourra être utile : elle assemble (en ligne,
de gauche à droite) les images contenues dans une liste pour n'en
faire qu'une seule.

```python
def merge_images(l):
    """
    :param l: (list) list of PIL Image
    :return: (Image) merged image of images contained in l
    """
    total_width, total_height = 0, 0
    for im in l:
        w, h = im.size
        total_width += w
        total_height = max(total_height, h)
    # create new Image
    res = Image.new("RGB", (total_width,total_height))
    w = 0
    for im in l:
        res.paste( im, (w, 0) )
        w += im.size[0]
    return res
```
    
Voici une démonstration de ce que l'on souhaite réaliser :
    
```python
egyptian = NumerationA1(NUMBERS['egyptian'], './figs/')
img = egyptian.number_image(7659)
img.save('./figs/example.png')
```
        
    et le résultat :
    
[![img](./figs/example.png)](figs/example.png)


<a id="org818dd7c"></a>

## Programmation objet en python

Pour avoir un code plus simple et plus extensible (OCP), je vous
suggère l'utilisation de la programmation orientée objet.

Python implémente quelques principes de la poo.

Le document suivant décrit comment sont implantés ces principes en python :

[POO en python](poo_python.pdf)


<a id="org8be8a86"></a>

# Matériel fourni

Une [archive zip](./numeration.zip) contenant :
-   le dictionnaire des numérations contenant les coordonnées des
    symboles dans les images ;
-   Les fichiers contenant les glyphes 

