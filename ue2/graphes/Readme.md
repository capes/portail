Graphes - Travaux pratiques
===========================

Pour un travail en local :

* notebook jupyter [graphe.ipynb](graphe.ipynb) et les figures associées [./fig/*](./fig/)

* installer préalablement la biliothéque [networkx](https://networkx.org/)

<!--
En ligne via le serveur `jupyter.fil.univ-lille.fr` (utiliser le VPN depuis l'extérieur) :

* accessible viac	[frama.link/diu-ipynb-graphe](https://frama.link/diu-ipynb-graphe) 
-->