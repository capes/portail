# Circuits logiques

**Objectifs**
  - voir (ou revoir) les fonctions booléennes et les portes logiques
  - réaliser un circuit correspondant à une fonction booléenne.
  - réaliser une opération élémentaire (addition)
  - utiliser un logiciel de simulation de circuits logiques

## Fonctions booléennnes, portes logiques
Cette section résume les prérequis nécessaires.

#### Portes logiques
  - elles réalisent les implémentations matérielles des opérations booléennes
  - on utilise une représentation schématique indiquant ces opérations:

<div >
<table>
<thead><tr><th>Opération</th><th>Symbole</th><th>Table de vérité</th></tr></thead>
<tr>
<td>NON<br/>(NOT)</td>
<td style="background-color: white;"><img src="figs/NOT_ANSI_Labelled.svg"/></td><td>
<table style="margin:0px;"><thead><tr><th>A</th><th>Q</th></tr></thead>
<tbody>
<tr><td> 0 </td><td> 1 </td></tr>
<tr><td> 1 </td><td> 0 </td></tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>ET<br/>(AND)</td>
<td style="background-color: white;"><img src="figs/AND_ANSI_Labelled.svg"/></td><td>
<table style="margin:0px;"><thead><tr><th>A</th><th>B</th><th>Q</th></tr></thead>
<tbody>
<tr><td> 0 </td><td> 0 </td><td> 0 </td></tr>
<tr><td> 0 </td><td> 1 </td><td> 0 </td></tr>
<tr><td> 1 </td><td> 0 </td><td> 0 </td></tr>
<tr><td> 1 </td><td> 1 </td><td> 1 </td></tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>OU<br/>(OR)</td>
<td style="background-color: white;"><img src="figs/OR_ANSI_Labelled.svg"/></td><td>
<table style="margin:0px;"><thead><tr><th>A</th><th>B</th><th>Q</th></tr></thead>
<tbody>
<tr><td> 0 </td><td> 0 </td><td> 0 </td></tr>
<tr><td> 0 </td><td> 1 </td><td> 1 </td></tr>
<tr><td> 1 </td><td> 0 </td><td> 1 </td></tr>
<tr><td> 1 </td><td> 1 </td><td> 1 </td></tr>
</tbody>
</table>
</td>
</tr>
</table>
</div>

#### Réalisation physique: Transistors
  - la réalisation physique des portes logiques repose actuellement
    sur l'utilisation de
    [transistor](https://www.youtube.com/watch?v=7ukDKVHnac4) transistors
    en mode commutation (passant/bloquant).
  - il existe d'autres dispositifs plus manuels pour réaliser des portes logiques
    - des [poulies](https://www.alexgorischek.com/project/pulley-logic-gates)
    - des [poulies en impression 3D](https://www.thingiverse.com/thing:1720219)
    - des [dominos](https://www.youtube.com/watch?v=lNuPy-r1GuQ)
    - des [billes sur un plateau basculant](https://youtu.be/H6o9DTIfkn0)


### Une fonction, plusieurs circuit.

À chaque expression booléenne de la grammaire présentée en UE1
correspond un circuit ( à l'emplacement près des portes ) utilisant
uniquement les portes ET, OU et NON.

**Exercice** Réaliser le circuit correspondant à l'expression
$`(\overline{a}\land b)\lor (\overline{b}\land\overline{c})`$.

**Exercice** 
- Choisir un des 7 segments d'un afficheur 7 segment ;
- Déterminer une table de Karnaugh pour la fonction booléenne associée
  à ce segment ;
- Déterminer une expression de la fonction choisie ;
- Réaliser le circuit correspond à votre fonction.
    
    
## Réaliser une addition binaire avec des circuits logiques

Nous supposons simplement connues les portes ET, OU et NON.

#### Additionner deux nombres binaires de 1 bit

- Soient 2 nombres $`a`$ et $`b`$ de l'ensemble $`\lbrace
  0;1\rbrace`$. Ces nombres sont représentables sur 1 bit.
  
  Soit $`s=a+b`$
  - si $`a`$ et $`b`$ valent 0, alors $`s`$ vaut 0,
    - si $`a`$ vaut 1 ou bien $`b`$ vaut 1 (mais pas les deux),alors
      $`s`$ vaut 1,
    - et si $`a`$ et $`b`$ valent 1, alors $`s`$ vaut 2, et 1 bit ne
      suffit plus à représenter la somme.
  - Nous avons donc à calculer 2 fonctions logiques :
    - une pour le bit de poids faible de $`s`$. On l'appelle $`u`$;
    - une pour le bit de poids fort de $`s`$. On l'appelle $`r`$.
  - Tables de vérité de $`u`$ et de $`r`$:

| a | b | u | r |
|---|---|---|---|
| 0 | 0 | 0 | 0 |
| 0 | 1 | 1 | 0 |
| 1 | 0 | 1 | 0 |
| 1 | 1 | 0 | 1 |

**Exercice** 
  - Déterminer une expression de $`u`$ et de $`r`$.
  - Réaliser le schéma avec des portes logiques
  - Ce circuit se nomme un *demi-additionneur* (*half-adder* en anglais)

#### Additionner deux nombres binaires de 1 bit avec retenue entrante
  - Vous allez réaliser maintenant un additionneur sur 1 bit, avec une
    retenue entrante
  - Entrées: $`a`$, $`b`$, et $`c`$ la retenue entrante
  - Sorties: $`u`$ le chiffre des unités de la somme, et $`r`$ la
    retenue sortante
  - Comme précédemment, construisez les tables de vérités pour les 2
    fonctions u et r. Remarquez que lorsque la retenue entrante c vaut
    0, la table de vérité est identique à celle ci-dessus. Je vous
    propose donc de la construire en utilisant l'ordre c, a, b, la
    moitié du travail est ainsi déjà faite.

| c | a | b | u | r |
|---|---|---|---|---|
| 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 1 | 0 |
| 0 | 1 | 0 | 1 | 0 |
| 0 | 1 | 1 | 0 | 1 |
| 1 | 0 | 0 |   |   |
| 1 | 0 | 1 |   |   |
| 1 | 1 | 0 |   |   |
| 1 | 1 | 1 |   |   |

  - construisez les circuits calculant respectivement u et r à l'aide
    de portes logiques

  - ce circuit se nomme un *additionneur complet* (*full-adder* en
    anglais)

#### Additionner deux nombres binaires de 4 bits
  - il est aisé de réaliser l'addition de 2 nombres A et B de 2 bits
    si l'on dispose de 2 additionneurs 1 bit complets: on utilise un
    additionneur pour additionner chaque bit des opérandes A et B, et
    on fait se propager la retenue en cascadant la retenue sortante
    d'un additionneur avec la retenue entrante de l'additionneur
    suivant.
  - Le schéma suivant illustre ce principe.
  
    ![](figs/adder2bits.png)
  - Construisez un additionneur de 2 nombres sur 4 bits, soit à l'aide
    de 4 additionneurs complets de 1 bit, soit à l'aide de deux
    additionneurs 2 bits.

#### Un mot concernant l'Unité Arithmétique et Logique
  - l'UAL dans un processeur désigne l'ensemble des circuits
    effectuant les opérations élémentaires définies par le langage
    d'assemblage
  - Les opérations logiques, et les décalages se réalisent directement
    avec des portes logiques ou de simples connexions
  - les opérations arithmétiques utilisent au plus un additionneur
    binaire:
    - soustraction: opérande passée en complément à 2 (complément à 1
    (NOT) + 1 (en retenu entrante))
    - multiplication/division: "séquence"
      d'additions/soustractions/décalages, ou réduction additive de
      produits binaires (AND) partiels. Certains microcontrôleurs ne
      disposent pas de ces opérations car elles sont complexes.
  - toutes les opérations élémentaires sont calculées en permanence et
    en parallèle
  - un seul résultat est conservé en fonction de l'instruction
    exécutée
  - la sélection du résultat est réalisée avec un multiplexeur

#### Le demi-additionneur
  ![](figs/bitadder.png)
  - Noter que A et B sont des pins en entrée, S et R des pins en
    sortie: nous pourrons réutiliser ce circuit dans un autre circuit,
    ces pins étant alors accessibles dans le niveau supérieur
  - Nous avons ajouté une horloge `sysclk` qui n'est pas connectée:
    elle est seulement nécessaire pour réaliser des chronogrammes de
    circuit (accessible dans le menu Simulation)

    ![](figs/chrono_bitadder.png)
  - Vous pouvez ainsi vérifier sur ce chronogramme que l'additionneur
    1 bit est correct

#### Manipulation
  - réalisez le schéma de l'additionneur complet avec Logisim et
    vérifiez sa correction à l'aide d'un chronogramme
  - réalisez l'additionneur 4 bits à l'aide de cet additionneur
    complet.
