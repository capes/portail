## Logisim

[Logisim](https://github.com/reds-heig/logisim-evolution) est un outil
éducatif de conception et de simulation de circuits logiques
numériques. Il est écrit en Java et est donc multiplateforme
(Linux/Mac/Windows). Il ne nécessite que l'installation préalable de
Java (version [Oracle](https://www.java.com/fr/download/) propriétaire
ou [OpenJDK](https://jdk.java.net/) GPL).

#### Installation
  - Télécharger l'archive de l'application
    [`logisim-evolution.jar`](http://reds-data.heig-vd.ch/logisim-evolution/logisim-evolution.jar)
  - Exécutez l'application directement ou depuis un terminal de commandes
  ```
  java -jar logisim-evolution.jar
  ```

#### Prise en main
![](figs/logisim.png)
  - Logisim présente, en plus des menus et boutons d'outils, deux
    zones principales:
    - à droite la zone de dessin de schéma avec une grille de
      placement des objets
    - à gauche un liste hiérarchique (arbre) des objets que l'on peut
      placer dans la grille
    - sous cette liste, une zone pour éditer les propriétés de l'objet
      sélectionné
  - sous le menu, à gauche, les 2 boutons importants sont la flèche
    pour sélectionner un objet dans le schéma ou relier les objets par
    des fils, et la main pour modifier les valeurs/actionner les
    boutons...
  - la simulation du circuit est par défaut active en permanence (voir
    menu Simulation)
  - Reproduisez le schéma donné en exemple en modifiant les propriétés
    des objets indiqués dans la figure.
  - On utilisera dans la suite préférentiellement des pins pour les
  entrées et les sorties des circuits car ils permettent de réaliser
  des circuits hiérarchiques.  Seul le schéma principal comportera
  tous les dispositifs d'entrées/sorties nécessaires.
