UE2 - Fonctions booléennes et circuits
======================================

Travail autour des fonctions booléennes et de la représentation des
nombres.

- [prise en main de logisim](./logisim.md)
- [réalisation de circuits booléens](./circ-logique.md) 

