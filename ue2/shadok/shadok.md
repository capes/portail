```
title : Numération Shadok
```

![img](./shadok_base4.png)

**Objectifs**

-   effectuer des changements de base
-   utiliser des chaînes de caractères et des listes
-   utiliser des caractères Unicode
-   utiliser les fonctions `chr` et `ord` de Python.

**Attention**

**Toute fonction doit être documentée, en précisant ses contraintes d'utilisation.** **Toute fonction doit être testée**.


# La numération shadok

1.  Étudiez cette vidéo.
    
    <div class="HTML" id="org612ce59">
    <p>
    &lt;iframe width="420" height="315" 
      src="<a href="http://www.youtube.com/embed/nm0cw6b1PMA">http://www.youtube.com/embed/nm0cw6b1PMA</a>" 
      frameborder="0" allowfullscreen&gt;
    &lt;/iframe&gt;
    </p>
    
    </div>

2.  Les shadoks n'ont que quatre symboles O (GA), − (BU), ⨼ (ZO) et ◿ (MEU).
    
    Le professeur Shadoko a donc décidé que les nombres seraient écrits en utilisant ces quatre symboles, chacun d'eux correspondant à un chiffre selon le tableau
    
    | Chiffre | Valeur | Unicode |
    |---------|--------|---------|
    | O (GA)  | 0      | 0x004F  |
    | − (BU)  | 1      | 0x2212  |
    | ⨼ (ZO)  | 2      | 0x2A3C  |
    | ◿ (MEU) | 3      | 0x25FF  |
    
    À l'aide de la fonction `chr`, définissez quatre constantes `GA`, `BU`, `ZO` et `MEU` ayant pour valeur les quatre chiffres shadoks.

3.  Définissez une variable `ALPHABET_SHADOK` dont la valeur est la liste des quatre chiffres shadoks dans l'ordre O, −, ⨼ et ◿.

4.  Réalisez une fonction `entier_en_shadok` qui renvoie l'écriture shadok de l'entier passé en paramètre. Contentez-vous d'une fonction qui convient pour les entiers positifs ou nuls.
    
    ```
    >>> entier_en_shadok (0)
    'O'
    >>> entier_en_shadok (1)
    '−'
    >>> entier_en_shadok (2)
    '⨼'
    >>> entier_en_shadok (3)
    '◿'
    >>> entier_en_shadok (13)
    '◿−'
    ```

5.  Quelle est l'écriture shadok de `2023` ?

6.  Réalisez la fonction nommée `shadok_en_entier` réciproque de la précédente qui renvoie le nombre correspondant à une écriture shadok.
    
    ```
    >>> shadok_en_entier (GA)
    0
    >>> shadok_en_entier (BU)
    1
    >>> shadok_en_entier (ZO)
    2
    >>> shadok_en_entier (MEU)
    3
    >>> shadok_en_entier (MEU+BU)
    13  
    ```

7.  Répondez à la question posée à la fin de la vidéo : quel est l'entier correspondant au nombre écrit − ⨼ O ◿ ?


# Messages Shadok

Les chiffres shadok peuvent aussi servir à écrire des messages.

1.  Expliquez pourquoi tout nombre entier représentable sur un octet peut être écrit en shadok avec quatre chiffres shadoks.

2.  Réalisez une fonction nommée `octet_en_shadok` qui renvoie l'écriture shadok avec exactement quatre chiffres d'un entier représentable sur un octet.
    
    ```
    >>> octet_en_shadok (0)
    'OOOO'
    >>> octet_en_shadok (255)
    '◿◿◿◿'
    >>> octet_en_shadok (65)
    '−OO−'
    ```

3.  Tout caractère ASCII peut être codé en shadok par une chaînes de caractères contenant quatre chiffres shadoks. Par exemple, le caractère `A` dont le code ASCII est 65 (0x41) est codé par la chaîne '− O O −'.
    
    Quel est la chaîne de caractères shadoks qui code le caractère `e` ?

4.  Réalisez une fonction `code_car_en_shadok` qui code un caractère ASCII en shadok.
    
    ```
    >>> code_car_en_shadok ('A')
    '−OO−'
    >>> code_car_en_shadok ('e')
    '−⨼−−'
    ```

5.  Réalisez une fonction `code_en_shadok` qui code en shadok la chaîne de caractères ASCII passée en paramètre.
    
    ```
    >>> code_en_shadok ('Timoleon')
    '−−−O−⨼⨼−−⨼◿−−⨼◿◿−⨼◿O−⨼−−−⨼◿◿−⨼◿⨼'
    ```

6.  Quel est le caractère ASCII codé par la chaîne 'O⨼⨼⨼' ?
    
    Est-il possible que la chaîne '⨼◿◿−' corresponde à un caractère ASCII ?

7.  Réalisez une fonction `decode_car_du_shadok` qui décode une chaîne de quatre caractères shadoks en un caractère ASCII. (Réfléchissez bien sur les contraintes d'utilisation)
    
    ```
    >>> decode_car_du_shadok ('−OO−')
    'A'
    >>> decode_car_du_shadok ('O⨼⨼⨼')
    '*'
    ```

8.  Réalisez une fonction `decode_du_shadok` qui décode une chaîne shadok.
    
    ```
    >>> decode_du_shadok ('−−−O−⨼⨼−−⨼◿−−⨼◿◿−⨼◿O−⨼−−−⨼◿◿−⨼◿⨼')
    'Timoleon'
    ```

9.  Décodez le message '−O−◿−OO−−OO⨼−−−−−−⨼⨼−O◿◿−O◿−−O−−−−−−'.
