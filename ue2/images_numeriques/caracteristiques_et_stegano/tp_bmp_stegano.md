---
title: TP caractéristiques images et steganographie
---

# Dans les programmes de SNT

Thème : La photographie numérique

Contenus :

![BO SNT](./fig/bo_snt.jpg)

# Caractéristiques des images

Une image numérique est un ensemble discret de points appelés __PIXELS__ (contraction de PICTure ELements). Elle a pour vocation d’être affichée sur un écran. 

Pour réaliser un affichage numérique on utilise habituellement la __synthèse additive__ des couleurs à l'aide de trois couleurs primaires : le rouge (R), le vert (V) et le bleue(B).

![BO SNT](./fig/principes_rvb.jpg)

Les logiciels de retouche photo utilisent aussi un autre système TSL (_Teinte saturation Luminosité_) qui se rapproche plus de notre vision réelle


* La __définition__ d’une image correspond au nombre de pixels qui la constituent

* La  __taille__ d’une image se calcule ainsi :   
$Taille = nombre\ d’octets\ par\ pixel \times définition$

* La __résolution__ d'une image correspond au nombre de pixels par unité de longueur  
Pour l'impression, la résolution d'une image s'exprime en _ppp_ (_points_ par pouce) ou _dpi_ (dots per inch).  
Pour l'affichage sur un écran, la résolution s'exprime en _ppp_ (_pixels_ par pouce) ou _ppi_ (pixels per inch).

# Activité SNT

_Extrait de Sciences Numérique et Technologie (Hachette)_

![Le code derrière l'image](./fig/image_bmp.jpg)

On cherche à trouver l'image cachée derrière le fichier précédent.

__1-__ De combien de couleurs l'image à décourir se compose-t-elle? Lesquelles?  
__2-__ Quelle est la définition de cette image? Sa taille?  
__3-__ Retrouvez la taille du fichier et expliquez la différence avec la valeur de la question précédente  
__4-__  Il existe plusieurs types de fichiers  bitmap, en utilisant les deux ressources suivantes, retrouvez le type du fichier précédent :  
https://www.commentcamarche.net/contents/1200-bmp-format-bmp  
https://en.wikipedia.org/wiki/BMP_file_format#File_structure  
 
__5-__  Représenter l'image sur une feuille quadrillée  
__6-__ L'information dans ce fichier n'est pas compressée. En raison du très grand nombre de répétition de couleurs identiques succesives il est intéressant d'utiliser un algorithme de compression comme __RLE__ (_run-length encoding_)
L’idée est la suivante : dans une image en noir et blanc, si chaque pixel est représenté
par un 1 (noir) ou un 0 (blanc) : pour comprimer une image avec 8 × 8 = 64 pixels, on transforme tout d’abord celle-ci un une séquence de 64 bits, en « lisant » l’image ligne par ligne. 

On procéde comme suit : 
* on divise la séquence en paquets de 4 bits de longueur ; 
* dans chaque paquet, le premier bit symbolise la couleur (0 ou 1) de la suite de pixels et les 3 bits suivants indiquent en binaire le nombre de pixels consécutifs de cette couleur, moins 1. 

`0010` signifie « 3 pixels consécutifs de couleur 0 »
`1101` signifie « 6 pixels consécutifs de couleur 1 »

En utilisant des 0 et des 1 : donnez le codage de l'image (uniquement son contenu) ligne par ligne en utilisant cette compression.



# Images et steganographie

* Présentation

https://www.fil.univ-lille1.fr/~wegrzyno/Stegano/stegano.html#/

* Application

https://www.fil.univ-lille1.fr/~routier/isn/activites/activite4/index.html

