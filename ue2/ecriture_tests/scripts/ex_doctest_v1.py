from math import sqrt

# avec Python > 3.10 on pourra écrire
# def plus_grande_racine (a : int | float, b : int | float, c : int | float) -> float

def plus_grande_racine (a : float, b : float, c : float) -> float:
    """Renvoie la plus grande racine du polynôme de second degré ax^2 + bx + c

    :param a: coefficient a - associé à x^2
    :param b: coefficient b - associé à x
    :param c: coefficient c - constant
    :return: la plus grande racine du polynôme
    :CU: a doit être non nul et (b**2 - 4*a*c) doit être positif ou nul
    
    :Exemples:

    >>> plus_grande_racine(5, 0, -5)
    1.0  
    >>> plus_grande_racine(2, 2, 0)
    0.0
    >>> plus_grande_racine(1, 1, -6) == 2
    True
    >>> plus_grande_racine(1, 1, 2)
    Traceback (most recent call last):
    ...
    ValueError: math domain error
    """
    delta = b**2 - 4 * a * c
    r1 = (-b +sqrt(delta))/ (2 *a )
    r2 = (-b -sqrt(delta))/ (2 * a)
    return max(r1, r2)


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
    