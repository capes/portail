class Cell:
    """classe représentant une cellule du labyrinthe
    """

    def __init__(self):
        """initialise une case du jeu qui a les caractéristiques suivantes :
            * n'est pas la case d'arrivée
            
        :Exemples:
        
        >>> cel = Cell()
        >>> cel.is_final()
        False
        """
        self.__final = False
         
        
    def set_final(self) :
        """La case devient la case d'arrivée

        :Exemples:
        
        >>> cel = Cell()
        >>> cel.set_final()
        >>> cel.is_final() 
        True
        """
        self.__final = True
        
    
    def is_final(self):
        """Détermine si la case est la sortie

        :return: `True` si la case est la sortie du labyrinthe, `False` sinon
        :rtype: bool
        
        :Exemples:
        
        >>> cel = Cell()
        >>> cel.set_final()
        >>> cel.is_final()
        True
        """
        return self.__final

if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)