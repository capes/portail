from robot_box import *

def test_sans_boite_à_la_creation():
    # Arrange
    robot = Robot(23)
    
    # Act
    # ici rien à effectuer
    
    # Assert
    assert not robot.is_carrying_a_box()
    
    
def test_robot_peut_prendre_boite_legere():
    # Arrange
    robot = Robot(23)
    box = Box(12)
    box_2 = Box(8)
    
    # Act
    robot.take_box(box)
    
    # Assert
    assert robot.is_carrying_a_box()
    assert box == robot.get_box()
