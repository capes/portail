class Robot:
    """ Un robot peut porter une caisse d'un poids maximal défini à la construction du robot
    . Initialement un robot ne porte pas de caisse.
    S'il porte déjà une caisse il ne peut en prendre une autre"""
    
    def __init__(self, max_weight):
        self.max_weight = max_weight
        self.carried_box = None
    
    def take_box(self, box):
        if not self.is_carrying_a_box() and box.get_weight() <= self.max_weight :
            self.carried_box = box
    
    def is_carrying_a_box(self):
        return self.carried_box != None
    
    
    def get_box(self):
        return self.carried_box
    
class Box:
    """ Une boite définie par son poids"""
    def __init__(self, weight):
        self.weight = weight
        
    def get_weight(self):
        return self.weight
    