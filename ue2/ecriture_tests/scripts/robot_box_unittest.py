import unittest
from robot_box import *

class TestRobot(unittest.TestCase):

    def test_sans_boite_a_la_creation(self):
        # Arrange
        robot = Robot(23)
        
        # Act
        # ici rien à effectuer
        
        # Assert
        self.assertFalse(robot.is_carrying_a_box())
   
    
    def test_robot_peut_prendre_boite_legere(self):
        # Arrange
        robot = Robot(23)
        box = Box(12)
        box_2 = Box(8)
    
        # Act
        robot.take_box(box)
    
        # Assert
        self.assertTrue(robot.is_carrying_a_box())
        self.assertEqual(box, robot.get_box())

if __name__ == '__main__':
    unittest.main()