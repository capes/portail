from saison import *

def test_printemps():
    attendu = "printemps"
    effectif = saison(3, 22)
    assert attendu == effectif


def test_ete():
    attendu = "été"
    effectif = saison(8, 21)
    assert attendu == effectif
    
    
def test_automne():
    attendu = "automne"
    effectif = saison(12, 6)
    assert attendu == effectif
    
    
def test_hiver():
    attendu = "hiver"
    effectif = saison(12, 24)
    assert attendu == effectif