nb_jours = [31,29,31,30,31,30,31,31,30,31,30,31]

def saison(mois, jour):
    assert isinstance(mois, int) and not isinstance(mois, bool)
    assert isinstance(jour, int) and not isinstance(jour, bool)
    assert mois >= 1 and mois <= 12
    assert jour>= 1 and jour <= nb_jours[mois - 1]
    if (mois == 12 and jour >= 21) or mois == 1 or mois == 2 or (mois == 3 and jour < 21):
        return "hiver"
    if (mois == 3 and jour >= 21) or mois == 4 or mois == 5 or (mois == 6 and jour < 21):
        return "printemps"
    if (mois == 6 and jour >= 21) or mois == 7 or mois == 8 or (mois == 9 and jour < 21):
        return "été"
    if (mois == 9 and jour >= 21) or mois == 10 or mois == 11 or (mois ==12 and jour < 21):
        return "automne"