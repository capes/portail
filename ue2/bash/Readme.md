---
title: TP initiation langage bash
---

# Pour commencer

_source : https://www.inf.telecom-sudparis.eu/_

## Syntaxe

- [Diaporama](http://www-inf.telecom-sudparis.eu/cours/CSC3102/Supports/ci1-bash/ci-bash.pptx.pdf)
- [TP](http://www-inf.telecom-sudparis.eu/cours/CSC3102/Supports/?page=ci1-bash/tp-bash&wrap=true)

## Flux

- [Diaporama](http://www-inf.telecom-sudparis.eu/cours/CSC3102/Supports/ci3-flux/ci-flux.pptx.pdf)
- [TP](http://www-inf.telecom-sudparis.eu/cours/CSC3102/Supports/ci3-flux/ci-flux.pptx.pdf)

# Quelques idées de scripts

## Environnement Python

Ecrire un script qui à partir d'un fichier `requirements.txt` contenant les noms de plusieurs bibliothéques python, les installe toutes.

## Alarme

Ecrire un script qui déclenche une alarme à une heure choisie, ce script est lançé en arrière-plan.
Lors du déclenchement de l'alarme, il efface l'écran et affiche "Il est l'heure!" puis stoppe le script

## Analyse code python

Afficher tous les noms de fonctions utilisées dans un script Python passé en paramètre avec leur annotation de type si elles existent.

## Poubelle

_idée provenant de www.lri.fr/~mandel_

Ecrire un script `jeter.sh` qui permet de manipuler une poubelle à fichier (un répertoire nommé poubelle situé à votre racine).  
La commande accepte trois options :  
– `jeter -l` pour lister le contenu de la poubelle ;  
– `jeter -s`  fichier chemin pour sortir le fichier de la poubelle et le placer à l’emplacement chemin ;  
– ` jeter -v`  pour vider la poubelle ;  
– ` jeter fichier1 fichier2 ...`  pour déplacer les fichiers considérés vers la poubelle ;
Si la poubelle n’existe pas, elle est créée à l’appel de la commande.