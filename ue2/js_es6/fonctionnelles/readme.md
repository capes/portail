---
title : Javascript ES6
---


__Objectifs__ : découvrir le langage et ses évolutions "récentes"


Javascript est devenu le langage de programmation le plus utilisé. Javascript a beaucoup évolué depuis quelques années. En particulier avec la version appelée EcmaScript 6 (ES6). Le langage continue d'évoluer régulièrement et d'intégrer de nouvelles fonctionnalités.


# Elements de syntaxe

__[Etudier le cours suivant](https://www.fil.univ-lille.fr/~routier/enseignement/licence/js-s4/html/javascript-1.html)__


_Notes d'utilisation du cours :_

> _Il faut cliquer sur la petite flèche en haut à gauche de chaque bloc_. Le résultat de l'évaluation de la dernière expression est fournie. Il est le plus souvent nécessaire de réaliser les évaluations dans l'ordre du document car des blocs de code placés plus bas peuvent réutiliser des définitions faites plus haut.

> La console sur la droite reçoit les sorties produites par la commande `log ` utilisée dans ces blocs de code. Elle permet également de réaliser des évaluations en saisissant une expression dans la zone dédiée située en bas. La commande `$raz` vide son contenu et les flèches haut et bas permettent de parcourir son historique.

# TP : méthodes fonctionnelles 

* Le sujet de [TP](https://www.fil.univ-lille.fr/~routier/enseignement/licence/js-s4/tdtp/index.html)
* L'[archive à utiliser avec le TP](./fichiers-exercices-1.zip)
