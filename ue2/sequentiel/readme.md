Circuits séquentiels
====================

- Les circuits logiques que nous avons vus jusque maintenant
  permettent d'implémenter les fonctions booléennes: ils se nomment
  circuits *combinatoires*.
- Par construction, leurs sorties changent d'état dès que la valeur
  d'une de leurs entrées change d'état: plus exactement après que les
  transistors sur le chemin liant l'entrée aux sorties aient tous
  commutés
- Par construction également, ils ne possèdent pas de boucle de
  rétroaction
- Les circuits *séquentiels* introduisent la rétroaction dans les
  circuits logiques:
  - la rétroaction s'opère en reliant un sous ensemble des sorties
    d'un circuit combinatoire à un sous ensemble de ses entrées
  - cette rétroaction introduit une notion d'état:
    - l'état correspond à la valeur des sorties en rétroaction
    - l'état de sortie est influencé par l'état antérieur (avant/après
      traversée du circuit combinatoire)

Bascules et bistables
---------------------

Le comportement d'un circuit séquentiel étant difficile à prévoir, on
se contente de manipuler des *briques de bases* : les bascules et les
bistables.

Les bascules sont construisent avec des portes logiques, les bistables
se construisent avec les bascules.

### Bascules

Il y a plusieurs façons de réaliser une bascule. En voici une réalisée
sur logisim : 

![bascule SR](./figs/rs.png)

**à faire**

Réaliser ce circuit avec logisim, puis analyser l'état de la sortie en
fonction des entrées et de la sortie précédente.

Cette bascule est appelée bascule SR (pour set reset)

Les bascules sont *asynchrones*, c'est-à-dire que leur sortie change
rapidement après un changement des entrées. Afin d'intégrer ces
composants dans une architecture synchrones, il convient de
synchroniser le changement sur le rythme d'un signal périodique appelé
*horloge*.

### Bistable

Un bistable est une bascule qui change d'état lorsque le signal
d'horloge passe d'un front haut à un front bas.

Il y a plusieurs façons de réaliser le bistable. On considère ici le
bitable /D flip-flop/ :

![flip-flop D](./figs/flipflopD.png)

**à faire**

Réaliser ce circuit avec logisim, puis réaliser un chronograme. 

La bascule de gauche est le maître et celle de droite l'esclave.
Regardons d'abord ce qui se passe lorsque le signal d'horloge vaut 1.
Dans ce cas, les deux portes-et devant les entrées du maître sont
ouvertes, à savoir qu'elles permettent à la valeur de l'entrée de
passer directement sur l'entrée s de la bascule de gauche et de son
inverse de passer sur l'entrée r . Par conséquent, la valeur de l'entrée
va passer directement sur la sortie q du maître.

Par contre, les deux portes-et qui se trouvent devant l'esclave sont
fermées, à savoir que leur sortie vaut 0. Par conséquent, l'esclave
maintient son ancienne valeur. 

Au contraire, lorsque l'horloge vaut 0, c'est la situation inverse, à
savoir que les portes-et du maître sont fermées et celles de l'esclave
sont ouvertes. Dans ce cas, le bistable est complètement insensible
aux variations.

Maintenant, considérons ce qui se passe lorsque l'horloge passe de 1
à 0. Pour que cela fonctionne, il va falloir supposer que l'entrée
reste stable brièvement pendant ce passage. 

La première chose qui se produit est la fermeture des portes-et du
maître, à savoir qu'elles deviennent insensibles aux changements
futurs du signal. La valeur de la sortie du maître sera donc
égale à la valeur du signal d'entrée juste avant que l'horloge passe de 1
à 0. 

Un moment bref plus tard, le signal de l'horloge aura traversé
l'inverseur et arrivera aux portes-et de l'esclave. Ces portes
s'ouvrent et laissent passer la valeur q du maître sur la sortie q de
l'esclave. La valeur de la sortie q de l'esclave (et donc celle du
bistable entier) est alors celle du signal d'entrée juste avant le
passage de l'horloge de 1 à 0.

C'est donc le passage d'un état haut à un état bas qui provoque la
copie de l'entrée vers la sortie du bistable. Par contre, il n'y a
jamais de passage direct de l'entrée vers la sortie du bistable. La
sortie change de manière synchrone avec le passage de l'horloge de 1 à
0 (le front descendant d'horloge).

Finalement, regardons ce qui se passe lorsque l'horloge passe de 0
à 1. D'abord, les portes-et du maître s'ouvrent, laissant passer la
valeur du signal. Avant que le signal n'arrive au maître, le signal
d'horloge inversé arrive aux portes-et de l'esclave qui se ferment
avant que la sortie (potentiellement modifiée) du maître n'arrive à
l'esclave. L'esclave maintient son ancienne valeur. Vu de l'extérieur,
il ne se passe rien. Cependant le maître est sensible aux changements
du signal.


Expliquer ce que réalise ce circuit.
<!-- copier son entrer vers sortie lorsque l'horloge passe de 1 à 0. La valeur copiée est cette de l'entrée immédiatement avant la transition de l'horloge. Pendant le reste de la période, le bistable est insensible à tout changement de son entrée -->

### circuits séquentiels

Les circuits séquentiels se construisent à partir des bistables. Il
sont donc *synchrones*. En général, il s'agit de circuits à $`m`$
entrées et $`n`$ sorties plus une entrée distinguée : l'*horloge*. La
description du circuit de l'action du circuite se fait à l'aide d'une
*table d'états*.

Une table d'état ressemble à une table de vérité. Toutefois, comme il
faut tenir compte de la sortie, on divise la table en deux.

Dans la partie de gauche, on inscrit les valeurs des entrées **et des
sorties**.

Dans la partie de droite, on inscrit les valeurs des sorties **après
le prochain front d'horloge**.

Voici un exemple de table d'états :

| $`x`$ | $`a`$ | $`b`$ | $`c`$ |   | $`a'`$ | $`b'`$ | $`c'`$ |
|:------|-------|-------|-------|---|--------|--------|-------:|
| 0     | 0     | 0     | 0     |   | 0      | 0      |      0 |
| 0     | 0     | 0     | 1     |   | 0      | 0      |      1 |
| 0     | 0     | 1     | 0     |   | 0      | 1      |      0 |
| 0     | 0     | 1     | 1     |   | 0      | 1      |      1 |
| 0     | 1     | 0     | 0     |   | 1      | 0      |      0 |
| 0     | 1     | 0     | 1     |   | 1      | 0      |      1 |
| 0     | 1     | 1     | 0     |   | 1      | 1      |      0 |
| 0     | 1     | 1     | 1     |   | 1      | 1      |      1 |
| 1     | 0     | 0     | 0     |   | 0      | 0      |      1 |
| 1     | 0     | 0     | 1     |   | 0      | 1      |      0 |
| 1     | 0     | 1     | 0     |   | 0      | 1      |      1 |
| 1     | 0     | 1     | 1     |   | 1      | 0      |      0 |
| 1     | 1     | 0     | 0     |   | 1      | 0      |      1 |
| 1     | 1     | 0     | 1     |   | 1      | 1      |      0 |
| 1     | 1     | 1     | 0     |   | 1      | 1      |      1 |
| 1     | 1     | 1     | 1     |   | 0      | 0      |      0 |

**à faire**

Décrire le comportement d'un circuit ayant cette table d'états.
<!-- il s'agit d'un compteur avec une entrée supplémentaire pour indiquer si le compteur compte ou par -->

### Conception d'un circuit séquentiel

En général, pour concevoir un circuit séquentiel à $`m`$ entrées et
$`n`$ sorties, on utilise $`n`$ bistables $`D`$ (un pour chaque
sortie), et un circuit combinatoire à $`m+n`$ entrées et $`n`$
sorties. De plus, on relie les $`n`$ sorties des bistables aux entrées
du circuit combinatoire.

Grace aux bistables $`D`$, le front d'horloge synchronise la recopie
des sorties du circuit combinatoire sur les sortie du circuit
séquentiel.

En utilisant cette méthode, il est possible d'utiliser les méthodes de
conception des circuits combinatoire, en utilisant utilisant la table
d'états comme une table de vérité.

Pour illustrer cela, réalisons un compteur de 2 bits :

| $`u/d`$ | $`y_1`$ | $`y_0`$ |   | $`y'_1`$ | $`y'_0`$ |
|---------|---------|---------|---|----------|----------|
| 0       | 0       | 0       |   | 0        | 0        |
| 0       | 0       | 1       |   | 0        | 0        |
| 0       | 1       | 0       |   | 0        | 1        |
| 0       | 1       | 1       |   | 1        | 0        |
| 1       | 0       | 0       |   | 0        | 1        |
| 1       | 0       | 1       |   | 1        | 0        |
| 1       | 1       | 0       |   | 1        | 1        |
| 1       | 1       | 1       |   | 1        | 1         |
- 
**à faire** 

Réaliser le circuit conditionnel correspondant, puis créer le circuit
séquentiel en reliant les sorties des bistables aux entrées.

Lancer la simulation.


**exercice**

Réaliser un circuit dont la table d'état est :

| $`x`$ | $`a`$ | $`y`$ |   | $`a'`$ | $`y'`$ |
|:------|-------|-------|---|--------|-------:|
| 0     | 0     | 0     |   | 0      |      0 |
| 0     | 0     | 1     |   | 0      |      0 |
| 0     | 1     | 0     |   | 1      |      1 |
| 0     | 1     | 1     |   | 1      |      1 |
| 1     | 0     | 0     |   | 0      |      1 |
| 1     | 0     | 1     |   | 0      |      1 |
| 1     | 1     | 0     |   | 1      |      0 |
| 1     | 1     | 1     |   | 1      |      0 |

### Circuits séquentiels classiques

#### Registre

Un *registre* est un circuit séquentiel à $`n+1`$ entrées et $`n`$
sorties. À chaque sortie correspond une entrée. Les $`n`$ premières
entrées sont notées $`x_0,x_1,\ldots,x_{n-1}`$ et la dernière $ld$
(*load*). Les sorties sont appelées $`y_0,y_1,\ldots,y_{n-1}`$.

Lorsque $`ld`$ vaut 0, les sorties ne sont pas affectées par un front
d'horloge. Elles préservent leurs anciennes valeurs. Par contre,
lorsque $ld$ vaut 1, les entrées sont copiées vers les sorties après
le prochain front d'horloge.

Plus formellement :

| $`ld`$ | $`x_3`$ | $`x_2`$ | $`x_1`$ | $`x_0`$ | $`y_3`$ | $`y_2`$ | $`y_1`$ | $`y_0`$ | $`y'_3`$ | $`y'_2`$ | $`y'_1`$ | $`y'_0`$ |
|:-------|---------|---------|---------|---------|---------|---------|---------|---------|----------|----------|----------|---------:|
| 0      | *       | *       | *       | *       | $`c_3`$ | $`c_2`$ | $`c_1`$ | $`c_0`$ | $`c_3`$  | $`c_2`$  | $`c_1`$  |  $`c_0`$ |
| 1      | $`c_3`$ | $`c_2`$ | $`c_1`$ | $`c_0`$ | *       | *       | *       | *       | $`c_3`$  | $`c_2`$  | $`c_1`$  |  $`c_0`$ |

Il y a différents types de registre, en fonction :
- des entrées : parallèles ou série
- du type de registre : mémoire, décalage

Les registres s'obtiennent en enchaînant des bistables ou des bascules.

**à faire**

On donne le registre suivant :

![](./figs/registre.png)

Et le fichier logisim correspondant :

[Registre général 4 bit](./circ/reg4bit.circ)

Observer le comportement de ce registre en fonction des valeurs de $`s_0`$ et $`s_1`$.

#### Multiplication binaire

Avec le registre précédent, on est armé pour effectuer la multiplication binaire.

L'idée est :
1. Charger la première opérande $`y`$ dans le registre.
2. Configurer le registre pour décaler à droite.
3. Pour chaque bit en sortie, on effectue un et logique avec chaque
   bit de la deuxième opérande $`x`$.
4. Le résultat est cumulé au résultat précédent grace à un
   additionneur 4 bits.
6. On enregistre les résultats en sortie en utilisant le registre
   configuré pour se décaler.
   
![](./figs/multiplier.png)
