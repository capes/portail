---
title: mini-projet bataille (Tle NSI)
---

# Démarche de Projet 

Voici un extrait du Programme de Tle NSI paru au BO :

> "Un enseignement d’informatique ne saurait se réduire à une présentation de concepts ou de méthodes sans permettre aux élèves de se les approprier en développant des projets.
> Un quart au moins de l’horaire total de la spécialité est réservé à la conception et à l’élaboration de projets conduits par les élèves.
> Les projets réalisés par les élèves, sous la conduite du professeur, constituent un apprentissage fondamental tant pour l’appropriation des concepts informatiques que pour l’acquisition de compétences "

On souhaite faire réaliser par les élèves un "mini-projet" guidé ayant pour objectifs de réutiliser quelques concepts déjà vus en classe de Terminale NSI:   

- la POO
- les structures de pile et de file

On s'appuiera pour ce faire sur un programme qui permet de jouer à une version simplifiée du « jeu de la bataille » (sans carte masquée en cas de « bataille »).

# Description du jeu de la Bataille

Dans cette version, les joueurs ont initialement un nombre égal de cartes au hasard. 
À chaque tour, les joueurs jouent chacun la première de leur carte en la déposant sur la table.  
Celui qui a déposé la carte de plus haute valeur remporte toutes les cartes sur la table et les range à la fin de ses cartes.   
Si les hauteurs des deux cartes jouées sont égales, il y a alors « bataille » et ces cartes sont laissées sur la table en les empilant sur les cartes éventuellement présentes.
Le tour suivant peut alors commencer.

Le jeu s’arrête dès que l’un des joueurs n’a plus de carte, dans ce cas il a perdu la partie (NB : des parties infinies sont possibles, on ne cherchera pas à détecter ces situations).

Le nombre de cartes que possède chaque joueur
au début de la partie devra pouvoir être choisi au démarrage (ce nombre devra être inférieur ou égal à 16 qui sera aussi la valeur par défaut)

# Contraintes de programmation

* Le module [card](./card.py) est fourni
* les structures de pile et de file utilisées devront provenir du module [queue](https://docs.python.org/fr/3/library/queue.html?highlight=queue#module-queue) et/ou du module [collections](https://docs.python.org/fr/3/library/collections.html#module-collections) de Python dont vous pourrez étudier préalablement la documentation 

# Séquence pédagogique

__Construire une activité qui amène les élèves à progressivement à implanter le programme de bataille précédemment décrit.__  

Vous devrez respecter les __régles__ données et les __contraintes__ imposées.

__Le projet doit être guidé__ : vous devrez donc fournir un ou des __scripts supports à compléter__ ainsi qu'un jeu de __questions progressives__ permettant à des élèves qui ne connaissant pas à priori les modules fournis de coder ce que vous leur demandez

Un __pseudo-code déroulant un tour de jeu__ devra être fourni.

Les élèves devront être en capacité de __vérifier les portions de codes progressivement écrites__ à l'aide d'un outil adpaté que vous leur founirez.

# Travail à rendre

Sur votre dépot git `meef-m1`, dans un répertoire nommé `bataille`

- Le __sujet à destination des élèves__ en version markdown accompagné des scripts fournis aux élèves
- Un __Makefile__ permettant de générer un pdf à partir du sujet précédent
- Un __script correction__ (à destination de l'enseignant) fournissant un programme fonctionnel.