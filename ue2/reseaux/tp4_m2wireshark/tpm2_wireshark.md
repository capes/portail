---
title: Analyse de trames avec Wireshark
---

_Exercices proposés (pour une grande partie des questions) au [DIU EIL de Bordeaux](https://moodle1.u-bordeaux.fr/course/view.php?id=4713#section-3)_

# Introduction

Voici les informations utiles sur la machine qui a servi à capturer les trames réseaux que vous allez analyser. Ces informations sont obtenues avec les commandes Linux suivantes :

```bash
$ ip addr
eth0: flags=4163<UP,BROADCAST,RUNNING,  MULTICAST>  mtu 1500
        inet 10.0.2.15  netmask 255.255.255.0    broadcast 10.0.2.255
        ether 08:00:27:0b:03:37  txqueuelen   1000  (Ethernet)
        RX packets 60  bytes 8043 (7.8 KiB)
        RX errors 0  dropped 0  overruns 0    frame 0
        TX packets 63  bytes 10204 (9.9 KiB)
        TX errors 0  dropped 0 overruns 0    carrier 0  collisions 0
lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0    frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0    carrier 0  collisions 0

$ ip route
default via 10.0.2.2 dev eth0 proto dhcp src 10.0.2.15 metric 100
10.0.2.0 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
10.0.2.2 dev eth0 proto dhcp scope link src 10.0.2.15 metric 100


$ cat /etc/resolv.conf
nameserver 192.168.1.1
```

__Q1__ Retrouvez l'adresse IP de la machine utilisée (et son masque de réseau), l'adresse IP de la passerelle vers Internet, ainsi que l'adresse du serveur DNS local...  

__Q2__ A quoi correspond l'interface réseau `lo` affichée par la commande `ip addr` ?  

__Q3__ A quoi sert le fichier `/etc/resolv.conf` ?

# Protocoles DNS / commande ping

Considérons la trame suivante [`ping.pcap`](./ping.pcap) à télécharger et à ouvrir avec Wireshark

La trace enregistrée se divise en trois étapes principales identifiées avec des couleurs différentes dans Wireshark :  

- Trames 1-2 (ARP) : On cherche à découvrir l'adresse Ethernet de la passerelle, afin de connaître la porte de sortie du réseau local vers Internet.
- Trames 3-4 (DNS) : On cherche à trouver l'adresse IP de la machine cible (www.google.com).
- Trames 5-18 (ICMP) : 7 pings et leurs réponses... 

__Q4__ A quelle adresse Ethernet est destinée la requête ARP (trame 1) émise par la machine cliente ? Expliquez .

__Q5__ La requête ARP "WHO HAS" (trame 1) cherche à trouver l'adresse Ethernet de la machine 10.0.2.2. Pourquoi cette machine et non pas la machine cible www.google.com ?

__Q6__ Quel est le protocole de transport utilisé pour les échanges DNS (trames 3-4) ? Observez en détail la réponse DNS (section Answers) et découvrez ainsi l'adresse IP de la "machine" www.google.com retourné par le serveur DNS.

__Q7__ Consultez la [ressource suivante](https://www.nameshield.com/ressources/lexique/dns-domain-name-system/) et faites un résumé des étapes liées à la résolution DNS.

__Q8__ Observez la première requête / réponse ICMP (trames 5-6) et observez la valeur du champs type dans l'en-tête... 

# Connexion TCP et protocole HTTP

Que se passe-t-il quand je consulte une page web (par exemple, http://www.perdu.com ) sur Internet avec mon navigateur préféré ? Pour comprendre en détail ce qui se passe, nous avons capturé, à l'aide de l'outil tcpdump (en ligne de commande) ou directement avec Wireshark, tout le traffic réseau entrant & sortant de la carte réseau (Ethernet) de cette machine.

```bash
$ tcpdump -w http.pcap &     # on démarre la capture en arrière-plan (&)
$ wget http://www.perdu.com  # on télécharge la page web en ligne de commande
$ pkill tcpdump              # on stoppe la capture
```

La trace enregistrée [`http.pcap`](./http.pcap) effectue les mêmes étapes préliminaires que précédemment : requêtes ARP et DNS... Concentrons nous ici sur la conversation TCP/IP (trames 7-17). On demande la page d'accueil "/" ou "/index.html" du serveur web (www.perdu.com) en utilisant le protocole HTTP au dessus de TCP, ce qui implique plusieurs étapes intermédiaires :

- La connexion TCP en trois temps (trames 7-9)
- La requête HTTP ainsi que la réponse incluant le code HTML (trames 10-13). 
- La phase de déconnexion (trames 14-17). 

__Q9__ Considérons la première trame TCP/IP qui ouvre la connexion (trame 7). Trouvez dans l'en-tête TCP le port source et le port de destination. Ce dernier est standard pour tous les serveur web (80). A quoi correspond le flag SYN dans cette en-tête ? 

__Q10__ Identifiez dans la conversation TCP les trames correspondant à la requête HTTP et à la réponse HTTP...  
Dans l'en-tête de la requête HTTP, on observe sur la première ligne qu'il s'agit de la requête "GET / HTTP/1.1". Identifiez le rôle des champs suivants : User-Agent, Host, Connection.

__Q11__ La réponse HTTP commence par la ligne suivante "HTTP/1.1 200 OK" qui indique que tout s'est bien passé (code 200). Vous devez déjà connaître le fameux code d'erreur 404
Rappelez les différentes __catégories__ de code d'erreur et leur significations

__Q12__ Observez maintenant les différents champs dans la réponse HTTP et en déduire le logiciel serveur, la longueur et le type de contenu dans cette réponse.

__Q13__ Immédiatement après l'en-tête HTTP, vous pouvez identifier le code HTML de la page web : "<html>...</html>". 
Trames 7-16 : Pour lire plus facilement la conversation TCP, vous pouvez cliquer sur un des paquets TCP et utiliser "Follow -> TCP Stream"... Notez qu'il est possible de reconstruire précisément le fil de la conversation grâce au sequence number (en octets) qui se trouve dans l'en-tête TCP. Expliquez

## Tempus Fugit

Pour aller plus loin, étudions cette page web : http://aurelien.esnard.emi.u-bordeaux.fr/tempus.html. Etudiez son code source dans Chrome ou Firefox (Ctrl+U). Voici la capture des trames réseaux lorsqu'un navigateur consulte cette page... 

Ouvrez la trace  [`tempus.pcap`](./tempus.pcap ) dans Wireshark.

__Q14__ Une page web comme tempus.html se compose en général de plusieurs fichiers annexes (JavaScript, Images, ...). Ici, combien de fichiers sont téléchargés en plus ? Lesquels ?

__Q15__ Combien y-a-t-il de connexions TCP/IP ? Que signifie le champs "Connection: Keep-Alive" dans l'en-tête de la requête HTTP ?