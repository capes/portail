# Objectifs

* Découvrir un logiciel de simulation de réseau pouvant être utilisé simplement au lycée.
* __Notions__ : modèle en couches TCP/IP, commutateur, routeur, adresse physique, protocoles ARP et ICMP, broadcast, passerelle.
* Trouver l'adresse du réseau et le masque de sous-réseau à partir d'une adresse IPv4 en notation CIDR.  

# Dans les programmes de lycée

* En SNT :

![bo snt](./fig/bo_snt.png)

* En 1ère NSI :

![bo snt](./fig/bo_1ere.png)

# Le logiciel FILIUS

* Le logiciel est déjà installé sur les machines de la salle mais vous pouvez aussi le télécharger [ici](https://www.lernsoftware-filius.de/Herunterladen)

_la version sans installateur (par exemple sous windows) contient une archive jar exécutable : ceci vous permet d'utiliser la dernière version en date_

Lors de la première exécution vous pouvez sélectionner le langage Français

Si vous vous êtes trompés,  dans le répertoire d'installation modifiez le fichier `filius.ini` (dans le répertoire `config`) en décommantant la ligne

```ini
# locale=fr_FR
```

* [Guide du débutant en français](https://github.com/KELLERStephane/KELLER-Stephane-Tests2maths/blob/master/6%20-%20Filius/Filius%20guide%20du%20debutant%202022.pdf) ou [ici](./filius_guide_2021.pdf)

Lire les premières pages (_3-8_) pour découvrir l'interface.  
La suite de l'activité s'inspire des situations proposées mais va plus loin...

---

__DURANT TOUTE CETTE ACTIVITE, REPONDEZ AUX DIFFERENTES QUESTIONS ET NOTEZ CE QUI VOUS SEMBLE IMPORTANT DANS UN DOCUMENT MARKDOWN__

---

# Connexion directe entre 2 machines

__1.__ Suivre pas à pas les _exercices 1 et 2_ du guide débutant

Le `ping` est un outil d’administration permettant de vérifier la disponibilité d’un autre ordinateur.

__2.__ a- Quel est le nom du __protocole__ utilisé ? A quelle __couche__ du modèle cela correspond-t-il ?  
       b- Le guide du débutant parle de __modèle__ OSI est-ce le cas ici ? Donner le nom (et l'ordre) des couches dans les modèles évoqués précedemment.    
       c- En cliquant sur une des "lignes" de la fenêtre échange de données, on peut afficher les informations sur les couches inférieures. Expliquez le principe de l'__encapsulation__.    
       d- Quelle critique pourriez vous formuler quant au choix fait dans ce logiciel ?

__3.__ La trace d'exécution de la commande `ping` dans le terminal délivre plusieurs informations. Expliquez-les.

# Connexions d'ordinateurs à l'aide d'un commutateur (switch)

(_Attention ne suivez pas le guide débutant !!_)

__1.__ Ajoutez au réseau précédent un nouvel ordinateur d'IP `192.168.0.12` connecté au switch

![3 ordinateurs](./fig/3ordis_switch.png)

a- Expliquez la __notation utilisée__ ici pour représenter l'IP? A quelle __norme__ cela correspond-t-il ? Existe-t-il d'autres normes ?

b- On ne peut pas utiliser n'importe quelle adresse IP pour un réseau privé. Complétez le tableau suivant en précisant le nombre d'adresses disponibles pour chacune des plages d'adresses privées possibles

|Plage IP|Nombre d'adresses|
|:---:|:---:|
|10.0.0.0 – 10.255.255.255| |
|172.16.0.0 – 172.31.255.255| |
|192.168.0.0 – 192.168.255.255| |

__2.__ Lancer la simulation et vider les tables d'échanges de `0.10` et `0.11`. Faites un ping depuis `0.10` vers `0.12`, obervez les tables d'échanges.  
a- Quel est le rôle du protocole __ARP__ ? A quelle couche appartient-il ?  
b- Observez la table d'échanges des différentes machines du réseau. Expliquer la nature de l'adresse évoquée dans le commentaire de ce protocole (précisez sa forme).  
c- Retrouvez cette adresse en utilisant le terminal de `0.10`  

```bash
% ipconfig
```

Est-il correct de dire qu'une machine est associée à une __adresse physique__ ?

__3.__ Vérifiez la destination de la requête ARP en regardant la couche réseau ? A quoi correspond cette addresse ?

__4.__ Vérifiez-le en comparant les échanges au niveau de l'autre portable `0.11`

__5.__ Double-cliquer sur le switch et observez sa __table SAT__. Quel est son utilité ? Que est le nom plutôt utilisé habituellement pour désigner cette table ?

__6.__ Installez l'application ligne de commande sur `0.11`

Exécutez la commande permettant d'afficher le contenu du cache arp de la machine.

```bash
arp -a
```

Faites de même pour la machine `0.10`, qu'en déduisez-vous ?

__7.__ Faites un ping vers `0.10` et expliquez toutes les modifications effectuées sur les différentes machines du réseau. 

__8.__ Refaites un ping ? Quelle différence ?

Pour en savoir plus : https://fr.wikipedia.org/wiki/Address_Resolution_Protocol

# Simulation de réseaux interconnectés

__1.__ Suivre pas à pas les _exercices 5 et 6_ du guide du débutant  et répondre aux questions suivantes (le serveur `0.12` est remplacé dans notre cas par un __portable `0.12`__)

![routeur](./fig/routeur.png)

__2.__ En mode simulation, vérifier sur un des portables que la passerelle par défaut est bien configurée

__3.__ Que vaut le ttl des paquets ICMP reçus par `0.10` lors de l'utilisation de la commande `ping` une fois les configurations de passerelles correctement effectuées ?  
Vérifier le cache `arp` de cet ordinateur, quelle adresse a été rajoutée après un ping vers un ordinateur appartenant à un autre sous-réseau ?

__4.__ Utilisez la commande `traceroute` entre l'ordinateur `0.10` et l'ordinateur `1.11`. Quelles informations sont fournies ?

__5.__ Essayez cette commande sur votre machine avec l'adresse _fr.wikipedia.org_ par exemple.  

_Remarque_ : Le nom de domaine est ici préalablement résolu en IP à l'aide du __DNS__ (par défaut il s'agit certainement de celui configuré par votre FAI.)
Pour en savoir plus :  

* https://fr.wikipedia.org/wiki/Domain_Name_System
* Exercices 10, 11 et 12 du guide du débutant
