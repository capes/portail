# Objectifs

Nous allons utiliser Wireshark pour voir différents aspects du protocole HTTP : requête/réponse HTTP, format des messages, récuperation de fichier HTML volumineux, fichiers HTML avec objets inclus, authentification HTTP.

# Travail préalable
Installer  [Wireshark](https://www.wireshark.org/download.html) sur votre machine.  
Réaliser les captures indiquées par les icônes ⚙️ dans la suite de l'activité.  
Vous sauvegarderez chacune de ces captures dans un fichier `.pcapng`  

Ce TP est adapté des Wireshark labs de Kurose et Ross proposés en suppléments à leur livre _Computer Networking : a top down approach_ ( c 2005-2012, J.F Kurose and K.W. Ross, All Rights Reserved) et de la version proposée en M1 Informatique par l'Université de Bourgogne.

# Requêtes et réponses

⚙️ Démarrer votre navigateur web (on utilisera de préférence `Firefox`), démarrer `wireshark` et mettre le filtre `http` dans la fenêtre de filtre d'affichage. Attendre 1 minute et démarrer la capture.

⚙️ Saisir l'URL http://gaia.cs.umass.edu/wireshark-labs/HTTP-wireshark-file1.html dans le navigateur et stopper la capture.

🆘[_La capture à télécharger (uniquement si vous n'y arrivez pas)_](./captures/c1.pcapng)

Chaque requête se compose d'une ou plusieurs lignes de texte ASCII, le premier mot de la première ligne correspondant au nom de la __méthode__ invoquée par le protocole HTTP. Cette première ligne est suivie d'en-têtes de message (_request headers_).  
En regardant les messages de requête et reponse, répondre aux questions suivantes :

1. Quelle méthode est invoquée dans la requête faite par le client ?
2. Quelle sont les versions d'HTTP de votre navigateur? du serveur?
3. Quel(s) langage(s) votre navigateur accepte-t-il? Recherchez la signification des caractères `;q=0.5`...
4. Quand le fichier auquel vous accédez a-t-il été modifié pour la dernière fois côté serveur? Où trouve-t-on cette information ?
5. Que contient le corps de la réponse ?


# GET conditionnel HTTP

⚙️ Lancer wireshark, démarrer la capture, saisir l'URL http://gaia.cs.umass.edu/wireshark-labs/HTTP-wireshark-file2.html dans le navigateur et rapidement après, entrer à nouveau la même URL (ou cliquer sur le bouton de rafraichissement), puis stopper la capture.  
_Si vous souhaitez réitérer l'opération pensez à vider le cache de votre navigateur avant `CTRL + F5`_

🆘[_La capture à télécharger (uniquement si vous n'y arrivez pas)_](./captures/c2.pcapng)

Repondre aux questions suivantes :

1. Dans la première requête HTTP GET, le champs `If-Modified-Since` est-il présent?
2. Le serveur a-t-il retourné le contenu du fichier?
3. Dans la seconde requête HTTP GET (_concernant le fichier html_), le champ `If-Modified-Since` est-il présent? Si oui, quelle information contient-il?
4. Quels sont les codes et messages retournés par le serveur? Le serveur a-t-il retourné le contenu du fichier?
5. A l'aide des observations précédentes et des ressources suivantes :  

* https://developer.mozilla.org/fr/docs/Web/HTTP/Conditional_requests
* https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/If-Modified-Since
* https://developer.mozilla.org/fr/docs/Web/HTTP/Caching#freshness

Proposez une explication simple que vous pourriez fournir à un élève de 1ère sur la mise en cache et le GET conditionnel

# Fichier HTML volumineux

⚙️ Lancer wireshark, démarrer la capture, saisir l'URL http://gaia.cs.umass.edu/wireshark-labs/HTTP-wireshark-file3.html dans le navigateur, puis stopper la capture.

🆘[_La capture à télécharger (uniquement si vous n'y arrivez pas)_](./captures/c3.pcapng)

Répondre aux questions suivantes :

1. Combien de requêtes HTTP GET votre navigateur a-t-il envoyé? Quel numéro de paquet dans la trace contient le message GET?
2. Quel numéro de paquet contient le code de status et le message associé retourné par le serveur?
3. Combien de segments TCP de données ont été nécessaires pour transporter la réponse HTTP et le message associé (Déclaration des droits Americains)? _Vous pouvez visualisez toutes les trames indépendamment si vous le souhaitez (clic droit Show linked packet)_
4. Recherchez la signification des termes `MTU` et `MSS` et expliquez les tailles des segments TCP

# Document HTML avec objets inclus

⚙️ Lancer wireshark, démarrer la capture, saisir l'URL http://gaia.cs.umass.edu/wireshark-labs/HTTP-wireshark-file4.html dans le navigateur, puis stopper la capture.

🆘[_La capture à télécharger (uniquement si vous n'y arrivez pas)_](./captures/c4.pcapng)

Répondre aux questions suivantes :

1. Combien de requêtes HTTP GET votre navigateur a-t-il envoyé? A quelles adresses internet ces requêtes ont-elles ete envoyées?
2. Est-il possible de savoir si votre navigateur a télechargé les images de façon séquentielle ou si elles ont été téléchargées en parallèle? Expliquer.
3. HTTP/1.1 gère les connexions persistantes : recherchez ce que cela signifie.
4. Qu'apporte HTTP/2 ?

Pour vous aider : https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/Evolution_of_HTTP

# Authentication HTTP

⚙️ Lancer wireshark, démarrer la capture, saisir l'URL http://gaia.cs.umass.edu/wireshark-labs/protected_pages/HTTP-wireshark-file5.html.
Dans le navigateur, saisir `wireshark-students` pour username et `network` pour le password puis stopper la capture.

🆘[_La capture à télécharger (uniquement si vous n'y arrivez pas)_](./captures/c5.pcapng)

Repondre aux questions suivantes :

1. Quelle est la réponse du serveur (code et message) à la première requête HTTP GET?
2. Quand votre navigateur envoi la requête pour la seconde fois, quels nouveaux champs sont inclus dans ce message GET HTTP?
3. Vérifier l'encodage correct des identifiants  
Pour en savoir plus sur l'authentification en HTTP : https://developer.mozilla.org/fr/docs/Web/HTTP/Authentication

# Séquence pédagogique

Voici un extrait du programme de 1ère NSI :

![bo nsi](./fig/bo_1ere.png)

Wireshark est un outil surement un peu trop complexe pour être utilisé avec des élèves de lycée. Heureusement Firefox(et pas seulement) propose dans ses outils de développement un moniteur réseau.
La ressource suivante vous le présente https://developer.mozilla.org/fr/docs/Tools/Network_Monitor

__Proposez un TP complet utilisant ce moniteur réseau à réaliser avec des élèves.__  

__Sont attendus : un TP élève, une correction (sources markdown et pdf)__


