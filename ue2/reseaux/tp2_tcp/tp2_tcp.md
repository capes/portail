# Objectifs

* Etablissement d'une connexion TCP entre un client et un serveur. 
* Utilisation d'un analyseur de réseau Wireshark

# Dans les programmes de lycée

* En SNT :

![bo snt](./fig/bo_snt_1.png)

# Etablissement et libération d'une connexion TCP

Le protocole TCP permet dans la couche transport de découper le flux d'octets en __segments__ dont la structure est la suivante :

![segment TCP](./fig/segment_tcp.png)

Etudions l'établissement de la connexion TCP entre un client et son hôte.

__1.__ Récupérez le fichier [tcp.fls](./tcp.zip) et ouvrez-le avec Filius.  

* Installer l'application `serveur générique` sur le __SERVEUR__ et le démarrer.
* Installer l'application `client générique` sur `CLIENT1` en configurant l'adresse du serveur puis connectez-vous.

Lorsqu'un client souhaite se connecter à un serveur avec le protocole TCP, il initie une mise en place de connexion en trois temps (`three way handshake`).

En observant les tables d'échanges de données réalisés, complétez le chronograme suivant

![modèle de chronogramme](./fig/chronogramme.jpg) 

Vous devrez représenter chaque trame envoyée et reçue de part et d'autre. Et,  pour chacune d'entre elles vous préciserez sur le chronogramme :  
* Les étiquettes des segments (`flags`) (`SYN` pour synchronised, `SYN-ACK` : pour synchronized-acknoledgment ou `ACK` )  
* les numéros des séquences et des acquittements échangés (les 3 derniers chiffres suffisent)

__2__ Au début de cette connexion, le client choisit un numéro aléatoire pour la séquence. Il en est de même pour le serveur.
Dans les échanges successifs, quel lien y-a-t-il entre les numéros de séquence et d'acquittement ?

__3.__ A la fin hote et client partagent une paire commune de numéros de séquence.
TCP est un protocole fonctionnant en mode __connecté__ : une session de communication est établie entre les 2 machines (tant que la connexion TCP est établie : la communication est possible).
Déconnectez le client et décrire ce qui se passe.  

# Utilisation d'un analyseur de trames : Wireshark

[Wireshark](https://www.wireshark.org/download.html) est utilisé pour l'analyse des réseaux : il est libre et gratuit.    
Nous utiliserons par la suite un fichier de capture provenant du MOOC _Principe des réseaux de données_ (platemorme _fun-mooc.fr_).  

Télécharger puis ouvrez dans Wireshark le fichier  [Trace_30_9_2014.pcapng](./Trace_30_9_2014.pcapng).

![interface Wireshark](./fig/interface_wireshark.png)

__1.__ Combien de trames sont capturées dans ce fichier ?

__2.__  Quelle est l'adresse IP de la machine sur lequel a été faite cette capture ?

__3.__ Déplacez-vous sur la trame qui est passée à l'instant `24,488582`  
    __a-__ Quelle est la taille de la trame en octets ?  
    __b-__ Quel est le protocole de transport de cette trame ?  

__c-__ Combien de couches (entités) protocolaires cette trame va-t-elle traverser de la couche physique (incluse) à la dernière couche qui l'utilise (incluse)?

Vous pouvez cliquer sur les différentes couches et observez l'encapsulation des données   
__4.__ Sélectionnez le paquet IP.
Une en-tête IPv4 correspond au format suivant : ![datagramme ip](./fig/datagramme_ip.png)

Pour plus d'infos sur les champs : https://fr.wikipedia.org/wiki/IPv4

__a-__ Retrouvez dans ce paquet la version du protocole, le protocole utilisé par la couche supérieure, l'adresse source et l'adresse de destination ( Vérifiez que le nombre de bits pour chacune de ces informations correspond bien au format précédent.)

__b-__ Ce paquet a-t-il été fragmenté ?

__5.__ Les trames 50, 51 et 52 correspondent à l'établissement d'une connexion TCP. Retrouvez les informations précédemment en regardant notamment les numéros de séquence et d'acquittement échangés.

__6.__ Filtrez les protocoles TCP et recherchez à quelles trames correspondent la fermeture de cette connexion.
_remarque_ : il suffit de taper dans la zone de filtre le nom du protocole par exemple et valider

![filtre](./fig/filtre.png)

__7.__ On s'intéresse aux segments TCP échangés durant la session délimitée par les trames précédentes.
Observez les trames 53 à 56.
A chaque fois que le client envoie un segment TCP il est acquitté. En observant les contenus de ces segments expliquez comment assure la fiabilité des échanges (Vous expliquerez notamment la différence avec Filius)


__POUR ALLER PLUS LOIN SUR LA COUCHE TRANSPORT__ : https://zestedesavoir.com/tutoriels/2789/les-reseaux-de-zero/dans-les-basses-couches-du-modele-osi/exploration-de-la-couche-transport-1-2/