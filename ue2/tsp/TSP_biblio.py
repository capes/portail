#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: module
:author: FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>_
:date: Janvier 2020

Lecture et tracé d'une tournée.
"""

import random, numpy, math, pylab, copy

def get_tour_fichier(fname):
    """Lecture du fichier de villes au format : ville, latitude, longitude
    Renvoie un tour contenant les villes dans l ordre du fichier
    :param fname: (str) le nom d'un fichier contenant la tournée
    :rtype: list
    """
    with open(fname, "r") as f:
        tour_initial = f.read().replace (",", ".").split ("\n")
    tour_initial = [t.strip("\r\n ").split ("\t") for t in tour_initial]
    tour_initial = [t[:1] + [float (x) for x in t [1:]] for t in tour_initial]
    return tour_initial[:len(tour_initial)-1]


def distance (tour, i, j) :
    """Calcul de la distance euclidienne entre i et j
    :param tour: (list) sequence de villes
    :param i: (int) numero de la ville de départ
    :param j: (int) numero de la ville d arrivee
    :rtype: float
    :CU: i et j dans le tour
    """
    dx = tour[i][1] - tour[j][1]
    dy = tour[i][2] - tour[j][2]
    return (dx**2 + dy**2) ** 0.5


def longueur_tour (tour) :
    """Calcule la longueur totale d une tournée partant de la ville de départ et revenant à celle-ci
    :param tour: (list) tournée de villes, n villes = n segments
    :return: (float) distance totale
    """
    d = 0
    for i in range (0, len(tour)-1) :
        d += distance (tour, i, i+1)
    # il ne faut pas oublier de boucler pour le dernier segment  pour retourner à la ville de départ
    d += distance (tour, 0, -1)
    return d


def trace (tour) :
    """Trace la tournée realisée
    :param tour: liste de villes
    """
    x = [t[1] for t in tour]
    y = [t[2] for t in tour] 
    x += [x[0]] # on ajoute la dernière ville pour boucler
    y += [y[0]] #
    pylab.plot (x, y, linewidth=5)
    for ville,x,y in tour :
        pylab.text (x, y, ville) 
    pylab.show()

    
def test():
    """Test d'utilisation des fonctions precedentes
    """
    the_tour = get_tour_fichier("exemple.txt")
    print(the_tour)
    print(longueur_tour(the_tour))
    trace(the_tour)
