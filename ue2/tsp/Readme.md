# TSP - Le voyageur de commerce 

Le problème du voyageur de commerce - _Traveling Salesman Problem_ TSP -, étudié depuis le 19e
siècle, est l’un des plus connus dans le domaine de la recherche opérationnelle. William Rowan
Hamilton a posé pour la première fois ce problème sous forme de jeu dès 1859.

## Problème

Le problème du TSP sous sa forme la plus classique est le suivant : « Un voyageur de commerce doit
visiter une et une seule fois un nombre fini de villes et revenir à son point d’origine. Trouvez
l’ordre de visite des villes qui minimise la distance totale parcourue par le voyageur ». Ce
problème d’optimisation combinatoire appartient à la classe des problèmes NP-Complets.

Les domaines d’application sont nombreux : problèmes de logistique, de transport aussi bien de
marchandises que de personnes, et plus largement toutes sortes de problèmes
d’ordonnancement. Certains problèmes rencontrés dans l’industrie se modélisent sous la forme d’un
problème de voyageur de commerce, comme l’optimisation de trajectoires de machines outils : comment
percer plusieurs points sur une carte électronique le plus vite possible ?

Pour un ensemble de $`n`$ points, il existe au total $`n!`$ chemins possibles. Le point de départ ne
changeant pas la longueur du chemin, on peut choisir celui-ci de façon arbitraire, on a ainsi
$`(n-1)!`$ chemins différents. Enfin, chaque chemin pouvant être parcouru dans deux sens et les deux
possibilités ayant la même longueur, on peut
diviser ce nombre par deux.  
Par exemple, si on nomme les points, $`a,b, c, d`$ , les chemins $`abcd, bcda, cdab, dabc, adcb, dcba, cbad, badc`$ ont tous la même longueur, seul le point de départ et le sens
de parcours change. On a donc $`\frac{1}{2}(n-1)!`$ chemins candidats à
considérer.   
Par exemple, pour $`71`$ villes, le nombre de chemins
candidats est supérieur à $`5 × 10^{80}`$ qui est environ le nombre
d'atomes dans l'univers connu.   
([Page wikipedia _Problème du voyageur de commerce_](https://fr.wikipedia.org/wiki/Problème_du_voyageur_de_commerce)).

![img](http://helios.mi.parisdescartes.fr/~moisan/gtnum/data/recuit/car54.jpg)

## Heuristique gloutone

L'objectif de ce TP est de réaliser un algorithme glouton pour
résoudre le TSP.

Pour cela vous avez à votre disposition :

- un jeu de données [exemple.txt](exemple.txt) contenant les coordonnées de différentes villes à
  raison d'une par ligne sous la forme `nom_de_la_ville latitude longitude`, vous pouvez bien sur
  l'étendre ou en générer un nouveau avec vos propres villes.

  Par exemple

  ```
    Annecy	6,082499981	45,8782196
    Auxerre	3,537309885	 47,76720047
    Bastia	9,434300423	42,66175842
  ```

- Un fichier [TSP_biblio.py](TSP_biblio.py) contenant un ensemble de fonctions permettant la lecture
  des données et la visualisation d'un tour réalisé par le voyageur (ici pour le moment dans l'ordre
  d'apparition). Voici les principales fonctions

    ```python
    def get_tour_fichier(fname):
        """Lecture du fichier de villes au format : ville, latitude, longitude
        Renvoie un tour contenant les villes dans l ordre du fichier
        :param fname: (str) le nom d'un fichier contenant la tournée
        :rtype : list
    """
    ```

    ```python
    def distance (tour, i, j) :
        """Calcule de la distance euclidienne entre i et j
        :param tour: (list) sequence de villes
        :param i: (int) numero de la ville de départ
        :param j: (int) numero de la ville d arrivee
        :rtype: float
        :CU: i et j dans le tour
        """
    ```

    ```python
    def longueur_tour (tour) :
        """Calcule la longueur totale d une tournée partant de la ville de départ et revenant à celle-ci
        :param tour: (list) tournée de villes, n villes = n segments
        :return: (float) distance totale
        """
    ```

    ```python
    def trace (tour) :
        """Trace la tournée realisée
        :param tour: liste de villes
        """
    ```


![Tournée Annecy (plus proche voisin)](tournee_Annecy_ppv.png)

Afin de créer l'algorithme glouton pour résoudre le problème du TSP,
nous allons réaliser certaines étapes.

1. Définir l'heuristique de choix de la solution optimale locale
2. Réaliser un programme Python utilisant les fonctions définies pour
   la lecture et l'affichage permettant de mettre en œuvre
   l'heuristique. Pour cela vous pouvez :
   1. réaliser une fonction qui génère une matrice qui stocke les distances 2 à 2 entre toutes les
      villes afin de ne calculer ces distances qu'une seule fois.
   2. réaliser une fonction qui renvoie l'indice de la ville la plus proche étant donnés : une ville
      représentée par son indice, une liste d'indices de villes non encore parcourues, une matrice
      de distance.
   3. réaliser l'heuristique gloutonne donnant le tour parcouru par le voyageur de commerce à partir
      d'une ville donnée en paramètre, la liste des villes et la matrice de distance ville à ville
      (on passera par un système d'indice).


## Pistes de reflexion

### Lecture de fichier

Le fichier fourni en exemple est un fichier csv (le séparateur est une
tabulation).

- Le module python `pandas` permet de manipuler des données lues à
  partir de ces fichiers ;

- Le site [kaggle](http://www.kaggle.com) permet de récupérer de tels
  fichiers, comme par exemple [worldcities.csv](https://www.kaggle.com/juanmah/world-cities)

Écrire un programme python permettant de manipuler les tournées d'un
pays en utilisant `pandas`.

### Calcul de distance.

La distance utilisée dans le fichier est la distance du plan euclidien.

- Est-ce la distance la mieux appropriée ? pourquoi ?

- Peut-on calculer la distance exacte ?

On donne la formule suivante :

$`d(A, B) = R\cos^{-1}\left(\sin(a)\sin(b)+\cos(a)\cos(b)\cos(c-d)\right)`$

avec 

- $`R`$ le rayon de la terre ;

- $`a`$ est la latitude du point $`A`$ exprimée en radians ; 

- $`b`$ est la latitude du point $`B`$ exprimée en radians ;

- $`c`$ est la longitude du point $`A`$ exprimée en radians ; 

- $`d`$ est la longitude du point $`B`$ exprimée en radians ;

Après avoir implanté cette nouvelle notion de distance, comparez les
résultats obtenus.


