---
title: site support avec mkdocs
---

> __Objectifs__ :  
> 
> - Découvrir MkDocs
> - Utiliser un environnement virtuel python
> - Soigner la syntaxe YAML
> - Apprendre quelques "nouvelles" commandes git utiles
> - Découvrir l'intégration continue avec Gitlab

# MkDocs : qu'est-ce que c'est ?

> "MkDocs is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation. Documentation source files are written in Markdown, and configured with a single YAML configuration file."[^1]

Générer simplement un site statique à partir de fichiers écrits en markdown peut être une option intéressante si vous souhaitez mettre à disposition des éléments de cours/tp/ressources pour vos élèves. D'autres solutions existent (Hugo, Sphinx, JupyterBook) mais MkDocs est de plus en plus choisi par des collègues en NSI, ce qui permet de trouver des ressources, de l'aide, et tout un écosystème pédagogique plus facilement.

Quelques exemples(_non exhaustifs et non représentatifs_) après une rapide recherche sur un moteur généraliste :  

- https://fabricenativel.github.io/
- https://pgdg.frama.io/tnsi/
- https://trieles.gitlab.io/nsi/

Afin de découvrir cet outil nous utiliserons un framework associé __Material for MkDocs__[^2] : permettant l'ajout de nombreux plugins

# Pré-requis

En salle de TP du M5, afin d'avoir la main sur l'installation des bibliothéques Python, vous devez utiliser un environnement virtuel python à l'aide de [`virtualenv`]

> "virtualenv est un outil pour créer des environnements virtuels Python isolés. virtualenv crée un dossier qui contient tous les exécutables nécessaires pour utiliser les paquets qu’un projet Python pourrait nécessiter.."[^3]


Des explication succintes ici : <https://intranet.fil.univ-lille.fr/2020/08/12/utiliser-python-au-fil/>

Installez notamment :  

- mkdocs
- mkdocs-material

# Mon premier projet

## Initialisation

Dans votre environnement virtuel, créer un répertoire nommé _mkdocs_example_ que vous versionnerez grâce à git et initialisez-y votre premier projet mkdocs.

```bash
(pythonvenv)$ mkdir mkdocs_example
(pythonvenv)$ cd mkdocs_example
(pythonvenv)$ git init --initial-branch=main
(pythonvenv)$ mkdocs new .
```

Une structure hiérarchique est créée (vous pouvez le vérifiez) contenant un répertoire `docs` qui contiendra principalement vos fichiers au format _markdown_ et un fichier `mkdocs.yml` configurant votre projet/

```bash
.
├─ docs/
│  └─ index.md
└─ mkdocs.yml
```

## Material for MkDocs

L'utilisation du framework _Material for MkDocs_ cité précédemment doit être configuré dans votre projet, modifiez `mkdocs.yml`

```yaml
site_name: example MkDocs (à éditer)
theme:
  name: material
```

_Remarques_ :  

- attention à la syntaxe YAML les noms des champs doivent être suivis d'un `:` sans espace le précédent.
- Les caractères des _tabulation_ ne sont pas autorisés afin de maintenir la portabilité. Préférez donc des espaces pour réalisez des indentations qui servent à définir la hiérarchie de données.
- Les commentaires peuvent être ajoutés avec `#`

## Générer la documentation à partir de la source

1) __1ère option__ : à chaque fois que vous le souhaitez, vous générez un site (page html, répêrtoires et fichiers associés) dans un nouveau répertoire nommé `site` 

```bash
$ mkdocs build
```

```
.
├── docs
└── site
    ├── index.html
    ├── css
    ├── fonts
    ├── img
    ├── js
    └── search
```

Le fichier `index.html` est la page d'accueil de votre site ainsi généré : __ouvrez-la avec un navigateur__.

2) __2ème option__  : utiliser un serveur de développement pendant la conception afin de bénéficier du `live-reloading`

```bash
$ mkdocs serve
```

Dans ce cas là le site est généré/raffraîchi à la volée, vous pouvez donc modifiez des pages dans votre éditeur favori et __visualisez en même temps le résultat en local__ <http://127.0.0.1:8000/>

_Remarque_ : Il faudra ultérieurement générer avec `build` votre documentation si vous souhaitez la garder après la fermeture du serveur local.

__Essayez quelques modifications, un ajout de page, etc..__

# Deploiement avec GitLabPages

Le site généré avec _Material for MkDocs_ est conçu pour être Responsive Design. Si vous souhaitez le mettre à disposition de vos élèves simplement, vous pouvez utiliser une forge gitlab et profiter de l'hébergement de votre site gratuitement.  
L'intégration continue proposée par la forge vous permettra de mettre en place un processus de genration et déploiement automatique à chaque envoi (`push`) sur votre dépôt distant.

## Définitions de tâches pour l'intégration continue

Ajoutez un fichier `.gitlab-ci.yml` à la racine de votre dépôt et copier/coller le contenu suivant

```yaml
image: python:latest
pages:
  stage: deploy
  script:
    - pip install mkdocs-material
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
```

Ce fichier de configuration permet de définir l'architecture de votre pipeline de déploiement :  

- image Docker utilisée
- tâches (_jobs_) à éxécuter

__Créer un dépôt vide(sans Readme) sur le gitlab étudiant du FIL (la visibilité peut être privée) puis associez-le à votre dépôt versionné local et effectuez votre premier envoi__

```bash
...:~/mkdocs_example$ git remote add origin git@gitlab-etu.fil.univ-lille.fr:<namespace>/<projectname>.git
...:~/mkdocs_example$ git add .
...:~/mkdocs_example$ git commit -m "Initial commit" # lisez la suite avant de faire un push
```

La commande `remote` permet de créer une connection nommée _origin_ pointant sur le répertoire cloné distant 

> _remarque_ : si éventuellement vous vous êtes trompés d'URL pour votre dépôt vous pouvez toujours la modifier à posteriori
>  
> ```bash
> git remote rm origin # puis nouveau remote add
> ```

Le répertoire `site` n'a pas vocation à être suivi :  supprimer-le puis modifiez votre validation précédente

```bash
git commit --amend -m "suppression de site/"
```

Vous pouvez maintenant envoyez vos modifications

```bash
git push --set-upstream origin main
```

_Remarque_ : il sera important par la suite d'ajouter un fichier `.gitignore` pour éviter ce genre de mésaventure.

## Mais où est mon site ?

> if you set up a GitLab Pages project on GitLab, it’s automatically accessible under a subdomain of namespace.example.io. The namespace is defined by your username on GitLab.com, or the group name you created this project under. For GitLab self-managed instances, replace example.io with your instance’s Pages domain. For GitLab.com, Pages domains are *.gitlab.io.
> 
> _source_ : https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html

Chaque forge indique le nommage utilisé pour l'hébergement statique proposé.  
Dans le cas du gitlab étudiant, vous pouvez retrouver le nom de domaine dans le menu `Déploiement/Pages`, il est de la forme `http://<namespace>.gitlab-etu.fil.univ-lille.fr/<site_name>`.  
__Il est conseillé de l'indiquer dans le Readme pour ne pas l'oublier.__

Si le déploiement s'est correctement déroulé, le symbole ✅ sera présent à côté du nom du dernier commit. Sinon vous pouvez consulter le menu `Compilation/Jobs` pour comprendre d'où vient le problème.

# Organiser l'affichage des documents lors de la navigation

Le plugin `mkdocs-awesome-pages-plugin` vous permet de contrôler les noms des ressources s'affichant dans votre barre de navigation ainsi que l'ordre dans lequel elles s'affichent.

La documentation vous fournit l'essentiel de ce qu'il faut savoir : <https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin>

> __Attention__ pour utiliser `awesome-pages` , vous devez installer le paquet  via pip.
> Afin de ne pas devoir indiquer par une nouvelle instruction dans le fichier `.gitlab-ci.yml`
> chaque ajout, une bonne pratique consite à créer un fichier texte `requirements.txt` contenant les noms de toutes les biliothéques python nécessaires au déploiement.

__Créez-le un fichier `requirements.txt` à la racine de votre projet et éditez-le comme ci-dessous (il faudra bien évidemment aussi installer ces paquets en local dans votre environnement virtuel) :__

```txt
mkdocs-material==8.5.3
mkdocs-macros-plugin==0.7.0
mkdocs-awesome-pages-plugin==2.8.0
mkdocs-material-extensions==1.0.3
```

Dans `.gitlab-ci.yml`, ajoutez au champ script

```yaml
pip install -r requirements.txt
```

__Essyez de créer plusieurs répertoires/documents et utilisez des fichiers `.pages` pour contrôler les affichages.__

_Remarque:_ un tutoriel est disponible [ici](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/09_pages/pages/). Attention cependant, l'emploi du champ ~~`arrange`~~ est déprécié, préférez `nav`.

# Enrichissez vos supports

## Extensions

- Les [admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/) permettent d'intégrer des contenus secondaires visuellement mis en valeur

- Le package `mkdocs-material-extensions` utilisé dans le fichier _requirements.txt_ ajoute une collection d'extensions très intéressantes : intégration de diagrammes Mermaid, de formules Latex...

Un exemple de fichier de configuration vous est fourni avec [`mkdocs_example.yml`](./mkdocs_modele.yml). 

__Essayez d'explorer les différents champs et comprendre leur utilité en vous référant notamment à <https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/>__

## Themes et plugins

Une liste de thèmes, plugins vous est fournie sur <https://github.com/mkdocs/catalog> 

Vous pouvez aussi intégrer des iframe ou d'autres applications via des hook, quelques exemples :

- intégrer une console python avec Basthon : <https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/bibs_graphiques/bib_images/>
- utilisation de Logic pour utiliser des portes logiques <https://romainjanvier.forge.aeif.fr/nsipremiere/5_architecture_OS/1_circuits_logiques/1_nand/>
- intégrer une console sql ! <https://epithumia.github.io/mkdocs-sqlite-console/>

[^1]: https://www.mkdocs.org/
[^2]: https://squidfunk.github.io/mkdocs-material/getting-started/
[^3]: https://python-guide-pt-br.readthedocs.io/fr/latest/dev/virtualenvs.html