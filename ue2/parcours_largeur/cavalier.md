
# Le déplacement du cavalier

(_très fortement inspiré de Prépabac Tle NSI Hatier_)

Sur un jeu d'échec (plateau de taille 8 x 8) le mouvement d'un cavalier se décompose en :

- un déplacement de 2 cases dans une direction (_verticale ou horizontale_)
- puis  1 case dans l'autre direction comme illustré ci-desous

On réprésente ci-après deux situations où sont visibles la position initiale d'un cavalier et les positions atteignables après un mouvement.

![situation 1](./fig/knight-1.png) ![situation 2](./fig/knight-2.png)

__A partir d'une position initiale d'un cavalier sur le plateau, on souhaite connaître à quelles distances se trouvent les autres cases__ (_autrement dit : combien de mouvements du cavalier sont nécessaires pour atteindre chacune des autres cases ?_)

L'objectif est de produire une image comme celle présentée ci-après :

![situation 1](./fig/distances.png)

## Modélisation

__Q1)__ Proposer une modélisation d'une classe représentant le problème présenté.  
Expliquez vos choix (notamment le constructeur, les types des attributs et les méthodes utiles à la résolution de ce problème)

__Q2)__
__a-__ Rappelez sous forme de pseudo-code le principe du parcours en largeur d'abord dans un graphe.  
__b-__ Comment le modifier afin qu'il nous fournisse les distances ? Vous préciserez notamment les valeurs initiales et la manière de les modifier.

## Calcul des distances

__Q3)__ Implanter la classe modélisée (sans ce soucier pour l'instant d'une quelconque représentation textuelle ou graphique).  
Vous utiliserez le module [`queue`](https://docs.python.org/fr/3/library/queue.html) pour instancier une file.

__Q4)__ Expliquez brièvement comment vous auriez pu utiliser une liste Python pour obtenir le même comportement qu'une file.

## Visualisation des distances avec matplotlib

La bibliothéque [matplotlib](https://matplotlib.org/stable/tutorials/introductory/quick_start.html) permet de visualiser des données sous forme de grahiques.
La documentation est dense mais vous pouvez vous limiter aux instructions suivantes :

* import du sous-module `pyplot`

```python
import matplotlib.pyplot as plt
```

* création d'une nouvelle figure et attribution d'un titre

```python
plt.figure(<nom optionnel>)
plt.tile(<titre>)
```

* création d'une segment entre les points de coordonnées (x0, x1) et (y0, y1)

```python
plt.plot([x0,x1], [y0,y1], **kwargs)
```

Pour plus d'informations sur les paramètres optionnels comme les couleurs voir [ici](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html)

* création d'une zone texte aux coordonnées (x,y)

```python
plt.text(x,y,<texte>, **kwargs)
```

Pour plus d'informations sur les paramètres optionnels comme la taille de la police, les alignements... voir [ici](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.text)

* Affichage de la figure

```python
plt.show()
```

__Q5)__ Implantez une méthode `display(self)` produisant une image (telle que présentée précedemment) des distances à la case de départ du cavalier dans l'échiquier.