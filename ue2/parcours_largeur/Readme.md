---
title:  Parcours en largeur et calcul de distances dans un graphe
---

L'algorithme de parcours en largeur d'abord permet de calculer les distances de tous les noeuds à un noeud quelconque d'un graphe.  
Nous allons illustrer cette caractéristique à travers plusieurs exemples.

* [Problème du cavalier](./cavalier.md)
* Plus court chemin à origine unique : 
  * [algorithme de Dijsktra](./dijkstra.md)
  * [algorithme de Bellman-Ford](./bellman.md)
* Plus court chemin entre toutes paires de sommets:
  * [algorithme de Floyd-Warshall](./floyd.md)