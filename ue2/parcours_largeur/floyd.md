
# Algorithme de Floyd-Warshall

On s'intéresse à la recherche des plus courts chemins entre tous les couples de sommets d'un graphe.

__Q1__ Pourrait-on résoudre le problème avec l'algorithme de Dijkstra ?

__Q2__ L'algorithme de Floyd-Warshall est valable pour des graphes pouvant contenir des arcs de poids négatifs mais pas de circuit absorbant (_circuit de poids strictement négatif_)

On dispose en entrée :

- d'un graphe
- d'une fonction de pondération $w$

On notera $\{1,2,\cdots,n\}$ les $n$ sommets du graphe.

Le principe est le suivant (_ref : [Froidevaux Gaudel Soria](https://www.lri.fr/~chris/LivreAlgorithmique/FroidevauxGaudelSoria.pdf)_) :

> on considère successivement les chemins de i vers j qui ne passent d'abord par aucun autre sommet, puis qui passent éventuellement par le sommet 1, puis par les sommets 1 et 2, etc.    
> A l'étape $k$, on calcule le coût d'un plus court chemin de $i$ à $j$ passant par des sommets inférieurs ou égaux à $k$

L'algorithme de Floyd-Warshall peut s'écrire ainsi :

> __POUR__ $k$ allant de $1$ à $n$ :   
> &nbsp;&nbsp;&nbsp;&nbsp;__POUR__ $j$ allant de $1$ à $n$ :   
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__POUR__ $i$ allant de $1$ à $n$ :   
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__SI__ $d_{ik}+d_{kj} \lt d_{ij}$:  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$d_{ij}=d_{ik} + d_{kj}$  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$\pi_{ij}=\pi_{kj}$  
> __SI__ $`\nexists i | d_{ii}<0`$:  
> &nbsp;&nbsp;&nbsp;&nbsp;__RENVOYER__ $d$ et $\pi$ 

_Remarques:_

- La matrice $d$ des __plus courts chemins__ est initialisée ainsi

$`d_{i,j}=
\left\{
\begin{array}{ll}
0 & \text{si}\  i=j \\
w_{i,j}  & \text{sinon}
\end{array}
\right.`$

elle contiendra à chaque étape les coûts des plus courts chemins calculés

- La matrice $\pi$ corresponds aux __prédecesseurs__ de chaque sommet dans les plus courts chemins.

__Q3-__ Consulter l'[animation proposée ici](https://algorithms.discrete.ma.tum.de/graph-algorithms/spp-floyd-warshall/index_en.html) puis résolvez un exercice en ligne proposé afin de vous approprier l'algorithme

__Q4-__ Pourquoi cet algorithme est qualifié d'algorithme de programmation dynamique ?

__Q5-__  
__a-__ Quelles sont les complexités de cet algorithme ?  
__b-__ Pour quels graphes Floyd-Warshall peut être un meilleur choix que Dijkstra ?  
__c-__ Citer une application possible.  

__Q6-__ Expliquez comment initialiser la matrice des prédécesseurs.

__Q7-__ __Implémentez en Python cet algorithme.__
A chaque itération $k$ vous devrez produire une représentation textuelle de la matrice des plus courts chemins et de la matrice des prédecesseurs (pour les plus courts chemins).
Vous pouvez par exemple générer un fichier texte pour sauvegardez ces étapes (_réutilisez les scripts précédents en modularisant ce qui peut l'être_)