
# Algorithme de Bellman-Ford

L'algorithme de Bellman-Ford permet de proposer un plus court-chemin dans un graphe en partant d'une origine unique dans un cas plus général où les poids des arcs peuvent avoir des valeurs négatives.

Etant donné un graphe orienté pondéré $G=(V,E)$ de fonction de poids $w$ et un sommet origine $r$, l'algorithme proposé renvoie, pour chaque sommet $u$ de $G$ un chemin de poids minimal de $r$ vers $u$  s'il n'existe pas de poids négatif. Il renvoie donc aussi une valeur booléenne traduisant la présence ou non d'un tel circuit.

__Q1-__ Qu'est-ce qu'un cycle absorbant ?

__Q2-__ Etudier la video https://www.youtube.com/watch?v=Pn7rCyqwZbQ

et appliquer l'algorithme de Bellman-Ford au graphe suivant en partant du sommet $E$

![Exemple](fig/bellman.png)

_Remarque_ : _Attention il y a 2 arcs reliant les sommets $A$ et $B$_

__Q3-__ Même question en changeant le poids de l'arc $(D,B)$ à $4$

__Q4-__ Quelles structures de données sont nécessaires lors de l'initialisation de cet algorithme et quelles valeurs sont associées?

__Q5-__  Ecrivez la fonction `bellman_ford` répondant aux spécifications de l'algorithme décrit précédemment.

__Q6-__  Quelle est la complexité de cet algorithme ?