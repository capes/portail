
# Algorithme de Dijkstra

_E.W.Dijstra_(1930-2002) informaticien néerlandais est à l'origine de l'algorithme du même nom permettant de calculer toutes les distances entre un sommet de départ et tous les autres sommets d'un graphe pondéré.

Le principe est expliqué [ici](http://math.univ-lyon1.fr/irem/Formation_ISN/formation_parcours_graphes/dijkstra/1_algorithme.html)

## Etude débranchée

Après étude des ressources précédentes essayez de retrouver la trace d'exécution correspondant au graphe suivant

![graph 1](./fig/graph1.png)

_trace d'exécution de l'algorithme exécuté à partir d'un sommet (ici A)_

```
 etapes n°   sommet traité   dA      dB      dC      dD      dE      dF   
-------------------------------------------------------------------------
     0                       0(A)   inf()   inf()   inf()   inf()   inf()  
     1             A         0(A)    7(A)    9(A)   inf()   inf()   14(A)  
     2             B         0(A)    7(A)    9(A)   22(B)   inf()   14(A)  
     3             C         0(A)    7(A)    9(A)   20(C)   inf()   11(C)  
     4             F         0(A)    7(A)    9(A)   20(C)   20(F)   11(C)  
     5             D         0(A)    7(A)    9(A)   20(C)   20(F)   11(C)  
     6             E         0(A)    7(A)    9(A)   20(C)   20(F)   11(C) 
```

## Implantation

__Objectif__ : produire un exécutable python acceptant utilisant un fichier csv représentant un graphe pondéré et un sommet de départ. 

```shell
$ ./script.py graph.csv A
```
Cet exécutable devra générer __2 fichiers__ :

* `graph.png` : une image du graphe correspondante
* `graph.txt` : une trace d'exécution de l'algorithme de Dijkstra telle que présentée juste avant

On fournit un exemple de fichier csv représentant le graphe 1 : [graph1.csv](./graph1.csv)

Vous produirez également un deuxième fichier pour cet autre graphe

![graph 2](./fig/graph2.png)

__Contraintes__

Vous devrez utiliser :

* le module [`csv`](https://docs.python.org/fr/3/library/csv.html) pour générer une liste d'adjacence correspondant à un graphe fourni sous forme de fichier csv
* le module [`NetworkX`](https://networkx.org/) pour générer l'image d'un graphe à partir d'une liste d'adjacence

_Conseils:_ réfléchissez préalablement aux structures de données à utiliser, au paradigme de programmation, à la modularité de votre programme