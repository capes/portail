---
title:  Markdown et pandoc pour la création de documents
---

Markdown
==========

Syntaxe
-------

Le **markdown** est un **langage de balisage léger** permettant de formater des documents textes avec une syntaxe réduite afin de faciliter sa lecture.

Ce langage n'a jamais été totalement standardisé mais vous pourrez trouver de nombreuses ressources permettant de vous familiariser avec la syntaxe :  

* un document réalisé par le FIL [ici](./syntaxe_markdown.md)
* ou [pour aller plus loin](https://www.markdownguide.org/)

Un sujet en markdown
--------------------

Voici un petit exercice de syntaxe markdown : 
Vous devez reproduire le plus fidèlement possible ce document :

[Mon beau document](./fibonacci_final.pdf)

On vous fournit pour ce faire les ressources suivantes :

* [le texte non formaté](./fibonacci_source.txt)
* [l'image d'illustration](./fibonacci.png)

et les outils décrits dans les sections ci-après...

__1)__ Pour commencer votre travail vous produirez tout d'abord un fichier markdowm (extension `md`) à partir du texte non formaté.
De nombreux IDE comme Codium (_ou VS Code_) propose des prévisualisations automatiquse du rendu d'un fichier dans ce format. Ne vous souciez pas pour l'instant :

* de la légende (et de la numérotation des figures)
* des couleurs
* de la numérotation des titres
* de la table des matières
* du rendu des expressions mathématiques
* de la mise en page (alignements, etc.)

__2)__  Les documents seront à rendre via le [__gitlab étudiant de l'intranet__](https://gitlab-etu.fil.univ-lille.fr).
Créer un dépôt `meef_<votre_nom>` (ou utiliser un dépôt déjà créé) il servira à toutes les remises de travaux à venir.
Pensez à rajouter vos enseignants avec le rôle de `Developper`.  

Y ajouter un répertoire `fibonacci_markdown` qui contiendra votre fichier formaté et l'image associée.  
Visualisez le rendu sur la plateforme.  

Quelques ressources pour vous aider : 

* [Document d'introduction au GitLab étudiant du FIL](https://www.fil.univ-lille.fr/~routier/enseignement/licence/poo/tdtp/gitlab.pdf)
* [Video d'introduction reprendant les étapes décrites dans le document précédent](https://www.fil.univ-lille.fr/~routier/enseignement/licence/poo/cours/videos/demo-git.m4v)
* [Installation VPN Etudiant (pour un accés extérieur)](https://sciences-technologies.univ-lille.fr/informatique/stock-pages-speciales/documentation-vpn)

Des outils complémentaires : formules mathématiques en LaTeX
----------------------------------------------------------------

Il est possible d'insérer des expressions mathématiques en ligne dans un document markdown en utilisant la syntaxe LateX comme par exemple $\sum_{i=1}^n a_i$.

Ces expressions seront délimitées par un `$`, ainsi l'expression précédente s'écrira :

```
$\sum_{i=1}^n a_i$
```

Quelques éléments de **syntaxe LaTeX utiles pour les mathématiques** [ici](https://en.wikibooks.org/wiki/LaTeX/Mathematics)

__3)__  Mettez en forme les expressions mathématiques dans votre document et visualisez le rendu sur gitlab.


Convertir son document avec pandoc
==================================

Un seul format pour les gouverner tous
---------------------------------------

Si vous souhaitez convertir votre document dans un autre format (comme `pdf` par exemple pour distribuer une version papier) : vous pouvez utiliser [`pandoc`](https://pandoc.org/). Il s'agit d'un programme permettant de convertir des fichiers texte en d'autres formats avec la syntaxe générale :

```shell
pandoc [options] [input-file]
```

Par exemple à partir d'un fichier au format markdown :

* Pour obtenir un fichier au format `.org` :

```shell
pandoc -s -o fibonacci.org fibonacci.md
```

* Au format `.tex` :

```shell
pandoc -s -o fibonacci.tex fibonacci.md 
```

Le document LaTeX peut ensuite être compilé en `.pdf`, mais on peut aussi le faire directement ainsi :

```shell
pandoc -s -o fibonacci.pdf fibonacci.md 
```

__4)__ Générez le pdf via cette méthode.
Que manque-t-il encore à votre document pour qu'il se rapproche le plus possible du rendu final présenté précédemment ?  

Metadonnées et YAML
-------------------

Le `YAML`(_Yet Another Markup Language_) est un format de représentation de données comme le sont `JSON` ou `CSV`.  
Il utilise la syntaxe `clé: valeur` (un espace après la clé).

On peut placer au début du fichier mardown un bloc YAML (délimité par `---`)

Par exemple :

```yaml
---
title: REPRESENTATION DES ENTIERS
subtitle: Bases numériques
author: Département Informatique, Université de Lille
fontsize: 12pt
geometry: margin=3cm
---
```

Lors de la conversion en pdf, les métadonnées YAML seront utilisées pour afficher automatiquement les métainformations en début de document. Les instructions de mise en page si elles sont présentes seront également appliquées. Enfin, les annotations seront présentes en bas de chaque page ainsi qu'une numérotation automatique de ces dernières.

Ce bloc contient à la fois des métadonnées sur le document (son titre, sous-titre, auteur) mais aussi des instructions à appiquer sur sa mise en page.  
Toutes ces données pourront être utilisées par exemple lors de la conversion du document dans un autre format comme nous le verrons avec la génération d'un pdf.

Quelques ressources à utiliser :

* Quelques champs YAML [ici](https://cran.r-project.org/web/packages/ymlthis/vignettes/yaml-fieldguide.html) : vous regarderez notamment les sections _Basic YAML_ et _LateX/PDF Options_.  
* Quelques compléments [là](https://cran.r-project.org/web/packages/ymlthis/vignettes/yaml-overview.html)

__5)__ mettre en place une entête yaml adéquate pour vous rapprocher le plus possible du résultat attendu.

L'entête YAML peut devenir vite surchargée par des informations qui sont de la pure mise en forme (marges, couleurs, toc, etc..).
On peut passer quelques champs yaml plutôt en option de la commande `pandoc` avec le flag `-V` (variables)
Par exemple :

```shell
pandoc -V fontsize:"12pt" -s -o fibonacci.pdf fibonacci.md 
```

Quelques ressources à ce sujet :

* https://pandoc.org/MANUAL.html#variables-for-context
* https://jdhao.github.io/2019/05/30/markdown2pdf_pandoc/

__6)__ Proposer une commande qui répond à ce besoin et garder le minimum dans l'entête yaml

Makefile
---------

__7)__ Afin d'optimiser la génération des documents précédents proposer un Makefile adapté.  
Quelques ressources pour vous rappeler leur conception :

* https://makefiletutorial.com/
* https://riptutorial.com/Download/makefile-fr.pdf

_Conseils_ :

* utiliser des variables pour regroupes les options de pandoc

Rendu du travail
================

Sur le dépot git précédemment créé, dans le répertoire `fibonacci_markdown` vous devrez placer:  

* le fichier `fibonnaci.md`
* l'image associée
* un Makefile permettant de générer le pdf correspondant

**Veillez bien à respecter le plus fidèlement le rendu final proposé**
