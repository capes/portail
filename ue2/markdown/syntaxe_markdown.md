Markdown zéro
=============

> La première partie, « styles de texte » est une adaptation de [_La syntaxe markdown_](https://framagit.org/framasoft/docs/blob/master/fr/grav/markdown.md) du _Projet de docs centralisées_ [docs.framasoft.org/fr/](https://docs.framasoft.org/fr/) de Framasoft.
>
> Principalement
> * suppression de spécificités de Grav le moteur de Framasite
>   [frama.site/](https://frama.site/), service libre de création de sites web
> * ajout de précisions pour la présentation de code

Au menu :
* styles de texte
* interpréter ou traduire le Markdown

Styles de texte
===============

Vous pouvez utiliser `_` ou `*` autour d'un mot pour le mettre en italique. Mettez-en deux pour le mettre en gras.
*  `_italique_` s'affiche ainsi : _italique_
*  `**gras**` s'affiche ainsi : **gras**
* `**_gras-italique_**` s'affiche ainsi : **_gras-italique_**
* `~~barré~~` s'affiche ainsi : ~~barré~~

Entourez les éléments de code d'une backquote (accent grave). Ainsi

    ```
    Des extraits de `code` en ligne sont `entourés de backquote` !
    ```

s'affiche

```
Des extraits de `code` en ligne sont `entourés de backquote` !
```

Blocs de code
-------------

Créez un bloc de code en indentant chaque ligne avec quatre espaces, ou en mettant trois accents graves sur la ligne au dessus et en dessous de votre code.
Exemple :

    ```
    bloc de code
    ```

s'affiche ainsi :

```
bloc de code
```

Précisez le langage pour profiter de la prise en charge la coloration syntaxique :

    ```python
    def next(i, s) :
        j = i+1
        t = s + 'un'
        return j, t 
    ```

s'affiche

```python
def next(i, s) :
    j = i+1
    t = s + 'un'
    return j, t 
```

    ```html
    <html>
      <head><title>Title!</title></head>
      <body>
        <p id="foo">Hello, World!</p>
        <script type="text/javascript">var a = 1;</script>
        <style type="text/css">#foo { font-weight: bold; }</style>
      </body>
    </html>
    ```

s'affiche

```html
<html>
  <head><title>Title!</title></head>
  <body>
    <p id="foo">Hello, World!</p>
    <script type="text/javascript">var a = 1;</script>
    <style type="text/css">#foo { font-weight: bold; }</style>
  </body>
</html>
```

_GitLab utilise la biblithèque [Rouge Ruby](http://rouge.jneen.net/) pour cette coloration syntaxique. Consultez la liste des langages supportées sur le site de Rouge._


Liens
-----

Créez un lien intégré en mettant le texte désiré entre crochets et le lien associé entre parenthèses.

```
Consultez [la page vitrine du MEEF de Lille](https://gitlab-fil.univ-lille.fr/capes/portail/-/blob/master/Readme.md) !
```

s'affichera :

Consultez [la page vitrine du MEEF de Lille](https://gitlab-fil.univ-lille.fr/capes/portail/-/blob/master/Readme.md) !

Plus simplement, écrivez `https://gitlab-fil.univ-lille.fr/capes/portail/-/blob/master/Readme.md` pour afficher https://gitlab-fil.univ-lille.fr/capes/portail/-/blob/master/Readme.md

Utilisez des chemins relatifs ou abolus pour crer des liens vers les fichiers locaux :

```
Consultez [la page d'accueil de l'ue1](../../ue1/README.md).
```

s'affichera

Consultez [la page d'accueil de l'ue1](../../../ue1/README.md).

Images
------

Affichez une image en ligne en copiant son adresse (finissant par `.jpg`, `.png`, `.gif` etc.) avec un texte alternatif entre crochets (qui sera affiché si l'image n'apparaît pas).

```
![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
```

donnera :

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

Utilisez simplement un chemin (absolu, ou relatif) pour désigner une image locale :

```
![Université de Lille ](./fig/ulille-logo-100.jpg)
```

donnera

![Université de Lille ](./fig/ulille-logo-100.jpg)


Citations
---------

Les citations se font avec le signe `>` :

```
> Oh la belle prise !
```

> Oh la belle prise !

Et sur plusieurs lignes :

```
> L'optimisation prématurée est la racine de tous les maux (ou, du moins, la plupart d'entre eux) en programmation.  
> _(en) Premature optimization is the root of all evil (or at least most of it) in programming.  
> Donald Knuth (trad. Wikiquote), Decembre 1974, Conférence du Prix Turing 1974, dans Communications of the ACM.
```

> L'optimisation prématurée est la racine de tous les maux (ou, du moins, la plupart d'entre eux) en programmation.  
> _(en) Premature optimization is the root of all evil (or at least most of it) in programming._  
> Donald Knuth (trad. Wikiquote), Decembre 1974, Conférence du Prix Turing 1974, dans Communications of the ACM.

Listes
------

Vous pouvez créer des listes avec les caractères `*` et `-` pour des listes non ordonnées ou avec des nombres pour des listes ordonnées.

Une liste non ordonnée :

```
* un élément
* un autre
  * un sous élément
  * un autre sous élément
* un dernier élément
```

* un élément
* un autre
  * un sous élément
  * un autre sous élément
* un dernier élément

Une liste ordonnée :

```
1. élément un
2. élément deux
```

1. élément un
2. élément deux

La valeur des nombres importe peu :

```
1. élément un
1. élément deux
```

1. élément un
1. élément deux


Titres et sections
------------------

Soulignez le texte en utilisant le signe `=` pour créer un titre de section de niveau 1, en utilisant le signe `-` pour un tire de section de niveau 2. Le nombre de signe importe peu  :

```
Un titre du niveau le plus haut
===========

Un titre de niveau inférieur
-----------------------------
```

Un titre du niveau le plus haut
===========

Un titre de niveau inférieur
-----------------------------

Vous pouvez également précéder le texte d'un ou plusieurs `#` pour créer des titres 

```
# Un titre du niveau le plus haut 
## Un titre de niveau inférieur
### Un titre de niveau moindre
```

# Un titre du niveau le plus haut 
## Un titre de niveau inférieur
### Un titre de niveau moindre

etc. jusqu'à 6 niveaux.

Tableaux
--------

Pour créer un tableau vous devez placer une ligne de tirets (`-`) sous la ligne d'entête et séparer les colonnes avec des `|`. Vous pouvez aussi préciser l'alignement en utilisant des `:`. :

```
| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| ce texte         | ce texte        |  ce texte |
| est              | est             |  est |
| aligné à gauche  | centré  |  aligné à droite |
```

| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| ce texte         | ce texte        |  ce texte |
| est              | est             |  est |
| aligné à gauche  | centré  |  aligné à droite |

Remarquez que les `|` ne sont pas nécessairement alignés dans le texte source Markdown.

Interpréter ou traduire le Markdown
===================================

Le code source Markdown est un format texte simple lisible par tous. Il peut être transmis en tant que tel.

Les fichiers au format Markdown sont reconnus par les plateformes GitLab et autre GitHub qui en proposent une visualisation. \
Markdown s'impose alors comme le format de documentation des projets gérés par ces plateformes.

Les fichiers Markdown peuvent être traduits en HTML, PDF, EPUB, LaTeX, et bien d'autres formats, par exemple via Pandoc [pandoc.org/](https://pandoc.org/). \
Il est ainsi possible d'écrire le contenu d'un site web en Markdown, d'écrire des documents textes de toutes sortes en  Markdown, d'écrire des supports de présentation en Markdown, etc. 




