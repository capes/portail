UE2
====

EC1 : Didactique
-----------------

Préparation à la deuxième épreuve d'admissibilité (Écrit 2).

On s'entraine :

1.  en analysant les programmes -> connaissances
2.  en élaborant des documents (cours, TD, TP, PRJ, DS) -> didactique
3.  enfin en analysant des productions


Cette épreuve consiste en l'analyse et l'exploitation d'un ensemble de documents.  
Les questions posées indiqueront avec précision le travail attendu à partir de ces documents.
    
-   Évaluations : 
        
    -   capacités d'analyse, de synthèse et d'argumentation
    -   aptitude à mobiliser des savoirs discplinaires et didactiques dans une activité d'enseignement.
        
- Types de documents :
        
    -   extraits de manuels scolaires
    -   productions d'élèves
    -   sujets d'exercices, de TP, de projets, d'examens
    -   documentation de bibliothèques de langages de programmation
    -   extraits de publication scientifiques
    -   extraits de grande presse
    -   écrits abordant une question scientifique dans leur dimension sociétale
    
-   Exemples de questions pouvant être posées :
    
    -   Élaborer un barême d'un examen donné et justifier ce barême
    -   Élaborer un sujet d'examen afin d'évaluer certaines connaissances et/ou compétences pour un 
        niveau de classe donné.
    -   Corriger la copie d'un éléve donnée. Indiquer les connaissances/compétencs attendues qui ne 
        sont pas acquises par l'élève. Proposer une activité de remédiation.
    -   Proposer des activités pratiques sur un thème donné. Préciser la apprentissages liés à ces activités.
    -   Expliquer comment une utilisation d'activité pratique permet d'illustrer une notion.
    -   Compléter un document donné dans le but de le donner aux élèves avec un objectif fixé. Suggérer d'autres 
        documents qu'il serait intéressant de fournir aux élèves.
    -   Proposer l'utilisation d'un logiciel pour un problème donné. Justifier l'intérêt et les limites
        de l'utilisation de ce logiciel par les élèves.
    -   Analyser des productions d'élèves données.
    -   Proposer des éléments de reflexion à discuter en classe sur les enjeux d'un sujet donné sur la société


EC2 : Tice spécifique
---------------------

On verra bien : 

-   jupyter + extensions
-   LaTeX 
    -   pythontex
    -   beamer
-   markdown 
    -   pandoc
-   orgmode
-   


