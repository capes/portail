---
title: A2 - Programmation dynamique
author: Préparation CAPES - MEEF NSI
date: avril 2022
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr
---

L'exercice suivant est extrait du [sujet 0 de l'épreuve de Terminale NSI](https://eduscol.education.fr/media/3962/download)

_Cet exercice porte sur la programmation en général et la récursivité en particulier._

On considère un tableau de nombres de $`n`$ lignes et $`p`$ colonnes.

Les lignes sont numérotées de $`0`$ à $`n-1`$ et les colonnes sont numérotées de $`0`$ à $`p-1`$. La case en haut à
gauche est repérée par $`(0,0)`$ et la case en bas à droite par $`(n-1,p-1)`$.

On appelle __chemin__ une succession de cases allant de la case $`(0,0)`$ à la case $`(n-1,p-1)`$, en n’autorisant que
des déplacements case par case : soit vers la droite, soit vers le bas.

On appelle __somme d’un chemin__ la somme des entiers situés sur ce chemin.

Par exemple, pour le tableau T suivant :

$`\begin{array}{|c|c|c|c|c} \hline \textbf4 & \textbf1 & \textbf1 & 3 \\ \hline 2 & 0 & \textbf2 & 1 \\ \hline 3 & 1 & \textbf5 & \textbf1 \\ \hline \end{array}`$   


— Un chemin est __$`(0,0), (0,1), (0,2), (1,2), (2,2), (2,3)`$__ (en gras sur le tableau) ;  
— La somme du chemin précédent est 14.  
— $`(0,0), (0,2), (2,2), (2,3)`$ n’est pas un chemin.

L’objectif de cet exercice est de déterminer la somme maximale pour tous les chemins possibles allant de la case $`(0,0)`$
à la case $`(n-1,p-1)`$.

**Question 1** 

On considère tous les chemins allant de la case $`(0,0)`$ à la case $`(2,3)`$ du tableau T donné en exemple.

1. Un tel chemin comprend nécessairement 3 déplacements vers la droite. Combien de déplacements vers le bas comprend-il?
2. La longueur d’un chemin est égal au nombre de cases de ce chemin. Justifier que tous les chemins allant de $`(0,0)`$ à $`(2,3)`$ ont une longueur égale à 6.

**Question 2** 

En listant tous les chemins possibles allant de $(0,0)$ à $(2,3)$ du tableau T, déterminer un chemin qui permet d’obtenir la somme maximale et la valeur de cette somme.

**Question 3** 

On veut créer le tableau `T’` où chaque élément `T’[i][j]` est la somme maximale pour tous les chemins possibles allant de $`(0,0)`$ à $`(i,j)`$.

1. Compléter et recopier sur votre copie le tableau T’ donné ci-dessous associé au tableau T.  

T=
$`\begin{array}{|c|c|c|c|c} \hline 4 & 1 & 1 & 3 \\ \hline 2 & 0 & 2 & 1 \\ \hline 3 & 1 & 5 & 1 \\ \hline \end{array}`$   


T'=
$`\begin{array}{|c|c|c|c|c} \hline 4 & 5 & 6 & ? \\ \hline 6 & ? & 8 & 10 \\ \hline 9 & 10 & ? & 16 \\ \hline \end{array}`$   

2. Justifier que si $j$ est différent de 0, alors : `T’[0][j] = T[0][j] + T’[0][j-1]`

**Question 4**

Justifier que si $`i`$ et $`j`$ sont différents de 0, alors : `T’[i][j] = T[i][j] + max(T’[i-1][j], T’[i][j-1]`

**Question 5**

On veut créer la fonction récursive `somme_max` ayant pour paramètres un tableau `T`, un entier `i` et un entier `j`. Cette fonction renvoie la somme maximale pour tous les chemins possibles allant de la case $(0,0)$ à la case $(i,j)$.

1. Quel est le cas de base, à savoir le cas qui est traité directement sans faire appel à la fonction `somme_max`? Que renvoie-t-on dans ce cas?

2. À l’aide de la question précédente, écrire en Python la fonction récursive `somme_max`.

3. Quel appel de fonction doit-on faire pour résoudre le problème initial? 


# Analyse de l'exercice proposé

1) Proposer une correction complète de cet exercice
   
2) Comment qualifieriez-vous la démarche de résolution qui est proposée ici ? Quel est son inconvénient ?
   
3) a- Quel critique pourriez-vous faire sur la question 2 ?    
   b- Ecrivez une fonction python permettant de calculer le nombre de chemins différents pour un tableau de $n$ lignes et $p$ colonnes permettant d'aller de la case $`(0,0)`$ à la case $`(n-1,p-1)`$  
   c- (_Complément_) Donnez une expression mathématique exprimant le nombre de chemins en fonction de $n$ et $p$
   
   
4) Quell(e)s questions vous paraissent superflues ou au contraire quell(e)s questions auriez vous ajouté ?

# Modifications de l'exercice

On cherche maintenant à proposer (à partir du même problème initial) d'autres approches de résolution pouvant être demandées à des élèves de Terminale (_voir extraits du programme en Annexe_) 

5) **Résolution gloutonne de ce problème.** 
    
   a- Quel choix doit être fait pour appliquer cette approche de résolution ?   
   b- Proposez une fonction `somme_max_glouton` permettant de fournir une solution à ce problème    
   c- Quelles questions de l'énoncé peut-on conserver ?    
   d- Rédiger les questions manquantes à destination d'un élève (_Vous tiendrez compte de la nature de l'épreuve écrite_).  
   e- Proposez une question finale avec un exemple bien choisi montrant les inconvénients de ce type de résolution.
 
6) **Résolution avec mémoïsation (top-down)**   
   
   a- Quel aspect ici de cette résolution doit être mis en avant ?  
   b- Proposez une fonction `somme_max_mem` permettant de fournir une solution à ce problème   
   c- Rédiger les questions manquantes à destination d'un élève.  
   d- Quelle données pourriez-vous fournir à un élève pour le convaincre que cette démarche est préférable ?

7) **Résolution bottom-up**

L'approche bottom-up consiste à résoudre d'abord les sous-problèmes de taille plus petite, ensuite les sous-problèmes de taille plus grande, en utilisant les sous-problèmes déjà résolus.  
En procédant de manière itérative, proposez une fonction `somme_max_bottom_up` implémentant cette approche.

# Extraits du Programme de Terminale

![recursivité](./fig/bo_recursivite.png)

![programmation dynamique](./fig/bo_progdyn.png)



