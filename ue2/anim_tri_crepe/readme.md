---
title: Animation HTML/CSS/JS tri crepes
---

# Objectif

Création d'une animation en HTML/CSS/JS illustrant le tri crêpes

# Etapes

## Etape 1 : codage des fonctions relatives au tri en javascript

Conseils :  

* Utilisez tout d'abord une variable globale pile initiale avec des entiers pour représenter la pile
* Créer les fonctions `tri`, `retourner`, `indice_plus_grande_crepe`
* Afficher dans la console la pile initiale puis la pile triée

##  Etape 2 : réalisation d'un page web support

* Ajouter des éléments html permettant de visualiser le contenu numérique des piles initiale  triée
* Mettre en place un formulaire avec notamment le choix du nombre de crêpes
* Générer des valeurs aléatoires représentant chacune des crêpes


## Etape 3 : CSS/JS

* Afficher d'une pile de crêpes sous forme de div correspondant aux nombres

## Etape 4 : JS

* Permettre le retournement des crêpes par click sur une crêpe ( en survolant chque crêpe on devra pouvoir l'identifier)


# Etape 5 : JS

* Compléter le formulaire afin de trier automatiquement la pile et ajouter toute amélioration souhaitée