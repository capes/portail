Calendrier M2
============

- [ ] 10/5 : rendu mémoire (moodle)


Semaine 19
----------

### Mercredi 10 mai ###

| Quand      | Où        | Quoi  | Qui        |
|------------|-----------|-------|------------|
| 8h30-11h45 | m3 Turing | ue a2 | P. Thibaud |

### Jeudi 11 mai ###

| Quand       | Où           | Quoi         | Qui        |
|-------------|--------------|--------------|------------|
| 13h00-14h30 | amphi Turing | ue a1 (oral) | B. Papegay |

Semaine 18
----------

### Mercredi 3 mai ###

| Quand      | Où     | Quoi  | Qui         |
|------------|--------|-------|-------------|
| 8h30-11h45 | m3 211 | ue b3 | M. Guichard |

### Jeudi 4 mai ###

| Quand       | Où           | Quoi         | Qui        |
|-------------|--------------|--------------|------------|
| 13h00-14h30 | amphi Turing | ue a1 (oral) | B. Papegay |


Semaines 16 & 17
----------------

Interruption pégagogique


Semaine 15
---------


### Mercredi 12 avril ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud |
| 13h00-14h30 | sup 116   | ue c  | B. Papegay |
| 14h45-16h15 | sup 117   | ue c  | B. Papegay |


### Jeudi 13 avril ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

Semaine 14
---------

### Lundi 3 avril ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue a2 | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c  | B. Papegay |

### Mercredi 5 avril ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |

### Jeudi 6 avril ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a11    | ue a1 (tp)   | B. Papegay |

Semaine 13
---------

### Lundi 27 mars ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue a2 | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c  | B. Papegay |

### Mercredi 29 mars ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | sup 209   | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |

### Jeudi 30 mars ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |

Semaine 10
---------

### Lundi 6 mars ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue a2 | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c  | B. Papegay |
    

### Mardi 7 mars ###

stage ou mémoire

### Mercredi 8 mars ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | sup 108   | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup 116   | ue a1 | B. Papegay  |

### Jeudi 9 mars ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | B. Papegay |


### Vendredi 10 mars ###

*stage ou mémoire*

Semaine 9
---------

### Lundi 27 février ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue c  | B. Papegay |
| 14h45-16h15 | sup 116 | ue a1 | B. Papegay |

- a1 : sujet "tas binomial" sur machine.

### Mardi 28 février ###

stage ou mémoire

### Mercredi 1 mars ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | sup 108   | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup 116   | ue a1 | B. Papegay  |

### Jeudi 2 mars ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a4     | ue a1 (tp)   | P. Thibaud |

- tp [algos gloutons](./ue2/glouton/td_glouton.md) n - coloriage

### Vendredi 3 mars ###

*stage ou mémoire*


Semaine 08
----------

*interruption pédagogique*

Semaine 07
----------

### Lundi 13 février ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a1/a2 | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c     | B. Papegay |


### Mardi 14 février ###

stage ou mémoire

### Mercredi 15 février ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup 116   | ue a1 | B. Papegay  |

### Jeudi 16 février ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a11    | ue a1 (tp)   | P. Thibaud |

- [Hachage et collision](./ue1/hachage-crypto/hachage-collision.md)

### Vendredi 17 février ###

*stage ou mémoire*

Semaine 06
----------

### Lundi 6 février ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue c  | B. Papegay |
| 14h45-16h15 | sup 116 | ue a1 | B. Papegay |

- *tp* : [JS ES6 : Arrray utilisation de fonctionnelles](./ue2/js_es6/fonctionnelles/readme.md)

### Mardi 7 février ###

stage ou mémoire

### Mercredi 8 février ###

| Quand      | Où          | Quoi  | Qui        |
|------------|-------------|-------|------------|
| 8h00-13h00 | m1 Painlevé | ds a2 | P. Thibaud |


### Jeudi 9 février ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a11    | ue a1 (tp)   | P. Thibaud |


### Vendredi 10 février ###

*stage ou mémoire*

Semaine 05
----------

### Lundi 30 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue c  | B. Papegay |
| 14h45-16h15 | sup 116 | ue a1 | B. Papegay |

### Mardi 31 janvier ###

stage ou mémoire

### Mercredi 1 février ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | sup 108   | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup 116   | ue a1 | B. Papegay  |


### Jeudi 2 février ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5 a11    | ue a1 (tp)   | P. Thibaud |


### Vendredi 3 février ###

*stage ou mémoire*

Semaine 04
----------

### Lundi 23 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue c  | B. Papegay |
| 14h45-16h15 | sup 116 | ue a1 | B. Papegay |

### Mardi 24 janvier ###

stage ou mémoire

### Mercredi 25 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | sup 108   | ue b3 | M. Guichard |
| 10h15-11h45 | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup 116   | ue a1 | B. Papegay  |

- [securisation](./ue1/securisation/)

### Jeudi 26 janvier ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h15 | m3 Turing | ue a1 (oral) | B. Papegay |
| 14h30-16h30 | m5 a4     | ue a1 (tp)   | B. Papegay |

- [securisation](./ue1/securisation/)

### Vendredi 27 janvier ###

*stage ou mémoire*

Semaine 03
----------

### Lundi 16 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | SUP-116 | ue a2 | P. Thibaud |
| 14h45-16h15 | SUP-116 | ue c1 | B. Papegay |

- *tp* : TP Programmation dynamique (problème de la découpe de barre)

### Mardi 17 janvier ###

stage ou mémoire

### Mercredi 18 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | sup-108   | UE B3 | M. Guichard |
| 10h15-11h45 | m3-Turing | UE A2 | P. Thibaud  |
| 13h00-14h30 | sup-116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup-116   | ue a1 | B. Papegay  |

- [squelette vigenere](./ue1/securisation/src/squel_vigenere.py)
- [cryptogrammes vigenere](./ue1/securisation/src/cryptogrammes_vigenere.py)
- [cryptanalyse vigenere](./ue1/securisation/attaque_vigenere.md)

### Jeudi 19 janvier ###

*matin* : inspé 

| Quand       | Où     | Quoi       | Qui        |
|-------------|--------|------------|------------|
| 13h00-14h30 | m5-a14 | ue a1 - tp | B. Papegay |
| 14h45-16h30 | m5-a14 | ue a1 - tp | B. Papegay |

- [tp enigma](./ue1/securisation/enigma.md)

### Vendredi 20 janvier ###

*stage ou mémoire*


Semaine 02
----------

### Lundi 9 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | SUP-116 | ue a2 | P. Thibaud |
| 14h45-16h15 | SUP-116 | ue c1 | B. Papegay |

### Mardi 10 janvier ###

stage ou mémoire

### Mercredi 11 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | sup-108   | UE B3 | M. Guichard |
| 10h15-11h45 | m3-Turing | UE A2 | P. Thibaud  |
| 13h00-14h30 | sup-116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup-116   | ue a1 | B. Papegay  |

### Jeudi 12 janvier ###

*matin* : inspé 

| Quand       | Où               | Quoi         | Qui                   |
|-------------|------------------|--------------|-----------------------|
| 13h00-14h15 | m3-226 :warning: | ue a1 (oral) | B. Papegay/P. Thibaud |
| 14h30-16h30 | m5-a14           | ue a1 - tp   | B. Papegay            |

- [arbres rouge/noir](./ue1/abr/readme.md)

### Vendredi 6 janvier ###

*stage ou mémoire*

Semaine 01
----------

### Mardi 3 janvier ###

stage ou mémoire

### Mercredi 4 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | p4-309    | UE B3 | M. Guichard |
| 10h15-11h45 | m3-Turing | UE A2 | P. Thibaud  |
| 13h00-14h30 | sup-116   | ue a1 | B. Papegay  |
| 13h45-16h15 | sup-116   | ue a1 | B. Papegay  |

[lien vers algo texte](./ue1/algo-texte/)

### Jeudi 5 janvier ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui                   |
|-------------|-----------|--------------|-----------------------|
| 13h00-14h15 | m3-Turing | ue a1 (oral) | B. Papegay/P. Thibaud |
| 14h30-16h30 | m5-a14    | ue a1 - tp   | B. Papegay            |

- [arbres rouge/noir](./ue1/abr/readme.md)

### Vendredi 6 janvier ###

*stage ou mémoire*

Semaine 50
----------

### Lundi 12 décembre ###

| Quand       | Où     | Quoi       | Qui |
|-------------|--------|------------|-----|
| 13h15-16h30 | m5-a12 | tp Huffman | diu    |

[lien vers tp huffman](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/tree/master/bloc4-5/huffman)

### Mardi 13 décembre ###

stage ou mémoire

### Mercredi 14 décembre ###

| Quand       | Où                   | Quoi              | Qui         |
|-------------|----------------------|-------------------|-------------|
| 8h30-10h00  | M1-Cartan            | UE B3             | M. Guichard |
| 10h15-11h45 | M3-ext-226 :warning: | UE A2             | P. Thibaud  |
| 13h15-16h00 | m5-a11               | tp-prog dynamique | diu         |

[lien vers tp programmation dynamique]()

### Jeudi 15 décembre ###

*matin* : inspé 

| Quand       | Où     | Quoi         | Qui                   |
|-------------|--------|--------------|-----------------------|
| 13h00-14h15 | m3-226 | ue a1 (oral) | Ph. Marquet/P. Thibaud |
| 14h30-16h30 | m5-a11 | icewalker    | diu                   |

[lien vers tp icewalker](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/bloc4-5/icewalker/icewalker.md)

### Vendredi 16 décembre ###

*stage ou mémoire*

Semaine 49
----------

### Lundi 5 décembre ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | SUP-116 | ue c1 | B. Papegay |
| 14h45-16h15 | SUP-116 | ue a2 | P. Thibaud |

- manim : [les animations](ue3/manim/animation.md)

### Mardi 6 décembre ###

stage ou mémoire

### Mercredi 7 décembre ###

| Quand       | Où      | Quoi      | Qui                 |
|-------------|---------|-----------|---------------------|
| 8h00-13h00  | p1-Bohr | **ds-a1** | service des examens |
| 14h45-16h00 | sup-118 | ue a1     | B. Papegay          |

### Jeudi 8 décembre ###

*matin* : inspé 

| Quand       | Où     | Quoi         | Qui                   |
|-------------|--------|--------------|-----------------------|
| 13h00-14h30 | m3-226 | ue a1 (oral) | B. Papegay/P. Thibaud |
| 14h45-16h15 | m5-a12 | ue a1        | B. Papegay            |


### Vendredi 25 novembre ###

*stage ou mémoire*

Semaine 48
----------

### Lundi 28 novembre ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | SUP-116 | UE C1 | B. Papegay |
| 14h45-16h15 | SUP-116 | UE C3 | B. Papegay |

- manim : [les textes](./ue3/manim/textes.md)
- tp web : [api rest](./ue3/web/webservice.md)

### Mardi 29 novembre ###

stage ou mémoire

### Mercredi 30 novembre ###

| Quand       | Où              | Quoi  | Qui         |
|-------------|-----------------|-------|-------------|
| 8h30-10h00  | M1-Cartan       | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing       | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210, SUP118 | UE A1 | B. Papegay  |


### Jeudi 1 Décembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui                   |
|-------------|-----------|--------------|-----------------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay/P. Thibaud |
| 14h45-16h15 | m5-a12    | ue a2        | P. Thibaud            |


### Vendredi 25 novembre ###

*stage ou mémoire*

Semaine 47
----------

### Lundi 21 novembre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |

- statistiques : [uec](./ue3/stats/readme.md)
- manim : [les groupes](./ue3/manim/groupes.md)

### Mardi 22 novembre ###

stage ou mémoire

### Mercredi 23 novembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M1-Cartan | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing | UE A2 | P. Thibaud  |
| 12h30-14h00 | M3-226    | UE A1 | B. Papegay  |
| 15h30-16h30 |           | UE A1 | B. Papegay  |


### Jeudi 24 novembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui                   |
|-------------|-----------|--------------|-----------------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay/P. Thibaud |
| 14h45-16h15 | m5-a12    | ue a1        | B. Papegay            |


### Vendredi 25 novembre ###

*stage ou mémoire*

Semaine 46
----------

### Lundi 14 novembre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |

- statistiques : [uec](./ue3/stats/readme.md)

### Mardi 15 novembre ###

stage 

### Mercredi 16 novembre ###

| Quand       | Où              | Quoi  | Qui         |
|-------------|-----------------|-------|-------------|
| 8h30-10h00  | M1-Cartan       | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing       | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210, SUP118 | UE A1 | B. Papegay  |

### Jeudi 17 novembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a12    | ue a1        | B. Papegay |

- [tp sql2](./ue1/sql2/Readme.md)
- [tp sql4](./ue1/sql4/tp_normalisation.md)

### Vendredi 18 novembre ###

*stage*

Semaine 45
----------

### Lundi 7 novembre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |


### Mardi 8 novembre ###

stage 

### Mercredi 9 novembre ###

| Quand       | Où              | Quoi  | Qui         |
|-------------|-----------------|-------|-------------|
| 8h30-10h00  | M1-Cartan       | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing       | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210, SUP118 | UE A1 | B. Papegay  |

### Jeudi 10 novembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a12    | ue a1        | B. Papegay |

- [tp sql2](./ue1/sql2/Readme.md)
- [tp sql4](./ue1/sql4/tp_normalisation.md)

### Vendredi 11 novembre ###

*férié*

Semaine 44
----------

Intérruption pédagogique méritée

Semaine 43
----------

### Lundi 24 octobre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |


### Mardi 25 octobre ###

stage 

### Mercredi 26 octobre ###

| Quand       | Où              | Quoi  | Qui         |
|-------------|-----------------|-------|-------------|
| 10h15-11h45 | M3-Turing       | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210, SUP118 | UE A1 | B. Papegay  |

### Jeudi 27 octobre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a12    | ue a1        | B. Papegay |

### Vendredi 28 octobre ###

*stage*

Semaine 42
----------

### Lundi 17 octobre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |

- [mermaid](./ue3/outils/mermaid.md)
- [mermaid graphiques](./ue3/outils/mermaid_graphiques.md)
- [python graphiques](./ue3/outils/python_graphiques.md)
- *tp :* [algorithmes de Floyd-Warshall et Bellman-Ford](./ue2/parcours_largeur/Readme.md)


### Mardi 18 octobre ###

stage 

### Mercredi 19 octobre ###

| Quand       | Où              | Quoi  | Qui         |
|-------------|-----------------|-------|-------------|
| 8h30-10h00  | M1-Cartan       | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing       | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210, SUP118 | UE A1 | B. Papegay  |

### Jeudi 20 octobre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a12    | ue a1        | B. Papegay |

- *suite et fin tp :* [algorithmes de Floyd-Warshall et Bellman-Ford](./ue2/parcours_largeur/Readme.md)

### Vendredi 21 octobre ###

*stage*


Semaine 41
----------

### Lundi 10 octobre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |

### Mardi 11 octobre ###

stage 

### Mercredi 12 octobre ###

| Quand      | Où         | Quoi  | Qui |
|------------|------------|-------|-----|
| 8:00-13:00 | SUP-Galilé | DS A2 |     |

### Jeudi 13 octobre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a15    | ue a1        | B. Papegay |

- [manim, cours 2](./ue3/manim/mobject.md)

### Vendredi 14 octobre ###

*stage*


Semaine 40
----------

### Lundi 3 octobre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |

- *ue3* : [cycles hamiltoniens](./ue3/hamilton/readme.md)
- *tp :* [algorithme de Dijkstra](./ue2/parcours_largeur/dijkstra.md)

### Mardi 4 octobre ###

stage 

### Mercredi 5 octobre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M1-Cartan | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210   | UE A1 | B. Papegay  |

### Jeudi 6 octobre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a15    | ue a1        | B. Papegay |

- [manim, cours 2](./ue3/manim/mobject.md)

### Vendredi 7 octobre ###

*stage*

Semaine 39
----------

### Lundi 26 septembre ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | SUP-116 | UE C1    | B. Papegay |
| 14h45-16h15 | SUP-116 | UE A2/C3 | P. Thibaud |

- *tp :* [Déplacement du cavalier](./ue2/parcours_largeur/Readme.md)
- *ue3* : [cycles hamiltoniens](./ue3/hamilton/readme.md)

### Mardi 27 septembre ###

stage 

### Mercredi 28 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M1-Cartan | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210   | UE A1 | B. Papegay  |

### Jeudi 29 septembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a15    | ue a1        | B. Papegay |

### Vendredi 30 septembre ### 

*stage*

Semaine 38
----------

### Lundi 19 septembre ###

| Quand       | Où         | Quoi     | Qui        |
|-------------|------------|----------|------------|
| 13h00-14:30 | P4-117/119 | UE C1    | B. Papegay |
| 14h45-16h15 | m5-a15     | UE A2/C3 | P. Thibaud |

### Mardi 20 septembre ###

stage 

### Mercredi 21 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M1-Cartan | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210   | UE A1 | B. Papegay  |

### Jeudi 22 septembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a15    | ue a1        | B. Papegay |

### Vendredi 23 septembre ### 

*stage*

Semaine 37
----------

### Lundi 12 septembre ###

| Quand       | Où         | Quoi     | Qui        |
|-------------|------------|----------|------------|
| 13h00-14:30 | P4-117/119 | UE C1    | B. Papegay |
| 14h45-16h15 | m5-a15     | UE A2/C3 | P. Thibaud |

### Mardi 13 septembre ###

stage 

### Mercredi 14 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M1-Cartan | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210   | UE A1 | B. Papegay  |

*diapo:* [Graphes en SNT](./ue2/graphes_snt/graphes_snt.pdf)

### Jeudi 15 septembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a15    | ue a1        | B. Papegay |

### Vendredi 9 septembre ### 

*stage*

Semaine 36
----------

### Lundi 5 septembre ###

| Quand       | Où         | Quoi     | Qui        |
|-------------|------------|----------|------------|
| 13h00-14:30 | P4-117/119 | UE C1    | B. Papegay |
| 14h45-16h15 | m5-a15     | UE A2/C3 | P. Thibaud |

*tp :* [Rush Hour](./ue2/rushhour/Readme.md)

### Mardi 6 septembre ###

stage 

### Mercredi 7 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M1-Cartan | UE B3 | M. Guichard |
| 10h15-11h45 | M3-Turing | UE A2 | P. Thibaud  |
| 13h00-16h15 | SUP-210   | UE A1 | B. Papegay  |


*diapo:* [Présentation épreuve oral 1](./ue1/epreuve_oral1/presentation_oral1.pdf)
*doc:* [Méthodologie oral 1](./ue1/epreuve_oral1/methodo_oral.md)

### Jeudi 8 septembre ###

*matin* : inspé 

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 13h00-14h30 | m3-Turing | ue a1 (oral) | B. Papegay |
| 14h45-16h15 | m5-a15    | ue a1        | B. Papegay |

[les graphes en python](./ue1/graphes_python/graphe.ipynb)

/récréation/ : [bases de manim](./ue3/manim/base.md)


### Vendredi 9 septembre ### 

*stage*
