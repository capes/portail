---
title: Master MEEF Informatique 
subtitle: Université de Lille 
author: |
	| Benoit Papegay
	| 
	| 
date: septembre 2023
lang: fr
---

<!-- à compiler par
pandoc -t beamer --slide-level 2 -V "aspectratio=1610" -s pre-rentree-23.md -o pre-rentree-23.pdf
--> 

Le Contexte
===========

L'informatique : discipline au lycée
------------------------------------

* Depuis la **réforme du lycée** entrée en vigueur en 2019,
* L'informatique est devenue une discipline au lycée :
  
  * pour tous les élèves de **seconde** (SNT : **S**ciences **N**umériques et **T**echnologie )
  * pour les élèves de la voie générale qui choisissent l'enseignement de **spécialité** 
    NSI : **N**umérique et **S**cience **I**nformatique :
    
    * 4h en 1ere, 
    * 6h en Terminale.

Le Concours
-----------

* Pour enseigner au lycée, il faut passer le concours du **CAPES NSI**
  (CAPES : **C**ertificat d'**A**ptitude au **P**rofessorat de l'**E**nseignement du **S**econd degré)
  
* Programmes : ceux du lycée avec le recul niveau master.

* 2 épreuves d'admissibilité (fin mars) :
  
    * Écrit 1 : connaissances informatiques
    * Écrit 2 : didactique, transposer ses connaissances pour les enseigner 

* 2 épreuves d'admission (fin juin)
  
    * Oral 1 : "Mise en situation professionnelle" _disciplinaire_
    * Oral 2 : Épreuve d'entretien avec le jury _professionel_

* Le concours aura lieu en **fin de M2** :
    * mars 2024 pour les M2 ou les M1 ayant déjà obtenu un master.

* Nombre de postes :
    * 2023 : 50 postes ;
    * 2022 : 50 postes ;
    * 2021 : 60 postes ;
    * 2020 : 30 postes.
    
l'INSPE
------

Le master MEÉF (**M**étiers de l'**E**nseignement, de l'**É**ducation et de la 
**F**ormation ) dépend de l'INSPÉ (**I**nstitut **N**ational **S**upérieur du **P**rofessorat 
et de l'**É**ducation)... 


* Master MEÉF mention 2nd degré, parcours informatique 
    
	* UE-BCC A : 
         * A1 : *Culture, maitrise et epistémologie dsciplinaire*  
         * A2 : *Culture et maitrise didactique*  
    * UE-BCC B : 
         * B1 : *Droit et devoir du fonctionnaire*  
         * B2 : *Stratégie d'enseignement de tronc commun*  
         * B3 : *Stratégie d'enseignement appliqué à la discipline*  
    * UE-BCC C : 
         * C1 : *Initiation, apports et pratiques de la recherche disciplinaire à visée professionnelle*  
         * C2 : *Initiation, apports et pratiques de la recherche en contexte d'éducation*  
         * C3 : *Travail sur la réflexivité professionnelle, irriguée par la recherche*  
	* UE d'anglais
    * Module de remise à niveau (*en fonction des effectifs*)

Pour valider le master, il faut valider tous les BCC.  
Il n'y a pas de compensation entre BCC.

Organisation des deux années
=======================

Semestre 1
----------

 * Cours théoriques et didactiques ;
 * Un *stage* de trois semaines ; 
   _Stage d'observation, préparation des séances avec un enseignant._

Semestre 2
-----------

 * Cours ;
 * Un *stage* de trois semaine ;
   Stage(s) d'observation et/ou de pratique accompagnée.

_Des allez-retours fréquents entre la faculté et les établissements scolaires._

Semestre 3
----------

 * En **alternance** dans un établissement scolaire, avec un maître de stage ...  
   _c'est le grand bain !_ ;
 * ou en stage long
 * Cours (entraînement aux écrits renforcé).

Semestre 4
----------

 * **En alternance** dans un établissement scolaire, avec un maître de stage ; 
 * Cours (entraînement aux oraux renforcé) ; 
 * Mémoire de fin d'étude ;
 * Concours !!


Emplois du temps
================

M1
--

| période | Lundi        | Mardi          | Mercredi    | Jeudi         | Vendredi  |
|---------|--------------|----------------|-------------|---------------|-----------|
| matin 1 | ue a1 (fond) |                | ue b3       |               | inspe     |
|         | B. Papegay   |                | M. Guichard |               |           |
|         | m1 302       |                |             |               |           |
|---------|--------------|----------------|-------------|---------------|-----------|
| matin 2 | ue c2        | ue a1 rsx/syst | ue a2       | ue c1/c3      | inspe     |
|         | B. Papegay   | L. Noé         | P. Thibaud  | B. Papegay    |           |
|         | m1 302       | P. Marquet     | m3 Turing   | m1 Fatou      |           |
|         |              | m1 Fatou       |             |               |           |
|---------|--------------|----------------|-------------|---------------|-----------|
| am 1    | ue a1 tp     |                | ue a1       | ue a1 (oral)  | anglais   |
|         | m5 a14       |                | B. Papegay  | P. Thibaud    | (à partir |
|         |              |                | m1 Fatou    | m3-Turing     | du 19/09) |
|---------|--------------|----------------|-------------|---------------|-----------|
| am 2    | ue a1 tp     |                |             | ue a2/a3 (tp) |           |
|         | m5 a14       |                |             | P. Thibaud    |           |
|         |              |                |             | m5-a15        |           |
 
M2
--

| période | Lundi      | Mardi | Mercredi    | Jeudi        | Vendredi |
|---------|------------|-------|-------------|--------------|----------|
| matin 1 |            |       | ue a2 + c3  | inspe        |          |
|         |            |       | P. Thibaud  |              |          |
|         |            |       | sup 116     |              |          |
|---------|------------|-------|-------------|--------------|----------|
| matin 2 |            |       | ue b3       | inspe        |          |
|         |            |       | M. Guichard |              |          |
|         |            |       | m1 Riesz    |              |          |
|---------|------------|-------|-------------|--------------|----------|
| am 1    | ue a1      | stage | ue a2       | ue a3 (oral) | stage    |
|         | B. Papegay |       | P. Thibaud  | B. Papegay   |          |
|         | m5 a14     |       | m1 Riesz    | m3-Turing    |          |
|---------|------------|-------|-------------|--------------|----------|
| am 2    | ue a1      |       | ue c1       | ue a1 (tp)   |          |
|         | B. Papegay |       | B. Papegay  | B. Papegay   |          |
|         | m5 a14     |       | m1 Riesz    | m5-a15       |          |

Calendrier 
----------

Document distribué (M1 et M2)

Divers 
======

Liens 
-----

* Site du jury du capes : [http://capes-nsi.org](http://capes-nsi.org)
* Site du FIL : [http://fil.univ-lille.fr](http://fil.univ-lille.fr)
* gitlab [https://gitlab-fil.univ-lille.fr/capes/portail](https://gitlab-fil.univ-lille.fr/capes/portail)
  Calendriers, ressources, ...
