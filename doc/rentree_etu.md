---
title: Master MEEF Informatique 
subtitle: Université de Lille 
author: |
	| Benoit Papegay — Philippe Marquet
	| 
	| 
date: Septembre 2019
lang: fr
---

<!-- à compiler par
pandoc -t beamer --slide-level 2 -V "aspectratio=1610" -s rentree_etu.md -o rentree_etu.pdf!
--> 

Contexte
========

Réforme du lycée
----------------

* Réforme du lycée entre en vigueur cette rentrée 
* L'enseignement de l'informatique
  
  * pour tous les élèves de seconde (SNT)
  * pour les élèves de la voie générale qui choisissent la spécialité NSI.
	(4h en 1ere, 6h en Tale)

Le Concours
-----------

* Programme : ceux de SNT et NSI avec le recul niveau master.

* 2 épreuves d'admissibilité :
  
    * Écrit 1 : connaissances informatique
    * Écrit 2 : analyse-synthèse sur documents 

* 2 épreuves d'admission
  
    * Oral 1 : "Mise en situation professionnelle"
      Oral sur un thème tiré dans une liste.
    * Oral 2 : Dossier
      à préparer au S2, sujet au choix, max 20 pages à transemettre avant l'oral
	  soutenance devant le jury.

* Inscription aux concours octobre/novembre 
     * capes
	 * cafep
	 
l'ESPE
------
* Master MEÉF mention 2nd degré, parcours informatique 
    
	* UE1 : disciplinaire
	* UE2 : didactique
	* UE3 : recherche
	* UE4 : contexte d'exercice du métier
	* UE5 : mise en situation professionnelle
	* UE CN TICE
	* UE7 : anglais

Organisation de l'année
=======================

Semestre 1
----------
  1 semaine de stage et 12 semaines de cours/examen

  * 7 semaines de cours entre le 9 septembre et le 26 octobre
  * 3 semaines de cours entre le 4 novembre et 23 novembre
  * 1 semaine de stage du 25 novembre au 7 décembre
  * 1 semaine de cours entre le 9 décembre et le 14 décembre
  * du 16 au 19 décembre : examens. 20 décembre : Examen UE 4
  
  -> Renseigner le lieu de stage souhaité.
  
Semestre 2
-----------
  2 semaines de stage et 8 semaines de cours :

  * 2 semaines de cours du 6 janvier au 18 janvier
  * 2 semaines de stage entre le 20 Janvier et le 31 Janvier
  * 3 semaines de cours du 3 Février au 22 février
  * 3 semaines de cours du 2 Mars au 21 Mars

  Concours début Avril ??

Unités d'enseignement
=====================

UE 1
----

* UE 1 : 142 h sur 12 semaines ~ 12h/semaine
  
| EC                                   |
|--------------------------------------|
| **Informatique fondamentale**        |
| **Informatique pour l'enseignement** |
| **Oral 1**                           |


UE2 - Description
-----------------
* UE 2 : 48 h => 4h semaine

| EC                  |
|---------------------|
| **didactique**      |
| **TICE spécifique** |
| **UE CN TICE**      |


Examens blancs
--------------
* 3 épreuves écrites
    * 25 septembre
    * 13 nov
    * 11 déc.
* Oraux blancs : tout au long du semestre 
	* liste en élboration
  -> volontaires pour la semaine 38 


Emploi du temps
---------------
   - [https://gitlab-fil.univ-lille.fr/capes/portail](https://gitlab-fil.univ-lille.fr/capes/portail)
   - [calendrier sur le portail](https://gitlab-fil.univ-lille.fr/capes/portail/blob/master/calendrier.md)
   - [calendier de rentrée (ne sera pas mis à jour)](https://gitlab-fil.univ-lille.fr/capes/portail/blob/master/doc/calendrier%20rentree%20M1%20MEEF%2019-20.ods)

