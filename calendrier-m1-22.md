Calendrier M1
=============

Dates des ds :
- [x] 12/10/22, A1
- [x] 6/12/22, Anglais, 13h30-15h30 SUP Lavoisier
- [x] 7/12/22, A2 8h00-13h00 P1 Bohr
- [x] 7/12/22, Anglais IO 13h30-17h45 SUP 10 et 11
- [x] 13/12/22, Anglais IO 13h30-17h45 SUP 10 et 11
- [x] 8/02/23 : A1
- [x] 16/02/23 : Rattrapage A1 
- [x] 4/04/23 : A2
- [ ] examens 15 mai / 24 mai a1, a2

Amphi Turing indisponible le 8/12

- [x] TP jeudi en A12 à partir du 20/10
- [x] Anglais à partir du 26/09
- [x] lundi 17 octobre : cours archi / système
- [x] 26/10 : pas de cours b3
- [x] 9/11 : pas de cours b3, 
- [x] rattrapage b3 le 8/11, c15 103 bis
- [x] cours b3 le 5/12, 15h45-17h00
- [x] rendu ue c1 et c2 le 15/12
- [x] rendu ue c3 le 4/01/23
- [ ] 15/05 : uec, rendu du projet de mémoire

Anglais, semestre 2 :
- [x] 16/02/23, cours 1
- [x] 02/03/23, cours 2
- [x] 09/03/23, cours 3
- [x] 30/03/23, cours 4
- [x] 06/04/23, cours 5
- [ ] 04/05/23, cours 6
- [ ] 11/05/23, cours 7

ue b3 :
- [x] 11/01 : SUP 108 à 8h30 ; SUP 209 à 10h15
- [x] 18/01 : SUP 108 à 8h30 ; P4 309 à 10h15
- [x] 25/01 : SUP 108 à 8h30 ; P4 309 à 10h15
- [x] 01/02 : SUP 108 à 8h30 ; SUP 209 à 10h15
- [x] 08/02 : SUP 108 à 8h30 ; SUP 209 à 10h15
- [x] 15/02, 01/03, 08/03, 15/03, 22/03 : SUP 108 de 8h30 à 11h45
- [x] 29/03 :  SUP 209 à 8h30 ; SUP 210 à 10h15
- [ ] 8/05 : rendu du dossier

Semaine 19
----------

### Mercredi 10 mai ###

| Quand      | Où     | Quoi  | Qui         |
|------------|--------|-------|-------------|
| 8h30-11h45 | m3 211 | ue b3 | M. Guichard |


### Jeudi 11 mai ###

| Quand       | Où           | Quoi         | Qui        |
|-------------|--------------|--------------|------------|
| 10h15-11h45 | sup 116      | ue c         | B. Papegay |
| 13h00-14h30 | amphi Turing | ue a1 (oral) | P. Thibaud |

Semaine 18
----------

### Mercredi 3 mai ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 8h30-11h45  | m3 Turing | ue a2 | P. Thibaud |

### Jeudi 4 mai ###

| Quand       | Où           | Quoi         | Qui        |
|-------------|--------------|--------------|------------|
| 10h15-11h45 | sup 116      | ue c         | B. Papegay |
| 13h00-14h30 | amphi Turing | ue a1 (oral) | P. Thibaud |

Semaines 16 & 17
---------------

Interruption pédagogique

Semaine 15
----------

### Mardi 11 avril ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | p4 309  | ue a1 | B. Papegay |
| 10h15-11h45 | sup 116 | ue c  | B. Papegay |

### Mercredi 12 avril ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 8h30-10h00  | m3 Turing | ue a2 | P. Thibaud |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay |
| 14h45-16h15 | sup 117   | ue a1 | B. Papegay |


### Jeudi 13 avril ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 10h15-11h45 | m5 a4     | ue a1 ou c   | B. Papegay  |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h45-16h15 | m5 a4     | ue a1 - tp   | P. Thibaud  |
| 15h00-17h00 | sup       | anglais      | C. Demailly |

[tp image](./ue1/progdyn/tp-images)

Semaine 14
----------

### Lundi 3 avril ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a2 tp | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c     | B. Papegay |

### Mardi 4 avril ###

| Quand       | Où          | Quoi    | Qui         |
|-------------|-------------|---------|-------------|
| 8h00-13h00  | m1 painlevé | ds - a2 |             |
| 14h00-15h30 | p2 316      | ue b3   | M. Guichard |


### Mercredi 5 avril ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3 Turing | ue a2 | P. Thibaud  |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay  |
| 14h45-16h15 | sup 116   | ue a1 | B. Papegay  |


### Jeudi 6 avril ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h45-16h15 | m5 a11    | ue a1 - tp   | P. Thibaud  |
| 15h00-17h00 | sup       | anglais      | C. Demailly |



Semaine 13
----------

### Lundi 27 mars ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a2 tp | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c     | B. Papegay |

- [transformations bijectives d'images](./ue1/bijection-images/readme.md)

### Mercredi 29 mars ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3 Turing | ue a2 | P. Thibaud  |
| 10h15-11h45 | sup 210   | ue b3 | M. Guichard |
| 13h00-14h30 | sup 116   | ue a1 | B. Papegay  |
| 14h45-16h15 | sup 116   | ue a1 | B. Papegay  |


### Jeudi 30 mars ###

| Quand       | Où        | Quoi         | Qui                      |
|-------------|-----------|--------------|--------------------------|
| 8h30-10h00  | sup-116   | ue c         | B. Papegay               |
| 10h15-11h45 | p4-119    | ue a1        | B. Papegay               |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud & Ph. Marquet |
| 14h45-16h15 | m5 a4     | ue a1 - tp   | P. Thibaud               |
| 15h00-17h00 | sup       | anglais      | C. Demailly              |

- [programmation dynamique](./ue1/progdyn/tp-covid/Projet_COVID_PD.md)

Semaine 11 et 12
----------------

*stage en établissement*

Semaine 10
----------

### Lundi 6 mars ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a2 tp | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c     | B. Papegay |

- [algos gloutons : plannificateurs de tâches](./ue2/glouton/td_glouton.md)


### Mardi 7 mars ###

**les cours seront rattrapés ultérieurement**

### Mercredi 8 mars ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3 Turing | ue a2 | P. Thibaud  |
| 10h15-11h45 | sup 108   | ue b3 | M. Guichard |


### Jeudi 9 mars ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 8h30-10h00  | sup-116   | ue c1/c2/c3  | B. Papegay  |
| 10h15-11h45 | p4-119    | ue a1        | B. Papegay  |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h45-16h15 | m5 a4     | ue a1 - tp   | P. Thibaud  |
| 15h00-17h00 | sup       | anglais      | C. Demailly |

- [programmation dynamique](./ue1/progdyn/tp-covid/Projet_COVID_PD.md)

Semaine 09
----------

### Lundi 27 février ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a2 tp | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c     | B. Papegay |

- [tsp](./ue2/tsp/Readme.md)

### Mardi 28 février ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | p4 309  | ue a1 | B. Papegay |
| 10h15-11h45 | sup 116 | ue a1 | B. Papegay |

### Mercredi 1 mars ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3 Turing | ue a2 | P. Thibaud  |
| 10h15-11h45 | sup 108   | ue b3 | M. Guichard |


### Jeudi 2 mars ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 8h30-10h00  | sup-116   | ue c1/c2/c3  | B. Papegay  |
| 10h15-11h45 | p4-119    | ue a1        | B. Papegay  |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h45-16h15 | m5 a4     | ue a1 - tp   | B. Papegay  |
| 15h00-17h00 | sup       | anglais      | C. Demailly |

- [Bob Morane](./ue1/bob-morane/Readme.md)
- [algos gloutons](./ue2/glouton/td_glouton.md)

Semaine 08
----------

*interruption pédagogique*

Semaine 07
----------

### Lundi 13 février ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a2 tp | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c     | B. Papegay |

- *tp* : [révisions JS (suite)](./ue1/js1/readme.md)
- *tp* : [JS ES6 : Arrray utilisation de fonctionnelles](./ue2/js_es6/fonctionnelles/readme.md)


### Mardi 14 février ###

| Quand       | Où              | Quoi  | Qui        |
|-------------|-----------------|-------|------------|
| 10h15-11h45 | sup 116 :alert: | ue a1 | B. Papegay |

### Mercredi 15 février ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3 Turing | ue a2 | P. Thibaud  |

### Jeudi 16 février ###

| Quand       | Où        | Quoi             | Qui         |
|-------------|-----------|------------------|-------------|
| 9h30-12h00  | m1 Newton | rattrapage ue a1 | B. Papegay  |
| 13h00-14h30 | m3 Turing | ue a1 (oral)     | P. Thibaud  |
| 14h45-16h15 | m5 a11    | ue a1 - tp       | B. Papegay  |
| 15h00-17h00 | sup       | anglais          | C. Demailly |

- [Bob Morane](./ue1/bob-morane/Readme.md)

Semaine 06
----------

### Lundi 6 février ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a2 tp | P. Thibaud |
| 14h45-16h15 | sup 116 | ue c     | B. Papegay |

- *tp* : [révisions JS (suite)](./ue1/js1/readme.md)
- *tp* : [JS ES6 : Arrray utilisation de fonctionnelles](./ue2/js_es6/fonctionnelles/readme.md)


### Mardi 7 février ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8h30-10h00  | p4 309 | ue a1 | B. Papegay |
| 10h15-11h45 | p4 309 | ue a1 | B. Papegay |

### Mercredi 8 février ###

| Quand      | Où          | Quoi  | Qui        |
|------------|-------------|-------|------------|
| 8h00-13h00 | m1 Painlevé | ds a1 | B. Papegay |


### Jeudi 9 février ###

| Quand       | Où        | Quoi         | Qui        |
|-------------|-----------|--------------|------------|
| 8h30-10h00  | sup 116   | ue c         | B. Papegay |
| 10h15-11h45 | p4 119    | ue a1        | B. Papegay |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5 a11    | ue a1 - tp   | B. Papegay |

- *tp* : [JS ES6 : Arrray utilisation de fonctionnelles](./ue2/js/fonctionnelles/readme.md)


Semaine 05
----------

### Lundi 30 janvier ###

| Quand       | Où      | Quoi     | Qui        |
|-------------|---------|----------|------------|
| 13h00-14:30 | sup 116 | ue a2 tp | P. Thibaud |
| 14h45-16h15 | sup 116 | ue a1    | B. Papegay |

- [tp huffman](./ue1/huffman/)

### Mardi 31 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | sup 124 | ue c  | B. Papegay |
| 10h15-11h45 | sup 116 | ue a1 | B. Papegay |

### Mercredi 1 février ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3 Turing | ue a2 | P. Thibaud  |
| 10h15-11h45 | sup 209   | ue b3 | M. Guichard |

### Jeudi 2 février ###

| Quand       | Où        | Quoi         | Qui         |
|-------------|-----------|--------------|-------------|
| 10h15-11h45 | m3 226    | ue a1        | Ph. Marquet |
| 13h00-14h30 | m3 Turing | ue a1 (oral) | P. Thibaud  |
| 14h45-16h15 | m5 a11    | ue a1 - tp   | B. Papegay  |

- *tp* : [révisions JS](./ue1/js1/readme.md)

Semaine 04
----------

### Lundi 23 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue a2 | P. Thibaud |
| 14h45-16h15 | sup 116 | ue a1 | B. Papegay |

- *tp* : [protocole HTTP](./ue2/reseaux/tp3_http/tp_http.md)
- [jupyter et tikz](./ue1/jupytikz)

### Mardi 24 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 8h30-10h00  | p4 309  | ue a1 | B. Papegay |
| 10h15-11h45 | sup 116 | ue a1 | B. Papegay |

### Mercredi 25 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | p4 309    | UE B3 | M. Guichard |

### Jeudi 26 janvier ###

| Quand       | Où        | Quoi          | Qui        |
|-------------|-----------|---------------|------------|
| 8h30-10h00  | sup-116   | ue c1/c2/c3   | B. Papegay |
| 10h15-11h45 | p4-119    | ue a1         | B. Papegay |
| 13h00-14h30 | m3-Turing | ue a1 (oral)  | P. Thibaud |
| 14h45-16h15 | m5-a4     | ue a2/a3 - tp | P. Thibaud |

- [les filtres de Bloom](./ue1/bloom/bloom.md)
- [algorithme de Huffman](./ue1/huffman/Readme.md)


Semaine 03
----------

### Lundi 16 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | sup 116 | ue a2 | P. Thibaud |
| 14h45-16h15 | sup 116 | ue a1 | B. Papegay |

- *tp* : [protocole TCP](./ue2/reseaux/tp2_tcp/tp2_tcp.md)

### Mardi 17 janvier ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 8h30-10h00  | p4-309  | ue a1 | B. Papegay  |
| 10h15-11h45 | sup 116 | ue a1 | Ph. Marquet |

### Mercredi 18 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | p4-309    | UE B3 | M. Guichard |

### Jeudi 19 janvier ###

| Quand       | Où     | Quoi          | Qui        |
|-------------|--------|---------------|------------|
| 13h00-14h30 | m3-226 | ue a1         | P. Thibaud |
| 14h45-16h15 | m5-a14 | ue a2/a3 - tp | P. Thibaud |

- *tp* : icewalker, protocole TCP ou [les filtres de Bloom](./ue1/bloom/bloom.md)

Semaine 02
----------

### Lundi 9 janvier ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14:30 | SUP-116 | ue c  | B. Papegay |
| 14h45-16h15 | SUP-116 | ue a1 | B. Papegay |

### Mardi 10 janvier ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8h30-10h00  | p4-309 | ue a1 | B. Papegay |
| 10h15-11h45 | p4-309 | ue a1 | B. Papegay |

### Mercredi 11 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | sup-209   | UE B3 | M. Guichard |

### Jeudi 12 janvier ###

| Quand       | Où      | Quoi          | Qui        |
|-------------|---------|---------------|------------|
| 8h30-10h00  | sup-116 | ue c1/c2/c3   | B. Papegay |
| 10h15-11h45 | p4-119  | ue a1         | B. Papegay |
| 13h00-14h30 | m3-226  | ue a1 (oral)  | P. Thibaud |
| 14h45-16h15 | m5-a14  | ue a2/a3 - tp | P. Thibaud |

- [méthodes d'ajustement affine](./ue3/ajustement/ajustement.org)

Semaine 01
----------

### Lundi 2 janvier ###

**pas de cours**

### Mardi 3 janvier ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 8h30-10h00  | p4-309 | ue a1 | B. Papegay |
| 10h15-11h45 | p4-309 | ue a1 | B. Papegay |

### Mercredi 4 janvier ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | m3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | p4-309    | UE B3 | M. Guichard |

### Jeudi 5 janvier ###
    
| Quand       | Où        | Quoi          | Qui        |
|-------------|-----------|---------------|------------|
| 8h30-10h00  | sup-116   | ue c1/c2/c3   | B. Papegay |
| 10h15-11h45 | p4-119    | ue a1         | B. Papegay |
| 13h00-14h30 | m3-Turing | ue a1 (oral)  | P. Thibaud |
| 14h45-16h15 | m5-a14    | ue a2/a3 - tp | P. Thibaud |

- [icewalker](./ue1/icewalker/icewalker.md)
- [arbres rouge/noir](./ue1/abr/readme.md)

Semaine 50
----------

### Lundi 12 décembre ###

**pas de cours**

### Mardi 13 décembre ###

| Quand       | Où           | Quoi      | Qui        |
|-------------|--------------|-----------|------------|
| 8h30-10h00  | M1-Fatou     | ue a1     | B. Papegay |
| 10h15-11h45 | M1-Dirichlet | ue a1     | L. Noé     |
| 13h30-?     |              | Anglais ? |            |

### Mercredi 14 décembre ###

| Quand       | Où                   | Quoi  | Qui         |
|-------------|----------------------|-------|-------------|
| 8h30-10h00  | M3-ext-226 :warning: | UE A2 | P. Thibaud  |
| 10h15-11h45 | M1-Cartan            | UE B3 | M. Guichard |

### Jeudi 15 décembre ###
    
| Quand       | Où              | Quoi         | Qui        |
|-------------|-----------------|--------------|------------|
| 8h30-10h00  | sup-116         | ue c1/c2/c3  | B. Papegay |
| 10h15-11h45 | m1-Hermite      | ue a1        | B. Papegay |
| 13h00-14h30 | m3-Turing       | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | M5-A16 avec DIU | ue a2/a3     | P. Thibaud |

[lien vers tp icewalker](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/bloc4-5/icewalker/icewalker.md)

### Vendredi 16 décembre ### 

inspé selon calendrier

Semaine 49
----------

### Lundi 5 décembre ###

| Quand       | Où      | Quoi  | Qui         |
|-------------|---------|-------|-------------|
| 14h00-15h30 | SUP 116 | UE C  | B. Papegay  |
| 15h45-17:15 | SUP 108 | UE B3 | M. Guichard |

uec :
- le point sur les rendus
- au choix :
  - [réaliser une animation](./ue3/py5/Readme.md) ;
  - [les abr](./ue3/abr/readme.md) (prochain thème d'étude).

### Mardi 6 décembre ###

| Quand       | Où                   | Quoi    | Qui        |
|-------------|----------------------|---------|------------|
| 8h30-10h00  | M1-Fatou             | ue a1   | B. Papegay |
| 10h15-11h45 | M1-Dirichlet         | ue a1   | L. Noé     |
| 13h30-15h30 | SUP Lavoisier **ds** | Anglais | N. Chapel  |

- cours a1 : les arbres, définitions, implantation et vocabulaire.

### Mercredi 7 décembre ###

| Quand      | Où      | Quoi      | Qui                 |
|------------|---------|-----------|---------------------|
| 8h00-13h00 | p1-Bohr | **ds-a2** | service des examens |

### Jeudi 8 décembre ###
    
| Quand       | Où      | Quoi         | Qui        |
|-------------|---------|--------------|------------|
| 8h30-10h00  | sup-116 | ue c1/c2/c3  | B. Papegay |
| 10h15-11h45 | sup-116 | ue a1        | B. Papegay |
| 13h00-14h30 | m3-226  | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5-a12  | ue a2/a3     | P. Thibaud |

- cours a1 : td arbres
- *tp* : [tp filius](./ue2/reseaux/tp1_filius/tp1_filius.md)

### Vendredi 9 décembre ### 

inspé selon calendrier
    

Semaines 47 & 48
----------------

*stage* : pensez à rédiger le CR pour l'inspé dès que possible.

Semaine 46
----------

### Lundi 14 novembre ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 13h00-14h30 | SUP 118 | UE C  | B. Papegay |
| 14h45-16:15 | SUP 118 | UE A1 | B. Papegay |

*tris linéaires (uec)* [tris](./ue3/tris_lineaires/readme.md)
*tp sql* : [sql](./ue1/sql1/Readme.md)

### Mardi 15 novembre ###

| Quand       | Où           | Quoi    | Qui         |
|-------------|--------------|---------|-------------|
| 8h30-10h00  | M1-Fatou     | ue a1   | B. Papegay  |
| 10h15-11h45 | M1-Dirichlet | ue a1   | Ph. Marquet |
| 13h30-15h30 | SUP 10       | Anglais | N. Chapel   |

### Mercredi 16 novembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | M1-Cartan | UE B3 | M. Guichard |
| 14h00-15h30 | M1-202    | UE B3 | M. Guichard |

### Jeudi 17 novembre ###
    
| Quand       | Où         | Quoi         | Qui        |
|-------------|------------|--------------|------------|
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5-a12     | ue a2/a3     | P. Thibaud |

### Vendredi 18 novembre ### 

inspé selon calendrier
    
Semaine 45
----------

### Lundi 7 novembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 14h45-16:15 | M5-A15 | UE A1 | B. Papegay |

possibilité de venir à partir de 14h00 avec les m2.

*tp* : [tp analyse des tris](./ue1/analyse_tris/Readme.md)

### Mardi 8 novembre ###

| Quand       | Où           | Quoi    | Qui        |
|-------------|--------------|---------|------------|
| 8h30-10h00  | M1-Fatou     | ue a1   | B. Papegay |
| 10h15-11h45 | M1-Dirichlet | ue a1   | L. Noé     |
| 13h30-15h30 | SUP 10       | Anglais | N. Chapel  |

### Mercredi 9 novembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |

### Jeudi 10 novembre ###
    
| Quand       | Où         | Quoi         | Qui        |
|-------------|------------|--------------|------------|
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud |
| 14h45-16h15 | m5-a12     | ue a2/a3     | P. Thibaud |

*tp à finir* : [tp analyse des tris](./ue1/analyse_tris/Readme.md)
*tp sql* : [sql](./ue1/sql1/Readme.md)

### Vendredi 11 novembre ### 

**férié**
    
Semaine 44
----------

Interruption pédagogique méritée


Semaine 43
----------

### Lundi 24 octobre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 14h45-16:15 | M5-A15 | UE A1 | B. Papegay |

possibilité de venir à partir de 14h00 avec les m2.

### Mardi 25 octobre ###

| Quand       | Où           | Quoi    | Qui         |
|-------------|--------------|---------|-------------|
| 8h30-10h00  | M1-Fatou     | ue a1   | B. Papegay  |
| 10h15-11h45 | M1-Dirichlet | ue a1   | Ph. Marquet |
| 13h30-15h30 | SUP 10       | Anglais | N. Chapel   |

### Mercredi 26 octobre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |

### Jeudi 27 octobre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a12     | ue a2/a3     | P. Thibaud   |

### Vendredi 28 octobre ### 

*matin :* inspe selon calendrier


Semaine 42
----------

### Lundi 17 octobre ###

| Quand       | Où     | Quoi  | Qui         |
|-------------|--------|-------|-------------|
| 14h45-16:15 | M5-A15 | UE A1 | Ph. Marquet |


### Mardi 18 octobre ###

| Quand       | Où           | Quoi    | Qui        |
|-------------|--------------|---------|------------|
| 8h30-10h00  | M1-Fatou     | ue a1   | B. Papegay |
| 10h15-11h45 | M1-Dirichlet | ue a1   | L. Noé     |
| 13h30-15h30 | SUP 10       | Anglais | N. Chapel  |

### Mercredi 19 octobre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | M1-Cartan | UE B3 | M. Guichard |

### Jeudi 20 octobre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 8h30-10h00  | m1-Fatou   | ue c1        | F. De Comité |
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a12     | ue a2/a3     | P. Thibaud   |

- suite tp [pile et file: taquin](./ue1/pile_et_file/readme.md)
- projet Tle NSI [bataille](./ue2/bataille/war.md)

### Vendredi 21 octobre ### 

*matin :* inspe selon calendrier


Semaine 41
----------

### Lundi 10 octobre ###

| Quand       | Où      | Quoi  | Qui        |
|-------------|---------|-------|------------|
| 14h45-16:15 | SUP-116 | UE A1 | B. Papegay |

(vous pouvez venir à partir de 14h00 avec les m2)

- *tp* : [listes récursives](./ue1/listes/listes.md)
- [pile et file](./ue1/pile_et_file/readme.md)

### Mardi 11 octobre ###

| Quand       | Où           | Quoi    | Qui        |
|-------------|--------------|---------|------------|
| 8h30-10h00  | M1-Fatou     | ue a1   | B. Papegay |
| 10h15-11h45 | M1-Dirichlet | ue a1   | B. Papegay |
| 13h30-15h30 | SUP 10       | Anglais | N. Chapel  |

### Mercredi 12 octobre ###

| Quand      | Où          | Quoi  | Qui |
|------------|-------------|-------|-----|
| 8:00-13:00 | SUP-Galiléé | DS A1 |     |

### Jeudi 13 octobre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 8h30-10h00  | m1-Fatou   | ue c1        | F. De Comité |
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a15     | ue a2/a3     | P. Thibaud   |

### Vendredi 14 octobre ### 

*matin :* inspe selon calendrier


Semaine 40
----------

### Lundi 3 octobre ###

| Quand       | Où      | Quoi    | Qui        |
|-------------|---------|---------|------------|
| 14h45-16:15 | SUP-116 | UE A1   | B. Papegay |

(vous pouvez venir à partir de 14h00 avec les m2)

- *tp* : [listes récursives](./ue1/listes/listes.md)

### Mardi 4 octobre ###

| Quand       | Où           | Quoi    | Qui        |
|-------------|--------------|---------|------------|
| 8h30-10h00  | M1-Fatou     | ue a1   | B. Papegay |
| 10h15-11h45 | M1-Dirichlet | ue a1   | L. Noé     |
| 13h30-15h30 | SUP 10       | Anglais | N. Chapel  |

### Mercredi 5 octobre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | M1-Cartan | UE B3 | M. Guichard |

### Jeudi 6 octobre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 8h30-10h00  | m1-Fatou   | ue c1        | F. De Comité |
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a15     | ue a2/a3     | P. Thibaud   |

### Vendredi 7 octobre ### 

*matin :* inspe selon calendrier


Semaine 39
----------

### Lundi 26 septembre ###

| Quand       | Où      | Quoi    | Qui        |
|-------------|---------|---------|------------|
| 14h00-15:30 | SUP-116 | UE A1   | B. Papegay |

- *tp* [La course aux chicons !](./ue1/course_chicon/readme.md)

### Mardi 27 septembre ###

| Quand       | Où       | Quoi    | Qui         |
|-------------|----------|---------|-------------|
| 8h30-10h00  | M1-Fatou | ue a1   | B. Papegay  |
| 10h15-11h45 | SUP-116  | ue a1   | Ph. Marquet |
| 13h30-15h30 | SUP 10   | Anglais | N. Chapel   |

* [Manipulation de processus](./ue1/system/Readme.md) 

### Mercredi 28 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | M1-Cartan | UE B3 | M. Guichard |

### Jeudi 29 septembre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 8h30-10h00  | m1-Fatou   | ue c1        | F. De Comité |
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a15     | ue a2/a3     | P. Thibaud   |

### Vendredi 30 septembre ### 

*matin :* inspe selon calendrier

Semaine 38
----------

### Lundi 19 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 14h45-16:15 | M5-A15 | UE A1 | B. Papegay |

*tp* :
- finir le tp "poo en python"

### Mardi 20 septembre ###

| Quand       | Où           | Quoi  | Qui        |
|-------------|--------------|-------|------------|
| 8h30-10h00  | M1-Fatou     | ue a1 | B. Papegay |
| 10h15-11h45 | M1-Dirichlet | ue a1 | L. Noé     |

### Mercredi 21 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | M1-Cartan | UE B3 | M. Guichard |

### Jeudi 22 septembre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 8h30-10h00  | m1-Fatou   | ue c1        | F. De Comité |
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a15     | ue a2/a3     | P. Thibaud   |

- [fichiers et codage des caractères](./ue1/tp_codage_caracteres/Readme.md)

### Vendredi 23 septembre ### 

*matin :* inspe selon calendrier

Semaine 37
----------

### Lundi 12 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 14h45-16:15 | M5-A15 | UE A1 | B. Papegay |

*tp :* [poo en python](./ue1/poo_python/readme.md)

### Mardi 13 septembre ###

| Quand       | Où           | Quoi  | Qui         |
|-------------|--------------|-------|-------------|
| 8h30-10h00  | M1-Fatou     | ue a1 | B. Papegay  |
| 10h15-11h45 | M1-Dirichlet | ue a1 | Ph. Marquet |

### Mercredi 14 septembre ###

| Quand       | Où        | Quoi  | Qui         |
|-------------|-----------|-------|-------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud  |
| 10h15-11h45 | M1-Cartan | UE B3 | M. Guichard |

### Jeudi 15 septembre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 8h30-10h00  | m1-Fatou   | ue c1        | F. De Comité |
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a15     | ue a2/a3     | P. Thibaud   |

*tp :* [markdown/pandoc](./ue2/markdown/Readme.md)

### Vendredi 16 septembre ### 

*matin :* inspe selon calendrier

Semaine 36
----------

### Lundi 5 septembre ###

| Quand       | Où     | Quoi  | Qui        |
|-------------|--------|-------|------------|
| 14h45-16:15 | M5-A15 | UE A1 | B. Papegay |

*tp :* [phrases de passe](./ue1/phrases_de_passe/readme.md)

### Mardi 6 septembre ###

| Quand       | Où       | Quoi  | Qui         |
|-------------|----------|-------|-------------|
| 8h30-10h00  | M1-Fatou | ue a1 | B. Papegay  |
| 13h15-14h45 | M5-A12   | ue a1 | Ph. Marquet |

* [Système d'exploitation, 1re séance](./ue1/system/Readme.md) 

### Mercredi 7 septembre ###

| Quand       | Où        | Quoi  | Qui        |
|-------------|-----------|-------|------------|
| 8h30-10h00  | M3-Turing | UE A2 | P. Thibaud |
| 10h15-11h45 | M1-Cartan | UE B3 | M. Guichard |

*diapo M1:* [Ressources programmes](./ue2/ressources/diapo_m1_presentation_ressources.pdf)

### Jeudi 8 septembre ###

| Quand       | Où         | Quoi         | Qui          |
|-------------|------------|--------------|--------------|
| 8h30-10h00  | m1-Fatou   | ue c1        | F. De Comité |
| 10h15-11h45 | m1-Hermite | ue c1/c2/c3  | B. Papegay   |
| 13h00-14h30 | m3-Turing  | ue a1 (oral) | P. Thibaud   |
| 14h45-16h15 | m5-a15     | ue a2/a3     | P. Thibaud   |

*diapo:* [Présentation épreuve oral 1](./ue1/epreuve_oral1/presentation_oral1.pdf)
*doc:* [Méthodologie oral 1](./ue1/epreuve_oral1/methodo_oral.md)

### Vendredi 9 septembre ### 

*matin :* inspe selon calendrier

Semaine 35
----------
    
### Jeudi 1 septembre ###

  - pré-rentrée (Amphi Turing, Bâtiment M3) à 14h00
  

