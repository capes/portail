Passages oraux
===============

mis à jour le 03/04/2024

| Leçon       | Etudiant(e)                                                                        | Date  |
|:------------|:-----------------------------------------------------------------------------------|:-----:|
| Theo T.     | Représentation des données : type et valeurs de base                               | 14/09 |
| Clement     | Mise au point des programmes, documentation et gestion des bugs                    | 21/09 |
| Lucas  .    | Constructions élémentaires des langages de programmations, fonctions               | 28/09 |
| Nicolas D.  | Récursivité                                                                        | 05/10 |
| Timothee    | Paradigmes de programmation                                                        | 12/10 |
| Nicolas L.  | Structures linéaires de données                                                    | 19/10 |
| Hugo        | Algorithmes de tri                                                                 | 26/10 |
| Antoine     | Interactions homme-machine sur le Web                                              | 09/11 |
| _stage M1_ |-------|16/11|
| _stage M1_ |-------|23/11|
| _stage M1_ |-------|30/11|
| Theo Q.     | Traitement des données en table                                                    | 07/12 |
| Matthieu    | Langage SQL d'interrogation et de manipulation d'une base de données relationnelle | 14/12 |
| Theo T.     | Arbres : structures de données et algorithmes                                      | 21/12 |
| Lucas       | Graphes : structures de données et algorithmes                                     | 11/01 |
| Clement     | Principes du Web                                                                   | 25/01 |
| Nicolas  D. | Algorithmes gloutons                                                               | _Mercredi 31/01_ |
| Matthieu |Méthode diviser pour régner|01/02|
| _stage M1_ |-------|08/02|
| _stage M1_ |-------|15/02|
| _stage M1_ |-------|22/02|
| Nicolas D. |Conception et modélisation de données dans une base de données relationnelle|_Mercredi 06/03_|
|Hugo|Programmation dynamique|07/03|
| Timothee |Principe de fonctionnement des réseaux|14/03|
| _écrits CAPES_ |---------|21/03|
| Antoine     |Architecture d'une machine|28/03|
| _M1 INSPE_ |---------|04/04|
| Theo Q. |Principe de fonctionnement d'un système d'exploitation|11/04|
| Nicolas L. |Sécurisation des communications|_Mercredi 17/04_|
| Lucas       |Algorithmes et protocoles de routage dans les réseaux|18/04|
| Theo T.     |Gestion des processus et des ressources par un système d'exploitation|16/05|
| Clement     |Calculabilité et décidabilité|23/05|
