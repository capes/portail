Calendrier M1
=============


Planning Soutenances UE3
------------------------

| Date        | Horaire | Année | Groupe                    | Sujet                                         |                  |
|-------------|---------|-------|---------------------------|-----------------------------------------------|------------------|
| Lundi 10/05 |         |       | Exam d'anglais l'ap midi  | possibilité le matin                          |                  |
| Mardi 11/05 | 9h00    | M1    | E. DEMERVILLE, M. VINIT   | Les cryptomonnaies                            | Benoit           |
| Mardi 11/05 | 9h45    | M1    | C. BRASSART, J. BENOUWT   | Problème de l'arrêt                           | Benoit           |
| Mardi 11/05 | 10h30   | M1    | P. ZAGACKI, A. MARETTE    | Ordinateur qui apprend à jouer                | Benoit, Patrice  |
| Mardi 11/05 | 11h15   | M1    | J. HERMILLIER,            | Informatique et enjeu environnementaux        | Philippe, Benoit |
| Mardi 11/05 | 12h00   | M2    | T. DECOSTER, J. DUCHAINE, | Les variables et fonctions en info            | Philippe, Benoit |
| Mardi 11/05 | 14h00   | M2    | F. MATHIEU, N. MABRIEZ    | Apprentissage de l'informatique par les jeux  | Philippe, Benoit |
| Mardi 11/05 | 15h00   | M1    | T. BODDAERT, M. BERTILLE  | Apprentissage de l'informatique par les jeux  | Maude, Benoit    |
| Lundi 17/05 | 10h00   | M2    | P. BODDAERT, A. MERESSE   | Informatique et vie privée                    | Laetitia, Benoit |
| Lundi 17/05 | 11h00   | M2    | Y. IKKLI, E. FREMEAUX     | L'informatique et les enjeux environnementaux | Laetitia, Benoit |


Anglais
-------

- ECRIT (CO + CE + PE type CLES) le lundi 10 de 13h30 à 16h15, amphis 13 et 14 au SUP

- IO : lundi 10 et mardi 11, salles de langue du SUP ; s'inscrire (en trinômes si possible) sur ce document https://docs.google.com/spreadsheets/d/1zseONCcp1MB7i-ya0Kz3zWlUARA7PsiWSFxNjS8VBIc/edit?fbclid=IwAR3UNFEAOMrw5rwKK-xbIPZOE3YWA4HdYnvVn0Nm-Nho1hF6PyH7ZDc2KgA#gid=0

Semaine 14
---------------

### Mardi 6 avril

| Quand       | Où       | Quoi                                            | Qui             |
|-------------|----------|-------------------------------------------------|-----------------|
| 12h00-14h00 | Présence | Oral 1 (Amélie) : Structure linéaire de données | Patrice, Benoit |

### Jeudi 8 avril

| Quand       | Où       | Quoi              | Qui             |
|-------------|----------|-------------------|-----------------|
| 10h00-13h00 | M5 - A4  | UE2               | P. Thibaud      |
| 13h30-15h30 | M3 - 226 | Oral 1 (Philippe) | Patrice, Benoit |

Semaine 13
----------

### Mercredi 31 mars
---------------------

| Quand       | Comment    | Quoi | Qui          |
|-------------|------------|------|--------------|
| 13h30-15h30 | Distanciel | UE5  | C. Mieszczak |


### Jeudi 1 avril
-----------------

| Quand | Où      | Quoi | Qui        |
|-------|---------|------|------------|
| Matin | M5 - A4 | UE2  | P. Thibaud |


Semaine 12
----------

- Première épreuve : **lundi 22 mars**, de 9h à 14h ;
- Deuxième épreuve : **mardi 23 mars**, de 9h à 14h.

- réunion Jeudi matin pour commenter les épreuves et prévoir la suite.

Semaine 11
----------

### Lundi 15 mars ###

| Quand       | Où        | Quoi                                                   | Qui         |
|-------------|-----------|--------------------------------------------------------|-------------|
| 13h00-16h00 | M3-Turing | [UE1 - Système d'exploitation](./ue1/system/Readme.md) | Ph. Marquet |

### Mardi 16 mars ###


| Quand       | Où       | Quoi                 | Qui        |
|-------------|----------|----------------------|------------|
| 8h30-10h00  | Présence | UE1 : Correction DS7 | P. Thibaud |
| 10h15-12h00 | Présence | UE1 : Correction DS7 | B. Papegay |

Semaine 10
----------

Lectures :
- Histoire :
  + [Histoire de l'informatique](https://delmas-rigoutsos.nom.fr/documents/) ,
  + [Une synthèse](http://www.e-miage.fr/MONE2/section1/pdf/section1_1.pdf);
- Enjeux sociétaux :
  + [informatique et liberté](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/bloc3/libre/libertes-ecran.pdf),
  + [informatique et partage](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/bloc3/libre/partage-ecran.pdf);
- [Enjeux environmentaux](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/Readme-1820.md#-enjeux-environnementaux).

### Lundi 08 mars

| Quand       | Où       | Quoi                 | Qui |
|-------------|----------|----------------------|-----|
| 13h00-16h00 | Présence | UE1 - Correction DS6 |     |


### Mardi 09 mars

| Quand       | Où       | Quoi           | Qui        |
|-------------|----------|----------------|------------|
| 8h30-10h00  | Présence | UE? : histoire | B. Papegay |
| 10h15-12h00 | Présence | UE1 : Réseaux  | L. Noé     |

### Mercredi 10 mars

| Quand      | Où                   | Quoi    | Qui        |
|------------|----------------------|---------|------------|
| 8h00-13h00 | Présence - A5 à 7h30 | DS- UE1 | B. Papegay |

### Jeudi 11 mars

| Quand      | Où        | Quoi                   | Qui        |
|------------|-----------|------------------------|------------|
| 9h00-11h30 | Distance  | UE1 : Algos génétiques | B. Papegay |
| Après-midi | Autonomie | UE1 : TP-Réseau        |            |

TP - Réseau :
- [TD préparatoire](https://www.fil.univ-lille1.fr/~noe/rsx2_portail/files/TD4.pdf) ;
- [Correction du TD](https://www.fil.univ-lille1.fr/~noe/rsx2_portail/files/TD_g1g2_notes_diffusees/RSX2-TD4-mes-notes-NOE.pdf) ;
- [Le TP (partie MTU facultative)](https://www.fil.univ-lille1.fr/~noe/rsx2_portail/files/TP2/TP.pdf).

Semaine 09
----------

### Lundi 01 mars

| Quand       | Où       | Quoi                                 | Qui        |
|-------------|----------|--------------------------------------|------------|
| 13h00-16h00 | Présence | UE1 : Classification supervisée, knn | B. Papegay |
|             |          |                                      |            |


### Mardi 02 mars

| Quand       | Où       | Quoi                        | Qui        |
|-------------|----------|-----------------------------|------------|
| 8h30-10h00  | Présence | UE1 : préparation à l'écrit | B. Papegay |
| 10h15-12h00 | Présence | UE2 : Correction DS5        | P. Thibaud |

### Mercredi 03 mars

| Quand      | Où       | Quoi    | Qui        |
|------------|----------|---------|------------|
| 8h00-13h00 | Présence | DS- UE2 | P. Thibaud |

### Jeudi 04 mars

| Quand      | Où       | Quoi                                             | Qui        |
|------------|----------|--------------------------------------------------|------------|
| 9h00-11h30 | Distance | UE1 : préparation à l'écrit / arbres de décision | B. Papegay |
| Après-midi | Distance | [UE1 : TP-knn](./ue1/pokemon/pokemon.md)             | B. Papegay |

Semaine 08
----------

lundi 22 au samedi 27 février. Interruption pédagogique 

Semaine 07
----------

### Lundi 15 février

| Quand       | Où       | Quoi                                 | Qui         |
|-------------|----------|--------------------------------------|-------------|
| 14h00-15h00 | Distance | [UE1 - archi](./ue1/archi/Readme.md) | Ph. Marquet |
|             |          |                                      |             |

### Mardi 16 février

| Quand       | Comment            | Quoi | Qui        |
|-------------|--------------------|------|------------|
| 8h30-10h00  | Présentiel, M3 226 | UE1  | L. Noé     |
| 10h15-12h00 | Présentiel, M3 226 | UE1  | B. Papegay |


### Mercredi 17 février

_entrainement à l'écrit_  en autonomie, correction jeudi.

### Jeudi 18 février

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 8h30-12h00  | Distanciel | UE2  | P. Thibaud |
| 13h00-16h00 | Distanciel | UE1  | B. Papegay |

Semaine 06
----------

### Lundi 8 février

| Quand       | Où        | Quoi        | Qui         |
|-------------|-----------|-------------|-------------|
| 14h00-17h00 | M3-Turing | [UE1 - archi](./ue1/archi/Readme.md) | Ph. Marquet |

### Mardi 9 février

| Quand       | Comment            | Quoi | Qui        |
|-------------|--------------------|------|------------|
| 8h30-12h00  | Présentiel, M3 226 | UE1  | L. Noé     |


### Mercredi 10 février

| Quand       | Comment                        | Quoi     | Qui       |
|-------------|--------------------------------|----------|-----------|
| 8h00-13h00  | Présentiel (M3 226)/Distanciel | DS - UE1 |           |
| 14h45-16h15 | Distanciel                     | Anglais  | N. Chapel |


### Jeudi 11 février

| Quand       | Comment    | Quoi | Qui          |
|-------------|------------|------|--------------|
| 8h30-10h00  | Distanciel | UE5  | C. Mieszczak |
| 10h15-12h00 | Distanciel | UE2  | P. Thibaud   |


Semaine 05
----------

### Lundi 1 février

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 13h00-16h00 | Distanciel | [UE1](./ue1/web/techno_web.md)  | B. Papegay |

### Mardi 2 février

| Quand       | Comment  | Quoi | Qui        |
|-------------|----------|------|------------|
| 8h30-11h45  | Distance | UE1  | P. Thibaud |
| 13h30-16h00 | Distance | UE1  | B. Papegay |

### Mercredi 3 février

| Quand      | Comment               | Quoi   | Qui                   |
|------------|-----------------------|--------|-----------------------|
| 8h00-13h00 | Présentiel/Distanciel | DS-UE2 | P. Thibaud/B. Papegay |

### Jeudi 4 février

| Quand       | Comment  | Quoi    | Qui        |
|-------------|----------|---------|------------|
| 10h00-12h00 | Distance | UE1     | B. Papegay |

Semaine 04
----------

Stage

Semaine 03
----------

Stage

Semaine 02
----------

### Lundi 11 janvier

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 13h00-16h00 | Distanciel | UE1  | B. Papegay |

### Mardi 12 janvier

| Quand       | Comment  | Quoi | Qui        |
|-------------|----------|------|------------|
| 8h30-11h45  | Distance | UE1  | B. Papegay |
| 13h45-15h45 | Distance | UE2  | P. Thibaud |

### Mercredi 13 janvier

| Quand       | Comment    | Quoi   | Qui           |
|-------------|------------|--------|---------------|
| 8h00-13h00  | Présentiel | DS-UE1 | SUP-Lavoisier |
| 15h30-17h30 | Distanciel | UE5    | C. Mieszczak  |


### Jeudi 14 janvier

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 10h30-12h30 | Distanciel | UE2  | P. Thibaud |

Semaine 01
----------

### Lundi 04 janvier

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 13h00-16h00 | Distanciel | UE1  | B. Papegay |

### Mardi 5 janvier

| Quand       | Comment  | Quoi | Qui        |
|-------------|----------|------|------------|
| 8h30-11h45  | Distance | UE1  | B. Papegay |
| 13h45-15h45 | Distance | UE2  | P. Thibaud |

### Mercredi 6 janvier

| Quand      | Comment    | Quoi | Qui        |
|------------|------------|------|------------|
| 8h30-11h30 | Distanciel | UE1  | B. Papegay |


### Jeudi 7 janvier

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 10h30-12h30 | Distanciel | UE2  | P. Thibaud |
| 13h30-16h00 | Distanciel | UE1  | B. Papegay |


Semaine 51
----------

### Lundi 14 décembre

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 8h30-10h30  | Distanciel | [UE2](./ue2/graphes/Readme.md)  | P. Thibaud |
| 13h00-16h00 | Distanciel | UE1  | B. Papegay |

### Mardi 15 décembre

| Quand      | Comment  | Quoi       | Qui        |
|------------|----------|------------|------------|
| 8h30-11h45 | Distance | UE1        | B. Papegay |
| après-midi | Présence | DS Anglais |            |


### Mercredi 16 décembre 

| Quand       | Comment    | Quoi   | Qui                    |
|-------------|------------|--------|------------------------|
| 8h00-13h00  | Présentiel | DS-UE2 | P. Thibaud, B. Papegay |
| 15h30-17h30 | Distanciel | UE5    | C. Mieszczak           |

### Jeudi 17 décembre

| Quand       | Comment    | Quoi                     | Qui        |
|-------------|------------|--------------------------|------------|
| 10h30-12h00 | Distanciel | UE1 (Leçon sécurisation) | B. Papegay |
| 13h30-16h00 | Distanciel | UE1                      | B. Papegay |


Semaine 50
----------

### Lundi 7 décembre

| Quand       | Comment    | Quoi | Qui        |
|-------------|------------|------|------------|
| 13h00-16h00 | Distanciel | UE1  | B. Papegay |

En autonomie : finir la correction du ds.

### Mardi 8 décembre

| Quand       | Comment  | Quoi | Qui        |
|-------------|----------|------|------------|
| 8h30-11h45  | Distance | UE1  | B. Papegay |
| 14h00-16h00 | Distance | [UE2](./ue2/tris/Readme.md)  | P. Thibaud |


### Mercredi 9 décembre 

| Quand       | Comment  | Quoi                  | Qui                           |
|-------------|----------|-----------------------|-------------------------------|
| 13h30-16h30 | Distance | UE2, La calculabilité | E. Wegrzynowski (avec le DIU) |


### Jeudi 10 décembre

| Quand       | Comment    | Quoi | Qui          |
|-------------|------------|------|--------------|
| 8h30-10h30  | Distanciel | UE5  | C. Mieszczak |
| 13h00-16h00 | Distanciel | UE1  | B. Papegay   |


Semaine 49
----------

Stage

Semaine 48
----------

Stage 

### Mercredi 25 novembre

| Quand       | Comment  | Quoi                | Qui         |
|-------------|----------|---------------------|-------------|
| 13h30-16h30 | Distance | Algoirhmes du texte | avec le DIU |

Semaine 47
----------

- fin de la correction

### Lundi 16 novembre

| Quand       | Comment  | Quoi | Qui        |
|-------------|----------|------|------------|
| 13h00-16h00 | Distance | UE1  | B. Papegay |


### Mardi 17 novembre

| Quand       | Comment  | Quoi                    | Qui         |
|-------------|----------|-------------------------|-------------|
| 8h30-12h00  | Distance | UE1                     | B. Papegay  |
| 13h30-16h30 | Distance | Programmation Dynamique | avec le DIU |

### Mercredi 18 novembre 

| Quand      | Comment  | Quoi                  | Qui        |
|------------|----------|-----------------------|------------|
| 8h00-13h00 | Distance | DS2, sujet sur moodle | B. Papegay |


### Jeudi 19 novembre

| Quand       | Comment  | Quoi                        | Qui        |
|-------------|----------|-----------------------------|------------|
| 10h00-12h00 | Distance | UE1, Correction écrit       | B. Papegay |
| 13h00-16h00 | Distance | [UE2](./ue1/sql2/Readme.md) | B. Papegay |

Le responsable de la formation commençant à perdre la boule,
Christophe n'a pas pu effectuer ses séances dans de bonnes conditions.
Il a eu la gentillesse de fournir ces liens concernant la mise en stage :

  * A propos du dossier : https://gitlab-fil.univ-lille.fr/capes/ue5/-/blob/master/m1/dossier/dossier.md
  * Tuto Mahara : https://gitlab-fil.univ-lille.fr/capes/ue5/-/blob/master/m1/dossier/mahara.md


Semaine 46
----------

    [Sujet à travailler à l'écrit](./devoirs/19-20/ds2/ds2.pdf) : 

### Lundi 9 novembre

| Quand       | Comment  | Quoi | Qui        |
|-------------|----------|------|------------|
| 13h00-16h00 | Distance | UE1  | B. Papegay |


### Mardi 10 novembre

| Quand       | Comment  | Quoi  | Qui        |
|-------------|----------|-------|------------|
| 8h30-12h00  | Distance | UE1   | B. Papegay |
| 13h30-16h30 | Distance | Écrit | Autonomie  |

### Mercredi 11 novembre 

Férié

### Jeudi 12 novembre

| Quand       | Comment  | Quoi                  | Qui          |
|-------------|----------|-----------------------|--------------|
| 8h30-10h30  | Distance | UE5                   | C. Mieszczak |
| 10h45-12h15 | Distance | UE1, Correction écrit | B. Papegay   |
| 13h30-16h00 | Distance | UE2, avec le DIU      | B. Papegay   |

Prévoir d'installer openssl 

Semaine 45
----------

### Lundi 2 novembre

| Quand       | Comment  | Quoi                         | Qui        |
|-------------|----------|------------------------------|------------|
| 10h15-12h00 | Distance | [UE2](./ue2/crr/2-11-20.org) | B. Papegay |
| 13h00-16h00 | Distance | [UE1/2](./ue2/Readme.md)     | B. Papegay |


### Mardi 3 novembre

| Quand      | Comment  | Quoi | Qui        |
|------------|----------|------|------------|
| 8h30-11h45 | Distance | UE1  | B. Papegay |

[icewalker](./ue2/icewalker/Readme.md)

### Mercredi 4 novembre 

| Quand       | Comment  | Quoi | Qui        |
|-------------|----------|------|------------|
| 13h30-15h30 | Distance | UE1  | B. Papegay |


### Jeudi 5 novembre

| Quand       | Comment  | Quoi                            | Qui          |
|-------------|----------|---------------------------------|--------------|
| 10h45-12h15 | Distance | [UE1](./ue1/graphes/graphes.md) | B. Papegay   |
| 13h15-16h15 | Distance | [UE2](./ue2/icewalker/Readme.md)   | B. Papegay   |

Semaine 44
----------

Interruption pédagogique

Semaine 43
----------

### Lundi 19 octobre ###

| Quand       | Où     | Quoi | Qui       |
|-------------|--------|------|-----------|
| 13h00-16h00 | M5-A15 | [UE1](./ue1/abr)  | B.Papegay |

### Mardi 20 octobre ###

| Quand      | Où     | Quoi        | Qui                      |
|------------|--------|-------------|--------------------------|
| 8h30-11h50 | M3 226 | UE1 - Oraux | E.Wegrzynowski,B.Papegay |


### Mercredi 21 octobre ###

| Quand       | Où     | Quoi | Qui            |
|-------------|--------|------|----------------|
| 8h30-11h50  | M1-202 | UE1  | E.Wegrzynowski |
| 13h30-17h30 | M3-226 | UE5  | C. Mieszczak   |

### Jeudi 22 octobre ###

| Quand       | Où     | Quoi | Qui             |
|-------------|--------|------|-----------------|
| 8h30-11h50  | M1-202 | UE1  | E. Wegrzynowski |
| 13h00-16h00 | M5-A16 | UE2  | B. Papegay      |

Semaine 42
----------

### Lundi 12 octobre ###

| Quand       | Où     | Quoi          | Qui        |
|-------------|--------|---------------|------------|
| 10h00-12h00 | M5-A15 | UE2 - Jupyter | B. Papegay |
| 13h00-16h00 | M5-A15 | [UE1](./ue1/huffman)           | B.Papegay  |

### Mardi 13 octobre ###

| Quand      | Où     | Quoi        | Qui                      |
|------------|--------|-------------|--------------------------|
| 8h30-11h50 | M3 226 | UE1 - Oraux | E.Wegrzynowski,B.Papegay |


### Mercredi 14 octobre ###

| Quand       | Où          | Quoi | Qui            |
|-------------|-------------|------|----------------|
| 8h30-11h50  | M1-202      | UE1  | E.Wegrzynowski |

### Jeudi 15 octobre ###

| Quand       | Où     | Quoi | Qui             |
|-------------|--------|------|-----------------|
| 8h30-11h50  | M1-202 | UE1  | E. Wegrzynowski |
| 13h00-16h00 | M5-A16 | UE2  | B. Papegay      |

Semaine 41
----------

### Lundi 5 octobre ###

| Quand       | Où     | Quoi                                | Qui       |
|-------------|--------|-------------------------------------|-----------|
| 13h00-16h00 | M5-A15 | [UE1](./ue1/pile_et_file/readme.md) | B.Papegay |

### Mardi 6 octobre ###

| Quand      | Où     | Quoi        | Qui                      |
|------------|--------|-------------|--------------------------|
| 8h30-11h50 | M3 226 | UE1 - Oraux | E.Wegrzynowski,B.Papegay |


### Mercredi 7 octobre ###

| Quand       | Où          | Quoi | Qui            |
|-------------|-------------|------|----------------|
| 8h30-11h50  | M1-202      | UE1  | E.Wegrzynowski |
| 13h30-15h30 | M3-Delattre | UE5  | C. Mieszczak   |

### Jeudi 8 octobre ###

| Quand       | Où     | Quoi | Qui             |
|-------------|--------|------|-----------------|
| 8h30-11h50  | M1-202 | UE1  | E. Wegrzynowski |
| 13h00-16h00 | M5-A16 | UE2  | B. Papegay      |

Semaine 40
----------

### Lundi 28 septembre ###

| Quand       | Où     | Quoi                 | Qui        |
|-------------|--------|----------------------|------------|
| 10h00-12h00 | M5-A15 | UE2 - [git](./ue1/git/gitlab.md), [markdown](./ue2/markdown/readme.md), [exo](./ue2/fibo_md/Readme.md) | B. Papegay |
| 13h00-16h00 | M5-A15 | [UE1 - Listes](./ue1/listes/listes.md)                  | B.Papegay  |

### Mardi 29 septembre ###

| Quand      | Où       | Quoi        | Qui                      |
|------------|----------|-------------|--------------------------|
| 8h30-11h50 | M1-Riesz | UE1 - Oraux | E.Wegrzynowski,B.Papegay |


### Mercredi 30 septembre ###

| Quand       | Où          | Quoi    | Qui            |
|-------------|-------------|---------|----------------|
| 8h00-13h00  | M1-Chatelet | DS1-UE1 | E.Wegrzynowski |

### Jeudi 1 octobre ###

| Quand       | Où     | Quoi | Qui             |
|-------------|--------|------|-----------------|
| 8h30-11h50  | M1-202 | UE1  | E. Wegrzynowski |
| 13h00-16h00 | M5-A16 | [UE2](./ue2/wator/readme.md)  | B. Papegay      |

Semaine 39
----------

### Lundi 21 septembre ###

| Quand       | Où      | Quoi                                                                   | Qui       |
|-------------|---------|------------------------------------------------------------------------|-----------|
| 13h00-16h00 | M5-A15  | UE1 - [TP conversion caracteres](./ue1/tp_codage_caracteres/Readme.md) | B.Papegay |
|             |         | [TP Base64](./ue1/base64/readme.md)                                    |           |
| 16h30-18h00 | SUP-207 | Anglais                                                                | N. Chapel |

### Mardi 22 septembre ###

| Quand      | Où       | Quoi        | Qui                      |
|------------|----------|-------------|--------------------------|
| 8h30-11h50 | M1-Riesz | UE1 - Oraux | E.Wegrzynowski,B.Papegay |


### Mercredi 23 septembre ###

| Quand       | Où          | Quoi | Qui            |
|-------------|-------------|------|----------------|
| 8h30-11h50  | M1-202      | UE1  | E.Wegrzynowski |
| 15h30-17h30 | M3-Delattre | UE5  | C. Mieszczak   |


### Jeudi 24 septembre ###

| Quand       | Où     | Quoi                                                                        | Qui             |
|-------------|--------|-----------------------------------------------------------------------------|-----------------|
| 8h30-11h50  | M1-202 | UE1                                                                         | E. Wegrzynowski |
| 13h00-16h00 | M5-A16 | UE2 - [Les bases de numération dans l'histoire](./ue2/numeration/Readme.md) | B. Papegay      |
|             |        | [circuits séquentiels](./ue2/sequentiel/readme.md)                          |                 |

### Vendredi 18 septembre ### 

Semaine 38
----------

### Lundi 14 septembre ###

| Quand       | Où     | Quoi                                                            | Qui       |
|-------------|--------|-----------------------------------------------------------------|-----------|
| 10h00-12h00 | M5-A4  | UE2 - [Programmes](./ue2/programmes/)                           | B.Papegay |
| 13h00-16h00 | M5-A15 | UE1 - TP : [Phrases de passe](./ue1/phrases_de_passe/readme.md) | B.Papegay |


### Mardi 15 septembre ###

| Quand      | Où       | Quoi        | Qui                      |
|------------|----------|-------------|--------------------------|
| 8h30-11h50 | M1-Riesz | UE1 - Oraux | E.Wegrzynowski,B.Papegay |


### Mercredi 16 septembre ###

| Quand       | Où     | Quoi         | Qui            |
|-------------|--------|--------------|----------------|
| 8h30-11h50  | M1-202 | UE1 - Codage | E.Wegrzynowski |
| 13h30-15h30 | C1-003 | UE5          | C. Mieszczak   |


### Jeudi 17 septembre ###

| Quand       | Où     | Quoi                                                                        | Qui             |
|-------------|--------|-----------------------------------------------------------------------------|-----------------|
| 8h30-11h50  | M1-202 | UE1 - Codage                                                                | E. Wegrzynowski |
| 13h00-16h00 | M5-A16 | UE2 - [Les bases de numération dans l'histoire](./ue2/numeration/Readme.md) | B. Papegay      |


### Vendredi 18 septembre ### 



Semaine 37
----------

### Lundi 7 septembre ###

| Quand       | Où     | Quoi | Qui                       |
|-------------|--------|------|---------------------------|
| 13h00-16h00 | M5-A15 | UE1  | B.Papegay/E. Wegrzynowski |

### Mardi 8 septembre ###

| Quand      | Où       | Quoi | Qui             |
|------------|----------|------|-----------------|
| 8h30-11h50 | M1-Riesz | UE1  | E. Wegrzynowski |

### Mercredi 9 septembre ###

| Quand      | Où     | Quoi | Qui            |
|------------|--------|------|----------------|
| 8h30-11h50 | M1-202 | UE1  | E.Wegrzynowski |
|            |        |      |                |
### Jeudi 10 septembre ###

| Quand       | Où     | Quoi                           | Qui             |
|-------------|--------|--------------------------------|-----------------|
| 8h30-11h50  | M1-202 | UE1                            | E. Wegrzynowski |
| 13h00-16h00 | M5-A16 | [UE2](./ue2/logisim/Readme.md) | B. Papegay      |


### Vendredi 11 septembre ### 

| Quand       | Où     | Quoi                                     | Qui       |
|-------------|--------|------------------------------------------|-----------|
| 13h30-15h30 | M1-202 | UE3 - [présentation](./ue3/M1/Readme.md) | B.Papegay |

Semaine 36
----------

### Mercredi 2 septembre ###

  - pré-rentrée (Salle A1, Bâtiment M5) à 13h00
  


